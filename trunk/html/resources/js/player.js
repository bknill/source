
	function start(){
	    //no html5 or admin load Flash
		  if(subdomain == 'gms' || !canvas2DSupported || isPreIE11())
		       preloadFlash();
		     else
		       loadBeekJs(); 
	}



	function showHideHelp(){
	        $('#guidehelpcontainer').hide();
	        $('#descriptionHolder').hide();
	        
	    };
    
  

    function isIOSBrowser()
    {
        return (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
    }

    function isMobileBrowser()
    {
        return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    }

    function startMyApp()
    {
        var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
        if(iOS)
            launchIOSApp();

        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") &gt; -1;
        if(isAndroid)
            launchAndroidApp();
     }

    function launchIOSApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'itms://itunes.apple.com/us/app/beekAppId';
      }, 300);
    }

    function launchAndroidApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'http://m.#{GuideViewController.domain}/android';
      }, 300);
    }

    function hasFlash()
    {
        var version = swfobject.getFlashPlayerVersion();
        return (version["major"] >= 11);
    }
    
    function getIEVersion() {
        var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
        return match ? parseInt(match[1]) : undefined;
    }
	
	function isIE () {
    	return navigator.userAgent.toLowerCase().indexOf('msie') !== -1 || navigator.appVersion.toLowerCase().indexOf('trident/') > 0; 
    	  
    }
    
    
    function isPreIE11(){
    	return isIE() && getIEVersion() < 11;
    }
    


    function showUpgradeFlash()
    {
        loadFlash();
        document.getElementById("player-div").className = "flash-visible";
        document.getElementById("preview").className = "preview-hidden";
    }
    
    
    function loadBeekJs()
    {
        $("#preview").hide();
        $("#guidehelpcontainer").hide();
        $("#swfcontainer").hide();
        $("#player-div").hide();
        loadBeekJsFiles();     
    }

 

    function trackSceneView(sceneId)
    {
        _gaq.push(['_trackPageview', guidePath + '/s' + sceneId]);
    }

    function trackEvent(category, action, label)
    {
        _gaq.push(['_trackEvent', category, action, label]);
    }

    


        $(window).resize(function() {
            showHideHelp();
        });
        

        function player_DoFSCommand(command, args) {
        if (command == "openWindow") { 
            var windowArgs = args.split("|"); 
            var domain = windowArgs[0]; 
            var width = windowArgs[1]; 
            var height = windowArgs[2]; 
            
            domain.replace('"', '');

            $('.iframeshim').css("left",(($( window ).width() - width)/2) + 10 +"px");
            $('.iframeshim').css("top",(($( window ).height() - height)/2) + 30 +"px");
            $('.iframeshim').css("width",width - 20 +"px");
            $('.iframeshim').css("height",height - 40 +"px");
            $('.iframeshim').attr("src",domain);

            $('.iframe-container').show();
       };
        
        if (command == "closeWindow") {$('.iframe-container').hide();}
    }
    

	function showPreloader()
	{
	    $("preloader").css({'display' : 'block', 'visibility' : 'visible'});
	    
	    showProgress(0.2);
	}

    function preloadFlash()
    {
        showHideHelp();
        showPreloader();
        
        if(!hasFlash()){
        	$('preloadText').append("<span>Beek required WebGL, HTML5 Canvas or Flash Version 11 to run. Please update Flash to view this content or use a modern browser</span>");
        	return;
        }

        loadFlash();
        loadImmediately = true;
    }
    
    function loadFlash()
    {
        if(flashLoaded)
            return;

        flashLoaded = true;

        showProgress(0.2);

        var params = {
            "quality" : "high",
            "bgcolor" : "#ffffff",
            "allowscriptaccess" : "always",
            "allowfullscreen" : "true",
            "wmode" : "direct"
        };

        var attributes = {
            "id" : "player",
            "name" : "player",
            "align" : "top"
        };

   
     //   if (window.location.protocol == 'https:'){};
        swfUrl = swfUrl.replace('http://cdn.beek.co','https://d3ixl5oi39qj40.cloudfront.net');


        swfobject.embedSWF(
                swfUrl,
                "player-div",
                "100%", "100%",
                "10.2.0",
                "/resources/swf/playerProductInstall.swf",
                flashvars,
                params,
                attributes,
                function(e) {
                    if(e.success && isIE()){ 
                    	//fail safe for shitty IE bugs where FSCommand not working
                    	setTimeout(function(){if(!flashComplete)showFlash();},6000);
                    	}
                }
        );
    }

    var currpercent = 0;
    var timeout;



    function showProgress(percent, endfunction)
    {
        currpercent += 0.02;

        var width = $('document').width();	

        var barWidth = $("#preloadtrack").width() * currpercent;

        $("#preloadbar").width(barWidth);

        if(currpercent < percent)
            timeout = setTimeout("showProgress("+percent+", "+endfunction+");", 20);

        else if(endfunction != null)
            endfunction();
    }

    
	var loadImmediately = false;
	var flashLoaded = false;
	var flashComplete = false;

    
    function showFlash()
    {
        clearTimeout(timeout);
        flashComplete = true;

        showProgress(1, setSwfVisible);
    }

    function setSwfVisible()
    {
        $('#swfContainer').css({'left': '0', 'visibility' : 'visible', 'display' : 'block'});
        $('#preloader').remove();
        $('#guideHelpContainer').remove();
        $('#preview').hide();
        $('#container').remove();
        $('#guide').remove();
    }

    function trackVisitKey(key)
    {
        _gaq.push(['_setCustomVar', 1, 'key', key, 1]);
    }
    
    

    var STAGE_SWF_LOADED = 1;
    var STAGE_GUIDE_LOADED = 2;
    var STAGE_SCENE_LOADED = 3;
    
    function setSwfStage(stage)
    {
    	
        switch(stage)
        {
            case STAGE_SWF_LOADED:
                showProgress(0.5);
                break;

            case STAGE_GUIDE_LOADED:
                showProgress(0.7);
                break;

            case STAGE_SCENE_LOADED:
                showFlash();
                break;
        }
    }

    
    	  
    function loadBeekJsFiles(){
    	
    	showPreloader();

    	Sid.js(["//ajax.googleapis.com/ajax/libs/threejs/r69/three.min.js",
		    	"../../resources/js/beekjs/Detector.js",
				"../../resources/js/beekjs/beek.min.js",
				"../../resources/js/beekjs/tween.min.js"],
				
				function(){
	    		showProgress(0.4);
				init();
    		}
		);

    }
    
	//IE bug
    (function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
    	