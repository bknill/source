var hs_bubble, 
	refractSphereCamera,
	selectedHotspot,
	viewingHotspot,
	hotspotLat,
	hotspotLon,
	closeButton = false,
	group,
	photos,
	posters,
	sounds;


//Sid.js([
//
//    	"../../resources/js/beekjs/hotspots.bubbles.js",
//    	"../../resources/js/beekjs/hotspots.boards.js",
//    	"../../resources/js/beekjs/hotspots.photos.js",
//    	"../../resources/js/beekjs/hotspots.posters.js",
//    	"../../resources/js/beekjs/hotspots.sounds.js"
//    	]);

Sid.css(['http://fonts.googleapis.com/css?family=Lato:400,300,900']);


	function hotspots(){

		console.log('hotspots()');

        for(var b in bubbles)
        	bubble(bubbles[b]);
        for(var b in boards)
        	board(boards[b]);
        for(var p in photos)
        	createPhoto(photos[p]);
        for(var p in posters)
        	createPoster(posters[p]);
        for(var s in sounds)
        	createSound(sounds[s]);
        
	}
	
	function textSprite(text,size,color) {
	    var font = "Lato";
	    padding = 30;

	    font = size + "px " + font;

	    var canvas = document.createElement('canvas');
	    var context = canvas.getContext('2d');
	    context.font = font;

	    var metrics = context.measureText(text),
	        textWidth = metrics.width;

	    canvas.width = textWidth + 3;
	    canvas.height = size + 20;

	    context.font = font;
	    context.fillStyle = color;
	    context.fillText(text, 0, size + 3);

	    var texture = new THREE.Texture(canvas);
	    texture.needsUpdate = true;
	    
	    var mesh = new THREE.Mesh(
	    new THREE.BoxGeometry(canvas.width, canvas.height,1),
		    new THREE.MeshBasicMaterial({
		    	map: texture,
		    	transparent: true
		    })
	    );	
		
	    return mesh;
	}
	
	
	function hotspotClick(object){
		console.log('hotspots.hotspotClick()');
		selectedHotspot = object.parent;
		
		selectedHotspot.castShadow = true;
		selectedHotspot.receiveShadow = true;
		isUserInteracting = false;
		

		
		//sort bubbles
		if(selectedHotspot.hotspotData.targetSceneId){
			object.material.opacity = 0.5;
			tweenToSelectedHotspot(selectedHotspot, function(){checkForSceneToLoad()}, true );
			getScene(selectedHotspot.hotspotData.targetSceneId);
		}
		//should be signboards
		else if(object.hotspotData && viewingHotspot){
			object.material.opacity = 0.75;
			selectedHotspot = null;
			getScene(object.hotspotData.scene.id);
		}
		//everything else tweens
		else if(!viewingHotspot)
			tweenHotpotToCamera(object.parent);
	}
	
	function tweenHotpotToCamera(object){
		console.log('hotspots.tweenHotpotToCamera()');
		
		selectedHotspot = object;
		
		//don't tween bubbles
		if(selectedHotspot.hotspotData.targetSceneId)
			return;
		
		//make sure we've got some light
		 //flashlight.target = camera.position;
       
		//this might be a bad way to copy values. 
		object.originalPosition = JSON.parse( JSON.stringify(object.position) );
		object.orginalRotation = JSON.parse( JSON.stringify(object.rotation) );
		object.orginalQuaternion = JSON.parse( JSON.stringify(object.quaternion) );

		//magic maths to get objects around the same size of the screen
		var vFOV = camera.fov * Math.PI / 180; 
		var ratio = 2 * Math.tan( vFOV / 2 );
		var screen = ratio * (window.innerWidth * 0.5/ window.innerHeight * 0.7) ; 
		var size = getCompoundBoundingBox( object ).max.y;
		var dist = (size/screen) * (object.scale.x * 2); 
		
		var pLocal = new THREE.Vector3( 0, 0, -dist );
		var target = pLocal.applyMatrix4( camera.matrixWorld );
		var tweenMove = new TWEEN.Tween(object.position).to(target, 1500).easing(TWEEN.Easing.Cubic.InOut);
		tweenMove.onUpdate(function(){object.lookAt(camera.position);});
		tweenMove.onComplete(function(){setLights(0);addCloseButton();});
		tweenMove.start();
		
		if(object.hotspotData.showIcon){
			object.children[0].visible = false;
			object.children[1].visible = true;
		}

		viewingHotspot = object;
	}
	
	function addCloseButton(){

		if(closeButton)
			return;
			
		$( "body" ).append( '<div id = "closeHotspotButton" >X</div>');
		$( "#closeHotspotButton" ).click(function(){removeCloseButton();});
		
		closeButton = true;
	}
	
	function removeCloseButton(){
		
		$( "#closeHotspotButton" ).remove();
		deselectHotspot();
		closeButton = false;
	}
	
	function tweenToSelectedHotspot(object , callback, zoom){
			
		var newLatLon = {lat: lat, lon : lon};
		var targetLatLon = hotspotPanTiltToLatLon(object.hotspotData.pan,object.hotspotData.tilt );
		var newFov = {nFov : camera.fov};
		var targetFov = {nFov : object.hotspotData.distance*0.02};
		var distance = Math.abs(targetLatLon.lon - newLatLon.lon)*10;
		
		if(newLatLon.lon < 0)
			targetLatLon.lon -= 360;
		if(newLatLon.lon > 360)
			targetLatLon.lon += 360;
		
		//set up zoom tween
		var tweenZoom = new TWEEN.Tween( newFov ).to(targetFov, 2000).easing(TWEEN.Easing.Sinusoidal.InOut);
		tweenZoom.onUpdate(function(){
			camera.fov = newFov.nFov;
			camera.updateProjectionMatrix();
		});
		//tween lon/lat to tween camera
		var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, distance).easing(TWEEN.Easing.Sinusoidal.Out);
		tweenLon.onUpdate(function(){
			lon = newLatLon.lon;
			lat = newLatLon.lat;
			if(lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5 && zoom) 
				tweenZoom.start();
			});
		tweenLon.onComplete(function(){callback.call()});
		tweenLon.start();

	}
	
	function checkForSceneToLoad(){
		console.log('checkForSceneToLoad()');
		if(sceneReady)
			loadScene();
		else if(selectedHotspot)
			setTimeout(checkForSceneToLoad,100);
	}
	
	function deselectHotspot()
	{
		if(!selectedHotspot)
			return;
		
		var shs = selectedHotspot;
		selectedHotspot = null;
		viewingHotspot = null;
		
		new TWEEN.Tween(shs.position).to(shs.originalPosition, 1000).easing(TWEEN.Easing.Sinusoidal.InOut)
		.onComplete(function(){
			
			//turn posters back into icons
			if(shs)
				if(shs.hotspotData.showIcon){
					shs.children[0].visible = true;
					shs.children[1].visible = false;
				};
	
			}).start();
		
		new TWEEN.Tween(shs.rotation).to(shs.originalRotation, 1000).start();
		new TWEEN.Tween(shs.quaternion).to(shs.orginalQuaternion, 1000).start();
	
    	setLights(0.8);
    	

	}
	
	
	function powerOf2Down(value)
	{
		if(value < 80) 
			return 64;
		else if(value < 150) 
			return 128;
		else if(value < 400) 
			return 256;
		else if(value < 800)
			return 512;
		
		return 1024;
	}

	 function powerOf2Up(value)
	{
		if(value <= 64) 
			return 64;
		else if(value <= 128) 
			return 128;
		else if(value <= 256) 
			return 256;
		else if(value <= 512)
			return 512;
		
		return 1024;
	}
	 
	 
	 function getCompoundBoundingBox(object3D) {
		    var box = null;
		    object3D.traverse(function (obj3D) {
		        var geometry = obj3D.geometry;
		        if (geometry === undefined) return;
		        geometry.computeBoundingBox();
		        if (box === null) {
		            box = geometry.boundingBox;
		        } else {
		            box.union(geometry.boundingBox);
		        }
		    });
		    return box;
		}
	
	
	