var guideSections = [],
	childSections = [],
	guideSection,
	guideScenes = [],
	guideScene,
	guideData,
	wall,
	$dragging,
	guideOpen;

	//this organises all the guideData 
	function createGuide( data ){
		
        guideData = data;

		//add sections
		for(var section in guideData.guideSections){
			guideSections.push(guideData.guideSections[section]);
			//add scenes
			for(var scene in guideData.guideSections[section].guideScenes)	
			guideScenes.push(guideData.guideSections[section].guideScenes[scene]);
		};	
		
		//find child sections
		for(var cs in guideSections){
			if(guideSections[cs].parent_section_id)
				for(var s in guideSections)
					if(guideSections[s].id == guideSections[cs].parent_section_id){
						
						if(!guideSections[s].childSections)
							guideSections[s].childSections = [];
						
						guideSections[s].childSections.push(guideSections[cs]);
						guideSections[cs].parent = guideSections[s];
					}
		}
		
		//give all sections with scenes an image, if no image remove section (it's probably a dev/test one)
		for(var s in guideSections){
			var section = guideSections[s];
			if(!section.page_image){
				if(section.guideScenes.length > 0)
					section.image = section.guideScenes[0].scene.thumbName;
				else if(section.childSections){
					if(section.childSections[0].guideScenes.length > 0)
						section.image = section.childSections[0].guideScenes[0].scene.thumbName;
					else if(section.childSections[0].childSections[0].guideScenes.length > 0)
						section.image = section.childSections[0].childSections[0].guideScenes[0].scene.thumbName;
					else if(section.childSections[0].childSections[0].childSections[0].guideScenes.length > 0)
						section.image = section.childSections[0].childSections[0].childSections[0].guideScenes[0].scene.thumbName;
					else
						guideSections.splice(s, 1);
				}
				else
					guideSections.splice(s, 1);
			}
			else
				section.image = section.page_image;
		}
		
	}
	
	//this puts the bits on screen
	function createGuideElements(){

		//set up the basic guide elements
		$('#guideButton').remove();
		$('#prevButton').remove();
		$('#nextButton').remove();

		$( "body" ).append( '<div id = "guideBack" ><i class="fa fa-arrow-left"></i></div>'  +
				 			'<div id = "guideClose" ><i class="fa fa-minus"></i></div>'+
				 			'<div id = "guide" />' 
				);
		
		//create guide buttons
		$( "#guideBack" ).click(function(){
			$('#guide').animate({'left' :  0},"slow");
		});		
		
		$( "#guideClose" ).click(function(){ closeGuide(); } );
		
		//create guide
		$( "#guide" )
		.append('<div id="sceneTextBackground" class="sceneTextBackground block" ><div id="sceneText" class="sceneText"/></div>' +
				'<div id = "sceneContainer" class="sceneContainer block" data-nested=".sceneButton" data-gutterX=2 data-gutterY=2 data-fitSize=false />' 
				+'<div id = "contents" class="contentsContainer section block"  data-nested=".level-1" data-gutterX=10 data-gutterY=10 data-fitSize=false  />'
        );
		
		//create scene Buttons
		$('#guide').append();
		$('#sceneContainer').append('<div id="telephone" class="telephone sceneButton"><i class="fa fa-phone"></i></div>');
		$('#sceneContainer').append('<div id="email"  class="email sceneButton"><i class="fa fa-envelope"></i></div>');
		$('#sceneContainer').append('<div id="website"  class="website sceneButton"><i class="fa fa-laptop"></i></div>');
	
		updateGuideSceneTextButtons();
		
		//create the contents and sections
		for(var s in guideSections)
			if(!guideSections[s].parent_section_id)
				createSection(guideSections[s],s);
		
		//get the text before adding to FreeWall
		updateGuideScene(currentScene.id);
		
		//reset the contents width so no massive gap
		var count = $( "#contents" ).children().length;
		console.log(count);
		if(count > 3)
			$( "#contents" ).width( Math.ceil(count/3) * 220);
		console.log(Math.ceil(count/3));
		
		
		//on mobile go fullscreen
		if(isMobile)
			launchFullscreen(document.documentElement);
		
		//do the FreeWall bits
		setTimeout(function(){

	    wall = new freewall("#guide");
        wall.reset({
			selector: '.block',
        	fitSize:    0,
			gutterY: 	10,
			gutterX: 	40,
			onResize: 	function() {
							wall.fitHeight( $('#guide').height() );
						}
		});
       
	        $(window).trigger("resize");  
	        
		},100);

			
		//These are the dragging functions for the guide
			$dragging = null;
		    $('#guide').on("mousedown", dragGuideMouse);	
	

		   document.getElementById("guide").addEventListener('touchstart', dragGuideTouch, false);
		  
		    //give it a moment to build guide before tween in
		   setTimeout(function(){
			//animate the guide in
			var l = isMobile ? '2%' : '10%';
			$('#guide').animate({'left' :  l },"slow");
	    	guideOpen = true;
	    	
	    	//turn down the lights
	    	setLights(0);
	    	
		   },250);
	
	}
	
	
	
	 function dragGuideMouse(e) {
		 
	        $('#guide').addClass('draggable');
	        var pos = $('#guide').css('left');
	        var now = e.pageX - (parseInt($(this).css('left')) || 0);
	        
	        $('#guide').on("mousemove", function(e) {
				e.stopPropagation();
	        	
	            if ($dragging ) {
	            	$('#guide').offset({
	            		left: e.pageX - now
	                });
	            }
	            
	        });
	        
	        $dragging = $('#guide');
	        
	        $('#guide').on("mouseup", function(e) {
		        $dragging = null;
		        $('#guide').removeClass('draggable');
		    });

	    }
	 
	 //handles all buttons because detecting for button drag.. poss remove other 
	 function dragGuideTouch(e) {
		 
	    var button = e.target;
    	
	    e.preventDefault();
	 
        $('#guide').addClass('draggable');
        var pos = parseInt( $('#guide').css('left') );
        var now = e.touches[0].pageX - (parseInt($(this).css('left')) || 0);
        
        $('#guide').on("touchmove", function(e) {
			//e.stopPropagation();

            if ($dragging ) {
            	$('#guide').offset({
            		left: e.originalEvent.targetTouches[0].pageX - now
                });
            }
            
        });
        
        $dragging = $('#guide');
        
        $('#guide').on("touchend", function(e) {
	        $dragging = null;
	        $('#guide').removeClass('draggable');
	        
	        //if guide hasn't moved
	        var  newPos = parseInt( $('#guide').css('left') );
	        
	        if(pos > newPos - 10 && pos < newPos + 10 ){

	        	
				var id = $(button).attr("sectionId");
				var targetSceneId = $(button).attr('sceneId');
				
				console.log();
				
				if(id){//this is a section button so tween
					var left = parseInt( $('.'+ id).css('left') ) - $(window).width() * 0.1;
					setLights(0);
					$('#guide').animate({'left' :  - left},"slow");
					
				} else if(targetSceneId){ //load the scene
					getScene(targetSceneId);
					updateGuideScene(targetSceneId);

					//show the text
					$('#guide').animate({'left' :  0},"slow");
					setTimeout( setLights, 1000, 0.1 );
					e.stopPropagation();
				};
	        	
	        }
	        		
	    });

    }
	
	
	function createSection(section,order){

		//add section blocks
		$( "#guide" )
		.append('<div id="' + 
				section.id + '"   class="'+ 
				section.id + ' section block" data-nested=".level-1" data-gutterX=10 data-gutterY=10 data-fitSize=false ><div id="title_'+
				section.id +'" class="sectionTitle level-1" sectionId="'+
				section.id +'"> <h1>' +
				section.title + '</h1></div></div>'	
		);
		
		//buttons in contents or parents sections
		if(!section.parent_section_id)
			addSectionButton('contents',section);
		else
			addSectionButton(section.parent_section_id,section,order);
		
		//create map or image
		if(section.page_image) {
			$( "#" + section.id ).append( '<div class="map level-1" id="map_'+ section.id +'">' );
			$( "#map_" + section.id ).css({
				'background-image' : 'url("http://cdn.beek.co/' + section.page_image + '" )'
			});
			
			
			
			//child section map markers
			for(var cs in section.childSections){
				
				$( "#map_" + section.id ).append('<div class="mapMarker" id="mapMarker_' +
						section.childSections[cs].id + '"><p>' +
						(parseInt(cs) + 1) + '</p></div>');
				
	
				$( '#mapMarker_' + section.childSections[cs].id).css({
					'top' : 100 +  section.childSections[cs].pagePositionY, 
					'left' : 100 + section.childSections[cs].pagePositionX 
				});
				
			}
			
			//scene map markers
			for( var gs in section.guideScenes ){
				
				$( "#map_" + section.id ).append('<div class="mapMarker" id="mapMarker_' +
						section.guideScenes[gs].id + '"><p>' +
						(parseInt(gs) + 1) + '</p></div>');
				
				
				$( '#mapMarker_' + section.guideScenes[gs].id).css({
					'top' : 100 +  section.guideScenes[gs].pagePositionY, 
					'left' : 100 + section.guideScenes[gs].pagePositionX 	
				});
				
			}
			
		}
		
		//create the childSections buttons
		if(section.childSections)
			for(var cs in section.childSections)
				createSection( section.childSections[cs], cs );
		
		//create the guideScene buttons
		if(section.guideScenes)
			addGuideSceneButtons( section );
		
		//reset the block width so it doesn't have massive gaps in freewall
		var count = $( "#" + section.id ).children().length;
		if(count > 2 && !section.page_image)
			$( "#" + section.id ).width( Math.round(count/2) * 200);
		else if(count > 2 && section.page_image)
			$( "#" + section.id ).width( 490 + (count - 2)/3 * 200);
	}
	
	
	function addSectionButton(parent, section, order){
		
		//add the button div to the parent
		$( "#" + parent )
		.append('<div id="sectionButton_' + 
				section.id + '" sectionId="'+
				section.id + '" class="level-1"> <span>' +
				section.title + '</span></div>'
		);
		
		//add class depending on if it goes into contents section
		if(!section.parent_section_id)
			$('#sectionButton_'+ section.id).addClass("contents");
		else
			$('#sectionButton_'+ section.id).addClass("contents guideScene");
		
		$('#sectionButton_'+ section.id).css({
			'background-image' : 'url("http://cdn.beek.co/' + section.image + '")'
		});
		
		//add the map number
		if(section.parent)
			if(section.parent.page_image)
				$('#sectionButton_'+ section.id).append('<div class="mapNumber"><p>'+ (parseInt(order) +1) + '</p></div>');
		
		
		//click handler
		var l;
		
		//mouse down check left
		$('#sectionButton_'+ section.id).mousedown(function(e){
			l = Math.round($('#guide').offset().left);
		});
		
		//click function
		$('#sectionButton_'+ section.id).mouseup(function(e){
			
				//check if guide has moved before firing events
				if(l != Math.round($('#guide').offset().left))
					return;
				
				//stop dragging
	            $dragging = null;
			
				//find the right place to tween to
				var id = $(this).attr("sectionId");
				var left = parseInt( $('.'+ id).css('left') ) - $(window).width() * 0.1;
				
				//tween guide and background
				setLights(0);
				$('#guide').animate({'left' :  - left},"slow");
		});
		
		
		
	}
	
	//this adds the buttons that link to scenes
	function addGuideSceneButtons( section ){
		
		for(var sc in section.guideScenes){
			
			var guideScene = section.guideScenes[sc];
			
			//create the button div
			$( '#' + section.id ).append('<div id="' + 
					guideScene.id + '" class="guideScene level-1" sceneId="'+
					guideScene.scene.id + '"><span>' +
					guideScene.titleMerged + '</span></div>'
			);
			
			//background image
			$( '#'+ guideScene.id ).css({
				'background-image' : 'url("http://cdn.beek.co/' + guideScene.scene.thumbName + '")'
			});
			
			//add the map number
			if(section.page_image)
				$( '#'+ guideScene.id ).append('<div class="mapNumber"><p>'+ (parseInt(sc) +1) + '</p></div>');
			
			//click handler
			var left;
			
			//copy the guide position
			$('#'+ guideScene.id).mousedown(function(e){
				left = Math.round($('#guide').offset().left);
			});
			
			
			//complicated mouseup so you can still drag guide with buttons
			$('#'+ guideScene.id).mouseup(function(e){	

				//check if guide has moved before firing events
				if(left != Math.round($('#guide').offset().left))
					return;
				
				//stop dragging
                $dragging = null;
                
                //load the scene
				var targetSceneId = $(this).attr('sceneId');
				getScene(targetSceneId);
				closeGuide();

			});
				
		};
		
	}
	
	
	//set the guideScene variable
	function updateGuideScene(id){
		console.log('updateGuideScene('+id+')');
		
		for(var s in guideScenes)
			if(guideScenes[s].sceneId == id){
				guideScene = guideScenes[s];
				updateGuideSceneTextButtons();
				processGuideSceneHotspots();
			}
	}
	
	//set the text
	function updateGuideSceneTextButtons()
	{
		console.log('updateGuideSceneTextButtons()');
			
		if(!guideScene)
			return;
		
		var $desc = (guideScene.description || guideScene.scene.description || "")
      
		//filter out Flash rubbish code
        .replace(/<p.+?>/ig, "<p> ")
        .replace(/<L.+?>/ig, "<li>")
        .replace(/<[b|u]>/ig, " ")
        .replace(/<\/[b|u]>/ig, " ")
        .replace(/<TEXTFORMAT.+?>/g, " ")
        .replace(/<\/TEXTFORMAT>/g, " ")
        .replace(/<FONT.+?>/g, " ")
        .replace(/<\/FONT>/g, " ")
        .replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig, "<a href=\"javascript:guideLink(\'$1\')\"  >$2</a>");

		var height = $('window').height() * 0.7;
		
		$('.sceneText').html('<h1 id="title">' +guideScene.titleMerged+ '</h1><span> ' + $desc + '</span>');

		guideScene.links = $('.sceneText a').map(function(){
		    return this.getAttribute('href').split(/'/)[1];
		}).get();				

		if(guideScene.scene.phone)$('.telephone').show(); else $('.telephone').hide();
		if(guideScene.scene.email)$('.email').show(); else	$('.email').hide();
		if(guideScene.scene.infoURL)$('.website').show(); else $('.website').hide();
		
	}
	
	function guideLink(data){
		
		if(guideOpen)
			setLights(0.6);
		
		if(data.indexOf('|') > 0){
			var look = data.split('|');
			panoLook(look[0], look[1], look[2]);
		}
		else
			hotspotLook(data);
	}
	
	
	function  closeGuide(){
		
		$('#guide').animate({'left' :  '2000' },"slow", function(){
			
			$('#guide').empty();
			$('#guide').remove();
		});
	
		//turn up the lights
		setLights(0.8);
		
		$('#guideBack').remove();
		$('#guideClose').remove();

		openGuideButton();
		
		//exitFullscreen();
		
    	guideOpen = false;	
	
	}
	
	function openGuideButton(){
		
		$( "body" ).append( '<div id = "guideButton" ><i class="fa fa-plus"></i></div>'+
							'<div id = "prevButton" class="nextPrev" ><i class="fa fa-chevron-left"></i></div>' +
							'<div id = "nextButton" class="nextPrev" ><i class="fa fa-chevron-right"></i></div>' 
						   );
		
		$('#guideButton').click(function(){ createGuideElements(); });
		$('#prevButton').click(function(){ prevScene(); });
		$('#nextButton').click(function(){ nextScene(); });
		
	}
	
	
	function processGuideSceneHotspots(){
        for (ix in guideScene.photos) 
            photos.push(guideScene.photos[ix]); 
	}
	
	function nextScene(){
		
		var int = guideScenes.indexOf( guideScene ) ;
		var next =  int + 1 < guideScenes.length -1
					? guideScenes[ int + 1 ] 
					: guideScenes[ 0 ]; 
					
		getScene(  next.scene.id );
	}
	
	function prevScene(){

		var int = guideScenes.indexOf( guideScene ) ;
		var prev =  int - 1 > 0
					? guideScenes[ int - 1 ] 
					: guideScenes[ guideScenes.length - 1 ]; 
					
		getScene(  prev.scene.id );
	}
	
	
	function sceneInGuide(sceneId){
		
		for(var i in guideScenes)
			if(guideScenes[i].scene.id == sceneId)
				return true;
		
		return false;
			
		
	}
	
	function play(links){
		
		console.log( links );
		var i;
		
		   setTimeout(function () {    
			   	guideLink( links[i] );        
			      i++;                    
			      if (i < guideScene.links.length) {           
			         play();            
			      }                        
			   }, 3000);
	}
	

	