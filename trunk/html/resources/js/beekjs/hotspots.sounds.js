var context, 
	volume = 0.5, 
	initialised = false,
	enabled = true;
	sceneSounds = [];


	function createSound(data){
		
		console.log( 'createSound()' );
		
		if(!initialised)
			initialiseWebAudio();
		
		var soundFileName = 'http://cdn.beek.co/' + data.file.realName;

		loadSound( soundFileName, data );
	};
	
	function loadSound(url, data ) {
	    var request = new XMLHttpRequest();
	    request.open('GET', url, true);
	    request.responseType = 'arraybuffer';
	    // Decode asynchronously
	    request.onload = function() {
	        context.decodeAudioData(request.response, function(buffer) {
	            myAudioBuffer = buffer;
	            playSound(myAudioBuffer, data);//Play Audio Track
	        });
	    }
	    request.send();
	 }
	
	function playSound(buffer,  data) {
		
		if(!enabled)
			return;
		
		var sound = {};
		sound.source = context.createBufferSource();
		sound.volume = context.createGain();
		sound.source.connect(sound.volume);
		sound.volume.connect(context.destination);

		sound.source.buffer = buffer;
		sound.data = data;
		
	     if(data.looping == '1')
	    	 sound.source.loop = true;
	     
	     //sound.source.connect(context.destination);
	     sound.source.start();
	     
	     sceneSounds.push( sound );
	 }
	
	
	function initialiseWebAudio(){
		
		initialised = true;
		// Create a new audio context.
		 try {
			    // Fix up for prefixing
			    window.AudioContext = window.AudioContext||window.webkitAudioContext;
			    context = new AudioContext();
				context.listener.setPosition(0, 0, 0);
				mainVolume = context.createGain();
				mainVolume.connect(context.destination);
				mainVolume.gain.value = 0.5;
			  }
			  catch(e) {
				  enabled = false;
		  }
		
	}	
	
	
	function updateSceneSounds( lon )
	{
	
		for(var s in sceneSounds){
			var sound = sceneSounds[s];

			var soundPan = sound.data.pan;
			var soundVol = 1 - (sound.data.distance / 1000);

			if (soundPan < 0)
				soundPan = (soundPan % 360) + 360;
			else
				soundPan = soundPan % 360;
			
			var distance = Math.abs(soundPan - (lon + 180)) % 360;
			
			if(distance > 180)
				distance = 180 - (distance - 180);
				
			var panVolume = (1- distance/180) * (sound.data.ambience * sound.data.ambience);

			//Turn off 3D sound when ambiance is 1
			if(sound.data.ambience == 1)
				panVolume = 1;
			
			// Final compiled volume.
			sound.volume.gain.value = (volume * soundVol * panVolume);
		}
	}