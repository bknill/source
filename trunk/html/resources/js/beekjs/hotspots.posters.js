	function createPoster(data){
		
		console.log('poster()');
		
		var poster = new THREE.Object3D();
			poster.hotspotData = data;
			poster.position.copy( panTiltToVector( data.pan, data.tilt, (data.distance / 2000) * 1024) );
			poster.scale.normalize().multiplyScalar(0.1);
			poster.lookAt( camera.position ); 
			poster.rotation.x = -data.rotationX;
			if(data.rotationY != null)
				poster.rotation.y = data.rotationY > 0 ? data.rotationY : Math.PI - data.rotationY;
			poster.rotation.z = data.rotationZ;
			
			//for those pesky ones with no values
			if(poster.rotation.x == poster.rotation.y == poster.rotation.z == 0)
				poster.lookAt( camera.position );
			
			var posterText = posterSprite ( data.cleanText, $(data.text).find( "FONT" ).attr('size'), $(data.text).find( "FONT" ).attr('color'), data.frame );
		
			if(data.showIcon){
				posterText.visible = false;
				icon = createIcon();
				poster.add( icon );
			}
			
			poster.add( posterText );
			scene.add( poster );
			 
	    	targetList.push( poster );
	}
	
	
	
	
	function posterSprite( text, size, color, background ) {
	    
		var font = "Lato";
	    padding = 30;
	    size = parseInt(size) * 2;
	    font = size + "px " + font;

	    width = 1000;
	    height = 500;
	    var canvas = createHiDPICanvas(width, height);
	    var context = canvas.getContext('2d');
	    var maxWidth = width - 100;
	    var lineHeight = size;
	    var x = (width - maxWidth) / 2;
	    var y = 60;
	    
	   context.font = font;
	   context.fillStyle = color;

	   var height = wrapText(context,  text,  x, y, maxWidth, lineHeight);
	   
	  if(background != 0){
		  if(color == '#ffffff')
			   context.fillStyle = '#000';
		  else
		   context.fillStyle = '#fff';
		   context.fillRect(0, 0, width, height + lineHeight * 2);
		   context.fillStyle = color;
		   wrapText(context,  text,  x, y, maxWidth, lineHeight);
	  };
	  
	  	canvas.style.height = height;

	    var texture = new THREE.Texture(canvas);
	    texture.needsUpdate = true;
	    
	    var mesh = new THREE.Mesh(
	    new THREE.BoxGeometry(canvas.width, canvas.height,1),
		    new THREE.MeshPhongMaterial({
		    	map: texture,
		    	transparent: true
		    })
	    );	
		
	    return mesh;
	}
	
	
	
	  function wrapText(context, text, x, y, maxWidth, lineHeight) {
		    
		   var words = text.split(' ');
	        var line = '';
	
	        for(var n = 0; n < words.length; n++) {
	          var testLine = line + words[n] + ' ';
	          var metrics = context.measureText(testLine);
	          var testWidth = metrics.width;
	          if (testWidth > maxWidth && n > 0) {
	            context.fillText(line, x, y);
	            line = words[n] + ' ';
	            y += lineHeight;
	          }
	          else {
	            line = testLine;
	          }
	          
	                  context.fillText(line, x, y);
	        }

	        	return y;
	      }
	  
	  
	  var PIXEL_RATIO = (function () {
		    var ctx = document.createElement("canvas").getContext("2d"),
		        dpr = window.devicePixelRatio || 1,
		        bsr = ctx.webkitBackingStorePixelRatio ||
		              ctx.mozBackingStorePixelRatio ||
		              ctx.msBackingStorePixelRatio ||
		              ctx.oBackingStorePixelRatio ||
		              ctx.backingStorePixelRatio || 1;

		    return dpr / bsr;
		})();

	  
	  
		createHiDPICanvas = function(w, h, ratio) {
		    if (!ratio) { ratio = PIXEL_RATIO; }
		    var can = document.createElement("canvas");
		    can.width = w * ratio;
		    can.height = h * ratio;
		    can.style.width = w + "px";
		    can.style.height = h + "px";
		    can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
		    return can;
		};
		
		
		
		function createIcon() {
			var canvas = createHiDPICanvas(400, 400);
		    var context = canvas.getContext('2d');
		    context.font = "250px helvetica neue";
			context.fillStyle = '#fff';
		    context.fillText("i", 170, 280);
		    
		      context.beginPath();
		      context.arc(200, 200, 150, 0, 2 * Math.PI, false);
		      context.lineWidth = 30;
		      context.strokeStyle = '#fff';
		      context.stroke();
		    
		    var texture = new THREE.Texture(canvas);
		    texture.needsUpdate = true;
		    
		    var mesh = new THREE.Mesh(
		    new THREE.BoxGeometry(canvas.width, canvas.height,1),
			    new THREE.MeshLambertMaterial({
			    	map: texture,
			    	transparent: true
			    })
		    );	
			
		    return mesh;
			
		};