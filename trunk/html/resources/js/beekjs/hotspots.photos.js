	function createPhoto(data){
		
		console.log('photo()');
	   
		var photo = new THREE.Object3D();
			photo.hotspotData = data;
			photo.position.copy( panTiltToVector( data.pan, data.tilt, (data.distance / 2000) * 1024) );
			
			photo.lookAt( camera.position ); 
			photo.rotation.x = -data.rotationX;
			if(data.rotationY != null)
				photo.rotation.y = data.rotationY > 0 ? data.rotationY : Math.PI - data.rotationY;
			photo.rotation.z = data.rotationZ;
			
			//for those pesky ones with no values
			if(photo.rotation.x == 0 && photo.rotation.y == 0 && photo.rotation.z == 0)
				photo.lookAt( camera.position );
			

	    var image = new Image();
		    image.crossOrigin = '';
		    image.onload = function() {
		    	
		    	 var texture = new THREE.Texture(texture_placeholder);
		    	 var photoMaterial = new THREE.MeshLambertMaterial({
		    		 map: texture,
		 	        transparent: true
		 	    });
		    	
		        texture.image = this;
		        texture.needsUpdate = true;
		        photoMaterial.side = THREE.DoubleSide;

				var w = powerOf2Down(this.width);
				var scale = w / this.width;
				var scaledHeight = this.height * scale;
				var h = powerOf2Up(scaledHeight);
				
				var scaleFactor  =  ( this.width / w) * ( this.height / h ) * 0.1;
				photo.scale.normalize().multiplyScalar(scaleFactor);
				
				var photoGeom = new THREE.PlaneGeometry( w  , h  );

		    	var photoMesh = new THREE.Mesh( photoGeom, photoMaterial );
		    	
		    	photo.castShadow = true;
				photo.add( photoMesh );
		        scene.add( photo );
		        
		    };
		    
		    image.src = 'http://cdn.beek.co/' + data.file.realName;



    	targetList.push( photo );
			
	}