var camera, scene, renderer,box, controls, composer;
var materials = [];
var texture_placeholder,
    isUserInteracting = false,
    onMouseDownMouseX = 0,
    onMouseDownMouseY = 0,
    lon = 90,
    onMouseDownLon = 0,
    lat = 0,
    onMouseDownLat = 0,
    phi = 0,
    theta = 0,
    target,
	previewTileCounter,
	isMobile,
	_touchZoomDistanceEnd,
	_touchZoomDistanceStart = 0,
	_touchZoomDistanceEnd = 0,
	
	STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_ZOOM_PAN: 4 };
	//_panStart = new THREE.Vector2(),
	//_panEnd = new THREE.Vector2(),
	_state = STATE.NONE,
	_prevState = STATE.NONE;



var targetList = [];
var projector, mouse = { x: 0, y: 0 };

var jsonpPrefix = 'https://gms.beek.co';
var cdnPrefix = 'http://cdn.beek.co';

var FACES = "rludfb";
var TILES = ['0_0', '0_1', '1_0', '1_1'];

var tiles, bubbles;

 

	function init() {
		console.log('initBeek()');
	    var container;
	
	    container = document.getElementById('container');
	
	    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1024);
	    
	    scene = new THREE.Scene();
	    
	    target = new THREE.Vector3();

		projector = new THREE.Projector();
		
		var mobile = window.matchMedia("only screen and (max-width: 760px)");
		if(mobile.matches)
			isMobile = true;

	if ( ! Detector.webgl ) 
			renderer = new THREE.CanvasRenderer();
		else
			renderer = new THREE.WebGLRenderer({antialias: true});
		
	    renderer.setSize(window.innerWidth, window.innerHeight);
	    renderer.shadowMapEnabled = true;

        renderer.render(scene, camera);
	    container.appendChild(renderer.domElement);
	    
	    document.getElementById("container").addEventListener('mousedown', onDocumentMouseDown, false);
	    document.getElementById("container").addEventListener('mousemove', onDocumentMouseMove, false);
	    document.getElementById("container").addEventListener('mouseup', onDocumentMouseUp, false);
	    document.getElementById("container").addEventListener('mousewheel', onDocumentMouseWheel, false);
	
	    document.getElementById("container").addEventListener('touchstart', onDocumentTouchStart, false);
	    document.getElementById("container").addEventListener('touchmove', onDocumentTouchMove, false);
	    document.getElementById("container").addEventListener( 'touchend', onDocumentTouchEnd, false );
	
	    window.addEventListener('resize', onWindowResize, false);
	    
	    initBeek();
	    

	}


	function initBeek() {
	console.log('initBeek()');

	    $.getJSON('https://gms.beek.co/guide/' + guideId + '/jsonp?callback=?', $.proxy(function(data) {
	
	        if (sceneId) this.beekScene = {
	            sceneId: data.firstScene ||
	                data.guideSections[0].firstScene ||
	                data.guideSections[0].guideScenes[0].sceneId
	        };
	        
	        createGuide(data);
	        
	        if(!currentScene)
	        	getScene(sceneId);
			
		    animate();

		    //controls are ropey. Needs refining. 
/*		    if(isMobile){
			    controls = new THREE.DeviceOrientationControls( camera );
			    controls.connect();
		    }*/


	    }, this));
	    
	    //changes hash for an s
		$.address.state('s');
	    
	    //handles back function
	    $.address.change(function(event) {  
	        var id = parseInt( event.value.slice(1) );
	        if(currentScene)
		        if(currentScene.id != id)
		        	getScene(id);
	    });  
	};

	function beekParams(param) {
	    var ret = {};
	    if (guide) ret.guide = guide.toString();
	    if (scene) ret.scene = scene.toString();
	    return ret;
	}
	
    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    }  

    function detectHotspotClick(){
    	
    	var vector = new THREE.Vector3( mouse.x, mouse.y, 1 );
    	
    	projector.unprojectVector( vector, camera );
    	
    	var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    	var intersects = ray.intersectObjects( targetList, true );

    	if ( intersects.length > 0){
    		console.log( intersects[ 0 ].point );
    		hotspotClick(intersects[ 0 ].object);
    	}
    	
    }
    
    function onDocumentMouseDown(event) {

        event.preventDefault();

        isUserInteracting = true;
        
        if(selectedHotspot && !viewingHotspot){
        	TWEEN.removeAll(); 
        	selectedHotspot = null;
        }

        onPointerDownPointerX = event.clientX;
        onPointerDownPointerY = event.clientY;

        onPointerDownLon = lon;
        onPointerDownLat = lat;
        
    	// update the mouse variable
    	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    	
    	detectHotspotClick();

    }

    function onDocumentMouseMove(event) {

        if (isUserInteracting === true) {
        	
        	
            lon = (onPointerDownPointerX - event.clientX) * 0.2 + onPointerDownLon;
            lat = (event.clientY - onPointerDownPointerY) * 0.2 + onPointerDownLat;

        }

    }

    function onDocumentMouseUp(event) {

        isUserInteracting = false;

    }

    function onDocumentMouseWheel(event) {

    	setZoom(camera.fov - event.wheelDeltaY * 0.05);

    }

    
    function setZoom(fov){
    	
        camera.fov = fov;
        
        if(camera.fov < 30) camera.fov = 30;
        if(camera.fov > 100) camera.fov = 100;
        
        camera.updateProjectionMatrix();

    }

    function onDocumentTouchStart(event) {

              event.preventDefault();
    	
        if (event.touches.length == 1) {

            onPointerDownPointerX = event.touches[0].pageX;
            onPointerDownPointerY = event.touches[0].pageY;

            onPointerDownLon = lon;
            onPointerDownLat = lat;
            
            mouse.x = +(event.targetTouches[0].pageX / window.innerwidth) * 2 +-1;
            mouse.y = -(event.targetTouches[0].pageY / window.innerHeight) * 2 + 1; 
            
        	detectHotspotClick();

        }
        
        if (event.touches.length == 2) {
        	
        	_state = STATE.TOUCH_ZOOM_PAN;
	        var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
	        var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
	        _touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt( dx * dx + dy * dy );
	
        }

    }

    function onDocumentTouchMove(event) {

        if (event.touches.length == 1) {

            event.preventDefault();

            lon = (onPointerDownPointerX - event.touches[0].pageX) * 0.4 + onPointerDownLon;
            lat = (event.touches[0].pageY - onPointerDownPointerY) * 0.4 + onPointerDownLat;

        }
        
        if (event.touches.length == 2) {
        	
        	   var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
               var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
               _touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );
 
  			var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
	    	setZoom(camera.fov * factor);
			
        }

    }
    
    function onDocumentTouchEnd( event ) {

			_touchZoomDistanceStart = _touchZoomDistanceEnd = 0;

	}
    
    zoomCamera = function () {

		if ( _state === STATE.TOUCH_ZOOM_PAN ) {

			var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
			_eye.multiplyScalar( factor );

		} else {

			var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

			if ( factor !== 1.0 && factor > 0.0 ) {

				_eye.multiplyScalar( factor );

				if ( _this.staticMoving ) {

					_zoomStart.copy( _zoomEnd );

				} else {

					_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

				}

			}

		}

	};


    function animate() {

        requestAnimationFrame(animate);
        update();

    }

    function update() {
    	  
        TWEEN.update();
        
        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        target.x = 512 * Math.sin(phi) * Math.cos(theta);
        target.y = 512 * Math.cos(phi);
        target.z = 512 * Math.sin(phi) * Math.sin(theta);
        

        	camera.lookAt(target);
        
/*        if(controls)
        	controls.update();*/
        
        if(sceneSounds.length > 0)
        	updateSceneSounds( lon );

        renderer.render(scene, camera);
    }
    
    
    function latLonToVector(lat,lon){
        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        var vector = new THREE.Vector3();
        vector.x = 512 * Math.sin(phi) * Math.cos(theta);
        vector.y = 512 * Math.cos(phi);
        vector.z = 512 * Math.sin(phi) * Math.sin(theta);
    	
    	return vector;
    }

    function vectorToLatLon(position){
    	
    	// another solution posted on SO if this one is unreliable
    	//var lat = Math.atan2( Math.sqrt( target.x*target.x + target.z*target.z) , target.y) ;
    	//var lon = Math.atan2( target.x, target.y);
    	
    	return {lat : Math.acos(position.y / 512), lon  : Math.atan(position.x / position.z) };
    }
    
    function panTiltToVector(pan,tilt,distance){
    	
        distance = distance/4;
        var pr = THREE.Math.degToRad( pan + 90);
        var tr = THREE.Math.degToRad( tilt );
        
        var vector = new THREE.Vector3(distance * Math.cos(pr) * Math.cos(tr),distance * Math.sin(tr),distance * Math.sin(pr) * Math.cos(tr));
    	
        return vector;
    }
    
    function hotspotPanTiltToLatLon(pan,tilt){

    	pan += 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : tilt, lon : pan };
    }
    
    function panTiltToLatLon(pan,tilt){

    	pan -= 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : -tilt, lon : pan };
    }
    

    var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
    
    
    function filters(){
    	
    	return;
    	
	    composer = new THREE.EffectComposer( renderer);
    	
    	filmPass = new THREE.ShaderPass( THREE.FilmShader );
		filmPass.uniforms[ "sCount" ].value = 800;
		filmPass.uniforms[ "sIntensity" ].value = 0.9;
		filmPass.uniforms[ "nIntensity" ].value = 0.4;
		
		composer.addPass( filmPass );
    	
    }	
    

	function launchFullscreen(element) {
		  if(element.requestFullscreen) {
		    element.requestFullscreen();
		  } else if(element.mozRequestFullScreen) {
		    element.mozRequestFullScreen();
		  } else if(element.webkitRequestFullscreen) {
		    element.webkitRequestFullscreen();
		  } else if(element.msRequestFullscreen) {
		    element.msRequestFullscreen();
		  }
		}
	
	
	function exitFullscreen() {
		  if(document.exitFullscreen) {
		    document.exitFullscreen();
		  } else if(document.mozCancelFullScreen) {
		    document.mozCancelFullScreen();
		  } else if(document.webkitExitFullscreen) {
		    document.webkitExitFullscreen();
		  }
		}
	

