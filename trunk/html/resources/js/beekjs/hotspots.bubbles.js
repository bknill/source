	function bubble(bubble){

		console.log('bubble');
		console.log(sceneInGuide( bubble.targetSceneId ));
		
		if(!sceneInGuide( bubble.targetSceneId ) )
			return;
		
		console.log('created');
		
        var hsText = textSprite ( bubble.titleMerged, 40 , "#000" );
	    var hsGeom = new THREE.BoxGeometry( hsText.geometry.parameters.width*1.2, hsText.geometry.parameters.height*1.1, 0.1);
		//var mirrorCubeCamera = new THREE.CubeCamera( 1, 1024 , 100 );
	    var hsMaterial = new THREE.MeshBasicMaterial( {
				reflectivity: 0.1,
				color: 0xF5F5F5//, 
				//envMap: mirrorCubeCamera.renderTarget
	    	});
		var hsCube = new THREE.Mesh( hsGeom, hsMaterial );
		
		var triangleGeom = new THREE.Geometry();
		var v1 = new THREE.Vector3(0,0,0);var v2 = new THREE.Vector3(30,0,0);var v3 = new THREE.Vector3(30,30,0);
		triangleGeom.vertices.push( v1 );triangleGeom.vertices.push( v2 );triangleGeom.vertices.push( v3 );

		triangleGeom.faces.push( new THREE.Face3( 0, 1, 2 ) );
		
		var triangle = new THREE.Mesh( triangleGeom, hsMaterial );
		triangle.rotation.z = -0.75;
		triangle.position.y = -hsText.geometry.parameters.height*0.4;
		triangle.position.x -= 15;
		
		hsBubble = new THREE.Object3D();
		hsBubble.position.copy( panTiltToVector( bubble.pan, bubble.tilt, bubble.distance ) );	
		hsBubble.lookAt( camera.position );
		hsBubble.scale.normalize().multiplyScalar(0.3);
		hsBubble.hotspotData = bubble;
		hsBubble.rotateY = Math.PI;
		
		//mirrorCubeCamera.updateCubeMap( renderer, scene );

		hsBubble.add( hsCube );	
		//hsBubble.add( mirrorCubeCamera );
		hsBubble.add( hsText );
		hsBubble.add( triangle );
		scene.add(hsBubble);
    	
    	targetList.push( hsBubble );
		
	}