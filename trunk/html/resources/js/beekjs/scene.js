	var sceneReady = false,
		firstRun = true,
		currentScene,
		boards,
		bubbles,
		historyScenes = [],
		hemiLight;	
	
	function getScene(sceneId) {
		console.log('getScene()');
	    var jsonpURL = 'https://gms.beek.co/scene/' + sceneId + '/jsonp?callback=?';
	    $.getJSON(jsonpURL, $.proxy(startScene, this));
	    
	    if(hemiLight)
			setLights(0);	
	};
	
	
	function startScene(sceneObject) {
		console.log('startScene()');
		
		if(isUserInteracting)
			return;
		
		currentScene = sceneObject;
		
		historyScenes.push( sceneObject );

		$.address.state(sceneObject.id);  
		
		//get tiles
	    tiles = [];
	    materials = [];
	    previewTileCounter = 0;
	    for (var face in FACES) {
	        materials.push(loadTexture(cdnPrefix + '/scene_' + sceneObject.id + "_pano_" +
	        		sceneObject.panoIncrement + "_" + FACES[face] + "/9/0_0.jpg" ));
	        for (var tile in TILES) tiles.push(
	        		cdnPrefix + '/scene_' + sceneObject.id + "_pano_" + sceneObject.panoIncrement + "_" + FACES[face] + "/10/" + TILES[tile] + ".jpg"
	        );
	        
        }

	    processHotspots(sceneObject);
		updateGuideScene(sceneObject.id);   
		addLights();
	} 
	
	function processHotspots(sceneObject){
		//get bubbles
        bubbles = [];
        for (ix in sceneObject.bubbles) 
        	bubbles.push(sceneObject.bubbles[ix]);

        //get boards
        boards = [];
        for (ix in sceneObject.boards) 
            boards.push(sceneObject.boards[ix]);
        
        //get photos
        photos = [];
        for (i in sceneObject.photos) 
            photos.push(sceneObject.photos[i]);
        
        //get posters
        posters = [];
        for (i in sceneObject.posters) 
            posters.push(sceneObject.posters[i]);
        
        //get sounds
        sounds = [];
        for (i in sceneObject.sounds) 
            sounds.push(sceneObject.sounds[i]);

	}

	
	function clearScene() {
		console.log('scene.clearScene()');

		//remove everything from the scene and reset variables
		setLights(0);
		selectedHotspot = null;
		viewingHotspot = null;
		targetList = [];
		
		for(i in sceneSounds)
			sceneSounds[i].source.stop();
		
		sceneSounds = [];
		
		var obj, i;
		for ( i = scene.children.length - 1; i >= 0 ; i -- ) {
		    obj = scene.children[ i ];
		    if ( obj !== projector && obj !== camera) {
		        scene.remove(obj);
		    }
		}
		
		if(closeButton)
			removeCloseButton();
	};
	
	function createBox(){
		
		//the inital pano cube
	    box = new THREE.Mesh(new THREE.BoxGeometry(1024,1024,1024, 20, 20, 10,10), new THREE.MeshFaceMaterial(materials));
	    box.scale.x = -1;
	    scene.add(box);
	}

	function loadTexture(path) {
	
	    var texture = new THREE.Texture(texture_placeholder);
	    var material = new THREE.MeshBasicMaterial({
	        map: texture,
	        overdraw: 0.5
	    });
	
	    var image = new Image();
	    image.crossOrigin = '';
	    
	    image.onload = function() {
	
	        texture.image = this;
	        texture.needsUpdate = true;
	        previewTileCounter ++;
	        
	        if(previewTileCounter == 6){
	        	sceneReady = true;
	        	if(!selectedHotspot)
	        		loadScene();
	        }

	
	    };
	    image.src = path;
	
	    return material;
	
	}
	
	function setDefaults(){
		console.log('setting defaults');
		lon = currentScene.pan+90;
		lat = currentScene.tilt;
		camera.fov = currentScene.fov * 0.8;
		camera.updateProjectionMatrix();
		
    	sceneReady = false;
	}
	
	function loadScene(){
		
		console.log('scene.loadScene()');
		selectedHotspot = null;

    	TWEEN.removeAll();
    	
		clearScene();
	    createBox(); 
	    hotspots();
    	setDefaults();
    	addLights();

        
		if ( Detector.webgl ) 
			loadTiles(tiles);


	}
	
   function loadTiles(tiles){
	   
	   var face_b = new THREE.Object3D();
	   	   face_b.position.z = -511;
	   	   face_b.receiveShadow = true;
	   	   scene.add(face_b);
	   var face_f = new THREE.Object3D();
   	   	   face_f.position.z = 511;
   	   	   face_f.rotation.y = 180;
	   	   face_f.receiveShadow = true;
	   	   face_f.lookAt( camera.position );
   	   	   scene.add(face_f);
	   var face_l = new THREE.Object3D();
	   	   face_l.position.x = 511;
	  	   face_l.rotation.y = -90;
	   	   face_l.receiveShadow = true;
	   	   face_l.lookAt( camera.position );
	   	   scene.add(face_l);
	   var face_r = new THREE.Object3D();
  	   	   face_r.position.x = -511;
	  	   face_r.rotation.y = 90;
	   	   face_r.receiveShadow = true;
	   	   face_r.lookAt( camera.position );
	  	   scene.add(face_r);
		var face_u = new THREE.Object3D();
			face_u.position.y = 511;
	   	   	face_u.lookAt( camera.position );
	   	   face_u.receiveShadow = true;
	   	   face_u.rotation.z = THREE.Math.degToRad(180);
			scene.add(face_u);
		var face_d = new THREE.Object3D();
			face_d.position.y = -511;
	   	   	face_d.lookAt( camera.position );
			face_d.rotation.z = THREE.Math.degToRad(180);
	   	    face_d.receiveShadow = true;
			scene.add(face_d);
	   
	   for(var tile in tiles){
		   
		   var image = new Image();
		   image.crossOrigin = '';
	       image.src = tiles[tile];
	       image.group = image.src.charAt(image.src.length - 12);
	       image.vertical = image.src.charAt(image.src.length - 7);
	       image.horizontal = image.src.charAt(image.src.length - 5);
 
	       var counter = 0;
	       
		   image.onload = function() {
			   
			   	var texture = new THREE.Texture(texture_placeholder);
	            texture.image = this;
	            texture.needsUpdate = true;
		        var material = new THREE.MeshLambertMaterial({
		            map: texture
		        });
			   
		        material.side = THREE.DoubleSide;
		        material.receiveShadow = true;
		        
	            var geometry = new THREE.PlaneGeometry(512,512);
	                geometry.receiveShadow = true;
			    var planeMesh = new THREE.Mesh( geometry, material );
			    	planeMesh.receiveShadow = true;
			    	
			    if(this.horizontal == 0)
			    	planeMesh.position.y += 256;
			     if(this.horizontal == 1)
			    	planeMesh.position.y -= 256;
			     if(this.vertical == 0)
			    	planeMesh.position.x -= 256;
			     if(this.vertical == 1)
			    	planeMesh.position.x += 256;
			    
			    
			    if(this.group == 'b'){				    
				    face_b.add(planeMesh);
			    }else if(this.group == 'f'){
			    	face_f.add(planeMesh);	
			    }else if(this.group == 'l'){
			    	face_l.add(planeMesh);	
			    }else if(this.group == 'r'){
			    	face_r.add(planeMesh);	
			    }else if(this.group == 'u'){
			    	face_u.add(planeMesh);	
			    }else if(this.group == 'd'){
			    	face_d.add(planeMesh);	
			    }
			    
			    counter ++;
			    
			    if(counter = 16 && !guideOpen)
			    		setLights(0.8);			  
		   };
	   };
	   
	   
	   	if(firstRun == true){
	        $("#preloader").remove();
	        //give it a moment to organise tiles before guide load
			openGuideButton();
			firstRun = false;
		}
   }
   
   
   function panoLook( pan, tilt, zoom ){
	
			var newLatLon = {lat: lat, lon : lon};
			var targetLatLon = panTiltToLatLon( Number(pan), Number(tilt) );
			var newFov = {nFov : camera.fov};
			var targetFov = {nFov : zoom * 0.8};
			var distance = Math.abs(targetLatLon.lon - newLatLon.lon)*10;
			

			
			if(newLatLon.lon < 0)
				targetLatLon.lon -= 360;
			if(newLatLon.lon > 360)
				targetLatLon.lon += 360;

			//set up zoom tween
			var tweenZoom = new TWEEN.Tween( newFov ).to(targetFov, 2000).easing(TWEEN.Easing.Sinusoidal.InOut);
			tweenZoom.onUpdate(function(){
				camera.fov = newFov.nFov;
				camera.updateProjectionMatrix();

				});

			//tween lon/lat to tween camera
			var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, distance).easing(TWEEN.Easing.Sinusoidal.Out);
			tweenLon.onUpdate(function(){
				lon = newLatLon.lon;
				lat = newLatLon.lat;

				if(lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5) 
					tweenZoom.start();
					//tweenLon.stop();
				});
			
			tweenLon.start();

		
	   
   }
   
   function hotspotLook( id ){
	   
	   for ( var i = 0; i < scene.children.length; i ++ ) {
		    var object = scene.children[ i ];
		    if ( object.hotspotData)
		    	if ( object.hotspotData.id == id) 
		    		tweenToSelectedHotspot(object);
	
		}
	   
   }
   
   function addLights(){
	   
	   
	   console.log('add lights');

	   flashlight = new THREE.SpotLight( 0xffffff, 0.7, 300, Math.PI/2,1 );
	   camera.add(flashlight);
	   flashlight.castShadow = true;
	   scene.add( camera );
	   flashlight.position.set(0,0,10);
	 //  flashlight.shadowCameraVisible = true;
	   flashlight.target = camera;

	
		hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.06); 
		scene.add( hemiLight);
		
		var ambiLight = new THREE.AmbientLight( 0x404040 ); // soft white light
		scene.add( ambiLight );
	   
   }
   
   function setLights(value){
		var tween = new TWEEN.Tween( hemiLight ).to( {intensity: value}, 1000 ).start();
	
   }