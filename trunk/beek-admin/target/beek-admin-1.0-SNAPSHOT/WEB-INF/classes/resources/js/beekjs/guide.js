var guideSections = [],
	childSections = [],
	topLevelSections = [],
	guideSection,
	guideSectionOrder = [],
	guideScenes = [],
	guideSceneOrder = [],
	guideScene,
	guideData,
	minimalSpace = false,
	$dragging,
	guideOpen,
	infoOpen,
	guideOrigin,
	sectionX,
	processedGuideSceneText,
	headerWidth;

	//this organises all the guideData 
	function createGuide( data ){


        guideData = data;

		//add sections
		for(var section in guideData.guideSections){
			guideSections.push(guideData.guideSections[section]);
			//add scenes
			for(var scene in guideData.guideSections[section].guideScenes)
				guideScenes.push(guideData.guideSections[section].guideScenes[scene]);


			guideData.guideSections[section].guideScenes.sort(dynamicSort("order"));
		};

		guideSections.sort(dynamicSort("order"));
		
		//find child sections and check for map
		for(var cs in guideSections) {

			if (guideSections[cs].parent_section_id) {

				for (var s in guideSections)
					if (guideSections[s].id == guideSections[cs].parent_section_id) {
						if (!guideSections[s].childSections)
							guideSections[s].childSections = [];

						guideSections[s].childSections.push(guideSections[cs]);
						guideSections[cs].parent = guideSections[s];
					}
			}
			else
				topLevelSections.push(guideSections[cs]);

			if(guideSections[cs].title.toLowerCase().indexOf('map') > -1){
				mapSections.push(guideSections[cs]);
				loadMaps();
			}

		}
		
		//set child sections and scenes order
		for(var s in guideSections){
			if(guideSections[s].childSections)
				guideSections[s].childSections.sort(dynamicSort("order"));
			if(guideSections[s].guideScenes)
				guideSections[s].guideScenes.sort(dynamicSort("order"));

		}

		
		//create the orders for the next button
		for(var s in guideSections){
		 if(!guideSections[s].parent_section_id){

		 guideSectionOrder.push(guideSections[s]);
		 for (var sc in guideSections[s].guideScenes)
		 guideSceneOrder.push(guideSections[s].guideScenes[sc]);

		 for (var cs in guideSections[s].childSections){
		 guideSectionOrder.push(guideSections[s].childSections[cs]);
		 for (var sc in guideSections[s].childSections[cs].guideScenes)
		 guideSceneOrder.push(guideSections[s].childSections[cs].guideScenes[sc]);

		 for (var gcs in guideSections[s].childSections[cs].childSections){
		 guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs]);
		 for (var sc in guideSections[s].childSections[cs].childSections[gcs].guideScenes)
		 guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].guideScenes[sc]);

		 for (var ggcs in guideSections[s].childSections[cs].childSections[gcs].childSections){
		 guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs]);
		 for (var sc in guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes)
		 guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes[sc]);

		 }
		 }

		 }
		 }
		 }

		if(!sceneId)
		 	getScene(guideSections[0].guideScenes[0].sceneId);



		$( "body" ).append('<div id="guide" class="panel" role="navigation" ><div id="guideTitle" class="sectionTitle"><h1>' +
			data.title +'</h1></div><div id ="menu" class="slinky-menu"><ul class="topSectionList"></ul></div></div>');

		$( "body" ).append('<div id = "sceneContainer" class="sceneContainer" ><div id="sceneText" class="sceneText"/> </div>');
		$('#sceneContainer').hide();

		for(var s in guideSections)
			if(!guideSections[s].parent_section_id){
				$( "#menu .topSectionList" ).append('<li class="section" id="li_'+guideSections[s].id+'"><a href="#">' + guideSections[s].title + '</a></li>');
				$( "#li_" + guideSections[s].id ).append('<ul id=ul_' + guideSections[s].id +'></ul>');

				if(guideSections[s].guideScenes){
					console.log(guideSections[s].title,guideSections[s].guideScenes.length);
					for (var csc in guideSections[s].guideScenes){
						console.log(guideSections[s].guideScenes[csc].title);
					$( "#ul_" + guideSections[s].id ).append('<li class="scene" id="li_'+guideSections[s].guideScenes[csc].scene.id+'"><a href="#">' + guideSections[s].guideScenes[csc].title + '</a></li>');
					}
				}

				if(guideSections[s].childSections)
					for (var se in guideSections[s].childSections){

						var section = guideSections[s].childSections[se];

						$( "#ul_" + guideSections[s].id ).append('<li class="section" id="li_'+section.id+'"><a href="#">' + section.title + '</a></li>');
						$( "#li_" + section.id ).append('<ul id=ul_' + section.id +'></ul>')


						if(section.guideScenes)
							for (var sc in section.guideScenes)
								$( "#ul_" + section.id ).append('<li class="scene" id="li_'+section.guideScenes[sc].scene.id+'"><a href="#">' + section.guideScenes[sc].title + '</a></li>')


						if(section.childSections)
							for (var cse in section.childSections){

								var childsection = section.childSections[cse];

								$( "#ul_" + section.id ).append('<li class="section" id="li_'+childsection.id+'"><a href="#">' + childsection.title + '</a></li>');
								$( "#li_" + childsection.id ).append('<ul id=ul_' + childsection.id +'></ul>')


								if(childsection.guideScenes)
									for (var sc in childsection.guideScenes)
										$( "#ul_" + childsection.id ).append('<li class="scene" id="li_'+childsection.guideScenes[sc].scene.id+'"><a href="#">' + childsection.guideScenes[sc].title + '</a></li>')


								if(childsection.childSections)
									for (var gcse in childsection.childSections){

										var grandChildSection = childsection.childSections[gcse];

										$( "#ul_" + childsection.id ).append('<li class="section" id="li_'+grandChildSection.id+'"><a href="#">' + grandChildSection.title + '</a></li>');
										$( "#li_" + grandChildSection.id ).append('<ul id=ul_' + grandChildSection.id +'></ul>');



										if(grandChildSection.guideScenes)
											for (var sc in grandChildSection.guideScenes)
												$( "#ul_" + grandChildSection.id ).append('<li class="scene" id="li_'+grandChildSection.guideScenes[sc].scene.id+'"><a href="#">' + grandChildSection.guideScenes[sc].title + '</a></li>')


									}
							}
					}
			}


		$('#menu').slinky();
		updateScrollers("guide");

		if(mapsInitiated)
			$(mapSections).each(function(index, section){
				$('#ul_' + section.id).remove();
				$('#li_' + section.id + ' a').removeClass('next');
				$('#li_' + section.id + ' a').bind('click', function() {

					if(!mapShowing)
					if (section.parent_section_id == null)
						createMap(topLevelSections)
					else
						createMap(getGuideSection(section.parent_section_id).childSections)


				});

			});



		$('.section').bind('click',function(e){
			e.stopPropagation();

			var id = this.id.substring(3, this.id.length);

			//set the guide title
			if(guideSection.id != id){
				guideSection = getGuideSection(id);
				$('#guideTitle h1').text(guideSection.title);
			}
			else if(guideSection.parent_section_id == null) {
				$('#guideTitle h1').text(guideData.title);
			}
			else{
				guideSection = getGuideSection( guideSection.parent_section_id );
				$('#guideTitle h1').text( guideSection.title );
			}

			setTimeout(function(){
				updateScrollers("guide");
			},300);
		})


		$('.scene').bind('click',function(e){
			e.stopPropagation();
			var id = this.id.substring(3, this.id.length);
			getScene(id);
		});


		//check to see if video player is required
		for(var sc in guideScenes)
			if(guideScenes[sc].transitionVideo)
				loadVideoPlayer();
		
		if(guideScenes[0].transitionVideo)
			firstSceneVideo = true;
		
		for(var sc in guideScenes)
			if(guideScenes[sc].posters.length > 0)
				loadHtml2Canvas();
		
		if(guideData.gameTasks.length)
			game(guideData.gameTasks);
		
		   //set default section 
		   guideSection = guideSections[0];

	}
	

	
	//create scene Text and Buttons
	function openSceneInfo(){
		console.log('createSceneInfo()');
		
		if(infoOpen || gameInProgress || videoPlaying || connected)
			return;


		updateGuideSceneTextButtons();
		
		$('#sceneContainer').css({'left': -$('#sceneContainer').width() , 'width' : !isMobile ? ($(window).innerWidth() * 0.4) : ($(window).innerWidth() * 0.6)});	
		$('#openSceneInfo').find('i').toggleClass('fa-plus fa-times');
		
		updateH1Margin();
		$('#sceneContainer').animate({'left' : 0 },"slow");
	    infoOpen = true;
	    
	    //hide prev button if mobile
	    if(isMobile && $('#prevButton').length)
	    	$('#prevButton').toggle();
	    
    	setLights(0.6);
    	
		$('#sceneContainer').append(
				'<div id="sceneButtons" >'
				+'<div id="booking"  class="booking sceneButton"><i class="fa fa-dollar"></i></div>'
				+'<div id="telephone" class="telephone sceneButton"><i class="fa fa-phone"></i></div>'
				+'<div id="email"  class="email sceneButton"><i class="fa fa-envelope"></i></div>'
				+'<div id="website"  class="website sceneButton"><i class="fa fa-laptop"></i></div>'
				+'<div id="facebook"  class="sceneButton"><i class="fa fa-facebook"></i></div>'
				+'<div id="facebookCount"  class="sceneButton sceneButtonCount"><span/></div>'
				+'<div id="twitter"  class="sceneButton"><i class="fa fa-twitter"></i><span/></div>'
				+'<div id="twitterCount"  class="sceneButton sceneButtonCount"><span/></div></div>'
				+'</div>'		
		);

		if(!guideScene.scene.phone) $('.telephone').hide();
		if(!guideScene.scene.email)	$('.email').hide();
		if(!guideScene.scene.infoURL) $('.website').hide();
		if(!guideScene.scene.bookingURL) $('.booking').hide();
	
		$('#booking').bind('click',function(){sceneButtonOpen(guideScene.scene.bookingURL);});	
		$('#telephone').bind('click',function(){sceneButtonOpen('tel:'+ guideScene.scene.phone);});	
		$('#email').bind('click',function(){sceneButtonOpen('mailto:'+ guideScene.scene.email);});	
		$('#website').bind('click',function(){sceneButtonOpen( guideScene.scene.infoURL );});
		$('#facebook').bind('click',function(){ sceneButtonOpen( 'http://www.facebook.com/sharer/sharer.php?u=http://beek.co/'+ window.location.pathname );});
		$('#twitter').bind('click',function(){ sceneButtonOpen(  'http://www.twitter.com/share?url=http://beek.co/'+ window.location.pathname );});
	}



	function sceneButtonOpen( object ){
		console.log('sceneButtonOpen(' + object + ')');
		window.open(object, '_blank');
		trackEvent( 'conversion', object.id, guideId+'|'+currentScene.id );

	}

	
	//set the guideScene variable
	function updateGuideScene(id){
		console.log('updateGuideScene('+id+')' + firstRun);
		
		for(var s in guideScenes)
			if(guideScenes[s].sceneId == id){
				guideScene = guideScenes[s];
				updateGuideSceneTextButtons();
				processGuideSceneHotspots();

				if(guideScene.description || guideScene.scene.description ){
					if(enoughSpaceForGuideAndButtons() && !firstRun){

						$('#sceneContainer').show();
						if(!infoOpen)
							$('#sceneContainer .scene-link').trigger('click');

						$('#openSceneInfo').show();
					}
					if(!infoOpen)

					$('#openSceneInfo').show();
					updateScrollers("guide");
				}
				else{
					if(infoOpen)
						closeTab('right');

					$('#openSceneInfo').hide();
				}
			}
		
		//if there's a video load it
		if(guideScene){
			if(guideScene.transitionVideo)
				loadVideo(guideScene.transitionVideo);

			//if there's a voiceover load it
			if(guideScene.voiceover)
				loadVoiceover(guideScene.voiceover);
		}
	}

	
	function processGuideSceneHotspots(){
		
		console.log('processGuideSceneHotspots()');
		
        for (ix in guideScene.photos) 
            photos.push(guideScene.photos[ix]); 
        
        for (ix in guideScene.posters) 
            posters.push(guideScene.posters[ix]); 
        
        for (ix in guideScene.sounds) 
            sounds.push(guideScene.sounds[ix]); 
	}
	
	//set the text
	function updateGuideSceneTextButtons()
	{
		console.log('updateGuideSceneTextButtons()');
			
		if(!guideScene || gameInProgress)
			return;
		
		var $desc = (guideScene.description || guideScene.scene.description || "")
      
		//filter out Flash rubbish code
		    .replace(/<p.+?>/ig, "<span>")
		    .replace(/<\/p.+?>/ig, "</span> <br/>")
	        .replace(/<LI>/ig, "")
	        .replace(/<\/LI>/ig, "<br/>")
	        .replace(/<[b|u]>/ig, " ")
	        .replace(/<\/[b|u]>/ig, " ")
	        .replace(/<TEXTFORMAT.+?>/g, " ")
	        .replace(/<\/TEXTFORMAT>/g, " ")
	        .replace(/<FONT.+?>/g, " ")
	        .replace(/<\/FONT>/g, " ")
	        .replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig, "<a href=\"javascript:guideLink(\'$1\')\"  >$2</a> ");
		
	//	var height = $('window').height() * 0.7;

		
		$('.sceneText').html( '<h1>' +guideScene.titleMerged + '</h1><span>' +  $desc  + '</span>');
		processedGuideSceneText = $('.sceneText').html();

		
		guideScene.links = $('.sceneText a').map(function(){
			if( this.getAttribute('href') )
		    return this.getAttribute('href').split(/'/)[1];
		}).get();	
		
		

			//getFBShares();
			//getTwitterShares();
		
	}
	
	
	function getFBShares() {
	    var url = 'http://graph.facebook.com/http://beek.co' + window.location.pathname;
	    try{ $.getJSON(url, $.proxy(updateFBShares, this));}
	    catch(error){}
	   
	}
	
    function updateFBShares(data){
    	if(data.shares > 0)
    	$('#facebookCount span').text( data.shares );
    	else $('#facebookCount').remove();
    }
    
	function getTwitterShares() {
	    var url = 'http://urls.api.twitter.com/1/urls/count.json?url=http://beek.co' + window.location.pathname + '&callback=?';    
	    try{  $.getJSON(url, $.proxy(updateTwitterShares, this));}
	    catch(error){}
	}
	
    function updateTwitterShares(data){
    	if(data.count > 0)
    		$('#twitterCount span').text( data.count );
    	else $('#twitterCount').remove();
    }

   
	function getLinks(){
		
		if(!guideScene)
			return;

		var text = guideScene.description || guideScene.scene.description || " ";
		var gdesc = $('<p>' + text + '</p>' );
		var glinksData = $('a', gdesc);
		var glinks = [];
		
		$(glinksData).each(function(){
		    glinks.push($(this).attr("href").split(/:/)[1] );
		});
		
		return glinks;
	}
	
	function cleanText( text, tags ){
		var $desc = (text)
	      
		//filter out Flash rubbish code
		    .replace(/<p.+?>/ig, tags ? "<p> " : "")
	        .replace(/<L.+?>/ig,tags ? "<li>": "")
	        .replace(/<[b|u]>/ig, " ")
	        .replace(/<\/[b|u]>/ig, " ")
	        .replace(/<TEXTFORMAT.+?>/g, " ")
	        .replace(/<\/TEXTFORMAT>/g, " ")
	        .replace(/<FONT.+?>/g, " ")
	        .replace(/<\/FONT>/g, " ")
	        .replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig,tags ? "<a href=\"javascript:guideLinkClicked(\'$1\')\"  >$2</a>" : "");

		return $desc;
	}
	
	
	function guideLinkClicked(data){

		isUserInteracting = true;
		autoplaying = false;
		
		guideLink(data);
	}
	
	function guideLink(data){
		
		console.log('guideLink(' + data + ')');

		if(!data)
			return;
		
		if(data.indexOf('|') > 0){
			var look = data.split('|');
			panoLook(look[0], look[1], look[2]);
		}
		else
			hotspotLook(data);
		
		if(infoOpen){
			 var t = $('.sceneText').html().replace( data + "')",data + "')\" class=\"guideLinkSelected");
			 $('.sceneText').html(t);
			 $('#sceneText').find('h1').css({'width' : headerWidth });
			 
		}
	}
	
	
	function  closeGuide(){
		console.log('closeGuide()');
		
		if(!guideOpen)
			return;
		
    	guideOpen = false;	
		$('#guide').animate({'left' :  '100%' },"slow", function(){
			
			$('#guide').empty();
			$('#guide').remove();
			setLights(0.8);
			
			$('#guideBack').remove();
			$('#headerButtons').remove();
			
			$('#openGuide').toggle();
		});
		

    	
	}
	
	function openGuideSceneButtons(){
		console.log("openGuideSceneButtons()");
		
		Sid.css(["../../resources/css/guide.css"]);
		
		$( "body" ).append( 
				'<div id="guideButtons" class="unselectable">'
				+ '<div id = "fullscreen" class="guideButton" ><i class="fa fa-expand"></i></div>'  
				+ '<div id = "connect" class="guideButton" ><i class="fa fa-exchange"></i></div>'
				+ '<div id = "volume" class="guideButton" ><i class="fa fa-volume-off"></i></div>'
				+ '<div id = "zoom" class="guideButton" ><i class="fa fa-search-plus"></i></div></div>'
		);
		$( "#container" ).append('<div id = "openGuide"  class="openGuideButton"><a href="#guide" class="guide-link"><i class="fa fa-bars"></i></a></div>');
		$( "#sceneContainer" ).append('<div id = "openSceneInfo"  class="openSceneInfo pushRight"><a href="#scene" class="scene-link"><i class="fa fa-plus"></i></a></div>');

		if(connectId){
			$("#connect i").toggleClass('fa-phone fa-phone-square');
			 $( "#connect" ).on('click',closeConnection);
		}
		else
			  $( "#connect" ).on('click',connect);
		
		$( "#fullscreen" ).click(function(){ fullscreenChange(); } );
		$( "#volume" ).click(function(){ volumeChange(); } );
		$( "#zoom" ).click(function(){ zoomChange(); } );

		$('.guide-link').bigSlide({
			menu: '#guide',
			push: '.push',
			side: 'left',
			menuWidth: 250,
			beforeOpen: function() {drawersChange('left', 'beforeOpen');},
			afterOpen: function() {drawersChange('left', 'afterOpen');},
			afterClose: function() {drawersChange('left', 'afterClose');}
		});

		$('.scene-link').bigSlide({
			menu: '#sceneContainer',
			push: '.pushRight',
			side: 'right',
			beforeOpen: function() {drawersChange('right', 'beforeOpen');},
			afterOpen: function() {drawersChange('right', 'afterOpen');},
			afterClose: function() {drawersChange('right', 'afterClose');}
		});



		
/*		if(inIframe() == true)
			{
			    $( "#openGuide" ).toggle();
			    $( "#openSceneInfo" ).toggle();
			}*/

	}
	
	function removeUIButtons(){
		
		
	}

	function closeTab(side){
		console.log('guide.closeTab:' + side);

		if(side == 'right' && infoOpen)
			$('#sceneContainer .scene-link').trigger('click');
		else if(side == 'left' && guideOpen)
			$('#container .guide-link').trigger('click');
	}

	function drawersChange(side,event){
		console.log("guide.drawersChange(" +side,event +")");
		switch(event){
			case 'beforeOpen':
				if(!enoughSpaceForGuideAndInfo())
					switch(side){
						case 'left':
							if(infoOpen)closeTab('right');
							break;
						case 'right':  if(guideOpen)closeTab('left'); break;
					}
				if(!enoughSpaceForGuideAndButtons())
					$('#guideButtons').hide();
				break;
			case 'afterOpen':
				switch(side){
					case 'left':
						guideOpen = true;
						$('.guide-link i').toggleClass('fa-bars fa-times');
						setTimeout(function(){updateScrollers("guide")},300);
						break;
					case 'right':
						infoOpen = true;
						$('.scene-link i').toggleClass('fa-plus fa-times');
						setTimeout(function(){updateScrollers("scene")},300);
						break;
				}
				break;
			case 'afterClose':
				$('#guideButtons').show();
				switch(side){
					case 'left':
						$('.guide-link i').toggleClass('fa-bars fa-times');
						guideOpen = false;
						$('#guideButtons').show();
						break;
					case 'right':
						$('.scene-link i').toggleClass('fa-plus fa-times');
						infoOpen = false;
						break;
				}
				break;

		}

	}


	function enoughSpaceForGuideAndButtons(){
		console.log($( document ).width(),$( '#guide').width() + $('#guideButtons').width() + $( '#openGuide').width());
		return  $( document ).width() > $( '#guide').width() + $('#guideButtons').width() + $( '#openGuide').width();
	}

	function enoughSpaceForGuideAndInfo(){
		return  $( document ).width() > $( '#guide').width() + $('#sceneContainer').width() + 50;
	}

	
	function sceneInGuide(sceneId){
		
		for(var i in guideScenes)
			if(guideScenes[i].scene.id == sceneId)
				return true;
		
		return false;
	}
	
	function getGuideScene( sceneId ){
		
		for(var i in guideScenes)
			if(guideScenes[i].scene.id == sceneId )
				return guideScenes[i];
		
		return null;
		
	}
	
	function getGuideSection( sectionId ){
		
		for(var i in guideSections)
			if(guideSections[i].id == sectionId )
				return guideSections[i];
		
		return null;	
	}
	
	function fullscreenChange(){
		$('#fullscreen i').toggleClass('fa-expand fa-compress');
		
		if(fullscreen)
			exitFullscreen();	
		else
			launchFullscreen(document.documentElement);	
		
	}

	
	function volumeChange(){
		$('#volume i').toggleClass('fa-volume-off fa-volume-up');
		
		if(volume > 0){
			volume = 0;
			if(currentVideo) currentVideo.mute();
		}
			
		else{
			volume = 1;
			if(currentVideo) currentVideo.unmute();
		}
			
		

			
		
		updateSceneSounds( lon );
	}
	
	function zoomChange(){
		$('#zoom i').toggleClass('fa-search-plus fa-search-minus');
		
		if($('#zoom i').hasClass('fa-search-plus'))
			tweenZoom(50);
		else
			tweenZoom(20);
		
		userInteracting = true;

	}
	function getOrientation(){

		if(window.innerHeight > window.innerWidth)
			return 'portrait'
		else
			return 'landscape'
	}
	
	
	function createHotspotText( hotspot,width ){
		
		console.log( 'showHotspotText()' );
		$( "body" ).append('<div id = "hotspotTextContainer"><div id = "hotspotText" class="hotspotText" ><h1>' + cleanText(hotspot.hotspotData.title) + '</h1><span>' + cleanText(hotspot.hotspotData.description) +'</span></div></div>');
		
		var height;
		
		if(isMobile && getOrientation() == 'portrait' )
			height = $( window ).height() * 0.5;
		else if(isMobile && getOrientation() == 'landscape' )
			height = $( window ).height() - 20;
		else
			height = $( window ).height() - 100;

			
		$( "#hotspotTextContainer" ).height(height);
		
		    $('#hotspotText').slimScroll({
		    	height: $width = height,
		        color: '#ffffff',
		        size: '10px',
		        distance: '20px',
		        alwaysVisible: true
		    });
		

		
	}
	
	function removeHotspotText(){
		
		console.log( 'removeHotspotText()' );
		
		$( "#hotspotTextContainer" ).remove();
		$( "#hotspotText" ).html( ' ');
		 $('#hotspotText').slimScroll({destroy: true});
		$( "#hotspotText" ).remove();

	}
	
	
	function addCloseButton(){

		if(closeButton)
			return;
			
		$( "body" ).append( '<div id = "closeButton" ><i class="fa fa-times"/>CLOSE</div>');
		$( "#closeButton" ).click(function(){removeCloseButton();});
		
		closeButton = true;
	}
	
	function removeCloseButton(){
		
		$( "#closeButton" ).remove();
		
		if(selectedHotspot)
			deselectHotspot();
		else if (guideOpen)
			closeGuide();
		
		closeButton = false;
		
		if(gameInProgress)
			$( document).trigger('hotspotClosed');

		
	}
	
	function updateH1Margin(){
		 var margin = $('.guideButton').css('width');
		 margin = margin.replace(/px/,"");
		 var m = parseInt(margin) +  4;
		$('#sceneText').find('h1').css({'margin-left' : m + 'px' });
		
	}

	function updateScrollers(element){
		console.log("guide.updateScrollers()" );

		var h =  $(document).height();

		if(element == "guide"){

			if($( document).height() < $('#menu').height() + $('#sectionTitle').height()){

				$('#menu').slimScroll({
					height:  $( document).height(),
					position: 'right',
					railVisible: true,
					alwaysVisible: true,
					height: h + "px"
				});

				$('.slinky-menu').css('margin-right', '10px');
			}
			else
				$('.slinky-menu').css('margin-right', '0');
		}
		else{

			if($( document).height() < $('#sceneText').height()){

				$('#sceneText').slimScroll({
				 height:  $( document).height(),
				 position: 'left',
				 railVisible: true,
				 alwaysVisible: true,
				 height: h + "px"
				 });


		}
		}
	}
