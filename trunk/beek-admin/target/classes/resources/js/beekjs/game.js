var gameInProgress, gameTask, gameTasks = [], hotspotsToHide = [], awaitingInteraction = false, showingHotspotDescription = false;

function game(data){
	
	gameInProgress = true;
	
	gameTasks = data;
	//sort data by order
	gameTasks.sort(dynamicSort("order"));


	//create list of hidden hotspots
	for (var i in gameTasks){
		 if(gameTasks[i].output)
			var output = JSON.parse( gameTasks[i].output );
		 if(output)
			 for(var o in output)
				 if(output[o].id)
					 hotspotsToHide.push(output[o].id);	
	}
	
	//create game UI
	$( "body" ).append('<div id = "gameContainer" class="sceneContainer" ><div id="gameText" class="gameText"/></div>');
}

function updateGameTask( task ){
	console.log("game.updateGameTask( " + task + ")");
	if(!task)
		completeGame();
	
	gameTask = task;
	
	//set the text
	var html = '<span>' + task.title != null ? '<h1>' + task.title + '</h1>' : null;
	html = html + task.instructions + '</span>'
	$('#gameText').html(html);
	
	//notify event dispatchers
	awaitingInteraction = true;

	console.log(gameTask.code);
	
	//sort listeners
	if(gameTask.code == 'panodrag')
		$(document).bind('drag', dragEvent);
	if(gameTask.code == 'findhotspots'){

		$(document).bind('hotspot', hotspotEvent);
		var output = JSON.parse( task.input );
		task.hotspotsToFind = [];
		 	for(var o in output)
				 task.hotspotsToFind.push( output[o] );		 
	}
}


function hotspotEvent(event){
	console.log('hotspotEvent()' );

	if(selectedHotspot.hotspotData.description){
		$('#gameContainer').fadeTo(200,0);
		$(document).bind('hotspotClosed', hotspotClosedHandler);
		showingHotspotDescription = true;
	}

	for (var i in gameTask.hotspotsToFind)
		if(gameTask.hotspotsToFind[i].id == selectedHotspot.hotspotData.id){
			$('#gameText').html( $('#gameText').html() + '<br/><br/><span><i class="fa fa-check"></i>  ' + gameTask.hotspotsToFind[i].title + '</span>');
			gameTask.hotspotsToFind.splice( i,1 );
			break;
		}
	
	if(gameTask.hotspotsToFind.length < 1){
		$(document).unbind('hotspot', hotspotEvent);
		taskComplete();
	}
}

function hotspotClosedHandler(e){
	$('#gameContainer').fadeTo(200,1);
	showingHotspotDescription = false;
	//nextTask();
}

function dragEvent(event){
	console.log('dragEvent()');
	
	taskComplete();
	$(document).unbind('drag', dragEvent);
}

function taskComplete(){
	console.log( 'taskComplete()');
	
	if(gameTask.feedback)
		$('#gameText').html( $('#gameText').html() + '<br/><br/><span>' + gameTask.feedback + '</span> ');

		$('#gameText').html( $('#gameText').html() + '<br/><br/><a href="#"><h1 class="next">Next ></h1></a>');

		$('.next').bind('click', nextTask);


		//if(isMobile && getOrientation() === 'portrait')
		$('#gameText').slimScroll({
			height: isMobile ? $( document).height() * 0.3 + "px" :  $( document).height() + "px",
			position: 'left',
			railVisible: true,
			start: 'bottom'
		});


	if(gameTask.load_scene)
		setTimeout(getScene(gameTask.load_scene),2000);

	//nextTask();
}

function nextTask(){
	console.log( 'nextTask()' );

	if(showingHotspotDescription)
		return;
	
	if(!gameTasks.length){
		completeGame();
		return;
	}

	if(gameTask.output){
		var output = JSON.parse( gameTask.output );
		for(var i in hiddenHotspots)
			for(var o in output)
				if(hiddenHotspots[i].id == output[o].id)
					showHiddenHotspot( hiddenHotspots[i]);
	}

	//little delay to let user read feedback
	setTimeout(updateGameTask, 500, gameTasks[ gameTasks.indexOf(gameTask) + 1] );
	//remove current task
	gameTasks.splice(gameTasks.indexOf(gameTask), 1);



	
}

function completeGame(){
	//routine to build normal page
	gameInProgress = false;
	
	$('#gameContainer').remove();
	openGuideSceneButtons();
	nextButton();
	prevButton();
	
}
