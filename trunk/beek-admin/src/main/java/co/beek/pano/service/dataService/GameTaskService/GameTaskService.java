package co.beek.pano.service.dataService.GameTaskService;

import co.beek.pano.model.dao.entities.GameTask;

public interface GameTaskService {

	public GameTask getGameTask(String taskId);

	public void saveGameTask(GameTask task);

	public void deleteGameTask(GameTask task);

}
