package co.beek.pano.model.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;

@Entity
@Table(name = "locations")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Photoshoots that have been accepted, and are in the 'process' stage
	 */
	public static int STATE_NEW = 0;

	/**
	 * Locations that have been through the 'process' stage, and are awaiting
	 * approval.
	 */
	public static int STATE_PENDING = 1;

	/**
	 * Locations that have been approved.
	 */
	public static int STATE_LIVE = 2;

	/**
	 * Locations that have been cancelled.
	 */
	public static int STATE_CANCELLED = 3;

	public static int LOGO_SQUARE = 130;



	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	@NotNull
	@Column(name = "team_id")
	private String teamId;

	@Column(name = "order_id")
	private String photoShootId;

	@Column(name = "ew_id")
	private String ewId;

	@NotNull
	@Column(name = "sceneordercount")
	private int sceneOrderCount;



	@NotNull
	@Column(name = "sceneapprovedcount")
	private int sceneApprovedCount;

	@NotNull
	@Column(name = "scenecount")
	private int sceneCount;

	@NotNull
	@Column(name = "status")
	private int status;

	@NotNull
	@Column(name = "type")
	private int type;

	@NotNull
	@Column(name = "destination_id")
	private String destinationId;

	@Column(name = "default_scene_id", insertable = true, updatable = true)
	private String defaultSceneId;

	//@Column(name = "logo_file_id", insertable = true, updatable = true)
	//private long logoFileId;

	@NotNull
	@Column(name = "latitude")
	private double latitude;

	@NotNull
	@Column(name = "longitude")
	private double longitude;

	@Size(max = 255)
	@Column(name = "title")
	public String title;

	@Lob
	@Column(name = "description")
	private String description;

	/**
	 * Comments that came from photoshoot
	 */
	@Lob
	@Column(name = "comments")
	private String comments;
//
	@Size(max = 100)
	@Column(name = "infourl")
	private String infoURL;

	@Size(max = 100)
	@Column(name = "bookingurl")
	private String bookingURL;

	// @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
	// message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the
	// field contains phone or fax number consider using this annotation to
	// enforce field validation
	@Size(max = 100)
	@Column(name = "phone")
	private String phone;

	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider
	// using this annotation to enforce field validation
	@Size(max = 100)
	@Column(name = "email")
	private String email;
//
//	@Size(max = 200)
//	@Column(name = "address")
//	private String address;

	@Immutable
	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@NotFound(action = NotFoundAction.IGNORE)
	@Cascade({ org.hibernate.annotations.CascadeType.MERGE })
	@JoinColumn(name = "default_scene_id", insertable = false, updatable = false)
	private Scene defaultScene;

//	@ManyToOne()
//	@Fetch(value = FetchMode.JOIN)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@Cascade({ org.hibernate.annotations.CascadeType.MERGE })
//	@JoinColumn(name = "logo_file_id", insertable = false, updatable = false)
//	private FileData logoFile;

	/**
	 * Making this insertable/updatable means that i can save location/scenes in
	 * one go with hibernate when processing an order
	 */
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name = "location_id", insertable = true, updatable = true)
	// private List<Scene> scenes = new ArrayList<Scene>();

	public Location() {
	}

	/**
	 * Constructor when creating a scene from an order.
	 */
	public Location(Destination destination, EWPhotoShoot shoot) {
		setDestination(destination);
		setPhotoShoot(shoot);

	}

	public void setDestination(Destination destination) {
		destinationId = destination.getId();
		latitude = destination.getLatitude();
		longitude = destination.getLongitude();
	}

	public void setPhotoShoot(EWPhotoShoot shoot) {
		teamId = shoot.getTeamId();
		photoShootId = shoot.getId();
		title = shoot.getLocationTitle();
		comments = shoot.getComments();
		sceneOrderCount = shoot.getSceneOrderCount();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInfoURL() {
		return infoURL;
	}

	public void setInfoURL(String infoURL) {
		this.infoURL = infoURL;
	}

	public String getBookingURL() {
		return bookingURL;
	}

	public void setBookingURL(String bookingURL) {
		this.bookingURL = bookingURL;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}

	// @JSON(include = false)
	// public List<Scene> getScenes() {
	// return scenes;
	// }
	//
	// public void addScene(Scene scene) {
	// scenes.add(scene);
	//
	// // update this each time scene is added
	// sceneCount = scenes.size();
	//
	// if(sceneCount == 1)
	// {
	// defaultScene = scene;
	// defaultSceneId = scene.getId();
	// }
	// }

	// @JSON(include = false)
	// public HashMap<Long, String> getSceneLookup() {
	// HashMap<Long, String> lookup = new HashMap<Long, String>();
	// for (Scene scene : scenes) {
	// lookup.put(scene.getId(), scene.getTitle());
	// }
	// return lookup;
	// }
	//
	// public void setScenes(List<Scene> scenes) {
	// this.scenes = scenes;
	// }

//	public FileData getLogoFile() {
//		return logoFile;
//	}
//
//	public void setLogoFile(FileData logoFile) {
//		this.logoFileId = logoFile.getId();
//		this.logoFile = logoFile;
//	}

	public Scene getDefaultScene() {
		return defaultScene;
	}

	public void setDefaultScene(Scene defaultScene) {
		this.defaultScene = defaultScene;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDefaultSceneId() {
		return defaultSceneId;
	}

	public void setDefaultSceneId(String defaultSceneId) {
		this.defaultSceneId = defaultSceneId;
	}

//	public long getLogoFileId() {
//		return logoFileId;
//	}
//
//	public void setLogoFileId(long logoFileId) {
//		this.logoFileId = logoFileId;
//	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Location)) {
			return false;
		}
		Location other = (Location) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Location[ id=" + id + " ]";
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getComments() {
		return comments;
	}

	public String getPhotoShootId() {
		return photoShootId;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusText() {
		if (status == Location.STATE_NEW)
			return "New";

		if (status == Location.STATE_PENDING)
			return "Pending Approval";

		if (status == Location.STATE_LIVE)
			return "Live";

		if (status == Location.STATE_CANCELLED)
			return "Cancelled";

		return "Other Status";
	}

	public void setSceneCount(int sceneCount) {
		this.sceneCount = sceneCount;
	}

	public int getSceneCount() {
		return sceneCount;
	}

	public int getSceneOrderCount() {
		return sceneOrderCount;
	}


	public int getSceneApprovedCount() {
		return sceneApprovedCount;
	}

	public void setSceneApprovedCount(int sceneApprovedCount) {
		this.sceneApprovedCount = sceneApprovedCount;
	}

	public String getEwId() {
		return ewId;
	}

	public void setEwId(String ewId) {
		this.ewId = ewId;
	}
}
