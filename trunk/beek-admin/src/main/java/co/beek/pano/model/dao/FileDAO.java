package co.beek.pano.model.dao;

import java.util.List;

import co.beek.pano.model.dao.entities.FileData;

public interface FileDAO {
    public List<FileData> getPublicFiles();

    public void addFile(FileData file);
}
