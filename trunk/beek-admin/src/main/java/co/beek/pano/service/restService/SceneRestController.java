package co.beek.pano.service.restService;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import co.beek.pano.service.dataService.FileUploadService;
import co.beek.pano.service.dataService.sceceService.SceneService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import flexjson.JSONDeserializer;
import flexjson.JSONException;

@Controller
public class SceneRestController extends BeekRestController {
	@Inject
	private SceneService sceneService;
	@Inject
	private S3UploadService s3UploadService;

	@Inject
	private FileUploadService fileUploadService;

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/json", method = RequestMethod.GET)
	public ResponseEntity<String> getScene(
			@PathVariable("sceneId") String sceneId) {

		try {
			SceneDetail scene = sceneService.getSceneDetail(sceneId);
			/*
			 * try { Thread.sleep(4000); } catch (InterruptedException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); }// simulate
			 * remote server
			 */
			return getPayloadResponse(scene);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding scene: "
					+ e.getCause().toString());
		}

	}

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getScenep(
			@PathVariable("sceneId") String sceneId,
			@RequestParam("callback") String callback) {

		try {
			SceneDetail scene = sceneService.getSceneDetail(sceneId);

			String sessionJson = callback + "("
					+ serializer.deepSerialize(scene) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding scene: "
					+ e.getCause().toString());
		}

	}

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/pano", method = RequestMethod.POST)
	public ResponseEntity<String> savePanoUpload(
			DefaultMultipartHttpServletRequest request,
			@PathVariable("sceneId") String sceneId) {

		EWContact contact = getUser(request);
		if (contact == null)
			return getErrorResponse(123456, "not logged in");

		Scene scene = sceneService.getScene(sceneId);

		// check permissions on this user uploading for this team
//		if (isSuperUser(request) == false
//				&& contact.isInTeam(scene.getLocation().getTeamId()) == false)
//			return getErrorResponse(123456,
//					"does not have permission to upload pano for scene:"
//							+ sceneId);

		MultipartFile upload = request.getFile("file");
		if (upload == null)
			return getErrorResponse(123456, "no upload found");

		try {
			String tempFileName = UUID.randomUUID().toString() + ".jpg";
			java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
					+ tempFileName);
			upload.transferTo(tempFile);

			fileUploadService.addTask(sceneId, tempFileName, "dan@beek.co");
			return getSuccessResponse();

		} catch (Exception e) {
			e.printStackTrace();
			return getErrorResponse(1234, "upload failed");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/increment", method = RequestMethod.GET)
	public ResponseEntity<String> getPanoIncrement(
			@PathVariable("sceneId") String sceneId) {

		Scene scene = sceneService.getScene(sceneId);
		String increment = Integer.toString(scene.getPanoIncrement());
		return new ResponseEntity<String>(increment, new HttpHeaders(),
				HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/panotask", method = RequestMethod.GET)
	public ResponseEntity<String> getPanoTask(
			@PathVariable("sceneId") String sceneId) {
		try {
			return getPayloadResponse(fileUploadService.getPanoTask(sceneId));
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}
	
	
	

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/save", method = RequestMethod.POST)
	public ResponseEntity<String> saveScene(HttpServletRequest request,
			@PathVariable("sceneId") String sceneId,
			@RequestParam("json") String json) {

		EWContact contact = getUser(request);

		// If not logged in, return early
		if (contact == null)
			return getErrorResponse(1234, "not logged in");

		SceneDetail scene = sceneService.getSceneDetail(sceneId);

		// insufficient permissions
		if (!scene.hasWritePermission(contact))
			return getErrorResponse(1234, "does not have permission");

		try {
			// We copy details from json onto the scene
			scene = new JSONDeserializer<SceneDetail>().deserializeInto(json,
					scene);

			// Update the scene with new details
			sceneService.updateSceneDetail(scene);

			// Serialize the updated scene
			return getSuccessResponse();

		} catch (Exception e) {
			return getErrorResponse(1234,
					"Error saving scene." + e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/scene/{sceneId}/thumb", method = RequestMethod.POST)
	public ResponseEntity<String> saveSceneThumb(
			@PathVariable("sceneId") String sceneId,
			@RequestParam("thumbName") String thumbName,
			DefaultMultipartHttpServletRequest request) {

		// If not logged in, return early
		if (!isLoggedIn(request))
			return getErrorResponse(1234, "not logged in");

		try {
			// Create and populate the thumb with the new bitmap
			//File file = new File(Constants.TEMP_DIR_PATH + thumbName);
			//request.getFile("thumb").transferTo(file);

			java.io.File file = new java.io.File(Constants.TEMP_DIR_PATH
					+ thumbName);
			request.getFile("thumb").transferTo(file);

			// upload thumb to the scenes bucket
			s3UploadService.uploadFile(assetsBucket, file);

			return getSuccessResponse();
		} catch (Exception e) {
			return getErrorResponse(1234, "Error uploading scene thumb");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/scenes/within/{lat}/{lon}/{meters}/", method = RequestMethod.GET)
	public ResponseEntity<String> getNearby(@PathVariable("lat") double lat,
			@PathVariable("lon") double lon, @PathVariable("meters") int meters) {
		try {
			List<Scene> scenes = sceneService.getScenesWithin(lat, lon, meters);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/scenes/in/location/{locationId}/", method = RequestMethod.GET)
	public ResponseEntity<String> getSameLocation(@PathVariable("locationId") String locationId) {
		try {
			List<Scene> scenes = sceneService.getAllScenesForLocation(locationId);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
}
