package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;
import co.beek.pano.model.dao.hotspots.RssReader;
import co.beek.pano.model.dao.hotspots.Sound;
import co.beek.pano.model.dao.hotspots.Video;
import flexjson.JSON;

class GuideSceneComparator implements Comparator<GuideScene> {
	public int compare(GuideScene object1, GuideScene object2) {
		return object1.getOrder() - object2.getOrder();
	}
}

@Entity
@Table(name = "guidescenes")
public class GuideScene implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	public static final GuideSceneComparator orderComparator = new GuideSceneComparator();

	public static final GuideScene create(String guideSectionId, Scene scene) {
		GuideScene guideScene = new GuideScene();
		guideScene.id = UUID.randomUUID().toString();
		guideScene.guideSectionId = guideSectionId;
		guideScene.sceneId = scene.getId();
		guideScene.scene = scene;

		return guideScene;
	}

	@Id
	@NotNull
	@Column(name = "id")
	public String id;

	@NotNull
	@Column(name = "guidesection_id")
	private String guideSectionId;

	@NotNull
	@Column(name = "scene_id")
	private String sceneId;
	

	@NotNull
	@Column(name = "[order]")
	public int order;

	// @NotNull
	@Size(max = 100)
	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;

	@Column(name = "hotspots_to_hide")
	public String hotspotsToHide;
	
	@Column(name = "page_position_x")
	public Integer pagePositionX;
	
	@Column(name = "page_position_y")
	public Integer pagePositionY;

	@Column(name = "transition_video")
	public String transitionVideo;
	
	@Column(name = "voiceover")
	public String voiceover;
	
	@Column(name = "voiceover_track")
	public String voiceoverTrack;

	@Column(name = "visits")
	public Integer visits;
	
	@Column(name = "avg_time")
	public Double avgTime;

	@Immutable
	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@JoinColumn(name = "scene_id", insertable = false, updatable = false)
	private Scene scene;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guidescene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Photo> photos = new ArrayList<Photo>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guidescene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	public List<Poster> posters = new ArrayList<Poster>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guidescene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Sound> sounds = new ArrayList<Sound>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guidescene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Video> videos = new ArrayList<Video>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guidescene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	public List<RssReader> rssReaders = new ArrayList<RssReader>();

	public GuideScene() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		if (title != null && !title.equals(""))
			return title;
		return scene.getTitle();
	}

	public String getTitleMerged() {
		if (title != null && !title.equals(""))
			return title;

		return scene.getTitle();
	}
	
	public String getTitleUrl() {
		if (title != null && !title.equals(""))
			return title.replace(' ', '-');

		return scene.getTitle().replace(' ', '-');
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	


	public String getDescription() {
		return description;
	}
	
	public String getCleanDescription() {
    	if(description != null){
    	String cleanText = StringEscapeUtils.unescapeXml(description);
    	cleanText = cleanText.replaceAll("\\<[^>]*>","");
		return cleanText;
    	}
		return "";
	}

	public void setDescription(String description) {

		this.description = description;

		if(scene != null)
			if(scene.getDescription() == null || scene.getDescription().length() < 1 && description != null)
				scene.setDescription(description);

	}

	/**
	 * Used in the guide view page
	 * 
	 * @return
	 */
	@JSON(include = false)
	public String getDescriptionMergedCleaned() {
		String descMerged = "";
		if (description != null && !description.equals(""))
			descMerged = description;

		else if (scene.getDescription() != null)
			descMerged = scene.getDescription();

		return descMerged.replaceAll("\\<.*?\\>", "");
	}

	public void setDescriptionMergedCleaned(String text){

		description = text;

		if (scene.getDescription() != null)
			scene.setDescription(text);

	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Scene getScene() {
		return scene;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}
	
	public List<Poster> getPosters() {
		return posters;
	}

	public List<Video> getVideos() {
		return videos;
	}

	public void setVideos(List<Video> videos) {
		this.videos = videos;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GuideScene)) {
			return false;
		}
		GuideScene other = (GuideScene) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "GuideScene[ id=" + id + " ]";
	}

	public void setSounds(List<Sound> sounds) {
		this.sounds = sounds;
	}

	public List<Sound> getSounds() {
		return sounds;
	}

	public String getGuideSectionId() {
		return guideSectionId;
	}

	public void setGuideSectionId(String guideSectionId) {
		this.guideSectionId = guideSectionId;
	}

	public Integer getVisits() {
		if(visits != null)
			return visits;
		else
			return 0;
	}

	public void setVisits(Integer visits) {
		this.visits = visits;
	}

	public Double getAvgTime() {
		if(avgTime != null)
			return avgTime;
		else
			return 0.0;
	}

	public void setAvgTime(Double avgTime) {
		this.avgTime = avgTime;
	}

	public GuideScene cloneNow(String sectionId) throws CloneNotSupportedException {
		GuideScene clone = (GuideScene) super.clone();
		clone.id = UUID.randomUUID().toString();
		clone.guideSectionId = sectionId;
		
		clone.photos = new ArrayList<Photo>();
		for (Photo photo : photos)
			clone.photos.add(photo.cloneNow(clone.id));

		clone.posters = new ArrayList<Poster>();
		for (Poster poster : posters)
			clone.posters.add(poster.cloneNow(clone.id));

		clone.sounds = new ArrayList<Sound>();
		for (Sound sound : sounds)
			clone.sounds.add(sound.cloneNow(clone.id));

		clone.videos = new ArrayList<Video>();
		for (Video video : videos)
			clone.videos.add(video.cloneNow(clone.id));

		clone.rssReaders = new ArrayList<RssReader>();
		for (RssReader reader : rssReaders)
			clone.rssReaders.add(reader.cloneNow(clone.id));

		return clone;
	}
}
