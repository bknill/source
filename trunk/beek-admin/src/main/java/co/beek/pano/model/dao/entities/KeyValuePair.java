package co.beek.pano.model.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clienterrors")
public class KeyValuePair implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "key")
	private String key;

	@Column(name = "value")
	private String value;

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
