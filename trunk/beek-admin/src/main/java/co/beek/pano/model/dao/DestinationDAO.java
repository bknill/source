package co.beek.pano.model.dao;

import java.util.List;

import co.beek.pano.model.dao.entities.Destination;

public interface DestinationDAO {
    public Destination getDestination(String destinationId);

    public List<Destination> getAllDestinations();

    public Destination getDestinationByTitle(String title);

    public void addDestination(Destination destination);

    public void updateDestination(Destination destination);

    public void deleteDestination(Destination destination);
}
