package co.beek.pano.service.dataService.hotspotService;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;

@Service
@Scope("prototype")
@Transactional
public class HotspotServiceImpl implements HotspotService {

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Photo getPhoto(String photoId) throws HibernateException {
		return (Photo) sessionFactory.getCurrentSession().get(Photo.class,
				photoId);
	}

	@Override
	public Poster getPoster(String posterId) throws HibernateException {
		return (Poster) sessionFactory.getCurrentSession().get(Poster.class,
				posterId);
	}

	@Override
	public Bubble getBubble(String bubbleId) throws HibernateException {
		return (Bubble) sessionFactory.getCurrentSession().get(Bubble.class,
				bubbleId);
	}

}
