package co.beek.pano.model.beans;

import java.util.ArrayList;

public class ReferralGuideTableRow {
	private String guideTitle;
	private long guideId;
	private int pageviews;
	private double timeSpent;
	private int conversions;
	private ArrayList<ReferralSceneTableRow> referralSceneTable;
	
	public ReferralGuideTableRow(String guideTitle,int pageviews, double timeSpent, ArrayList<ReferralSceneTableRow> referralSceneTable) {
		this.guideTitle = guideTitle;
		this.pageviews = pageviews;
		this.timeSpent = timeSpent;
		this.referralSceneTable = referralSceneTable;
	}
	
	public ReferralGuideTableRow(){}

	public String getGuideTitle() {
		return guideTitle;
	}

	public void setGuideTitle(String guideTitle) {
		this.guideTitle = guideTitle;
	}

	public long getGuideId() {
		return guideId;
	}

	public void setGuideId(long guideId) {
		this.guideId = guideId;
	}

	public int getPageviews() {
		return pageviews;
	}

	public void setPageviews(int pageviews) {
		this.pageviews = pageviews;
	}

	public double getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(double timeSpent) {
		this.timeSpent = timeSpent;
	}

	public ArrayList<ReferralSceneTableRow> getReferralSceneTable() {
		return referralSceneTable;
	}

	public void setReferralSceneTable(ArrayList<ReferralSceneTableRow> referralSceneTable) 
	{
		this.referralSceneTable = referralSceneTable;
	}

	public int getConversions() {
		return conversions;
	}

	public void setConversions(int conversions) {
		this.conversions = conversions;
	}
	
	
}
