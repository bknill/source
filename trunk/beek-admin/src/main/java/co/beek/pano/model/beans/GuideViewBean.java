package co.beek.pano.model.beans;

import java.io.Serializable;

import co.beek.pano.service.dataService.guideService.GuideService;

public class GuideViewBean implements Serializable{
    private GuideService guideService;

    public GuideViewBean(GuideService service) {
        this.guideService = service;
    }
}
