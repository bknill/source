package co.beek.pano.model.dao.hotspots;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "rssreader")
public class RssReader implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    public String id;
    
    @Column(name = "scene_id")
    public String sceneId;

    @Column(name = "guidescene_id")
    public String guideSceneId;
    
    @NotNull
    @Column(name = "pan")
    public double pan;

    @NotNull
    @Column(name = "tilt")
    public double tilt;

    @NotNull
    @Column(name = "distance")
    public double distance;

    @Column(name = "rotationx")
    public Double rotationX;

    @Column(name = "rotationy")
    public Double rotationY;

    @Column(name = "rotationz")
    public Double rotationZ;

    @Size(max = 200)
    @Column(name = "title")
    public String title;

    @Column(name = "url")
    public String url;
    
   @Column(name = "type", columnDefinition = "TINYINT")
   public int type;
    
    
    public RssReader cloneNow(String guideSceneId) throws CloneNotSupportedException {
    	RssReader clone = (RssReader) super.clone();
    	clone.id = UUID.randomUUID().toString();
    	clone.guideSceneId = guideSceneId;
    	//clone.file = null;
		return clone;
	}
    
  

	}
  

