package co.beek.web.guide;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

import co.beek.pano.model.dao.entities.GameTask;
import co.beek.pano.service.dataService.hotspotService.HotspotService;

/**
 * Created by Ben on 21/10/2015.
 */
@Named
public class HotspotConverter implements Converter {

    @Inject
    public HotspotService hotspotService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }


        try {

            if(hotspotService.getBubble(submittedValue) != null)
                 return hotspotService.getBubble(submittedValue);

            else if(hotspotService.getPhoto(submittedValue) != null)
                return hotspotService.getPhoto(submittedValue);

             else if(hotspotService.getPoster(submittedValue) != null)
                return hotspotService.getPoster(submittedValue);

            else
                return null;

        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Guide ID", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }


        if (modelValue instanceof GameTask) {
            return String.valueOf(((GameTask) modelValue).getId());
        } else {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Guide", modelValue)));
        }
    }

}