package co.beek.web.lead;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.model.dao.enterprizewizard.EWLeadResponse;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@ManagedBean
@Scope("session")
@Controller("LeadListToDoController")
public class LeadListToDoController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -1556328919286681960L;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	

	@Inject
	private EWService ewService;

	public List<EWLead> leadList;
	public DateFormat df = new SimpleDateFormat("MMM dd yyyy");
	private String today;
	private Date dateToday;

	
	BeekSession session;
	public EWContact user;

	 public LeadListToDoController() throws ParseException {  
		 
		 setTodaysDate();
		 
		//session = getSession();
			
		//user = session.getUser();

	}
	 
	    
	 public void setTodaysDate()
	 {
		dateToday = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		df.setTimeZone(TimeZone.getTimeZone("NZ"));
        today = df.format(dateToday);
	 }
	 
	 public String getToday()
	 {
        return today;
	 }
	 
	 public void setToday(String Today)
	 {
		today = Today;
	 }
	
	public List<EWLead> getLeadList()
	{
           return leadList;
	}
	
	public void setLeadList(List<EWLead> list)
	{
           leadList = list;
	}
	
	
	public List<EWLead> getTodayToDoList()
	{
		List<EWLead> foundLeads = new ArrayList<EWLead>();
		
		   for (int i = 0; i < leadList.size(); i++)
	        {
			   if(leadList.get(i).next_action_date == null || leadList.get(i).business_name == null)
				   continue;
			   
				if(leadList.get(i).next_action_date.equals(today))
					foundLeads.add(leadList.get(i));
			
	        }
		   
		   return foundLeads;
		
	}
	
	public List<EWLead> getOldToDoList()
	{
		System.out.println("getOldToDoList()");
		List<EWLead> foundLeads = new ArrayList<EWLead>();
		
		   for (int i = 0; i < leadList.size(); i++)
	        {
			   //don't include those with missing data - they don't deserve to be on this list
			   if(leadList.get(i).next_action_date == null || leadList.get(i).next_action == null || leadList.get(i).business_name == null)
				   continue;
			   
			   //add all applicable leads with a date before today
				try {
					if(df.parse(leadList.get(i).next_action_date).before(dateToday))
                        foundLeads.add(leadList.get(i));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
		      
		   Collections.sort(foundLeads, new Comparator<EWLead>() {
			    public int compare(EWLead m1, EWLead m2) {
			        try {
						return df.parse(m1.next_action_date).compareTo(df.parse(m2.next_action_date));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 0;
			    }

			});
		   
		   Collections.reverse(foundLeads);
		   
		   return foundLeads;
		
	}
	
	public void updateNextActionDate(int days, String id, String next_action, String next_action_date) throws ParseException{
		

		String newNextActionDate;
		//update new date if supplied or set to today if not
		 if(days > 0){
			 Date nextActionDate = df.parse(next_action_date);
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(nextActionDate);
			cal.add(Calendar.DATE, days); 
			df.setTimeZone(TimeZone.getTimeZone("NZ"));
			newNextActionDate = df.format(cal.getTime());
		 }
		 else 
			 newNextActionDate = today;
		 
		 //update lead with new next action date to remove from list
		 for(EWLead lead : leadList)
			 if(lead.getId().equals(id))
				 lead.next_action_date = newNextActionDate;

		 //commit the data
		 updateLeadNextAction(id, next_action, newNextActionDate);
		
	}
	
	public void updateLeadNextAction(String id, String next_action, String next_action_date){
		try {
			EWLeadResponse response = ewService.UpdateLeadNextAction(session.getUsername(), session.getPassword(), id, next_action, next_action_date);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
      

}
