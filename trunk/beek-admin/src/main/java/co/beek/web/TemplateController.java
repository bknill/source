package co.beek.web;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.util.BeekSession;

@Controller("TemplateController")
public class TemplateController extends BaseController implements Serializable {
	private static final long serialVersionUID = 284259433912057854L;

	@RequestMapping(value = "/favicon.ico", method = RequestMethod.GET)
	public String faviconIco() {
		return "/resources/images/favicon.ico";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mapView(HttpServletRequest request) {
		try {
			BeekSession session = getSession(request);
			EWContact contact = session.getUser();
			
			System.out.println("The session : " + session);
			System.out.println("The contact : " + contact);
			System.out.println(Constants.domain);
			System.out.println("Is logged in : " + session.isLoggedIn());
			if (session.isLoggedIn()) {
				System.out.println("The type of session : " + session.getType());
				
				if(session.getType().equals("employees")) {
					return "/views/adminDashboard.jsf";
				} else {
					return "/views/customerDashboard.jsf";
				}
			} else {
				return "/views/login.jsf";
			}
		} catch (Exception ex) {
			System.out.println("Failed to find the map view : " + ex.getMessage());
			ex.printStackTrace();
			return "/views/adminDashboard.jsf";
		}
	}

	public boolean isLoggedIn() {
		return getSession().isLoggedIn();
	}

	public boolean isEmployee() {
		try {
			return getSession().getType().equals("employees");
		} catch (Exception ex) {
			System.out.println("Could not determine if employee : " + ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}

	public void viewInfo() throws IOException
	{
		redirect("/user/info");
	}

	public void viewLocationDashboard() throws IOException
	{
		redirect("/location/dashboard");
	}
	
	public void viewLeadList() throws IOException
	{
		redirect("/lead/list");
	}
	
	public void viewGuideDashboard() throws IOException
	{
		redirect("/guide/dashboard");
	}
	
	public void viewDashboard() throws IOException
	{
		redirect("/");
	}
	
	public void logout() throws IOException {
		/*System.out.println("called logout");
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"called logout", "called logout");
		FacesContext.getCurrentInstance().addMessage(null, m); */
		getSession().logout();
		redirect("/");
	}
	
	public void test() throws IOException
	{

		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_WARN,
				"testing the button", "testing the button");
		FacesContext.getCurrentInstance().addMessage(null, m);
		getSession().logout();
		redirect("/");
		
	}
}
