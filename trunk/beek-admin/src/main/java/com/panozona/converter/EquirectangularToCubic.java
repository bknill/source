package com.panozona.converter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

/*
Copyright 2009 Zephyr Renner
This file is part of EquirectangulartoCubic.java.
EquirectangulartoCubic is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
EquirectangulartoCubic is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with EquirectangulartoCubic. If not, see http://www.gnu.org/licenses/.

The Equi2Rect.java library is modified from PTViewer 2.8 licenced under the GPL courtesy of Fulvio Senore, originally developed by Helmut Dersch.  Thank you both!

Some basic structural of this code are influenced by / modified from DeepJZoom, Glenn Lawrence, which is also licensed under the GPL.

compilation (Mac OS X 10.6): javac EquirectangularToCubic 
jar creation (Mac OS X 10.6): jar cvfm EquirectangularToCubic.jar META-INF/MANIFEST.MF EquirectangularToCubic EquirectangularToCubic.class EquirectangularToCubic\$1.class EquirectangularToCubic\$CmdParseState.class FileListFilter.class ImageTo2DIntArrayExtractor.class Equi2Rect.class
*/

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

import co.beek.pano.util.ImageUtil;


/**
 * @author Zephyr Renner
 */
public class EquirectangularToCubic {

    private enum CmdParseState {DEFAULT, OUTPUTDIR, OVERLAP, INPUTFILE, INTERPOLATION, QUALITY}

    ;
    static Boolean deleteExisting = true;
    static String format = "jpg";

    // The following can be overriden/set by the indicated command line arguments
    static int overlap = 1;                 // -overlap
    static String interpolation = "lanczos2";   // -interpolation (lanczos2, bilinear, nearest-neighbor)
    static float quality = 0.8f;     // -quality (0.0 to 1.0)
    // static File outputDir = null;         // -outputdir or -o
    static Boolean verboseMode = true;   // -verbose or -v
    static Boolean debugMode = true;     // -debug
    static Vector<File> inputFiles = new Vector<File>();  // must follow all other args
    static Vector<File> outputFiles = new Vector<File>();

    private Equi2Rect equi2Rect;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//        try {
//            try {
//				
//                parseCommandLine(args);
//                // default location for output files is folder beside input files with the name of the input file
//				if (outputFiles.size() == 1) {
//					File outputFile = outputFiles.get(0);
//					if (!outputFile.exists()) {
//						File parentFile = outputFile.getAbsoluteFile().getParentFile();
//						String fileName = outputFile.getEmail();
//						outputFile = createDir(parentFile, fileName);
//					}
//				}
//				if (outputFiles.size() == 0) {
//					Iterator<File> itr = inputFiles.iterator();
//			        while (itr.hasNext()) {
//			        	File inputFile = itr.next();
//                  		File parentFile = inputFile.getAbsoluteFile().getParentFile();
//                  		String fileName = inputFile.getEmail();
//        				String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
//						File outputFile = createDir(parentFile, "cubic_" + nameWithoutExtension);
//						outputFiles.add(outputFile);
//                  	}
//				}
//                if (debugMode) {
//                    System.out.printf("overlap=%d ", overlap);
//                    System.out.printf("interpolation=%s ", interpolation);
//                    System.out.printf("quality=%d ", quality);
//                }
//
//            } catch (Exception e) {
//            	String message = "Invalid command: " + e.getMessage() + "\n";
//message += "Help: \n";
//message += "Requirements: Java SE 1.6 or later \n";
//message += "Usage: \n";
//message += "java [-java_options] -jar path/to/EquirectangularToCubic.jar [-options] [args...] \n";
//message += "For a list of java options try: java -help or java -X for a list of less common \n";
//message += "options. Loading large images for conversion takes a lot of RAM so you will find the \n";
//message += "-Xmx option useful to raise Java's maximum heap size. The -Xmx command is \n";
//message += "followed immediately by an integer specifying RAM size and a unit indicator. For \n";
//message += "example, -Xmx1024m means to use 1024 megabytes. If you see an error about heap size, \n";
//message += "then you will need to increase this value. \n";
//message += "\n";
//message += "EquirectangularToCubic options and example usages:\n";
//message += "Basic usage example for the jar file:\n";
//message += "java -Xmx1024m -jar path/to/EquirectangularToCubic.jar path/to/directory/of/images/ \n";
//message += "This will generate a folder of converted files beside the input directory or file \n";
//message += "with 'cubic_' prepended onto the name. So in the basic example above, the output \n";
//message += "files would be in path/to/directory/of/cubic_images/. \n";
//message += "\n";
//message += "Options:\n";
//message += "-overlap: number of pixels of overlap added around the cubefaces.  A value of 1 \n";
//message += "causes the cube faces to be 90 degrees wide in pixels (dependent on the size of the \n";
//message += "input equirectangular) plus 1 pixel. \n";
//message += 		"\tDefault is 1. \n";
//message += 		"\tExample: java -Xmx1024m -jar path/to/EquirectangularToCubic.jar -overlap 10 \n";
//message += 		"\tpath/to/directory/of/images/ \n";
//message += "-interpolation: possible values are: lanczos2, bilinear, nearest-neighbor.  Sets the \n";
//message += 		"\tinterpolation algorithm to use during remapping.  Lanczos2 and bilinear are \n";
//message += 		"\tthe highest quality.  nearest-neighbor is faster and generally lower quality, \n";
//message += 		"\talthough it preserves sharp edges better. \n";
//message += 		"\tDefault is lanczos2. \n";
//message += 		"\tExample: java -Xmx1024m -jar path/to/EquirectangularToCubic.jar -interpolation \n";
//message += 		"\tnearest-neighbor path/to/directory/of/images/ \n";
//message += "-quality: output JPEG compression.  Value must be between 0.0 and 1.0.  0.0 is \n";
//message += 		"\tmaximum compression, lowest quality, smallest file.  1.0 is least compression, \n";
//message += 		"\thighest quality, largest file. \n";
//message += 		"\tDefault is 0.8.\n";
//message += 		"\tExample: java -Xmx1024m -jar path/to/EquirectangularToCubic.jar -quality 0.5 \n";
//message += 		"\tpath/to/directory/of/images/ \n";
//message += "-outputdir or -o: the output directory for the converted images.  It need not exist. \n";
//message += 		"\tDefault is a folder next to the input folder or file, with 'cubic_' prepended \n";
//message += 		"\tto the name of the input (input files will have the extension removed). \n";
//message += 		"\tExample: java -Xmx1024m -jar path/to/EquirectangularToCubic.jar -outputdir \n";
//message += 		"\tmy/outputdirectory path/to/directory/of/images/ \n";
//message += "-verbose or -v: makes the utility more 'chatty' during processing. \n";
//message += "-debug: print various debugging messages during processing. \n";
//message += "\n";
//message += "The arguments following any options are the input JPEGs or folders (or both).  If \n";
//message += "there are multiple input folders or JPEGs, each should be separated by a space.  \n";
//message += "Input folders will not be NOT be recursed; Only files immediately inside the folder \n";
//message += "ending in .jpg, .jpeg, .JPG, or .JPEG will be processed. All inputs will be processed \n";
//message += "into the one output directory, so general usage is to process one folder containing \n";
//message += "multiples JPEGs or to process one singe JPEG. \n";
//message += 		"\tExample: java -Xmx1024m -jar path/to/EquirectangularToCubic.jar \n";
//message += 		"\tpath/to/EquiA.jpg path/to/EquiB.jpg \n";
//message += "\n";
//message += "Happy converting!  And next time you see Helmut Dersch, Fulvio Senore, and other \n";
//message += "contributors to PTViewer, say thanks!\n";
//                //System.out.println("Invalid command: " + e.getMessage());
//                System.out.println(message);
//                return;
//            }
//            
//            Equi2Rect.init();            
//            
////             Iterator<File> itr = inputFiles.iterator();
////             while (itr.hasNext())
////                  processImageFile(itr.next(), outputDir);
//            for( int i = 0; i < inputFiles.size(); i++ ) {
//            	processImageFile(inputFiles.get(i), outputFiles.get(i));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch(Exception e){
//        	e.printStackTrace();
//        }
    }

    /**
     * Process the command line arguments
     *
     * @param args the command line arguments
     
    private static void parseCommandLine(String[] args) throws Exception {
        CmdParseState state = CmdParseState.DEFAULT;
        for (int count = 0; count < args.length; count++) {
            String arg = args[count];
            switch (state) {
                case DEFAULT:
                    if (arg.equals("-verbose") || arg.equals("-v"))
                        verboseMode = true;
                    else if (arg.equals("-debug")) {
                        verboseMode = true;
                        debugMode = true;
                    } else if (arg.equals("-outputdir") || arg.equals("-o"))
                        state = CmdParseState.OUTPUTDIR;
                    else if (arg.equals("-overlap"))
                        state = CmdParseState.OVERLAP;
                    else if (arg.equals("-interpolation"))
                        state = CmdParseState.INTERPOLATION;
                    else if (arg.equals("-quality"))
                        state = CmdParseState.QUALITY;
                    else
                        state = CmdParseState.INPUTFILE;
                    break;
                case OUTPUTDIR:
                    outputFiles.add(new File(args[count]));
                    state = CmdParseState.DEFAULT;
                    break;
                case OVERLAP:
                    overlap = Integer.parseInt(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
                case INTERPOLATION:
                    interpolation = args[count];
                    state = CmdParseState.DEFAULT;
                    break;
                case QUALITY:
                    quality = Float.parseFloat(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
            }
            if (state == CmdParseState.INPUTFILE) {
                File inputFile = new File(arg);
                if (!inputFile.exists())
                    throw new FileNotFoundException("input file not found: " + inputFile.getPath());
                //check if file is folder.
                if (inputFile.isDirectory()) {
                    Vector<String> exts = new Vector<String>();
                    exts.add("jpg");
                    exts.add("jpeg");
                    exts.add("JPG");
                    exts.add("JPEG");
                    FilenameFilter select = new FileListFilter(exts);
                    File[] files = inputFile.listFiles(select);
                    java.util.List fileList = java.util.Arrays.asList(files);
                    for (java.util.Iterator itr = fileList.iterator(); itr.hasNext(); ) {
                        File f = (File) itr.next();
                        inputFiles.add((File) f);
                    }
                } else {
                    inputFiles.add(inputFile);
                }
            }
        }
        if (inputFiles.size() == 0)
            throw new Exception("No input files given");
    }

*/
    
    public EquirectangularToCubic() {
        equi2Rect = new Equi2Rect();
        equi2Rect.init();
    }

    public void startTransformationProcess(File inFile) throws Exception {
        File parentFile = inFile.getAbsoluteFile().getParentFile();
        String fileName = inFile.getName();
        String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
        File outputDir = createDir(parentFile, "cubic_" + nameWithoutExtension);
        processImageFile(inFile, outputDir);
        equi2Rect = null;
    }

    public void startTransformationProcess(File inFile, String cubicDirName) throws Exception {
    	File parentFile = inFile.getAbsoluteFile().getParentFile();
    	File outputDir = createDir(parentFile, cubicDirName);
    	processImageFile(inFile, outputDir);
    	equi2Rect = null;
    }

    /**
     * Process the given image file, producing its Deep Zoom output files
     * in a subdirectory of the given output directory.
     *
     * @param inFile    the file containing the image
     * @param outputDir the output directory
     * @throws Exception
     */
    public void processImageFile(File inFile, File outputDir) throws Exception {
    	System.out.println("EquirectangularToCubic.processImageFile()");
        if (verboseMode)
            System.out.printf("Processing image file: %s\n", inFile);

        String fileName = inFile.getName();
        String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
        //String pathWithoutExtension = outputDir + File.separator + nameWithoutExtension;

        BufferedImage equi = loadImage(inFile);
        //Image equi = (Image) loadImage(inFile);

        int equiWidth = equi.getWidth();
        int equiHeight = equi.getHeight();

        if (equiWidth != equiHeight * 2) {
            if (verboseMode)
                System.out.println("Image is not equirectangular (" + equiWidth + " x " + equiHeight + "), skipping...");
            throw new Exception("Image is not EquiRectangular");
        }

        // private static int[][] im_allocate_pano(int ai[][], int i, int j)
        //im_allocate_pano(equiData, equiWidth, equiHeight))
        int equiData[][] = ImageUtil.convertImageToPixels(equi);        
//      too slow  
//      new ImageTo2DIntArrayExtractor(equiData, (Image) equi).doit();

        //release image resource
        equi.flush();
        equi = null;
        
        double fov; // horizontal field of view
        // peri = 2PIr
        double r = equiWidth / (2D * Math.PI);
        double y = (Math.tan(Math.PI / 4D) * r + overlap);
        fov = Math.atan(y / r) * 180 / Math.PI * 2;
        int rectWidth = (int) (y * 2);
        int rectHeight = rectWidth;

        Boolean bilinear;
        Boolean lanczos2;
        if (interpolation == "lanczos2") {
            lanczos2 = true;
            bilinear = false;
        } else if (interpolation == "bilinear") {
            lanczos2 = false;
            bilinear = true;
        } else if (interpolation == "nearest-neighbor") {
            lanczos2 = false;
            bilinear = false;
        } else {
            lanczos2 = true;
            bilinear = false;
        } // lanczos2 is default for junk values.

        int rectData[];
        double pitch;
        double yaw;
        BufferedImage output;

        equi2Rect.initForIntArray2D(equiData);

        yaw = 0;
        pitch = 0;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_f", quality);

        yaw = 90;
        pitch = 0;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_r", quality);

        yaw = 180;
        pitch = 0;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_b", quality);

        yaw = 270;
        pitch = 0;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_l", quality);

        yaw = 0;
        pitch = 90;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_u", quality);

        yaw = 0;
        pitch = -90;
        rectData = equi2Rect.extractRectilinear(yaw, pitch, fov, equiData, rectWidth, rectHeight, equiWidth, bilinear, lanczos2);
        output = new BufferedImage(rectWidth, rectHeight, BufferedImage.TYPE_INT_RGB);
        output.setRGB(0, 0, rectWidth, rectHeight, rectData, 0, rectWidth);
        saveImageAtQuality(output, outputDir + File.separator + nameWithoutExtension + "_d", quality);

    }

    /**
     * Delete a file
     *
     * @param path the path of the directory to be deleted
     */
    private void deleteFile(File file) throws IOException {
        if (!file.delete())
            throw new IOException("Failed to delete file: " + file);
    }

    /**
     * Recursively deletes a directory
     *
     * @param path the path of the directory to be deleted
     */
    private void deleteDir(File dir) throws IOException {
        if (!dir.isDirectory())
            deleteFile(dir);
        else {
            for (File file : dir.listFiles()) {
                if (file.isDirectory())
                    deleteDir(file);
                else
                    deleteFile(file);
            }
            if (!dir.delete())
                throw new IOException("Failed to delete directory: " + dir);
        }
    }

    /**
     * Creates a directory
     *
     * @param parent the parent directory for the new directory
     * @param name   the new directory name
     */
    private static File createDir(File parent, String name) throws IOException {
        assert (parent.isDirectory());
        File result = new File(parent + File.separator + name);
        if (result.exists()) return result;
        if (!result.mkdir())
            throw new IOException("Unable to create directory: " + result);
        return result;
    }

    /**
     * Loads image from file
     *
     * @param file the file containing the image
     */
    private static BufferedImage loadImage(File file) throws IOException {
        System.out.println("Max Heap Size: "+Runtime.getRuntime().maxMemory());
    	System.out.println("totalMemory: "+Runtime.getRuntime().totalMemory());
    	System.out.println("freeMemory: "+Runtime.getRuntime().freeMemory());
        BufferedImage result = null;
        try {
            result = ImageIO.read(file);
        } catch (Exception e) {
        	System.out.println("Cannot read image file " + e.getMessage());
            throw new IOException("Cannot read image file: " + file);
        }
        return result;
    }

    /**
     * Saves image to the given file
     * @param img the image to be saved
     * @param path the path of the file to which it is saved (less the extension)
     */
//     private static void saveImage(BufferedImage img, String path) throws IOException {
//         File outputFile = new File(path + "." + format);
//         try {
//             ImageIO.write(img, format, outputFile);
//         } catch (IOException e) {
//             throw new IOException("Unable to save image file: " + outputFile);
//         }
//     }

    /**
     * Saves image to the given file
     *
     * @param img     the image to be saved
     * @param path    the path of the file to which it is saved (less the extension)
     * @param quality the compression quality to use (0-1)
     */

    private static void saveImageAtQuality(BufferedImage img, String path, float quality) throws IOException {
        File outputFile = new File(path + "." + format);
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
        ImageWriter writer = (ImageWriter) iter.next();
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(quality);
        FileImageOutputStream output = new FileImageOutputStream(outputFile);
        writer.setOutput(output);
        IIOImage image = new IIOImage(img, null, null);
        try {
            writer.write(null, image, iwp);
        } catch (IOException e) {
            throw new IOException("Unable to save image file: " + outputFile);
        } finally {
            output.close();
            writer.dispose();
        }
    }
}


class FileListFilter implements FilenameFilter {
    private Vector<String> extensions;

    public FileListFilter(Vector<String> extensions) {
        this.extensions = extensions;
    }

    public boolean accept(File directory, String filename) {
        if (extensions != null) {
            Iterator<String> itr = extensions.iterator();
            while (itr.hasNext()) {
                String ext = (String) itr.next();
                if (filename.endsWith('.' + ext)) return true;
            }
        }
        return false;
    }
}