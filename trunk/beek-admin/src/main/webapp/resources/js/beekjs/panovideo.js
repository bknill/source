var video, panoVideoMesh, firstPlay = true;



function panovideo(sceneObject) {
    console.log("panovideo");

    if(!video){
        video = document.createElement( 'video' );

        if(!gameInProgress)
            video.loop = true;
        video.crossOrigin = 'anonymous';
        video.preload = 'auto';
    }

    video.src = cdnPrefix + "/"+ sceneObject.video;

    console.log(video.src);


    if(!isMobile)
        panoVideoPlay();
    else if(!firstPlay)
        panoVideoPlay();
    else{

        $('body').append('<div id="play-button" class="playButton"><i class="fa fa-play"></i></div>');

        window.addEventListener('touchstart', function videoStart() {
            panoVideoPlay();

            if(firstPlay){
                $("#play-button").remove();
                firstPlay = false;
                showLoader();
            }

            this.removeEventListener('touchstart', videoStart);
        });
    }


}

function panoVideoPlay(){
    console.log("panoVideo.panoVideoPlay()");

    makeVideoPlayableInline(video);
    video.play();
    checkPlayBack();

}


var checkPlayBack = function(event){
    console.log("panoVideo.checkPlayBack()");

    var panoPlayBackInterval = setInterval(function(){
        if (video.currentTime > 0){
            clearInterval(panoPlayBackInterval);
            hideLoader();
            showVideo(video);
        }
    },10);
}

function showVideo(video){
    console.log("panoVideo.showVideo()");
    var geometry = new THREE.SphereGeometry( 500, 60, 40 );
    var texture = new THREE.VideoTexture( video );
    texture.minFilter = THREE.LinearFilter;
    texture.magFilter = THREE.LinearFilter;
    texture.format = THREE.RGBFormat;

    var material   = new THREE.MeshBasicMaterial( { map : texture } );
    // material.side = THREE.BackSide;

    panoVideoMesh = new THREE.Mesh( geometry, material );
    panoVideoMesh.scale.x = -1;
    panoVideoMesh.rotation.y = Math.PI / 2;

    loadScene();
    scene.remove(box);
    scene.add(panoVideoMesh);
    addLights();
}


var makeVideoPlayableInline=function(){"use strict";function e(e){var r=void 0;var i=void 0;function n(t){r=requestAnimationFrame(n);e(t-(i||t));i=t}this.start=function(){if(!r){n(0)}};this.stop=function(){cancelAnimationFrame(r);r=null;i=0}}function r(e,r,i,n){function t(r){if(Boolean(e[i])===Boolean(n)){r.stopImmediatePropagation()}delete e[i]}e.addEventListener(r,t,false);return t}function i(e,r,i,n){function t(){return i[r]}function u(e){i[r]=e}if(n){u(e[r])}Object.defineProperty(e,r,{get:t,set:u})}var n=typeof Symbol==="undefined"?function(e){return"@"+(e||"@")+Math.random()}:Symbol;var t=/iPhone|iPod/i.test(navigator.userAgent);var u=n();var a=n();var d=n("nativeplay");var o=n("nativepause");function s(e){var r=new Audio;r.src=e.currentSrc||e.src;r.crossOrigin=e.crossOrigin;return r}var f=[];f.i=0;function c(e,r){if((f.tue||0)+200<Date.now()){e[a]=true;f.tue=Date.now()}e.currentTime=r;f[++f.i%3]=r*100|0/100}function v(e){return e.driver.currentTime>=e.video.duration}function p(e){var r=this;if(r.video.readyState>=r.video.HAVE_FUTURE_DATA){if(!r.hasAudio){r.driver.currentTime=r.video.currentTime+e*r.video.playbackRate/1e3;if(r.video.loop&&v(r)){r.driver.currentTime=0}}c(r.video,r.driver.currentTime)}if(r.video.ended){r.video.pause(true)}}function l(){var e=this;var r=e[u];if(e.webkitDisplayingFullscreen){e[d]();return}if(!e.paused){return}r.paused=false;if(!e.buffered.length){e.load()}r.driver.play();r.updater.start();e.dispatchEvent(new Event("play"));e.dispatchEvent(new Event("playing"))}function m(e){var r=this;var i=r[u];i.driver.pause();i.updater.stop();if(r.webkitDisplayingFullscreen){r[o]()}if(i.paused&&!e){return}i.paused=true;r.dispatchEvent(new Event("pause"));if(r.ended){r[a]=true;r.dispatchEvent(new Event("ended"))}}function y(r,i){var n=r[u]={};n.paused=true;n.hasAudio=i;n.video=r;n.updater=new e(p.bind(n));if(i){n.driver=s(r)}else{n.driver={muted:true,paused:true,pause:function t(){n.driver.paused=true},play:function a(){n.driver.paused=false;if(v(n)){c(r,0)}},get ended(){return v(n)}}}r.addEventListener("emptied",function(){if(n.driver.src&&n.driver.src!==r.currentSrc){c(r,0);r.pause();n.driver.src=r.currentSrc}},false);r.addEventListener("webkitbeginfullscreen",function(){if(!r.paused){r.pause();r[d]()}else if(i&&!n.driver.buffered.length){n.driver.load()}});if(i){r.addEventListener("webkitendfullscreen",function(){n.driver.currentTime=r.currentTime});r.addEventListener("seeking",function(){if(f.indexOf(r.currentTime*100|0/100)<0){n.driver.currentTime=r.currentTime}})}}function b(e){var n=e[u];e[d]=e.play;e[o]=e.pause;e.play=l;e.pause=m;i(e,"paused",n.driver);i(e,"muted",n.driver,true);i(e,"playbackRate",n.driver,true);i(e,"ended",n.driver);i(e,"loop",n.driver,true);r(e,"seeking");r(e,"seeked");r(e,"timeupdate",a,false);r(e,"ended",a,false)}function h(e,r,i){r=typeof r==="undefined"?true:r;i=typeof i==="undefined"?true:i;if(i&&!t||e[u]){return}y(e,r);b(e);e.classList.add("IIV");if(!r&&e.autoplay){e.play()}}h.isWhitelisted=t;return h}();
