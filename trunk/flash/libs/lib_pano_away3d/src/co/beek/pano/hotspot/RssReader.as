package co.beek.pano.hotspot
{
	import co.beek.BeekFont;
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.FacebookUser;
	import co.beek.model.data.TwitterStatus;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.service.VisitService;
	import co.beek.utils.BrowserUtil;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.JSONBeek;
	
	import com.adobe.serialization.json.JSON;
	import com.facebook.graph.Facebook;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;

	
	public class RssReader extends Hotspot
	{
		
		private var loader:URLLoader = new URLLoader;
		
		private var ReaderUI:RssReaderFl;
		
		private var FBRenderers:Vector.<RssReaderFBRenderer> = new Vector.<RssReaderFBRenderer>();
		
		private var scrollBox:fbFeedScroll = new fbFeedScroll;
		
		protected static const APP_ID:String = "308635632515451"; 
		
		public static const DRAG_ON:String = "DRAG_ON";
		public static const DRAG_OFF:String = "DRAG_OFF";
		
		public function RssReader(data:HotspotData, ui:RssReaderFl)
		{
			super(data, ui);
			this.ReaderUI = ui;
			
			this.visible = false;
			
			ui.closeButton.visible = false;
			ui.closeButton.mouseChildren = false;
			ui.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			
			
			ReaderUI.inWorldTwitter.visible = false;
			ReaderUI.inWorldFacebook.visible = false;
			
			data.addEventListener(Event.CHANGE, onHotspotDataChange);
			
			data.addEventListener(RssReaderData.TYPE_CHANGED, onTypeChange);
			
			if(!Model.isMobile){
				Security.loadPolicyFile("http://graph.facebook.com/crossdomain.xml");
				Security.loadPolicyFile("http://profile.ak.fbcdn.net/crossdomain.xml");
				Security.loadPolicyFile("https://fbexternal-a.akamaihd.net/crossdomain.xml");
				//Security.allowDomain("*");
				//Security.allowInsecureDomain("*");
			}
			
			updateType();

		}
		
		
		private function get readerFl():RssReaderFl
		{
			return RssReaderFl(_ui);
		}
		
		private function get rssData():RssReaderData
		{
			return _data as RssReaderData;
		}
		

		
		private function twitter():void
		{
			loader.load(new URLRequest(Model.config.serviceUrl+"/social/" + Model.state.guideId + "/" + Model.currentScene.id +"/twitter"));
			loader.addEventListener(Event.COMPLETE,onTwitterDataLoaded)
		
			ReaderUI.inWorldTwitter.visible = true;
		}
		
		
		
		private function onHotspotDataChange(event:Event):void
		{
			update();
		}
		
		private function onTypeChange(event:Event):void
		{
			updateType();
		}
		
		private function updateType():void
		{
			if(rssData.type == 0)
				twitter()	
			else if (rssData.type == 1)
				facebook()
			else if (rssData.type == 2)
				facebookFeed()
		}
		
		
		public function update():void
		{
			trace("RSSReader.Update()");
			var ui:RssReaderFl = readerFl;

			if(!rssData)
				return;
			
			if(rssData.type == 0)
				inworldSprite = ui.inWorldTwitter;
			else if (rssData.type == 1)
				inworldSprite = ui.inWorldFacebook;
			else if (rssData.type == 2)
				generateBitmap(ui.inWorldFacebook);
		}
		
		public function generateBitmap(value:DisplayObject):void
		{
			var w:Number = powerOf2Down(value.width);
			var scale:Number = w/value.width;
			
			var scaledHeight:Number = value.height * scale;
			var h:Number = powerOf2Up(scaledHeight);

			var bitmap:BitmapData = new BitmapData(w, h, true, 0x00000000);
			bitmap.draw(value);
			
			bmData = bitmap;
			
			inworldBitmap = bitmap;
		}

		
	
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			Model.hotspot = null;
		}
		
		public override function dispose():void
		{
			super.dispose();
		}
		
		private function  onTwitterDataLoaded(event:Event):void
		{
			if(rssData)
			if(rssData.type != 0)
				return;
			
			var json:String = URLLoader(event.target).data;
			var data:Object = JSONBeek.decode(json);
			
			if(data.payload != null){
				var twitter:TwitterStatus = new TwitterStatus(data.payload);
				var urlStart:int = twitter.status.search("http");
				var urlEnd:int = twitter.status.indexOf(" ",urlStart);
				var statusNoUrl:String = twitter.status.slice(0,urlStart);
				var userLink:String = "<a href='https://www.twitter.com/" + twitter.user + "'target='_blank'>@" + twitter.user + "</a>";
				var shortenDate:String = twitter.date.slice(0,11) + twitter.date.slice(-4,twitter.date.length);
				Fonts.format(ReaderUI.inWorldTwitter.status,Fonts.titleFont,statusNoUrl);
				Fonts.formatHTML(ReaderUI.inWorldTwitter.username,Fonts.bodyFont,userLink);
				Fonts.format(ReaderUI.inWorldTwitter.date,Fonts.bodyFont,shortenDate);
			}
			
			ReaderUI.inWorldTwitter.status.autoSize = "left";
			ReaderUI.inWorldTwitter.status.wordWrap = true;
			ReaderUI.inWorldTwitter.username.y = ReaderUI.inWorldTwitter.status.height;
			ReaderUI.inWorldTwitter.date.y = ReaderUI.inWorldTwitter.status.height;
			
			ReaderUI.inWorldTwitter.twitterIcon.y = ReaderUI.inWorldTwitter.username.y + ReaderUI.inWorldTwitter.username.height;
			ReaderUI.inWorldTwitter.twitterInstructions.y = ReaderUI.inWorldTwitter.twitterIcon.y;
			


			Fonts.formatHTML(ReaderUI.inWorldTwitter.twitterInstructions,Fonts.titleFont,"Tweet your review or comments");

			ReaderUI.inWorldTwitter.addEventListener(MouseEvent.CLICK,function():void{
				Model.dispatchTrackingEvent(Constants.GA_CATEGORY_CONVERSION,"twitterReview", Model.state.guideId);
				var tweetLink:String = "https://www.twitter.com/share?url=http://beek.co/g" 
					+ Model.state.guideId + "/s" + Model.currentScene.id 
					+"&text=Add your review or message here"
				var url:URLRequest = new URLRequest(tweetLink);
				navigateToURL(url, "_blank");
			});
			
			ReaderUI.inWorldTwitter.twitterInstructions.textColor = 0xffffff;
			ReaderUI.inWorldTwitter.username.textColor = 0xffffff;
			
			this.visible = true;
			update();
		}
		
		private function facebook():void
		{
			trace("RSSReader.facebook()");
			loader.load(new URLRequest(Model.config.serviceUrl+"/user/" + Model.state.guideId + "/" + Model.currentScene.id +"/"));
			loader.addEventListener(Event.COMPLETE,onFacebookDataLoaded)
			
			Fonts.formatHTML(ReaderUI.inWorldFacebook.facebookInstructions,Fonts.boldFont,"I've been here!");
			ReaderUI.inWorldFacebook.facebookInstructions.textColor = 0xffffff;
			ReaderUI.inWorldFacebook.facebookInstructions.wordWrap = true;
			ReaderUI.inWorldFacebook.facebookInstructions.autoSize = "left";
			ReaderUI.inWorldFacebook.facebookIcon.addEventListener(MouseEvent.CLICK,handleLoginClick);
			ReaderUI.inWorldFacebook.facebookInstructions.addEventListener(MouseEvent.CLICK,handleLoginClick);
			
			ReaderUI.inWorldFacebook.facebookIcon.x = 0;
			ReaderUI.inWorldFacebook.facebookIcon.y = 0;
			ReaderUI.inWorldFacebook.facebookInstructions.x = ReaderUI.inWorldFacebook.facebookIcon.width + 20;
			ReaderUI.inWorldFacebook.facebookInstructions.y = 0;
			
			ReaderUI.inWorldFacebook.visible = true;
			
			this.visible = true;
			update();
		}
		
		private var facebookFeedPageId:String;
		private var facebookFeedPageAccess:String;
		
		private function facebookFeed():void   
		{
			trace("RSSReader.FacebookFeed()");
			ReaderUI.inWorldFacebook.visible = true;
			
			if(rssData.url){
				var array:Array = rssData.url.split("|");
				var pageId:String = array[0];
				facebookFeedPageId = pageId;
				var accessToken:String = array[1];
				facebookFeedPageAccess = accessToken;
				facebookFeedData(pageId,accessToken);
				
			}
			
			
			Fonts.formatHTML(ReaderUI.inWorldFacebook.facebookInstructions,Fonts.boldFont,"Like this page");
			ReaderUI.inWorldFacebook.facebookInstructions.textColor = 0xffffff;
			ReaderUI.inWorldFacebook.facebookInstructions.wordWrap = true;
			ReaderUI.inWorldFacebook.facebookInstructions.autoSize = "left";
			ReaderUI.inWorldFacebook.facebookIcon.addEventListener(MouseEvent.CLICK,handleLikeClick);
			ReaderUI.inWorldFacebook.facebookInstructions.addEventListener(MouseEvent.CLICK,handleLikeClick);
			
			ReaderUI.inWorldFacebook.facebookIcon.x = 0;
			ReaderUI.inWorldFacebook.facebookIcon.y = 0;
			ReaderUI.inWorldFacebook.facebookInstructions.x = ReaderUI.inWorldFacebook.facebookIcon.width + 20;
			ReaderUI.inWorldFacebook.facebookInstructions.y = 0;
			
			this.visible = false;	
		}
		
		private function facebookFeedData(pageId:String, accessToken:String):void
		{
			trace("RSSReader.facebookFeedInitHander()" + accessToken);
			var request:String = "/" + pageId + "/feed";
			var requestType:String = "GET";
			var params:Object = {access_token : accessToken};
			Facebook.api(request, loadFeedDataHandler, params, requestType);	
		}
		
		
		private function loadFeedDataHandler(response:Object, fail:Object):void
		{
			trace("RSSReader.loadFeedDataHandler()");
			if (response) {
				var json:String = com.adobe.serialization.json.JSON.encode(response);
				var posts:Object = JSONBeek.decode(json);
				
				var count:int = posts.length > 5 ? 5 : posts.length;
				
				for(var i:uint = 0; i < count; i++){
					if(posts[i].message || posts[i].picture)
						renderFbPost(posts[i]);
					else
						count ++
				}
				
				this.visible = true;
				update();
				
			}
			if (fail){
				trace(fail);
			}
		}	
		
		
		private function renderFbPost(data:Object):void
		{
			trace("RssReader.renderFbPost()");
			var json:String = com.adobe.serialization.json.JSON.encode(data);
			var post:Object = JSONBeek.decode(json);
			var renderer:RssReaderFBRenderer = new RssReaderFBRenderer(post);
			
			if(post.picture)
				renderer.addEventListener(RssReaderFBRenderer.LOADED,postLoaded);
			else
				loadPost(renderer);	
			
			FBRenderers.push(renderer);
		}
		
		private var rendererY:int = 10;

		
		private function postLoaded(event:Event):void
		{
			trace("RssReader.postLoaded()");
			var renderer:RssReaderFBRenderer = event.target as RssReaderFBRenderer;
			loadPost(renderer);		
		}
		
		private function loadPost(renderer:RssReaderFBRenderer):void
		{
			trace("RssReader.loadPost()");
			renderer.y = rendererY;
			rendererY = renderer.y + renderer.height;

			ReaderUI.inWorldFacebook.postLocator.addChild(renderer);
			update();
		}
		
		
		protected function handleLikeClick(event:MouseEvent):void 
		{
			BrowserUtil.navigate("https://www.facebook.com/"+facebookFeedPageId, BrowserUtil.BLANK);	
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_CONVERSION,	
				"facebookLike", 
				facebookFeedPageId +"|"+
				Model.state.guideId);
			
			facebookInit();
		}
		
		
		private function onFacebookDataLoaded(event:Event):void
		{
			var json:String = URLLoader(event.target).data;
			var data:Object = JSONBeek.decode(json);
			var payload:Array = data.payload;

			if(data.payload)
			for(var i:int = 0; i < payload.length; i++){
				var facebook:FacebookUser = new FacebookUser(payload[i]);
				createFacebookRender(facebook.email);	
			}
			
			this.visible = true;
			update();
		}
		
		
		protected function handleLoginClick(event:MouseEvent):void {
			
			if(!facebookLoggedInWithToken)
				facebookInit()
			else
				loadProfileData()		
		}
		
		
		
		private var userIds:Array = [];
		
		private function createFacebookRender(userId:String):void{

			if(userIds.indexOf(userId) != -1)
				return;
				
			ImageLoader.load("https://graph.facebook.com/" + userId + "/picture?type=square", onPhotoLoaded, onPhotoLoadError,true);
			userIds.push(userId);		
		}
		
		private var images:Vector.<Bitmap> = new Vector.<Bitmap>;
		
		private function onPhotoLoaded(data:BitmapData):void
		{
			var bitmap:Bitmap = new Bitmap(null, "auto", true);
			bitmap.bitmapData = data;
			images.push(bitmap);
			makeGrid();
		}
		

		
		private function makeGrid():void {
			
			if(numObjects == images.length)
				return;
		
			var gridContainer:Sprite = new Sprite();
			gridContainer.y = ReaderUI.inWorldFacebook.facebookIcon.height + 20;
			ReaderUI.inWorldFacebook.addChild(gridContainer);

			var numObjects:int = images.length;
			var numCols:int = 8;
			var column:int = 0;
			var row:int = 0;
			var gap:Number = 2;
			var cell:Bitmap;
			for (var i:int = 0; i < numObjects; i++) {
				column = i % numCols;
				row = int(i / numCols);
				cell = images[i];
				var fn:Function = function(e:Event):void{openFacebookPage(e,userIds[i])};
				cell.addEventListener(MouseEvent.MOUSE_DOWN, openFacebookPage);
				cell.x = (cell.width + gap) * column;
				cell.y = (cell.height + gap) * row;
				gridContainer.addChild(cell);
			}
			this.visible = true;
			update();
		}
		
		private function openFacebookPage(event:Event, data:String):void
		{
			var myURL:URLRequest = new URLRequest("https://www.facebook.com/" + data);
			navigateToURL(myURL, "_blank");
		}
		
		private function onPhotoLoadError():void
		{
			trace("Photo failed to load");
		}
		
		
		private var fbInit:Boolean = false;
		private var facebookLoggedInWithToken:Boolean;
		
		private function facebookInit():void   
		{
				Facebook.init(APP_ID, facebookInitHandler,{
					appId: APP_ID,
					status: true,
					cookie: true,
					xfmbl: true,
					channelUrl: 'http://beekdev.co/resources/js/channel.html',
					oauth: true,
					perms: 'publish_stream,email'
				});

		}
		
		private function facebookInitHandler(response:Object, fail:Object):void
		{
	
			try{
				if (response.accessToken)
				{
					var userAccessToken:String = com.adobe.serialization.json.JSON.encode(response.accessToken);
					facebookLoggedInWithToken = true;
					loadProfileData();
				} else {
					facebookLoggedInWithToken = false;
					Facebook.login(loadProfileData);
				}
			}
			catch(e:Error)
			{
				trace("login failed" + e.message);
				Facebook.login(facebookLogInHandler,{scope: 'publish_stream,email'});
			}

		}
		
		
		
		private function facebookLogInHandler(response:Object, fail:Object):void
		{
			try{
				if (response.accessToken)
				{
					var userAccessToken:String = com.adobe.serialization.json.JSON.encode(response.accessToken);
					facebookLoggedInWithToken = true;
					loadProfileData();
				} else {
					facebookLoggedInWithToken = false;
					Facebook.login(loadProfileData);
				}
			}
			catch(e:Error)
			{
				trace("login failed" + e.message);
				//Facebook.login(facebookLogInHandler,{scope: 'publish_stream,email'});
			}
		
		}

		
		private function loadProfileData():void
		{
			var request:String = "/me";
			var requestType:String = "GET";
			var params:Object = null;
			Facebook.api(request, loadProfileDataHandler, params, requestType);
		}
		

		
		
		private function loadProfileDataHandler(response:Object, fail:Object):void
		{
			if (response) {
				var userID:String = response.id;
				var fullName:String = response.name;
				var firstName:String = response.first_name;
				var lastName:String = response.last_name;
				var userEmail:String = response.email;
				var userPicURL:String = "http://graph.facebook.com/" + userID + "/picture";
			
				VisitService.user_id = userID;
				VisitService.first_name = firstName;
				VisitService.last_name = lastName;
				VisitService.email = userEmail;
				VisitService.beenHere();

				ReaderUI.inWorldFacebook.facebookIcon.removeEventListener(MouseEvent.CLICK,handleLoginClick);
				ReaderUI.inWorldFacebook.facebookInstructions.removeEventListener(MouseEvent.CLICK,handleLoginClick);
			
				createFacebookRender(userID);
				
				var params:Object = {   
					message: "I've been here!",
					link: 'https://beek.co/g'+ Model.state.guideId + '/s' + Model.currentScene.id};
				
				Facebook.api( 'me/feed', onComplete, params, "POST" );
			}
			
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_CONVERSION,	
				"facebookBeenHere", 
				Model.state.guideId);
		}
		
		

		
		private function onComplete( response:Object, fail:Object ):void 
		{
           trace(response);
		}
	}
}