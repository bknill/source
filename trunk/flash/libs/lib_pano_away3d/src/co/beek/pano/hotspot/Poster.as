package co.beek.pano.hotspot
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.hotspots.PosterData;
	import co.beek.service.VisitService;
	import co.beek.utils.BrowserUtil;
	
	public class Poster extends Hotspot
	{
		//private var ui:PosterContainerFl = new PosterContainerFl;
		
		private var frames:Dictionary = new Dictionary;
		
		private var posterOpenTime:Number;
		
		public function Poster(data:PosterData, ui:PosterContainerFl)
		{
			super(data, ui);
			
			// Store the frames in a n array for showing/hiding
			
			// this is to set before text gets added and num lines gets counted
			//ui.inWorld.textField.width = 110;
			
			ui.inWorld.closeButton.visible = false;
			ui.inWorld.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			
			ui.inWorld.viewButton.visible = false;
			ui.inWorld.viewButton.addEventListener(MouseEvent.CLICK, onViewButtonClick);
			
			ui.inWorld.bg.alpha = 0.75;
			data.addEventListener(Event.CHANGE, onHotspotDataChange);
			
			
			
			update();
		}
		
		private function onHotspotDataChange(event:Event):void
		{
			update();
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			event.stopPropagation();
			Model.hotspot = null;
		}
		
		private function onViewButtonClick(event:MouseEvent):void
		{
			if(Model.loggedIn)
				return;
			
			if(posterData.buttonSceneId)
				Model.state.loadingScene = posterData.buttonScene;

			else if(posterData.buttonFavourite)
				favouriteThis();
			
			else if(posterData.buttonUrl)
				openUrl();
		}
		
		private function favouriteThis():void
		{
			VisitService.favourite(posterData.title);
		}
		
		
		private function openUrl():void
		{
			if(posterData.buttonUrl == null)
				return;
			
			BrowserUtil.navigate(posterData.buttonUrl, BrowserUtil.BLANK);
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_CONVERSION,	
				"hotspot_poster_button", 
				 posterData.title +"|"+
				 posterData.id +"|"+
				 Model.currentScene.id);
			
		}
		
		private function get posterUi():PosterContainerFl
		{	return PosterContainerFl(_ui) }
		
		private function get posterData():PosterData
		{	return PosterData(_data) }
		
		private function update():void
		{
			var ui:PosterContainerFl = posterUi;
			
			if(!posterData.text)
				
				posterData.text = " ";
				
				Fonts.formatHTML(ui.inWorld.textField, Fonts.bodyFont, posterData.text);
			
			ui.inWorld.bg.visible = posterData.frame != PosterData.FRAME_NONE;
			
			ui.inWorld.textField.height = ui.inWorld.textField.textHeight +20;
			
			ui.inWorld.viewButton.y = ui.inWorld.textField.y + ui.inWorld.textField.height;
			if(posterData.hasButton)
			{
				ui.inWorld.viewButton.visible = true;
				Fonts.format(
					ui.inWorld.viewButton.textField, 
					Fonts.bodyFont, 
					posterData.buttonLabel ? posterData.buttonLabel : "MORE INFO",
					true
				);
				
				ui.inWorld.viewButton.textField.width = ui.inWorld.viewButton.textField.textWidth + 20;
				
				ui.inWorld.bg.height = ui.inWorld.viewButton.y + ui.inWorld.viewButton.height + 10;
			}
			else
			{
				ui.inWorld.viewButton.visible = false;
				ui.inWorld.bg.height = ui.inWorld.viewButton.y + 10;
			}
			
			if(posterData.showIcon)
				inworldSprite = ui.icon;
				
			else
				inworldSprite = ui.inWorld;
		}
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
			if(value)
				setInworldBlank();
			
			else if(posterData.showIcon)
				inworldSprite = posterUi.icon;
				
			else
				inworldSprite = posterUi.inWorld;
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			
			if(value)
			{
				onTweenToFlatComplete();
				posterUi.inWorld.bg.alpha = 1;
				posterOpenTime = getTimer();
			}
			else 
			{
				tweenToRotated();
				posterUi.inWorld.bg.alpha = 0.75;
				var deltaTime:Number = getTimer() - posterOpenTime;
				
				// If the poster has loaded, and was viewed for more than 1/10 second
				if(deltaTime > 100)
					Model.dispatchTrackingEvent(
						Constants.GA_CATEGORY_CONTENT_ELEMENT,	
						"poster",
						posterData.title +"|"+
						posterData.id +"|"+
						Model.currentScene.id, 
						deltaTime
					);			
			}
			
		}

		
		private function onTweenToFlatComplete():void
		{
			var ui:PosterContainerFl = posterUi;
			ui.inWorld.closeButton.visible = true;
			ui.inWorld.viewButton.visible = posterData.hasButton;
			
			ui.icon.visible = false;
			
			if(posterData.showIcon)
			{
				ui.icon.visible = false;
				ui.inWorld.visible = true;
				ui.inWorld.alpha = 0;
				Tweener.addTween(ui.inWorld,
					{
						time:0.5,
						alpha:1
					}
				);
			}
		}
		
		private function tweenToRotated():void
		{
			var ui:PosterContainerFl = posterUi;
			ui.inWorld.closeButton.visible = false;
			ui.inWorld.viewButton.visible = false;
			if(posterData.showIcon)
			{
				ui.icon.visible = true;
				Tweener.addTween(ui.inWorld,
					{
						time:0.5,
						alpha:0,
						onComplete:onHideElementsComplete
					}
				);
			}
		}
		
		private function onHideElementsComplete():void
		{
			if(posterUi)
				posterUi.inWorld.visible = false;
		}
		
		public override function dispose():void
		{
			super.dispose();
		}
	}
}