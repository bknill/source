package co.beek.pano.hotspot
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	import flash.utils.flash_proxy;
	import flash.utils.setTimeout;
	
	import spark.primitives.Rect;
	
	import away3d.materials.TextureMaterial;
	import away3d.textures.BitmapTexture;
	
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.pano.Pano;
	import co.beek.utils.BitmapUtil;
	
	public class VideoPlayer extends Hotspot
	{
		private var frames:Dictionary = new Dictionary;
		
		private var scale9Frame:Sprite = new Sprite;
		
		private var frameExtraHeight:Number;
		private var frameExtraWidth:Number;
		
		private var flvPlayer:Video;
		
		private var netConnection:NetConnection;
		
		private var netStream:NetStream;
	
		private var isVideoPaused:Boolean;
		
		private var previewBitmap:Bitmap;
		
		private var youtubePlayer:Object;
		
		
		public function VideoPlayer(data:VideoData, ui:TelevisionContainerFl)
		{
			super(data, ui);
			
			frameExtraHeight = ui.inWorld.frameNone.height - ui.inWorld.videoLocator.height;
			frameExtraWidth = ui.inWorld.frameNone.width - ui.inWorld.videoLocator.width;
			
			// hide redundant items 
			ui.spinner.visible = false;
			ui.inWorld.playButton.visible = false;
			ui.inWorld.removeChild(ui.inWorld.videoLocator);
			
			// Store the frames in a n array for selecting
			frames[VideoData.FRAME_NONE] = ui.inWorld.frameNone;
			frames[VideoData.FRAME_PROJECTOR] = ui.inWorld.frameProjector;
			frames[VideoData.FRAME_WALL_TV] = ui.inWorld.frameWallTV
			frames[VideoData.FRAME_TV] = ui.inWorld.frameTv;
			for each(var frame:Sprite in frames)
				ui.inWorld.removeChild(frame);
			
			BitmapUtil.drawAndScale9Grid(frames[data.frame], scale9Frame);
			//scale9Frame.x = ui.inWorld.x;
			//scale9Frame.y = ui.inWorld.y;
			ui.inWorld.addChildAt(scale9Frame, 0);
			
			data.addEventListener(Event.CHANGE, onDataChange);
			Model.state.addEventListener(State.VOLUME_CHANGE, onVolumeChange);
			
			if(videoData.usesYoutube)
				ImageLoader.load(fileUrl, onPreviewLoaded, onPreviewLoadError);
			
			else if(videoData.preview)
				ImageLoader.load(previewUrl, onPreviewLoaded, onPreviewLoadError);
			
			else if(videoData.file && Model.config.device == Constants.WEB)
				setTimeout(playFlvFile, 1000);// Only on the web, Delay videos, cause they hog cpu/bandwidth
			
			ui.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			
			setInworldBlank();
		}
		
		private function get videoUI():TelevisionContainerFl
		{	return TelevisionContainerFl(_ui) }
		
		private function get videoData():VideoData
		{	return VideoData(_data) }
		
		private function get fileUrl():String
		{	return Model.config.getAssetUrl(videoData.file.realName) }

		private function get previewUrl():String
		{	return Model.config.getAssetUrl(videoData.preview) }
		
		private function onPreviewLoaded(data:BitmapData):void
		{
			previewBitmap = new Bitmap;
			previewBitmap.bitmapData = data;
			previewBitmap.x = videoUI.inWorld.videoLocator.x;
			previewBitmap.y = videoUI.inWorld.videoLocator.y;
			
			// make the inworld bitmap exactly 256 wide
			previewBitmap.width = 512 - frameExtraWidth;
			previewBitmap.scaleY = previewBitmap.scaleX;
			
			scale9Frame.width = 512;
			scale9Frame.height = Math.ceil(previewBitmap.height + frameExtraHeight);
			
			videoUI.inWorld.playButton.visible = true;
			videoUI.inWorld.playButton.x = previewBitmap.x + (previewBitmap.width - videoUI.inWorld.playButton.width)/2;
			videoUI.inWorld.playButton.y = previewBitmap.y + (previewBitmap.height - videoUI.inWorld.playButton.height)/2;
			videoUI.inWorld.addChildAt(previewBitmap, videoUI.inWorld.getChildIndex(videoUI.inWorld.playButton));
			
			if(!_hidden)
				inworldSprite = videoUI.inWorld;
		}
		
		private function onPreviewLoadError():void
		{
			trace("Preview failed to load");
		}
		
		private function onDataChange(event:Event):void
		{
			BitmapUtil.drawAndScale9Grid(frames[videoData.frame], scale9Frame);
			inworldSprite = videoUI.inWorld;
		}
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
			if(value)
			{
				setInworldBlank();
			}
			else if(netStream)
			{
				inworldSprite = videoUI.inWorld;
				netStream.play(fileUrl);
			}
		}
		
		/**
		 * Creates and starts up a net netConnection.
		 */
		private function startNetConnection():void 
		{
			//Start up the net netConnection.
			netConnection = new NetConnection();
			netConnection.addEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
			netConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			netConnection.connect(null);
		}
		
		/**
		 * Play the video.
		 */
		private function playFlvFile():void
		{
			//trace("playFlvFile()");
			if(!videoUI)
				return;
			
			flvPlayer = new Video;
			flvPlayer.x = videoUI.inWorld.videoLocator.x;
			flvPlayer.y = videoUI.inWorld.videoLocator.y;
			videoUI.inWorld.addChildAt(flvPlayer, videoUI.inWorld.getChildIndex(videoUI.inWorld.playButton));
			
			
			netConnection = new NetConnection()
			netConnection.addEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
			netConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			netConnection.connect(null);
			
			if(!_hidden && videoData.file)
			{
				//visible = true;
				netStream.play(fileUrl);
				updateVolume();
			}
		}
		
		private function onNetConnectionStatus(event:NetStatusEvent):void 
		{
			//trace("VideoPlayer.onNetConnectionStatus:"+event.info.code);
			if(event.info.code == "NetConnection.Connect.Success")
				connectStream();
		}
		
		
		private function connectStream():void 
		{
			netStream = new NetStream(netConnection);
			netStream.client = this;
			netStream.bufferTime = 9;
			netStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStreamStatus);
			netStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			flvPlayer.attachNetStream(netStream);
			
			if(videoData.file)
				netStream.play(fileUrl);
		}
		
		private function onNetStreamStatus(event:NetStatusEvent):void 
		{
			//trace("VideoPlayer.onNetStreamStatus:"+event.info.code);
			switch (event.info.code) 
			{
				case "NetStream.Play.StreamNotFound":
					//trace("Stream not found: " + fileUrl);
					break;
				case "NetStream.Buffer.Full":
					// starts playing
					if(previewBitmap && previewBitmap.parent == videoUI.inWorld)
						videoUI.inWorld.removeChild(previewBitmap);
					_ui.addEventListener(Event.ENTER_FRAME, onEnterFrame);
					break;
				case "NetStream.Buffer.Empty":
					//trace("NetStream.Buffer.Empty");
					break;
				//loops the video
				case "NetStream.Play.Stop":
					if(videoData.looping)
						netStream.seek(0);
					break;
				case "NetStream.Play.Complete":
					//trace("NetStream.Play.Complete");
					Model.videoComplete(videoData)
					break;
			}
		}
		
		/**
		 * Callback called from netstream
		 */
		public function onMetaData(data:Object):void
		{
			trace("VideoPlayer.onMetaData: " + data);
			resizeToVideo(data.width, data.height);
			
			var bm:BitmapData = new BitmapData(scale9Frame.width, powerOf2Up(scale9Frame.height), true, 0x00000000);
			bm.draw(videoUI.inWorld);
			
			inworldBitmap = bm;
			
			// Trying to correct the inworld frame, but who cares.
			//var scale:Number = inworldBitmap.width/videoUI.inWorld.width;
			//var scaledHeight:Number = videoUI.inWorld.height * scale;
			
			// Save a reference for faster access on enter frame
			videoRect = flvPlayer.getRect(_ui);
			//videoRect.y = (inworldBitmap.height-scaledHeight)/2;
			
			videoTexture = BitmapTexture(TextureMaterial(material).texture);
			videoBitmap = videoTexture.bitmapData;
		}
		
		
		private function resizeToVideo(streamWidth:Number, streamHeight:Number):void
		{
			/*if(streamWidth > Pano.instance.width - 700)
			{
				streamHeight = (Pano.instance.width - 700)/streamWidth * streamHeight;
				streamWidth = Pano.instance.width - 700;
			}

			if(streamHeight > Pano.instance.height - 250)
			{
				streamWidth = (Pano.instance.height - 250)/streamHeight * streamWidth;
				streamHeight = Pano.instance.height - 250;
			}*/
			
			var totalWidth:Number = streamWidth + frameExtraWidth;
			var totalHeight:Number = streamHeight + frameExtraHeight;
			
			var w:Number = powerOf2Down(totalWidth);
			
			var scale:Number = Math.min(w/totalWidth);//, h/totalHeight);
			
			scale9Frame.width = w;
			scale9Frame.height = totalHeight*scale;
			
			flvPlayer.width = w - frameExtraWidth;
			flvPlayer.height = scale9Frame.height - frameExtraHeight;
			
			videoUI.closeButton.x = scale9Frame.width - videoUI.closeButton.width - frameExtraWidth/2;
			
			
		}
		
		private var videoRect:Rectangle = new Rectangle(10, 10, 200, 200);
		
		private var videoTexture:BitmapTexture;
		
		private var videoBitmap:BitmapData;
		
		private function onEnterFrame(event:Event):void
		{
			updateInworld();
		}
		
		private function updateInworld():void
		{
			videoBitmap.lock();
			videoBitmap.fillRect(videoRect, 0);
			videoBitmap.draw(_ui, null, null, null, videoRect);
			videoBitmap.unlock();
			
			videoTexture.invalidateContent();
		}

		public function onXMPData(data:Object):void
		{
			//trace("onXMPData: " + data);
		}

		public function onCuePoint(data:Object):void
		{
			//trace("onCuePoint: " + data);
		}

		public function onPlayStatus(data:Object):void
		{
			//trace("onPlayStatus: " + data);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void 
		{
			//trace("securityErrorHandler: " + event);
		}
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void 
		{
			// ignore AsyncErrorEvent events.
			//trace("AsyncErrorEvent:"+event.error.message)
		}
		
		public override function set pan(value:Number):void
		{
			if(_pan == value)
				return;
			_pan = value;
			
			
			updateVolume();
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			
			if(value)
				showSelected();
			else
				showInWorld();
		}
		
		protected function showSelected():void{
			videoUI.inWorld.playButton.visible = false;
			if(videoData.usesYoutube)
				playYouTubeVideo();
			
			else if(netStream)
				netStream.resume();
			
			else if(videoData.preview)
				playFlvFile();
		}
		
		
		private function playYouTubeVideo():void 
		{ 
			if(youtubePlayer != null)
			{
				youtubePlayer.playVideo();
			}
			else
			{
				videoUI.spinner.visible = true;
				videoUI.inWorld.playButton.visible = false;
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit, false, 0, true);
				loader.load(new URLRequest("http://www.youtube.com/apiplayer?version=3"));
			}
		}
		
		private function onLoaderInit(event : Event) : void {
			var loaderInfo:LoaderInfo = LoaderInfo(event.target);
			loaderInfo.removeEventListener(Event.INIT, onLoaderInit);
			
			youtubePlayer = loaderInfo.content; 
			youtubePlayer.x = videoUI.inWorld.videoLocator.x;
			youtubePlayer.y = videoUI.inWorld.videoLocator.y;
			youtubePlayer.visible = false;
			youtubePlayer.addEventListener("onReady", onPlayerReady, false, 0, true);
			youtubePlayer.addEventListener("onStateChange", onStateChange, false, 0, true);
			
			//var index:int = scale9Frame.getChildIndex(bitmap)+1;
			videoUI.inWorld.addChildAt(DisplayObject(youtubePlayer), videoUI.inWorld.getChildIndex(videoUI.inWorld.playButton));
			
			videoUI.spinner.visible = true;
			videoUI.setChildIndex(videoUI.spinner, videoUI.numChildren-1);
		}
		
		private function onPlayerReady(event : Event) : void {
			youtubePlayer.removeEventListener("onReady", onPlayerReady);
			// Once this event has been dispatched by the player, we can use
			// cueVideoById, loadVideoById, cueVideoByUrl and loadVideoByUrl
			// to load a particular YouTube video.  
			youtubePlayer.setSize(previewBitmap.width, previewBitmap.height);
			youtubePlayer.loadVideoById(videoData.videoId);
			youtubePlayer.setLoop(videoData.looping);
		}
		
		private function onStateChange(event : Event) : void {
			if(youtubePlayer.getPlayerState() == 1)
			{
				youtubePlayer.removeEventListener("onStateChange", onStateChange);
				youtubePlayer.visible = true;
				videoUI.spinner.visible = false;
			}
		}
		
		protected function showInWorld():void
		{
			//ui.spinner.visible = false;
			
			if(youtubePlayer)
			{
				youtubePlayer.seekTo(0);
				youtubePlayer.pauseVideo();
				youtubePlayer.visible = false;
			}
			else if(videoData && videoData.preview)
			{
				_ui.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				if(netStream)
					netStream.pause();
				
				videoUI.inWorld.playButton.visible = true;
				inworldSprite = videoUI.inWorld;
			}
		}
		
		public override function dispose():void
		{
			Model.state.removeEventListener(State.VOLUME_CHANGE, onVolumeChange);
			
			_ui.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			if(previewBitmap)
			{
				previewBitmap.bitmapData = null;
				previewBitmap = null;
			}
			
			if(flvPlayer)
			{
				flvPlayer.clear();
				flvPlayer = null;
			}
			
			if(netStream)
			{
				netStream.close();
				netStream.removeEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
				netStream.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
				netStream.client = {};
				netStream = null;
			}
			
			if(netConnection)
			{
				netConnection.removeEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
				netConnection.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
				netConnection.close();
				netConnection = null;
			}
			
			if(youtubePlayer)
			{
				videoUI.inWorld.removeChild(youtubePlayer as DisplayObject);
				youtubePlayer.removeEventListener("onStateChange", onStateChange);
				youtubePlayer.removeEventListener("onReady", onPlayerReady);
				youtubePlayer = null;
			}
			
			super.dispose();
		}
		
		private function updateVolume():void
		{
			if(!netStream)
				return;
			
			var soundPan:Number = videoData.panoPos.pan;
			
			// Number between 0-180 that dictates the volume change with panning 
			var deltaRaw:Number = (Math.max(_pan, soundPan) - Math.min(_pan, soundPan));
			var deltaPan:Number = (deltaRaw > 180) ? 180 - deltaRaw%180 : deltaRaw;
			
			// Number between 0-1 that dictates how panning is affecting the volume
			var panVolume:Number = (1 - deltaPan/180) * videoData.ambience;
			
			// Final compiled volume.
			var volume:Number = Model.state.volume * videoData.volume * panVolume;
			
			// The amout the sound is offset left and right
			// when deltaPan is low, close to 0;
			var panning:Number = deltaRaw < 180 && _pan > soundPan 
				? -(deltaPan/180) * (1 - volume)
				: (deltaPan/180) * (1 - volume);
			
			////trace("panning: "+panning);
			////trace("panning:"+panning+" volume:"+volume+" panVolume:"+panVolume+" ambientVolume:"+ambientVolume+" Model.volume:"+Model.volume+"")
			netStream.soundTransform = new SoundTransform(volume, panning);
		}
		
		// sets the volume based on the slider 	
		private function onVolumeChange(event:Event):void
		{
			updateVolume();
		}
		
		// sets the volume based on the slider 	
		private function onCloseButtonClick(event:MouseEvent):void
		{
			Model.hotspot = null;
		}
	}
}