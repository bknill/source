package co.beek.pano
{
	import com.chargedweb.utils.MatrixUtil;
	
	import co.beek.model.Model;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	public class TransitionBitmap extends Bitmap
	{
		private static const rl:Number = 0.3086;
		private static const gl:Number = 0.6094;
		private static const bl:Number = 0.0820;
		private static const blackAndWhite:ColorMatrixFilter = new ColorMatrixFilter([
			rl, gl, bl, 0,
			0,  rl, gl, bl,
			0,  0,  rl, gl,
			bl, 0,  0,  0,
			0,  0,  1,  0
		]);
		
		/** @private **/
		protected static var _idMatrix:Array = [1,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0];
		/** @private **/
		protected static var _lumR:Number = 0.212671; //Red constant - used for a few color matrix filter functions
		/** @private **/
		protected static var _lumG:Number = 0.715160; //Green constant - used for a few color matrix filter functions
		/** @private **/
		protected static var _lumB:Number = 0.072169; //Blue constant - used for a few color matrix filter functions
		
		/** @private **/
		public static function setSaturation(m:Array, n:Number):Array {
			if (isNaN(n)) {
				return m;
			}
			var inv:Number = 1 - n;
			var r:Number = inv * _lumR;
			var g:Number = inv * _lumG;
			var b:Number = inv * _lumB;
			var temp:Array = [
				r + n, g    , b    , 0, 0,
				r    , g + n, b    , 0, 0,
				r    , g    , b + n, 0, 0,
				0    , 0    , 0    , 1, 0
			];
			return applyMatrix(temp, m);
		}
		
		public static function applyMatrix(m:Array, m2:Array):Array {
			if (!(m is Array) || !(m2 is Array)) {
				return m2;
			}
			var temp:Array = [], i:int = 0, z:int = 0, y:int, x:int;
			for (y = 0; y < 4; y += 1) {
				for (x = 0; x < 5; x += 1) {
					if (x == 4) {
						z = m[i + 4];
					} else {
						z = 0;
					}
					temp[i + x] = m[i]   * m2[x]      + 
						m[i+1] * m2[x + 5]  + 
						m[i+2] * m2[x + 10] + 
						m[i+3] * m2[x + 15] +
						z;
				}
				i += 5;
			}
			return temp;
		}
		
		public static const BLUR_COMPLETE:String = "BLUR_COMPLETE";
		
		public static const FADE_COMPLETE:String = "FADE_COMPLETE";
		
		private static const origin:Point = new Point();
		private var blurTimeout:uint;
		private var _blur:int;
		private var _blurring:Boolean;
		private var _saturation:Number;
		private var _brightness:Number;

		private var _burnFactor:Number;
		private var burnMatrix:Matrix = new Matrix;
		
		private var original:BitmapData;
		
		private var blurFilter:BlurFilter = new BlurFilter(0, 0, BitmapFilterQuality.LOW);
		
		private var blackAndWhiteFilter:ColorMatrixFilter = new ColorMatrixFilter();
		
		public function TransitionBitmap()
		{
			super();
		}
		
		public override function set bitmapData(value:BitmapData):void
		{
			super.bitmapData = value;
			_blur = 0;
			_saturation = 1;
			_brightness = 0;
			_blurring = false;
		}
		
		/*public function get bitmapData():BitmapData
		{
			return bitmapData;
		}*/
		private var blurTime:uint;
		
		public function blurSlowly(seconds:Number, blackAndWhite:Boolean = true):void
		{
			trace("TransitionBitmap.blurSlowly(seconds:"+seconds+");");
			if(!bitmapData)
				return;
			
			_blurring = true;
			Tweener.removeTweens(this);
			Tweener.addTween(this, {
				"time":seconds, 
				"blur":3, 
				"brightness" : -3,
				"transition":"linear",
				"onComplete" : onBlurComplete
			});
			
			blurTime = getTimer();
		}
		
		/*private function onBlurEnterFrame(event:Event):void
		{
			
		}*/


		public function get blurring():Boolean
		{
			return _blurring;
		}
		public function get blur():int
		{
			return _blur;
		}
		
		public function set blur(value:int):void
		{
			//trace("TransitionBitmap.set blur(): milis: "+ (getTimer()-fadeTime));
			if(_blur == value || bitmapData == null)
				return;
			
			//trace("set blur(value:"+value+")");
			_blur = value;
			
			
			blurFilter.blurX = blurFilter.blurY = _blur;
			bitmapData.applyFilter(bitmapData, bitmapData.rect, origin, blurFilter);
		}

		public function get saturation():Number
		{
			return _saturation;
		}
		
		public function set saturation(value:Number):void
		{
			value = Math.round(value*10)/10;
			if(_saturation == value || bitmapData == null)
				return;
			
			//trace("set saturation(value:"+value+")");
			_saturation = value;
			
			blackAndWhiteFilter.matrix = setSaturation(_idMatrix.slice(), saturation);
			bitmapData.applyFilter(bitmapData, bitmapData.rect, origin, blackAndWhiteFilter);
		}

		public function get brightness():Number
		{
			return _brightness;
		}
		
		public function set brightness(value:Number):void
		{
			value = Math.round(value*10)/10;
			if(_brightness == value || bitmapData == null)
				return;
			
			//trace("set brightness(value:"+value+")");
			
			_brightness = value;
			bitmapData.applyFilter(bitmapData, bitmapData.rect, origin, MatrixUtil.setBrightness(_brightness));
		}
			
		
		public function onBlurComplete():void
		{
			trace("TransitionBitmap.onBlurComplete(); millis: " + (getTimer() - blurTime) + " millis");
			_blurring = false;
			
			//if(!Model.hotspot)
			//	Tweener.addTween(this,{scaleX : 1.5,scaleY : 1.5, x : -this.width*0.33, y : -this.height*0.33, time : 10});
			dispatchEvent(new Event(BLUR_COMPLETE));
		}
		
		public function fadeAway(seconds:Number):void
		{
			trace("TransitionBitmap.fadeSlowly(seconds:"+seconds+");");
			alpha = 1;
			Tweener.removeTweens(this);
			Tweener.addTween(this, {
				"time" : seconds, 
				"alpha" : 0, 
				"transition" : "linear", 
				"onComplete" : fadeSlowlyComplete
			});
			
			fadeTime = getTimer();
			_blurring = false;
		}
		
		private var fadeTime:uint;	   
		
		public function fadeSlowlyComplete():void
		{
			trace("TransitionBitmap.fadeSlowlyComplete(); millis: "+(getTimer()-fadeTime));
			alpha = 1;
			_blurring = false;
			clear();
			dispatchEvent(new Event(FADE_COMPLETE));
		}
		
		public function clear():void
		{
			bitmapData = null;
			Tweener.removeTweens(this);
			this.scaleX = 1;
			this.scaleY = 1;
			this.x = 0;
			this.y = 0;
			_blur = 0;
			_blurring = false;
		}
	}
}