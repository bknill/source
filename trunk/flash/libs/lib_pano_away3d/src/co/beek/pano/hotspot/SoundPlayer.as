package co.beek.pano.hotspot
{
	import away3d.audio.Sound3D;
	import away3d.audio.drivers.RepeatPanVolumeDriver;
	import away3d.audio.drivers.SimplePanVolumeDriver;
	import away3d.cameras.Camera3D;
	import away3d.containers.ObjectContainer3D;
	import away3d.core.math.Vector3DUtils;
	import away3d.core.math.Matrix3DUtils;
	
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.SoundData;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	public class SoundPlayer extends Hotspot
	{
		private var soundPlayerUI:SoundPlayerFl;

		public var sound:Sound = new Sound;
		public var driver:SimplePanVolumeDriver = new SimplePanVolumeDriver();
		public var sound3D:Sound3D;
		public var ambiance:Number;
		private var channel:SoundChannel;
		
		
		public function SoundPlayer(data:SoundData, ui:SoundPlayerFl, listener:ObjectContainer3D)
		{
			super(data, ui);
			
			soundPlayerUI = ui;
			var url:String;
			
			data.addEventListener(Event.CHANGE, onSoundDataChange);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditingSceneChange);
			Model.state.addEventListener(State.VOLUME_CHANGE, onVolumeChange);
			
			if(data.preview)
				url = data.preview
			else
			    url = Model.config.getAssetUrl(data.file.realName);
			
			sound = new Sound();
			sound.addEventListener(IOErrorEvent.IO_ERROR, onSoundIOError);
			sound.load(new URLRequest(url));

			sound3D = new Sound3D(sound, listener, driver);
			sound3D.position = this.position;
			
			
			ambiance = data.ambience;
			updateVolume();

			sound3D.play();
			
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				inworldSprite = soundPlayerUI;
			else
				setInworldBlank();
		}
		
		private function onSoundDataChange(event:Event):void
		{
			updateVolume();
		}
		
		private function onEditingSceneChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				inworldSprite = soundPlayerUI;
			else
				setInworldBlank();
		}
		
		// sets the volume based on the slider
		private function onVolumeChange(event:Event):void
		{
			if(!channel)
				return;
			
			updateVolume();
		}
		
		public function get soundData():SoundData
		{    return SoundData(_data) }
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			
		}
		
		private function onSoundIOError(event:IOErrorEvent):void
		{
			//We could not load the sound
			trace("Sound failed to load");
		}
	
		private function updateVolume():void
		{
			driver.volume = soundData.volume * Model.state.volume;
		}
		
		public function update():void
		{
			sound3D.position = this.position;
		}
		
		public override function dispose():void
		{
			Model.state.removeEventListener(State.UI_STATE_CHANGE, onEditingSceneChange);
			Model.state.removeEventListener(State.VOLUME_CHANGE, onVolumeChange);
			
			// if we are still downloading
			if (sound != null && sound.bytesLoaded < sound.bytesTotal)
				sound.close();
			
			sound = null;
			sound3D.stop();
			

			
			super.dispose();
		}
	}
}