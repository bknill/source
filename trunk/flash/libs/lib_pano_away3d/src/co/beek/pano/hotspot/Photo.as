package co.beek.pano.hotspot
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.utils.clearTimeout;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	

	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.service.VisitService;
	import co.beek.utils.BrowserUtil;
	
	public class Photo extends Hotspot
	{
		private var holder:Sprite = new Sprite;
		
		private var pivot:MovieClip = new MovieClip;
		
		private var bitmap:Bitmap = new Bitmap(null, "auto", true);

		private var transition:Bitmap = new Bitmap(null, "auto", true);
		
		private var photoFl:PhotoContainerFl;
		
		private var photoOpenTime:Number;
		
		private var pageIndexes:Array = [];
		private var pages:Array = [];
		
		private var scrollIndex:int;
		
		public function Photo(data:PhotoData, ui:PhotoContainerFl)
		{
			super(data, holder);
			trace('Photo('+data+ui+')');
			photoFl = ui;

			pivot.addChild(transition)
			holder.addChild(pivot);
			
			ui.inWorld.photoLocator.visible = false;
			bitmap.x = ui.inWorld.photoLocator.x;
			bitmap.y = ui.inWorld.photoLocator.y;
			ui.inWorld.addChildAt(bitmap, 2);

			ui.info.x = ui.inWorld.x;
			ui.info.y = ui.inWorld.y;
			ui.info.visible = false;
			
			ui.info.backButton.mouseChildren = false;
			ui.info.backButton.addEventListener(MouseEvent.CLICK, onBackButtonClick);
			
			ui.info.closeButton.mouseChildren = false;
			ui.info.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
	
			
			ui.info.viewButton.mouseChildren = false;
			ui.info.viewButton.addEventListener(MouseEvent.CLICK, onViewButtonClick);

			ui.info.nextButton.mouseChildren = false;
			ui.info.nextButton.addEventListener(MouseEvent.CLICK, onNextButtonClick);

			ui.info.prevButton.mouseChildren = false;
			ui.info.prevButton.addEventListener(MouseEvent.CLICK, onPrevButtonClick);
			
			data.addEventListener(Event.CHANGE, onDataChanged);
			data.addEventListener(PhotoData.MASK_CHANGED, onMaskChanged);
			data.addEventListener(PhotoData.FLIPPED_CHANGE, onFlippedChanged);
			
			if(photoData.file)
			{
				var url:String = Model.config.getAssetUrl(photoData.file.realName);
				
				///if(Model.config.domain.indexOf("beekdev") > -1)
				//	url = "http://beekdev.co/cdn/"+photoData.file.realName;
				
				ImageLoader.load(url, onPhotoLoaded, onPhotoLoadError);
			}
			
			resizeInfoPanel(photoFl.inWorld.bg.width, photoFl.inWorld.bg.height);
			
			updatePanelToData();
			setInworldBlank();
			
			//inworldSprite = photoFl.inWorld.bg;
		}

		private function onEditingSceneChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				inworldSprite = photoFl.inWorld;
			
			else if(_hidden)
				setInworldBlank();
			
			else
				inworldSprite = photoFl.inWorld;
		}
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
			if(value)
				setInworldBlank();
			
			else if(bitmap.bitmapData)
				inworldSprite = photoFl.inWorld;
		}
		
		private var _flipped:Boolean;
		
		private function onInfoButtonClick(event:MouseEvent):void
		{
			event.stopPropagation();
			clearTimeout(flipTimeout);
			photoData.flipped = true;
		}
		
		private function onBackButtonClick(event:MouseEvent):void
		{
			transition.bitmapData = infoBitmap;
			performFlip(0);
			
			if(photoData.webUrl)
				closeWebView();
		}
		
		private function flipPhoto():void
		{
			transition.bitmapData = frontBitmap;
			performFlip(1);
		}
		
		private function performFlip(percent:Number):void
		{
			Tweener.addTween(this, {
				flipPercent: percent,
				time:1,
				transition:"easeinoutquad",
				onComplete: onFlipComplete
			});
			
			
			transition.x = -transition.bitmapData.width/2;
			pivot.x = transition.bitmapData.width/2;
			photoFl.addChild(pivot)
			
			photoFl.info.visible = false;
			photoFl.inWorld.visible = false;
			
			//var perspective:PerspectiveProjection = new PerspectiveProjection();
			//perspective.projectionCenter = new Point(-2000, 0);
			//pivot.perspectiveProjection = perspective;
		}
		
		private var _flipPercent:Number = 0;
		public function get flipPercent():Number
		{
			return _flipPercent;
		}
		
		public function set flipPercent(value:Number):void
		{
			_flipPercent = value;
			
			pivot.rotationY = value * 180;
			//transition.x = value * transition.bitmapData.width;
			
			flipped = (value > 0.75);// for some reson i cant figure out, perspecitve cant be fixed
		}
		
		private function set flipped(value:Boolean):void
		{
			if(_flipped == value)
				return;
			
			_flipped = value;
			
			if(_flipped)
				transition.bitmapData = infoBitmap;
			
			else
				transition.bitmapData = frontBitmap;
		}
		
		private function get frontBitmap():BitmapData
		{
			var bm:BitmapData = new BitmapData(photoFl.inWorld.width, photoFl.inWorld.height, true, 0x00000000);
			bm.draw(photoFl.inWorld);
			return bm;
		}
		
		private function get infoBitmap():BitmapData
		{
			var flipMatrix:Matrix = new Matrix();
			flipMatrix.scale(-1, 1);
			//flipMatrix.rotate(Math.PI);
			flipMatrix.translate(photoFl.info.width, 0);
			
			var bm:BitmapData = new BitmapData(photoFl.info.width, photoFl.info.height, false, 0xFFFFFF);
			bm.draw(photoFl.info, flipMatrix);
			
			return bm;
		}
		
		private function onFlipComplete():void
		{
			photoFl.inWorld.visible = !_flipped;
			photoFl.info.visible = _flipped;
			photoFl.removeChild(pivot);
			
			if(photoData.webUrl)
				showWebView();

		}
		
		private function showWebView():void
		{
			data.inWorldHeight = photoFl.info.height;
			data.inWorldWidth = photoFl.info.width;

			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_SHOW_WEB, data));
		}
		
		private function closeWebView():void
		{
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLOSE_WEB, data));
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			event.stopPropagation();
			Model.hotspot = null;
						
			if(photoData.webUrl)
				closeWebView();
		}
		
		private function onNextButtonClick(event:MouseEvent):void
		{
			pageScrollIndex += numLines;
			
		}

		private function onPrevButtonClick(event:MouseEvent):void
		{
			pageScrollIndex -= numLines+1;// some hack to fix a bug in showing pages.
		}
		
		private function onViewButtonClick(event:MouseEvent):void
		{
			if(!_selected)
				return;
			
			///if(Model.loggedIn)
			//	return;
			
			if(photoData.buttonScene)
				loadScene();
			
			else if(photoData.buttonUrl)
				openUrl();
			
			else if(photoData.buttonFavourite)
				favouriteThis();
		}
		
		/**
		 * This will get called on a timeout from the pano when the photo is not selectable
		 */
		public function get loadsScene():Boolean
		{
			return photoData.buttonScene != null; 	
		}
		
		private var loadSceneTimeout:uint;
		public function loadSceneDelayed(delay:int):void
		{
			loadSceneTimeout = setTimeout(loadScene, delay);
		}
		
		private function loadScene():void
		{
			if(Model.state.uiState != State.UI_STATE_EDITING_SCENE)
				Model.state.loadingScene = photoData.buttonScene;
			
			//BrowserUtil.navigate(photoData.buttonUrl, BrowserUtil.BLANK);
			/*Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_CONVERSION,	
				"content_element_button", 
				photoData.title +"|"+
				photoData.id +"|"+
				Model.currentScene.id);*/

		}
		
		private function clearHotspot():void
		{
			Model.hotspot = null;
		}
		
		
		private function favouriteThis():void
		{
			VisitService.favourite(photoData.title);
		}
		
		private function openUrl():void
		{
			if(photoData.buttonUrl == null)
				return;
			
			BrowserUtil.navigate(photoData.buttonUrl, BrowserUtil.BLANK);
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_CONVERSION,	
				"content_element_button", 
				 photoData.title +"|"+
				 photoData.id +"|"+
				 Model.currentScene.id);
			
		}
		
		public function get photoData():PhotoData
		{	return PhotoData(_data) }
		
		private function onDataChanged(event:Event):void
		{
			updatePanelToData();
			inworldSprite = photoFl.inWorld;
		}
		
		private function onFlippedChanged(event:Event):void	
		{
			if(_selected && photoData.flipped)
				flipPhoto();
		}
		
		private function onMaskChanged(event:Event):void	
		{
			//drawPhotoMask(bitmap.width/rawBitmapData.width);
		}
		
		
		private var photomask:Sprite = new Sprite;
		private var photomaskOutline:Sprite = new Sprite;
		
		/**
		 * Draws the user created mask around the photo
		 */
		private function drawPhotoMask(scale:Number):void
		{
			photomask.graphics.clear();
			
			var points:Vector.<Point> = photoData.mask;
			if(points.length == 0)
			{
				bitmap.mask = null;
				return;
			}
			
			if(photoData.outline)
				photomaskOutline.graphics.lineStyle(2, 0xFFFFFF);
			
			if(photoData.fill)// && !viewing)
				photomaskOutline.graphics.beginFill(0, 0.5);
			
			photomask.graphics.beginFill(0, 1);
			photomaskOutline.graphics.moveTo(points[0].x * scale, points[0].y* scale);
			photomask.graphics.moveTo(points[0].x * scale, points[0].y* scale);
			for(var i:int=1; i<points.length; i++)
			{
				photomask.graphics.lineTo(points[i].x* scale, points[i].y* scale);
				photomaskOutline.graphics.lineTo(points[i].x* scale, points[i].y* scale);
			}
			
			photomask.x = bitmap.x;
			photomask.y = bitmap.y;
			bitmap.mask = photomask;
			
			photoFl.inWorld.addChild(photomask);
			photoFl.inWorld.addChild(photomaskOutline);
			photoFl.inWorld.bg.visible = false;
		}
		
		private function onPhotoLoadError():void
		{
			trace("Photo failed to load");
		}
		
		private var inworldScale:Number;
		
		private function onPhotoLoaded(data:BitmapData):void
		{
			// If the photo was disposed before it loaded
			if(photoData == null)
				return;
			
			bitmap.bitmapData = data;
			
			inworldScale = Math.min(1,
				photoFl.inWorld.photoLocator.width/data.width
			);
			
			photoScale = inworldScale;
			
			//photoFl.info.x = photoFl.inWorld.width
			//resizeInfoPanel(photoFl.inWorld.bg.width, photoFl.inWorld.bg.height);
			
/*			photoFl.inWorld.infoButton.y = 40;
			photoFl.inWorld.infoButton.x = 40;	
			photoFl.inWorld.infoButton.visible = false;*/
			
			if(!_hidden)
				inworldSprite = photoFl.inWorld;
		}
		
		private var _photoScale:Number;
		
		public function get photoScale():Number
		{
			return _photoScale;
		}
		
		public function set photoScale(value:Number):void
		{
			_photoScale = value;
			
			
			var locator:Sprite = photoFl.inWorld.photoLocator;
			
			var paddingWidth:Number = photoFl.inWorld.bg.width - locator.width;
			var paddingHeight:Number = photoFl.inWorld.bg.height - locator.height;
			
			if(bitmap.bitmapData)
			{
				bitmap.width = locator.width = bitmap.bitmapData.width * _photoScale;
				bitmap.height = locator.height = bitmap.bitmapData.height * _photoScale;
			}
			photoFl.inWorld.bg.width = bitmap.width + paddingWidth;
			photoFl.inWorld.bg.height = bitmap.height + paddingHeight;
			
			var deltaScale:Number = _photoScale - inworldScale;
			photoFl.x = -photoFl.inWorld.bg.width/2;
			photoFl.y = -photoFl.inWorld.bg.height/2;
			
			if(photoData.mask && photoData.mask.length > 0)
				drawPhotoMask(_photoScale);
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			
			// if it was waiting to load the scene and was deselected
			clearTimeout(loadSceneTimeout);
			
			if(value)
			{
				holder.addChild(photoFl);
				//photoFl.info.visible = true;
				//photoFl.inWorld.visible = false;
				//photoFl.inWorld.infoButton.visible = true;
				
				if(bitmap.bitmapData)
				{
					var scaleFactor = Model.isPlayingGame ? 0.5 : 0.8;
					
					bitmap.smoothing = false;
					var viewingScale:Number = Math.min(
						(photoFl.stage.stageWidth * scaleFactor)/bitmap.bitmapData.width,
						(photoFl.stage.stageHeight * 0.8)/bitmap.bitmapData.height
					);
					Tweener.addTween(this, {
						"time" : 2,
						"photoScale" : viewingScale,
						"onComplete" : onScaleUpComplete
					});
				}
				else
				{
					updatePanelToData();
				}
			}
			else if(photoFl.parent == holder)
			{
				holder.removeChild(photoFl);
				clearTimeout(flipTimeout);
				
			/*	photoFl.inWorld.infoButton.x = 40;
				photoFl.inWorld.infoButton.y = 40;
				photoFl.inWorld.infoButton.visible = false;
*/
				photoFl.inWorld.visible = true;
				photoFl.info.visible = false;
				
				
				photoData.flipped = false;
				flipPercent = 0;
				photoScale = inworldScale;
				//resizeInfoPanel(photoFl.inWorld.bg.width, photoFl.inWorld.bg.height);
				
				var deltaTime:Number = getTimer() - photoOpenTime;
				
				// If the photo has loaded, and was viewed for more than 1/10 second
				if(photoData.file && bitmap.bitmapData && deltaTime > 100)
					Model.dispatchTrackingEvent(
						Constants.GA_CATEGORY_CONTENT_ELEMENT,	
						"photo",
						photoData.title +"|"+
						photoData.id +"|"+
						Model.currentScene.id, 
						deltaTime
					);	
				

			}
		}
		
		private var flipTimeout:uint;
		
		
		public function onScaleUpComplete():void
		{
			/*photoFl.inWorld.infoButton.x = photoFl.inWorld.bg.x + photoFl.inWorld.bg.width - photoFl.inWorld.infoButton.width - 15;
			photoFl.inWorld.infoButton.y = photoFl.inWorld.bg.y + 12;
			photoFl.inWorld.infoButton.visible = photoData.canFlip;
		*/	
			bitmap.smoothing = true;
			
			resizeInfoPanel(photoFl.inWorld.bg.width, photoFl.inWorld.bg.height);
			updatePanelToData();
			
			if(photoData.description.length > 1 || photoData.webUrl)
				flipTimeout = setTimeout(flipOnTimeout, 500);
			
		}
		
		private function flipOnTimeout():void
		{
			photoData.flipped = true;
		}
		
		
		private function updatePanelToData():void
		{
			photoFl.inWorld.bg.visible = photoData.frame != PosterData.FRAME_NONE;

			if(photoData.hasButton){
				photoFl.info.viewButton.visible = true;
				photoFl.info.viewButton.textField.autoSize = "left";
				photoFl.info.viewButton.bg.width = photoFl.info.viewButton.textField.width * 1.1;
				if(photoFl.info.viewButton.bg.width > photoFl.info.width - 10)
					photoFl.info.viewButton.bg.width = photoFl.info.width - 10;
				photoFl.info.viewButton.x = photoFl.info.width/2 - photoFl.info.viewButton.width/2
			}
			else
				photoFl.info.viewButton.visible = false;
			

				Fonts.format(photoFl.info.viewButton.textField, Fonts.bodyFont, buttonLabel, true);	
				Fonts.format(photoFl.info.nextButton.textField, Fonts.bodyFont, "next", true);	
				Fonts.format(photoFl.info.prevButton.textField, Fonts.bodyFont, "prev", true);
				
				var scrollBefore:int = pageScrollIndex;
				if(photoData.description)
					Fonts.formatHTML(photoFl.info.descriptionField, Fonts.bodyFont, photoData.description);
				var required:int = photoFl.info.descriptionField.numLines % numLines;
				var concatenate:String = "";
				for(var i:int; i<required; i++)
					concatenate += "<BR />";
				
				photoFl.info.descriptionField.htmlText += concatenate;
				pageScrollIndex = scrollBefore;
				photoFl.info.prevButton.visible = scrollBefore > 1;
				photoFl.info.nextButton.visible = photoFl.info.descriptionField.numLines > numLines;
				photoFl.info.viewButton.textField.width = photoFl.info.viewButton.textField.textWidth + 10;
		
		}
		
		private function get buttonLabel():String
		{
			if(!photoData.buttonLabel)
				return "MORE INFO";
			
			var parts:Array = photoData.buttonLabel.split("|");
			if(photoData.buttonFavourite && parts.length == 2)
				return VisitService.hasBeenFavourited(photoData) ? parts[1] : parts[0];
			
			return photoData.buttonLabel;
		}
		
		private function get numLines():int
		{
			return (photoFl.info.descriptionField.bottomScrollV - photoFl.info.descriptionField.scrollV) + 1;
		}
		
		private function get pageScrollIndex():int
		{
			return photoFl.info.descriptionField.scrollV;
		}
		
		private function set pageScrollIndex(value:int):void
		{
			photoFl.info.descriptionField.scrollV = value;
			
			photoFl.info.nextButton.visible = value < photoFl.info.descriptionField.numLines-numLines;
			photoFl.info.prevButton.visible = value > 0;
		}
		
		private function resizeInfoPanel(w:Number, h:Number):void
		{
			photoFl.info.bg.width = w;
			photoFl.info.bg.height = h;
			
			photoFl.info.descriptionField.width = w - (photoFl.info.descriptionField.x * 2);
			photoFl.info.closeButton.x = w - photoFl.info.closeButton.width - 10;
			photoFl.info.backButton.x = photoFl.info.closeButton.x - photoFl.info.backButton.width - 10;
			photoFl.info.viewButton.y 
				= photoFl.info.prevButton.y 
				= photoFl.info.nextButton.y 
				= h - photoFl.info.viewButton.height - 20;

			photoFl.info.nextButton.x = w - photoFl.info.nextButton.width - 10;
			photoFl.info.prevButton.x = photoFl.info.nextButton.x - photoFl.info.prevButton.width - 10;
			
			photoFl.info.descriptionField.height = photoFl.info.viewButton.y - photoFl.info.descriptionField.y - 20;
			
			
			//photoFl.info.rotationY = 180;
			//photoFl.info.x = photoFl.info.width;
		}
		
		public override function dispose():void
		{
			photoData.removeEventListener(Event.CHANGE, onDataChanged);
			photoData.removeEventListener(PhotoData.MASK_CHANGED, onMaskChanged);
			
			photoFl.info.viewButton.removeEventListener(MouseEvent.CLICK, onViewButtonClick);
			
			//Model.removeEventListener(Model.HOTSPOT_CHANGE, onHotspotChange);
			Model.state.removeEventListener(Model.SESSION_CHANGE, onEditingSceneChange);
			
			bitmap.bitmapData = null;
			super.dispose();
		}
	}
}