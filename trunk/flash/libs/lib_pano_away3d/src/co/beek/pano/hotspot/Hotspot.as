package co.beek.pano.hotspot
{
	import away3d.entities.Mesh;
	import away3d.entities.Sprite3D;
	import away3d.library.assets.BitmapDataAsset;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.PlaneGeometry;
	import away3d.textures.BitmapTexture;
	
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.RssReaderData;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import flashx.textLayout.events.ModelChange;

	public class Hotspot extends Mesh
	{
		public static const HOTSPOT_CENTRED:String = "HOTSPOT_CENTRED";
		
		public static const HOTSPOT_OFF_CENTRE:String = "HOTSPOT_OFF_CENTRE";

		protected var _data:HotspotData;
		
		protected var _ui:DisplayObject;

		protected var _selected:Boolean;
		
		protected var _center:Boolean;

		protected var _hidden:Boolean;
		
		protected var _pan:Number;
		
		protected var _tilt:Number;
		
		private	var shCircle:Shape;
		private	var shFill:Shape;
		private	var shLine:Shape;
		private var spBoard:Sprite;
		
		public var autorotate:Boolean;
		
		private	var degree:Number = 0; // Initial angle
		private	var degChange:Number = 1; // Amount angle will change on each click
		private	var circleR:Number = 250; // Circle radius (in pixels)
		private	var circleX:Number = 256; // Screen coordinates of center of circle
		private	var circleY:Number = 256;
		private	var angleR:Number = circleR/4; // Radius of circular arc that illustrates the angle
		private var circleThickness:Number = 30;
		private var circleCircSteps: Number = 10;
		
		private	var inworldHotspotTimer:Sprite3D;
		private	var hotspotTimerBitmapData:BitmapData;
		private	var hotspotTimer:MovieClip = new MovieClip();;
		private	var timerBitmap:Bitmap;
		private	var timerHotspot:Hotspot;
		private	var bitmapTexture:BitmapTexture;
		private	var timerMaterial:TextureMaterial;
		private var timerCount:int;

		private var originalBitmap:BitmapData;
		public var bmData:BitmapData;
		
		private var timer:Timer = new Timer(1,180);

		
		private var blank:BitmapData = new BitmapData(2, 2, true, 0x00000000);
		
		public function Hotspot(data:HotspotData, ui:DisplayObject)
		{
			super(new PlaneGeometry(ui.width, ui.height, 1,1,true,true));
			this._data = data;
			this._ui = ui;
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditSceneChange);
			Model.state.addEventListener(State.PANO_ENGAGED,onPanoEngaged);
		}
		
		
		
		protected function setInworldBlank():void
		{
			inworldBitmap = blank;
		}
		
		public function get distanceRatio():int
		{
			return 2;
		}

		public function get transitionMillis():int
		{
			return 0;
		}
		
		/**
		 * Listens for the session change and draws the rectangle on the inworld bitmap
		 */
		private function onEditSceneChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
			{
				var color:uint = adminColor;
				if(color != 0xFFFFFF)
					drawBitmapBorder(color);
			}
			else if(originalBitmap)
			{
				BitmapTexture(TextureMaterial(material).texture).bitmapData = originalBitmap;
				BitmapTexture(TextureMaterial(material).texture).invalidateContent();
			}
		}
		
		
		public function get adminColor():uint
		{
			if(Model.isEditableScene && Model.currentScene.hotspots.indexOf(_data) > -1)
				return Constants.BEEK_BLUE;
			
			if(Model.isEditableGuide && Model.guideScene && Model.guideScene.hotspots.indexOf(_data) > -1)
				return Model.guideData.guideColor;
			
			return 0xFFFFFF;
		}
		
		private function drawBitmapBorder(color:uint):void
		{
			var bm:BitmapData = BitmapTexture(TextureMaterial(material).texture).bitmapData.clone();
			if(bm)
			{
				drawRectOnBitmap(bm, color);
				BitmapTexture(TextureMaterial(material).texture).bitmapData = bm;
				BitmapTexture(TextureMaterial(material).texture).invalidateContent();
			}
		}
		

		
		private function drawRectOnBitmap(bm:BitmapData, color:uint):void
		{
			if(originalBitmap == null)
				originalBitmap = bm.clone();
			
			var t:Sprite = new Sprite;
			t.graphics.lineStyle(1, color);
			t.graphics.drawRect(1, 1, bm.width-2, bm.height-2);
			
			bm.draw(t);
		}
		
		
		
		
		protected function get inworldBitmap():BitmapData
		{
			return BitmapTexture(TextureMaterial(material).texture).bitmapData;
		}
		
		/**
		 * Called by the videoplayer
		 */
		protected function set inworldBitmap(value:BitmapData):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
			{
				var color:uint = adminColor;
				if(color != 0xFFFFFF)
				{
					originalBitmap = value.clone();
					drawRectOnBitmap(value, color);
				}
			}
			
			
			
			PlaneGeometry(geometry).width = value.width;
			PlaneGeometry(geometry).height = value.height;
			
			var textureMaterial:TextureMaterial = new TextureMaterial(new BitmapTexture(value));
			textureMaterial.alphaBlending = true;
			this.material = textureMaterial;
		}

		
		/**
		 * Width is used to caclualte the scale
		 * Height is based of proportional scalaing, and next largest power of 2
		 */ 
		protected function set inworldSprite(value:DisplayObject):void
		{
			var w:Number = powerOf2Down(value.width);
			var scale:Number = w/value.width;
		
			
			var scaledHeight:Number = value.height * scale;
			var h:Number = powerOf2Up(scaledHeight);

			var bitmap:BitmapData = new BitmapData(w, h, true, 0x00000000);
			var m:Matrix = new Matrix(scale,0,0,scale, 0, (h-scaledHeight)/2)
			bitmap.draw(value, m);
			
			bmData = bitmap;
			
			inworldBitmap = bitmap;
		}
		
		protected function powerOf2Down(value:Number):int
		{
			if(value < 80) 
				return 64;
			else if(value < 150) 
				return 128;
			else if(value < 400) 
				return 256;
			else if(value < 800)
				return 512;
			
			return 1024;
		}

		protected function powerOf2Up(value:Number):int
		{
			if(value <= 64) 
				return 64;
			else if(value <= 128) 
				return 128;
			else if(value <= 256) 
				return 256;
			else if(value <= 512)
				return 512;
			
			return 1024;
		}
		
		public function get data():HotspotData
		{
			return _data;
		}

		public function get ui():DisplayObject
		{
			return _ui;
		}
		
		/**
		 * Function overridden in sub classes to update the hotspot of the current pan
		 */
		public function set pan(value:Number):void
		{
			_pan = value;
		}
		
		public function get pan():Number
		{
			return _pan;
		}
		
		public function set tilt(value:Number):void
		{
			_tilt = value;
		}
		
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
		}
		
		public function set center(value:Boolean):void
		{	
			if(_center == value)
				return;
		
			_center = value;	

			if(_center == true && Model.state.mode != State.MODE_AUTO
			&& !Model.loggedIn)
				 addHotspotTimer();
			else if(_center == false)
				removeTimerFromScene();
		}
		
		public function set hidden(value:Boolean):void
		{
			_hidden = value;
		}
		
		public function get hidden():Boolean
		{return _hidden}

		public function get scaleUniform():Number
		{return scaleX;}
		
		public function set scaleUniform(value:Number):void
		{scaleX = scaleY = scaleZ = value}
		
		public override function dispose():void
		{
			_ui = null;
			_data = null;
		}

		private function addHotspotTimer():void
		{
			if(!_center || Model.hotspot)
				return;
			
			spBoard = new Sprite;
			spBoard.x = circleX;
			spBoard.y = circleY;
			
			shFill = new Shape;
			shFill.x = 0;
			shFill.y = 0;
			shFill.rotationZ = 180;
			
			shLine = new Shape;
			shLine.x = 0;
			shLine.y = 0;

			shLine.graphics.beginFill(0xffffff);
			shLine.graphics.moveTo(-(circleR - circleThickness*2),0);
			shLine.graphics.drawRect(-(circleR - circleThickness/2),0,circleThickness*2,circleThickness);
			shLine.graphics.endFill();
			
			spBoard.addChild(shLine);
			spBoard.addChild(shFill);
			
			hotspotTimer.addChild(spBoard);
			addTimerToScene();
			
			timerCount = 0;
			updateTimer(timerCount);
		}
		
		private function addTimerToScene():void
		{
			hotspotTimerBitmapData = new BitmapData(512, 512, true, 0x00000000);
			hotspotTimerBitmapData.draw(hotspotTimer, new Matrix(),null,null,null,true);

			bitmapTexture = new BitmapTexture(hotspotTimerBitmapData);
			timerMaterial = new TextureMaterial(bitmapTexture);
			inworldHotspotTimer = new Sprite3D(timerMaterial,hotspotTimerBitmapData.width*1.5,hotspotTimerBitmapData.height*1.5);
			
			addChild(inworldHotspotTimer);
			inworldHotspotTimer.z +=10;

			Model.hotspotInView = data;
			Model.addEventListener(Model.HOTSPOT_IN_VIEW,onHotspotInView);	
		}
		
		private function onHotspotInView(e:Event):void
		{
			if(Model.hotspotInView != data)
				removeTimerFromScene();
		}
			
		private function onPanoEngaged(event:Event):void
		{
			if(Model.hotspotInView == data)
				removeTimerFromScene();
		}
		
		private function removeTimerFromScene():void
		{
			if(shFill != null){
				shFill.graphics.clear();
				shLine.graphics.clear();
			}
			
			
			if(this.contains(inworldHotspotTimer)){
				removeChild(inworldHotspotTimer);
				inworldHotspotTimer.dispose();
			}
			timerCount = 0;

			Model.removeEventListener(Model.HOTSPOT_IN_VIEW,onHotspotInView);	
			Model.hotspotInView = null;
			dispatchEvent(new Event(HOTSPOT_OFF_CENTRE));
		}
		
		private function updateTimer(value:int):void
		{

			if(!_center || value > 360)
				return;
				
			updatePicture(value);
			hotspotTimerBitmapData.draw(hotspotTimer);
			timerMaterial.texture = new BitmapTexture(hotspotTimerBitmapData);
			timerMaterial.alphaBlending = true;
			inworldHotspotTimer.material = timerMaterial;
		
			timerCount = value + 10;

			if(timerCount > 360 && _center && Model.hotspotInView == data)
			{
				dispatchEvent(new Event(HOTSPOT_CENTRED));
				Model.hotspot = data;
				selected = true;
				removeTimerFromScene();
			}
			else
				setTimeout(updateTimer,1,timerCount);
		}	
		
		
		
		private function updatePicture(t:Number):void {

			var radianAngle:Number = t*Math.PI/180.0;
			var i:int;

			
/*			if(t > 100)
			for (i=0; i<=t; i++){
				shLine.graphics.lineStyle(30,0xffffff);
				shLine.graphics.lineTo((circleR/t) *Math.cos(i*Math.PI/180), -(circleR/t)*Math.sin(i*Math.PI/180));
			}
			else*/
			for (i=0; i<=t; i++) {
				shFill.graphics.clear();
				shFill.graphics.beginFill(0xffffff,1);
				shFill.graphics.lineStyle(0,0xffffff);
				shFill.graphics.moveTo((circleR - circleThickness) *Math.cos(i*Math.PI/180), -(circleR - circleThickness)*Math.sin(i*Math.PI/180));
				shFill.graphics.lineTo(circleR*Math.cos(i*Math.PI/180), -circleR*Math.sin(i*Math.PI/180) );
				shFill.graphics.lineStyle(1,0x999999);
				shFill.graphics.lineTo(circleR*Math.cos((i+circleCircSteps)*Math.PI/180), -circleR*Math.sin((i+circleCircSteps)*Math.PI/180) );
				shFill.graphics.lineStyle(0,0xffffff);
				shFill.graphics.lineTo((circleR - circleThickness) *Math.cos((i+circleCircSteps)*Math.PI/180), -(circleR - circleThickness)*Math.sin((i+circleCircSteps)*Math.PI/180));
				shFill.graphics.lineStyle(1,0x999999);
				shFill.graphics.lineTo((circleR - circleThickness) *Math.cos(i*Math.PI/180), -(circleR - circleThickness)*Math.sin(i*Math.PI/180));
				shFill.graphics.endFill();
			}


		}
	}
}