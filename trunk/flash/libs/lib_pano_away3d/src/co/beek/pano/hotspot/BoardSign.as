package co.beek.pano.hotspot
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import co.beek.Fonts;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.BoardSignData;
	import flash.filters.DropShadowFilter;
	import co.beek.utils.ColorUtil;
	
	
	public class BoardSign extends BoardSignFl
	{
		private var _data:BoardSignData;
		
		private var arrowsRef:Dictionary = new Dictionary;
		
		private var deltaTitleWidth:Number;
		
		public function BoardSign(data:BoardSignData)
		{
			super();
			_data = data;
			_data.addEventListener(Event.CHANGE, onDataChange);

			
			arrowsRef[BoardSignData.RIGHT] = arrows.iconRight;
			arrowsRef[BoardSignData.LEFT] = arrows.iconLeft;
			arrowsRef[BoardSignData.UP] = arrows.iconUp;
			arrowsRef[BoardSignData.DOWN] = arrows.iconDown;
			
			selected = false;
			
			deltaTitleWidth = bg.width - titleField.textWidth;
			titleField.height = bg.height - titleField.y;
			Fonts.format(titleField, Fonts.boldFont);
			

		//	ColorUtil.colorize(arrows, 0xffffff);
			//titleField.textColor = 0xffffff;

			
			update();
			
			//bg.alpha = 0.2;

		}
		
		private function onDataChange(event:Event):void
		{
			update();
		}
		
		/**
		 * First called when the width is being set
		 */
		private function update():void
		{
			showDir();
			
			var guideScene:GuideSceneData = Model.guideData 
				? Model.guideData.getGuideSceneFromId(data.scene.id) 
				: null;
			
			titleField.text = guideScene 
				? guideScene.titleMerged 
				: _data.title;
			
			titleField.width = titleField.textWidth * 1.2;
			width = titleField.width + deltaTitleWidth;
		}
		
		/*public function get titleWidth():Number
		{
			return titleField.textWidth + deltaTitleWidth;
		}*/
		
		public override function get width():Number
		{
			return bg.width;
		}
		
		public override function set width(value:Number):void
		{
			bg.width = value;
			arrows.x = bg.x + bg.width - arrows.width - 20;
		}
		
		public function get data():BoardSignData
		{
			return _data;
		}
		
		private function onThumbError():void
		{
			trace("onThumbError");
		}
		
		private function showDir():void
		{
			for(var i:String in arrowsRef)
				MovieClip(arrowsRef[i]).visible = false;
			
			selectedArrow.visible = true;
			
		}
		
		private function get selectedArrow():Sprite
		{
			return MovieClip(arrowsRef[_data.direction]);
		}
		
		public function set selected(value:Boolean):void
		{
			if(value)
				Fonts.format(titleField, Fonts.boldFont);
			else
				Fonts.format(titleField, Fonts.boldFont);
		}
		
		public function dispose():void
		{
			_data.removeEventListener(Event.CHANGE, onDataChange);
			_data = null;
		}
	}
}