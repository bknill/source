package co.beek.pano
{
	import away3d.textures.BitmapCubeTexture;
	
	import co.beek.model.data.SceneData;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	public class BitmapCubeTextureDeep extends BitmapCubeTexture
	{
		public static const SCENE_BASIC_LOADED:String  = "SCENE_BASIC_LOADED";
		public static const SCENE_DEEP_LOADED:String  = "SCENE_DEEP_LOADED";
		
		private var _front:PanoFaceLoader = new PanoFaceLoader("f");
		private var _back:PanoFaceLoader = new PanoFaceLoader("b");
		
		private var _left:PanoFaceLoader = new PanoFaceLoader("l");
		private var _right:PanoFaceLoader = new PanoFaceLoader("r");
		
		private var _up:PanoFaceLoader = new PanoFaceLoader("u");
		private var _down:PanoFaceLoader = new PanoFaceLoader("d");
		
		private var _scene:SceneData;
		private var _faces:Array;
		
		private var count:int;
		private var prevCount:int;
		private var deepCount:int;
		
		public function BitmapCubeTextureDeep()
		{
			super(_right.data, _left.data, _up.data, _down.data, _front.data, _back.data);
			
			addLoader(_front);
			addLoader(_back);
			addLoader(_left);
			addLoader(_right);
			addLoader(_up);
			addLoader(_down);
		}
		
		private function addLoader(loader:PanoFaceLoader):void
		{
			loader.addEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			loader.addEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			loader.addEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
		}
		
		public function set scene(value:SceneData):void
		{
			_scene = value;
			
			count = 0;
			prevCount = 0;
			prevCount = 0;
			
			_front.scene = _scene;
			_back.scene = _scene;
			_left.scene = _scene;
			_right.scene = _scene;
			_up.scene = _scene;
			_down.scene = _scene;
		}
		
		public function get scene():SceneData
		{
			return _scene;
		}
		
		public function get bytesLoaded():Number
		{
			return _front.bytesLoaded
				+ _back.bytesLoaded
				+ _left.bytesLoaded
				+ _right.bytesLoaded
				+ _up.bytesLoaded
				+ _down.bytesLoaded;
		}
		
		public function get bytesTotal():Number
		{
			return _front.bytesTotal
				+ _back.bytesTotal
				+ _left.bytesTotal
				+ _right.bytesTotal
				+ _up.bytesTotal
				+ _down.bytesTotal;
		}
		
		private function onPanoBitmapUpdate(event:Event):void
		{
			count ++;
			
			if(count == 6)
			{
				positiveZ = _front.data;
				negativeZ = _back.data;
				
				positiveX = _right.data;
				negativeX = _left.data;
				
				positiveY = _up.data;
				negativeY = _down.data;
				
				// clear all the loaders now that they have been reassine
				//clear();

				
				
				dispatchEvent(new Event(SCENE_BASIC_LOADED))
			}
		}
		
		private function onPreviewTileLoaded(event:Event):void
		{
			var face:String = event.target.face;
			
			prevCount ++;

			if(prevCount == 6){
				clear();
				ShowPreviewTiles();
				dispatchEvent(new Event(SCENE_BASIC_LOADED));
				
				if(!scene.video){
					_front.loadDeepTiles();
					_back.loadDeepTiles();
					_left.loadDeepTiles();
					_right.loadDeepTiles();
					_up.loadDeepTiles();
					_down.loadDeepTiles();
				}
			}
		}
		
		private function ShowPreviewTiles():void
		{
			positiveZ = _front.data;
			negativeZ = _back.data;
			positiveX = _right.data;
			negativeX = _left.data;
			positiveY = _up.data;
			negativeY = _down.data;
			
		}
		
		private function onFaceTilesLoaded(event:Event):void
		{
			var face:String = event.target.face;
			
			switch(face){
				case "f":
					positiveZ = _front.data;
					_front.clear();
					break;
				case "b":
					negativeZ = _back.data;
					_back.clear();
					break;
				case "r":
					positiveX = _right.data;
					_right.clear();
					break;
				case "l":
					negativeX = _left.data;
					_left.clear();
					break;
				case "u":
					positiveY = _up.data;
					_up.clear();
					break;
				case "d":
					negativeY = _down.data;
					_down.clear();
					break;
				
			}
			
			deepCount ++;
			if(deepCount == 6){
				dispatchEvent(new Event(SCENE_DEEP_LOADED));
				deepCount = 0;
			}
			
		}
		
		public function clear():void
		{
			clearFinalListeners();

			_front.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_front.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_front.clear();

			_back.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_back.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_back.clear();

			_left.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_left.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_left.clear();

			_right.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_right.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_right.clear();

			_up.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_up.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_up.clear();

			_down.removeEventListener(PanoFaceLoader.BITMAP_CHANGE, onPanoBitmapUpdate);
			_down.removeEventListener(PanoFaceLoader.PREVIEW_TILE_LOADED, onPreviewTileLoaded);
			_down.clear();

		}
		
		public function clearFinalListeners():void
		{
		
			_front.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			_back.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			_left.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			_right.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			_up.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
			_down.removeEventListener(PanoFaceLoader.TILES_LOADED, onFaceTilesLoaded);
		}
		
		
	}
}