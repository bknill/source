package co.beek.pano
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.system.Capabilities;
	import flash.system.LoaderContext;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.Log;
	
	public class PanoFaceLoader extends EventDispatcher
	{
		public static const BITMAP_CHANGE:String = "BITMAP_CHANGE";
		public static const PREVIEW_TILE_LOADED:String = "PREVIEW_TILE_LOADED";
		public static const TILES_LOADED:String = "TILES_LOADED";
		
		private var _data:BitmapData;
		
		public var face:String;
		
		private var preview:Loader = new Loader();
		
		private var tl:Loader = new Loader();
		private var tr:Loader = new Loader();
		private var bl:Loader = new Loader();
		private var br:Loader = new Loader();
		
		private var positions:Dictionary = new Dictionary();
		
		private var _scene:SceneData;
		
		public function PanoFaceLoader(face:String)
		{
			this.face = face;
			
			
			/*			_data = Model.config.device == Constants.IPAD_1 || Constants.isPhone(Model.config.device)
			? new BitmapData(512, 512)
			: new BitmapData(1024, 1024);*/
			
			_data = new BitmapData(1024, 1024);
			
			// do nothing
			preview.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreviewLoadComplete);
			preview.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onPreviewIOError);
			
			addListeners(tl);//.contentLoaderInfo.addEventListener(Event.COMPLETE, onTileLoadComplete);
			addListeners(tr);//.contentLoaderInfo.addEventListener(Event.COMPLETE, onTileLoadComplete);
			addListeners(bl);//.contentLoaderInfo.addEventListener(Event.COMPLETE, onTileLoadComplete);
			addListeners(br);//.contentLoaderInfo.addEventListener(Event.COMPLETE, onTileLoadComplete);
			
			positions[tl] = new Point(0,0);
			positions[tr] = new Point(512, 0);
			positions[bl] = new Point(0,512);
			positions[br] = new Point(512,512);
		}
		
		private function addListeners(loader:Loader):void
		{
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onTileLoadComplete);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onTileIOError);
		}
		
		private var count:int;
		
		public function set scene(value:SceneData):void
		{
			_scene = value;
			
			load(preview, "9/0_0.jpg");
			
		}
		
		private function load(loader:Loader, imagePath:String):void
		{
			//Log.record("PanoFaceLoader.load()");
			loader.load(new URLRequest(getAssetUrl(imagePath)), new LoaderContext(true));
		}
		
		private function getAssetUrl(imagePath:String):String
		{
			if(Model.config.domain.indexOf("beekdev") > -1 && Model.state.guideId == "513")
				return "http://beekdev.co/cdn/"+panoBasePath+"/"+imagePath;
			
			return Model.config.getAssetUrl(panoBasePath+"/"+imagePath);
		}
		
		private function get panoBasePath():String
		{
			return "scene_"+_scene.id+"_pano_"+_scene.panoIncrement+"_"+face;
		}
		
		public function get bytesLoaded():Number
		{
			var loaded:Number = 0;
			if(preview.contentLoaderInfo)
				loaded += preview.contentLoaderInfo.bytesLoaded;
			
			if(tl.contentLoaderInfo)
				loaded += tl.contentLoaderInfo.bytesLoaded;
			if(tr.contentLoaderInfo)
				loaded += tr.contentLoaderInfo.bytesLoaded;
			if(bl.contentLoaderInfo)
				loaded += bl.contentLoaderInfo.bytesLoaded;
			if(br.contentLoaderInfo)
				loaded += br.contentLoaderInfo.bytesLoaded;
			
			return loaded;
		}
		
		public function get bytesTotal():Number
		{
			var total:Number = 1;// hack to say at least one byte
			if(preview.contentLoaderInfo)
				total += preview.contentLoaderInfo.bytesTotal;
			
			if(tl.contentLoaderInfo)
				total += tl.contentLoaderInfo.bytesTotal;
			if(tr.contentLoaderInfo)
				total += tr.contentLoaderInfo.bytesTotal;
			if(bl.contentLoaderInfo)
				total += bl.contentLoaderInfo.bytesTotal;
			if(br.contentLoaderInfo)
				total += br.contentLoaderInfo.bytesTotal;
			
			return total;
		}
		
		private function onPreviewIOError(event:Event):void
		{
			Log.record("Error loading: " + preview.contentLoaderInfo.url)
			dispatchEvent(new Event(BITMAP_CHANGE));
		}
		
		private function onPreviewLoadComplete(event:Event):void
		{
			var bitmap:BitmapData = Bitmap(preview.content).bitmapData;
			if(bitmap.width == _data.width)
				_data = bitmap;
				
			else
				_data.draw(bitmap, new Matrix(2,0,0,2), null, null, null, true);
			
			
			count = 0;
			
			
			
			dispatchEvent(new Event(PREVIEW_TILE_LOADED));
		}
		
		public function loadDeepTiles():void
		{
			if(!Constants.usesOnlyPreviewTiles(Model.config.device)){
				load(tl, "10/0_0.jpg");
				load(bl, "10/0_1.jpg");
				load(tr, "10/1_0.jpg");
				load(br, "10/1_1.jpg");
			}
		}
		
		
		private function onTileIOError(event:IOErrorEvent):void
		{
			var loader:Loader = LoaderInfo(event.target).loader;
			Log.record("Error loading: " +positions[loader] + " " + event.text)
			count ++;
			if(count == 4)
				dispatchEvent(new Event(BITMAP_CHANGE));
		}
		
		private function onTileLoadComplete(event:Event):void
		{
			//Log.record("PanoFaceLoader.onTileLoadComplete()");
			try{
				var loader:Loader = LoaderInfo(event.target).loader;
				var bitmap:BitmapData = Bitmap(loader.content).bitmapData;
				
				
				//var delta:Number = 4;
				//bitmap.colorTransform(bitmap.rect, new ColorTransform(delta, delta, delta, delta));
				if(bitmap.width == _data.width)
					_data = bitmap;
					
				else
					_data.copyPixels(bitmap, bitmap.rect, positions[loader]);
			}
			catch(e:Error)
			{
				Log.record(e.message);
			}
			count ++;
			if(count == 4){
				dispatchEvent(new Event(TILES_LOADED));
			}
		}
		
		public function get data():BitmapData
		{
			return _data;
		}
		
		public function clear():void
		{
			try{ preview.close() }catch(e:Error){};
			if(preview.content is Bitmap)
				Bitmap(preview.content).bitmapData = null;

			try{ tl.close() }catch(e:Error){};
			try{ tr.close() }catch(e:Error){};
			try{ bl.close() }catch(e:Error){};
			try{ br.close() }catch(e:Error){};
		}
	}
}