package co.beek.pano
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	public class TransitionVideo extends Sprite
	{
		public static const COMPLETE:String = "COMPLETE";
		
		public static const BUFFER_FULL:String = "BUFFER_FULL";
		
		public static const FADE_COMPLETE:String = "FADE_COMPLETE";
		
		private var video:Video = new Video();
		
		private var stageVideo:StageVideo;
		
		protected var softwareVideo:Video;
		
		private var netConnection:NetConnection;
		
		private var netStream:NetStream;
		
		private var _flvUrl:String;
		
		private var videoBackground:Sprite = new Sprite();
		
		private var _width:Number;
		
		private var _height:Number;
		
		public var skipButton:SkipButton = new SkipButton;
		
		public function TransitionVideo()
		{
			super();
		
				
			netConnection = new NetConnection()
			netConnection.addEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
			netConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			netConnection.connect(null);
			
			skipButton.mouseChildren = false;
			skipButton.addEventListener(MouseEvent.CLICK, onSkipButtonClick);
		}
		
		public function resize(width:Number, height:Number):void
		{
			_width = width;
			_height = height;
			
			skipButton.x = _width/2 - skipButton.width - 15;
			skipButton.y = _height - skipButton.height - 40;
		}
		
		public function set flvUrl(value:String):void
		{
			_flvUrl = value;
			
			if(!Constants.isIOS() || !Constants.isAndroid() && stage.stageVideos.length > 0)
			{
				stageVideo = stage.stageVideos[0];
				stageVideo.viewPort = new Rectangle(0 , 0, _width, _height);
				stageVideo.attachNetStream( netStream );
				
			}
/*			else if(video.parent != this)
			{
				addChild(video);
			}*/

			
			if(skipButton.parent != this)
				addChild(skipButton);
			
			
			if(netConnection.connected)
				netStream.play(_flvUrl);
		}
		
		public function get flvUrl():String
		{
			return _flvUrl;
		}
		
		
		private function onNetConnectionStatus(event:NetStatusEvent):void 
		{
			switch (event.info.code) 
			{
				case "NetConnection.Connect.Success":
					netStream = new NetStream(netConnection);
					netStream.client = this;
					netStream.bufferTime = 0;
					netStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStreamStatus);
					video.attachNetStream(netStream);
					
					if(_flvUrl)
					{
						netStream.play(_flvUrl);
						//netStream.pause();
					}
					break;
			}
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void 
		{
			trace("securityErrorHandler: " + event);
		}
		
		private function onNetStreamStatus(event:NetStatusEvent):void 
		{
			trace("TransitionVideo.onNetStreamStatus() " + event.info.code);
			switch (event.info.code) 
			{
				case "NetStream.Buffer.Full":
					dispatchEvent(new Event(BUFFER_FULL));
					break;
				
				case "NetStream.Play.Stop":
					netStream.pause();
					dispatchEvent(new Event(COMPLETE));
					break;
				
			}
		}
		
		public function onMetaData(data:Object):void
		{
			trace("TransitionVideo.onMetaData: " + data);
			var scale:Number = Math.max(_width/data.width, _height/data.height);
			video.width = data.width * scale;
			video.height = data.height * scale;
			video.x = (_width - video.width)/2;
			video.y = (_height - video.height)/2;
		}
		
		public function onXMPData(data:Object):void
		{
			trace("TransitionVideo.onXMPData: " + data);
		}
		
		public function onCuePoint(data:Object):void
		{
			trace("TransitionVideo.onCuePoint: " + data);
		}
		
		public function onPlayStatus(data:Object):void
		{
			trace("TransitionVideo.onPlayStatus: " + data);
		}
		
		public function stopAndFadeSlowly(seconds:Number):void
		{
			trace("TransitionVideo.stopAndFadeSlowly();");
			netStream.pause();
			fadeAway(seconds);
		}
		
		public function fadeAway(seconds:Number):void
		{
			trace("TransitionVideo.fadeAway(" + seconds + ");");
			seconds = 2;
					
			Tweener.removeTweens(this);
			Tweener.addTween(video, {
				"time" : seconds, 
				"alpha": 0, 
				"transition": "linear", 
				"onComplete": fadeSlowlyComplete
			});
			
		}
		
		public function fadeSlowlyComplete():void
		{
			trace("TransitionVideo.fadeSlowlyComplete();");
			_flvUrl = null;
			video.alpha = 1;
			video.clear();
			netStream.close();
			
			//if(video && video.parent == this)
				//removeChild(video);
			
			//if(skipButton.parent == this)
				removeChild(skipButton);
			
			
			dispatchEvent(new Event(FADE_COMPLETE));
		}
		
		private function onSkipButtonClick(event:MouseEvent):void
		{
			netStream.pause();
			dispatchEvent(new Event(COMPLETE));
		}
	}
}