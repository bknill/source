package co.beek.pano
{
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.geom.Matrix;

	
	import caurina.transitions.Tweener; 

	public class Black extends Sprite
	{
		private var blackShape:Shape = new Shape();
		public  var alphaValue:Number;
		private var square:Shape = new Shape;
		
		public function Black() 
		{
			super();
			blackShape.alpha = 0;
			blackShape.cacheAsBitmap = true;
			square.cacheAsBitmap = true;
			addChild(blackShape);
			addChild(square);
			blackShape.mask = square;
		}
		
		public function resize(width:Number, height:Number):void
		{
			blackShape.graphics.beginFill(0x000000);
			blackShape.graphics.drawRect(0,0, width, height);
			blackShape.graphics.endFill();

			createGradient(width, height);

		}
		
		public function tween(alpha:Number):void
		{
			
			alphaValue = alpha;
			
			Tweener.addTween(blackShape, {
				time : 0.3, 
				alpha: alpha
			});
			
			Tweener.addTween(square, {
				time : 2, 
				alpha: alpha
			});
			
			if(alpha > 0)
				this.mouseEnabled = true;
			else
				this.mouseEnabled = false;

		}
		
		public function createGradient(width:Number, height:Number):void
		{
			var mat:Matrix= new Matrix();
			var colors:Array=[0xFFFFFF,0xFFFFFF];
			var alphas:Array=[0,1];
			var ratios:Array=[0,255];
			var spreadMethod:String = SpreadMethod.PAD; 
			var interp:String = InterpolationMethod.LINEAR_RGB; 
			var focalPtRatio:Number = 0.5; 
			
			var matrix:Matrix = new Matrix(); 
			var boxWidth:Number = width; 
			var boxHeight:Number = height; 
			var boxRotation:Number = Math.PI/2; // 90° 
			var tx:Number = 0; 
			var ty:Number = 0; 
			matrix.createGradientBox(boxWidth, boxHeight, boxRotation, tx, ty); 
			
			mat.createGradientBox(width,height);
			square.graphics.lineStyle();
			square.graphics.beginGradientFill(GradientType.RADIAL,colors,alphas,ratios,matrix, spreadMethod, interp);
			square.graphics.drawRect(0, 0, width, height); 
			square.graphics.endFill();
		}
		
		
	}
}