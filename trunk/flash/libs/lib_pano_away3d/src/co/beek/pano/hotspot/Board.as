package co.beek.pano.hotspot
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BoardSignData;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.setTimeout;
	
	public class Board extends Hotspot
	{
		private var _renderers:Vector.<BoardSign> = new Vector.<BoardSign>();
		
		private var selectedRenderer:BoardSign;
		
		private var maxSignWidth:int;
		private var signsHeight:int;
		
		public function Board(data:BoardData, ui:BoardFl)
		{
			super(data, ui);
			
			_data.addEventListener(BoardData.SCENES_CHANGED, onScenesChanged);
			
			//ui.closeButton.visible = false;
			//ui.closeButton.addEventListener(MouseEvent.CLICK, onCloseClick);
			//ui.bg.alpha = 0.4;
			addAllSigns();
			
			if(_hidden)
				setInworldBlank();
			else
				inworldSprite = ui;
		}
		
		private function get boardData():BoardData
		{	
			return BoardData(_data) 
		}

		private function get boardUI():BoardFl
		{	
			return BoardFl(_ui) 
		}
		
		private function addAllSigns():void
		{
			removeAllSigns();
			
			var insetX:Number = boardUI.signsLocator.x - boardUI.bg.x;
			maxSignWidth = boardUI.bg.width - 2*insetX;
			
			var signs:Vector.<BoardSignData> = boardData.boardSigns;
			for(var i:int = 0; i<signs.length; i++)
				addSign(signs[i]);
			
			updateWidths();
			updateHeights();
		}
		
		private function addSign(data:BoardSignData):void
		{
			if(Model.isTypeExcluded(data.scene.locationType))
				return;
			
			var sign:BoardSign = new BoardSign(data);
			sign.y = (Math.round(boardUI.signsLocator.numChildren * sign.height * 1.2));
			sign.mouseChildren = false;
			sign.addEventListener(MouseEvent.MOUSE_DOWN, onSignClick);
			
			boardUI.signsLocator.addChild(sign);
			
			_renderers.push(sign);
			
			maxSignWidth = Math.max(maxSignWidth, sign.width);
			signsHeight = sign.y + sign.height;
		}
		
		private function updateWidths():void
		{
			for(var i:int = 0; i<_renderers.length; i++)
				BoardSign(_renderers[i]).width = maxSignWidth;
			
			var inset:Number = boardUI.signsLocator.x - boardUI.bg.x;
			boardUI.bg.width = maxSignWidth + inset*2;
			
			//boardUI.closeButton.x = 
			//boardUI.bg.x + boardUI.bg.width - boardUI.closeButton.width;
		}

		private function updateHeights():void
		{
			var insetY:Number = boardUI.signsLocator.y - boardUI.bg.y;
			boardUI.bg.height 
				= Math.max(40, signsHeight + insetY*2);
		}
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
			if(value)
				setInworldBlank();
			else
				inworldSprite = boardUI;
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			
			if(value)
				showBoardFlat();
			else
				showBoardInworld();
		}
		
		private function showBoardFlat():void
		{
			boardUI.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, false);
			
			if(_renderers.length == 1){
				var renderer:BoardSign = _renderers[0];
				selectedRenderer = renderer;
				fadeAndGoToScene();
				
			}
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			var i:int = _renderers.indexOf(selectedRenderer);
			if(event.keyCode == Keyboard.DOWN)
				selectSign(_renderers[(i == _renderers.length-1 ? 0 :i + 1)])
			
			if(event.keyCode == Keyboard.UP)
				selectSign(_renderers[(i == 0 ? _renderers.length-1 :i - 1)])
			
			if(event.keyCode == Keyboard.ENTER)
				fadeAndGoToScene();
			
			if(event.keyCode == Keyboard.ESCAPE)
				Model.hotspot = null;
		}

		private function showBoardInworld():void
		{
			//boardUI.bg.alpha = 0.75;
			//boardUI.closeButton.visible = false;
			
			// We are closing, but a sign is still selected
			if(selectedRenderer)
				selectedRenderer.selected = false;
			selectedRenderer = null;
			
			
			if(boardUI.stage)
				boardUI.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		private function onSignClick(event:Event):void
		{
			event.stopPropagation();
			var renderer:BoardSign = BoardSign(event.target);
			selectSign(renderer);
			
			if(Model.config.isAvailable(selectedRenderer.data.scene))
				fadeAndGoToScene();
			else
				Model.dispatchOfflineError(selectedRenderer.titleField.text);
		}
		
		private function fadeAndGoToScene():void
		{
			Tweener.addTween(_ui, {
				"time":0.8, 
				"alpha":0, 
				"transition":"easeInQuad", 
				"onComplete":goToScene
			});
		}
		
		private function selectSign(renderer:BoardSign):void
		{
			if(selectedRenderer)
				selectedRenderer.selected = false;
			
			selectedRenderer = renderer;
			selectedRenderer.selected = true;
			selectedRenderer.titleField.alpha = 0.5
		}
	
		private function goToScene():void
		{
			
			// Dispatch the tracking for the sign navigation
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_NAVIGATION,
				"sign_board",
				selectedRenderer.data.scene.id+"|"+Model.currentScene.id);
			
			
			// Dispatch change scene event last, clears the hotspots
			Model.state.loadingScene = selectedRenderer.data.scene;
			
			selectedRenderer = null;
		}
		
		private function onScenesChanged(event:Event):void
		{
			removeAllSigns();
			addAllSigns();
			
			inworldSprite = boardUI;
		}
		
		private function removeAllSigns():void
		{
			for(var i:int = 0; i < _renderers.length; i++)
			{
				var sign:BoardSign = BoardSign(_renderers[i]);
				sign.removeEventListener(MouseEvent.CLICK, onSignClick);
				sign.dispose();
				Tweener.removeTweens(sign);
				boardUI.signsLocator.removeChild(sign);
			}
			
			_renderers = new Vector.<BoardSign>();
			selectedRenderer = null;
		}
		
		private function onCloseClick(event:MouseEvent):void
		{
			Model.hotspot = null;
		}
		
		public override function dispose():void
		{
			// Call before super so that data is not yet null
			_data.removeEventListener(BoardData.SCENES_CHANGED, onScenesChanged);
			Tweener.removeTweens(boardUI.bg);
			removeAllSigns();
			
			super.dispose();
		}
	}
}