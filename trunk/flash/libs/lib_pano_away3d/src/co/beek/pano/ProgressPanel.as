package co.beek.pano
{
	import flash.events.Event;
	
	import co.beek.Fonts;
	import co.beek.utils.ColorUtil;

	public class ProgressPanel extends PanoProgressPanelFl
	{
		public static const CANCEL:String = "CANCEL";
		
		public function ProgressPanel(cancelable:Boolean)
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public override function get width():Number
		{
			return progressTrack.width;
		}

		public override function set width(value:Number):void
		{
			progressTrack.width = value;
			progressField.width = value;
		}
		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			//Fonts.setFormat(progressField, Fonts.titleFormatSmall);
			Fonts.format(progressField, Fonts.boldFont);
		}
		
		public function set text(value:String):void
		{
			progressField.text = value;
		}
		
		public function set chromeColor(value:uint):void
		{
			progressField.textColor = value;
			ColorUtil.colorize(progressTrack, value);
		}
		
		public function set barColor(value:uint):void
		{
			ColorUtil.colorize(progressBar, value);
		}
		
		public function set percent(value:Number):void
		{
			progressBar.width = Math.max(progressBar.height, Math.min(1, value) * (progressTrack.width - 2));
		}
		
		private function onCancelButtonClick(event:Event):void 
		{
			dispatchEvent(new Event(CANCEL));
		}
	}
}