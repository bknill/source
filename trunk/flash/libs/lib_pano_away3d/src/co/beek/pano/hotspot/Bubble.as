package co.beek.pano.hotspot
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Scene;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import away3d.entities.Sprite3D;
	import away3d.materials.TextureMaterial;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.textures.BitmapTexture;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.pano.Pano;
	import co.beek.utils.ColorUtil;
	

	
	/**
	 * Bubble is a special case, 
	 * We use the inworld bitmap for the selected view.
	 * This is because flash tries to reposition the text fields to be on-pixel.
	 * This is causing issues with the 4 dark fields, which are moving arould a little
	 */
	public class Bubble extends Hotspot
	{
		private var bubbleUI:Bubble2Fl;
		public var bitmapData:BitmapData;

		public function Bubble(data:BubbleData, ui:Bubble2Fl)
		{
			super(data, ui);
			this.bubbleUI = ui; 
			
			Fonts.format(ui.titleField, Fonts.boldFont);
			
			bubbleData.addEventListener(Event.CHANGE, onDataChange);
			Model.addEventListener(Model.SECTION_CHANGE,onSectionChange);
			Model.addEventListener(State.UI_STATE_CHANGE,onUIStateChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE,onGuideViewChange);
			update();
			
			ui.mouseChildren = false;
			
			//hides bubbles when their scene isn't in guide on mobile - ropey
			if(Model.guideData && !Model.sceneInGuide(bubbleData.targetScene.id) && Model.state.uiState != State.UI_STATE_EDITING_SCENE)
				disable();
			
/*			//Check if location is in guide and not currently editing scene
			if(!Model.locationInGuide(bubbleData.targetScene.locationId) && Model.state.uiState != State.UI_STATE_EDITING_SCENE)
				disable();*/
			
		}
		
		public override function get distanceRatio():int
		{
			return 2;
		}
		
		private function get bubbleData():BubbleData
		{	return BubbleData(_data) }
		
		private function onDataChange(event:Event):void
		{
			update();
		}
		
		private function hide():void
		{
			this.visible = false;
		}
		
		private function show():void
		{
			this.visible = true;
		}
		
		private function onUIStateChange(e:Event):void
		{
			
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE && !this.visible)
			{
				this.visible = true;
				bubbleData.addEventListener(Event.CHANGE, onDataChange);
				Model.addEventListener(Model.SECTION_CHANGE,onSectionChange);
			}
			else if(!Model.sceneInGuide(bubbleData.targetScene.id))
				disable();
			
		}
		
		private function disable():void
		{
			this.visible = false;
			bubbleData.removeEventListener(Event.CHANGE, onDataChange);
			Model.removeEventListener(Model.SECTION_CHANGE,onSectionChange);
		}
		
		private function onSectionChange(e:Event):void
		{
			
			
			if(Model.currentSection == null){
				this.visible = true;
				return
			}

			
			for each(var guideScene:GuideSceneData in Model.guideData.guideScenes)
				if(guideScene.scene.id == bubbleData.targetScene.id)
					var thisSectionId:String = guideScene.sectionId
			
			if(thisSectionId == null)
				return;
					
			for each(var guideSection:GuideSectionData in Model.guideData.guideSections)
				if(guideSection.id == thisSectionId)
					var targetSection:GuideSectionData = guideSection;
				
						
			if(!targetSection.isFamily(Model.currentSection) && targetSection != Model.currentSection)
				hide()
			else
				setTimeout(show,200);
						
		}
		
		private function onGuideViewChange(e:Event):void
		{
		
			//if(Model.state.guideView == State.GUIDE_VIEW_CLOSED)
			//	setTimeout(show,200);
		
		}
		
		
/*		override function set inworldSprite():void
		{
			
		}
		*/
		private function update():void
		{
			if(!bubbleData.targetScene)
				return;
			
			var guideScene:GuideSceneData = Model.guideData 
				? Model.guideData.getGuideSceneFromId(bubbleData.targetScene.id) 
				: null;
			
			
			//Title is 1. override title, 2. GuideSceneTitle 3. Scene Title
			bubbleUI.titleField.text = (_data.title != bubbleData.targetScene.title) 
						 ? _data.title 
						 : (guideScene != null) 
						 	? guideScene.titleMerged
						    : bubbleData.targetScene.title;
			
			bubbleUI.titleField.width = bubbleUI.titleField.textWidth + 10;
			bubbleUI.titleField.textColor = 0x000000;
			bubbleUI.bg.width  = bubbleUI.titleField.x * 2 + bubbleUI.titleField.width;
			bubbleUI.notch.x = bubbleUI.bg.width/2 - bubbleUI.notch.width/2;
				
		}
		


			
		private function bubbleClicked():void
		{
			trace("Bubble.onUIClick");
			bubbleUI.titleField.textColor = 0x000000;
			Tweener.removeTweens(_ui);
			if(Model.config.isAvailable(bubbleData.targetScene))
				Tweener.addTween(_ui, {
					"time" : 0.2, 
					"alpha" : 0, 
					"transition" : "easeInQuad", 
					"onComplete" : navigateToScene
				});
			else
				Model.dispatchOfflineError(bubbleData.targetScene.title);
		}
		
		private function navigateToScene():void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_NAVIGATION,
				"bubble", 
				bubbleData.targetSceneId+"|"+bubbleData.sceneId);
			
			// cannot navigate when admining scene
			Model.state.loadingScene = bubbleData.targetScene;
			
			dispose();
		}

		private function onCloseButtonClick(event:MouseEvent):void 
		{
			Tweener.removeTweens(_ui);
			Model.hotspot = null;
		}
		
		public var material3d:TextureMaterial;
		
		public override function set hidden(value:Boolean):void
		{
			super.hidden = value;
			
			if(value)
				setInworldBlank();
			else{
				inworldSprite = _ui;
				var bmt:BitmapTexture = new BitmapTexture(inworldBitmap);
				material3d = new TextureMaterial(bmt);
				material3d.alphaBlending = true;
				var inworldSprite3D:Sprite3D = new Sprite3D(material3d,inworldBitmap.width*2,inworldBitmap.height*2);
				addChild(inworldSprite3D);	
				
				var blank:BitmapData = new BitmapData(inworldBitmap.width,inworldBitmap.height, true, 0x00000000);
				inworldBitmap = blank;
			}
		}
		

		
		
		public override function set selected(value:Boolean):void
		{
			trace("Bubble.setSelected");
			super.selected = value;
			
			Tweener.removeTweens(_ui);
			
			if(value &&  bubbleData){
				Model.state.loadingScene = bubbleData.targetScene;
				//bubbleUI.titleField.alpha = 0.3;
			}
			else
				bubbleUI.titleField.alpha = 0.3;

		}
		
		
	
		public override function dispose():void
		{
			Tweener.removeTweens(_ui);
			bubbleData.removeEventListener(Event.CHANGE, onDataChange);
			Model.removeEventListener(Model.SECTION_CHANGE,onSectionChange);
			super.dispose();
		}
		
		
	}
}