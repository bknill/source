package co.beek.pano
{
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.Stage3D;
	import flash.display3D.Context3D;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.clearTimeout;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import away3d.cameras.Camera3D;
	import away3d.cameras.lenses.PerspectiveLens;
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.containers.View3D;
	import away3d.controllers.HoverController;
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.core.math.Quaternion;
	import away3d.core.pick.PickingCollisionVO;
	import away3d.core.pick.RaycastPicker;
	import away3d.entities.Mesh;
	import away3d.events.MouseEvent3D;
	import away3d.materials.TextureMaterial;
	import away3d.materials.utils.SimpleVideoPlayer;
	import away3d.primitives.SkyBox;
	import away3d.primitives.SphereGeometry;
	import away3d.textures.NativeVideoTexture;
	import away3d.textures.VideoTexture;
	
	import caurina.transitions.Tweener;
	
	import co.beek.IPano;
	import co.beek.Log;
	import co.beek.event.PanoLookEvent;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.PanoAngle;
	import co.beek.model.data.PanoLook;
	import co.beek.model.data.PanoPos;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GameTaskHotspot;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.pano.hotspot.Board;
	import co.beek.pano.hotspot.Bubble;
	import co.beek.pano.hotspot.Hotspot;
	import co.beek.pano.hotspot.Photo;
	import co.beek.pano.hotspot.Poster;
	import co.beek.pano.hotspot.RssReader;
	import co.beek.pano.hotspot.SoundPlayer;
	import co.beek.pano.hotspot.VideoPlayer;
	import co.beek.utils.MathUtil;
	
	
	
	public class Pano extends Sprite implements IPano
	{
		public static const PANO_TILES_LOADED:String = "PANO_TILES_LOADED";
		public static const PANO_DEEP_TILES_LOADED:String = "PANO_DEEP_TILES_LOADED";
		public static const PANO_HOSPOTS_READY:String = "PANO_HOSPOTS_READY";
		public static const PANO_MOUSE_DOWN:String = "PANO_MOUSE_DOWN";
		public static const PANO_MOUSE_UP:String = "PANO_MOUSE_UP";
		public static const START_DRAG:String = "START_DRAG";
		public static const STOP_DRAG:String = "STOP_DRAG";
		public static const HOTSPOT_CLICKED:String = "HOTSPOT_CLICKED";
		public static const START_AUTOROTATE:String = "START_AUTOROTATE";
		public static const STOP_AUTOROTATE:String = "STOP_AUTOROTATE";
		public static const PAUSE_AUTOROTATE:String = "PAUSE_AUTOROTATE";
		public static const GYRO_ACTIVE:String = "GYRO_ACTIVE";
		public static const GYRO_STOP:String = "GYRO_STOP";
		public static const VR_MODE_ON:String = "VR_MODE_ON";
		public static const VR_MODE_OFF:String = "VR_MODE_OFF";
		
		public static var instance:Pano;
		
		// Away3D4 Vars
		private var scene:Scene3D = new Scene3D();
		public var camera:Camera3D = new Camera3D();
		public var view:View3D = new View3D(null, null, null, false, "baselineExtended");
		
		//camera controllers
		private var cameraController:HoverController;
		private var cameraControllerR:HoverController;
		
		// Away3D4 Camera handling variables (Hover Camera)
		public var mouseDragPano:Boolean = false;
		public var lastPanAngle:Number;
		public var lastTiltAngle:Number;
		public var currentPanAngle:Number;
		public var currentTiltAngle:Number;
		private var panSpeed:Number = 2;
		private var lastMouseX:Number;
		private var lastMouseY:Number;
		private var listener:ObjectContainer3D = new ObjectContainer3D;
		private var qu:Quaternion = new Quaternion();
		private var qi:Quaternion = new Quaternion();
		public var currentQuaternion:Quaternion = new Quaternion();
		private var m:Matrix3D = new Matrix3D();
		private var _pan:Number;
		private var _tilt:Number;
		
		
		// Primitives etc
		private var skyBox:SkyBox;
		private var skyBoxATFTexture:SkyboxATFTexture;
		private var skyBoxBitmapTexture:BitmapCubeTextureDeep;
		
		//private var skyBoxTexture:ATFCubeTexture;
		
		// Away3D Config
		private var cameraViewDistance:Number = 20000;
		private var antiAlias:Number = 2;
		
		private var angles:Dictionary = new Dictionary;
		
		private var _currentScene:SceneData;
		
		private var _currentGuideScene:GuideSceneData;
		
		private var _guideData:GuideData;
		
		private var transitionBitmap:TransitionBitmap = new TransitionBitmap;
		
		private var transitionVideo:TransitionVideo = new TransitionVideo;
		
		private var progressPanel:ProgressPanel = new ProgressPanel(false);
		private var progressPanel2:ProgressPanel = new ProgressPanel(false);
		
		private var hotspotSelectedTweenTimeout:uint;
		
		private var autorotateTimeout:int;
		
		private var ready:Boolean = false;
		
		private var active:Boolean = false;
		
		private var appScale:Number = 1;
		
		private var videoBackground:Sprite = new Sprite();
		
		private var hotspotDataToMesh:Dictionary = new Dictionary();
		
		private var _selectedHotspot:Hotspot;
		
		private var zoomBeforeHotspot:Number;
		
		private var mouseDownHotspot:Hotspot;
		private var draggingHotspot:Hotspot;
		private var draggingHotspotPos:PanoPos;
		private var hotspotInCentre:Bubble;
		
		private var loadingPercent:Number;
		
		private var loading:Boolean;
		
		public var _interrupted:Boolean = false;
		
		private var presentationTimer:Timer = new Timer(7000);
		
		private var startShowHotspotTime:uint;
		
		private var sounds:Vector.<SoundPlayer> = new Vector.<SoundPlayer>;
		
		private var channel:SoundChannel;
		
		private var stage3DManager:Stage3DManager;
		private var stage3DProxy:Stage3DProxy;
		private var rightEye:View3D;
		private var rightEyeCamera:Camera3D = new Camera3D();
		private var _lensL:PerspectiveLens;
		private var _lensR:PerspectiveLens;
		private var lens:PerspectiveLens;
		
		private var panoVideoMesh:Mesh;
		private var sphereGeometry:SphereGeometry;
		private var panoTexture2DBase;
		private var panoTextureMaterial:TextureMaterial;
		private var panoVideoMeshNext:Mesh;
		private var textureMaterialNext:TextureMaterial;
		private var texture2DBaseNext:NativeVideoTexture;
		
		
		//private var lightPicker:StaticLightPicker;
		
		private var sound:Sound;
		
		public function Pano()
		{
			super();
			instance= this;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		

		
		public function onAddedToStage(e:Event):void
		{
			Log.record("Pano.onAddedToStage()");
			// We have been added to the stage and now need to clean up that event listener
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			//stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);

			
			//transitionBitmap.addEventListener(TransitionBitmap.BLUR_COMPLETE, onBlurComplete);
			//transitionBitmap.addEventListener(TransitionBitmap.FADE_COMPLETE, onBitmapFadeAwayComplete);
			
			
			// add bitmaps after fading away the video.
			transitionVideo.addEventListener(TransitionVideo.BUFFER_FULL, onTransitionBufferFull);
			transitionVideo.addEventListener(TransitionVideo.COMPLETE, onTransitionVideoComplete);
			transitionVideo.addEventListener(TransitionVideo.FADE_COMPLETE, onTransitionVideoFadeAwayComplete);
			addChild(transitionVideo);
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneDataChange);
			Model.addEventListener(Model.HOTSPOT_CHANGE, onHotspotDataChange);
			Model.addEventListener(Model.PANO_LOOK, onPanoLookEvent);
			Model.addEventListener(Model.SESSION_CHANGE, onSessionChange);
			Model.addEventListener(Model.GAME_TASK_COMPLETED, onGameTaskChange);
			Model.addEventListener(Model.PROGRESS_NEXT_MANUAL, onProgressNextManual);
			Model.addEventListener(Model.RESET_GAME,resetGame);
			Model.addEventListener(Model.GUIDE_UPDATE,onGuideUpdate);

			
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE, onSceneLoadingChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE, onGuideViewChange);
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditSceneChange);
			Model.state.addEventListener(State.VR_MODE_CHANGE, onVRModeChange);
			Model.state.addEventListener(State.VOLUME_CHANGE, onVolumeChange);
			Model.state.addEventListener(State.MODE_CHANGE, onModeChange);
			Model.state.addEventListener(State.CONNECT_MODE_CHANGE, onConnectModeChange);
			Model.state.addEventListener(State.MAP_SELECTED, onMapSelected);
			
			//initializeStage3d();
		}
		
		
		public function panoMouse(value:Boolean):void
		{
			if(value){
				view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				view.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				view.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
				view.addEventListener(TransformGestureEvent.GESTURE_ZOOM, onGestureZoom);
			}
			else
			{
				view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				view.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				view.removeEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
				view.removeEventListener(TransformGestureEvent.GESTURE_ZOOM, onGestureZoom);
			}
		}
		
		
		
		private var autorotateForeverDisabled:Boolean;
		private function onKeyDown(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.SPACE)
				autorotateForeverDisabled = true;
		}
		
		private function onProgressNextManual(event:Event):void
		{
			autorotateForeverDisabled = true;
		}
		
/*		private function initializeStage3d():void
		{
			Log.record("Pano.initializeStage3d()");
			// Define a new Stage3DManager for the Stage3D objects
			stage3DManager = Stage3DManager.getInstance(stage);
			
			// Create a new Stage3D proxy to contain the separate views
			stage3DProxy = stage3DManager.getFreeStage3DProxy();
			stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onContextCreated);
			stage3DProxy.antiAlias = antiAlias;	
			stage3DProxy.width = stage.stageWidth;
			stage3DProxy.height = stage.stageHeight;
			stage3DProxy.viewPort.width = stage.stageWidth;
			stage3DProxy.viewPort.height = stage.stageHeight;
			
			panoMouse(true);
		}
		
		
		private function onContextCreated(e:Event):void
		{
			//initialize();
		}*/
		
		private var initialized:Boolean;
		/**
		 * Function called when the first scene load starts
		 */
		public function initializePano(_stage3DProxy:Stage3DProxy, width, height):void
		{
			initialized = true;
			
			// Setup camera
			camera.lens.far = cameraViewDistance;
			camera.lens.near = 100;
			camera.z = 0;

			// Setup view
			view.scene = scene;
			view.camera = camera;
			view.antiAlias = antiAlias;
			view.width = width;
			view.height = height;
		
			view.stage3DProxy = stage3DProxy = _stage3DProxy;
			view.shareContext = true;
			
			addChild(view);
			
			ready = false;
			
			
			//panoMouse(true);
			
			//if(!Model.state.vr)
			//	addEventListener(Event.ENTER_FRAME, onEnterFrame);
		//	else
			//	stage3DProxy.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			
			//set qu for quaternion calculations
			qu.fromAxisAngle(Vector3D.X_AXIS,Math.PI/2);
			
			scene.addChild(listener);
			
			
			
		}
		
		private var clearSceneTimeout:uint;
		
		private var sceneLoadStartTime:int;
		
		private function onSceneLoadingChange(event:Event):void
		{
			Log.record("Pano.onSceneLoadingChange() "+Model.state.loadingScene.id);
			
			
			//loading = true;

			if(Model.state.mode == State.MODE_AUTO)
				pause();
			
		//	if(!initialized)
			//	initialize();
			
			if(sound != null)
				sound = null;
			
			// cancel loads if any
		
			
			
			// show the loading animation
			var loadingScene:SceneElementData = Model.state.loadingScene;
			var guideScene:GuideSceneData = Model.guideData
				? Model.guideData.getGuideSceneFromId(loadingScene.id)
				: null;
			
	
			
			//if(guideScene && guideScene.transitionVideo)	
			if(guideScene && guideScene.transitionVideo && !Constants.isIOS() && !Constants.isAndroid())			
			{
				transitionVideo.flvUrl = Model.config.getAssetUrl(guideScene.transitionVideo);
				//dispatchEvent(new Event(START_DRAG));// hide hte guide
				
			}
			else// if(Model.state.networkAccess)
			{
				// stop the video if its playing
				if(transitionVideo.flvUrl)
					transitionVideo.stopAndFadeSlowly(1);
				
			}
		}
		
		
		private function clearCurrentScene():void
		{
			Log.record("Pano.clearCurrentScene() ");
			// Clean up old scene
			if(_currentScene)
			{
				_currentScene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
				_currentScene.removeEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);
				_currentScene = null;
			}
			
			if(skyBoxBitmapTexture){
				skyBoxBitmapTexture.clear();
				//skyBoxBitmapTexture.dispose();
				//skyBoxBitmapTexture = null;
			}
			else if(skyBoxATFTexture.texture){
				skyBoxATFTexture.clear();
				//skyBoxATFTexture = null;
			}
			
			if(skyBox){
				if(skyBox.assetFullPath)
					skyBox.disposeAsset();
			}

			if(panoVideoMesh && !Model.currentScene.video){
				
				var currentMaterial:TextureMaterial = panoVideoMesh.material as TextureMaterial;
				var currentTexture:NativeVideoTexture = currentMaterial.texture as NativeVideoTexture;

				
				
				
				
				if(currentTexture)
				currentTexture.dispose();
				if(currentMaterial)
				currentMaterial.dispose();
				panoVideoMesh.dispose();
				panoVideoMesh = null;
				
			}
			
			for each(var s:SoundPlayer in sounds){
				s.sound3D.stop();
			}
			
			sounds.splice(0, sounds.length);
			
			if(channel){
				channel.stop();
				channel.removeEventListener(Event.SOUND_COMPLETE,onVoiceoverComplete);
				voPlaying = false;
			}
			
			// Deselect any hotspots that are showing
			if(_selectedHotspot)
				deselectHotspot();
			
			for each(var hotspot:Hotspot in hotspotDataToMesh)
			removeHotspot(hotspot)
			
			hotspotDataToMesh = new Dictionary;
			draggingHotspot = null;
			draggingHotspotPos = null;
			
			//removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			clearTimeout(hotspotSelectedTweenTimeout);
			
			mouseDragPano = false;
			
			if(Model.state.mode == State.MODE_AUTO)
				pause();
		}
		
		
		
		private function onTransitionBufferFull(event:Event):void
		{
			Log.record("Pano.onTransitionBufferFull()");
			transitionBitmap.fadeAway(0.2);
			view.visible = false;
			view.render();
		}
		
		private function onTransitionVideoComplete(event:Event):void
		{
			Log.record("Pano.onTransitionVideoComplete()");
			Tweener.addTween(transitionVideo, {
				"time" : 1, 
				"alpha": 0, 
				"transition": "linear"
			});
			progressPanel.progressBar.width = 0;
			loadingPercent = 0;
			addChild(progressPanel);
			loadSceneTiles();
			
		}
		
		private function onSceneDataChange(event:Event):void
		{
			Log.record("Pano.onSceneDataChange() ");
			//if(!initialized)
			//	initialize();
			
			//loading = true;
			
			if(!Model.guideReady)
				Model.addEventListener(Model.GUIDE_INITIALISED,onSceneDataChange);

				loadSceneTiles();
			
			if(_selectedHotspot)
				_selectedHotspot = null;
	
		}
		

		
		private function loadSceneTiles():void
		{
			Log.record("Pano.loadSceneTiles()");
			
			//ready = false;

			skyBoxATFTexture = new SkyboxATFTexture;
			// add the listener here, because in one instance, don't listen for event.
			skyBoxATFTexture.addEventListener(SkyboxATFTexture.TEXTURE_LOADED, onPanoTilesLoaded);
			skyBoxATFTexture.addEventListener(SkyboxATFTexture.TEXTURE_FAILED, onATFTextureFail);
			skyBoxATFTexture.scene = Model.currentScene;

			
		}
		
		private function onATFTextureFail(e:Event):void
		{
			Log.record("Pano.onTextureFail");

			skyBoxATFTexture = null;
			skyBoxBitmapTexture = new BitmapCubeTextureDeep;
			skyBoxBitmapTexture.addEventListener(BitmapCubeTextureDeep.SCENE_BASIC_LOADED, onPanoTilesLoaded);
			skyBoxBitmapTexture.addEventListener(BitmapCubeTextureDeep.SCENE_DEEP_LOADED, onPanoDeepTilesLoaded);
			
			skyBoxBitmapTexture.scene =  Model.currentScene;

		}
		
		
		private function dispatchTilesLoaded():void
		{
			Log.record("Pano.dispatchTilesLoaded()");
			skyBoxBitmapTexture.dispatchEvent(new Event(BitmapCubeTextureDeep.SCENE_BASIC_LOADED));
			
		}
		
	
		private function onPanoTilesLoaded(event:Event = null):void
		{
			Log.record("Pano.onPanoTilesLoaded() ");
			
			if(_currentScene)
				clearCurrentScene();
			
			setTimeout(loadNextPano,10);
		}
			
			
		private function loadNextPano():void
		{
		
			if(skyBoxATFTexture){
				if(skyBoxATFTexture.texture){
					skyBox = new SkyBox(skyBoxATFTexture.texture);
					skyBoxATFTexture.removeEventListener(SkyboxATFTexture.TEXTURE_LOADED, onPanoTilesLoaded);
					skyBoxATFTexture.removeEventListener(SkyboxATFTexture.TEXTURE_FAILED, onATFTextureFail);
				}
			}
			else if(skyBoxBitmapTexture){
				skyBox = new SkyBox(skyBoxBitmapTexture);
				skyBoxBitmapTexture.removeEventListener(BitmapCubeTextureDeep.SCENE_BASIC_LOADED, onPanoTilesLoaded);

			}
			
			
			if(!scene.contains(skyBox))
			scene.addChild(skyBox);
			
			skyBox.material.mipmap = false;

			setTimeout(panoTilesLoaded,200);
		}
		
		
		private function panoTilesLoaded():void
		{
			Log.record("Pano.panoTilesLoaded()");
		
			//ready = false;

						
			//skyBoxTexture.removeEventListener(BitmapCubeTextureDeep.SCENE_BASIC_LOADED, onPanoTilesLoaded);
			
			_currentScene = Model.currentScene;
			_currentScene.addEventListener(SceneData.PANO_UPDATED, onPanoIncrementChange);
			_currentScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			_currentScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);
			
		
			
			if(Model.config.guideId)
			{
				_currentGuideScene = Model.guideScene;
				
				if(_currentGuideScene)
				{
					_currentGuideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
					_currentGuideScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);
					_currentGuideScene.addEventListener(GuideSceneData.GUIDE_SCENE_CHANGED,onGuideSceneChanged);
				}
			}
			
			
			// Due to difference between this pano and the last,
			// Pan values need to have 180 removed.
			// Tilt values need to be reversed.
			// This value will be removed when loading... here.
			_pan = (_currentScene.initialLook.pan + 180)%360;
			_tilt = -_currentScene.initialLook.tilt;
			
			//set default for Quaternion calculations
			if(Model.state.connectMode != State.CONNECT_MODE_RECEIVE)
				if(!Model.state.vr){
					cameraController = new HoverController(camera, null, _pan, _tilt, 0.1);
					cameraController.steps = 50;// Default to slow
					currentPanAngle = _pan;
				}
				else{
					var offset = pan-90 > 0 ? pan-90 : pan-90 + 360;
					qi.fromAxisAngle(Vector3D.Y_AXIS, RAD(offset));
				}
			
			zoom = _currentScene.initialLook.fov * 0.65;// Magic number to try to replicate old pano
			
			//view.render();
			
		//	if(Model.state.vr)
			//	rightEye.render();
			
			ready = true;
			//loading = false;
			
			setTimeout(fadeToPano,200);
		}
		
		
		private function onPanoDeepTilesLoaded(event:Event = null):void
		{
			Log.record("Pano.PanoDeepTilesLoaded() ");
			dispatchEvent(new Event(PANO_DEEP_TILES_LOADED));
			skyBoxBitmapTexture.removeEventListener(BitmapCubeTextureDeep.SCENE_DEEP_LOADED, onPanoDeepTilesLoaded);
			skyBoxBitmapTexture.clearFinalListeners();
		}
		
		/**
		 * new pano has been uploaded, reload the scene tiles without triggering event listeners.
		 */
		private function onPanoIncrementChange(event:Event):void
		{
			skyBoxBitmapTexture.scene = Model.currentScene;
		}
		

		private function fadeToPano():void
		{
			Log.record("Pano.fadeToPano()");
		
			dispatchEvent(new Event(PANO_TILES_LOADED));
			Model.state.dispatchEvent(new Event(State.PANO_TILES_LOADED));
			
			if(Model.state.mode == State.MODE_GYRO)
				if(Model.state.connected && Model.state.connectMode != State.CONNECT_MODE_RECEIVE)
					cameraController.autoUpdate = false;

			addHotspots();
			if(Model.currentScene.video){
				loadPanoVideo();
			}
			
			
		}
		
		private function onBitmapFadeAwayComplete(event:Event):void
		{
			Log.record("Pano.onBitmapFadeAwayComplete()");
			// fading while viewing a video is triggering this.
			if(!Model.guideScene || Model.guideScene.transitionVideo == null)
				addHotspots();
		}
		
		private function onTransitionVideoFadeAwayComplete(event:Event):void
		{
			Log.record("Pano.onTransitionVideoFadeAwayComplete()");
			addHotspots();
		}
		
		
		private function addHotspots():void
		{
			Log.record("Pano.addHotspots()");
			loading = false;
			
			var hotspots:Vector.<HotspotData> = Model.hotspotsMerged;
			for(var i:int=0; i<hotspots.length; i++)
				addHotspot(hotspots[i]);
			
			showHotspotFromModel();
			
			if(Model.state.vr) checkForHotspotInCentre();
			voiceover();
			dispatchEvent(new Event(PANO_HOSPOTS_READY));
			
			interrupted = false;

		}
		
		
		
		private function onSessionChange(event:Event):void
		{
			//stopAutorotate();
		}
		
		private function onGameTaskChange(event:Event):void
		{
			Log.record("Pano.onGameTaskChange()");
			if(Model.currentTask && Model.loggedIn)
				updateHotspots(Model.currentTask.hotspotsToShow, true);
			
			if(Model.currentTask)
				setTimeout(updateHotspots,4000,Model.currentTask.hotspotsToShow, false);
		}
		
		private function updateHotspots(hotspots:Array, hidden:Boolean):void
		{
			Log.record("Pano.updateHotspots(hotspots:"+hotspots+", hidden:"+hidden+")");
			for(var i:int; i<hotspots.length; i++)
			{
				var hotspot:HotspotData = Model.getHotspot(GameTaskHotspot(hotspots[i]).id);
				if(hotspot)
					Hotspot(hotspotDataToMesh[hotspot]).hidden = hidden
			}
		}
		
		private function onEditSceneChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE){
				updateAllHotspotsVisible();
				
				if(panoTexture2DBase)
					panoTexture2DBase.player.pause();
			}
				
			else{
				updateHotspotsToGamePos();
				
				if(panoTexture2DBase)
					panoTexture2DBase.player.play();
			}
		}
		
		private function updateHotspotsToGamePos():void
		{
			if(Model.currentTask && Model.loggedIn)
				updateHotspots(Model.pastHotspotsToShow, false);
			
			if(Model.prevTask)
				updateHotspots(Model.futureHotspotsToShow, true);
		}
		
		private function updateAllHotspotsVisible():void
		{
			for each(var hotspot:Hotspot in hotspotDataToMesh)
			hotspot.hidden = false;
		}
		
		public function queueAutoRotate(millis:int = 3000):void
		{
			Log.record("Pano.queueAutoRotate(millis:int = "+millis+")");
			if(Model.loggedIn
				|| loading
				|| Model.hotspot != null
				|| cameraController == null
				|| autorotateForeverDisabled)
				return;
			
			// no autoplay when game is in progress
			if(Model.isPlayingGame)
				return;
			
			clearTimeout(autorotateTimeout);
			autorotateTimeout = setTimeout(startAutoRotate, millis);
		}
		
		
		private var rotateDegrees:Number = 0;
		
		private function startAutoRotate():void
		{
			Log.record("Pano.startAutoRotate()");

			if(Model.isPlayingGame || Model.state.connected || cameraController == null || Model.guideScene.voiceoverTrack != null)
				return;
			
			interrupted = false;
			
	
			cameraController.steps = 30;
			
			
			var links:Array = [];
			links = Model.guideScene ? Model.guideScene.links : null;
			if(links == null || links.length == 0)
			{
				rotateDegrees = 0;
				
				addEventListener(Event.ENTER_FRAME, onAutoRotateEnterFrame);
				
			}
			else 
				Model.progressNext();
			
			dispatchEvent(new Event(START_AUTOROTATE));
		}
		
		public function stopAutoRotate():void
		{
			Log.record("Pano.stopAutorotate()");
			removeEventListener(Event.ENTER_FRAME, onAutoRotateEnterFrame);
			rotateDegrees = 0;
			
			if(cameraController)
				cameraController.steps = 50;
			dispatchEvent(new Event(STOP_AUTOROTATE));
			interrupted = true;
		}
		
		public function pause():void
		{
			removeEventListener(Event.ENTER_FRAME, onAutoRotateEnterFrame);
			rotateDegrees = 0;
			dispatchEvent(new Event(PAUSE_AUTOROTATE));
			interrupted = true;
			
		}
		
		
		private function onAutoRotateEnterFrame(event:Event):void
		{
			cameraController.tiltAngle /= 1.01;
			cameraController.panAngle += 0.1;
			
			rotateDegrees += 0.1;
			
			if(Model.guideScene && rotateDegrees > 180)
				Model.progressNext();
		}
		
		
		
		private function onGuideViewChange(event:Event):void
		{
			Log.record("Pano.onGuideViewChange()");
			if(loading)
				return;
			
		if(Model.state.guideView == State.GUIDE_VIEW_SECTION){ 
			if(Model.state.mode == State.MODE_AUTO)
					stopAutoRotate()
			}
		else if(Model.state.guideView != State.GUIDE_VIEW_SCENE)
			if(Model.state.mode == State.MODE_AUTO && !interrupted)
				startAutoRotate();
		}
		
		private function removeBlurredBitmap():void
		{
			transitionBitmap.clear();
			
		}
		
		
		
		/*private function flipBitmapData(original:BitmapData, axis:String = "x"):BitmapData
		{
		var flipped:BitmapData = new BitmapData(original.width, original.height, true, 0);
		var matrix:Matrix
		if(axis == "x"){
		matrix = new Matrix( -1, 0, 0, 1, original.width, 0);
		} else {
		matrix = new Matrix( 1, 0, 0, -1, 0, original.height);
		}
		flipped.draw(original, matrix, null, null, null, true);
		return flipped;
		}
		
		private var flip:Point = new Point;
		private var render:Shape = new Shape;
		private var front:BitmapData;
		private var back:BitmapData;
		
		private function turnPageForward():void{
		flip = new Point(view.width, view.height);
		Tweener.addTween(flip, {
		x:-view.width,
		y:view.height,
		_bezier:{x:0, y:view.height/2},
		time:3,
		transition:"easeinoutquad",
		onUpdate:drawPage,
		onComplete:onPageDone
		});
		drawPage();
		}
		
		private function drawPage():void{
		render.graphics.clear()
		var o:Object = PageFlip.computeFlip(flip,// flipped point
		new Point(1,1),// of bottom-right corner
		view.width,// size of the sheet
		view.height,
		true,// in horizontal mode
		1);// sensibility to one sensibility to one
		PageFlip.drawBitmapSheet(o,// computeflip returned object
		render,// target
		front,// bitmap page 0
		back);// bitmap page 1
		}
		
		
		
		private function onPageDone():void{
		Log.record("onPageDone()");
		removeChild(render);
		}*/
		
		private function onHotspotAdded(event:HotspotEvent):void
		{
			addHotspot(event.hotspot);
		}
		
		private function addHotspot(data:HotspotData):void
		{
			var hotspot:Hotspot = createHotspot(data);
			
			
			if(!hotspot)
				return;
			
			//update the sound player distance for Sound3D to work
			if(hotspot is SoundPlayer){
				var sound:SoundPlayer = hotspot as SoundPlayer;
				data.distance = (1 - sound.ambiance) * 2000;
				sounds.push(sound);
			}
			
			updateHotspotPosition(hotspot, data);
			
			if(sound){
				sound.update();
				sound = null;
			}
			
			hotspot.mouseEnabled = true;
			if(Model.config.guideId)
				hotspot.hidden = Model.gameRevealsHotspot(data) || (Model.guideScene && Model.guideScene.isHidden(data));
			else
				hotspot.hidden = false;
			hotspot.addEventListener(MouseEvent3D.MOUSE_DOWN, onHotspotMouseDown);
			hotspot.addEventListener(MouseEvent3D.MOUSE_UP, onHotspotMouseUp);
			hotspot.addEventListener(Hotspot.HOTSPOT_CENTRED,onHotspotCentred);
			hotspot.addEventListener(Hotspot.HOTSPOT_OFF_CENTRE,onHotspotOffCentre);
			
			
			if(hotspot is Photo)
				hotspot.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb),
					hotspot.addEventListener(HotspotEvent.HOTSPOT_CLOSE_WEB, onHotspotCloseWeb);
			
			
			Log.record("ADD HOTSPOT: "+ hotspot.data.title);
			
			scene.addChild(hotspot);
			
			hotspotDataToMesh[data] = hotspot;
			
			data.addEventListener(HotspotData.ORIENTATION_UPDATED, onHotspotOrientationUpdated);
		}
		
		
		private function onHotspotShowWeb(event:HotspotEvent):void
		{
			var data:HotspotData = event.hotspot;
			data.inWorldHeight = event.hotspot.inWorldHeight;
			data.inWorldWidth = event.hotspot.inWorldWidth;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_SHOW_WEB, data));	
		}
		
		private function onHotspotCloseWeb(event:HotspotEvent):void
		{
			var data:HotspotData = event.hotspot;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLOSE_WEB, data));	
		}
		
		private function removeNonGuideBubbles():void
		{
			
		}
		
		private function onHotspotOrientationUpdated(event:Event):void
		{
			var data:HotspotData = HotspotData(event.target);
			updateHotspotPosition(hotspotDataToMesh[data], data);
		}
		
		private function updateHotspotPosition(hotspot:Hotspot, data:HotspotData):void
		{
			hotspot.rotationX = data.rotationX - 90;
			hotspot.rotationY = data.rotationY;
			
			var distance:Number = Math.min(data.distance * hotspot.distanceRatio, 50000);
			var pr:Number = -1 * (data.panoPos.pan - 90) * MathUtil.TO_RADIANS;
			var tr:Number = data.panoPos.tilt * MathUtil.TO_RADIANS;
			
			hotspot.x = distance * Math.cos(pr) * Math.cos(tr);
			hotspot.y = distance * Math.sin(tr);
			hotspot.z = distance * Math.sin(pr) * Math.cos(tr);
			hotspot.scaleX = hotspot.scaleY = hotspot.scaleZ = hotspot.distanceRatio;
		}
		
		private function createHotspot(data:HotspotData):Hotspot
		{
			if(data is BubbleData){
				return new Bubble(data as BubbleData, new Bubble2Fl);
			}
			
			if(data is SoundData)
				return new SoundPlayer(data as SoundData, new SoundPlayerFl, listener);
			
			if(Model.state.vr)
				return null;
			
			if(data is PhotoData)
				return new Photo(data as PhotoData, new PhotoContainerFl);
			
			if(data is BoardData)
				return new Board(data as BoardData, new BoardFl);
			
			if(data is VideoData)
				return new VideoPlayer(data as VideoData, new TelevisionContainerFl);
			
			if(data is PosterData)
				return new Poster(data as PosterData, new PosterContainerFl);
			
			
			if(data is RssReaderData)
				return new RssReader(data as RssReaderData, new RssReaderFl);
			
			return null;
		}
		
		private function onHotspotRemoved(event:HotspotEvent):void
		{
			if(hotspotDataToMesh[event.hotspot] is Hotspot)
				removeHotspot(hotspotDataToMesh[event.hotspot] as Hotspot);
		}
		
		private function removeHotspot(hotspot:Hotspot):void
		{
			delete hotspotDataToMesh[hotspot.data];
			scene.removeChild(hotspot);
			hotspot.data.removeEventListener(HotspotData.ORIENTATION_UPDATED, onHotspotOrientationUpdated);
			
			hotspot.removeEventListener(MouseEvent3D.MOUSE_DOWN, onHotspotMouseDown);
			hotspot.removeEventListener(MouseEvent3D.MOUSE_UP, onHotspotMouseUp);
			
			hotspot.dispose();
		}
		
		
		private function onHotspotMouseDown(event:MouseEvent3D):void
		{
			Log.record("Pano.onHotspotMouseDown()");
			if(interactionDisabled)
				return;
			
			pause();
			
			mouseDownHotspot = event.target as Hotspot;
			
			//Makes hotspot flicker when clicked
			mouseDownHotspot.scale(1.05);
			
			
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLICKED, mouseDownHotspot.data));
			
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
			{
				draggingHotspot = event.target as Hotspot;
				draggingHotspotPos = draggingHotspot.data.panoPos;
				
				if(draggingHotspot.adminColor != 0xFFFFFF)
					dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLICKED, draggingHotspot.data));
			}
			
		}
		
		private function get interactionDisabled():Boolean
		{
			// if we are loading, do nothing;
			if(loading)
				return true;
			
			if(getTimer() - startShowHotspotTime < 500)
				return true;
			
			if(_selectedHotspot is Photo && Photo(_selectedHotspot).loadsScene)
				return true;
			
			if(_selectedHotspot is Bubble)
				return true;
			
			return false;
		}
		
		private function onHotspotMouseUp(event:MouseEvent3D):void
		{
			Log.record("Pano.onHotspotMouseUp()");
			var hotspot:Hotspot = Hotspot(event.target);
			
			if(Model.state.connectMode == State.CONNECT_MODE_RECEIVE 
				&& Model.state.connected)
				return;
			
			// so draging the pano does not trigger hotspots
			if(hotspot != mouseDownHotspot)
				return;
			
			interrupted = false;
			
			hotspot.scale(0.95);
			
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLICKED, hotspot.data));
			
			
			mouseDownHotspot = null;
			draggingHotspot = null;
			draggingHotspotPos = null;
			
			
			
			// if the mouse moved a bit too much
			if(MathUtil.delta(lastMouseX, stage.mouseX) > 20
				&& MathUtil.delta(lastMouseY, stage.mouseY) > 20)
				return;
			
			// clicking while selected does nothing
			if(_selectedHotspot)
				return;
			
			
			if(Model.state.uiState != State.UI_STATE_EDITING_SCENE)
				Model.hotspot = hotspot.data;
			
		}
		
		private function onPanoLookEvent(event:PanoLookEvent):void
		{
			Log.record("Pano.onPanoLookEvent()");
			
			if(interrupted)
				return;
			
			if(Model.hotspot)
			{
				Model.hotspot = null;
				tweenToHotspotTimeout = setTimeout(tweenToPanoLook, 800, event.panoLook);
			}
			else
			{
				tweenToPanoLook(event.panoLook);
			}
		}
		
		private function tweenToPanoLook(panoLook:PanoLook,proceed:Boolean = false):void
		{
			Log.record("Pano.tweenToPanoLook()");
			if(interrupted && !proceed){interrupted = false; return};
			cameraController.wrapPanAngle = true;
			cameraController.steps = 100;
			cameraController.panAngle = panoLook.pan;
			cameraController.tiltAngle = panoLook.tilt;
			
			PanoLookCheckPanComplete(panoLook);
		}
		
		private function PanoLookCheckPanComplete(panoLook:PanoLook):void
		{
			var panComplete:Boolean = false;
			var panoLookPan: int = panoLook.pan;
			var currentPan:Number = cameraController.currentPanAngle;
			
			if (panoLookPan < 0)
				panoLookPan = (panoLookPan % 360) + 360;
			else
				panoLookPan = panoLookPan % 360;
			
			if(panoLookPan < currentPan + 30 && panoLookPan > currentPan - 30){
				tweenToPanoLookZoom(panoLook);
				panComplete = true;
			}
			else if(!panComplete && Model.state.mode == State.MODE_AUTO && !interrupted)
				setTimeout(PanoLookCheckPanComplete,100,panoLook);
		}
		
		private var currentFov:int;
		
		private function tweenToPanoLookZoom(panoLook:PanoLook):void
		{
			Log.record("Pano.tweenToPanoLookZoom()" + interrupted);
			
			if(interrupted){interrupted = false; return};
			
			currentFov = this.zoom;
			
			Tweener.addTween(this, {
				time:2,
				zoom: panoLook.fov,
				transition: "easeInCubic",
				onComplete: tweenToPanoLookZoomBack
			});
		}
		
		private function tweenToPanoLookZoomBack():void
		{
			Log.record("Pano.tweenToPanoLookZoomBack()");
			
			if(interrupted){
				interrupted = false;
				return;
			}
			
			Tweener.addTween(this, {
				time:2,
				delay: 2,
				zoom: currentFov,
				transition: "easeInCubic"
			});
			
			if(Model.state.mode == State.MODE_AUTO)
				setTimeout(Model.progressNext,1000);
		}
		
		
		
		private function onHotspotDataChange(event:Event):void
		{
			Log.record("Pano.onHotspotDataChange()");
			if(interrupted){interrupted = false; return};
			showHotspotFromModel();
		}
		
		private function showHotspotFromModel():void
		{
			
			Log.record("Pano.showHotspotFromModel()" + interrupted);
			
			if(hotspotDataToMesh[Model.hotspot]){
				showSelectedHotpot();
			}
				
			else 
				deselectAndTweenBack()
		}
		
		private var tweenToHotspotTimeout:uint;
		
		private function showSelectedHotpot():void
		{
			
			Log.record("Pano.showSelectedHotpot()" + _selectedHotspot);
			
			if(interrupted){interrupted = false; return};
			
			if(_selectedHotspot)
			{
				deselectAndTweenBack();
				tweenToHotspotTimeout = setTimeout(tweenToSelectHotspot, 800);
			}
			else
			{
				tweenToSelectHotspot();
			}
		}
		
		private function tweenToSelectHotspot():void
		{
			Log.record("Pano.tweenToSelectHotspot()" + interrupted);
			var hotspotData:HotspotData = Model.hotspot;
			
			if(interrupted){interrupted = false; return};
			
			if(!_selectedHotspot)
				zoomBeforeHotspot = zoom;
			
			// Tween the camera towards the hotspot
			var newPan:Number = hotspotData.panoPos.pan + 180 %360;
			var delay:Number; 
			
			if(!Model.state.vr){
				if(Model.state.mode != State.MODE_AUTO)
					cameraController.steps = 10;
				else
					cameraController.steps = 100;
				
				cameraController.wrapPanAngle = true;
				cameraController.panAngle = newPan;
				cameraController.tiltAngle = -hotspotData.panoPos.tilt/2;
			}
			else
				tweenToSelectHotspotZoom();
			
			
			
			Model.registerHotspotClicked(hotspotData);
			
			_selectedHotspot = hotspotDataToMesh[hotspotData];
			
			
			
			
			if(!Model.state.vr)
				HotspotCheckPanComplete(newPan);
			
		}
		
		
		private function HotspotCheckPanComplete(newPan:Number):void
		{
			var panComplete:Boolean = false;
			var currentPan:Number = cameraController.currentPanAngle;
			
			if(newPan < 0)
				newPan += 360;
			if(newPan > 360)
				newPan -= 360;
			
			if(newPan < currentPan + 30 && newPan > currentPan - 30 && _selectedHotspot)
				tweenToSelectHotspotZoom();
			if(newPan < currentPan + 10 && newPan > currentPan - 10 && _selectedHotspot){
				tweenToSelectHotspotRotate();
				panComplete = true;
			}
			else if(!panComplete && _selectedHotspot)
				setTimeout(HotspotCheckPanComplete,10,newPan);
		}
		
		
		
		private function tweenToSelectHotspotZoom():void
		{
			Log.record("Pano.tweenToSelectHotspotZoom()");
			if(!_selectedHotspot || !Model.hotspot)
				return;
			
			var widthToDistance:Number = view.width/Model.hotspot.distance;
			var newZoom:Number = Math.atan(widthToDistance) * MathUtil.TO_DEGREES * 1.8;
			var zoomTime: Number;
			
			if (Model.state.mode == State.MODE_AUTO)
				zoomTime = 2
			else
				zoomTime = 1

			if(newZoom < zoom)
			{
				Tweener.removeTweens(this);
				Tweener.addTween(this, {
					time:zoomTime,
					transition: "easeInCubic",
					zoom: newZoom
				});
			}
			
		}
		
		
		private function tweenToSelectHotspotRotate():void
		{
			Log.record("tweenToSelectHotspotRotate()");
			if(!_selectedHotspot || !Model.hotspot)
				return;
			
			if(_selectedHotspot is Bubble){
				_selectedHotspot.selected  = true;
				return;
			}
			
			Tweener.addTween(_selectedHotspot, {
				time:1,
				rotationX: -Model.hotspot.panoPos.tilt-90,
				rotationY: Model.hotspot.panoPos.pan,
				transition: "easeInCubic",
				onComplete: tweenToSelectHotspotComplete
			});
			
			if(_selectedHotspot is PhotoData)
				Tweener.addTween(_selectedHotspot, {time:1.2, photoScale: 2});
			
		}
		
		
		private function tweenToSelectHotspotComplete():void
		{
			Log.record("Pano.tweenToSelectHotspotComplete");
			if(!_selectedHotspot)
				return;
			
			var offsetLeft:Number = 0;
			if(Model.state.guideView == State.GUIDE_VIEW_SCENE);// || Model.state.guideView == State.GUIDE_VIEW_TASK)
				offsetLeft = -width/2;
			
			_selectedHotspot.ui.x = Math.round((view.width || view.width)/2 - (_selectedHotspot.ui.width - offsetLeft)/2);
			_selectedHotspot.ui.y = Math.round(view.height/2 || view.width/2 - _selectedHotspot.ui.height/2);
			
			if(_selectedHotspot.ui.y < 10){
				_selectedHotspot.ui.y = 10;
				_selectedHotspot.ui.addEventListener(MouseEvent.MOUSE_DOWN,onHotspotDrag);	
				_selectedHotspot.ui.addEventListener(MouseEvent.MOUSE_UP,onHotspotDragUp);	
			}
			
			if(Model.hotspot)
				if(!Model.hotspot.selectionEnabled)
				{
					if(_selectedHotspot is Photo && Photo(_selectedHotspot).loadsScene)
						Photo(_selectedHotspot).loadSceneDelayed(500);
					else if(Model.state.mode == State.MODE_AUTO)
						setTimeout(deselectAndTweenBack,1000);
					
					return;
				}
			
			
			// addchild first so stage is set
			
			stage.addChild(_selectedHotspot.ui);
			_selectedHotspot.visible = false;
			_selectedHotspot.selected = true;
			
			
			transitionBitmap.bitmapData = bitmapData;
			
			// wait for hotspot transitions to end, then blur over one second
			hotspotSelectedTweenTimeout = setTimeout(
				transitionBitmap.blurSlowly,
				_selectedHotspot.transitionMillis,
				1
			);
			
			
			
			var delay:Number = 3000;
			
			if(_selectedHotspot is Photo){
				var thisPhoto:PhotoData = _selectedHotspot.data as PhotoData;
				if(thisPhoto.canFlip){
					delay = delay + (thisPhoto.description.length * 10) + 3000;
				}
			}
			else if (_selectedHotspot is Poster){
				var thisPoster:PosterData = _selectedHotspot.data as PosterData;
				delay = delay + (thisPoster.text.length * 10);
			} 
			
			if(Model.state.mode == State.MODE_AUTO)
				setTimeout(deselectAndTweenBack,delay);
		}
		
		
		
		
		private function deselectAndTweenBack():void
		{
			Log.record("Pano.deselectAndTweenBack()");
			if(!_selectedHotspot)
				return;
			
			var hotspot:Hotspot = _selectedHotspot;
			
			deselectHotspot();
			removeBlurredBitmap();
			
			var z:Number = zoom;
			Tweener.removeTweens(this);
			Tweener.addTween(this, {
				time:1,
				transition: "easeInCubic",
				zoom: zoomBeforeHotspot
			});
			
			// Tween the hotspot back to its rotated position
			Tweener.removeTweens(hotspot);
			
			if(hotspot.data)
				Tweener.addTween(hotspot, {
					time:0.7,
					
					rotationX : hotspot.data.rotationX - 90,
					rotationY : hotspot.data.rotationY,
					onComplete: onDeselectHotspotComplete
				});
			
			cameraController.steps = 50;
		}
		
		private function deselectHotspot():void
		{
			Log.record("pano.deSelectHotspot()");
			var found:Boolean = Model.allHotspotsFound.indexOf(_selectedHotspot.data) > -1;
			if(!_selectedHotspot)
				return;
			
			_selectedHotspot.visible = true;
			_selectedHotspot.selected = false;
			
			// deselected before tween in was complete
			if(_selectedHotspot.ui)
				if(_selectedHotspot.ui.parent == stage)
					stage.removeChild(_selectedHotspot.ui);
			
			if(_selectedHotspot is Photo)
				dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLOSE_WEB, _selectedHotspot.data));
			
			/*	if(Model.isPlayingGame && found && Model.allHotspotsFound.length == Model.currentTask.hotspotsToFind.length && Model.currentTask.hotspotsToShow.length > -1)
			_selectedHotspot.visible = false;*/
			
			
		}
		
		private function onDeselectHotspotComplete():void
		{
			Log.record("pano.onDeselectHotspotComplete()");
			if(!_selectedHotspot)
				return;
			
			_selectedHotspot = null;
			
			if(Model.state.mode == State.MODE_AUTO)
				Model.progressNext();
			
		}
		
		/*** Render handler */
		public function onEnterFrame(e:Event):void
		{
			// Handle hover camera
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE && draggingHotspot && draggingHotspot.adminColor != 0xFFFFFF)
			{
				draggingHotspot.data.pan = draggingHotspotPos.pan + 0.1 * (stage.mouseX - lastMouseX);
				draggingHotspot.data.tilt = draggingHotspotPos.tilt - 0.1 * (stage.mouseY - lastMouseY);
			}
			else if(mouseDragPano && cameraController && Model.state.mode != State.MODE_GYRO)
			{
				if(Math.abs(stage.mouseX - lastMouseX) > Math.abs(stage.mouseY - lastMouseY))
					cameraController.panAngle = currentPanAngle =  lastPanAngle - 0.2 * (stage.mouseX - lastMouseX)/2;
				else
					cameraController.tiltAngle = currentTiltAngle = lastTiltAngle - 0.1 * (stage.mouseY - lastMouseY)/3;
			}
			
			if(cameraController && ready && !loading){
				
				view.render();
				
				if(Model.state.vr && rightEye)
					rightEye.render();
			}
			
			
			updateSounds();

			if(Model.state.vr)
				checkForHotspotInCentre()
		}	
		
		
		private function checkForHotspotInCentre():void
		{
			

			
			//if camera moves away break loading
			if(hotspotInCentre ){
				if(Math.abs(hotspotInCentre.pan - camera.eulers.x) > 5){
					hotspotInCentre.center = false;
					hotspotInCentre = null;
				}
			}
			
			//ray caster to find hotspots
			var ray:RaycastPicker = new RaycastPicker(true);
			var object:PickingCollisionVO = ray.getViewCollision(width/4,height/2,view);
			
			if(object && !Model.hotspot){
				var hs:Bubble = object.entity as Bubble;
				if(hotspotInCentre == hs)
					return;
				
				//centre hotspot to add timer
				hs.center = true;
				hotspotInCentre = hs;
				hotspotInCentre.pan = camera.eulers.x;
			};
			
			
			
		}
		var pos:Vector3D;
		
		private function updateSounds():void
		{
			pos = camera.forwardVector;
			pos.scaleBy(200); 
			listener.position = pos;
			listener.inverseSceneTransform;
		}
		
		
		/*** Mouse down handler */
		public function onMouseDown(event:MouseEvent):void
		{
			Log.record("Pano.onMouseDown(event:MouseEvent)");
			if(interactionDisabled || Model.state.vr)
				return;
			
			event.stopImmediatePropagation();
			
			active = true;
			interrupted = true;
			Tweener.removeTweens(this);
			
			pause();
			
			if(_selectedHotspot)
			{
				Model.hotspot = null;
				deselectAndTweenBack();
			}
			
		/*	if(Model.currentScene.video && panoTexture2DBase){
				if(!panoTexture2DBase.player.playing)
				panoTexture2DBase.player.play();
			}*/
			
			
			//reset pan angle if changing from gyro controls
			if(Model.isMobile 
				&& cameraController.panAngle != currentPanAngle
				&& Model.state.mode == State.MODE_GYRO
			)
				cameraController = new HoverController(camera, null, currentPanAngle, 0, 0.1);
			
			
			Model.state.mode = State.MODE_DRAG;
			
			
			if(cameraController)
			{
				cameraController.wrapPanAngle = false;
				cameraController.autoUpdate = true;
				cameraController.steps = 50;
				lastPanAngle = cameraController.panAngle;
				lastTiltAngle = cameraController.tiltAngle;
			}			
			
			
			draggingHotspot = null;
			
			lastMouseX = stage.mouseX;
			lastMouseY = stage.mouseY;
			mouseDragPano = true;
			
			//stage.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
			
			//dispatchEvent(new Event(PANO_MOUSE_DOWN));
			//Model.state.dispatchEvent(new Event(State.PANO_MOVE));
			setTimeout(dispatchDragEvent, 200);
		}
		
		private function dispatchDragEvent():void
		{
			if(mouseDragPano || hotspotDataToMesh[Model.hotspot])
				dispatchEvent(new Event(START_DRAG));
			
		}
		
		/*** Mouse stage leave handler */
		private function onStageMouseLeave(e:Event):void
		{
			mouseDragPano = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
		
		/*** Mouse up handler */
		public function onMouseUp(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			
			Log.record("Pano.onMouseUp(event:MouseEvent)");
			/*			if(interactionDisabled)
			return;*/
			
			
/*			if(Model.state.connectMode == State.CONNECT_MODE_RECEIVE 
				&& Model.state.connected)
				return;*/
			
			if(!cameraController)
				return;
			
			cameraController.steps = 50;
			// if no hotspot was mouse downed since pano registered mouse down
			if(draggingHotspot == null)
				Model.hotspot = null;
			
			//dispatchEvent(new Event(STOP_DRAG));
		//	Model.state.dispatchEvent(new Event(State.PANO_STOP));
			
			qi.fromAxisAngle(Vector3D.Y_AXIS, cameraController.currentPanAngle * (Math.PI / 180));
			
			//cameraController.steps = 50; this made it tween for ages
			draggingHotspot = null;
			draggingHotspotPos = null;
			mouseDragPano = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
			
			if(Model.state.uiState == State.UI_STATE_NORMAL)
				Model.registerGameEvent(GameTaskData.GAME_PANO_DRAGGED)
			else if(Model.state.uiState == State.UI_STATE_EDITING_VO)
				Model.dispatchPanoLookEvent(panoLook)
			
		}
		
		private function onMouseWheel(event:MouseEvent):void
		{
			Log.record("Pano.onMouseWheel(event:"+event+")");
			Tweener.removeTweens(this);
			Tweener.addTween(this, {
				time:0.3,
				zoom: zoom - event.delta
			});
		}
		
		
		private var yOffset:int;
		
		private function onHotspotDrag(e:MouseEvent):void
		{
			Log.record("Pano.onHotspotDrag()");
			yOffset = mouseY - _selectedHotspot.ui.y; 
			view.addEventListener(MouseEvent.MOUSE_MOVE, handlemove);
		}
		
		private function onHotspotDragUp(e:MouseEvent):void
		{
			Log.record("Pano.onHotspotDrag()");
			view.removeEventListener(MouseEvent.MOUSE_MOVE, handlemove);
		}
		
		public	function handlemove(e:MouseEvent):void 
		{ 
			Log.record("Pano.handlemove()");
			if(_selectedHotspot)
				Tweener.addTween(_selectedHotspot.ui, {y :mouseY - yOffset, time: 0.3});
			else
				view.removeEventListener(MouseEvent.MOUSE_MOVE, handlemove);
		}
		
		
		private function onGestureZoom(event:TransformGestureEvent):void
		{
			Log.record("Pano.onGestureZoom(event:"+event+")");
			zoom /= Math.max(event.scaleX, event.scaleY);
			mouseDragPano = false;
		}
		
		var _width:Number;
		var sized:Boolean = false;
		
		public function resize(width:Number, height:Number):void
		{
			if(sized && !Model.isDesktop)
				return;
			
			if(Model.state.vr)
				return;

			view.width = width;
			_width = width;
			view.height = height;
			
			sized = true;
			
			transitionVideo.resize(width, height);
		}
		
		/**
		 * Functions used in the admin
		 */
		public function get zoom():Number
		{
			return PerspectiveLens(camera.lens).fieldOfView;
		}
		
		public function mobileAppScale(value:Number):void
		{
			appScale = value;
		}
		
		public function set zoom(value:Number):void
		{
			var zoom:Number =  Math.min(Math.max(value, 20), 100);
			PerspectiveLens(view.camera.lens).fieldOfView = zoom
			
			if(Model.state.vr)
				PerspectiveLens(rightEye.camera.lens).fieldOfView = zoom;
			//Model.state.zoom = zoom;
		}
		
		private var _bitmapData:BitmapData;
		
		public function queueSnapshot():void
		{
			var bm:BitmapData = new BitmapData(view.width, view.height, false);
			view.stage3DProxy.queueSnapshot(bm); 
			view.stage3DProxy.addEventListener("snapshotReady", function(){
				_bitmapData = bm;
				Model.state.dispatchEvent(new Event(State.PANO_SNAPSHOT_READY));
			});
			view.render();
		}
		
		public function get bitmapData():BitmapData
		{
			var bm:BitmapData = new BitmapData(view.width, view.height, false);
			stage3DProxy.context3D.clear();
			view.render();
			stage3DProxy.context3D.drawToBitmapData(bm);
			return bm;
		}
		
		public function getAngle(x:Number, y:Number):PanoAngle
		{
			var pan:Number = (cameraController.panAngle + 180)%360;
			return new PanoAngle(
				pan > 180 ? pan - 360 : pan/* + (width/2 - x)*/,
				-cameraController.tiltAngle /*+ (height/2 - y)*/
			);
		}
		
		public function get panoLook():PanoLook
		{
			return new PanoLook(
				cameraController.panAngle%360,
				cameraController.tiltAngle,
				zoom
			);
		}
		
		public function get pan():Number
		{
			//cameraController.wrapPanAngle = true;
			if(cameraController && ready)
				return cameraController.currentPanAngle;
			else
				return null;
		}
		
		
		
		private function stopDragEvent():void
		{
			if(!mouseDragPano || !hotspotDataToMesh[Model.hotspot])
				dispatchEvent(new Event(STOP_DRAG));
		}
		
		public function play():void
		{
			Log.record("pano.play()");
			if(Model.state.mode == State.MODE_AUTO && !interrupted && Model.config.guideId)
				startAutoRotate()
		}	
		
		private function onGuideSceneChanged(e:Event):void
		{
			
			if(Model.guideScene.voiceover)
				playVoiceover();
		}
		
		private function onVolumeChange(e:Event):void
		{
			if(channel)
				channel.soundTransform = new SoundTransform(Model.state.volume);
		}
		
		
		private function onModeChange(e:Event):void
		{
			interrupted = false;
			
			if(Model.state.mode == State.MODE_AUTO)
				startAutoRotate()
			else
				stopAutoRotate()			
		}
		
		private function onConnectModeChange(e:Event):void
		{

			if(Model.state.connectMode == State.CONNECT_MODE_RECEIVE 
				&& Model.state.mode == State.MODE_REMOTE)
			{
				cameraController.autoUpdate = false;
				mouseDragPano = false;
			}
			
		}
		
		
		private function voiceover():void
		{
			if(!Model.config.guideId)
				return;
			
			if(!Model.guideScene)
				return;
			
			if(Model.guideScene.voiceover != null)
				playVoiceover();
			
			if(Model.guideScene.voiceoverTrack != null)
				playVoiceoverTrack();
		}
		
		var voPlaying:Boolean;
		
		private function playVoiceover():void
		{	
			
			if(voPlaying)
				return;
			
			
			voPlaying = true;
			Log.record("pano.playVoiceover()");
			if(Model.guideScene.voiceover)
				var url:String = Model.config.getAssetUrl(Model.guideScene.voiceover);
			else
				return;
			
			sound = new Sound();
			sound.load(new URLRequest(url));
			sound.addEventListener(Event.COMPLETE,onVoiceoverLoaded);
			sound.addEventListener(Event.SOUND_COMPLETE,onVoiceoverComplete)
			
			channel = sound.play();
			channel.addEventListener(Event.SOUND_COMPLETE,onVoiceoverComplete);
			channel.soundTransform = new SoundTransform(Model.state.volume);
			
		}
		
		private function playVoiceoverTrack():void
		{
			Log.record("pano.playVoiceoverTrack()");
			var events:Array = Model.guideScene.voiceoverTrack.split(',');
			
			for(var i:int = 0; i< events.length;i++){
				var event:String = events[i].toString()
				var array:Array = event.split('/');
				var pl:String = array[0];
				var panoLooks:Array = pl.split('|');
				var t:Number = array[1];
				var panoLook:PanoLook = new PanoLook(panoLooks[0],panoLooks[1],panoLooks[2]);
				setTimeout(queuePanoLook,t,panoLook,Model.guideScene);
				
				//this doesn't work when the end of vo doesn't skip to next scene (i.e. no vo)
				if(i == events.length)
					setTimeout(Model.nextScene,1000);
			}
			
		}
		
		private function onVoiceoverLoaded(e:Event):void
		{
			
		}
		private function onVoiceoverComplete(e:Event):void
		{
			Log.record("Pano.onVoiceoverComplete()");
			if(Model.state.mode == State.MODE_AUTO && Model.state.uiState == State.UI_STATE_NORMAL && Model.guideScene.voiceoverTrack)
				setTimeout(Model.nextScene,5000);
			else
				setTimeout(function():void{playVoiceover()},5000);
			
			voPlaying = false;
		}
		
		private function queuePanoLook(panoLook:PanoLook,gs:GuideSceneData):void
		{
			Log.record("Pano.queuePanoLook");
			if(Model.state.mode == State.MODE_AUTO && Model.state.uiState == State.UI_STATE_NORMAL
				&& gs == Model.guideScene){
				Model.dispatchPanoLookEvent(panoLook);
				
			}
		}
		
		private function onHotspotCentred(e:Event):void
		{
			interrupted = false;
		}
		
		private function onHotspotOffCentre(e:Event):void
		{
			
		}
		
		public function get interrupted():Boolean
		{
			return _interrupted;
		}
		
		public function set interrupted(value:Boolean):void
		{
			_interrupted = value;
		}
		
		
		private function onVRModeChange(e:Event):void
		{
			if(Model.state.vr)
				setTimeout(loadVR,10);
			else
				closeVR();
			
		}
		
		public function loadVR():void
		{	
			
			Log.record('Pano.loadVR()');
			
			Model.state.mode = State.MODE_GYRO;
			
			view.width = stage.stageWidth/2;
			view.x = 0;

			rightEye = new View3D();
			rightEye.height = stage.stageHeight;
			rightEye.x = stage.stageWidth/2;
			rightEye.y = 0;
			rightEye.backgroundAlpha = 0;
			rightEye.shareContext = true;
			rightEye.stage3DProxy = stage3DProxy;
			rightEye.width = stage.stageWidth/2;
			rightEye.camera = rightEyeCamera;
			
			zoom = _currentScene.initialLook.fov * 0.65;
			
			rightEye.camera.lens = new PerspectiveLens(zoom);
			rightEye.camera.lens.far = cameraViewDistance;
			rightEye.camera.lens.near = 100;
			rightEye.camera.lookAt(view.camera.forwardVector);
			rightEye.scene = scene;
			addChild( rightEye );

			rightEye.render();
		}
			
		public function closeVR(){
			
			ready = false;
			
			view.width = stage.stageWidth;
			removeChild(rightEye);
			view.renderer;
			setTimeout(function(){ready = true},200);
		}
		
		public function onGyroChangeQuaternion(q:Quaternion):void
		{
			
			if(camera && ready){
				q.multiply(qu, q );
				q.multiply(qi, q );
				
				q.normalize();
				camera.transform = m;
				camera.transform = q.toMatrix3D();
				
				if(Model.state.vr)
					rightEyeCamera.transform = q.toMatrix3D();
				
				currentQuaternion = q.clone();
				currentPanAngle = DEG(q.toEulerAngles().z) + 180;
				
			}
			
		}
		
		
		
		private function DEG( radians:Number ):Number
		{
			return radians * 180 / Math.PI;
		}
		
		private function RAD( degrees:Number ):Number
		{
			return degrees * Math.PI/180;
		}
		
		public function onShake():void
		{
			if(cameraController){
				cameraController.autoUpdate = false;
				qi.fromAxisAngle(Vector3D.Y_AXIS, RAD(cameraController.currentPanAngle-180) );
			}
		}
		
		private function resetGame(e:Event):void
		{
			for each(var hotspot:Hotspot in hotspotDataToMesh)
				removeHotspot(hotspot)
			
			
			hotspotDataToMesh = new Dictionary;
			addHotspots();
		
		}
		

		
		public function remoteUpdate(obj:Object):void
		{
			
			switch(obj.type)
			{
				case 'drag':{ 
					camera.rotationX = obj.x;
					camera.rotationY = obj.y;
					camera.rotationZ = obj.z;
					zoom = obj.zoom;
					break;
				}
					
				case 'gyro':{

					var q:Quaternion = new Quaternion;
					q.w = obj.w;
					q.x = obj.x;
					q.y = obj.y;
					q.z = obj.z;
					
					camera.transform = q.toMatrix3D();
					break;
					
				}
					
					
			}
			
		}
		
		private function onGuideUpdate(e:Event):void
		{
			_currentGuideScene = Model.guideScene;
			_currentGuideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			_currentGuideScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);
			_currentGuideScene.addEventListener(GuideSceneData.GUIDE_SCENE_CHANGED,onGuideSceneChanged);
		}
		

		var newPlayer:SimpleVideoPlayer;
		
		
		
		
		private function loadPanoVideo():void
		{
			
		Log.record("Pano.loadPanoVideo()");
		
		
			
		if(!panoVideoMesh){
	
			sphereGeometry = new SphereGeometry(5000, 64, 48);
	
			if(Context3D.supportsVideoTexture){
				//panoTexture2DBase = new NativeVideoTexture("http://cdn.beek.co/uploads/iPadTest/out_2k_2.mp4", true, true);
				panoTexture2DBase = new NativeVideoTexture(Model.config.getAssetUrl(Model.currentScene.video), true, true);
				panoTexture2DBase.addEventListener(NativeVideoTexture.VIDEO_START,onInitialVideoStart);
				
				if(Model.isPlayingGame){
				panoTexture2DBase.addEventListener(NativeVideoTexture.VIDEO_STOP,function(){
					if(Model.currentTask.code == "watchpanovideo"){
						panoTexture2DBase.gameControl = true;
						panoTexture2DBase.player.pause();
						Model.panoVideoFinished();
					}
						
				});
				}
			}
			else{
				panoTexture2DBase = new VideoTexture(Model.config.getAssetUrl(Model.currentScene.video), 1024, 512, true,true);
				onInitialVideoStart();
			}
			
			
			panoTextureMaterial = new TextureMaterial(panoTexture2DBase, false, false, false);
			
			panoVideoMesh = new Mesh(sphereGeometry, panoTextureMaterial);
			panoVideoMesh.scaleX *= -1;
			
			panoVideoMesh.rotate(Vector3D.Y_AXIS,-90);
			
			scene.addChild(panoVideoMesh);
			panoTexture2DBase.player.play();
			view.render();
		
		}
		else if(Context3D.supportsVideoTexture)
			{
				
				newPlayer = new SimpleVideoPlayer;
				newPlayer.source = Model.config.getAssetUrl(Model.currentScene.video);
				newPlayer.ns.addEventListener(NetStatusEvent.NET_STATUS, loadNextVideo);
				newPlayer.play();
				
			}
		}
		
		private function onInitialVideoStart(e:Event = null):void{
			panoTexture2DBase.removeEventListener(NativeVideoTexture.VIDEO_START,onInitialVideoStart);
			//panoTilesLoaded();
		}
		
		
		private function loadNextVideo(stats:NetStatusEvent):void
		{
			trace("loadNextVideo: " + stats.info.code);
			
			switch (stats.info.code) {
				case "NetStream.Play.Start":
					panoTexture2DBase.player.stop();
					break;
				case "NetStream.Buffer.Full":
					newPlayer.ns.removeEventListener(NetStatusEvent.NET_STATUS, loadNextVideo);
					panoTexture2DBase.player.dispose();
					panoTexture2DBase.player = newPlayer;
					
					panoTexture2DBase.addEventListener(NativeVideoTexture.VIDEO_START,nextVideoReady);
					
					stage3DProxy.clear();
					panoTextureMaterial.texture.invalidateContent();
					view.render();

					break;
			}
			
		}
		
		private function nextVideoReady(e:Event){
			
			panoTexture2DBase.removeEventListener(NativeVideoTexture.VIDEO_START,nextVideoReady);
			
			if(_currentScene)
				clearCurrentScene();
			
			setTimeout(panoTilesLoaded,200);
		
		}
		
		private function onMapSelected(e:Event){
		
			if(_currentScene)
				clearCurrentScene();
			
		}

		
/*		private var polesATFTexture:PolesATFTexture;
		
		private function onPolesTextureLoaded(event:Event):void
		{
			var polesMesh:Mesh;
			var polesSphereGeometry:SphereGeometry;
			var polesTexture2DBase:Texture2DBase;
			var polesTextureMaterial:TextureMaterial;
		
			
			polesTextureMaterial = new TextureMaterial(polesATFTexture.texture, false, false, false);
			polesTextureMaterial.bothSides = true;
			polesTextureMaterial.alphaPremultiplied = true;
			//polesTextureMaterial.alphaBlending = true;
			//polesTextureMaterial.alphaThreshold = 0.5;
			polesSphereGeometry = new SphereGeometry(5000, 64, 48);
			polesMesh = new Mesh(polesSphereGeometry, polesTextureMaterial);
			polesMesh.scaleX *= -1;
			scene.addChild(polesMesh);
		
		}*/
		
	}
	
	
}