/**
 * SBSStereoRenderMethod.as
 * 
 * Written by Bibek Sahu, 22-Jan-2013
 * Based on "InterleavedStereoRenderMethod.as", from Away3D-4.1Alpha
 * License is the same as "InterleavedStereoRenderMethod.as", which is Apache-2.0 as of this writing
 * @see http://www.apache.org/licenses/LICENSE-2.0.html
 * @see http://away3d.com/
 */
package away3d.stereo.methods
{
	import away3d.core.managers.RTTBufferManager;
	import away3d.core.managers.Stage3DProxy;
	
	import flash.display3D.Context3DProgramType;

	public class SBSStereoRenderMethod extends StereoRenderMethodBase
	{
		private var _shaderData : Vector.<Number>;
		
		public function SBSStereoRenderMethod()
		{
			super();
			
			_shaderData = new <Number>[1,1,1,1, 1,1,1,1];
		}
		
		
		override public function activate(stage3DProxy:Stage3DProxy):void
		{
			if (_textureSizeInvalid) {
				var minV : Number;
				var rttManager : RTTBufferManager;
				
				rttManager = RTTBufferManager.getInstance(stage3DProxy);
				_textureSizeInvalid = false;
				
				minV = rttManager.renderToTextureRect.bottom / rttManager.textureHeight;
				
				_shaderData[0] = 2;
				_shaderData[1] = rttManager.renderToTextureRect.width;
				_shaderData[2] = 1;
				_shaderData[3] = .5;
				
				_shaderData[4] = -.5;	// scale constant, to stretch / shrink x-axis by -0.5
				_shaderData[5] = 0;
				_shaderData[6] = 0;
				_shaderData[7] = 0;
				
				_shaderData[8] = 2.0;	// scale constant, to stretch / shrink x-axis by 2
				_shaderData[9] = 1.0;
				_shaderData[10] = 1.0;
				_shaderData[11] = 1.0;
			}
			
			stage3DProxy.context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, _shaderData);
		}
		
		
		override public function deactivate(stage3DProxy:Stage3DProxy):void
		{
		//	stage3DProxy.setTextureAt(2, null);
		}
		
		
		override public function getFragmentCode():String
		{
			return	"" +
					"mul ft2, v1, fc2\n"+						// scale: ft2.x = v1.x * 2; ft2.yzw = v1.yzw;
					"tex ft0, ft2, fs0 <2d,linear,nomip>\n"+	// ft0 = getColorAt(texture=fs0, position=ft2)
					"add ft2, v1, fc1\n"+						// translate: ft2.x = v1.x + -.5; ft2.yzw = v1.yzw;
					"mul ft2, ft2, fc2\n"+						// scale: ft2.x = ft2.x * 2; ft2.yzw = ft2.yzw;
					"tex ft1, ft2, fs1 <2d,linear,nomip>\n" +	// ft1 = getColorAt(texture=fs1, position=ft2)
					"div ft3, v0.x, fc0.y\n"+					// ratio: get fraction of way across the screen (range 0-1, see next line)
					"frc ft3, ft3\n"+							// ratio: ft3 = fraction(v0.x / renderWidth);
					"slt ft4, ft3, fc0.w\n"+					// ft4 = (ft3 < 0.5) ? 1 : 0;
					"sge ft5, ft3, fc0.w\n"+					// ft5 = (ft3 >= 0.5) ? 1 : 0;
					"mul ft6, ft1, ft4\n"+						// ft6 = ft1 * ft4;		// ft6 = (right side of screen) ? texture_fs1 : transparent
					"mul ft7, ft0, ft5\n"+						// ft6 = ft1 * ft4;		// ft7 = (left side of screen) ? texture_fs0 : transparent
					"add oc, ft7, ft6";							// outputcolor = ft7 + ft6;		// merge two images
		}
	}
}