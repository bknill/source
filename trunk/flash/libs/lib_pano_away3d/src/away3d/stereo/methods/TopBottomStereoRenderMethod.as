/**
 * TopBottomStereoRenderMethod.as
 * 
 * Written by Bibek Sahu, 22-Jan-2013
 * Based on "InterleavedStereoRenderMethod.as", from Away3D-4.1Alpha
 * License is the same as "InterleavedStereoRenderMethod.as", which is Apache-2.0 as of this writing
 * @see http://www.apache.org/licenses/LICENSE-2.0.html
 * @see http://away3d.com/
 */
package away3d.stereo.methods
{
	import away3d.core.managers.RTTBufferManager;
	import away3d.core.managers.Stage3DProxy;
	
	import flash.display3D.Context3DProgramType;

	public class TopBottomStereoRenderMethod extends StereoRenderMethodBase
	{
		private var _shaderData : Vector.<Number>;
		
		public function TopBottomStereoRenderMethod()
		{
			super();
			
			_shaderData = new <Number>[1,1,1,1];
		}
		
		
		override public function activate(stage3DProxy:Stage3DProxy):void
		{
			if (_textureSizeInvalid) {
				var minV : Number;
				var rttManager : RTTBufferManager;
				
				rttManager = RTTBufferManager.getInstance(stage3DProxy);
				_textureSizeInvalid = false;
				
				minV = rttManager.renderToTextureRect.bottom / rttManager.textureHeight;
				
				_shaderData[0] = 2;
				_shaderData[1] = rttManager.renderToTextureRect.height;
				_shaderData[2] = 1;
				_shaderData[3] = .5;
				
				_shaderData[4] = 0;
				_shaderData[5] = -.5;	// scale constant, to stretch / shrink y-axis by -0.5
				_shaderData[6] = 0;
				_shaderData[7] = 0;
				
				_shaderData[8] = 1.0;
				_shaderData[9] = 2.0;	// scale constant, to stretch / shrink y-axis by 2
				_shaderData[10] = 1.0;
				_shaderData[11] = 1.0;
			}
			
			stage3DProxy.context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, _shaderData);
		}
		
		
		override public function deactivate(stage3DProxy:Stage3DProxy):void
		{
		//	stage3DProxy.setTextureAt(2, null);
		}
		
		
		override public function getFragmentCode():String
		{
			return	"" +
					"mul ft2, v1, fc2\n"+						// scale: ft2.y = v1.y * 2; ft2.xzw = v1.xzw;
					"tex ft0, ft2, fs0 <2d,linear,nomip>\n"+	// ft0 = getColorAt(texture=fs0, position=ft2)
					"add ft2, v1, fc1\n"+						// translate: ft2.y = v1.y + -.5; ft2.xzw = v1.xzw;
					"mul ft2, ft2, fc2\n"+						// scale: ft2.y = ft2.y * 2; ft2.xzw = ft2.xzw;
					"tex ft1, ft2, fs1 <2d,linear,nomip>\n" +	// ft1 = getColorAt(texture=fs1, position=ft2)
					"div ft3, v0.y, fc0.y\n"+					// ratio: get fraction of way across the screen (range 0-1, see next line)
					"frc ft3, ft3\n"+							// ratio: ft3 = fraction(v0.y / renderHeight);
					"slt ft4, ft3, fc0.w\n"+					// ft4 = (ft3 < 0.5) ? 1 : 0;
					"sge ft5, ft3, fc0.w\n"+					// ft5 = (ft3 >= 0.5) ? 1 : 0;
					"mul ft6, ft0, ft4\n"+						// ft6 = ft1 * ft4;		// ft6 = (top of screen) ? texture_fs0 : transparent
					"mul ft7, ft1, ft5\n"+						// ft6 = ft1 * ft4;		// ft7 = (bottom of screen) ? texture_fs1 : transparent
					"add oc, ft7, ft6";							// outputcolor = ft7 + ft6;		// merge two images
		}
	}
}