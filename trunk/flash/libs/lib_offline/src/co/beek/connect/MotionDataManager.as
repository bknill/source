package co.beek.connect
{
	import flash.events.Event;
	import flash.utils.clearInterval;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	
	import co.beek.model.Model;
	import co.beek.model.State;
	
	public class MotionDataManager
	{
		private static var _objects:Vector.<Object> = new Vector.<Object>;
		private static var _interval:int;
		private static var _filtering:Boolean;
		private static var _sceneLoading:Boolean;
		private static var obj:Object;
		
		public function MotionDataManager()
		{
			super();
			Model.addEventListener(Model.SCENE_CHANGE, onLoadingSceneChange);
			Model.state.addEventListener(State.PANO_TILES_LOADED, onPanoTilesLoaded);
		}
		
		public static function start():void
		{
			trace("MotionDataManager.start()");
			_interval = setInterval(processMotionObjects, 10);
		}
		
		public static function stop():void
		{
			trace("MotionDataManager.stop()");
			clearInterval(_interval);
		}
		
		
		public static function push(obj:Object):void{
			
			if(obj.type == "scene"){
				stop();
				Model.state.addEventListener(State.LOADING_SCENE_CHANGE, function(){start();});
				Model.loadGuideScene(obj.id);
			}
			else if(obj.type == "gyro" || "drag" 
				&& Model.state.mode == State.MODE_REMOTE
				&& Model.state.guideView == State.GUIDE_VIEW_CLOSED){
				obj.time = getTimer();
				_objects.push(obj);
			}
			else if(obj.type != "connection");
				Model.dispatchNetworkDataEvent(obj);
		}
		
		private static function processMotionObjects():void{
			
			if(_filtering || _sceneLoading
				|| Model.state.connectMode != State.CONNECT_MODE_RECEIVE 
				|| Model.state.guideView != State.GUIDE_VIEW_CLOSED)
				return;
			
			obj = _objects.pop();
			
			if(!obj)
				return;
			
			//clear out old ones
			if(getTimer() - obj.time > 100){
				_filtering = true;
				_objects = _objects.filter(function(o:Object, index:int, vector:Vector.<Object>):Boolean{
					return getTimer() - o.time < 100
				});
				Model.dispatchNetworkDataEvent(_objects.pop());
				_filtering = false;
			}
			else
				Model.dispatchNetworkDataEvent(obj);
				
		}
		
		private static function onLoadingSceneChange(e:Event = null):void
		{
			_sceneLoading = true;
			_objects.length = 0;
			
		}
		
		private static function onPanoTilesLoaded(e:Event = null):void
		{
			_sceneLoading = false;
		}
		
		
	}
}