package co.beek.connect
{
	import flash.events.DatagramSocketDataEvent;
	import flash.net.DatagramSocket;
	import flash.utils.ByteArray;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import co.beek.model.*;
	import co.beek.utils.NetworkUtil;
	
	public class ReceiveData
	{
		public function ReceiveData() 
		{
		}
		
		private static const subnetMask:Array = NetworkUtil.getSubnetMask(NetworkUtil.getPrefixLength());
		private static const ip:Array = NetworkUtil.getIpAddress().split(".");
		private static var connectionIp:String;
		private static const broadcastSocket:DatagramSocket = new DatagramSocket();
		private static const receiveDataSocket:DatagramSocket = new DatagramSocket();
		private static var _broadcastIp:Boolean = true;
		private static var intervalId:int;
		private static var broadcastPort:int = 7934;
		private static var receivePort:int = 7935;
		private static var _connected:Boolean;
		private static var obj:Object = new Object;
		private static var ba:ByteArray = new ByteArray();
		
		private var ipAddress:String;
		
		public static function createServer():void{
			trace('NetworkConnection.createServer()');
			
			//broadcast IP
			intervalId = setInterval(broadcastIP, 2000, NetworkUtil.getIpAddress());//broadcast the servers Ip-Address every 2 seconds
			Model.state.connectState = "Device IP is " + NetworkUtil.getIpAddress();
			//incoming Data
			if(!receiveDataSocket.bound){
				receiveDataSocket.bind(receivePort);
				receiveDataSocket.addEventListener(DatagramSocketDataEvent.DATA, dataReceived);
				receiveDataSocket.receive();
			}
		}
		
		private static function broadcastIP(message:String):void
		{
				try
				{
					if(subnetMask[1] != 255 || subnetMask[2] != 255){
						trace("!!! WARNING: NOT A 'C' CLASS NETWORK !!! (Will not broadcast IP.)");
						clearInterval(intervalId);
					}
					else
					{
						var data:ByteArray = new ByteArray();
						data.writeUTFBytes(message);

						for(var i4:int = subnetMask[3]; i4 <= 255; i4++){
							var tempIp:String = ip[0] + "." + ip[1] + "." + ip[2] + "." + (subnetMask[3] == 255 ? ip[3] : i4);
							if(tempIp != NetworkUtil.getBroadcastIp() && tempIp != NetworkUtil.getSubnetIp(ip, subnetMask)){
								broadcastSocket.send(data, 0, 0, tempIp, broadcastPort);
								Model.state.connectState = "Contacting devices..";
							}
						}
					}
				}
				catch (error:Error)
				{
					trace(error.message);
				}
		}
		
		private static function connected():void
		{
			_connected = true;
			clearInterval(intervalId);
			Model.state.connectState = "Connected";
			Model.state.connected = true;
		}
		
		
		public static function closeConnections():void
		{
			if(receiveDataSocket.connected)
				receiveDataSocket.close();
			if(broadcastSocket.connected)
				broadcastSocket.close();
		}


		
		protected static function dataReceived(e:DatagramSocketDataEvent):void {
			
			if(!_connected)
				connected();
			
			connectionIp = e.srcAddress;
				
				obj = e.data.readObject();
				
				if(obj)
					MotionDataManager.push(obj);
				
				Model.state.connectState = "Receiving Data";

		}
		
		public static function sendData(o:Object):void{

			ba.writeObject(o);

			broadcastSocket.send(ba,0,0,connectionIp,broadcastPort);
			Model.state.connectState = "Sending Data";
		}
		
		
		
		
	}
}