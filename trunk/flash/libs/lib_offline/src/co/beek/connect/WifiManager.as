package co.beek.connect
{
	import flash.errors.EOFError;
	import flash.events.DatagramSocketDataEvent;
	import flash.net.DatagramSocket;
	import flash.net.InterfaceAddress;
	import flash.utils.ByteArray;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.utils.NetworkUtil;
	
	public class WifiManager
	{
		public function WifiManager()
		{
			super();
			
		}
		
		private static var broadcastSocket:DatagramSocket = new DatagramSocket();
		private static var receiveSocket:DatagramSocket = new DatagramSocket();
		private static var broadcastPort:int = Model.state.connectMode == State.CONNECT_MODE_BROADCAST ? 7935 : 7934;
		private static var receivePort:int = Model.state.connectMode == State.CONNECT_MODE_BROADCAST ? 7934 : 7935;

		private static var subnetMask:Array;
		private static var ipArray:Array;
		private static var connectionIp:String;
		private static var connectionIps:Array = new Array;
		private static var localIps:Array = NetworkUtil.getIpAddresses();
		private static var localIp:String = NetworkUtil.getIpAddress();
		private static var receiveDataSocket:DatagramSocket = new DatagramSocket();
		private static var _broadcastIp:Boolean = true;
		private static var intervalId:int;
		private static var _connected:Boolean;

		
		//Receiver
		public static function createServer():void
		{
			trace('WifiManager.createServer()');
			intervalId = setInterval(broadcastIP, 5000);//broadcast the servers Ip-Address every 5 seconds
			Model.state.connectState = "Device IP: " + NetworkUtil.getIpAddresses();
			
			//incoming Data
			if(!receiveDataSocket.bound)
				bindReceivePort()
		}
		
		private static function bindReceivePort():void
		{
			trace("WifiManager.bindReceivePort()");
			try{receiveDataSocket.bind(receivePort);
				receiveDataSocket.addEventListener(DatagramSocketDataEvent.DATA, dataReceived);
				receiveDataSocket.receive();
				
				if(receiveDataSocket.bound)
					trace("Receive Port bounded");
				else
					trace("Receive Port not bounded");
			}
			catch(e:Error){
				trace(e.message);
				setTimeout(bindReceivePort,2000);
			};
		}
		
		//Receiver - broadcasts this IP to all available IP addresses on same networks (Local & Wifi)
		private static function broadcastIP():void
		{
			try
			{
				for each(var address:InterfaceAddress in NetworkUtil.InterfaceAddresses){
					ipArray = address.address.split(".");
					subnetMask = NetworkUtil.getSubnetMask(address.prefixLength);

					//broadcast IP
					var data:ByteArray = new ByteArray();
					var object:Object = new Object();
					object.type = "connection";
					object.ip = localIp = NetworkUtil.getIpAddress();
					object.guideId = Model.guideData.id || Model.config.guideId;
					object.sceneId = Model.currentScene.id || Model.config.sceneId;
					data.writeObject(object);
					
				if(subnetMask[1] != 255 || subnetMask[2] != 255){
					trace("!!! WARNING: NOT A 'C' CLASS NETWORK !!! (Will not broadcast IP.)");
					//clearInterval(intervalId);
				}
				else
					for(var i4:int = subnetMask[3]; i4 <= 255; i4++){
						var tempIp:String = ipArray[0] + "." + ipArray[1] + "." + ipArray[2] + "." + (subnetMask[3] == 255 ? ipArray[3] : i4);
						if(tempIp != NetworkUtil.getBroadcastIp() 
							&& tempIp != NetworkUtil.getSubnetIp(ipArray, subnetMask) 
							&& connectionIps.indexOf(tempIp) == -1)
								broadcastSocket.send(data, 0, 0, tempIp, broadcastPort);	
					}
				}
			}
			catch (error:Error)
			{
				trace(error.message);
			}
		}
		
		//Broadcaster
		public static function findNetwork():void
		{
			trace('findNetwork()');
			if(!receiveSocket.bound){
				receiveSocket.bind(receivePort);
				receiveSocket.addEventListener(DatagramSocketDataEvent.DATA, dataReceived);
				receiveSocket.receive();
			}
			
			Model.state.connectState = "Waiting for Connections";
		}
		
		private static function connected():void
		{
			_connected = true;
			clearInterval(intervalId);
			Model.state.connectState = "Connected";
			Model.state.connected = true;
		}
		
		private static function dataReceived(e:DatagramSocketDataEvent):void
		{
			var data:ByteArray = new ByteArray();
			var object:Object = new Object();

			try{object = e.data.readObject(); }
			catch(e:Error){trace(e.message);}
			
			if(object)
			{
				if(!object.ip)
					return;
				
				if(object.ip == localIp)
					return;
				
				if(!_connected)
					connected();
				
				if(connectionIps.indexOf(object.ip) == -1)
					connectionIps.push(object.ip);

				if(object.guideId != Model.state.guideId)
					Model.state.connectState = "Different guides, please load the same guide on both devices";
				else if(Model.state.connected)
				{
					MotionDataManager.push(object);
					Model.state.connectState = "Receiving Data";
				}
			}

		}
		
		public static function sendData(object:Object):void{
			
			object.guideId = Model.guideData.id || Model.config.guideId;
			object.sceneId = Model.currentScene.id || Model.config.sceneId;
			object.ip = localIp;
			
			var data:ByteArray = new ByteArray();
			data.clear();
			data.writeObject(object);
			if(connectionIps.length > -1){
				for each(var connectionIp:String in connectionIps){
					broadcastSocket.send(data,0,0,connectionIp,broadcastPort);
					Model.state.connectState = "Sending Data";
				}
			}
			else
				Model.state.connectState = "Cannot send data - no IP";
		}

		
		public static function close():void
		{
			if(broadcastSocket.connected)
				broadcastSocket.close();
			if(receiveSocket.connected)
				receiveSocket.close();
			
			connectionIps.length = 0;
			_connected = false;
		}
		
	
		

		
	}
}