package co.beek.connect
{
	import co.beek.Log;
	import co.beek.utils.*;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import co.beek.model.*;
	import co.beek.model.Model;
	
	import flash.display.*;
	import flash.utils.*;
	
	import caurina.transitions.Tweener;
	
	
	public class NetworkManager extends NetworkConnectFl
	{
		public  const BLUETOOTH_STARTED:String = "BLUETOOTH_STARTED";
		public  const BLUETOOTH_STOPPED:String = "BLUETOOTH_STOPPED";
		
		private var tweenInt:int;
		private var updateInt:int;
		
		public function NetworkManager()
		{
			super();
			
			broadcastButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			receiveButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			wifiButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			
			broadcastButton.addEventListener(MouseEvent.MOUSE_UP, onBroadcastButtonMouseUp);
			receiveButton.addEventListener(MouseEvent.MOUSE_UP, onReceiveButtonMouseUp);
			wifiButton.addEventListener(MouseEvent.MOUSE_UP, onWifiButtonMouseUp);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_UP, onBluetoothButtonMouseUp);
		
			onBroadcastButtonMouseUp();
			
			bluetoothButton.visible = false;
		}
		

		private function onButtonMouseDown(e:MouseEvent):void
		{
			e.target.alpha = 0.6;
		}
		
		private function onBroadcastButtonMouseUp(e:MouseEvent = null):void
		{
			Log.record("NetworkManager.onBroadcastButtonMouseUp()");
			ColorUtil.colorize(broadcastButton.button.fill, 0x111111);
			ColorUtil.colorize(receiveButton.button.fill, 0xffffff);
			Model.state.connectMode = State.CONNECT_MODE_BROADCAST;
			
			if(Model.state.connected)
				closeConnections();
			
		}
		
		private function onReceiveButtonMouseUp(e:MouseEvent = null):void
		{
			Log.record("NetworkManager.onReceiveButtonMouseUp()");
			ColorUtil.colorize(receiveButton.button.fill, 0x111111);
			ColorUtil.colorize(broadcastButton.button.fill, 0xffffff);	
			Model.state.connectMode = State.CONNECT_MODE_RECEIVE;
			closeConnections();
			
		}
		
		private function onWifiButtonMouseUp(e:MouseEvent  = null):void
		{
			Log.record("NetworkManager.onWifiButtonMouseUp()");
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_BLUETOOTH)
				stopBluetooth();
			
			Model.state.connectNetwork = State.CONNECT_NETWORK_WIFI;
			ColorUtil.colorize(wifiButton.icon, 0x666666);
			
			wifiButton.removeEventListener(MouseEvent.MOUSE_UP, onWifiButtonMouseUp);
			wifiButton.addEventListener(MouseEvent.MOUSE_UP, stopWifi);
			
			tweenIcon(wifiButton.icon);
			
			Model.state.addEventListener(State.NETWORK_CONNECT, onWifiConnect);
			Model.state.addEventListener(State.NETWORK_DISCONNECT, stopWifi);
			
			if(Model.state.connectMode == State.CONNECT_MODE_BROADCAST)
				WifiManager.findNetwork();
			else if(Model.state.connectMode == State.CONNECT_MODE_RECEIVE)
			{
				NetworkUtil.getAddress();
				WifiManager.createServer();		
			}
			
			updateInt = setInterval(function():void{if(Model.state.connectState != null)status.text = Model.state.connectState},10 );
		}
		
		private function closeConnections():void
		{
			WifiManager.close();
		}
		
		private function onWifiConnect(e:Event = null):void
		{
			Log.record("NetworkManager.onWifiConnect()");
			ColorUtil.colorize(wifiButton.icon, 0x006600);
			Model.state.removeEventListener(State.NETWORK_CONNECT, onWifiConnect);
			stopTween(wifiButton.icon);
			MotionDataManager.start();
	
			status.text = "Connected";
		}
		
		private function stopWifi(e:MouseEvent  = null):void
		{
			Log.record("NetworkManager.stopWifi()");
			if(Model.state.connected){
				closeConnections();
				Model.state.connected = false;
			}
			ColorUtil.colorize(wifiButton.icon, 0x000000);	
			stopTween(wifiButton.icon);
			wifiButton.icon.alpha = 1;
			wifiButton.alpha = 1;
			
			MotionDataManager.stop();
			
			clearInterval(updateInt);
			status.text = "";
			
			wifiButton.addEventListener(MouseEvent.MOUSE_UP, onWifiButtonMouseUp);
		}
		
		private function onBluetoothButtonMouseUp(e:MouseEvent  = null):void
		{
			Log.record("NetworkManager.onBluetoothButtonMouseUp()");
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_WIFI)
				stopWifi();
			
			Model.state.connectNetwork = State.CONNECT_NETWORK_BLUETOOTH;
			dispatchEvent(new Event(BLUETOOTH_STARTED));
			
			
			Model.state.addEventListener(State.NETWORK_CONNECT, onBluetoothConnect);
			Model.state.addEventListener(State.NETWORK_DISCONNECT, stopBluetooth);
			
			tweenIcon(bluetoothButton.icon);
			ColorUtil.colorize(bluetoothButton.icon, 0x666666);	

			bluetoothButton.removeEventListener(MouseEvent.MOUSE_UP, onBluetoothButtonMouseUp);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_UP, stopBluetooth);
			
			//update text field with notifications
			updateInt = setInterval(function(){if(Model.state.connectState != null)status.text = Model.state.connectState},10 );
		}
		
		private function stopBluetooth(e:Event  = null):void
		{
			Log.record("NetworkManager.stopBluetooth()");
			dispatchEvent(new Event(BLUETOOTH_STOPPED));
			
			ColorUtil.colorize(bluetoothButton.icon, 0x000000);	
			stopTween(bluetoothButton.icon);
			clearInterval(updateInt);
			
			Model.state.removeEventListener(State.NETWORK_CONNECT, onBluetoothConnect);
			Model.state.removeEventListener(State.NETWORK_DISCONNECT, stopBluetooth);
				
			bluetoothButton.removeEventListener(MouseEvent.MOUSE_UP, stopBluetooth);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_UP, onBluetoothButtonMouseUp);
			
			status.text = "";
			Model.state.connected = false;
			MotionDataManager.stop();
			

		}
			
		
		private function onBluetoothConnect(e:Event = null):void
		{
			Log.record("NetworkManager.onBluetoothConnect()");
			ColorUtil.colorize(bluetoothButton.icon, 0x0033FF);
			Model.state.removeEventListener(State.NETWORK_CONNECT, onBluetoothConnect);
			Model.state.addEventListener(State.NETWORK_DISCONNECT, stopBluetooth);
			stopTween(bluetoothButton.icon);
			
			if(Model.state.connectMode == State.CONNECT_MODE_RECEIVE)
				MotionDataManager.start();
		}
		
		private function tweenIcon(icon:DisplayObject):void
		{
			tweenInt = setInterval(function(){
				Tweener.addTween(icon,{alpha: 0.5, time: 0.5, onComplete:
				function(){
					Tweener.addTween(icon,{alpha: 1, time: 0.5});
				}});
			
			},1000);
		}
		private function stopTween(icon:DisplayObject):void
		{
			clearInterval(tweenInt);
			Tweener.removeAllTweens();
			icon.alpha = 1;
		}

		
		
	}
}