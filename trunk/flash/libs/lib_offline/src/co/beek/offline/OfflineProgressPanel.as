package co.beek.offline
{
	import co.beek.mobile.MobileConfig;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.service.ServiceLoader;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class OfflineProgressPanel
	{
		public static const COMPLETE:String = "COMPLETE";
		
		private var _guideBasic:GuideBasicData;
		
		private var guideScenes:Vector.<GuideSceneData>;
		
		private var _loaders:Vector.<IOfflineFile> = new Vector.<IOfflineFile>;
		
		private var _currentGuideScene:int = -1;
		
		private var processing:Boolean;
		
		public var percentComplete:Number;
		
		public var complete:Boolean = false;
		
		public function OfflineProgressPanel(data:GuideBasicData)
		{
			super();
			_guideBasic = data;
			
			processing = true;

			var loader:ServiceLoader = new ServiceLoader(GuideData);
			var url:String = Model.config.serviceUrl +"/guide/"+data.id+"/json";
			loader.load(url, onGuideDataLoaded);
		}
		
		public function get guideBasic():GuideBasicData
		{
			return _guideBasic;
		}
		
		private function onGuideDataLoaded(guideData:GuideData):void
		{
			trace("OfflineProgressPanel.onGuideDataLoaded()");
			// If it has been disposed already
			if(!_loaders)
				return;

			//barColor = guideData.guideColor;
			
			_loaders.push(new JsonData("guide_"+guideData.id+".json", JSONBeek.encode(ServiceResponse.create(guideData.data))));
			_loaders.push(new AssetLoader(Model.config.assetCdn, guideData.thumbName, guideData.thumbName));
			
			for(var i:uint = 0; i < guideData.guideSections.length; i++)
				if(guideData.guideSections[i].page_image)
					_loaders.push(new AssetLoader(Model.config.assetCdn, guideData.guideSections[i].page_image, "Page Image"));
			
			
			guideScenes = guideData.guideScenes;
			currentGuideScene = 0;
		}
		
		private function get currentGuideScene():int
		{
			return _currentGuideScene;
		}
		
		private function set currentGuideScene(value:int):void
		{
		//	trace("OfflineProgressPanel.set currentGuideScene()");
			_currentGuideScene = value;
			
			if(_currentGuideScene < guideScenes.length)
			{
				var guideScene:GuideSceneData = guideScenes[_currentGuideScene];
				
				var zipScene:OfflineGuideScene = new OfflineGuideScene(guideScene, _loaders)
				zipScene.addEventListener(OfflineGuideScene.READY, onSceneReady);
				zipScene.start();
			}
			else
			{
				startLoading();
				
			}
		}
		
		private function onSceneReady(event:Event):void
		{
			//trace("OfflineProgressPanel.onSceneReady()");
			var zipScene:OfflineGuideScene = OfflineGuideScene(event.target);
			zipScene.removeEventListener(OfflineGuideScene.READY, onSceneReady);
			zipScene.dispose();
			
			currentGuideScene ++;
		}
		
		private function startLoading():void
		{

			processing = false;
			loadIndex = 0;
		}
		
		private var _loadIndex:int;
		
		private function get loadIndex():int
		{
			return _loadIndex;
		}
		
		private function set loadIndex(value:int):void
		{
			//trace("OfflineProgressPanel.set loadIndex(" + value + ")");
			
			_loadIndex = value;
			
			if(_loadIndex < _loaders.length)
			{
				var p:Number = _loadIndex/_loaders.length;
				percentComplete = p;

				var loader:IOfflineFile = _loaders[_loadIndex];
				
				if(loader is AssetLoader)
					load(loader as AssetLoader)
					
				else if(loader is JsonData)
					writeJson(loader as JsonData)
				
				else
					loadIndex ++;
			}
			else
			{
				complete = true;
			}

		}
		
		private function load(loader:AssetLoader):void
		{
			loader.addEventListener(AssetLoader.ERROR, onLoadError);
			loader.addEventListener(AssetLoader.COMPLETE, onLoadComplete);
			loader.start();
		}
		
		private function onLoadError(event:Event):void 
		{
			trace("OfflineProgressPanel.onLoadError()");
			//trace("onLoadError: "+loader.path);
			var loader:AssetLoader = AssetLoader(event.target);
			loader.stop();
			loader.removeEventListener(AssetLoader.ERROR, onLoadError);
			loader.removeEventListener(AssetLoader.COMPLETE, onLoadComplete);
			loader = null
			
			loadIndex ++;
		}
		
		private function onLoadComplete(event:Event):void 
		{
			//trace("OfflineProgressPanel.onLoadComplete()");
			var loader:AssetLoader = AssetLoader(event.target);
			//trace("onLoadComplete: "+loader.path+" size:" + loader.data.length)
			
			var file:File = MobileConfig.localStorage.resolvePath(loader.path);
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(loader.data, 0, loader.data.length);
			fileStream.close();
			
			loader.stop();
			loader.removeEventListener(AssetLoader.ERROR, onLoadError);
			loader.removeEventListener(AssetLoader.COMPLETE, onLoadComplete);
			
			loadIndex ++;
		}
		
		private function writeJson(jsonData:JsonData):void
		{
			var file:File = MobileConfig.localStorage.resolvePath(jsonData.path);
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeMultiByte(jsonData.data, "UTF-8");
			fileStream.close();
			
			loadIndex ++;
		}
		
		
		public function dispose():void
		{
			_guideBasic = null;
			guideScenes = null;
			
			for(var i:int=_loadIndex; i<_loaders.length; i++)
			{
				if(_loaders[i] is AssetLoader)
				{
					AssetLoader(_loaders[i]).removeEventListener(AssetLoader.ERROR, onLoadError);
					AssetLoader(_loaders[i]).removeEventListener(AssetLoader.COMPLETE, onLoadComplete);
				}
			}
			_loaders = null;
		}
	}
}