package co.beek.offline
{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.offline.OfflineProgressPanel;
	import co.beek.pano.ProgressPanel;
	import co.beek.service.GuideService;
	import co.beek.service.data.GuideBasicList;
	import co.beek.utils.JSONBeek;
	
	
	
	public class OfflineManager
	{
		public const VR_BUTTON:String = "VR_BUTTON";
		public const CONNECT_BUTTON:String = "CONNECT_BUTTON";
		public const OTHER_GUIDES_BUTTON:String = "OTHER_GUIDES_BUTTON";
		public const OTHER_GUIDES_LOADED:String = "OTHER_GUIDES_LOADED";
		
		public static var progressPanel:OfflineProgressPanel;
		private static var _offlineGuides:Vector.<GuideBasicData>;
		private static var _otherGuides:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
		private static var _guide:GuideBasicData;
		private static var beekDB:SQLConnection = new SQLConnection;
		private static var otherGuidesSet:Boolean;
		private static var _guideChanged:Boolean;
		private static var thisGuideDownloaded:Boolean;
		
		public function OfflineManager()
		{
			super();
			
			Model.state.addEventListener(State.NETWORK_CHANGE, onNetworkChange);
			Model.state.addEventListener(State.GUIDE_ID_CHANGE, onGuideIDChanged);


			
		}
		
		private static function onNetworkChange(e:Event = null):void
		{
		}
		
		
		
		public static function connect(db:SQLConnection):void
		{  
			Log.record("SettingsMenu.connect()");
            beekDB = db;
			offlineGuides = queryOfflineGuides();
			onNetworkChange();
		}
		
		
		
		public static function downloadGuide():void
			
		{
			Log.record("SettingsMenu.onDownloadButtonMouseUp()");
			
			progressPanel = new OfflineProgressPanel(GuideBasicData.convert(Model.guideData));
	
					
			var interval = setInterval(function(){

					if(progressPanel.complete){
						clearInterval(interval);
						offlineComplete();
						Model.dispatchEvent(new Event(Model.OFFLINE_COMPLETE));
					}
				}
			,100);
		}
		
		public static function checkForOfflineLoad(id:String):GuideBasicData
		{
			Log.record("SettingsMenu.checkForOfflineLoad()");
	
			if(!offlineGuides)
				return null;
			
			if(!guide)
				for each(var basic:GuideBasicData in offlineGuides)
					if(basic.id == id)
						guide = basic;

			if(offlineGuides.indexOf(guide) > -1){
				GuideService.getGuideBasicJson(id, checkIfUpdateRequired)
				thisGuideDownloaded = true;
			
				return guide;
			}
			
			return offlineGuides[0];
		}
		
		private static function checkIfUpdateRequired(data:GuideBasicData):void
		{
			Log.record("SettingsMenu.checkIfUpdateRequired()");

			
		}
		
		private static function offlineComplete():void
		{
			Log.record("SettingsMenu.onOfflineComplete()");
		
			if(offlineGuides && Model.getGuide(offlineGuides, progressPanel.guideBasic.id) != null)
				updateLocalGuide(progressPanel.guideBasic);
			else
				insertLocalGuide(progressPanel.guideBasic);
			
			offlineGuides = queryOfflineGuides();

			if(progressPanel.guideBasic.id != Model.state.guideId || _guideChanged){
					_guideChanged = false;
		
			}
			
			progressPanel.dispose();
		}
		
		private static function insertLocalGuide(guide:GuideBasicData):void
		{
			Log.record("SettingsMenu.insertLocalGuide()");
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "INSERT INTO `localguides` (`guide_id`, `json`) values (?, ?)";
			insertStatement.parameters[0] = guide.id;     
			insertStatement.parameters[1] = JSONBeek.encode(guide.data);     
			insertStatement.execute();
		}
		
		private static function updateLocalGuide(guide:GuideBasicData):void
		{
			Log.record("SettingsMenu.updateLocalGuide()");
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "UPDATE `localguides` SET `json` = ? WHERE `guide_id` = ?";
			insertStatement.parameters[0] = JSONBeek.encode(guide.data);     
			insertStatement.parameters[1] = guide.id;
			insertStatement.execute();
		}
		
		private static function queryOfflineGuides():Vector.<GuideBasicData>
		{
			Log.record("SettingsMenu.queryOfflineGuides()");
			var getAllStatement:SQLStatement = new SQLStatement();
			getAllStatement.sqlConnection = beekDB;
			getAllStatement.text = "SELECT * FROM `localguides`";
			getAllStatement.execute();
			
			var data:Array = getAllStatement.getResult().data;
			var offlineGuides:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
			
			if(!data)
				return null;
			
			for(var i:uint = 0; i < data.length; i++)
				offlineGuides.push(new GuideBasicData(JSONBeek.decode(data[i]['json'])));
			
			return offlineGuides;
		}

		
		public static function get guide():GuideBasicData
		{
			return _guide;
		}
		
		public static function set guide(guide:GuideBasicData):void
		{
			_guide = guide;
		}
		
		
		public static function set offlineGuides(value:Vector.<GuideBasicData>):void
		{
			if(!value)
				return;
			
			 _offlineGuides = value;
		}
		
		public static function get offlineGuides():Vector.<GuideBasicData>
		{
			return _offlineGuides;
		}
		
		public static function set otherGuides(value:Vector.<GuideBasicData>):void
		{
			if(!value)
				return;
			
			_otherGuides = value;
		}
		
		public static function get otherGuides():Vector.<GuideBasicData>
		{
			return _otherGuides;
		}
		
		
		private static function onGuideIDChanged(e:Event = null):void{
			Log.record("SettingsMenu.onGuideIDChanged()");
			_guideChanged = true;

			
			if(!progressPanel && Model.config.guideId != Model.state.guideId){
				thisGuideDownloaded = false;
			}
		}
		

	

		public static function get guideChanged():Boolean
		{
			return _guideChanged;
		}

		public static function set guideChanged(value:Boolean):void
		{
			_guideChanged = value;
		}

		
	}
}