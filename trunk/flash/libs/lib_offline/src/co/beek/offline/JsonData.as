package co.beek.offline
{
	public class JsonData implements IOfflineFile
	{
		private var _name:String;
		private var _data:String;
		
		public function JsonData(name:String, data:String)
		{
			this._name = name;
			this._data = data;
		}
		
		public function get path():String
		{
			return _name;
		}

		public function get data():String
		{
			return _data;
		}
	}
}