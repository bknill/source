package co.beek.offline
{
	import flash.events.Event;
	
	public class StringEvent extends Event
	{
		private var _value:String;
		
		public function StringEvent(type:String, value:String)
		{
			super(type);
			this._value = value;
		}
		
		public function get value():String
		{
			return _value;
		}
	}
}