package co.beek.offline
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	public class AssetLoader extends EventDispatcher implements IOfflineFile
	{
		public static const COMPLETE:String = "COMPLETE";
		public static const ERROR:String = "ERROR";
		
		private var loader:URLLoader = new URLLoader;
		private var server:String;
		private var _path:String;
		private var _name:String;
		
		public function AssetLoader(server:String, path:String, name:String)
		{
			super();
			this.server = server;
			this._path = path;
			this._name = name;
			
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			loader.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			loader.addEventListener(Event.COMPLETE, onLoadComplete);
		}
		
		public function stop():void
		{
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			loader.removeEventListener(Event.COMPLETE, onLoadComplete);
			ByteArray(loader.data).clear();
			loader = null;
		}
		
		public function start():void
		{
			loader.load(new URLRequest(server+"/"+path));
		}
		
		private function onLoadError(event:IOErrorEvent):void 
		{
			dispatchEvent(new Event(ERROR));
		}
		
		private function onLoadComplete(event:Event):void 
		{
			dispatchEvent(new Event(COMPLETE));
		}
		
		public function get data():ByteArray
		{
			return loader.data;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function get path():String
		{
			return _path;
		}
	}
}