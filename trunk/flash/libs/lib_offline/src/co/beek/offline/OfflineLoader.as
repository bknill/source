package co.beek.offline
{
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	public class OfflineLoader extends URLLoader
	{
		private var server:String;
		private var _path:String;
		
		public function OfflineLoader(server:String, path:String)
		{
			super();
			this.server = server;
			this._path = path;
			
			if(!_path)
				trace("why?");
			
			dataFormat = URLLoaderDataFormat.BINARY;
		}
		
		public function start():void
		{
			load(new URLRequest(server+"/"+path));
		}
		
		public function get path():String
		{
			return _path;
		}
	}
}