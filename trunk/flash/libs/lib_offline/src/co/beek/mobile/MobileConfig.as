package co.beek.mobile
{
	import co.beek.model.Constants;
	import co.beek.model.IConfig;
	import co.beek.model.Model;
	import co.beek.Log;
	import co.beek.model.data.SceneBasicData;
	
	import flash.filesystem.File;
	import flash.system.Capabilities;
	
	public class MobileConfig implements IConfig
	{
		private static const TILE_SERVERS:Array = ["a", "b", "c"];
		
		public static var localStorage:File = File.applicationStorageDirectory;
		
		private var data:Object;
		
		private var _device:String;
		
		public function MobileConfig(data:Object)
		{
			this.data = data;
			this._device = getDevice();
		}
		
		private function getDevice():String { 
			var info:Array = Capabilities.os.split(" ");
			if (info[0] + " " + info[1] != "iPhone OS") {
				return Constants.UNKNOWN;
			}
			
			// ordered from specific (iPhone1,1) to general (iPhone)
			for each (var device:String in Constants.IOS_DEVICES) {	
				if (info[3].indexOf(device) != -1) {
					return device;
				}
			}
			return Constants.UNKNOWN;
		}
		
		public function get device():String { return _device }
		
		public function get gaCode():String { return null }
		
		public function get version():String { return null }
		
		public function get assetCdn():String { return data["assetCdn"] }
		
		public function get adminSwf():String { return null }
		
		public function get domain():String { return data["domain"] }
		
		public function get serviceUrl():String { return data["serviceUrl"] }
		
		public function get sceneId():String { return data["sceneId"] }
		
		public function get guideId():String { return data["guideId"] }
		
		public function get hotspotId():String { return null }
		
		public function get tourGuideId():String { return null }
		
		public function get tourRole():String { return null }
		
		public function get guideZip():String
		{
			return data["guideZip"];
		}
		
		
		public function get mapTileServer():String
		{
			var tileServer:String = TILE_SERVERS[int(Math.random()*(TILE_SERVERS.length-1))];
			return "http://"+tileServer+".tile.cloudmade.com";
		}
		
		public function getGuideJsonUrl(guideId:String):String
		{
			Log.record("MobileConfig.getGuideJsonUrl()");
			var file:File = localStorage.resolvePath("guide_"+guideId+".json");
			
			if(file.exists)
				return file.url;
			
			return serviceUrl+"/guide/"+guideId+"/json";
		}
		
		public function getSceneJsonUrl(sceneId:String):String
		{
			var file:File = localStorage.resolvePath("scene_"+sceneId+".json");
			if(file.exists)
				return file.url;
			
			return serviceUrl+"/scene/"+sceneId+"/json";
		}
		
		public function sceneOfflined(sceneId:String):Boolean
		{
			return localStorage.resolvePath("scene_"+sceneId+".json").exists;
		}
		
		public function localAssetPath(path:String):String
		{
			var file:File = localStorage.resolvePath(path);
			if(file.exists)
				return file.url;
			
			return null;
		}
		
		public function localAssetExists(path:String):Boolean
		{
			return localAssetPath(path) != null
		}
		
		public function getAssetUrl(path:String):String
		{
			var file:File = path != null ? localStorage.resolvePath(path) : null;
			if(file != null && file.exists)
				return file.url;
			
			return path != null ? assetCdn+"/"+path : null;
		}
		
		public function getMapTileUrl(path:String):String
		{
			var file:File = localStorage.resolvePath(path);
			if(file.exists)
				return file.url;
			
			return mapTileServer + "/" +path;
		}
		
		public function isAvailable(scene:SceneBasicData):Boolean
		{
			return Model.state.networkAccess || sceneOfflined(scene.id);
		}
	}
}