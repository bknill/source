package co.beek.mobile
{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.offline.OfflineProgressPanel;
	import co.beek.pano.ProgressPanel;
	import co.beek.service.GuideService;
	import co.beek.service.data.GuideBasicList;
	import co.beek.utils.JSONBeek;
	
	
	
	public class SettingsMenu extends SettingsMenuFl
	{
		public const VR_BUTTON:String = "VR_BUTTON";
		public const CONNECT_BUTTON:String = "CONNECT_BUTTON";
		public const OTHER_GUIDES_BUTTON:String = "OTHER_GUIDES_BUTTON";
		public const OTHER_GUIDES_LOADED:String = "OTHER_GUIDES_LOADED";
		
		public var progressPanel:OfflineProgressPanel;
		private var _offlineGuides:Vector.<GuideBasicData>;
		private var _otherGuides:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
		private var _guide:GuideBasicData;
		private var beekDB:SQLConnection = new SQLConnection;
		private var otherGuidesSet:Boolean;
		private var _guideChanged:Boolean;
		private var thisGuideDownloaded:Boolean;
		
		public function SettingsMenu()
		{
			super();
			
			Model.state.addEventListener(State.NETWORK_CHANGE, onNetworkChange);
			Model.state.addEventListener(State.GUIDE_ID_CHANGE, onGuideIDChanged);
			menuButton.addEventListener(MouseEvent.MOUSE_DOWN,toggle);
		
			menuItems.playButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
			menuItems.broadcastButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
			if(menuItems.vrButton)
				menuItems.vrButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
			menuItems.downloadButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
			menuItems.otherGuidesButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
			
			menuItems.playButton.addEventListener(MouseEvent.MOUSE_UP,onPlayButtonMouseUp);
			menuItems.broadcastButton.addEventListener(MouseEvent.MOUSE_UP,onBroadcastButtonMouseUp);
			if(menuItems.vrButton)
				menuItems.vrButton.addEventListener(MouseEvent.MOUSE_UP,onVRButtonMouseUp);
			
			menuItems.playButton.text.autoSize = "left";
			menuItems.broadcastButton.text.autoSize = "left";
			menuItems.vrButton.text.autoSize = "left";
			menuItems.downloadButton.text.autoSize = "left";
			menuItems.otherGuidesButton.text.autoSize = "left";
			
			menuItems.playButton.text.selectable = false;
			menuItems.broadcastButton.text.selectable = false;
			menuItems.vrButton.text.selectable = false;
			menuItems.downloadButton.text.selectable = false;
			menuItems.otherGuidesButton.text.selectable = false;

			
			Fonts.format(menuItems.playButton.text,Fonts.bodyFont,"Auto-Play");
			Fonts.format(menuItems.broadcastButton.text,Fonts.bodyFont,"Connect");
			if(menuItems.vrButton)
				Fonts.format(menuItems.vrButton.text,Fonts.bodyFont,"Virtual Reality");
			Fonts.format(menuItems.downloadButton.text,Fonts.bodyFont, "Download");
			Fonts.format(menuItems.otherGuidesButton.text,Fonts.bodyFont,"Other Guides");
			
		}
		
		private function onNetworkChange(e:Event = null):void
		{
			if(thisGuideDownloaded)
				Fonts.format(menuItems.downloadButton.text,Fonts.bodyFont,"Downloaded");
			
			if(Model.state.networkAccess){
				menuItems.downloadButton.addEventListener(MouseEvent.MOUSE_UP,onDownloadButtonMouseUp);
				menuItems.otherGuidesButton.addEventListener(MouseEvent.MOUSE_UP,onOtherGuidesButtonMouseUp);
				menuItems.downloadButton.alpha = 1;
				menuItems.otherGuidesButton.alpha = 1;
		
			}
			else
			{
				menuItems.downloadButton.alpha = 0.2;
				menuItems.downloadButton.removeEventListener(MouseEvent.MOUSE_UP,onDownloadButtonMouseUp);
				
				if(offlineGuides){
					Fonts.format(menuItems.otherGuidesButton.text,Fonts.bodyFont,"Other Guides ("+offlineGuides.length +")");
					menuItems.otherGuidesButton.addEventListener(MouseEvent.MOUSE_UP,onOtherGuidesButtonMouseUp);
					menuItems.otherGuidesButton.alpha = 1;
				}
				else
				{
					menuItems.otherGuidesButton.alpha = 0.2;
					menuItems.otherGuidesButton.removeEventListener(MouseEvent.MOUSE_UP,onOtherGuidesButtonMouseUp);
				}
			}
		}
		
		
		
		public function connect(db:SQLConnection):void
		{  
			Log.record("SettingsMenu.connect()");
            beekDB = db;
			offlineGuides = queryOfflineGuides();
			onNetworkChange();
		}
		
		public function toggle(e:MouseEvent = null):void
		{
			Log.record("SettingsMenu.onMenuButtonMouseDown()");
			
			var tweenTo:Number = this.y < 0 ? 0 : -menuItems.height * Model.appScale;
			
			Tweener.addTween(this, {
				time : 0.3, 
				y: tweenTo, 
				transition: "easeOutSine",
				onComplete: function():void{
					if(this.y == 0)
						menuItems.mouseChildren = true
					else
						menuItems.mouseChildren = false
				}});
			
			onNetworkChange();
		}
		
		private function onButtonMouseDown(e:MouseEvent):void
		{
			var b:DisplayObject = e.target as DisplayObject;
			b.alpha = 0.5;
		}
		
		private function onPlayButtonMouseUp(e:MouseEvent):void
		{
			Log.record("SettingsMenu.onPlayButtonMouseUp()");
			menuItems.playButton.alpha = 1;
			setTimeout(toggle,200);
			Model.state.mode = State.MODE_AUTO;

		}
		
		private function onBroadcastButtonMouseUp(e:MouseEvent):void
		{
			Log.record("SettingsMenu.onBroadcastButtonMouseUp()");
			menuItems.broadcastButton.alpha = 1;
			dispatchEvent(new MouseEvent(CONNECT_BUTTON));
			setTimeout(toggle,200);
		}
		
	
		
		private function onVRButtonMouseUp(e:MouseEvent):void
		{
			Log.record("SettingsMenu.onVRButtonMouseUp()");
			menuItems.vrButton.alpha = 1;
			dispatchEvent(new MouseEvent(VR_BUTTON));
			setTimeout(toggle,200);
		}
		
		private function onDownloadButtonMouseUp(e:MouseEvent):void
		{
			
			downloadGuide();
			
		}
		
		public function downloadGuide():void
			
		{
			Log.record("SettingsMenu.onDownloadButtonMouseUp()");
			
			progressPanel = new OfflineProgressPanel(GuideBasicData.convert(Model.guideData));
	
					
			var interval = setInterval(function(){

					if(progressPanel.complete){
						clearInterval(interval);
						offlineComplete();
						Model.dispatchEvent(new Event(Model.OFFLINE_COMPLETE));
					}
				}
			,100);
		}
		
		public function checkForOfflineLoad(id:String):GuideBasicData
		{
			Log.record("SettingsMenu.checkForOfflineLoad()");
	
			if(!offlineGuides)
				return null;
			
			if(!guide)
				for each(var basic:GuideBasicData in offlineGuides)
					if(basic.id == id)
						guide = basic;

			if(offlineGuides.indexOf(guide) > -1){
				GuideService.getGuideBasicJson(id, checkIfUpdateRequired)
				thisGuideDownloaded = true;
				menuItems.downloadButton.removeEventListener(MouseEvent.MOUSE_UP,onDownloadButtonMouseUp);
				
				return guide;
			}
			
			return offlineGuides[0];
		}
		
		private function checkIfUpdateRequired(data:GuideBasicData):void
		{
			Log.record("SettingsMenu.checkIfUpdateRequired()");
			if(guide.saveIncrement < data.saveIncrement){
				Fonts.format(menuItems.downloadButton.text,Fonts.bodyFont, "Update");
				menuItems.downloadButton.addEventListener(MouseEvent.MOUSE_UP,onDownloadButtonMouseUp);
			}
			
		}
		
		private function offlineComplete():void
		{
			Log.record("SettingsMenu.onOfflineComplete()");
		
			if(offlineGuides && Model.getGuide(offlineGuides, progressPanel.guideBasic.id) != null)
				updateLocalGuide(progressPanel.guideBasic);
			else
				insertLocalGuide(progressPanel.guideBasic);
			
			offlineGuides = queryOfflineGuides();

			if(progressPanel.guideBasic.id != Model.state.guideId || _guideChanged){
					_guideChanged = false;
					Fonts.format(menuItems.downloadButton.text,Fonts.bodyFont,"Download " + Model.guideData.title);
					menuItems.downloadButton.addEventListener(MouseEvent.MOUSE_UP,onDownloadButtonMouseUp);
			}
			
			progressPanel.dispose();
		}
		
		private function insertLocalGuide(guide:GuideBasicData):void
		{
			Log.record("SettingsMenu.insertLocalGuide()");
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "INSERT INTO `localguides` (`guide_id`, `json`) values (?, ?)";
			insertStatement.parameters[0] = guide.id;     
			insertStatement.parameters[1] = JSONBeek.encode(guide.data);     
			insertStatement.execute();
		}
		
		private function updateLocalGuide(guide:GuideBasicData):void
		{
			Log.record("SettingsMenu.updateLocalGuide()");
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "UPDATE `localguides` SET `json` = ? WHERE `guide_id` = ?";
			insertStatement.parameters[0] = JSONBeek.encode(guide.data);     
			insertStatement.parameters[1] = guide.id;
			insertStatement.execute();
		}
		
		private function queryOfflineGuides():Vector.<GuideBasicData>
		{
			Log.record("SettingsMenu.queryOfflineGuides()");
			var getAllStatement:SQLStatement = new SQLStatement();
			getAllStatement.sqlConnection = beekDB;
			getAllStatement.text = "SELECT * FROM `localguides`";
			getAllStatement.execute();
			
			var data:Array = getAllStatement.getResult().data;
			var offlineGuides:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
			
			if(!data)
				return null;
			
			for(var i:uint = 0; i < data.length; i++)
				offlineGuides.push(new GuideBasicData(JSONBeek.decode(data[i]['json'])));
			
			return offlineGuides;
		}

		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
		
		public function set guide(guide:GuideBasicData):void
		{
			_guide = guide;
		}
		
		
		public function set offlineGuides(value:Vector.<GuideBasicData>):void
		{
			if(!value)
				return;
			
			 _offlineGuides = value;
		}
		
		public function get offlineGuides():Vector.<GuideBasicData>
		{
			return _offlineGuides;
		}
		
		public function set otherGuides(value:Vector.<GuideBasicData>):void
		{
			if(!value)
				return;
			
			_otherGuides = value;
		}
		
		public function get otherGuides():Vector.<GuideBasicData>
		{
			return _otherGuides;
		}
		
		
		private function onGuideIDChanged(e:Event = null):void{
			Log.record("SettingsMenu.onGuideIDChanged()");
			_guideChanged = true;

			
			if(!progressPanel && Model.config.guideId != Model.state.guideId){
				Fonts.format(menuItems.downloadButton.text,Fonts.bodyFont,"Download");
				thisGuideDownloaded = false;
				menuItems.downloadButton.alpha = 1;
			}
		}
		
		private function onOtherGuidesButtonMouseUp(e:MouseEvent):void
		{
			Log.record("SettingsMenu.onOtherGuidesButtonMouseUp()");
			e.target.alpha = 1;
			
			GuideService.getGuideChildren(Model.config.guideId, onOtherGuidesData);
			
			dispatchEvent(new MouseEvent(OTHER_GUIDES_BUTTON));		
		}
		
		private function onOtherGuidesData(data:GuideBasicList):void
		{
			Log.record("SettingsMenu.onOtherGuidesData("+data.guides.length+")");
			
			dispatchEvent(new MouseEvent(OTHER_GUIDES_LOADED));			
			if(!data || !data.guides ||otherGuidesSet)
			return;
			
			otherGuidesSet = true;
			
			otherGuides = data.guides;
			
			var thisGuide:GuideBasicData = GuideBasicData.convert(Model.guideData);
			otherGuides.push(thisGuide);
			
			if(Model.guideData.parent)
				GuideService.getGuideBasicJson(Model.guideData.parent, onParentLoaded)
			else
			dispatchEvent(new MouseEvent(OTHER_GUIDES_LOADED));			
		}
		
		private function onParentLoaded(data:GuideBasicData):void
		{
		
			otherGuides.push(data);
			dispatchEvent(new MouseEvent(OTHER_GUIDES_LOADED));			
		}

		public function get guideChanged():Boolean
		{
			return _guideChanged;
		}

		public function set guideChanged(value:Boolean):void
		{
			_guideChanged = value;
		}

		
	}
}