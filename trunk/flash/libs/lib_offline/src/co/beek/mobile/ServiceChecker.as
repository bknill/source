package co.beek.mobile
{
	import air.net.URLMonitor;
	
	import co.beek.Log;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	public class ServiceChecker extends EventDispatcher
	{
		public static const CHECKED:String = "CHECKED";
		
		private var urlRequest:URLRequest = new URLRequest("https://gms.beek.co/user/session/");
		
		private	var loader:URLLoader = new URLLoader();

		private var checkedEvent:Event = new Event(CHECKED);
		
		private var _available:Boolean;
		
		private var monitor:URLMonitor;
		
		public function ServiceChecker()
		{
			Log.record("ServiceChecker()");
			super();
			
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			
			loader.addEventListener(Event.COMPLETE, onDataLoaded);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, onIOError);
		}
		
		public function get available():Boolean
		{
			return _available;
		}
		
		public function start():void
		{
			_available = false;
			try
			{
				trace(urlRequest.url);
				loader.load(urlRequest);
			}
			catch ( e:Error )
			{
				dispatchEvent(checkedEvent);
			}
			
		/*	monitor = new URLMonitor(urlRequest);
			monitor.addEventListener(StatusEvent.STATUS, netConnectivity);
			monitor.start();*/
		}
		
		private function onDataLoaded(event:Event):void
		{
			Log.record("ServiceChecker.onDataLoaded()")
			var json:String = URLLoader(event.target).data;
			trace(json);
			try{
				if(json.charAt(0) == "{")
				{
					var data:Object = JSONBeek.decode(json);
					var response:ServiceResponse = new ServiceResponse(data);
					
					_available = true;
					
				}
			}
			catch ( e:Error )
			{
				// should rarely get here
				Log.record("ServiceChecker error:"+json)
			}
			
			dispatchEvent(checkedEvent);
		}
		
		
		protected function netConnectivity(e:StatusEvent):void 
		{
			if(monitor.available)
			{
				_available = true;
			}
			else
			{
				_available = false;
			}
			
			monitor.stop();
			dispatchEvent(checkedEvent);
			monitor.removeEventListener(StatusEvent.STATUS, netConnectivity);
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace(event.errorID, event.text);
			dispatchEvent(checkedEvent);
		}
	}
}