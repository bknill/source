package co.beek.mobile
{
	import away3d.core.math.Quaternion;
	
	import flash.events.Event;
	
	public class DeviceMotionQuaternionEvent extends Event
	{
		
		private var _quaternion:Quaternion
		
		public function DeviceMotionQuaternionEvent(type:String, quaternion:Quaternion)
		{
			trace(quaternion);
			super(type, bubbles, cancelable);
			_quaternion = quaternion;
			
		}
		
		public function get quaternion():Quaternion
		{return _quaternion}
	}
}