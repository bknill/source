package co.beek.mobile
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.ColorTransform;
	import flash.utils.clearInterval;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.guide.GuideSceneData;
	
	public class BookmarkButton extends BookmarkRendererFl
	{
		
		public static const BOOKMARK_REMOVED:String = "BOOKMARK_REMOVED";
		
		private var _scene:SceneBasicData;
		private var dragInt:int;
		private var initalMouseY:Number;
		private var initalY:Number;
		public var buttonWidth:int = 90;
		public var buttonHeight:int = 70;
		public var sceneId:String;
		private var _title:String;

		
			
		
		public function BookmarkButton(id:String, title:String, thumb:String, addButton:Boolean = false)
		{
			super();
			ImageLoader.load(
				Model.config.getAssetUrl(thumb), 
				bookmarkImageLoadCallback,
				bookmarkImageLoadCallbackError
			);	

			
			if(addButton)
			{
				Fonts.format(label, Fonts.bodyFont, "Add bookmark");
				var fade = 0.6;
				locator.transform.colorTransform = new ColorTransform(fade, fade, fade, 1.0, 0, 0, 0, 0);
			}
			else
			{
				Fonts.format(label, Fonts.bodyFont, title);
				plus.visible = false;
			}
			
			sceneId = id;
			_title = title;

			addEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP,onMouseUp);
			

			bg.filters = [new DropShadowFilter(1.0,45,0x000000,0.3,4.0,4.0,1.0,1,false,false,false)];	
		}
		
		public function bookmarkThisScene():void
		{
			var fade = 1;
			locator.transform.colorTransform = new ColorTransform(fade, fade, fade, 1.0, 0, 0, 0, 0);
			plus.visible = false;
			Fonts.format(label, Fonts.bodyFont, _title);
		}
		
		
		private function bookmarkImageLoadCallback(data:BitmapData):void
		{
			trace("imagePageLoadCallback()");
			var buttonImage:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
			buttonImage.bitmapData = data;
			locator.addChild(buttonImage);
			buttonImage.width = buttonWidth;
			buttonImage.height = buttonHeight;	

		}
		
		private function bookmarkImageLoadCallbackError():void
		{
			trace("GuideSectionPage.imagePageLoadCallbackError");
		}
		
		public function get scene():SceneBasicData
		{return _scene}
		
		private function onMouseDown(e:MouseEvent):void
		{			
			trace('BookmarkButton.onMouseDown()');
			alpha = 0.6;
			addEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
		}
		
		
		private function onMouseUp(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseUp()');
			Model.loadGuideScene(sceneId);
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
			setTimeout(function(){alpha = 1},10);
			this.y = 0;
		}

		private function onMouseMove(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMove()');
			removeEventListener(MouseEvent.MOUSE_UP,onMouseUp);
			initalMouseY = stage.mouseY;
			initalY = this.y;
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
			addEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
			addEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);

		}
		
		private function onMouseMoving(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMoveUp()');
			drag();
		}
		
		private function onMouseMoveUp(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMoveUp()');
			//clearInterval(dragInt);
			this.y = initalY;
			removeEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
		}
		
		private function fadeAway():void
		{
			trace('BookmarkButton.fadeAway()');
				Tweener.addTween(this, {
					time:0.2,
					alpha: 0,
					transition: "easeInCubic",
					onComplete: dispose});
		}	
		
		private function dispose():void
		{
			trace('BookmarkButton.dispose()');
			dispatchEvent(new Event(BOOKMARK_REMOVED));
			removeEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			removeEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
			removeChildren();
		}
		
		private function drag():void
		{
			trace("drag");
			if(this.y < - this.height * 1.5)
			{
				clearInterval(dragInt);
				fadeAway();
			}
			else if(this.y <= initalY)
				this.y = stage.mouseY - initalMouseY;
		}
	}
}