package co.beek.mobile
{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.ColorTransform;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.service.VisitService;

	public class Bookmarks extends BookmarkButtonFl
	{
		
		public static const EMAIL_OPEN:String = "EMAIL_OPEN";
		public static const EMAIL_CLOSED:String = "EMAIL_CLOSED";

		private var beekDB:SQLConnection;
		private var buttons:Vector.<BookmarkButton> = new Vector.<BookmarkButton>;
		public var origin:Number;
		public var horizontalSpace:Number;
		public var renderersOrigin:Number;
		private var renderersInScale:Number;
		private var data:Array;
		private var renderers:MovieClip = new MovieClip;
		private var questionPanel:QuestionFl = new QuestionFl;
		private var addBookmarkButton:BookmarkButton;
		private static var _email:String;
		private static var dataToSend:Array = new Array;
		private var unCompletedData:Array = new Array;
		private var listToSend:Array = new Array;
		
		private var bX:int = 0;
		private var bY:int = 0;
		private var fade:Number;
		
		public function Bookmarks()
		{
			super();
			bmButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			bmButton.addEventListener(MouseEvent.MOUSE_UP, onBookMarkMouseUp);
			addChildAt(renderers,0);
			renderersOrigin = renderers.x = 25;
			renderers.scaleX = renderers.scaleY = renderersInScale = 40/90;
		
			emailButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			emailButton.addEventListener(MouseEvent.MOUSE_UP, onEmailMouseUp);
			emailButton.filters = [new DropShadowFilter(1.0,45,0x000000,0.3,4.0,4.0,1.0,1,false,false,false)];
			
			
			bmButton.plus.visible = false;
			bmButton.filters = [new DropShadowFilter(1.0,45,0x000000,0.3,4.0,4.0,1.0,1,false,false,false)];
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		
		}
		
		public function isDown():Boolean
		{return Math.round(this.y) > Math.round(origin)}
		
		public function toggle(event:Event = null):void
		{
			Log.record("Bookmarks.toggle()");

			
			//buttons container
			Tweener.addTween(renderers,{
				x : isDown() ? bmButton.width + 2 : renderersOrigin, 
				scaleX : isDown() ? 1 : renderersInScale, 
				scaleY : isDown() ? 1  : renderersInScale,
				y: isDown() ? 0 : 2,
				time: 0.3, 
				transition: "easeOutSine"});

			tweenRenderers();
			
			//whole thing
			Tweener.addTween(this, {
				time : 0.3, 
				y: isDown() ? origin : origin + (emailButton.height * Model.appScale) + 1,
				transition: "easeInOutSine"});
			
			if(isDown() && !isSceneBookmarked())
				bmButton.plus.visible = true;
			else
				bmButton.plus.visible = false;
			
		}
		
		private function tweenRenderers(e:Event = null):void
		{
			bX = 0;
			bY = 0;
			fade = isDown() ? 1 : 0.95;
			
			//buttons
			for each(var button:BookmarkButton in buttons){
				button.transform.colorTransform = new ColorTransform(fade, fade, fade, 1.0, 0, 0, 0, 0);
				Tweener.addTween(button,{
					x : bX, 
					y : bY, 
					time: 0.3,
					transition: "easeInOutSine"});
				
				//new row if reached width limit
				bX += isDown() ?  button.width + 2 : 5;
				if(bX > horizontalSpace - button.width){
					bX = 0;
					bY -= button.height + 2;
				}
			}
		}

		
		public function connect(db:SQLConnection):void
		{
			Log.record("Bookmarks.connect()")
			beekDB = db;
			var createBookmarkTableStmt:SQLStatement = new SQLStatement();
			createBookmarkTableStmt.sqlConnection = beekDB;
			createBookmarkTableStmt.text = "CREATE TABLE IF NOT EXISTS `bookmarkScenes` (" +
				"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`scene_id` INTEGER, " +
				"`guide_id` INTEGER, " +
				"`scene_title` TEXT," +
				"`scene_thumb` TEXT,"  +
				"`email` TEXT,"  +
				"`key` TEXT"  +
				")";
			createBookmarkTableStmt.execute();
			
			loadBookmarkScenes();
		
			
		}
		
		public function bookmarkScene():void{
			trace('bookmarkScene()');

			if(renderers)
				if(renderers.getChildByName(Model.currentScene.id || Model.guideData.firstScene.scene.id) || !Model.guideScene)
					return;
			
			var guideScene:GuideSceneData = Model.guideScene || Model.guideData.firstScene;
			
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "INSERT INTO `bookmarkScenes` (`scene_id`,`guide_id`, `scene_title`, `scene_thumb`) values (?, ?, ?, ?)";
			insertStatement.parameters[0] = guideScene.scene.id;     
			insertStatement.parameters[1] = Model.state.guideId;
			insertStatement.parameters[2] = guideScene.titleMerged;
			insertStatement.parameters[3] = guideScene.scene.thumbName;  
			insertStatement.execute();
			
			Model.dispatchEvent(new Event(Model.BOOKMARK_SCENE));
			
			bmButton.plus.visible = false;
			
			loadBookmarkScenes();
			
		}
		
		public function loadBookmarkScenes():void
		{
			trace('Bookmarks.loadBookmarkScenes()');
			
			var getAllStatement:SQLStatement = new SQLStatement();
			getAllStatement.sqlConnection = beekDB;
			getAllStatement.text = "SELECT * FROM `bookmarkScenes`";
			getAllStatement.execute();
			
			data = getAllStatement.getResult().data;
			
			var exists:Boolean = false;
			
			for each(var bookmark:Object in data)
			{
				//data with email assigned is offline data and shouldn't be shown
				if(bookmark.email){
					dataToSend.push(bookmark)
					continue;
				}
				
				//complex check for doubles
				exists = false;
				
				for each(var button:BookmarkButton in buttons)
					if(button.name == bookmark.scene_id)
						exists = true;

				if(exists)
					continue;

				var bm:BookmarkButton = new BookmarkButton(bookmark.scene_id, bookmark.scene_title, bookmark.scene_thumb);
				bm.name = bookmark.scene_id;
				bm.x = bX;
				renderers.addChildAt(bm,0);
				buttons.push(bm);
				bm.addEventListener(BookmarkButton.BOOKMARK_REMOVED, onBookmarkRemoved);
			}
			
			//send offline emails if they exist - 5 sec delay for loading
			if(dataToSend.length > 0 && Model.state.networkAccess)
				setTimeout(sendOfflineEmails,5000,dataToSend);
			
			
			//Update UI if there is data
			if(data){
					
				Fonts.format(bmButton.counter, Fonts.titleFont, buttons.length.toString());
				
				if(emailButton.alpha < 1){
					emailButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
					emailButton.addEventListener(MouseEvent.MOUSE_UP, onEmailMouseUp);
					emailButton.alpha = 1;
				}
			}
			else{
				
				Fonts.format(bmButton.counter, Fonts.titleFont, "0");
				
				emailButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
				emailButton.removeEventListener(MouseEvent.MOUSE_UP, onEmailMouseUp);
				emailButton.alpha = 0.5;
			}
		}
		
		private function isSceneBookmarked():Boolean
		{
			if(!buttons || !Model.currentScene)
				return false;

			return buttons.some(function(button:BookmarkButton, currentIndex:int, buttons:Vector.<BookmarkButton>)
			{return button.sceneId == Model.currentScene.id})
		}
		
		private function onBookmarkRemoved(e:Event):void
		{
			trace('onBookmarkRemoved()');
			
			var bm:BookmarkButton = e.target as BookmarkButton;
			bm.removeEventListener(BookmarkButton.BOOKMARK_REMOVED, onBookmarkRemoved);
			
			removeBookmark(bm.sceneId);

			loadBookmarkScenes();
			
		}
		
		private function removeBookmark(sceneId:String):void
		{
			var removeStatement:SQLStatement = new SQLStatement();
			removeStatement.sqlConnection = beekDB;
			removeStatement.text = "DELETE FROM `bookmarkScenes` WHERE `scene_id` = :sceneId";
			removeStatement.parameters[":sceneId"] = sceneId;
			removeStatement.execute();
		}
		
		private function removeOfflineBookmark(sceneId:String, key:String):void
		{
			Log.record("Bookmarks.removeOfflineBookmark("+sceneId+")");
			var removeStatement:SQLStatement = new SQLStatement();
			removeStatement.sqlConnection = beekDB;
			removeStatement.text = "DELETE FROM `bookmarkScenes` WHERE `scene_id` = :sceneId AND `key` = :key";
			removeStatement.parameters[":sceneId"] = sceneId;
			removeStatement.parameters[":key"] = key;
			removeStatement.execute();
		}
		
		private function onButtonMouseDown(e:MouseEvent):void
		{
			bmButton.alpha = 0.8;
		}
		
		private function onBookMarkMouseUp(e:MouseEvent):void{
			bmButton.alpha = 1;

			toggle();
			
			//after tween add or remove new bookmark button
			setTimeout(function(){
				
				if(!isDown()&& !isSceneBookmarked())
					bookmarkThisSceneButton()
				else
					bookmarkThisSceneButton(false)
				
			},500);
		
				

		}	
		
		private function bookmarkThisSceneButton(add:Boolean = true):void
		{
			trace('swf_mobile.bookmarkThisSceneButton()');
			if(add){
				
				if(addBookmarkButton)
					renderers.removeChild(addBookmarkButton);
				
				addBookmarkButton = null;
				
				var bm:BookmarkButton = new BookmarkButton(Model.currentScene.id, Model.guideScene.titleMerged, Model.currentScene.thumbName, true);
				bm.x = bX;
				bm.y = bY;
				addBookmarkButton = bm;
				addBookmarkButton.addEventListener(MouseEvent.MOUSE_DOWN,onButtonMouseDown);
				addBookmarkButton.addEventListener(MouseEvent.MOUSE_DOWN,onBookmarkThisSceneButtonUp);
				renderers.addChildAt(bm,0);
			}
			else{
				if(addBookmarkButton && renderers.contains(addBookmarkButton))
					renderers.removeChild(addBookmarkButton);
				
				addBookmarkButton = null;
			}
			
		}
		
		private function onBookmarkThisSceneButtonUp(e:Event = null):void
		{
			addBookmarkButton.bookmarkThisScene();
			bookmarkScene();

		}
		
		private function onSceneChange(e:Event = null):void
		{
			//have to wait until current scene is set
			setTimeout(function():void{
				if(!isSceneBookmarked())
					bmButton.plus.visible = true;
				if(!isDown())
					bookmarkThisSceneButton();
			},500);
		
		}
		
		private function onEmailMouseUp(e:MouseEvent):void{
			bmButton.alpha = 1;
			
			questionPanel.x = stage.stageWidth/2 - questionPanel.width/2;
			questionPanel.y = - stage.stageHeight/2 - (questionPanel.height/2 + renderers.y);
			addChild(questionPanel);
			
			questionPanel.close.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			questionPanel.close.addEventListener(MouseEvent.MOUSE_UP, closeEmail);
			
			questionPanel.submit.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			questionPanel.submit.addEventListener(MouseEvent.MOUSE_UP, onSubmitMouseDown);
			
			Fonts.format(questionPanel.question,Fonts.bodyFont,
				"Please enter your email address to send your bookmarks to your to your email. (Removes all bookmarks) ");
			
			Fonts.format(questionPanel.answer,Fonts.boldFont,"email");
			dispatchEvent(new Event(EMAIL_OPEN));
			
		}	
		
		private function closeEmail(e:Event = null):void
		{
			removeChild(questionPanel);
		}
		
		private function onSubmitMouseDown(e:Event):void
		{
			Log.record("Bookmarks.onSubmitMouseDown()");
			
			_email = questionPanel.answer.text;
			setTimeout(closeEmail,200);
			
			var updateStatement:SQLStatement = new SQLStatement();
			updateStatement.sqlConnection = beekDB;
			
			
			//Either send email and remove or update database with email & key for offline
			if(Model.state.networkAccess){
				sendEmail(_email,data);
				for each(var object:Object in data)
					removeBookmark(object.scene_id);
			}
			else
				for each(var object:Object in data)
				{
					if(object.email)
						continue;
					
					updateStatement.text = "UPDATE `bookmarkScenes` SET `email` = :email, `key` = :key WHERE `scene_id` = :sceneId";
					updateStatement.parameters[":key"] = object.key = VisitService.key;
					updateStatement.parameters[":email"] = object.email = _email;
					updateStatement.parameters[":sceneId"] = object.scene_id;
					updateStatement.execute();
				};
			
			clearRenderers();
			loadBookmarkScenes();
			VisitService.getNewKey();
				
		}
		
		private function clearRenderers():void
		{
			while(renderers.numChildren > 0)
				renderers.removeChildAt(0);
			
			Fonts.format(bmButton.counter, Fonts.titleFont, "0");
		}
		

		private function onFavouritesSent(e:Event):void
		{
			VisitService.removeEventListener(VisitService.FAVOURITES_SENT, onFavouritesSent);
			for each(var sentObject:Object in listToSend)
			removeOfflineBookmark(sentObject.scene_id, sentObject.key)
			
			
			if(unCompletedData.length > 0)
				sendOfflineEmails(unCompletedData);
		}
		
		private function sendOfflineEmails(data:Array):void
		{
			Log.record("Bookmarks.sendOfflineEmails()");
			
			if(data)
				var currentEmail:String = data[0].email;
			else
				return;

			for each(var object:Object in data){
				if(currentEmail == object.email)
					listToSend.push(object)
				else{
					
					sendEmail(currentEmail, listToSend);
					VisitService.addEventListener(VisitService.FAVOURITES_SENT, onFavouritesSent);
					unCompletedData	= data;
					return;
				}
			}
		}
		
		private static function sendEmail(email:String, data:Array):void
		{
			Log.record("Bookmarks.sendEmail("+email+","+data.length+")");
			VisitService.email = email;
			for each(var object:Object in data){
				for each(var guideScene:GuideSceneData in Model.guideSceneList)
					if(guideScene.scene.id == object.scene_id)
						VisitService.favouriteScene(guideScene)
			}
			
			VisitService.sendFavourites();
		}
		

		
	}
}