package co.beek.utils
{
	import co.beek.model.Constants;
	
	import flash.system.Capabilities;

	public class Device
	{
		public static function getDevice():String {
			var info:Array = Capabilities.os.split(" ");
			if (info[0] + " " + info[1] != "iPhone OS") {
				return Constants.UNKNOWN;
			}
			
			// ordered from specific (iPhone1,1) to general (iPhone)
			for each (var device:String in Constants.IOS_DEVICES) {	
				if (info[3].indexOf(device) != -1) {
					return device;
				}
			}
			return Constants.UNKNOWN;
		}
		
		
	}
}