package co.beek.utils
{

import flash.net.InterfaceAddress;
import flash.net.NetworkInfo;
import flash.net.NetworkInterface;

public class NetworkUtil
{
	public static var address:InterfaceAddress;
	public static var InterfaceAddresses:Vector.<InterfaceAddress> = new Vector.<InterfaceAddress>;
	
	{
		getAddresses(); //Get the adress of this host
	}
	
	public static function getAddresses():void
	{
		var interfaceVector:Vector.<NetworkInterface> = NetworkInfo.networkInfo.findInterfaces();
		for each (var networkInt:NetworkInterface in interfaceVector){
			var name:String;
			name = networkInt.displayName.toLowerCase();
			if (networkInt.active)
				if(name.indexOf("wi") > -1 || name.indexOf("local")  > -1)
				for each (var addresss:InterfaceAddress in networkInt.addresses) {
					if(addresss.ipVersion == "IPv4" && addresss.address != "127.0.0.1")
						InterfaceAddresses.push(addresss);
				}
				
		}
		
		if(InterfaceAddresses.length > 0 && !address)
			address = InterfaceAddresses[0]
	

	}
	
	public static function getPrefixLength(address):int
	{
		return address.prefixLength;
	}
	
	public static function getBroadcastIp():String
	{
		return address.broadcast;
	}
	
	public static function getIpAddress():String
	{
		return address.address;
	}
	
	public static function getIpAddresses():Array
	{
		var ips:Array = new Array;
		for each(var address:InterfaceAddress in InterfaceAddresses)
				ips.push(address.address)
			
		return ips;
	}
	
	public static function getSubnetIp(currentIp:Array, subnetMask:Array):String
	{           
		for(var i:int; i < 4; i++){
			currentIp[i] = (subnetMask[i] == 255 ? currentIp[i] : 0);
		}
		
		return currentIp[0] + "." + currentIp[1] + "." + currentIp[2] + "." + currentIp[3];
	}
	
	public static function getAmountOfHosts(prefixLength:int):int
	{
		return (256 << (24-prefixLength)) -2;
	}
	
	public static function getSubnetMask(prefixLength:int):Array
	{
		var subnetMask:Array = [];
		
		for(var i:int = 0; i < 4; i++){
			var subnet:uint = 255;
			if(prefixLength >= 8){
				prefixLength-=8;
			}else{
				subnet = 255 - (255 >> prefixLength);
				prefixLength=0;
			}
			
			subnetMask[i] = subnet;
		}
		return subnetMask;
	}
	
	public static function isValidIp(ip:String):Boolean {
		ip = ip.replace( /\s/g, ""); //remove spaces for checking
		var pattern:RegExp = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
		
		if(pattern.test(ip)){
			var octets:Array = ip.split(".");
			if (parseInt(String(octets[0])) == 0) {
				return false;
			}
			if (parseInt(String(octets[3])) == 0) {
				return false;
			}
			return true;
		}
		return false;
	}
}
}