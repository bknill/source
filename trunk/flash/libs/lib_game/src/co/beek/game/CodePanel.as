package co.beek.game
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.StringUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;

	public class CodePanel extends CodePanellFl
	{
		public static const HIDE:String = "HIDE";
		
		private var originalHeight:Number;
		
		public function CodePanel()
		{
			super();
			closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			goButton.addEventListener(MouseEvent.CLICK, onGoButtonClick);
			presentationModeButton.addEventListener(MouseEvent.CLICK, onPresentationModeButtonClick);
			
			codeField.displayAsPassword = true;
			codeField.text = "";//interislander";
			incorrectCode.visible = false;
			codeCorrect.visible = false;
			originalHeight = bgHeight;
			
			ColorUtil.colorize(border, Model.themeColor);
		}
		
		public function get codeEnteredCorrectly():Boolean
		{
			return StringUtil.trim(codeField.text) == Model.guideData.gameCode;
		}
		
		public function update():void
		{
			var value:Boolean = Model.state.uiState == State.UI_STATE_PRESENTATION_MODE;
			
			presentationModeButton.visible = value;
			presentationModeText.visible = value;
			
			instructions.visible = !value;
			codeField.visible = !value;
			goButton.visible = !value;
			
			codeCorrect.visible = false;
			
			if(value)
				bgHeight = presentationModeButton.y + presentationModeButton.height + 15;
			else
				bgHeight = goButton.y + goButton.height + 15;
				
		}
		
		public function get bgHeight():Number
		{
			return border.height;
		}
		
		public function set bgHeight(value:Number):void
		{
			border.height = value;
			bg.height = value - bg.y*2;
		}
		
		private function onPresentationModeButtonClick(event:MouseEvent):void
		{
			Model.state.uiState = State.UI_STATE_NORMAL;
			dispatchEvent(new Event(HIDE));
		}
		
		private function onGoButtonClick(event:MouseEvent):void
		{
			if(codeEnteredCorrectly)
			{
				instructions.visible = false;
				codeCorrect.visible = true;
				
				codeField.visible = false;
				goButton.visible = false;
				
				Tweener.addTween(this, {
					'time' : 0.5,
					'bgHeight' : codeCorrect.y + codeCorrect.height+15,
					'onComplete' : hideLoginPanelDelayed
				});
			}
			else
			{
				Tweener.addTween(this, {
					'time' : 0.5,
					'bgHeight' : incorrectCode.y + incorrectCode.height+15,
					'onComplete' : onShowIncorrectCodeComplete
				});
			}
		}
		
		private function onShowIncorrectCodeComplete():void
		{
			incorrectCode.visible = true;
			setTimeout(hideIncorrectCode, 3000)
		}
		
		private function hideIncorrectCode():void
		{
			// tween Back to the original pos
			incorrectCode.visible = false;
			Tweener.addTween(this, {
				'time' : 0.5,
				'bgHeight' : originalHeight
			});
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			hideLoginPanelComplete();
		}
		
		
		private function hideLoginPanelDelayed():void
		{
			setTimeout(hideLoginPanel, 2000);
		}
		
		private function hideLoginPanel():void
		{
			Model.state.uiState = State.UI_STATE_PRESENTATION_MODE;
			dispatchEvent(new Event(HIDE));
		}
		
		private function hideLoginPanelComplete():void
		{
			alpha = 1;
			dispatchEvent(new Event(HIDE));
		}
		
	}
}