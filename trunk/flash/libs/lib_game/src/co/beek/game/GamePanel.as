package co.beek.game
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.events.TrackingEvent;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GameTaskHotspot;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.utils.ColorUtil;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;

	public class GamePanel extends GamePanelFl
	{
		private var _canShow:Boolean;
		
		private var _currentTask:GameTaskData;
		
		public function GamePanel()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			visible = false;
			bgHeight = 10;
		}
		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			if(Model.currentTask)
				showCurrentTask();
			
			Model.addEventListener(Model.SESSION_CHANGE, onSessionChanged);
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChanged);
			Model.addEventListener(Model.GAME_TASK_CHANGED, onGameTaskChanged);
			Model.addEventListener(Model.GAME_TASK_CHANGED_INSTANT, onGameTaskChangedInstant);
			Model.addEventListener(Model.GAME_HOTSPOT_FOUND, onGameHotspotFound);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE, onUIStateChange);
			
			prevButton.addEventListener(MouseEvent.CLICK, onPrevClick);
			stepButton.addEventListener(MouseEvent.CLICK, onStepClick);
			nextButton.addEventListener(MouseEvent.CLICK, onNextClick);
			
			prevButton.visible = stepButton.visible = nextButton.visible = false;
		}
		
		/*public function set autoPlay(value:Boolean):void
		{
			_autoplay = value;
			_hotspotFindIndex = 0;
			
			Tweener.addTween(this, {
				"time":1,
				"alpha":0,
				"onComplete":onFadeAwayComplete
			});
			//showCurrentTask();
		}
		
		private function onFadeAwayComplete():void
		{
			alpha = 1;
			visible = false;
		}*/
		
		private function onPrevClick(event:MouseEvent):void
		{
			Model.currentTaskIndexInstant = Model.currentTaskIndex-1;
		}
		
		private function onStepClick(event:MouseEvent):void
		{
			executeNextTaskPrivate();
		}
		
		private function onNextClick(event:MouseEvent):void
		{
			Model.currentTaskIndexInstant = Model.currentTaskIndex+1;
		}
		
		public function set canShow(value:Boolean):void
		{
			_canShow = value;
			if(!_canShow)
				visible = false;
		}
		
		
		private function onSessionChanged(event:Event):void
		{
			// not sure what we are doing here
			if(Model.hasGame)
				showCurrentTask();
		}
		
		private function onUIStateChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_PRESENTATION_MODE
				|| Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				visible = false;
			
			else if(Model.state.uiState == State.UI_STATE_NORMAL)
				visible = Model.guideData.gameTasks.length > 0 
					&& Model.currentTaskIndex < Model.guideData.gameTasks.length;
			
			prevButton.visible = Model.prevTask != null && Model.state.uiState != State.UI_STATE_NORMAL;
			stepButton.visible = Model.currentTask != null && Model.state.uiState != State.UI_STATE_NORMAL;
			nextButton.visible = Model.nextTask != null && Model.state.uiState != State.UI_STATE_NORMAL;
			
			showCurrentTask();
		}
		
		/*private function onEditingGameChange(event:Event):void
		{
			//prevButton.visible = nextButton.visible = Model.state.editingGame;
			//showCurrentTask();
		}*/
		
		private function onGuideChanged(event:Event):void
		{
			ColorUtil.colorize(border, Model.themeColor);
		}
		
		public function onPanoHotspotsReady():void
		{
			if(!_canShow || visible || !Model.hasGame)
				return;
			
			if(Model.state.uiState == State.UI_STATE_PRESENTATION_MODE)
				showCurrentHotspot();
			else
				setTimeout(startGame, 2000);
				
		}
		
		private function startGame():void
		{
			visible = true;
			showCurrentTask();
		}
		
		private var endGameTimeout:uint;
		
		
		private function showCurrentTask():void
		{
			if(!_canShow)
				return;

			//if(Model.state.uiState == State.UI_STATE_PRESENTATION_MODE)
			//	return;
			
			if(Model.loggedIn || Model.state.uiState == State.UI_STATE_PRESENTATION_MODE )
			{
				prevButton.visible = Model.prevTask != null;
				stepButton.visible = Model.currentTask != null;
				nextButton.visible = Model.nextTask != null;
			}
			
			if(_currentTask)
				_currentTask.removeEventListener(Event.CHANGE, onTaskDetailsChange);
			_currentTask = Model.currentTask;
			_hotspotFindIndex = -1;
			
			if(Model.state.uiState != State.UI_STATE_PRESENTATION_MODE)
				visible = true;
			
			trophyIcon.visible = false;
			instructionsIcon.visible = false;
			feedbackField.visible = false;
			feedbackIcon.visible = false;
			hotspotsFound.visible = false;
			
			// stops the panel dissappearing when editing
			clearTimeout(endGameTimeout);
			
			if(_currentTask)
			{
				instructionsIcon.visible = true;
				instructionsField.text = _currentTask.instructions;
				instructionsField.height = Math.max(35, instructionsField.textHeight + 20);
				_currentTask.addEventListener(Event.CHANGE, onTaskDetailsChange);
			}
			else if(Model.prevTask)
			{
				instructionsIcon.visible = true;
				instructionsField.text = Model.prevTask.feedback;//"Congratulations, you completed the "+Model.guideData.title+" game.";
				instructionsField.height = Math.max(35, instructionsField.textHeight + 20);
				//trophyIcon.visible = true;
				//instructionsField.text = "Congratulations, you completed the "+Model.guideData.title+" game.";
				//instructionsField.height = Math.max(35, instructionsField.textHeight + 20);
				
				if(!Model.loggedIn && Model.state.uiState == State.UI_STATE_NORMAL)
					endGameTimeout = setTimeout(endGame, 10000);
			}
			else
			{
				visible = false;
			}
			var destHeight:Number = instructionsField.y + instructionsField.height;
			if(prevButton.visible || nextButton.visible || stepButton.visible)
				destHeight += 40;
			
			// when creating a new task and hte code is not yet set
			if(_currentTask && _currentTask.code == null)
				destHeight = 45;
			
			Tweener.removeTweens(this);
			Tweener.addTween(this, { 
				time: 1,
				bgHeight: destHeight
			});
			
		}
		
		private function onTaskDetailsChange(event:Event):void
		{
			instructionsField.text = Model.currentTask.instructions;
			instructionsField.height = Math.max(35, instructionsField.textHeight + 20);
			var destHeight:Number  = instructionsField.y + instructionsField.height;
			
			if(prevButton.visible || nextButton.visible)
				destHeight += 40;
			
			bgHeight = destHeight;
		}
		
		private function onGameTaskChanged(event:Event):void
		{
			_hotspotFindIndex = -1;
			
			//if(Model.state.uiState == State.UI_STATE_PRESENTATION_MODE)
			//	return;
			
			var feedbackY:Number = instructionsField.y + instructionsField.height;
			if(hotspotsFound.visible)
				feedbackY += hotspotsFound.height+10;
			
			feedbackField.text = Model.prevTask.feedback;
			feedbackField.height = feedbackField.textHeight + 20;
			feedbackField.y = feedbackY;
			
			
			feedbackIcon.y = feedbackY + 4;
			//continueButton.y = feedbackField.y + feedbackField.height;
			
			var destHeight:Number = feedbackField.y + feedbackField.height;
			if(prevButton.visible || nextButton.visible || stepButton.visible)
				destHeight += 40;
			
			setTimeout(showCurrentTask, 5000)
			Tweener.removeTweens(this);
			Tweener.addTween(this, { 
				time: 1,
				bgHeight: destHeight,
				onComplete: onTweenToFeedbackComplete
			});
			
			
		}
		
		private function onGameTaskChangedInstant(event:Event):void
		{
			showCurrentTask();
		}
		
		private function onTweenToFeedbackComplete():void
		{

			feedbackField.visible = true;
			feedbackIcon.visible = true;
			
			//continueButton.visible = true;
		}
		
		public function get bgHeight():Number
		{
			return border.height;
		}

		public function set bgHeight(value:Number):void
		{
			border.height = value;
			bg.height = value - bg.y*2;
			
			prevButton.y = stepButton.y = nextButton.y = value-40;
		}
		
		private function endGame():void
		{
			visible = false;
		}
		
		private function onGameHotspotFound(event:Event):void
		{
			hotspotsFound.y = instructionsField.y + instructionsField.height;
			hotspotsFound.visible = true;
			
			while(hotspotsFound.renderersLocator.numChildren > 0)
				hotspotsFound.renderersLocator.removeChildAt(0);
			
			var found:Vector.<HotspotData> = Model.hotspotsFound;
			for(var i:int; i<found.length; i++)
			{
				var renderer:GameHotspotRenderer = new GameHotspotRenderer;
				renderer.hotspotTitleField.text = found[i].title;
				renderer.y = hotspotsFound.renderersLocator.height;
				hotspotsFound.renderersLocator.addChild(renderer);
			}
			
			var destHeight:Number = hotspotsFound.y + hotspotsFound.height + 20;
			if(prevButton.visible || nextButton.visible || stepButton.visible)
				destHeight += 40;
			
			Tweener.removeTweens(this);
			Tweener.addTween(this, { 
				time: 1,
				bgHeight: destHeight
			});
		}
		
		private var _hotspotFindIndex:int = -1;
		/*private function executePrevTask():void
		{
			trace("executePrevTask()")
			Model.backwards = true;
			
			var task:GameTaskData = Model.currentTask;
			if(!task)
				return;
			
			if(task.code == Constants.GAME_FIND_HOTSPOTS)
			{
				_hotspotFindIndex --;
				if(_hotspotFindIndex < 0)
					_hotspotFindIndex = task.hotspotsToFind.length-1;
				
				for(var i:int;i<task.hotspotsToFind.length; i++)
					trace(task.hotspotsToFind[i].title+(i == _hotspotFindIndex ? "CURRNET" : ""))
					
				var hotspot:GameTaskHotspot = GameTaskHotspot(task.hotspotsToFind[_hotspotFindIndex]);
				Model.hotspot = Model.getHotspot(hotspot.id);	
			}
		}*/
		
		public function executeNextTask():void
		{
			trace("executeNextTask()")
			if(Model.state.uiState != State.UI_STATE_PRESENTATION_MODE)
				return;
			
			executeNextTaskPrivate();
		}
		
		private function executeNextTaskPrivate():void
		{
			var task:GameTaskData = Model.currentTask;
			if(!task)
				return;
			
			if(task.code == Constants.GAME_FIND_HOTSPOTS)
			{
				_hotspotFindIndex ++;
				showCurrentHotspot()
			}
			else if(task.code == Constants.GAME_PANO_DRAGGED)
			{
				Model.registerGameEvent(Constants.GAME_PANO_DRAGGED);
			}
		}
		
		public function showCurrentHotspot():void
		{
			var task:GameTaskData = Model.currentTask;
			if(!task)
				return;
			
			var hotspot:GameTaskHotspot = GameTaskHotspot(task.hotspotsToFind[_hotspotFindIndex]);
			if(!hotspot)
				return;
			
			if(Model.currentScene.id != hotspot.scene.id)
				Model.state.loadingScene = hotspot.scene;
				
			else
				Model.hotspot = Model.getHotspot(hotspot.id);	
		}
	}
}