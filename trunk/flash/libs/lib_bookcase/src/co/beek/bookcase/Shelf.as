package co.beek.bookcase
{
	import flash.display.Sprite;
	
	import co.beek.utils.BitmapUtil;

	public class Shelf extends ShelfFl
	{
		private var bgScaled:Sprite = new Sprite();
		
		public function Shelf()
		{
			super();
			addChildAt(bgScaled, 0);
			bg.visible = false;
			BitmapUtil.drawAndScale9Grid(bg, bgScaled);
		}
		
		public override function set width(value:Number):void
		{
			bgScaled.width = value;
		}
	}
}