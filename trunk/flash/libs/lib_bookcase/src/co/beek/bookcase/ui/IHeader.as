package co.beek.bookcase.ui
{
	import flash.display.Sprite;
	import flash.text.TextField;

	public interface IHeader
	{
		function get closeButton():Sprite;
		function get categoriesButton():Sprite;
		
		function get titleField():TextField;
	}
}