package co.beek.bookcase.ui
{
	import flash.display.Sprite;

	public interface ICategoriesPanel
	{
		function get renderersLocator():Sprite;
		function get bg():Sprite;
	}
}