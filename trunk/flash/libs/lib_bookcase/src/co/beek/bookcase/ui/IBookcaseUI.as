package co.beek.bookcase.ui
{
	public interface IBookcaseUI
	{
		function get selectedPanel():ISelectedPanel;
		
		function get header():IHeader;

		function get shelf():IShelf;

		function get categoriesPanel():ICategoriesPanel;

		function get categoryRenderer():ICategoriesRenderer;
	}
}