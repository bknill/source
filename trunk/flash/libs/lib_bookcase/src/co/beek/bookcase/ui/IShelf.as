package co.beek.bookcase.ui
{
	import flash.display.Sprite;

	public interface IShelf
	{
		function get guidesLocator():Sprite;
	}
}