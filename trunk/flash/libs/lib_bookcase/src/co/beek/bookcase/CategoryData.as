package co.beek.bookcase
{
	public class CategoryData
	{
		public static const CATEGORY_ID_DOWNLOADED:String = "CATEGORY_ID_DOWNLOADED";
		
		public static const CATEGORY_ID_ALL:String = "CATEGORY_ID_ALL";
		
		public static const CATEGORY_ID_OTHERS:String = "CATEGORY_ID_OTHERS";
		
		private var _id:String;
		
		private var _label:String;
		
		private var _guideCount:int;
		
		public function CategoryData(id:String, label:String, guideCount:int)
		{
			this._id = id;
			this._label = label;
			this._guideCount = guideCount;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function get guideCount():int
		{
			return _guideCount;
		}
	}
}