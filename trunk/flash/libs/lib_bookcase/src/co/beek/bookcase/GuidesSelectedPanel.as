package co.beek.bookcase
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.event.GuideBasicEvent;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.utils.StringUtil;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;

	public class GuidesSelectedPanel extends EventDispatcher
	{
		public static const TWEEN_COMLETE:String = "TWEEN_COMLETE";
		public static const TWEEN_CLOSE_START:String = "TWEEN_CLOSE_START";
		public static const CLOSE:String = "CLOSE";
		public static const LOAD:String = "LOAD";
		
		private var ui:GuidesSelectedPanelFl;
		
		private var bm:Bitmap = new Bitmap;
		
		private var _guideRenderer:GuideRenderer;

		private var guideRendererParent:DisplayObjectContainer;

		private var _isDownloaded:Boolean;
		
		public function GuidesSelectedPanel(ui:GuidesSelectedPanelFl)
		{
			super();
			this.ui = ui;
			
			ui.addChild(bm);
			
			Fonts.format(ui.titleField, Fonts.boldFont);
			Fonts.format(ui.descriptionField, Fonts.bodyFont);
			
			ui.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			//ui.closeButtonIphone.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			ui.loadButton.addEventListener(MouseEvent.CLICK, onLoadButtonClick);
			//ui.loadButtonIphone.addEventListener(MouseEvent.CLICK, onLoadButtonClick);
			//ui.downloadButton.addEventListener(MouseEvent.CLICK, onDownloadButtonClick);
			
			ui.bitmapLocator.visible = false;
			otherUIVisible = false;
		}
		
		private function imageLoadCallbackError():void
		{
			trace("imageLoadCallbackError");
		}
		
		public function get width():Number
		{
			return ui.bg.width;
		}
		
		public function set visible(value:Boolean):void
		{
			ui.visible = value;
			
			// WTF????
			//if(guideRenderer)
			//	guideRenderer.visible = value;
		}

		public function set isDownloaded(value:Boolean):void
		{
			_isDownloaded = value;
		}
		
		public function set x(value:Number):void
		{
			ui.x = Math.round(value);
		}

		public function set y(value:Number):void
		{
			ui.y = Math.round(value);
		}

		public function get height():Number
		{
			return ui.bg.height;
		}
		
		public function set guideRenderer(value:GuideRenderer):void
		{
			_guideRenderer = value;
			
			if(_guideRenderer)
				tweenFromRenderer()
		}
		
		public function get guideRenderer():GuideRenderer
		{
			return _guideRenderer;
		}
		
		private function tweenFromRenderer():void
		{
			_guideRenderer.visible = false;
			
			if(_guideRenderer.rawBitmapData)
			{
				
				var matrix:Matrix = new Matrix();
				matrix.scale(ui.bitmapLocator.width/_guideRenderer.rawBitmapData.width, 
					ui.bitmapLocator.height/_guideRenderer.rawBitmapData.height);
				
				var bmd:BitmapData = new BitmapData(
					ui.bitmapLocator.width, 
					ui.bitmapLocator.height, 
					true, 0x00000000);
				bmd.draw(_guideRenderer.rawBitmapData, matrix, null, null, null, true);
				bm.bitmapData = bmd;
			}
			
			var pos:Point = ui.globalToLocal(_guideRenderer.bitmap.localToGlobal(new Point()));
			bm.x = pos.x;
			bm.y = pos.y;
			bm.width = _guideRenderer.bitmap.width;
			bm.height = _guideRenderer.bitmap.height;
			
			ui.titleField.text = _guideRenderer.guide.title;
			ui.descriptionField.text = _guideRenderer.guide.description;
			//ui.sizeField.text = StringUtil.bytesToMbString(_guideRenderer.guide.zipBytes);
			
			Tweener.addTween(bm, {
				time: 0.5, 
				x: ui.bitmapLocator.x, 
				y: ui.bitmapLocator.y, 
				width: ui.bitmapLocator.width,
				height: ui.bitmapLocator.height,
				transition: Equations.easeInOutQuad,
				onComplete: onTweenComplete
			});
			
			//guideRendererParent = _guideRenderer.parent;
			//guideRendererParent.removeChild(_guideRenderer);
		}
		
		
		private function onTweenComplete():void
		{
			otherUIVisible = true;
			
			trace(ui.parent.localToGlobal(new Point()));
			//ui.loadButton.visible = _isDownloaded;
			//ui.downloadButton.visible = !_isDownloaded;
			
			dispatchEvent(new Event(TWEEN_COMLETE));
		}
		
		private function set otherUIVisible(value:Boolean):void
		{
			ui.bg.visible = value;
			ui.titleField.visible = value;
			ui.descriptionField.visible = value;
			ui.closeButton.visible = value;
			//ui.closeButtonIphone.visible = value;
			ui.loadButton.visible = value;
			//ui.loadButtonIphone.visible = value;
			//ui.downloadButton.visible = value;
			//ui.sizeField.visible = value;
		}
		
		private function onLoadButtonClick(event:MouseEvent):void
		{
			closeAndDispatchEvent(LOAD);
		}
		
		private var eventType:String;
		private function onCloseButtonClick(event:MouseEvent):void
		{
			closeAndDispatchEvent(CLOSE);
		}
		
		private function closeAndDispatchEvent(type:String):void
		{
			eventType = type;
			otherUIVisible = false;
			
			var pos:Point = ui.globalToLocal(_guideRenderer.bitmap.localToGlobal(new Point()));
			
			Tweener.addTween(bm, {
				time:0.5, 
				x: pos.x, 
				width: _guideRenderer.bitmap.width, 
				height: _guideRenderer.bitmap.height, 
				transition:Equations.easeInOutQuad,
				onComplete:onTweenBackComplete
			});
			
			// Tells the blocker graphics to clear
			dispatchEvent(new Event(TWEEN_CLOSE_START));
		}
			
		private function onTweenBackComplete():void
		{
			var g:GuideBasicData = _guideRenderer.guide;
			_guideRenderer.visible = true;
			_guideRenderer = null;
			
			bm.bitmapData = null;
			
			dispatchEvent(new GuideBasicEvent(eventType, g));
		}
		
		
		
		public function close():void
		{
			closeAndDispatchEvent(CLOSE);
		}
	}
}