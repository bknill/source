package co.beek.bookcase.ui
{
	import flash.text.TextField;

	public interface ICategoriesRenderer
	{
		function get titleField():TextField;
	}
}