package co.beek.bookcase
{
	import co.beek.Fonts;

	public class CategoryRenderer extends CategoriesRendererFl
	{
		private var _category:CategoryData;
		
		public function CategoryRenderer(category:CategoryData)
		{
			super();
			this._category = category;
			
			Fonts.format(titleField, Fonts.titleFont);
			titleField.text = category.label;
		}
		
		
		public function get category():CategoryData
		{
			return _category;
		}
		
		public function get textWidth():Number
		{
			return titleField.textWidth;
		}
		
		public override function get height():Number
		{
			return bg.height;
		}
		
		public function set overallWidth(value:Number):void
		{
			
			titleField.width = value;
			bg.width = value;
		}
	}
}