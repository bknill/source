package co.beek.bookcase.ui
{
	import flash.display.Sprite;
	import flash.text.TextField;

	public interface ISelectedPanel
	{
		function get closeButton():Sprite;
		function get loadButton():Sprite;
		function get guideLocator():Sprite;
		
		function get titleField():TextField;
		function get bodyField():TextField;
	}
}