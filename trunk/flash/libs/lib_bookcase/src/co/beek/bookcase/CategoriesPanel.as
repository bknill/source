package co.beek.bookcase
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	public class CategoriesPanel extends EventDispatcher
	{
		public static const SELECTED:String = "SELECTED";
		
		private var ui:GuideCategoriesPanelFl
		
		private var _categories:Vector.<CategoryData>;
		
		private var _category:CategoryData;
		
		public function CategoriesPanel(ui:GuideCategoriesPanelFl)
		{
			super();
			this.ui = ui;
		}
		
		public function set visible(value:Boolean):void
		{
			ui.visible = value;
		}
		
		public function set categories(value:Vector.<CategoryData>):void
		{
			_categories = value;
			
			removeRenderers();
			populatePanel();
		}
		
		public function get width():Number
		{
			return ui.bg.width;
		}
		
		private function removeRenderers():void
		{
			while(ui.renderersLocator.numChildren > 0)
			{
				var renderer:CategoryRenderer = CategoryRenderer(ui.renderersLocator.getChildAt(0));
				renderer.removeEventListener(MouseEvent.CLICK, onRendererClick);
				ui.renderersLocator.removeChild(renderer);
			}
		}
		
		private function populatePanel():void
		{
			var maxTextWidth:Number = 0;
			for(var i:int=0; i<_categories.length; i++)
			{
				var renderer:CategoryRenderer = new CategoryRenderer(_categories[i]);
				renderer.y = (renderer.height+4) * i;
				renderer.mouseChildren = false;
				renderer.addEventListener(MouseEvent.CLICK, onRendererClick);
				ui.renderersLocator.addChild(renderer);
				
				maxTextWidth = Math.max(maxTextWidth, renderer.textWidth);
			}
			
			for(i=0; i<_categories.length; i++)
				CategoryRenderer(ui.renderersLocator.getChildAt(i)).overallWidth = maxTextWidth+30;
			
			ui.bg.width = ui.renderersLocator.x*2 + ui.renderersLocator.width;
			ui.bg.height = ui.renderersLocator.y*2 + ui.renderersLocator.height - 15;// for the arrow
		}
		
		public function get categories():Vector.<CategoryData>
		{
			return _categories;
		}
		
		public function get category():CategoryData
		{
			return _category;
		}
		
		private function onRendererClick(event:MouseEvent):void
		{
			var renderer:CategoryRenderer = CategoryRenderer(event.target);
			_category = renderer.category;
			dispatchEvent(new Event(SELECTED));
		}
	}
}