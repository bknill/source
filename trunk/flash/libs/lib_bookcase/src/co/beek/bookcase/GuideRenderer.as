package co.beek.bookcase
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideBasicData;
	
	public class GuideRenderer extends BookshelfRendererFl
	{
		private var bm:Bitmap = new Bitmap;
		
		private var _rawBitmapData:BitmapData;
		
		private var _guide:GuideBasicData;
		
		public function GuideRenderer(guide:GuideBasicData)
		{
			super();
			trace("GuideRenderer()")
			this._guide = guide;
			
			bm.x = bitmapLocator.x;
			bm.y = bitmapLocator.y;
			addChild(bm);
			
			bitmapLocator.visible = false;
			
			mouseChildren = false;
			
			
			ImageLoader.load(
				Model.config.getAssetUrl(guide.thumbName), 
				imageLoadCallback,
				imageLoadCallbackError
			);
			
			visible = false;
		}
		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
		
		private function imageLoadCallbackError():void
		{
			trace("GuideRender.imageLoadCallbackError");
			visible = true;
		}
		
		private function imageLoadCallback(data:BitmapData):void
		{
			_rawBitmapData = data;
			
			var matrix:Matrix = new Matrix(bitmapLocator.width/data.width, 0, 0, bitmapLocator.height/data.height);
			var bmd:BitmapData = new BitmapData(bitmapLocator.width, bitmapLocator.height, true, 0x00000000);
			bmd.draw(data, matrix, null, null, null, true);
			bm.bitmapData = bmd;
			
			visible = true;
		}
		
		public function get rawBitmapData():BitmapData
		{
			return _rawBitmapData;
		}
		
		public function get bitmap():Bitmap
		{
			return bm;
		}
	}
}