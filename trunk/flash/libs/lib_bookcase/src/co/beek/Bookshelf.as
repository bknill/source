package co.beek
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import co.beek.bookcase.CategoriesPanel;
	import co.beek.bookcase.CategoryData;
	import co.beek.bookcase.GuideRenderer;
	import co.beek.bookcase.GuidesSelectedPanel;
	import co.beek.bookcase.Shelf;
	import co.beek.event.GuideBasicEvent;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.DestinationData;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.service.GuideService;
	import co.beek.service.data.DestinationList;
	import co.beek.service.data.GuideBasicList;
	import co.beek.utils.ColorUtil;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Rectangle;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	public class Bookshelf extends Sprite implements IBookcase
	{
		public static const OPENING:String = "OPENING";
		
		public static const CLOSING:String = "CLOSING";
		
		public static const LOAD:String = "LOAD";
		
		public static const SEARCH_DEFAULT:String = "search guides";
		
		private var _allGuides:Vector.<GuideBasicData>
		
		private var _offlineGuides:Vector.<GuideBasicData>;
		
		private var filteredGuides:Vector.<GuideBasicData>;

		private var shelves:Vector.<Shelf> = new Vector.<Shelf>;
		
		private var ui:BookshelfFl = new BookshelfFl;
		
		private var blocker:Sprite = new Sprite;
		
		private var categoriesPanel:CategoriesPanel;
		
		private var selectedGuidePanel:GuidesSelectedPanel;
		
		// I had to shoot a hostage
		private var hostage:BookshelfRendererFl = new BookshelfRendererFl();
		private var hostageShelf:ShelfFl = new ShelfFl;
		
		private var _width:Number;
		private var _height:Number;
		
		private var rows:int;

		private var cols:int;
		
		private var _open:Boolean;
		
		public function Bookshelf()
		{
			super();
			addChild(ui);
			
			/*blocker.addEventListener(MouseEvent.CLICK, onBlockerClick);
			
			ui.categoriesButton.visible = false;
			ui.addChildAt(blocker, ui.getChildIndex(ui.categoriesPanel)-1);*/
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			trace("Bookshelf.onAddedToStage()")
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
		/*	categoriesPanel = new CategoriesPanel(ui.categoriesPanel);
			categoriesPanel.visible = false;
			
			selectedGuidePanel = new GuidesSelectedPanel(ui.guideSelectedPanel);
			selectedGuidePanel.visible = false;
			selectedGuidePanel.addEventListener(GuidesSelectedPanel.TWEEN_COMLETE, onSelectedPanelTweenComplete);
			selectedGuidePanel.addEventListener(GuidesSelectedPanel.TWEEN_CLOSE_START, onSelectedPanelTweenClose);
			selectedGuidePanel.addEventListener(GuidesSelectedPanel.CLOSE, onSelectedPanelClose);
			selectedGuidePanel.addEventListener(GuidesSelectedPanel.LOAD, onSelectedPanelLoad);
			
			ui.searchField.visible = false;
			ui.searchField.text = SEARCH_DEFAULT;
			ui.searchField.addEventListener(FocusEvent.FOCUS_IN, onSearchFieldFocusIn);
			ui.searchField.addEventListener(Event.CHANGE, onSearchTextChange);
			
			ui.cancelSearchButton.visible = false;
			ui.cancelSearchButton.mouseChildren = false;
			ui.cancelSearchButton.addEventListener(MouseEvent.CLICK, onCancelSearchButtonClick);
			
			ui.closeButton.visible = false;
			ui.closeButton.mouseChildren = false;
			ui.closeButton.addEventListener(MouseEvent.CLICK, onCloseClick);
			
			Fonts.format(ui.categoriesButton.textField, Fonts.titleFont);
			ui.categoriesButton.mouseChildren = false;
			ui.categoriesButton.addEventListener(MouseEvent.CLICK, onCategoriesButtonClick);
			
			categoriesPanel.addEventListener(CategoriesPanel.SELECTED, onCategorySelected);
			
			Model.state.addEventListener(State.NETWORK_CHANGE, onNetworkChange);*/
		}
		
		public function get allGuides():Vector.<GuideBasicData>
		{
			return _allGuides;
		}
		
		private function onNetworkChange(event:Event):void
		{
			ui.categoriesButton.visible = Model.state.networkAccess && _allGuides != null;
			ui.searchField.visible = Model.state.networkAccess;
			if(!_allGuides)
				loadGuides();
		}
		

		private function onCategoriesButtonClick(event:MouseEvent):void
		{
			categoriesPanel.visible = true;
			
			blocker.graphics.beginFill(0, 0.5);
			blocker.graphics.drawRect(-ui.x, -ui.y, _width, _height);
			blocker.graphics.endFill();
			
		}
			
		private function onCategorySelected(event:Event):void
		{
			blocker.graphics.clear();
			categoriesPanel.visible = false;
			category = categoriesPanel.category;
		}
		
		private var _category:CategoryData;
		
		private function set category(value:CategoryData):void
		{
			trace("Bookshelf.set category()");
			if(_category == value)
				return;
			
			_category = value;
			
			if(_category.id == CategoryData.CATEGORY_ID_DOWNLOADED)
				filteredGuides = _offlineGuides;
			
			else if(_category.id == CategoryData.CATEGORY_ID_ALL)
				filteredGuides = _allGuides;
			
			else if(_category.id == CategoryData.CATEGORY_ID_OTHERS)
				filteredGuides = getOtherGuides(_category.id);
			
			else
				filteredGuides = filterGuides(_category.id);
				
			ui.categoriesButton.textField.text = _category.label; 
			
			populateShelves();
		}
		
		private function filterGuides(destinationId:String):Vector.<GuideBasicData>
		{
			var filtered:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
			
			for(var i:int=0; i<_allGuides.length; i++)
				if(_allGuides[i].destinationId == destinationId)
					filtered.push(_allGuides[i]);
			
			return filtered;
		}
		
		private function getOtherGuides(destinationId:String):Vector.<GuideBasicData>
		{
			var categoryIds:Vector.<String> = new Vector.<String>;
			for(var i:int=0; i<categoriesPanel.categories.length; i++)
				categoryIds.push(categoriesPanel.categories[i].id);
			
			var other:Vector.<GuideBasicData> = new Vector.<GuideBasicData>;
			for(i=0; i<_allGuides.length; i++)
				if(categoryIds.indexOf(_allGuides[i].destinationId) == -1)
					other.push(_allGuides[i]);
			
			return other;
		}
		
		private function onBlockerClick(event:MouseEvent):void
		{
			blocker.graphics.clear();
			categoriesPanel.visible = false;
			closeSelectedPanel();
		}
		
		public function mobileAppScale(appScale:Number):void
		{
			ui.paginationLocator.scaleX = appScale;
			ui.paginationLocator.scaleY = appScale;
			ui.searchField.scaleX = appScale;
			ui.searchField.scaleY = appScale;
			ui.categoriesButton.scaleX = appScale;
			ui.categoriesButton.scaleY = appScale;
			ui.categoriesPanel.scaleX = appScale;
			ui.categoriesPanel.scaleY = appScale;
			ui.guideSelectedPanel.scaleX = appScale;
			ui.guideSelectedPanel.scaleY = appScale;
			ui.guideSelectedPanel.x = (ui.width - (ui.guideSelectedPanel.width))/2; 
			ui.guideSelectedPanel.y = (ui.height - (ui.guideSelectedPanel.height * appScale))/2;
			ui.closeButton.scaleX = appScale;
			ui.closeButton.scaleX = appScale;
			
		}
		
		
		public function resize(width:Number, height:Number):void
		{
			trace("Bookshelf.resize(width:"+width+", height:"+height+")");
			_width = width;
			_height = height;
			
			var footherHeight:Number = 40;
			cols = Math.floor((_width - hostageShelf.locatorMc.x*2)/hostage.width);
			rows = Math.floor((_height - ui.headerBg.height -footherHeight)/hostageShelf.height);
			
			// Center the ui vertically
			ui.y = (height-(rows * hostageShelf.height + ui.headerBg.height + ui.footerBg.height))/2;
			
			removeShelves();
			buildShelves();
			arrangeControls();
			
			selectedGuidePanel.x = (ui.width - selectedGuidePanel.width)/2;
			selectedGuidePanel.y = (ui.height - selectedGuidePanel.height)/2;
			
			if(_open)
				drawBackground();
			else
				ui.x = _width;
			
			if(filteredGuides)
				populateShelves();
		}
		
		private function removeShelves():void
		{
			trace("Bookshelf.removeShelves();")
			for(var i:int=0; i<shelves.length; i++)
				ui.shelvesLocator.removeChild(shelves[i]);
			
			shelves = new Vector.<Shelf>;
		}
		
		private function buildShelves():void
		{
			trace("Bookshelf.buildShelves();")
			for(var i:int=0; i<rows; i++)
			{
				var shelf:Shelf = new Shelf;
				shelf.x = 0;
				shelf.y = i * shelf.height;
				//shelf.width = cols * hostage.width + shelf.locatorMc.x*2;//gap each end
				shelf.width = stage.fullScreenWidth;
				ui.shelvesLocator.addChild(shelf);
				shelves[i] = shelf;
			}
		}
		
		private function arrangeControls():void
		{
			ui.headerBg.width = ui.footerBg.width = ui.shelvesLocator.width;
			ui.headerBg.height = (ui.height - selectedGuidePanel.height)/2;
			ui.searchField.x =  ui.headerBg.width - ui.searchField.width*2;
			ui.searchField.y =  ui.searchField.height * 0.25;
			ui.cancelSearchButton.x = ui.searchField.x + ui.searchField.width - ui.cancelSearchButton.width - 1.5;
			ui.closeButton.x = ui.headerBg.width - ui.closeButton.width - 10;
			
			ui.categoriesButton.y = ui.searchField.y;
			ui.categoriesButton.height = ui.searchField.height;
			ui.categoriesPanel.y = ui.searchField.y + ui.categoriesButton.height;
			
			ui.guideSelectedPanel.x = (ui.width - ui.guideSelectedPanel.width)/2; 
			ui.guideSelectedPanel.y = (ui.height - ui.guideSelectedPanel.height)/2;
			
			ui.footerBg.y = ui.shelvesLocator.y + ui.shelvesLocator.height + 20;
			ui.footerBg.height = (ui.height - selectedGuidePanel.height)/2 - 20;
			ui.paginationLocator.y = ui.footerBg.y + ui.footerBg.height/2;
			ui.paginationLocator.x = ui.footerBg.x + (ui.footerBg.width - ui.paginationLocator.width)/2;
			
			var background:Sprite = new Sprite();
			background.graphics.beginFill(0xFFFFFF);
			background.graphics.drawRect(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			background.graphics.endFill();
			background.y = -((ui.height - selectedGuidePanel.height)/2)/2;

			ui.addChild(background);
			ui.setChildIndex(background, 0)
		}
		
		private var allGuidesLoading:Boolean;
		private function loadGuides():void
		{
			trace("Bookshelf.loadGuides();")
			if(allGuidesLoading)
				return;
			
			allGuidesLoading = true;
			GuideService.getLibraryGuides(onAllGuidesLoaded)
		}
		
		private function onAllGuidesLoaded(list:GuideBasicList):void
		{
			trace("Bookshelf.onAllGuidesLoaded();")
			allGuidesLoading = false;
			// copy in the local guides if any
			if(_offlineGuides)
			{
				for(var i:int=0; i<_offlineGuides.length; i++)
				{
					var listIndex:int = getGuideIndex(list.guides, _offlineGuides[i]);
					if(listIndex == -1)
						list.guides.push(_offlineGuides[i]);
				}
			}
			
			filteredGuides = _allGuides = list.guides;
			ui.categoriesButton.visible = true;
			
			loadDestinations();
		}
		
		private function getGuideIndex(guides:Vector.<GuideBasicData>, guide:GuideBasicData):int
		{
			for(var i:int=0; i<guides.length; i++)
				if(guides[i].id == guide.id)
					return i;
			
			return -1;
		}
		
		public function loadDestinations():void
		{
			trace("Bookshelf.loadDestinations();")
			GuideService.getDestinations(onDestinations);
		}
		
		private function onDestinations(data:DestinationList):void
		{
			trace("Bookshelf.onDestinations();")
			var categories:Vector.<CategoryData> = categoriesPanel.categories;
			if(!categories)
				categories = new Vector.<CategoryData>;
			
		categories.push(new CategoryData(CategoryData.CATEGORY_ID_ALL, "All", 100000));
			for(var i:int=0; i<data.destinations.length; i++)
			{
				var destination:DestinationData = data.destinations[i];
				var guideCount:int = getGuideCount(destination);
				if(guideCount > 1)
					categories.push(new CategoryData(destination.id, destination.title, guideCount));
			}
			categories.push(new CategoryData(CategoryData.CATEGORY_ID_OTHERS, "Other", 0));
			
			categories = categories.sort(orderCategories);
			categoriesPanel.categories = categories;
			
			// make the button the same width of hte panel
			ui.categoriesButton.bg.width = categoriesPanel.width;
			ui.categoriesButton.arrow.x = categoriesPanel.width - 20;
			
			
			//category = categories[0];
		}
		
		private function getGuideCount(destination:DestinationData):int
		{
			var guideCount:int = 0
			for(var i:int=0; i<_allGuides.length; i++)
				if(_allGuides[i].destinationId == destination.id)
					guideCount ++;
			
			return guideCount;
		}
		
		private function orderCategories(l1:CategoryData, l2:CategoryData): int
		{
			return int(l2.guideCount) - int(l1.guideCount);
		}
		
		private function populateShelves():void
		{
			trace("Bookshelf.populateShelves()");
			
			if(isNaN(_width) || isNaN(_height))
				return;
			
			rebuildPageButtons();
			page = 0;
		}
		
		private function rebuildPageButtons():void
		{
			trace("Bookshelf.rebuildPageButtons()");
			while(ui.paginationLocator.numChildren > 0)
			{
				var prevBtn:BookcasePageButtonFl = BookcasePageButtonFl(ui.paginationLocator.getChildAt(0));
				prevBtn.removeEventListener(MouseEvent.CLICK, onPageButtonClick);
				ui.paginationLocator.removeChild(prevBtn);
			}
			
			var pages:int = Math.ceil(filteredGuides.length/(cols * rows));
			if(pages > 1)
			{
				for(var i:int=0; i<pages && i<10; i++)
				{
					var btn:BookcasePageButtonFl = new BookcasePageButtonFl;
					btn.x = (btn.width + 10) * i;
					btn.mouseChildren = false;
					btn.addEventListener(MouseEvent.CLICK, onPageButtonClick);
					ui.paginationLocator.addChild(btn);
				}
			}
		}
		
		private function onPageButtonClick(event:MouseEvent):void
		{
			page = ui.paginationLocator.getChildIndex(Sprite(event.target));
		}
		
		private function showSelectedButton(page:int):void
		{
			for(var i:int=0; i<ui.paginationLocator.numChildren; i++)
				BookcasePageButtonFl(ui.paginationLocator.getChildAt(i)).transform.colorTransform = new ColorTransform;
			
			if(ui.paginationLocator.numChildren > page)
				ColorUtil.colorize(ui.paginationLocator.getChildAt(page), 0);
		}
		
		private var _page:int = -1;
		
		private function set page(value:int):void
		{
			trace("Bookshelf.set page()");
			_page = value;
			
			var pageSize:int = rows * cols;
			var start:int = pageSize * _page;
			var end:int = pageSize * (_page+1);
			var guides:Vector.<GuideBasicData> = filteredGuides.slice(start, end);
			
			for(var i:int=0; i<rows; i++)
				populateShelf(guides, i, cols);
			
			showSelectedButton(_page);
			

		}
		
		private function populateShelf(guides:Vector.<GuideBasicData>, row:int, cols:int):void
		{
			trace("Bookshelf.populateShelf(row:"+row+", cols:"+row+")");
			var shelf:Shelf = shelves[row];
			while(shelf.locatorMc.numChildren > 0)
			{
				trace("removing GuideRenderer: "+shelf.locatorMc.numChildren);
				
				var prevRenderer:GuideRenderer = GuideRenderer(shelf.locatorMc.getChildAt(0));
				prevRenderer.removeEventListener(MouseEvent.CLICK, onRendererMouseClick);
				shelf.locatorMc.removeChild(prevRenderer);
				
			}
			
			var start:int = row*cols;
			var max:int = Math.min(cols, guides.length - start);
			for(var i:int = 0; i<max; i++)
			{
				trace("new GuideRenderer(guides["+(start + i)+"])");
				var renderer:GuideRenderer = new GuideRenderer(guides[start + i]);
				renderer.x = renderer.width * i;
				renderer.y = -renderer.height;
				renderer.addEventListener(MouseEvent.CLICK, onRendererMouseClick);
				shelf.locatorMc.addChild(renderer);
			}
		}
		
		private function onRendererMouseClick(event:MouseEvent):void
		{
			var renderer:GuideRenderer = GuideRenderer(event.target);
			if(selectedGuidePanel.guideRenderer == null)
			{
				Model.dispatchTrackingEvent(
					Constants.GA_CATEGORY_BOOKCASE,
					"preview", 
					renderer.guide.id
				);
				
				selectedGuidePanel.guideRenderer = renderer;
				selectedGuidePanel.visible = true;
			}
		}
		
		public function tweenToOpen():void
		{
			trace("Bookshelf.tweenToOpen()");
			_open = true;
			
			ui.closeButton.visible = Model.guideData != null;
			ui.categoriesButton.visible = Model.state.networkAccess && _allGuides != null;
			ui.searchField.visible = Model.state.networkAccess;
			
			//ui.cacheAsBitmap = true;
			Tweener.addTween(ui, {
				time:0.5, 
				x: Math.round((_width - ui.headerBg.width)/2), 
				transition:Equations.easeInOutQuad,
				onComplete:tweenToOpenComplete
			});
			
			if(!_allGuides && Model.state.networkAccess)
				loadGuides();
			
			dispatchEvent(new Event(OPENING));
		}
		
		private function tweenToOpenComplete():void
		{
			trace("Bookshelf.tweenToOpenComplete()");
			
			drawBackground();
			
			//if(allGuides && _page < 0)
			//	populateShelves();
		}
		
		private function drawBackground():void
		{
			graphics.clear();
			graphics.beginFill(0, 0.75);
			graphics.drawRect(0, 0, _width, _height);
			graphics.endFill();
		}
		
		private function onSelectedPanelLoad(event:GuideBasicEvent):void
		{
			_guide = event.guideBasic;	
			
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_BOOKCASE,
				"Load", 
				_guide.id
			);
			
			tweenToClosed();
			// wait for the bookshelf to close
			setTimeout(dispatchLoadEvent, 1000);
		}
		
		private function dispatchLoadEvent():void
		{
			dispatchEvent(new Event(LOAD));
			closeSelectedPanel();
		}
		
		private var _guide:GuideBasicData;
		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
		
		public function set guide(guide:GuideBasicData):void
		{
			_guide = guide;
		}
		
		public function set offlineGuides(value:Vector.<GuideBasicData>):void
		{
			trace("Bookshelf.set offlineGuides()");
			if(!value)
				return;
			
			filteredGuides = _offlineGuides = value;
			
			
		/*	var categories:Vector.<CategoryData> = categoriesPanel.categories;
			if(!categories)
				categories = new Vector.<CategoryData>;
			
			if(!hasCategory(categories, CategoryData.CATEGORY_ID_DOWNLOADED))
				categories.splice(0, 0, new CategoryData(CategoryData.CATEGORY_ID_DOWNLOADED, "Downloaded", 1000000));
			
			ui.categoriesButton.visible = true;
			categoriesPanel.categories = categories;
			
			category = categories[0];*/
		}
		
		public function get offlineGuides():Vector.<GuideBasicData>
		{
			return _offlineGuides;
		}
		
		public function hasCategory(categories:Vector.<CategoryData>, id:String):Boolean
		{
			for(var i:int=0; i<categories.length; i++)
				if(categories[i].id == id)
					return true;
			
			return false;
		}

		/*private function onSelectedPanelDownoad(event:Event):void
		{
			dispatchEvent(new Event(DOWNLOAD))
			closeSelectedPanel();
			tweenToClosed();
		}*/
		
		public function get open():Boolean
		{
			return _open;
		}
		
		public function tweenToClosed():void
		{
			_open = false;
			Tweener.addTween(ui, {
				time:1, 
				x:_width, 
				transition:Equations.easeInOutQuad,
				onComplete:tweenToClosedComplete
			});
			dispatchEvent(new Event(CLOSING));
		}
		
		private function tweenToClosedComplete():void
		{
			graphics.clear();
		}
		
		private function onSelectedPanelTweenComplete(event:Event):void
		{
			blocker.graphics.beginFill(0, 0.5);
			blocker.graphics.drawRect(-ui.x, -ui.y, _width, _height);
			blocker.graphics.endFill();
		}

		private function onSelectedPanelTweenClose(event:Event):void
		{
			blocker.graphics.clear();
		}
		
		private function onSelectedPanelClose(event:Event):void
		{
			closeSelectedPanel();
		}
		
		private function closeSelectedPanel():void
		{
			blocker.graphics.clear();
			
			if(selectedGuidePanel.guideRenderer)
				selectedGuidePanel.close();
		}

		private function onSearchFieldFocusIn(event:FocusEvent):void
		{
			if(ui.searchField.text == SEARCH_DEFAULT)
				ui.searchField.text = "";
		}
		
		private function onCancelSearchButtonClick(event:MouseEvent):void
		{
			trace("Bookshelf.onCancelSearchButtonClick()");
			ui.searchField.text = SEARCH_DEFAULT;
			ui.categoriesButton.visible = true;
			ui.cancelSearchButton.visible = false;
			filteredGuides = _allGuides;
			populateShelves();
		}
		
		private function onSearchTextChange(event:Event):void
		{
			if(ui.searchField.text.length < 2)
				return;
			

			onWaitingFor();

		}
		
		private function onWaitingFor():void
		{
			if(retrievingSearchResults)
			{
				// keep looping till search results are recieved.
				clearTimeout(retrievingSearchResultsTimeout);
				retrievingSearchResultsTimeout = setTimeout(onWaitingFor, 1000);
			}
			else
				searchNow();
		}
		
		private var retrievingSearchResultsTimeout:uint;
		private var retrievingSearchResults:Boolean;
		
		private function searchNow():void
		{
			GuideService.searchGuides(ui.searchField.text, onSearchResults);
			retrievingSearchResults = true;
		}
		
		private function onSearchResults(list:GuideBasicList):void
		{
			trace("Bookshelf.onSearchResults()");
			retrievingSearchResults = false;
			ui.cancelSearchButton.visible = true;
			ui.categoriesButton.visible = false;
			filteredGuides = list.guides;
			populateShelves();
		}
		
		private function onCloseClick(event:MouseEvent):void
		{
			if(Model.guideData != null)
				tweenToClosed();
		}
	}
}