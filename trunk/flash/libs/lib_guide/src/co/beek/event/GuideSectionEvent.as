package co.beek.event
{
	import co.beek.model.guide.GuideSectionData;
	
	import flash.events.Event;
	
	public class GuideSectionEvent extends Event
	{
		private var _guideSection:GuideSectionData;
		
		public function GuideSectionEvent(type:String, guideSection:GuideSectionData)
		{
			super(type);
			_guideSection = guideSection;
		}
		
		public function get guideSection():GuideSectionData
		{
			return _guideSection;
		}
	}
}