package co.beek.event
{
	import co.beek.model.guide.GuideSceneData;
	
	import flash.events.Event;
	
	public class GuideSceneEvent extends Event
	{
		private var _guideScene:GuideSceneData;
		
		public function GuideSceneEvent(type:String, guideScene:GuideSceneData)
		{
			super(type);
			_guideScene = guideScene;
		}
		
		public function get guideScene():GuideSceneData
		{
			return _guideScene;
		}
	}
}