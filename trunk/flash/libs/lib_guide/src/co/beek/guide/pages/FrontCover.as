package co.beek.guide.pages
{
	import co.beek.Fonts;
	import co.beek.book.RigidPage;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GuideData;
	import co.beek.utils.ColorUtil;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextFieldAutoSize;

	public class FrontCover extends RigidPage
	{
		private var guideData:GuideData;
		
		private var guideThumbHolder:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		
		private var sceneThumbHolder:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		
		private var _firstScene:SceneData;
		
		private var frontCover:FrontCoverFrontFl;
		
		//private var frontSideBitmap:Bitmap;
		
		public function FrontCover(guideData:GuideData)
		{
			super(new Sprite, new FrontCoverBackFl);
			
			frontSide.graphics.beginFill(0, 0);
			frontSide.graphics.drawRect(0,0, 20, 20);
			frontSide.graphics.endFill();
			
			
			this.frontCover = new FrontCoverFrontFl;
			
			var rect:Rectangle = frontCover.getRect(frontCover);
			guideThumbHolder.x = rect.x;
			guideThumbHolder.y = rect.y;
			this.frontSide.mouseChildren = false;
			this.frontSide.addChild(guideThumbHolder);
			
			// temp show this cover
			
			this.frontSide.addChild(frontCover);
			
			ColorUtil.colorize(backSide.bg, guideData.guideColor);
			
			
			this.guideData = guideData;
			this.guideData.addEventListener(GuideData.NEW_THUMB, onNewThumbChange);
			
			updateCoverToData();
			
			//mouseChildren = false;
			Fonts.format(frontCover.titleField, Fonts.titleFont);
			frontCover.titleField.autoSize = TextFieldAutoSize.LEFT;
			
			frontCover.addChildAt(sceneThumbHolder, frontCover.getChildIndex(frontCover.bg)+1);
			
			//updateFromGuideData();
			ImageLoader.load(
				Model.config.getAssetUrl(guideData.thumbName), 
				guideThumbLoadCallback,
				guideThumbLoadCallbackError
			);
		}
		
		private function guideThumbLoadCallbackError():void
		{
			trace("guideThumbLoadCallbackError");
		}
		
		private function guideThumbLoadCallback(data:BitmapData):void
		{
			guideThumbHolder.bitmapData = data;
			if(frontCover.parent)
				frontCover.parent.removeChild(frontCover);
		}
		
		private function onNewThumbChange(event:Event):void
		{
			if(guideData.newThumb)
				guideThumbHolder.bitmapData = guideData.newThumb;
		}
		
		
		public function get frontSide():Sprite
		{
			return _front as Sprite;
		}
		
		private function get backSide():FrontCoverBackFl
		{
			return _back as FrontCoverBackFl;
		}
		
		/*
		private function onSceneChange(event:Event):void
		{
			currentScene = Model.currentScene;
		}
		
		private function set currentScene(value:SceneData):void
		{
			if(guideData.firstScene && value
				&& guideData.firstScene.scene.id == value.id)
				firstScene = value;
		}
		
		private function set firstScene(value:SceneData):void
		{
			if(_firstScene)
				_firstScene.removeEventListener(SceneData.NEW_THUMB, onSceneNewThumb);
			_firstScene = value;
			_firstScene.addEventListener(SceneData.NEW_THUMB, onSceneNewThumb);
		}
		
		private function onSceneNewThumb(event:Event):void
		{
			if(_firstScene.newThumb)
				sceneThumb = _firstScene.newThumb;
		}
		
		private function onGuideDataChange(event:Event):void
		{
			updateFromGuideData();
		}
		
		private function updateFromGuideData():void
		{
			frontCover.titleField.text = guideData.title;
			var scale:Number = Math.min((frontCover.titleBg.width-50)/frontCover.titleField.textWidth, 2.5);
			frontCover.titleField.scaleX = scale;
			frontCover.titleField.scaleY = scale;
			frontCover.titleField.x = frontCover.titleBg.x + (frontCover.titleBg.width - frontCover.titleField.width)/2 + 5;
			frontCover.titleField.y = frontCover.titleBg.y + (frontCover.titleBg.height - frontCover.titleField.height)/2 + 5;
			frontCover.titleField.textColor = 0xFFFFFF;
			
			ColorUtil.colorize(frontCover.bg, guideData.guideColor);
			ColorUtil.colorize(frontCover.beekIcon, guideData.guideColor);
			ColorUtil.colorize(frontCover.titleBg, guideData.guideColor);
			ColorUtil.colorize(backSide.bg, guideData.guideColor);
			
			if(guideData.coverDesign)
				ImageLoader.load(
					Model.config.getAssetUrl(guideData.coverDesign), 
					sceneThumbLoadCallback,
					sceneThumbLoadCallbackError
				);
			else if(guideData.firstScene && guideData.firstScene.scene.thumbName)
				ImageLoader.load(
					Model.config.getAssetUrl(guideData.firstScene.scene.thumbName), 
					sceneThumbLoadCallback,
					sceneThumbLoadCallbackError
				);
		}
		private function sceneThumbLoadCallbackError():void
		{
			trace("sceneThumbLoadCallbackError");
		}
		
		private function sceneThumbLoadCallback(data:BitmapData):void
		{
			sceneThumb = data;
		}
		*/
		
		private function updateCoverToData():void
		{
			if(guideData.coverDesign)
			{
				frontCover.titleField.visible = false;
				frontCover.titleBg.visible = false;
				frontCover.beekIcon.visible = false;
				frontCover.sheen.visible = false;
			}
			else
			{
				
				frontCover.titleField.visible = true;
				frontCover.titleBg.visible = true;
				frontCover.beekIcon.visible = true;
				frontCover.sheen.visible = true;
				
				frontCover.titleField.text = guideData.title;
				var scale:Number = Math.min((frontCover.titleBg.width-50)/frontCover.titleField.textWidth, 2.5);
				frontCover.titleField.scaleX = scale;
				frontCover.titleField.scaleY = scale;
				frontCover.titleField.x = frontCover.titleBg.x + (frontCover.titleBg.width - frontCover.titleField.width)/2 + 5;
				frontCover.titleField.y = frontCover.titleBg.y + (frontCover.titleBg.height - frontCover.titleField.height)/2 + 5;
				frontCover.titleField.textColor = 0xFFFFFF;
			}
			
			ColorUtil.colorize(frontCover.bg, guideData.guideColor);
			ColorUtil.colorize(frontCover.beekIcon, guideData.guideColor);
			ColorUtil.colorize(frontCover.titleBg, guideData.guideColor);
			ColorUtil.colorize(backSide.bg, guideData.guideColor);
		}
		
		public function getCoverThumb(bgIamge:BitmapData):BitmapData
		{
			sceneThumbHolder.bitmapData = bgIamge;
			sceneThumbHolder.smoothing = true;
			sceneThumbHolder.x = frontCover.bg.x;
			sceneThumbHolder.y = frontCover.bg.y;
			sceneThumbHolder.width = frontCover.bg.width;
			sceneThumbHolder.height = frontCover.bg.height;
			sceneThumbHolder.mask = frontCover.bg;
			
			updateCoverToData();
			var rect:Rectangle = frontCover.getRect(frontCover);
			
			var bitmapData:BitmapData = new BitmapData(frontCover.width, frontCover.height, true, 0x00000000);
			bitmapData.draw(frontCover, new Matrix(1,0,0,1, -rect.x, -rect.y));
			return bitmapData;
		}
	}
}