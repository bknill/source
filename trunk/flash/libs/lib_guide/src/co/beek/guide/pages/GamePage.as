package co.beek.guide.pages
{
	import co.beek.Fonts;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.hotspots.HotspotData;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class GamePage extends GamePageFl
	{
		private var _task:GameTaskData;
		private var highlights:MovieClip = new MovieClip();
		
		public function GamePage(task:GameTaskData)
		{
			super();
			
			_task = task;
			_task.addEventListener(Event.CHANGE, onTaskChange);
			Model.addEventListener(Model.GAME_TASK_CHANGED, onTaskChange);
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideUpdate);
			feedbackUI.visible = false;
			descriptionField.addEventListener(MouseEvent.MOUSE_DOWN,onDescriptionMouseDown);
			titleField.selectable = false;
			
			descriptionField.multiline = true;
			descriptionField.wordWrap = true;
			descriptionField.autoSize = "left";
			descriptionField.scaleX = titleField.scaleX =  1;
			descriptionField.width =  titleField.width = bg.width;
			feedbackUI.feedbackField.multiline = true; 
			feedbackUI.feedbackField.wordWrap = true;
			feedbackUI.feedbackField.autoSize = "left";

			titleField.autoSize = "left";
		

			updateFields();
		}
		
		private function onDescriptionMouseDown(e:MouseEvent):void
		{
			e.stopPropagation();
		}
		
		private function onTaskChange(event:Event):void
		{
			if(task != null)
				updateFields();
			
			feedbackUI.visible = true;
			feedbackUI.hotspotsFound.visible = true;
			feedbackUI.feedbackField.visible = true;
		}
		
		private function onGuideUpdate(event:Event):void
		{

			if(task.id == Model.currentTask.id)
				_task = Model.guideData.gameTasks[Model.currentTaskIndex];

			
			updateFields();
		}
		
		public function updateFields():void
		{
			Fonts.format(titleField, Fonts.titleFont, _task.title);
			Fonts.formatHTML(descriptionField, Fonts.bodyFont, _task.instructions);

			descriptionField.height = descriptionField.textHeight;
			
			feedbackUI.y = descriptionField.y + descriptionField.height;
			feedbackUI.feedbackField.text = _task.feedback;
			
			feedbackUI.feedbackField.width = descriptionField.textWidth;
			feedbackUI.feedbackField.height = feedbackUI.feedbackField.textHeight;
			
			bg.width = descriptionField.x + descriptionField.textWidth + 10;
			bg.height = descriptionField.y + descriptionField.textHeight + 10;
			bg.alpha = 0.9;
			

			Fonts.format(feedbackUI.feedbackField, Fonts.bodyFont);
		}
		
		public function get task():GameTaskData
		{
			return _task
		}
		
		public function set task(value:GameTaskData):void
		{
			if(value == null)
				return;
			
			 _task = value;
			 feedbackUI.feedbackField.visible = false;
			 
			 while(hotspotsFound.renderersLocator.numChildren > 0)
				 hotspotsFound.renderersLocator.removeChildAt(0);
			 
			 
			 updateFields();
		}
		
		
		private function get hotspotsFound():HotspotsFoundFl
		{
			return feedbackUI.hotspotsFound;
		}
		
		public function set taskHotspotsFound(found:Vector.<HotspotData>):void
		{
			//hotspotsFound.renderersLocator.y += 20;

			while(hotspotsFound.renderersLocator.numChildren > 0)
				hotspotsFound.renderersLocator.removeChildAt(0);
			
			for(var i:int; i<found.length; i++)
			{
				var renderer:GameHotspotRenderer = new GameHotspotRenderer;
					Fonts.format(renderer.hotspotTitleField,Fonts.bodyFont, found[i].title);
					renderer.y = hotspotsFound.renderersLocator.height;
					renderer.width = renderer.hotspotTitleField.width = renderer.hotspotTitleField.textWidth + 10;
					hotspotsFound.renderersLocator.addChild(renderer);
			}
			

			//Fonts.format(hotspotsFound.youHaveFoundField, Fonts.bodyFont);
			
			//feedbackUI.feedbackField.visible = true;
			hotspotsFound.visible = true;
			bg.height = feedbackUI.y + feedbackUI.height + 10;
		}
		
		public function showFeedback():void
		{
			if(hotspotsFound.visible)
				feedbackUI.feedbackField.y 
					= feedbackUI.hotspotsFound.height + 10;
				
			else
				feedbackUI.feedbackField.y 
					= 0;
			
			feedbackUI.visible = true;
			feedbackUI.feedbackField.visible = true;
			
			bg.height = feedbackUI.y + feedbackUI.height + 10;
			
		}
		
/*		public override function dispose():void
		{
			_task.removeEventListener(Event.CHANGE, onTaskChange);
			_task = null;
			
			super.dispose();
		}*/
	}
}