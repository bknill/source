package co.beek.guide
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.IGuide;
	import co.beek.Log;
	import co.beek.guide.ui.GuideUI;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;

	
	
	public class Guide extends Sprite implements IGuide
	{
		public static const SCENE_LIKED:String ="SCENE_LIKED";
		public static const PLAY_BUTTON_CLICKED:String ="PLAY_BUTTON_CLICKED";
		public static const GUIDE_OPEN:String = "GUIDE_OPEN";
		public static const SCENE_INFO_OPEN:String = "SCENE_INFO_OPEN";
		public static const GUIDE_CLOSE:String = "GUIDE_CLOSE";
		public static const GAME_COMPLETE:String ="GAME_COMPLETE";
		public static const NEXT:String = "NEXT";
		public static const CLOSE_MENUS:String = "CLOSE_MENUS";
		public static const MOVE_TWEEN_TIME:Number = 1.5;
		public static const GUIDE_PAGE_COVER:int = 0;
		
		private var ui:GuideUI = new GuideUI;
		private var _guideData:GuideData;		
		private var _width:Number;
		private var _height:Number;
		public var appScale:Number = 1;


		public function Guide()
		{
			super();
			
		//	addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
	/*	private var circleMenuHolder:Sprite = new Sprite;
		private var activeCircleMenu:CircleMenu;
		private var circleMenus:Vector.<CircleMenu> = new Vector.<CircleMenu>;
		private var circleMenuItems:Vector.<CircularMenuItem> = new Vector.<CircularMenuItem>;
		private var selectedItem:CircularMenuItem;
		private var rootMenu:CircleMenu;
		private var circleMenuOffset:Number = 110;
		private var circleMenuScale:int = 2;
		private var circleMenuRadius:Number;
		private var circleMenuX:Number;
		private var circleMenuDragging:Boolean;
		private var circleMenuDraggingMouseY:int;
		
		
		private var sceneText:SceneText = new SceneText;
		private var game:GamePage;
		private var circleMenuBackground:Shape = new Shape();
		private var blur:BlurFilter = new BlurFilter(10,10,flash.filters.BitmapFilterQuality.MEDIUM);
		
		private var previewImage:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		private var previewLoading:String = null;
		
		

		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.GUIDE_UPDATE, nextGameTask);
			Model.addEventListener(Model.SECTION_CHANGE,onSectionChange);
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE ,onLoadingSceneChange);
			Model.addEventListener(Model.SCENE_CHANGE, onSceneDataChange);
			Model.addEventListener(Model.SCENE_CHANGE ,onSceneChange);
			Model.state.addEventListener(State.PANO_TILES_LOADED,onPanoTilesLoaded);
			Model.state.addEventListener(State.GAME_VIEW_CHANGE,onGameViewChange);
			Model.state.addEventListener(State.PANO_ENGAGED, onPanoDrag);
			Model.state.addEventListener(State.VR_MODE_CHANGE, onVRModeChange);
			
			guideData = Model.guideData;
			
			

		}
		
		private function onGuideChange(event:Event):void
		{
			Log.record("Guide.onGuideChange()");
			guideData = Model.guideData;
			
		}
		
		private function set guideData(value:GuideData):void
		{
			Log.record("Guide.set guideData()");
			
			if(value == _guideData || value == null)
				return;
			
			if(_guideData)
			{
				_guideData.removeEventListener(GuideData.GUIDE_SECTIONS_CHANGED, onGuideSectionsChange);
			}
			
			_guideData = value;
			_guideData.addEventListener(GuideData.GUIDE_SECTIONS_CHANGED, onGuideSectionsChange);

			addChild(previewImage);
			addChild(circleMenuHolder);
			addChild(sceneText);
	
			circleMenuRadius = stage.stageHeight/2;
			circleMenuScale = Model.appScale == 1 ? 2 : Model.appScale;
			circleMenuHolder.x = circleMenuX = - (circleMenuRadius + (circleMenuOffset / 1.5)) * circleMenuScale;
			circleMenuHolder.y = stage.stageHeight/2;
			circleMenuHolder.addEventListener(MouseEvent.MOUSE_DOWN,onCircleMenuHolderMouseDown);
			createCircleMenu();

			if(Model.isPlayingGame)
				createGame();
			
		}
		

		
		private function createCircleMenu(guideSection:GuideSectionData = null, multiplier:int = 1):void
		{
		
	
			
			var circleMenu:CircleMenu = new CircleMenu(circleMenuRadius + (circleMenuOffset * multiplier),
				2 * multiplier, 10, multiplier);
			circleMenuHolder.addChildAt( circleMenu, 0);
		
			
			
			circleMenu.alpha = 0;
			
			for each(var section:GuideSectionData in guideSection != null 
				? guideSection.sectionChildren : _guideData.topLevelGuideSections)
			{ 
				var item:CircularMenuItem = new CircularMenuItem(section, multiplier, circleMenu);
				
				Fonts.format(item.txt, Fonts.titleFont, section.title); 
				item.addEventListener( CircularMenuItem.BUTTON_DOWN,onCircleMenuItemMouseDown);
				item.addEventListener( CircularMenuItem.BUTTON_UP,onCircleMenuItemMouseUp);
				circleMenu.addChild( item );
	
			}
			
			if(guideSection != null)
				for each(var scene:GuideSceneData in guideSection.guideScenes)
				{ 
					var item:CircularMenuItem = new CircularMenuItem(scene, multiplier, circleMenu);
					Fonts.format(item.txt, Fonts.bodyFont, scene.title); 
					item.addEventListener( CircularMenuItem.BUTTON_DOWN, onCircleMenuItemMouseDown);
					item.addEventListener( CircularMenuItem.BUTTON_UP,onCircleMenuItemMouseUp);
					circleMenu.addChild( item );
					circleMenuItems.push(item);
				}
			
		
			circleMenu.currentIndex = circleMenu.numChildren/2;
			
			if(guideSection)
				circleMenu.guideSection = guideSection;
			else{
				rootMenu = circleMenu;
				selectedItem = circleMenu.getChildAt(0) as CircularMenuItem;
			}
			
			circleMenu.scaleX = circleMenu.scaleY = circleMenuScale;
			circleMenu.x = 0;
			circleMenu.y = 0;
			circleMenu.open = true;
			
			drawCircleMenuBackground(circleMenu);
			
			
			Tweener.addTween(circleMenu, {alpha : 1, time : 0.7, delay: 0.2 });
			
			
			
			circleMenus.push(circleMenu);
			
			
			
		}
		
		private function onCircleMenuItemMouseDown(event:Event):void
		{
			
			Model.state.guideView = State.GUIDE_VIEW_OPEN;
			
			 selectedItem = event.currentTarget as CircularMenuItem;
			 activeCircleMenu = selectedItem.circleMenu;
			 
			 if(activeCircleMenu.oversized){
				 
				 startCircleMenuDrag(); 
				 setTimeout(function():void
				 {
				 	if(!circleMenuDragging) clearCircleMenuItemDragging();
				 },200);
			 }
		}
		
		
		private function onCircleMenuHolderMouseDown(event:Event):void
		{
			
			if(activeCircleMenu)
				if(activeCircleMenu.oversized){
					startCircleMenuDrag(); 
					setTimeout(function():void
					{
						if(!circleMenuDragging) clearCircleMenuItemDragging();
					},200);
				}
		}
		
		private function startCircleMenuDrag():void
		{
			
			circleMenuDraggingMouseY = stage.mouseY;
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onCircleMenuItemMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, clearCircleMenuItemDragging);		
		}
		
		private function onCircleMenuItemMouseMove(event:MouseEvent):void
		{
			
			var delta:int = activeCircleMenu.angleSpacing * circleMenuScale * 2;
			var change:int = circleMenuDraggingMouseY - stage.mouseY;
			

			if(Math.abs(change) < delta)
				return;
			
			circleMenuDragging = true;
			filterMenus();
			
			
			selectedItem.removeEventListener( CircularMenuItem.BUTTON_DOWN, onCircleMenuItemMouseDown);
			selectedItem.removeEventListener( CircularMenuItem.BUTTON_UP,onCircleMenuItemMouseUp);
			
			change > delta 
				? activeCircleMenu.next()
				: change < -delta ? activeCircleMenu.prev() : null;
			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onCircleMenuItemMouseMove);
			
			setTimeout(function():void{
				if(circleMenuDragging)
					stage.addEventListener(MouseEvent.MOUSE_MOVE, onCircleMenuItemMouseMove);		
				
			},200);
			
		}
		
		private function clearCircleMenuItemDragging(event:Event = null):void
		{

			circleMenuDragging = false;
			if(selectedItem){
				selectedItem.addEventListener( CircularMenuItem.BUTTON_DOWN, onCircleMenuItemMouseDown);
				selectedItem.addEventListener( CircularMenuItem.BUTTON_UP,onCircleMenuItemMouseUp);
			}
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onCircleMenuItemMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onCircleMenuItemMouseUp);
	
		}
		
		
		
		private function onCircleMenuItemMouseUp(event:Event):void
		{
			if(circleMenuDragging){
				clearCircleMenuItemDragging();
				return;
			}
			
			if(activeCircleMenu.open == false){
				filterMenus();
				drawCircleMenuBackground(activeCircleMenu);
				activeCircleMenu.open = true;
				return;
			}
			
			activeCircleMenu.scrollToItem( selectedItem );
			setTimeout(onCircleMenuPositionComplete,500);
			filterMenus();
			circleMenuBackground.visible = true;
			
			

		}
		
		private function onCircleMenuPositionComplete():void
		{
			if(selectedItem.guideSection)
				Model.currentSection = selectedItem.guideSection;

			showPreview(selectedItem.buttonImage.bitmapData);
		
			setTimeout( onCircleMenuPositionCompleteActions,400);
		}
		
		private function onCircleMenuPositionCompleteActions():void
		{
				
			if(selectedItem.guideScene){
				if(selectedItem.guideScene == Model.guideScene)
					return;

				Model.loadGuideScene(selectedItem.guideScene.scene.id);
				
			}
			else if(selectedItem.circleMenu.open){
				
				for each(var existing:CircleMenu in circleMenus)
				if(existing.guideSection == selectedItem.guideSection){
					existing.visible = true;
					return;
				}
				
				selectedItem.circleMenu.open = false;
				
				if(selectedItem.guideSection.firstScene)
					if(Model.guideScene != selectedItem.guideSection.firstScene){
						sceneText.show(selectedItem.guideSection.firstScene);
					}

				if(selectedItem.guideSection.firstScene)
					if(previewImage.visible 
						&& previewLoading == selectedItem.guideSection.firstScene.scene.thumbName
						&& selectedItem.guideSection.firstScene 
						&& selectedItem.guideSection == Model.currentSection){
						Model.loadGuideScene(selectedItem.guideSection.firstScene.scene.id);
					}
				

				
				createCircleMenu(selectedItem.guideSection, selectedItem.depth + 1 )
			}
			else
				selectedItem.circleMenu.open = true;
		}
		
		
		private function onSectionChange(event:Event):void
		{
			Log.record("Guide.onSectionChange()");
			
		}
		
		private function filterMenus(_onComplete:Function = null):void
		{
			var list:Vector.<CircleMenu> = new Vector.<CircleMenu>;
				list.push(rootMenu);
			
			for each(var menu:CircleMenu in circleMenus){
				
				if(menu.guideSection){
					 if(menu != activeCircleMenu 
						&& !Model.currentSection.isAncestor(menu.guideSection)
						&& !Model.currentSection.isDecendant(menu.guideSection)
					)
						menu.visible = false;
					 
					 else if(menu.guideSection == selectedItem.guideSection)
						 menu.visible = false;
					else{
						menu.open = menu == activeCircleMenu;
						
						list.push(menu);
					}
				}
				else
					menu.open = menu == activeCircleMenu;

			}
			circleMenus = list;
			
			
			var tweenX:Number = 
				circleMenuX - ((circleMenuOffset * (selectedItem != null ? selectedItem.depth - 1 : 0)) * circleMenuScale);

			if(!circleMenuDragging)
				Tweener.addTween(circleMenuHolder, {x:  tweenX, time : 0.4, onComplete: _onComplete});
		}
		
		

		
		
		private function nextPrevButtons():void
		{	
			
			Log.record('guide.nextPrevButtons()');
			//moreButton.scaleX = moreButton.scaleY = 1.5;
			
			resize(_width,_height);
			
		}
		
		
		
		private function onButtonMouseDown(e:MouseEvent):void
		{
			e.currentTarget.bg.alpha = 0.5;
		}
		

		
		//builds the list for next button and presentation
		private function buildSceneList(section:GuideSectionData):void
		{
			Model.guideSectionList.push(section);
			
			for each (var scene:GuideSceneData in section.guideScenes){
				Model.guideSceneList.push(scene);
			}
			
			for each(var childSection:GuideSectionData in section.sectionChildren)
			buildSceneList(childSection);
		}
		
		
		
		public function resize(width:Number, height:Number):void
		{
			Log.record("Guide.resize(" + width +"/"+ height + ")");
			
			sceneText.resize(width,height );
			_width = width;
			_height = height;
		}
		
		private function onLoadingSceneChange(event:Event):void
		{
			if(!Model.isPlayingGame)
				sceneText.hide();
			
			for each(var item:CircularMenuItem in circleMenuItems)
				if(item.guideScene.scene.id == Model.state.loadingScene.id)
					showPreview(item.buttonImage.bitmapData);

		}

		private function onSceneDataChange(event:Event):void
		{	
			if(!previewImage.visible)
				ImageLoader.load(
				Model.config.getAssetUrl(Model.currentScene.thumbName), 
				imageLoadCallback,
				imageLoadCallbackError);
		}
		
		private function imageLoadCallback(data:BitmapData):void
		{
			showPreview(data);
		}
		
		
		private function onSceneChange(event:Event):void
		{
			if(!Model.isPlayingGame)
				sceneText.show();
		}
		
		private function onGameViewChange(event:Event):void
		{
			Log.record("Guide.onGameViewChange()");
			
			if(Model.state.hideGame){
				nextPrevButtons();
				Model.state.guideView = State.GUIDE_VIEW_CLOSED;
				game.visible = false;
			}
			else{
				Model.state.guideView = State.GUIDE_VIEW_TASK;
				game.visible = true;
			}
			
		}
		

		private function getCurrentSectionFromSceneId(id:String):GuideSectionData
		{
			for(var i:int; i<_guideData.guideSections.length; i++){
				for(var s:int = 0; s<_guideData.guideSections[i].guideScenes.length; s++){
					if(_guideData.guideSections[i].guideScenes[s].scene.id == id){
						return _guideData.guideSections[i]		
					}
				}
			}
			return null;
			
	
		}
	

		
		private function onPanoTilesLoaded(event:Event):void
		{
				Tweener.addTween(previewImage,{alpha: 0, width: _width, height: _height, x:0, y: 0, time: 1, onComplete: function(){previewImage.visible = false}});
				Tweener.addTween(blur,{blurX:10, blurY:10,  time: 0.5});
				
				
		}	
	
		
		
		private function onGuideSectionsChange(event:Event):void
		{
			Log.record("Guide.onGuideSectionsChange()");
			//buildMenu();
		}
		

		
		private function createGame():void
		{
			Log.record('guide.createGame()');
			
			if(Model.state.htmlUpdate)
				return;
			
			circleMenuHolder.visible = false;

			sceneText.showTask(Model.currentTask);

			
			Model.state.guideView = State.GUIDE_VIEW_TASK;
			
			Model.addEventListener(Model.GAME_TASK_COMPLETED,onGameTaskComplete);
			Model.addEventListener(Model.GAME_HOTSPOT_FOUND, onHotspotFound);
			Model.addEventListener(Model.GAME_COMPLETE,onGameComplete);
			Model.addEventListener(Model.GAME_TASK_CHANGED,nextGameTask);
		}
		

		

		
		private function onGameTaskComplete(e:Event):void
		{
			Log.record('guide.onGameTaskComplete');
	

			if(!sceneText.taskComplete)
				sceneText.showFeedback();
			
			
			if(Model.currentTask) 
				setTimeout(nextGameTask,8000);
			else
				Model.dispatchGameComplete();
		}
		
		private function onHotspotFound(e:Event):void
		{
			Log.record('guide.onHotspotFound');
			
			sceneText.showFeedback(Model.hotspotsFound, 
				Model.currentTask.hotspotsToFind.length == Model.hotspotsFound.length ? true : false );

		}
		
		private function nextGameTask(e:Event = null):void
		{
			sceneText.showTask(Model.currentTask);
		}
		
		private function onGameComplete(e:Event):void
		{
			Log.record('guide.onGameComplete()');
			setTimeout(removeGame,2000);
			
		}
		
		private function removeGame():void
		{
			removeChild(game);
			this.mouseChildren = true;
			this.mouseEnabled = true;
			mouseChildren = true;
			mouseEnabled = true;
			//addChild(restartTasks);
			//restartTasks.x = 10;
			//restartTasks.y = 10;
			//Fonts.format(restartTasks.label,Fonts.bodyFont,"Restart Tasks >");
			//restartTasks.addEventListener(MouseEvent.MOUSE_DOWN, restartGame);
			
			game = null;
		}
		
		private function restartGame(e:Event = null):void
		{
			Model.resetGameTasks();
			//removeChild(restartTasks);
			//restartTasks.removeEventListener(MouseEvent.MOUSE_DOWN, restartGame);
			createGame();
		}
		
		private function onSectionSectionsChanged(e:Event):void
		{
			Log.record('Guide.onSectionSectionsChanged()');
			
		}
		
		private function onSectionScenesChanged(e:Event):void
		{
			Log.record('Guide.onSectionScenesChanged()');
			
		}
		
		public function hide():void
		{
			
			sceneText.hide();
			
			if(activeCircleMenu)
				activeCircleMenu.open = false;
			
			var tweenX:Number = 
				circleMenuX - ((circleMenuOffset * (selectedItem != null ? selectedItem.depth : 0)) * circleMenuScale);
			
			if(!circleMenuDragging)
				Tweener.addTween(circleMenuHolder, {x:  tweenX, time : 1});
			
			Model.state.guideView = State.GUIDE_VIEW_CLOSED;
		}
		
		
		private function onPanoDrag(e:Event):void
		{
			if(!Model.isPlayingGame)
				hide();

		}
		
		function drawCircleMenuBackground(circleMenu:CircleMenu):void {
			
			var mat:Matrix= new Matrix();
			var colors:Array=[0x000000,0x000000];
			var alphas:Array=[1,0.1];
			var	size = (circleMenu.innerRadius + circleMenuOffset) * circleMenuScale;
			var ratios:Array=[254.9,255];
			
			mat.createGradientBox(size, size);
			circleMenuBackground.graphics.clear();
			circleMenuBackground.graphics.beginGradientFill(GradientType.RADIAL, colors,alphas,ratios,mat);
			circleMenuBackground.graphics.drawCircle(size/2,size/2,size);
			circleMenuBackground.graphics.endFill();
			
			circleMenuHolder.addChildAt( circleMenuBackground, 0);
			circleMenuBackground.blendMode = "darken";
			circleMenuBackground.x = circleMenu.x - size/2;
			circleMenuBackground.y = circleMenu.y - size/2;
			
		}
		
		function showPreview(data:BitmapData):void
		{			
			previewImage.bitmapData = data;
			previewImage.filters = [blur];
			previewImage.width = _width;
			previewImage.height = _height;
			previewImage.x = 0;
			previewImage.y = 0;
			previewImage.visible = true;
			previewImage.alpha = 0;
			Tweener.addTween(previewImage,{alpha: 1, height:_height * 1.1,width : _width * 1.1, x:- _width * 0.05,y:- _height * 0.05,  time: 0.5});
			Tweener.addTween(blur,{blurX:30, blurY:30,  time: 1});

		}
		
		private function imageLoadCallbackError():void
		{
			trace("GuideSectionPage.imagePageLoadCallbackError");
		}
		
		private function onVRModeChange(event:Event):void
		{
			if(Model.state.vr){
				circleMenuHolder.visible = false;
			}
			else{
				circleMenuHolder.visible = true;
			}
		}
		*/
		
	}
}