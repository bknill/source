package co.beek.guide
{
	import flash.events.MouseEvent;
	
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.utils.ColorUtil;

	public class Bookmark extends BookmarkFl
	{
		public function Bookmark()
		{
			super();
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, onTopClick);
		}
		
		public function set color(value:uint):void
		{
			ColorUtil.colorize(bg, value);
		}

		
		private function onTopClick(event:MouseEvent):void
		{
			Model.state.guideView = State.GUIDE_VIEW_SCENE;
		}
	}
}