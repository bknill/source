package co.beek.guide.pages
{
	import flash.events.Event;
	
	import co.beek.Fonts;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.State;
	import co.beek.model.Model;

	public class GuideSectionPageRenderer extends GuideSectionPageRendererFl
	{
		private var _guideScene:GuideSceneData;
		
		public function GuideSectionPageRenderer(data:GuideSceneData)
		{
			super();
			_guideScene = data;
			_guideScene.addEventListener(Event.CHANGE, onGuideSceneChange);
			_guideScene.scene.addEventListener(Event.CHANGE, onGuideSceneChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE,onGuideViewChange);
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE,onSceneChange);
		
			
			titleField.text = data.titleMerged;
			titleField.selectable = false;
			arrowField.text = String(_guideScene.pageNum);
			
			Fonts.format(titleField, Fonts.bodyFont);
			Fonts.format(arrowField, Fonts.bodyFont);
			
			
			update();
		}
		
		public function get guideScene():GuideSceneData
		{
			return _guideScene;
		}
		
/*		public function get guideSection():GuideSceneData
		{
			return _guideSection;
		}*/
		
		private function onGuideSceneChange(event:Event):void
		{
			update();
		}
		
		private function update():void
		{
			titleField.text = _guideScene.titleMerged;
			/*var dictionary:Dictionary = Constants.getIconsDictionary(icons);
			for(var i:String in dictionary)
				MovieClip(dictionary[i]).visible = false;
			
			if(MovieClip(dictionary[_guideScene.scene.type]) != null)
				MovieClip(dictionary[_guideScene.scene.type]).visible = true;*/
			
			//ColorUtil.colorize(icons, Model.themeColor);
		}
		
		public function dispose():void
		{
			_guideScene.removeEventListener(Event.CHANGE, onGuideSceneChange);
			_guideScene.scene.removeEventListener(Event.CHANGE, onGuideSceneChange);
			_guideScene = null;
		}
		
		private function onGuideViewChange(event:Event):void
		{
			arrowField.text = String(_guideScene.pageNum);	
		}
		
		private function onSceneChange(event:Event):void
		{
			if(_guideScene.scene.id == Model.state.loadingScene.id)
				Fonts.format(titleField, Fonts.bodyFont,null,true)
			else
				Fonts.format(titleField, Fonts.bodyFont,null,false)
		}
	}
}