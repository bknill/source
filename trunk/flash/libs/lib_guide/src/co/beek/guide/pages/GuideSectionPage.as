package co.beek.guide.pages
{
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.event.GuideSceneEvent;
	import co.beek.event.GuideSectionEvent;
	import co.beek.guide.MenuRenderer;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	import co.beek.utils.ColorUtil;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	
	public class GuideSectionPage extends GuideSectionPageFrontFl
	{

		public static const SCENE_SELECTED:String = "SCENE_SELECTED";
		
		public static const SECTION_SELECTED:String = "SECTION_SELECTED";
		
		public static const IMAGE_LOADED:String = "IMAGE_LOADED";
		
		public static const EDIT_MARKERS:String = "EDIT_MARKERS";
		
		public static const BACK:String = "BACK";
		
		private var _guideSection:GuideSectionData;
		
		private var pageImage:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		
		private var newPage:GuideSectionPage;
		private var buttonY:int;
		private var buttonX:int;
		private var buttons:Vector.<MenuRenderer> = new Vector.<MenuRenderer>;
		private var buttonLocator:MovieClip = new MovieClip();
		
		public function GuideSectionPage(guideSection:GuideSectionData)
		{
			super();
			
			_guideSection = guideSection;
			_guideSection.addEventListener(GuideSectionData.GUIDE_SCENES_CHANGED, onScenesChanged);
			_guideSection.addEventListener(GuideSectionData.GUIDE_SECTIONS_CHANGED, onSectionsChanged);
			_guideSection.addEventListener(GuideSectionData.IMAGE_CHANGED, onImageChanged);
			_guideSection.addEventListener(GuideSectionData.NEW_IMAGE, onNewImage);
			_guideSection.addEventListener(GuideSectionData.MARKER_POSITION, onScenesChanged);
			_guideSection.addEventListener(Event.CHANGE, onDataChanged);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEdit);
			Model.addEventListener(Model.SECTION_CHANGE,onSectionChange);
		
			//when there is a image change the title fields properties
			if(_guideSection.page_image){
				createPageImage(_guideSection.page_image);
			}
			else if(_guideSection.newPageImage)
				loadNewImage();
			
			update();
				
		}
		
		private function onBackButton(e:Event):void
		{
			dispatchEvent(new Event(BACK));
		}
		
		private function imageFieldBackground(object:DisplayObject):void
		{
			var bg:Shape = new Shape;
			bg.graphics.beginFill(0xFFFFFFF); 
			bg.graphics.drawRect(object.x, object.y, object.width,object.height);
			bg.graphics.endFill(); 
			bg.alpha = 0.4;
			addChildAt(bg,4);
		}

		
		public function get guideSection():GuideSectionData
		{
			return _guideSection;
		}
		
		
		
		private function onDataChanged(event:Event):void
		{
			trace("GuideSectionPage.onDataChanged()");
			update();
		}
		
		private function update():void
		{
			trace("GuideSectionPage.update()");
			if(_guideSection.page_image)
				createPageImage(_guideSection.page_image);
			if(_guideSection.newPageImage)
				loadNewImage();
			else if (!_guideSection.page_image){
				imageLocator.removeChildren();	
			}
			
			clearMarkers();
			buildMarkers();
		}
		
		private function onScenesChanged(event:Event):void
		{
			trace("GuideSectionPage.onScenesChanged()");
			update()
		}
		
		private function onSectionsChanged(event:Event):void
		{
			trace("GuideSectionPage.onSectionsChanged()");
			update()
		}
		
		private function onImageChanged(event:Event):void
		{
			trace("GuideSectionPage.onImageChanged()");
			update()
		}
		
		private function clearMarkers():void
		{	
	//		markerLocator.removeChildren();
		}
		
	
		private var pageMask:Shape = new Shape; 
		
		
		private function buildMarkers():void
		{
			//mask to stop markers going over edge
			trace("GuideSectionPage.buildMarkers");
			pageMask.graphics.beginFill(0xFF0000); 
			//pageMask.graphics.drawRect(0, -400, 400,400);
			pageMask.graphics.endFill(); 
			addChild(pageMask);
			
			var order:int = 0;

			
			//child sections come first
			var sections:Vector.<GuideSectionData> = Model.guideData.guideSections;
			var multiplier = 365/400;
			
			
			for(var s:int; s<sections.length; s++)
			{
				if(sections[s].parent_section_id == _guideSection.id)
				{
					var sectionRenderer:GuideSectionPageMarkerRenderer = new GuideSectionPageMarkerRenderer(sections[s],order);
					if(sections[s].pagePositionX){
						sectionRenderer.x = sections[s].pagePositionX * multiplier;
						sectionRenderer.y = sections[s].pagePositionY * multiplier;
						
					}
					sectionRenderer.mouseChildren = false;
					sectionRenderer.addEventListener(MouseEvent.MOUSE_DOWN, onMarkerMouseDown),
				    sectionRenderer.addEventListener(MouseEvent.MOUSE_UP, onMarkerMouseUp);
					sectionRenderer.mask = pageMask;
					addChild(sectionRenderer);
					sections[s].addEventListener(GuideSectionData.NEW_IMAGE,onSectionMarkerNewImage);
					order ++;
				}
			}	
			
			for(var i:int=0; i<_guideSection.guideScenes.length; i++)
			{
				var renderer:GuideSectionPageMarkerRenderer = new GuideSectionPageMarkerRenderer(_guideSection.guideScenes[i],order);
				if(_guideSection.guideScenes[i].pagePositionX){
					renderer.x = _guideSection.guideScenes[i].pagePositionX * multiplier;
					renderer.y = _guideSection.guideScenes[i].pagePositionY * multiplier;
					renderer.addEventListener(MouseEvent.MOUSE_DOWN, onMarkerMouseDown),
					renderer.addEventListener(MouseEvent.MOUSE_UP, onMarkerMouseUp);
					
					
				}
				
				renderer.mouseChildren = false;
		
				renderer.mask = pageMask;
				addChild(renderer);
				order ++;
				
			}
			
			//apply the mask
			menuLocator.mask = pageMask;
		}
			

		private var newImagePage:GuideSectionPage;
		private function onSectionMarkerNewImage(e:Event):void
		{
			trace("GuideSectionPage.onSectionMarkerNewImage");
			newImagePage = new GuideSectionPage(Model.currentSection);
			newImagePage.alpha = 1;
			addChild(newImagePage);
			buttonLocator.removeChildren();
		/*	newImagePage.addEventListener(GuideSectionPage.IMAGE_LOADED,onImageLoaded);
			newImagePage.addEventListener(GuideSectionPage.BACK, onBack);
		*/	
		}
		

		private function onMarkerMouseDown(event:MouseEvent):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				GuideSectionPageMarkerRenderer(event.target).startDrag();
		
			dispatchEvent(new Event(EDIT_MARKERS));
		}
		
		private function onMarkerMouseUp(event:Event):void
		{
			var guideScene:GuideSceneData = GuideSectionPageMarkerRenderer(event.currentTarget).guideScene;
			var guideSection:GuideSectionData = GuideSectionPageMarkerRenderer(event.currentTarget).guideSection;

			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE){
				GuideSectionPageMarkerRenderer(event.target).stopDrag();
				if(guideScene){
					guideScene.pagePositionX = GuideSectionPageMarkerRenderer(event.currentTarget).x;
					guideScene.pagePositionY = GuideSectionPageMarkerRenderer(event.currentTarget).y;
				}
				else if(guideSection){
					guideSection.pagePositionX = GuideSectionPageMarkerRenderer(event.currentTarget).x;
					guideSection.pagePositionY = GuideSectionPageMarkerRenderer(event.currentTarget).y;
				}
				
			}
		
		}
		
		
		
		private function onImageLoaded(e:Event):void
		{
			Tweener.addTween(newPage,{alpha:1, time: 0.5, transition: 'easeInOutCubic'});	
		}
		
		
		private function onSectionChange(e:Event):void
		{
			for each(var button:MenuRenderer in buttons)
				if(this.getChildByName(button.name))
					removeChild(button)
		}
		
		
		private function onBack(e:Event):void
		{
			newPage = e.target as GuideSectionPage;
			Tweener.addTween(newPage,{alpha:0, time: 0.2, onComplete : removeNewPage, transition: 'easeInOutCubic'});	
			Model.currentSection = guideSection;
		}
		
		private function removeNewPage():void
		{
			removeChild(newPage);
		}
		

		public function dispose():void
		{
			super.dispose();
			_guideSection.removeEventListener(GuideSectionData.GUIDE_SCENES_CHANGED, onScenesChanged);
			_guideSection.removeEventListener(Event.CHANGE, onDataChanged);
			_guideSection.removeEventListener(GuideSectionData.MARKER_POSITION, onScenesChanged);
			_guideSection.removeEventListener(GuideSectionData.NEW_IMAGE, onNewImage);
			_guideSection = null;
		}
		
		private function createPageImage(image:String):void
		{
			ImageLoader.load(
				Model.config.getAssetUrl(image), 
				imagePageLoadCallback,
				imagePageLoadCallbackError
			);	
		}
		
		private function imagePageLoadCallback(data:BitmapData):void
		{
			trace("imagePageLoadCallback()");
			pageImage.bitmapData = data;
			pageImage.alpha = 1;
			addChildAt(pageImage,1);
			pageImage.width = 365;
			pageImage.height = 365;	
			
			dispatchEvent(new Event(IMAGE_LOADED));
			
			
		}
		
		
		private function mapLoadCallback(data:BitmapData):void
		{
			trace("GuideSectionPage.MapLoadCallback()");
			pageImage.bitmapData = data;
		}
		
		private function MapLoadCallbackError():void
		{
			trace("GuideSectionPage.imagePageLoadCallbackError");
		}
		
		
		
		private function imagePageLoadCallbackError():void
		{
			trace("GuideSectionPage.imagePageLoadCallbackError");
		}
		
		private function onNewImage(event:Event):void
		{
			trace("GuideSectionPage.onNewImage()");
			loadNewImage();
		}
		
		public function loadNewImage():void
		{
			trace("GuideSectionPage.loadNewImage");
			pageImage.bitmapData = _guideSection.newPageImage;
			pageImage.width = 365;
			pageImage.height = 365;
			imageLocator.addChild(pageImage);
			dispatchEvent(new Event(IMAGE_LOADED));
		}
		
		
		private function onEdit(event:Event):void
		{
			if(!_guideSection)
				return;
			
			if(Model.state.guideView == State.GUIDE_VIEW_SECTION && _guideSection.markers){
					clearMarkers();
					buildMarkers();
				}
		}
		
		

		
	}
}