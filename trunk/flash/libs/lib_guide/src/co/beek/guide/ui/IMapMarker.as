package co.beek.guide.ui
{
	import flash.display.Sprite;
	import flash.text.TextField;

	public interface IMapMarker
	{
		function get bg():Sprite;
		function get border():Sprite;
		function get numField():TextField;
		function get icons():Sprite;
		
		function get titleField():TextField;
		function get arrow():Sprite;
		function get selectedBg():Sprite;
		function get selectedBorder():Sprite;
	}
}