package co.beek.guide.ui
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	
	public class GuideMapMarker extends MapMarkerSceneFl implements IMapMarker
	{
		public function GuideMapMarker()
		{
			super();
		}
		
		public function get bg():Sprite
		{
			return _bg;
		}
		
		public function get border():Sprite
		{
			return _border;
		}
		
		public function get numField():TextField
		{
			return _numField;
		}
		
		public function get icons():Sprite
		{
			return _icons;
		}
		
		public function get titleField():TextField
		{
			return _titleField;
		}
		public function get arrow():Sprite
		{
			return _arrow;
		}
		
		public function get selectedBg():Sprite
		{
			return _selectedBg;
		}
		
		public function get selectedBorder():Sprite
		{
			return _selectedBorder;
		}
	}
}