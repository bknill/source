package co.beek.book
{
	import co.beek.guide.pages.BackCover;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Page extends Sprite
	{
		public static const DRAG_START:String = "DRAG_START";
		
		public static const FLICK_BACKWARDS:String = "FLICK_BACKWARDS";
		public static const FLIP_BACKWARDS_HALF:String = "FLIP_BACKWARDS_HALF";
		public static const FLIP_BACKWARDS_COMPLETE:String = "FLIP_BACKWARDS_COMPLETE";
		
		public static const FLICK_FORWARDS:String = "FLICK_FORWARDS";
		public static const FLIP_FORWARDS_HALF:String = "FLIP_FORWARDS_HALF";
		public static const FLIP_FORWARDS_COMPLETE:String = "FLIP_FORWARDS_COMPLETE";
		
		protected var _front:DisplayObject;
		protected var _back:DisplayObject;
		
		
		
		public function Page(front:DisplayObject, back:DisplayObject)
		{
			super();
			_front = front;
			_back = back;
			
			addChild(front);
			addChild(back);
			showFront();
		}
		
		public function get front():DisplayObject
		{	return _front }
		
		public function get back():DisplayObject
		{	return _back }
		
		public function showFront():void
		{
			_front.visible = true;
			_back.visible = false;
		}
		
		public function showBack():void
		{
			_front.visible = false;
			_back.visible = true;
		}
		
		public function flipForwards():void
		{
			showBack();
			dispatchEvent(new Event(FLIP_FORWARDS_HALF));
			dispatchEvent(new Event(FLIP_FORWARDS_COMPLETE));
		}
		
		public function flipBackwards():void
		{
			showFront();
			dispatchEvent(new Event(FLIP_BACKWARDS_HALF));
			dispatchEvent(new Event(FLIP_BACKWARDS_COMPLETE));
		}
		
		public function clearBitmapCache():void
		{
			//transform.matrix3D = null;
			//_front.transform.matrix3D = null;
			//_back.transform.matrix3D = null;
		}
		
		public function dispose():void
		{
			_front = null;
			_back = null;
		}
	}
}