package co.beek.book
{
	import caurina.transitions.Tweener;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class RigidPage extends Page
	{
		private static const FOLD_TIME:Number = 1.5;
		private static const DRAG_THRESHOLD:Number = 20;
		
		private var passedHalf:Boolean;
		
		private var mouseDown:Point;
		private var dragging:Boolean;
		private var rotationXStart:Number;
		
		public function RigidPage(front:DisplayObject, back:DisplayObject)
		{
			super(front, back);
			
			//addEventListener(MouseEvent.MOUSE_DOWN, onFrontMouseDown);
			//back.addEventListener(MouseEvent.MOUSE_DOWN, onBackMouseDown);
		}
		
		private function get mousePos():Point
		{
			return new Point(stage.mouseX, 0);
		}
		
		
		public function onFrontMouseDown(event:MouseEvent):void
		{
			var pos:Point = mousePos;
			//if(pos.x > _front.width/2)
			//{
				mouseDown = pos;
				stage.addEventListener(MouseEvent.MOUSE_MOVE, onFrontMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, onFrontMouseUp);
			//}
		}
		
		public function onFrontMouseMove(event:MouseEvent):void
		{
			var current:Point = mousePos;
			var distance:Number = Point.distance(mouseDown, current);
			if(dragging || distance > DRAG_THRESHOLD)
			{
				if(!dragging)
				{
					_back.scaleX = -1;
					dragging = true;
				}
				rotationY = (distance/_front.width) * 180;
				onFlipUpdate();
			}
		}
		
		private function onFlipUpdate():void
		{
			if(rotationY > 90)
				showBack();
			else
				showFront();
		}
		
		public function onFrontMouseUp(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onFrontMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onFrontMouseUp);
			
			if(!dragging)
				return;
			
			Tweener.addTween(this, {
				rotationY: (rotationY > 90) ? 180 : 0,
				time:FOLD_TIME,
				transition:"easeoutquad",
				onUpdate: onFlipUpdate,
				onComplete: (rotationY > 90) 
					? onFlipForwardsComplete 
					: onFlipBackwardsComplete
			});
			
			if(rotationY > 90)
				dispatchEvent(new Event(FLICK_FORWARDS))
			else
				dispatchEvent(new Event(FLICK_BACKWARDS))
		}
		
		/**
		 * Flips the page from the back to the front
		 */
		public override function flipForwards():void
		{
			//_back.scaleX = -1;
			cacheAsBitmap = true;
			Tweener.addTween(this, {
				rotationY: 180,
				time:FOLD_TIME,
				transition:"easeoutquad",
				onUpdate:onFlipForwardsUpdate,
				onComplete:onFlipForwardsComplete
			});
		}
		
		private function onFlipForwardsUpdate():void
		{
			if(rotationY > 90 && !passedHalf)
			{
				passedHalf = true;
				showBack();
				dispatchEvent(new Event(FLIP_FORWARDS_HALF));
			}
		}

		private function onFlipForwardsComplete():void
		{
			//_back.scaleX = 1;
			passedHalf = false;
			clearBitmapCache();
			dispatchEvent(new Event(FLIP_FORWARDS_COMPLETE));
		}

		/**
		 * Flips the page from the front to the back
		 **/
		public override function flipBackwards():void
		{
			//_front.scaleX = -1;
			rotationY = 0;
			cacheAsBitmap = true;
			Tweener.addTween(this, {
				rotationY: -180,
				time:FOLD_TIME,
				transition:"easeoutquad",
				onUpdate:onFlipBackwardsUpdate,
				onComplete:onFlipBackwardsComplete
			});
		}
		
		private function onFlipBackwardsUpdate():void
		{
			if(rotationY < -90 && !passedHalf)
			{
				passedHalf = true;
				showFront();
				dispatchEvent(new Event(FLIP_BACKWARDS_HALF));
			}
		}

		private function onFlipBackwardsComplete():void
		{
			//_front.scaleX = 1;
			passedHalf = false;
			clearBitmapCache();
			dispatchEvent(new Event(FLIP_BACKWARDS_COMPLETE));
		}
	}
}