package co.beek.book
{
	
	import caurina.transitions.Tweener;
	
	import com.foxaweb.pageflip.PageFlip;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	public class Book extends Sprite
	{
		private static const FOLD_BACK_TIME:Number = 0.7;
		public static const PAGE_CHANGING:String = "PAGE_CHANGING";
		public static const PAGE_CHANGED:String = "PAGE_CHANGED";
		public static const FOLDBACK_COMPLETE:String = "FOLDBACK_COMPLETE";
		public static const UNFOLD_COMPLETE:String = "UNFOLD_COMPLETE";
		
		public var pages:Vector.<Page> = new Vector.<Page>;
		
		private var _destPage:int;
		
		private var _pageNum:int; 
		
		
		private var foldData:FoldData = new FoldData;
		
		
		public function Book()
		{
			super();
		}
		
		private var front:Sprite;
		public function addSideAt(side:Sprite, index:int):void
		{
			if(front)
			{
				addPageAt(new FlexiPage(front, side), index);
				front = null;
			}
			else
			{
				front = side;
			}
		}

		public function addSide(side:Sprite):void
		{
			addSideAt(side, pages.length);
		}
		
		public function addPage(page:Page):void
		{
			addPageAt(page, pages.length);
		}
		
		public function addPageAt(page:Page, index:int):void
		{
			page.addEventListener(Page.DRAG_START, onPageDragStart);
			
			page.addEventListener(Page.FLICK_FORWARDS, onPageFlickForwards);
			page.addEventListener(Page.FLIP_FORWARDS_HALF, onPageFlipForwardsHalf);
			page.addEventListener(Page.FLIP_FORWARDS_COMPLETE, onPageFlipForwardsComplete);
			
			page.addEventListener(Page.FLICK_BACKWARDS, onPageFlickBackwards);
			page.addEventListener(Page.FLIP_BACKWARDS_HALF, onPageFlipBackwardsHalf);
			page.addEventListener(Page.FLIP_BACKWARDS_COMPLETE, onPageFlipBackwardsComplete);
			
			
			var insertPoint:int = 0;
			if(pages.length > 0 && index < pages.length)
				insertPoint = getChildIndex(pages[index-1]);
			
			addChildAt(page, insertPoint);
			pages.splice(index, 0, page);
		}
		
		public function removePageAt(index:int):Page
		{
			var page:Page = pages[index];
			
			page.removeEventListener(Page.DRAG_START, onPageDragStart);
			
			page.removeEventListener(Page.FLICK_FORWARDS, onPageFlickForwards);
			page.removeEventListener(Page.FLIP_FORWARDS_HALF, onPageFlipForwardsHalf);
			page.removeEventListener(Page.FLIP_FORWARDS_COMPLETE, onPageFlipForwardsComplete);
			
			page.removeEventListener(Page.FLICK_BACKWARDS, onPageFlickBackwards);
			page.removeEventListener(Page.FLIP_BACKWARDS_HALF, onPageFlipBackwardsHalf);
			page.removeEventListener(Page.FLIP_BACKWARDS_COMPLETE, onPageFlipBackwardsComplete);
			
			removeChild(page);
			pages.splice(index, 1);
			
			return page;
		}
		
		public function reset():void
		{
			pages[0].visible = true;
			for(var i:int = 1; i<pages.length-1; i++)
				pages[i].visible = false;
			
			if(_pageNum > 0)
				pages[_pageNum-1].visible = true;
			pages[_pageNum].visible = true;
			pages[pages.length-1].visible = true;
		}
		
		public function getPagesOfType(PageType:Class):Vector.<Page>
		{
			var pagesOfType:Vector.<Page> = new Vector.<Page>;
			for(var i:int = 0; i<pages.length; i++)
				if(pages[i] is PageType)
					pagesOfType.push(pages[i]);
			
			return pagesOfType;
		}

		public function getPageIndex(page:Page):int
		{
			return pages.indexOf(page);
		}

		public function getPage(num:int):Page
		{
			return pages[num];
		}
		
		public function get pageNum():int
		{
			return _pageNum;
		}

		public function get currentPage():Page
		{
			return pages[_pageNum];
		}

		public function get destPage():int
		{
			return _destPage;
		}
		
		public function isCover(page:int):Boolean
		{
			return page == 0 || page == pages.length;
		}

		/**
		 * Boolean value to state if the page is still changing
		 */
		public function get pageChanging():Boolean
		{
			return _destPage != _pageNum;
		}
		
		public function get backCover():Page
		{
			return pages[pages.length - 1];
		}
		
		public function clearTransforms():void
		{
			//transform.matrix = new Matrix(1,0,0,1, x, y);
			// clear the transforms of pages that have not been flipped.
			for(var i:int = 0; i<pages.length; i++)
				if(pages[i].rotationY == 0)
					pages[i].clearBitmapCache();
		}
		
		
		public function set pageNum(value:int):void
		{
			if(value < 0 
				|| value > pages.length 
				|| value == _destPage 
				|| value == _pageNum 
				)
				return;
			
			// dont keep changeing if a change is in progress
			if(pageChanging)
				return;
			
			_destPage = value;
			if(Math.abs(_destPage - _pageNum) > 1)
			{
				if(_destPage > _pageNum)
					performMultiPageFlipForwards();
				else
					performMultiPageFlipBackwards();
				return;
			}
			
			if(_destPage > _pageNum)
			{
				pages[_pageNum].flipForwards();
				pages[_pageNum +1].visible = true;
			}
			else
			{
				pages[_pageNum - 1].visible = true;
				pages[_pageNum - 1].flipBackwards();
			}
			
			mouseChildren = false;
			dispatchEvent(new Event(PAGE_CHANGING));
		}
		
		public function set pageNumImmediate(value:int):void
		{
			if(value < 0 
				|| value > pages.length 
				|| value == _destPage 
				|| value == _pageNum)
				return;
			
			_destPage = value;
			changeImmediatly();
		}
		
		private function changeImmediatly():void
		{
			_pageNum = _destPage;
			
			for(var i:int =0; i<pages.length; i++)
			{
				var p:Page = pages[i];
				//p.visible = true;
				
				if(i < _pageNum)
				{
					p.showBack();
					setChildIndex(p, i);
					
				}
				else
				{
					p.showFront();
					setChildIndex(p, pages.length-i-1);
				}
			}
			mouseChildren = true;
			reset();
			//clearTransforms();
			dispatchEvent(new Event(PAGE_CHANGED));
		}
		
		private var multiFront:BitmapData;
		private var multiBack:BitmapData;
		private var multiFlipHolder:Sprite = new Sprite;

		private function performMultiPageFlipForwards():void
		{
			var depth:int = pages.length- _destPage -1;
			//for(var i:int =_pageNum; i<_destPage; i++)
			//	pages[i].visible = false;
			
			pages[_pageNum].visible = false;
			pages[_destPage].visible = true;
			//pages[_destPage+1].visible = true;
			
			var front:DisplayObject = pages[_pageNum].front; 
			multiFront = new BitmapData(front.width, front.height, true, 0x00000000);
			multiFront.draw(front, new Matrix(1, 0, 0, 1, 0, front.height));

			var back:DisplayObject = pages[_destPage-1].back; 
			multiBack = new BitmapData(back.width, back.height, true, 0x00000000);
			multiBack.draw(back,  new Matrix(1, 0, 0, 1, back.width, back.height));
			
			
			var drag:Point = new Point(multiFront.width, multiFront.height);
			Tweener.addTween(drag, {
				x: -multiBack.width,
				y: multiBack.height,
				_bezier:{x:0, y:0},
				time:1,
				transition:"easeoutquad",
				onUpdate:dragForwards,
				onUpdateParams:[drag],
				onComplete:flipForwardsComplete
			});
			
			multiFlipHolder.y = -multiFront.height;
			multiFlipHolder.x = 0;
			multiFlipHolder.graphics.clear();
			addChild(multiFlipHolder);
		}

		private function performMultiPageFlipBackwards():void
		{
			//for(var i:int =_destPage; i<_pageNum; i++)
			//	pages[i].visible = false;
			
			
			var front:DisplayObject = pages[_destPage].front; 
			var back:DisplayObject = pages[_pageNum-1].back; 
			
			multiBack = new BitmapData(front.width, front.height, true, 0x00000000);
			multiBack.draw(front, new Matrix(1, 0, 0, 1, 0, front.height));

			multiFront = new BitmapData(back.width, back.height, true, 0x00000000);
			multiFront.draw(back,  new Matrix(1, 0, 0, 1, back.width, back.height));
			
			
			var drag:Point = new Point(0, multiFront.height);
			Tweener.addTween(drag, {
				x: multiFront.width * 2,
				y: multiFront.height,
				_bezier:{x:multiFront.width, y:0},
				time:1,
				transition:"easeoutquad",
				onUpdate:dragBackwards,
				onUpdateParams:[drag],
				onComplete:flipBackwardsComplete
			});
			
			multiFlipHolder.y = -multiFront.height;
			multiFlipHolder.x = -multiFront.width;
			multiFlipHolder.graphics.clear();
			addChild(multiFlipHolder);
		}
		
		private function flipBackwardsComplete():void
		{
			removeChild(multiFlipHolder);
			changeImmediatly();
		}
		
		public function dragBackwards(drag:Point):void
		{
			dragPos(drag, new Point(0,1))
		}
		
		public function dragForwards(drag:Point):void
		{
			dragPos(drag, new Point(1,1))
		}

		public function dragPos(drag:Point, computePoint:Point):void
		{
			var o:Object = PageFlip.computeFlip(
				drag.clone(), 
				computePoint,
				multiFront.width, 
				multiFront.height);
			
			multiFlipHolder.graphics.clear();
			PageFlip.drawBitmapSheet(o,// computeflip returned object
				multiFlipHolder,// target
				multiFront,// bitmap page 0
				multiBack);// bitmap page 1
		}
		
		private function flipForwardsComplete():void
		{
			removeChild(multiFlipHolder);
			changeImmediatly();
		}
		
		
		protected function onPageDragStart(event:Event):void
		{
			var page:Page = Page(event.target);
			pages[pages.indexOf(page) + 1].visible = true;
			dispatchEvent(new Event(PAGE_CHANGING));
		}
		
		private function onPageFlickForwards(event:Event):void
		{
			_destPage = _destPage + 1;
			pages[_destPage].visible = true;
		}

		private function onPageFlickBackwards(event:Event):void
		{
			_destPage = _destPage - 1;
			pages[_destPage].visible = true;
			//mouseChildren = false;
		}
		
		private function onPageFlipForwardsHalf(event:Event):void
		{
			var page:Page = Page(event.target);
			var index:int = pages.indexOf(Page(event.target));
			
			setChildIndex(page, numChildren-1);
			
			if(index < _destPage - 1 && index < pages.length-1)
				pages[index + 1].flipForwards();
		}
		
		
		private function onPageFlipForwardsComplete(event:Event):void
		{
			var index:int = pages.indexOf(Page(event.target));
			if(index == _destPage - 1)
				pageChangeComplete();
		}
		
		public function pageChangeComplete():void
		{
			_pageNum = _destPage;
			
			if(_pageNum < pages.length)
				setChildIndex(pages[_pageNum], numChildren-1);

			if(_pageNum > 0)
				setChildIndex(pages[_pageNum - 1], numChildren-1);
			
			mouseChildren = true;
			dispatchEvent(new Event(PAGE_CHANGED));
		}

		private function onPageFlipBackwardsHalf(event:Event):void
		{
			var page:Page = Page(event.target);
			var index:int = pages.indexOf(Page(event.target));
			
			setChildIndex(page, numChildren-1);
			
			if(index > _destPage && index > 0)
				pages[index - 1].flipBackwards();
		}
		
		
		private function onPageFlipBackwardsComplete(event:Event):void
		{
			var index:int = pages.indexOf(Page(event.target));
			if(index == _destPage)
				pageChangeComplete();
		}
		
		
		/*public function set folded(value:Boolean):void
		{
			if(value)
				foldBack();
			
			else
				unfold();
		}*/
		
		/*
		public function get folded():Boolean
		{
			return _folded;
		}
		
		public function foldBack():void
		{
			if(folding || _folded)
				return;
			
			backCover.back.scaleX = -1;
			
			setChildIndex(backCover, 0);
			
			Tweener.removeTweens(foldData);
			// populate the folddata in preparation for the tween
			foldData.locatorX = x;
			foldData.rotation = 0;
			
			Tweener.addTween(foldData, 
			{
				locatorX: backCover.width/2,
				rotation: -180,
				time: FOLD_BACK_TIME,
				transition: "easeoutquad",
				onUpdate: onFoldbackUpdate,
				onComplete: onFoldbackComplete
			});
		}
		
		public function clearTweens():void
		{
			Tweener.removeTweens(foldData);
		}
		
		private function onFoldbackUpdate():void
		{
			//x = data.locatorX;
			
			foldbackPassedHalf = (foldData.rotation < -90 && !foldbackPassedHalf);
				
			for(var i:int = _pageNum; i<pages.length; i++)
				pages[i].rotationY = foldData.rotation;
			
			if(foldbackPassedHalf)
				showBackCoverFolding();
		}
		
		private function showBackCoverFolding():void
		{
			backCover.showBack();
			for(var i:int = _pageNum; i<pages.length-1; i++)
				pages[i].z = i;
			backCover.z = i;
		}
		
		private function onFoldbackComplete():void
		{
			foldbackPassedHalf = false;
			_folded = true;
			
			dispatchEvent(new Event(FOLDBACK_COMPLETE));
		}
		
		public function unfold():void
		{
			if(folding)
				return;
			
			Tweener.removeTweens(foldData);
			
			foldData.locatorX = x;
			foldData.rotation = -180;
			
			Tweener.addTween(foldData, 
				{
					locatorX: 0,
					rotation: 0,
					time: FOLD_BACK_TIME,
					transition: "easeoutquad",
					onUpdate: onUnfoldUpdate,
					onComplete: onUnfoldComplete
				}
			);
		}
		
		private function onUnfoldUpdate():void
		{
			x = foldData.locatorX;
			
			foldbackPassedHalf = (foldData.rotation > -90 && !foldbackPassedHalf);
			
			for(var i:int = _pageNum; i<pages.length; i++)
				pages[i].rotationY = foldData.rotation;
			
			if(foldbackPassedHalf)
				showPagesUnfolding();
		}
		
		private function showPagesUnfolding():void
		{
			backCover.back.scaleX = 1;
			backCover.z = 0;
			backCover.showFront();
			
			for(var i:int = _pageNum; i<pages.length-1; i++)
				pages[i].z = 0;
		}
		
		private function onUnfoldComplete():void
		{
			foldbackPassedHalf = false;
			_folded = false;
			folding = false;
			clearTransforms();
			
			dispatchEvent(new Event(UNFOLD_COMPLETE));
		}
		public function alignPages():void
		{
			return;
			for(var i:int = 0; i<pages.length; i++)
				Tweener.addTween(pages[i], {time:1.5, x:0});
		}
		
		public function staggerPages():void
		{
			return;
			for(var i:int = 0; i<pages.length; i++)
				pages[i].x = i - _pageNum;
		}
		*/
		
		public function clear():void
		{
			_pageNum = 0;
			_destPage = 0;
			
			if(pages.length > 0)
			{
				while(numChildren > 0)
					removeChildAt(0);
				
				for(var i:int = 0; i<pages.length; i++)
					pages[i].dispose();
				
				pages = new Vector.<Page>;
			}
		}
		
	}
}