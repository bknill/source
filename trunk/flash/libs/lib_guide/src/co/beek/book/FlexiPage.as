package co.beek.book
{
	import caurina.transitions.Tweener;
	
	import com.foxaweb.pageflip.PageFlip;
	
	import co.beek.model.State;
	import co.beek.model.Model;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	public class FlexiPage extends Page
	{
		private static const FOLD_TIME:Number = 1;
		private static const DRAG_THRESHOLD:Number = 20;
		
		private var frontBitmap:BitmapData;
		private var backBitmap:BitmapData;
		
		private var pageFlipHolder:Sprite = new Sprite;
		private var passedHalf:Boolean;
		
		private var mouseDown:Point;
		private var dragging:Boolean;
		
		private var sound:Book_page_flip = new Book_page_flip;
		
		public function FlexiPage(front:Sprite, back:Sprite)
		{
			super(front, back);
			pageFlipHolder.y = -height;
			addChild(pageFlipHolder);
			
			frontBitmap = new BitmapData(width, height, true, 0x00000000);
			backBitmap = new BitmapData(width, height, true, 0x00000000);
			
			front.addEventListener(MouseEvent.MOUSE_DOWN, onFrontMouseDown);
			back.addEventListener(MouseEvent.MOUSE_DOWN, onBackMouseDown);
		}
		
		public override function get width():Number
		{
			return front.width;
		}
		
		public override function get height():Number
		{
			return front.height;
		}
		
		private function get frontMousePos():Point
		{
			return new Point(front.mouseX, height + front.mouseY);
		}
		
		
		public function onFrontMouseDown(event:MouseEvent):void
		{
			mouseDown = frontMousePos;
			if(mouseDown.x > width/2 && Model.state.uiState == State.UI_STATE_NORMAL)
			{
				stage.addEventListener(MouseEvent.MOUSE_MOVE, onFrontMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, onFrontMouseUp);
			}
		}
		
		public function onFrontMouseMove(event:MouseEvent):void
		{
			var current:Point = frontMousePos;
			if(dragging || Point.distance(mouseDown, frontMousePos) > DRAG_THRESHOLD)
			{
				dragForwards(current);
			}
		}
		
		public function onFrontMouseUp(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onFrontMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onFrontMouseUp);
			
			if(!dragging)
				return;
			
			var current:Point = new Point(front.mouseX, height + front.mouseY);
			var dragDistance:Number = Point.distance(mouseDown, current);
			var dest:Point = (dragDistance > DRAG_THRESHOLD)
				? new Point(-width, height)
				: new Point(width, height);
			
			Tweener.addTween(current, 
			{
				x: dest.x,
				y: dest.y,
				time: Point.distance(current, dest)/(width*2),
				transition:"easeoutquad",
				onUpdate:dragForwards,
				onUpdateParams:[current],
				onComplete: (dragDistance > DRAG_THRESHOLD)
					? flipForwardsComplete
					: flipBackwardsComplete
			});
			
			sound.play();
			
			if(dragDistance > DRAG_THRESHOLD)
				dispatchEvent(new Event(FLICK_FORWARDS))
		}
		
		private function get backMousePos():Point
		{
			return new Point(back.mouseX + back.width, back.height + back.mouseY);
		}
		
		public function onBackMouseDown(event:MouseEvent):void
		{
			mouseDown = backMousePos;
			if(mouseDown.x < width/2)
			{
				stage.addEventListener(MouseEvent.MOUSE_MOVE, onBackMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, onBackMouseUp);
			}
		}
		
		public function onBackMouseMove(event:MouseEvent):void
		{
			var current:Point = backMousePos;
			if(dragging || Point.distance(mouseDown, backMousePos) > DRAG_THRESHOLD)
			{
				dragBackwards(current);
			}
		}
		
		public function onBackMouseUp(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onBackMouseMove);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onBackMouseUp);

			if(!dragging)
				return;
			
			var current:Point = backMousePos;
			var dragDistance:Number = Point.distance(mouseDown, current);
			var dest:Point = dragDistance > DRAG_THRESHOLD 
				? new Point(width * 2, height)
				: new Point(0, height);
			var time:Number = Point.distance(current, dest)/(width*2);
			
			Tweener.addTween(current, 	
				{
					x: dest.x,
					y: dest.y,
					time: time,
					transition:"easeoutquad",
					onUpdate:dragBackwards,
					onUpdateParams:[current],
					onComplete: dragDistance > DRAG_THRESHOLD 
						? flipBackwardsComplete
						: flipForwardsComplete
				}
			);
			
			sound.play();
			
			if(dragDistance > DRAG_THRESHOLD)
				dispatchEvent(new Event(FLICK_BACKWARDS))
		}
		
		private function hideBoth():void
		{
			_front.visible = false;
			_back.visible = false;
			passedHalf = false;
		}
		
		private function drawBack(target:BitmapData):void
		{
			try
			{
				target.draw(_back, new Matrix(1, 0, 0, 1, width, height));
			}
			catch(e:Error)
			{
				//trace("Error drawing bitmap "+e.message);
			}
		}
		private function drawFront(target:BitmapData):void
		{
			try
			{
				target.draw(_front, new Matrix(1, 0, 0, 1, 0, height));
			}
			catch(e:Error)
			{
				//trace("Error drawing bitmap "+e.message);
			}
		}
		
		public override function flipForwards():void
		{
			prepareDragForwards();
			
			var drag:Point = new Point(width, height);
			Tweener.addTween(drag, {
				x: -width,
				y: height,
				_bezier:{x:0, y:0},
				time:1,
				transition:"easeoutquad",
				onUpdate:dragForwards,
				onUpdateParams:[drag],
				onComplete:flipForwardsComplete
			});
			
			sound.play();;
		}
		
		public function prepareDragForwards():void
		{
			hideBoth();
			
			backBitmap = new BitmapData(width, height, true, 0x00000000);
			drawBack(backBitmap);
			
			frontBitmap = new BitmapData(width, height, true, 0x00000000);
			drawFront(frontBitmap);
			
			pageFlipHolder.x = 0;
			
			dragging = true;
			dispatchEvent(new Event(DRAG_START));
		}
		
		public function dragForwards(drag:Point):void
		{
			if(!dragging)
				prepareDragForwards();
			
			var o:Object = PageFlip.computeFlip(
				drag.clone(), 
				new Point(1,1),
				width, 
				height);
			
			
			
			pageFlipHolder.graphics.clear();
			
			PageFlip.drawBitmapSheet(o,// computeflip returned object
				pageFlipHolder,// target
				frontBitmap,// bitmap page 0
				//pageFlipHolder,
				backBitmap);// bitmap page 1
			
			
			
			if(drag.x < 0 && !passedHalf)
			{
				passedHalf = true;
				dispatchEvent(new Event(FLIP_FORWARDS_HALF));
			}
		}
		
		private function flipForwardsComplete():void
		{
			clearFlip();
			showBack();
			
			dispatchEvent(new Event(FLIP_FORWARDS_COMPLETE));
		}
		
		private function clearFlip():void
		{
			frontBitmap.fillRect(frontBitmap.rect, 0x00000000);
			backBitmap.fillRect(frontBitmap.rect, 0x00000000);
			pageFlipHolder.graphics.clear();
			passedHalf = false;
			dragging = false;
		}
		
		
		public override function flipBackwards():void
		{
			prepareDragBackwards();
			
			var drag:Point = new Point(0, height);
			Tweener.addTween(drag, {
				x: width * 2,
				y: height,
				_bezier:{x:width, y:0},
				time:1,
				transition:"easeoutquad",
				onUpdate:dragBackwards,
				onUpdateParams:[drag],
				onComplete:flipBackwardsComplete
			});
			
			sound.play();
		}
		
		public function prepareDragBackwards():void
		{
			hideBoth();
			
			frontBitmap = new BitmapData(width, height, true, 0x00000000);
			drawBack(frontBitmap);
			
			backBitmap = new BitmapData(width, height, true, 0x00000000);
			drawFront(backBitmap);
			
			pageFlipHolder.x = -width;
			
			dragging = true;
			dispatchEvent(new Event(DRAG_START));
		}
		
		public function dragBackwards(drag:Point):void
		{
			if(!dragging)
				prepareDragBackwards();
			
			var o:Object = PageFlip.computeFlip(
				drag.clone(), 
				new Point(0,1),
				width, 
				height);
			
			
			pageFlipHolder.graphics.clear();
			PageFlip.drawBitmapSheet(o,// computeflip returned object
				pageFlipHolder,// target
				frontBitmap,// bitmap page 0
				//pageFlipHolder,
				backBitmap);// bitmap page 1
			
			pageFlipHolder.graphics.beginFill(0);
			pageFlipHolder.graphics.drawCircle(drag.x, drag.y, 4);
			pageFlipHolder.graphics.endFill();
			
			if(drag.x > width && !passedHalf)
			{
				passedHalf = true;
				dispatchEvent(new Event(FLIP_BACKWARDS_HALF));
			}
		}
		
		private function flipBackwardsComplete():void
		{
			clearFlip();
			showFront();
			dispatchEvent(new Event(FLIP_BACKWARDS_COMPLETE));
		}
		
		public override function dispose():void
		{
			front.removeEventListener(MouseEvent.MOUSE_DOWN, onFrontMouseDown);
			back.removeEventListener(MouseEvent.MOUSE_DOWN, onBackMouseDown);
			
			pageFlipHolder = null;
			frontBitmap = null;
			backBitmap = null;
			
			super.dispose();
		}
	}
}