package co.beek.header
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.clearTimeout;
	import flash.utils.flash_proxy;
	import flash.utils.setTimeout;
	
	import co.beek.Fonts;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideData;
	import co.beek.utils.JSONBeek;
	
	
	public class FacebookPanel
	{
		private var model:Model; 
		private var mc:ButtonFacebookLikeFl;
		
		private var loadLikeNumberTimeout:uint;

		
		public function FacebookPanel(mc:ButtonFacebookLikeFl, model:Model )
		{
			this.mc = mc;
			this.model = model;
			
			mc.visible = false;
			mc.addEventListener(MouseEvent.CLICK , onFacebookLikeButtonClick);
			
			Fonts.setFormat(mc.field, Fonts.titleFormatSmall);
			
			if(model.guideData)
				showFacebookbutton();
			else
				model.addEventListener(Model.GUIDE_CHANGE, onGuideDataLoaded);			
		}	
	
		
		private function onGuideDataLoaded(event:Event):void
		{
			showFacebookbutton();
		} 
		
		private function showFacebookbutton():void
		{
			mc.visible = true;	
			loadLikeNumber();
		}
		
		private function onFacebookLikeButtonClick(event:MouseEvent):void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"like",
				Model.guideUrl);

			trace("onFacebookLikeButton");
			ExternalInterface.call("openLikeFacebookWindow", Model.guideUrl);

			clearTimeout(loadLikeNumberTimeout);
			for(var i:int = 0 ; i <= 6; i++)
				loadLikeNumberTimeout = setTimeout(loadLikeNumber,i*6000);

		}	
		
		private function loadLikeNumber():void
		{
			trace("loadLikeNumber")
			try
			{
				var url:String = "https://api.facebook.com/method/fql.query?query=select%20like_count%20from%20link_stat%20where%20url=%27"+Model.guideUrl+"%27&format=json";
				var loader:URLLoader = new URLLoader();
				var request:URLRequest = new URLRequest(url);
				request.method = URLRequestMethod.POST;
				loader.addEventListener(Event.COMPLETE, onLoadLikeNumberComplete);
				loader.addEventListener(IOErrorEvent.IO_ERROR, onLoadLikeNumberIOError);
				loader.load(request);
			}
			catch(e:SecurityError)
			{
				trace("FacebookPanel.loadLikeNumber() FAILED!!")
			}
		}
		
		/**
		 * Catch the io errors here in-case facebooks servers stop returning
		 */
		private function onLoadLikeNumberIOError(event:Event):void
		{
			trace("Error loading facebook like number");
		}
		
		/**
		 * 
		 * Data looks like this
		 * [{"like_count":0}]
		 */
		private function onLoadLikeNumberComplete(event:Event):void
		{

			trace("onLoadLikeNumberComplete:");
			trace(event.target.data);
			
			var loader:URLLoader = URLLoader(event.target);
			loader.removeEventListener(Event.COMPLETE, onLoadLikeNumberComplete);
			
			try
			{
				var data:Object = JSONBeek.decode(loader.data);
				var text:String = "0";
				
				// Facebook returns an array of objects.
				// We assume the first in the array is what we want.
				if(data is Array && data[0] != null && data[0].hasOwnProperty('like_count'))
					text = String(data[0]['like_count']);
				
				mc.field.text = text;
			}
			catch(e:Error)
			{
				trace("Error decoding JSON for file:"+event.target.data)
			}
		}
	}
}