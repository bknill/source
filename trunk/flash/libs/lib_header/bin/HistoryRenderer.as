package co.beek.header
{
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.StringUtil;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;

	public class HistoryRenderer extends HistoryRendererFl
	{
		private var logoHolder:Bitmap = new Bitmap();
		
		private var model:Model;
		
		private var _scene:SceneData;
		
		public function HistoryRenderer(model:Model, scene:SceneData)
		{
			super();
			this.model = model;
			this._scene = scene;
			
			logoHolder.smoothing = true;
			logoHolder.x = thumbLocator.x;
			logoHolder.y = thumbLocator.y;
			logoHolder.mask = thumbLocator;
			addChild(logoHolder);
			
			// Is this the scene that is currently being viewed.
			var selected:Boolean = model.currentScene == scene;
			var border:Sprite;
			
			// colorize the currrent scene border to hilight it in the row.
			
			if(selected)
				ColorUtil.colorize(bg, 0xd7d7d7);
			else
				ColorUtil.colorize(bg, 0x666666);
				
			Fonts.setFormat(field, Fonts.bodyFormat);
			
			field.text = _scene.title;
			StringUtil.truncateContent(field);
			
			mouseChildren = false;
			
			if(_scene.thumbName)
				ImageLoader.load(
					model.config.getAssetUrl(_scene.thumbName), 
					onThumbFileLoaded,
					onThumbFileLoadError
					
				);
		}
		
		private function onThumbFileLoaded(data:BitmapData):void
		{
			logoHolder.bitmapData = data;
			var scale:Number = Math.max(
				(thumbLocator.width)/data.width, 
				(thumbLocator.height)/data.height);
			
			logoHolder.scaleX = scale;
			logoHolder.scaleY = scale;
		}
		private function onThumbFileLoadError():void
		{
			trace("onThumbFileLoadError");
		}
		
		public function get scene():SceneData
		{
			return _scene;
		}
		
		public function dispose():void
		{
			model = null;
		}
	}
}