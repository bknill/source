package co.beek.header
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import co.beek.Fonts;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	
	public class EmailWindow
	{
		private var mc:EmailPanelFl;
		private var model:Model;
		public function EmailWindow(mc:EmailPanelFl, model:Model)
		{
			this.mc = mc;
			this.model = model;			
			mc.buttonSend.addEventListener(MouseEvent.CLICK,onSendButtonClick);
			mc.visible = false;
			
			Fonts.setFormat(mc.inputSendTo, Fonts.bodyFormat);
			Fonts.setFormat(mc.inputEmailError, Fonts.bodyFormat);
			Fonts.setFormat(mc.inputEmailContent, Fonts.bodyFormat);
			Fonts.setFormat(mc.emailMessageStatus, Fonts.bodyFormat);
		}
		
		private function onSendButtonClick(event:MouseEvent):void
		{
			setupEmail();		
		}	
		
		private function setupEmail():void
		{
			clearEmailErrors();
			
			if(isValidEmail(mc.inputEmailContent.text)){
				mc.inputEmailError.text = "invalid address";
				mc.emailMessageStatus.textColor = 0xFF0000; 
				mc.emailMessageStatus.text = "Message not sent";
			}
			
			else if(mc.inputSendTo.text != "")		{	
				loadEmailObject();
			}
			else{
				mc.emailMessageStatus.textColor = 0xFF0000; 
				mc.emailMessageStatus.text = "Message not sent"; 
			}
		}
		
		private function clearEmailErrors():void
		{
			mc.inputEmailError.text = "";
			mc.emailMessageStatus.text = "";
			mc.inputEmailError.textColor = 0xFF0000;
		}	
		
		private function isValidEmail(email:String):Boolean {
			var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test(email);
		}
		
		private  function checkInvalidEmail(email:String):Boolean 
		{
			var emailExpression:RegExp = /^[a-z]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			return emailExpression.test(email);
		}	
		
		private function loadEmailObject():void
		{
			clearEmailErrors();
			sendEmail();
			restoreEmailFields();
			mc.emailMessageStatus.textColor = 0x24EE3F; // green
			mc.emailMessageStatus.text = "message sent";
		}		
		
		private function sendEmail():void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"email",
				mc.inputSendTo.text+" - "+ url);
			
			var url:String = model.config.serviceUrl+"/guides/share/"+model.guideData.id;	
			var variables:URLVariables = new URLVariables();		
			
			variables['emailAddress'] = mc.inputSendTo.text;   
			variables['emailContent'] = mc.inputEmailContent.text; 
			variables['guideUrl'] = Model.guideSceneUrl; 
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onSendEmailComplete);
			loader.load(request);
		}
		
		private function restoreEmailFields():void
		{
			mc.inputSendTo.text = ""
			mc.inputEmailContent.text = 
				"Hi,\n\n" +
				"I thought you might be interested in this interactive" +
				"\nguide.\n\n" +
				"\n\ncheers.";			
		}
		
		private function onSendEmailComplete(event:Event):void
		{
			URLLoader(event.target).removeEventListener(Event.COMPLETE, onSendEmailComplete);
			var json:String = URLLoader(event.target).data;
			var data:Object = JSONBeek.decode(json);
			var response:ServiceResponse = new ServiceResponse(data);
			
			if(response.error > 0)
				throw new Error(response.message);
		}
		
		public function set emailWindowVisibility(state:Boolean):void
		{
			mc.visible = state;
			if(state)
				resetWindow();
		}
		
		private function resetWindow():void
		{
			restoreEmailFields();
			clearEmailErrors();
		}
	}
}