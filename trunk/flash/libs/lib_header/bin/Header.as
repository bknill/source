package co.beek
{
	import caurina.transitions.Tweener;
	
	import co.beek.header.FacebookPanel;
	import co.beek.header.HistoryRenderer;
	import co.beek.header.IHeader;
	import co.beek.header.ShareWindow;
	import co.beek.header.VolumeSlider;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	
	import flash.display.MovieClip;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	/** Test commit */
	public class Header extends HeaderFl implements IHeader
	{
		private var _model:Model;	
		private var historyStartY:int;
		
		//header time out 
		private var closeTimeout:uint; 
		
		private static const OFFSET_RIGHT_BUTTONS:int = 140; 
		private var volumeSlider:VolumeSlider; 
		private var shareSlider:ShareWindow;
		private var facebookPanel:FacebookPanel;
		
		private var bgClass:HeaderBg;
		
		public function Header()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			init();
		}
		
		private function init():void
		{
			historyStartY = historyPanel.y;
			historyPanel.y = historyStartY - historyPanel.height;
			historyPanel.visible = false;
			
			backButton.addEventListener(MouseEvent.CLICK, onBackButtonClick);
			forwardButton.addEventListener(MouseEvent.CLICK, onForwardsButtonClick);
			homeButton.addEventListener(MouseEvent.CLICK, onHomeButtonClick);
			fullScreenButtonState(buttonsRight.fullscreenButton.fullscreen);
			
			
			buttonsRight.fullscreenButton.addEventListener(MouseEvent.CLICK, onFullscreenButtonClick);
			
			Fonts.setFormat(sceneTitleField, Fonts.titleFormat);
			sceneTitleField.mouseEnabled = false;
			
			bg.addEventListener(MouseEvent.CLICK, onBgClick);	
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			_model = new Model;
			volumeSlider = new VolumeSlider(buttonsRight.volumeSliderFl, _model);
			shareSlider = new ShareWindow(buttonsRight.shareButtonPanelsFl, _model);	
			facebookPanel = new FacebookPanel(buttonsRight.buttonFacebookLikeFl, _model);
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.state.addEventListener(State.UI_STATE_CHANGE, onUIStateChange);
			
		}
		
		private function onUIStateChange(event:Event):void
		{
			visible = !Model.loggedIn && Model.state.uiState != State.UI_STATE_PRESENTATION_MODE;
		}
		
		public override function get height():Number
		{
			return bg.height;
		}
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.F1){
				//				fileSystemButtons.visible = !fileSystemButtons.visible;
			}
		}
		
		public override function set width(value:Number):void
		{	
			bg.width = value;
			buttonsRight.x = value - OFFSET_RIGHT_BUTTONS ;
			
			//	fileSystemButtons.x = value - ( OFFSET_RIGHT_BUTTONS + 40 );// untill we unhide those other buttons
			historyPanel.bg.width = value;
			
			sceneTitleField.width = value - backButton.width - OFFSET_RIGHT_BUTTONS;
		}
		
		private function onSceneChange(event:Event):void
		{
			if(!_model.currentScene)
				return;
			
			var sceneTitle:String  = Model.guideScene 
				? Model.guideScene.titleMerged 
				: Model.currentScene.title;
			
			sceneTitleField.text = _model.currentScene.location.title+" - "+sceneTitle;
			
			backButton.mouseEnabled = _model.history.hasPrevious();
			backButton.alpha = backButton.mouseEnabled ? 1 : 0.5;
			
			homeButton.mouseEnabled = !_model.history.isHome();
			homeButton.alpha = homeButton.mouseEnabled ? 1 : 0.5;
			
			forwardButton.mouseEnabled = _model.history.hasNext();
			forwardButton.alpha = forwardButton.mouseEnabled ? 1 : 0.5;
			
			buildHistory();
		}
		
		private function buildHistory():void
		{
			while(historyPanel.renderersLocator.numChildren > 0)
				removeHistoryRenderer();
			
			var scenes:Vector.<SceneData> = _model.history.scenes;
			for(var i:int=0; i<scenes.length; i++)
				addHistoryRenderer(scenes[i]);
		}
		
		private function removeHistoryRenderer():void
		{
			var renderer:HistoryRenderer = HistoryRenderer(historyPanel.renderersLocator.getChildAt(0));
			renderer.removeEventListener(MouseEvent.CLICK, onRendererClick);
			renderer.dispose();
			
			historyPanel.renderersLocator.removeChild(renderer);
		}
		
		private function addHistoryRenderer(scene:SceneData):void
		{
			var renderer:HistoryRenderer = new HistoryRenderer(_model, scene);
			renderer.x = int(historyPanel.renderersLocator.width) + 5;
			renderer.addEventListener(MouseEvent.CLICK, onRendererClick);
			
			historyPanel.renderersLocator.addChild(renderer);
		}
		
		private function onRendererClick(event:MouseEvent):void
		{
			var renderer:HistoryRenderer = HistoryRenderer(event.target);
			
			_model.currentScene = renderer.scene;
			
			startCloseTimer();
		}
		
		
		private function onBackButtonClick(event:MouseEvent):void
		{
			Model.state.loadingScene = _model.history.getPrevious();
			openHistoryPanel();
		}
		
		private function onHomeButtonClick(event:MouseEvent):void
		{
			// if you are not viwing the configured guide, load that guide again.
			if(Model.config.guideId != Model.state.guideId)
				Model.state.guideId = Model.config.guideId;
			
			else
				Model.state.loadingScene = _model.history.getHome();
			openHistoryPanel();
		}
		
		private function onForwardsButtonClick(event:MouseEvent):void
		{
			Model.state.loadingScene = _model.history.getNext();
			openHistoryPanel();
		}
		
		private function openHistoryPanel():void
		{
			if(historyPanel.visible && historyPanel.y == historyStartY)
				return;
			
			historyPanel.visible = true;
			Tweener.addTween(historyPanel, {time:1, y:historyStartY});
			
			startCloseTimer();
		}
		
		private function startCloseTimer():void
		{
			clearTimeout(closeTimeout);
			
			closeTimeout = setTimeout(closeHistoryPanel, 4000);
		}
		
		private function closeHistoryPanel():void
		{
			var hiddenY:Number = historyStartY - historyPanel.height;
			Tweener.addTween(historyPanel, {time:1, y:hiddenY, onComplete:onHistoryCloseComplete});
		}
		
		private function onHistoryCloseComplete():void
		{
			historyPanel.visible = false;
		}
		
		private function onBgClick(event:MouseEvent):void
		{
			openHistoryPanel();
		}
		/** 
		 1 = minimizeDown  , 2 = minimize, 3 = fullScreenDown, 4 = fullscreen
		 */
		private function hideScreenButttons():void
		{
			buttonsRight.fullscreenButton.minimize.visible = false;
			buttonsRight.fullscreenButton.fullscreen.visible = false;
			buttonsRight.fullscreenButton.minimizeDown.visible = false;
			buttonsRight.fullscreenButton.fullScreenDown.visible = false;
			
		}
		
		private function fullScreenButtonState(button:MovieClip):void
		{
			hideScreenButttons();
			button.visible = true;
		}
		
		private function onFullscreenButtonClick(event:MouseEvent):void
		{
			if (stage.displayState == StageDisplayState.NORMAL) {
				stage.displayState=StageDisplayState.FULL_SCREEN;
				fullScreenButtonState(buttonsRight.fullscreenButton.minimize);
				
				Model.dispatchTrackingEvent(
					Constants.GA_CATEGORY_FUNCTIONS,
						"open Full Screen",
						_model.currentScene.id);
			} else {
				stage.displayState=StageDisplayState.NORMAL;
				fullScreenButtonState(buttonsRight.fullscreenButton.fullscreen);
				Model.dispatchTrackingEvent(
					Constants.GA_CATEGORY_FUNCTIONS,
						"close Full Screen", 
						_model.currentScene.id);
			}
		}	
		
	}
}