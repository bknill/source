package co.beek.header
{
	import assets.text.UrlWindowFL;
	
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	public class UrlWindow
	{
		private var mc:UrlWindowFL;
		private var model:Model;
		private	var thumb:Bitmap = new Bitmap();
		private var clipboardTimeout:uint;
		
		public function UrlWindow(mc:UrlWindowFL,model:Model)
		{
			this.mc = mc;
			this.model = model;
			
			mc.visible = false;
			// copy to clipboard
			mc.buttonUrl.addEventListener(MouseEvent.CLICK, onButtonUrlClick);
			mc.buttonEmbed.addEventListener(MouseEvent.CLICK, onButtonEmbedClick);
			mc.buttonThumbnail.addEventListener(MouseEvent.CLICK, onThumbnailButtonClick);
			
			Fonts.setFormat(mc.urlField, Fonts.bodyFormat);
			Fonts.setFormat(mc.embedlField, Fonts.bodyFormat);
			Fonts.setFormat(mc.textareaThunbnail, Fonts.bodyFormat);

		}
		
		private function setCurrentShareFields():void
		{
			var thumbUrl:String = model.config.getAssetUrl(model.currentScene.thumbName);
			var guideSceneUrl:String = Model.guideSceneUrl
			// url 
			mc.urlField.text = guideSceneUrl; 
			// embed 
			mc.embedlField.text = '<iframe width="480" height="800" src="'+guideSceneUrl+'"></iframe>';
			// tumbnail 
			mc.textareaThunbnail.text = 
				'<a href="'+guideSceneUrl+'" target="_blank">'
				+' <img src="'+thumbUrl+'" width="165" height="135" /></a>' ;	
		}
		private function loadThumbNail():void
		{
			thumb.smoothing = true;
			thumb.x = mc.thumbnailPreview.x;
			thumb.y = mc.thumbnailPreview.y;
			thumb.mask = mc.thumbnailPreview;
			mc.addChild(thumb);
			
			if(model.currentScene.thumbName)
				ImageLoader.load(
					model.config.getAssetUrl(model.currentScene.thumbName), 
					onThumbLoaded,
					onThumbLoadError
				); 
		}
		
		
		private function onThumbLoadError():void
		{
			trace("onThumbLoadError");
		}
		
		private function onThumbLoaded(data:BitmapData):void 
		{
			thumb.bitmapData = data;
			var scale:Number = Math.max(
				(mc.thumbnailPreview.width)/data.width, 
				(mc.thumbnailPreview.height)/data.height);
			
			thumb.scaleX = scale;
			thumb.scaleY = scale;		
			mc.addChild(thumb);		
		}
		
		private function onThumbnailButtonClick(event:MouseEvent):void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"thumbnail", 
				Model.guideSceneUrl);
			
			// We get the raw url, that won't be altered by different configs for offline
			var thumbUrl:String = model.config.assetCdn+"/"+model.currentScene.thumbName;
			changeTextFieldColor(mc.textareaThunbnail);
			mc.textareaThunbnail.text = 
				'<a href="'+Model.guideSceneUrl+'" target="_blank">'
				+'<img src="'+ thumbUrl+'" width="165" height="135" /></a>';				
			System.setClipboard(mc.textareaThunbnail.text); 
		}
		
		private function onButtonUrlClick(event:MouseEvent):void
		{
			changeTextFieldColor(mc.urlField);
			mc.urlField.text = Model.guideSceneUrl; 
			System.setClipboard(mc.urlField.text);   
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"link",
				Model.guideSceneUrl);
			
			
		}
		
		private function onButtonEmbedClick(event:MouseEvent):void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"embed", 
				Model.guideSceneUrl);
			
			changeTextFieldColor(mc.embedlField);
			mc.embedlField.text = '<iframe width="800" height="600" src="'+Model.guideSceneUrl+'"></iframe>';
			System.setClipboard(mc.embedlField.text);
		}
		
		private function changeTextFieldColor(field:TextField):void
		{
			// reset fields in case user clicks buttons in quick succession
			// only the last field is in the clipboard.
			resetTextFieldsColour();
			
			// clear a timeout in case the previous timeout was not done.
			clearTimeout(clipboardTimeout);
			
			// sets the fields color for 1 second, then resets it
			field.textColor = 0x909090;			
			clipboardTimeout = setTimeout(resetTextFieldsColour,1000);
		}
		/**
		 * Resets all the textfields to the un-interacted color
		 */
		private function resetTextFieldsColour():void
		{
			mc.urlField.textColor = 0xffffff;		
			mc.embedlField.textColor = 0xffffff;
			mc.textareaThunbnail.textColor = 0xffffff;
		}
		public function set urlWindowVisibility(state:Boolean):void
		{
			mc.visible = state;
			if(state)
				resetUrlWindow();
		}
		private function resetUrlWindow():void
		{
			setCurrentShareFields();
			loadThumbNail();
			resetTextFieldsColour();
		}
	}
}