package co.beek.header
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	public class VolumeSlider
	{
		private var mc:VolumeSliderFl;
		private var model:Model;
		// sound time out 
		private var soundPanelTimeout:uint;
		private var soundButtonTimeout:uint;
		
		//volumePanel
		private var panelIsOpen:Boolean = false; 
		
		private var mouseDownY:int
		
		public function VolumeSlider(mc:VolumeSliderFl, model:Model)
		{
			this.mc = mc;
			this.model = model;
			
			model.state.addEventListener(State.VOLUME_CHANGE,onVolumeChange);
			
			mc.buttonSound.addEventListener(MouseEvent.CLICK, onSoundButtonClick);
			mc.panel.slider.mouseEnabled = false;
			//mc.panel.track.mouseEnabled = false;
			mc.panel.visible = false; 
			hideVolumeButtons();
			mc.buttonSound.soundButton.visible = true;
			
			// mouse move
			mc.panel.hitarea.addEventListener(MouseEvent.MOUSE_UP, onHitareaUp);
			mc.panel.hitarea.addEventListener(MouseEvent.MOUSE_DOWN, onHitareaDown);	
			mc.panel.hitarea.addEventListener(MouseEvent.MOUSE_OVER, onHitareaOver);
			
			// stops the slider from moving to mouse 
			// Update the slider to the current values in the model
			sliderUpdate();	
		}	
		
		private function onHitareaMove(event:MouseEvent):void
		{
			updateFromHitareaPosition();	
		} 
		
		private var firstClicked:Boolean;
		
		private function onHitareaDown(event:MouseEvent):void
		{
			mc.panel.hitarea.addEventListener(MouseEvent.MOUSE_MOVE, onHitareaMove);
			mc.panel.hitarea.addEventListener(MouseEvent.MOUSE_OUT, onHitareaOut);
			updateWhenMouseMoves();
			displaySoundButtonDown();
			mouseDownY = event.localY;
			
			
		}
		
		private function onHitareaUp(event:MouseEvent):void
		{
			displaySoundButton();
			compareMousePosition(event.localY);
			mc.panel.hitarea.removeEventListener(MouseEvent.MOUSE_MOVE, onHitareaMove);
			mc.panel.hitarea.removeEventListener(MouseEvent.MOUSE_OUT, onHitareaOut);
		}
		
		
		private function onHitareaOut(event:MouseEvent):void
		{
			displaySoundButton();	
			compareMousePosition(event.localY);
			mc.panel.hitarea.removeEventListener(MouseEvent.MOUSE_MOVE, onHitareaMove);
			mc.panel.hitarea.removeEventListener(MouseEvent.MOUSE_OUT, onHitareaOut);
			startPanelTimer(2000);
		}
		
		private function compareMousePosition(mouseUpY:int):void
		{	
			if((mouseDownY - mouseUpY)>0)
				Model.dispatchTrackingEvent(
					Constants.GA_CATEGORY_FUNCTIONS,
					"volume",
					"volume up"+ model.currentScene.id);
			
			else if((mouseDownY - mouseUpY)<0)
				Model.dispatchTrackingEvent(
					Constants.GA_CATEGORY_FUNCTIONS,
					"volume",
					"volume down|"+ model.currentScene.id);
		
			// else no change 
		}

		private function onHitareaOver(event:MouseEvent):void
		{
			clearTimeout(soundPanelTimeout);
		}
		
		
		private function onSoundButtonClick(event:MouseEvent):void
		{	
			
			if(!firstClicked)
			{
				firstClicked = true;
				
				model.state.mute = true;
				mc.panel.slider.alpha = 0.5;
			}
			
			openSoundPanel();
		}
		
		private function openSoundPanel():void
		{
			//clear 
			clearTimeout(soundButtonTimeout);
			displaySoundButtonDown();	 
			//start mouse out
			soundButtonTimeout = setTimeout(soundButtonTimer, 200);
			
			if( panelIsOpen == false ){
				mc.panel.alpha = 1 ; 
				mc.panel.visible = true;
				onSliderClick();
			}
			else{
				closeSoundPanel();
			}
		}
		
		private function soundButtonTimer():void
		{
			displaySoundButton();
		}
		
		private function onSliderClick():void
		{	
			panelIsOpen = true; 
			sliderUpdate();		
			startPanelTimer(4000);
			displaySoundButton();
		}
		
		// updates from the model  
		private function onVolumeChange(event:Event):void
		{
			sliderUpdate();
		}
		
		private function updateWhenMouseMoves():void
		{
			updateFromHitareaPosition();
		}
		
		private function volumeButtonUpdate():void
		{
			// update volume button state.
			displaySoundButton();
		}
		
		private function startPanelTimer(time:int):void
		{
			clearTimeout(soundPanelTimeout);
			soundPanelTimeout = setTimeout(closeSoundPanel,time);
		}
		
		private function closeSoundPanel():void
		{
			clearTimeout(soundPanelTimeout);
			panelIsOpen = false; 
			Tweener.addTween(mc.panel,
				{
					alpha:0,
					time:0.2,
					transition:"easeOutQuad",
					onComplete:hidePanel
				});
		}	
		
		private function hidePanel():void
		{
			mc.panel.visible = false; 
		}
		
		private function updateFromHitareaPosition():void
		{
			// local 
			var mousePos:Point = new Point(mc.panel.hitarea.mouseX, mc.panel.hitarea.mouseY);		
			var globalPos:Point = mc.panel.hitarea.localToGlobal(mousePos);
			var trackPos:Point = mc.panel.track.globalToLocal(globalPos);
			
			var volume:Number = trackPos.y/mc.panel.track.height;
			
			// updates the model and corrects the volume
			model.state.volume = 1 - volume;
			// update volume button state.
			displaySoundButtonDown();
		}
		
		private function sliderUpdate():void
		{
			// sets the height of the slider to the fraction of the track height
			var sliderHeight:Number = mc.panel.track.height * model.state.volume;
			mc.panel.slider.height = Math.max(mc.panel.slider.width, sliderHeight);
			
			// Offsets the slider y to position it at the bottom of the track.
			mc.panel.slider.y = mc.panel.track.height - mc.panel.slider.height + mc.panel.track.y;
			mc.panel.slider.alpha = Model.state.mute ? 0.5 : 1;
		}
		
		private function onHitareaClick(event:MouseEvent):void
		{	
			// calculates the mouse in relation to the track
			var volume:Number =  (( event.localY)/ mc.panel.track.height ) ;
			
			// updates the model and corrects the volume
			model.state.volume = 1 - ( volume );
		}
		
		private function displaySoundButtonDown():void
		{
			if(model.state.volume == 0)
				displayVolumeButton(mc.buttonSound.soundButtonMuteDown);
			else
				displayVolumeButton(mc.buttonSound.soundButtonDown);	 	
		}
		
		private function displaySoundButton():void
		{
			if(model.state.volume == 0)
				displayVolumeButton(mc.buttonSound.soundButtonMute);
			else
				displayVolumeButton(mc.buttonSound.soundButton);	 	
		}
		
		private function displayVolumeButton(button:MovieClip):void
		{
			hideVolumeButtons();
			button.visible = true;
		}
		
		private function hideVolumeButtons():void
		{
			mc.buttonSound.soundButton.visible = false;
			mc.buttonSound.soundButtonDown.visible = false; 
			mc.buttonSound.soundButtonMute.visible = false;
			mc.buttonSound.soundButtonMuteDown.visible = false;
		}
	}
}