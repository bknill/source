package co.beek.header
{
	import caurina.transitions.Tweener;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GuideData;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	
	public class ShareWindow 
	{
		private var mc:ShareButtonPanelsFl;
		private var model:Model;
		
		// time out 
		private var shareTimeout:uint;
		
		private var _scene:SceneData;
		
		// share panel 
		private var panelShareIsOpen:Boolean = false; 
		private var panelEmailIsOpen:Boolean = false; 
		private var mouseCatcherIsOn:Boolean = false; 
		
		private var urlWindow:UrlWindow;
		private var emailWindow:EmailWindow;
		
		// catcher
		private var mouseCatcher:Sprite; 
		
		public function ShareWindow(mc:ShareButtonPanelsFl, model:Model)
		{
			this.mc = mc;
			this.model = model;
			
			mc.emailPanel.visible = false;
			mc.sharePanel.visible = false; 
			mc.urlPanel.visible = false;
			
			//share button
			mc.buttonShare.addEventListener(MouseEvent.CLICK, onShareButtonClick);
			// sharing buttons 
			mc.sharePanel.buttonShareOnWebsite.addEventListener(MouseEvent.CLICK, onPanelWebButtonClick);	
			mc.sharePanel.buttonShareOnEmail.addEventListener(MouseEvent.CLICK, onPanelEmailButtonClick);	
			mc.sharePanel.buttonShareOnFaceBook.addEventListener(MouseEvent.CLICK, onPanelFacebookButtonClick);	
			mc.sharePanel.buttonShareOnTwitter.addEventListener(MouseEvent.CLICK, onPanelTwitterButtonClick);	
			
			
			mc.buttonShare.visible = false;
			
			urlWindow = new UrlWindow(mc.urlPanel,model);
			emailWindow = new EmailWindow(mc.emailPanel, model);
			
			if(model.guideData)
				showShareButton();
			else
				model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);					
		}		
		
		private function showShareButton():void
		{
			mc.buttonShare.visible = true;
		}
		
		private function onGuideChange(event:Event):void
		{
			trace("onGuideChange");
			showShareButton();
		}
		
		private function onPanelWebButtonClick(event:MouseEvent):void
		{
			shareWeblink();		
		}
		
		private function onPanelEmailButtonClick(event:MouseEvent):void
		{
			shareOnEmail();
		}
		
		private function onPanelFacebookButtonClick(event:MouseEvent):void
		{
			shareOnFaceBook();					
		}
		
		private function onPanelTwitterButtonClick(event:MouseEvent):void
		{
			shareOnTwitter();		
		}
		
		private function onShareButtonClick(event:MouseEvent):void
		{
			openSharePanel();			
		}		
		
		private function openSharePanel():void
		{
			if(panelShareIsOpen == false)
			{
				closeAllPanels();
				
				panelShareIsOpen = true;
				mc.sharePanel.alpha = 1; 
				mc.sharePanel.visible = true; 	
				startSharePanelTimer(4000);				
				
				if(mouseCatcherIsOn == false)
					addMouseCatcher();	
			}
			else
			{
				closeAllPanels();
				removeMouseCatcher();
			}
		}
		
		private function addMouseCatcher():void
		{				
			mouseCatcherIsOn = true; 
			mouseCatcher = new Sprite();
			mouseCatcher.graphics.beginFill(0x0000FF ,0); // blue
			mouseCatcher.graphics.drawRect(0 - mc.width , 0, mc.stage.width + mc.width , mc.stage.height + mc.height);
			mouseCatcher.addEventListener(MouseEvent.CLICK, onMouseCatcherClick);
			
			var localPoint:Point = new Point(mc.x,mc.y);			
			var newPoint:Point = new Point(mc.localToGlobal(localPoint).x,mc.localToGlobal(localPoint).y);
			
			mouseCatcher.x = -newPoint.x; 
			mouseCatcher.y = newPoint.y; 
			
			mc.addChildAt(mouseCatcher,0);
		}
		
		private function onMouseCatcherClick(event:MouseEvent):void
		{	
			removeMouseCatcher();
		}
		
		private function removeMouseCatcher():void
		{
			mouseCatcherIsOn = false; 
			mouseCatcher.removeEventListener(MouseEvent.CLICK, onMouseCatcherClick);		
			
			panelShareIsOpen = true;
			
			closeAllPanels();
			if(mouseCatcher.parent != null)
				mouseCatcher.parent.removeChild(mouseCatcher);
		}
		
		private function closeAllPanels():void
		{	
			if(panelShareIsOpen)
				closeSharePanel();
	
			emailWindow.emailWindowVisibility = false;
			urlWindow.urlWindowVisibility = false;
			
			panelShareIsOpen = false; 
			panelEmailIsOpen = false; 
			
			clearTimeout(shareTimeout);
		}
		
		// brings up a weblink
		private function shareWeblink():void
		{			
			clearTimeout(shareTimeout);
			panelShareIsOpen = false; 
			mc.sharePanel.visible = false;
			
			urlWindow.urlWindowVisibility = true;
			
		}	
		
		//sends to a person over email
		private function shareOnEmail():void
		{
			clearTimeout(shareTimeout);
			panelShareIsOpen = false; 
			
			mc.sharePanel.visible = false;
			// brings up the email panel
			emailWindow.emailWindowVisibility = true;
			
			urlWindow.urlWindowVisibility = false;
		
		}

		// embed on a facebook wall
		private function shareOnFaceBook():void
		{	
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"facebook",
				Model.guideSceneUrl);

			ExternalInterface.call("shareOnFaceBook", Model.guideSceneUrl);
		}
		
		// share twitter 
		private function shareOnTwitter():void
		{
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_SHARE,
				"twitter",
				Model.guideSceneUrl);

			var path:URLRequest = new URLRequest("http://twitter.com/home");
			path.method = URLRequestMethod.GET; 
			var tweetVariables:URLVariables = new URLVariables(); 
			tweetVariables.status = Model.guideSceneUrl+ "  @beekguide";
			path.data = tweetVariables;
			navigateToURL(path, "_blank");			
		}
		
		/** time out events */ 
		private function startSharePanelTimer(time:int):void
		{
			clearTimeout(shareTimeout);
			shareTimeout = setTimeout(closeSharePanel,time);
		}
		
		private  function closeSharePanel():void
		{
			clearTimeout(shareTimeout);
			panelShareIsOpen = false; 
			closeSharePanelTween();
		}
		
		private function closeSharePanelTween():void
		{
			Tweener.addTween(mc.sharePanel,
				{
					alpha:0,
					time:0.2,
					transition:"easeOutQuad",
					onComplete:sharePanelVisible
				});	
		}
		
		private function sharePanelVisible():void
		{ 
			mouseCatcherIsOn = false;
			mc.sharePanel.visible = false;
			
			if(mouseCatcher.parent != null)
				mouseCatcher.parent.removeChild(mouseCatcher);
		}
	}	
}