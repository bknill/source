package co.beek.header
{
	import co.beek.event.GuideSceneEvent;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideData;
	import co.beek.utils.ColorUtil;
	
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.setTimeout;


	public class HeaderButtons extends HeaderButtonsFl
	{
		public static const BOOKCASE_OPEN:String = "BOOKCASE_OPEN";
		public static const BACK:String = "BACK";
		public static const HOME:String = "HOME";
		
		public var embed:Boolean;
		private var appScale:Number = 1;
		
		public function HeaderButtons()
		{
			super();
			fullscreenButton.mouseChildren = false;
			fullscreenButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			fullscreenButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			fullscreenButton.addEventListener(MouseEvent.MOUSE_UP, onFullscreenButtonMouseUp);


			normalscreenButton.visible = false;
			normalscreenButton.mouseChildren = false;
			normalscreenButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			normalscreenButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			normalscreenButton.addEventListener(MouseEvent.MOUSE_UP, onNormalscreenButtonMouseUp);
			
			volumeControls.volumeButton.mouseChildren = false;
			volumeControls.muteButton.mouseChildren = false;
			
			volumeControls.volumeButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			volumeControls.volumeButton.addEventListener(MouseEvent.MOUSE_OVER, onVolumeButtonMouseOver);
			volumeControls.volumeButton.addEventListener(MouseEvent.MOUSE_UP, onVolumeButtonMouseUp);

			volumeControls.muteButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			volumeControls.muteButton.addEventListener(MouseEvent.MOUSE_UP, onMuteButtonMouseUp);
			
			volumeControls.panel.visible = false;
			volumeControls.panel.mouseChildren = false;
			volumeControls.panel.addEventListener(MouseEvent.MOUSE_DOWN, onVolumePanelMouseDown);
			
			volumeControls.dragarea.alpha = 0;
			volumeControls.dragarea.visible = false;
			volumeControls.dragarea.addEventListener(MouseEvent.MOUSE_MOVE, onDragareaMouseMove);
			volumeControls.dragarea.addEventListener(MouseEvent.MOUSE_UP, onDragareaMouseUp);
			
			//volumeControls.hitarea.addEventListener(MouseEvent.MOUSE_OVER, onVolumeHitareMouseOver);
			//volumeControls.hitarea.addEventListener(MouseEvent.MOUSE_UP, onVolumeHitareMouseUp);
			volumeControls.addEventListener(MouseEvent.ROLL_OUT, onVolumeControlsMouseOut);
			
				
			//linkButton.mouseChildren = false;
			linkButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			linkButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			linkButton.addEventListener(MouseEvent.MOUSE_UP, onLinkButtonMouseUp);
		
			
			linkButtonBox.visible = false;
			linkButtonBox.copyToClipboardButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			linkButtonBox.copyToClipboardButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			linkButtonBox.copyToClipboardButton.addEventListener(MouseEvent.MOUSE_UP, onCopyButtonMouseUp);
			
			
			volumeControls.volumeButton.Bg.alpha = 0.6;
			volumeControls.muteButton.Bg.alpha = 0.6;
			linkButton.Bg.alpha = 0.6;
			fullscreenButton.Bg.alpha = 0.6;
			normalscreenButton.Bg.alpha = 0.6;
			homeButton.Bg.alpha = 0.6;
			backButton.Bg.alpha = 0.6;
			
		
			
			backButton.mouseChildren = false;
			backButton.alpha = 0.5;
			homeButton.alpha = 0.5;
			
			if(Model.isPlayingGame)
			{
				backButton.visible = false;
				linkButton.visible = false;
				homeButton.visible = false;
			};
			
			
			updateToVolume();
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.addEventListener(Model.HOTSPOT_CHANGE, onHotspotChange);
			Model.addEventListener(Model.GAME_COMPLETE, onGameComplete);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE, onGuideViewChange);

		}
		
		private function onHotspotChange(event:Event):void
		{
			showUrlInField();
		}
		
		private function onGameComplete(event:Event):void
		{
			backButton.visible = true;
			linkButton.visible = true;
			homeButton.visible = true;
			volumeControls.visible = true;
			fullscreenButton.visible = true;
		}
		
		private function onSceneChange(event:Event):void
		{
			trace("Header.onSceneChange()");
			showUrlInField();

			if(Model.history.hasPrevious() && Model.config.guideId)
				enableBackHome()
				
			if(Model.history.isHome() && Model.config.guideId)
				disableBackHome()
				
			if(!Model.config.guideId || Model.isPlayingGame)
			{
				backButton.visible = false;
				linkButton.visible = false;
				homeButton.visible = false;
			}	
			
		}
		
		private function enableBackHome():void
		{
			trace("Header.enableBack()");
			backButton.alpha = 1;
			backButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			backButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			backButton.addEventListener(MouseEvent.MOUSE_UP, onBackButtonMouseUp);
			homeButton.alpha = 1;
			homeButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			homeButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			homeButton.addEventListener(MouseEvent.MOUSE_UP, onHomeButtonMouseUp);
		}
		
		private function disableBackHome():void
		{
			trace("Header.disableBack()");
			backButton.alpha = 0.5;
			backButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			backButton.removeEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			backButton.removeEventListener(MouseEvent.MOUSE_UP, onBackButtonMouseUp);
			homeButton.alpha = 0.5;
			homeButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			homeButton.removeEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			homeButton.removeEventListener(MouseEvent.MOUSE_UP, onHomeButtonMouseUp);
		}

		private function onGuideViewChange(event:Event):void
		{
			//visible = Model.state.guideView != State.GUIDE_VIEW_HIDDEN;
		}
		
		public function embedMode(value:Boolean):void
		{
			if(value == true){
				embed = true;
				homeButton.visible = false;
				linkButton.visible = false;
				backButton.visible = false;
			}
			if(value == false){
				embed = false;
				homeButton.visible = true;
				linkButton.visible = true;
				backButton.visible = true;
			}
		}
		
		public function set hideOtherGuidesButton(value:Boolean):void
		{
/*			if(value == true)
				{
					backButton.x =+ 30.70;
					linkButton.x =+ 30.70;
					//linkButton.linkButtonBox.x = linkButton.linkButtonBox.x - 30;
					helpButton.x + 30.70;
					//helpButton.helpButtonHelp.x = helpButton.helpButtonHelp - 30;
				}*/

		}
		
		private function onButtonMouseDown(event:MouseEvent):void
		{
			event.target.alpha = 0.5;
			
		}
		
		private function onButtonMouseOut(event:MouseEvent):void
		{
			event.target.alpha = 1;
			
		}
		
		private function onFullscreenButtonMouseUp(event:MouseEvent):void
		{
			if(fullscreenButton.alpha != 0.5)
				return;
			
			if(Capabilities.manufacturer.indexOf("Google Pepper") > -1)
			{
				ExternalInterface.call("fsToggle()");
			}
			else
			{
				stage.displayState = StageDisplayState.FULL_SCREEN;
			}

			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_FUNCTIONS,
				"open Full Screen",
				Model.currentScene.id);
			
			normalscreenButton.visible = true;
			fullscreenButton.visible = false;
			embedMode(false);
		}
		

		private function onNormalscreenButtonMouseUp(event:MouseEvent):void
		{
			if(normalscreenButton.alpha != 0.5)
				return;
			
			if(Capabilities.manufacturer.indexOf("Google Pepper") > -1)
			{
				ExternalInterface.call("fsToggle()");
			}
			else
			{
				stage.displayState=StageDisplayState.NORMAL;
			}
			
			Model.dispatchTrackingEvent(
				Constants.GA_CATEGORY_FUNCTIONS,
				"close Full Screen", 
				Model.currentScene.id);
			
			normalscreenButton.visible = false;
			fullscreenButton.visible = true;
			embedMode(false);
		}

		private function onBookcaseButtonMouseUp(event:MouseEvent):void
		{
			
			linkButtonBox.visible = false;
			
			dispatchEvent(new Event(BOOKCASE_OPEN));
		}
		
		private function onBackButtonMouseUp(event:MouseEvent):void
		{
				dispatchEvent(new Event(BACK));
				
		}
		
		private function onHomeButtonMouseUp(event:MouseEvent):void
		{
			dispatchEvent(new Event(HOME));

		}
		
		private function onLinkButtonMouseUp(event:MouseEvent):void
		{
			linkButtonBox.visible = true;
			linkButtonBox.urlField.text = Model.currentURL;
			linkButton.alpha = 1;
			
			
		}
		private function onCopyButtonMouseUp(event:MouseEvent):void
		{

			linkButtonBox.copyToClipboardButton.alpha = 1;
			trace("GlobalButtons.onCopyToClipboardClick()");
			System.setClipboard(Model.currentURL);
			linkButtonBox.urlField.text = "Url copied to clipbord";
			setTimeout(showUrlInField, 1000);

		}
		
		private function showUrlInField():void
		{
			linkButtonBox.urlField.text = Model.currentURL;
			linkButtonBox.visible = false;
			
		}

		private function onVolumeButtonMouseOver(event:MouseEvent):void
		{
			volumeControls.panel.visible = true;
			
			Model.state.mute = false;
			volumeControls.volumeButton.visible = false;
			volumeControls.muteButton.visible = true;
			linkButtonBox.visible = false;
		}

		private function onVolumeHitareMouseOver(event:MouseEvent):void
		{
			volumeControls.panel.visible = true;
			volumeControls.volumeButton.visible = false;
			volumeControls.muteButton.visible = true;
			linkButtonBox.visible = false;
		}

		public function resize(width:Number, height:Number):void
		{
			width = width;
			height = height;
			homeButton.x = width - homeButton.width*2.2;
			backButton.x = width - backButton.width;
			
		}
		
		
		public function mobileAppScale(Scale:Number):void
		{
			appScale = Scale;
			volumeControls.volumeButton.visible = false;
			volumeControls.muteButton.visible = false;
			linkButton.visible = false;
			fullscreenButton.visible = false;
			normalscreenButton.visible = false;
			homeButton.scaleX = appScale;
			homeButton.scaleY = appScale;
			backButton.width = homeButton.width;
			backButton.height = homeButton.height;
		}
		
		
		private function onVolumeHitareMouseUp(event:MouseEvent):void
		{
			if(volumeControls.panel.visible)
			{
				Model.state.mute = !Model.state.mute;
				volumeControls.panel.visible = false;
				updateToVolume();
			}
			else
			{
				volumeControls.volumeButton.visible = false;
				volumeControls.muteButton.visible = true;
				volumeControls.panel.visible = true;
			}
			
			linkButtonBox.visible = false;
		}
		
		private function onVolumeControlsMouseOut(event:MouseEvent):void
		{
			volumeControls.panel.visible = false;
			updateToVolume();
		}
		
		private function onVolumeButtonMouseUp(event:MouseEvent):void
		{
			if(volumeControls.volumeButton.alpha == 1)
				return;
			
			volumeControls.volumeButton.alpha = 1;
				
			volumeControls.panel.visible = true;
			
			Model.state.mute = false;
			updateToVolume();
		}

		private function onMuteButtonMouseUp(event:MouseEvent):void
		{
			if(Model.state.mute)
			{
				volumeControls.panel.visible = true;
				
				Model.state.mute = false;
				updateToVolume();
			}
			else
			{
				//volumeControls.visible = false;
				
				Model.state.mute = true;
			}
		}
		
		private function onVolumePanelMouseDown(event:MouseEvent):void
		{
			volumeControls.dragarea.visible = true;
			
			Model.state.mute = false;
			Model.state.volume = 1-volumeControls.panel.track.mouseY/volumeControls.panel.track.height;
			updateToVolume();
		}
		
		private function onDragareaMouseMove(event:MouseEvent):void
		{
			Model.state.volume = 1-volumeControls.panel.track.mouseY/volumeControls.panel.track.height;
			
			updateToVolume();
		}
		
		private function updateToVolume():void
		{
			volumeControls.volumeButton.visible = !Model.state.mute;
			volumeControls.muteButton.visible = Model.state.mute;
			
			volumeControls.panel.slider.height = Model.state.volume * volumeControls.panel.track.height;
			volumeControls.panel.slider.y = volumeControls.panel.track.y 
				+ volumeControls.panel.track.height
				- volumeControls.panel.slider.height;
		}
		
		private function onDragareaMouseUp(event:MouseEvent):void
		{
			volumeControls.dragarea.visible = false;
			volumeControls.panel.visible = false;
		}
		

		

	}
}