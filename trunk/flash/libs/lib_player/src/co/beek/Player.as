package co.beek
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.system.System;
	import flash.utils.setTimeout;
	
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.events.Stage3DEvent;
	
	import co.beek.guide.StarlingGuide;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.pano.Pano;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	

	
	public class Player extends Sprite implements IPlayer
	{
		public static const PANO_TILES_LOADED:String = "PANO_TILES_LOADED";
		
		private var _pano:Pano;
		private var _starling:Starling;
		
		
		private var dragging:Boolean;
		
		private var stage3DManager:Stage3DManager;
		private var stage3DProxy:Stage3DProxy;
		private var antiAlias:Number = 2;

		
		public function Player()
		{
			super();
					
		
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		
		
		private function onAddedToStage(event:Event):void	
		{
			Log.record("Player.onAddedToStage()");
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			// Define a new Stage3DManager for the Stage3D objects
			stage3DManager = Stage3DManager.getInstance(stage);
			
			// Create a new Stage3D proxy to contain the separate views
			stage3DProxy = stage3DManager.getFreeStage3DProxy();
			
			stage3DProxy.antiAlias = antiAlias;	
			stage3DProxy.width = stage.stageWidth;
			stage3DProxy.height = stage.stageHeight;
			stage3DProxy.viewPort.width = stage.stageWidth;
			stage3DProxy.viewPort.height = stage.stageHeight;
			stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onContextCreated);
		}
		
		private function onContextCreated(e:Event):void
		{
			Log.record("player.onContextCreated()");
			stage3DProxy.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			
			createPano();
			//createGuide();
		}
		
		private function createPano(e:Event = null):void
		{
			Log.record("player.createPano()");
			
			if(_pano)
				return;
			
			_pano = new Pano;
			
			addChildAt(_pano,0);
			
			_pano.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb);
			_pano.addEventListener(HotspotEvent.HOTSPOT_CLOSE_WEB, onHotspotCloseWeb);
			_pano.addEventListener(Pano.PANO_TILES_LOADED, onPanoTilesLoaded);
			_pano.addEventListener(Pano.PANO_DEEP_TILES_LOADED, onPanoDeepTilesLoaded);
			_pano.addEventListener(Pano.START_DRAG, onStartDrag);
			_pano.addEventListener(Pano.STOP_DRAG, onStopDrag);
			_pano.addEventListener(Pano.START_AUTOROTATE, onPanoStartAutorotate);
			_pano.addEventListener(Pano.STOP_AUTOROTATE, onPanoStopAutorotate);
			
			_pano.panoMouse(true);
			
			_pano.initializePano(stage3DProxy, stage.stageWidth, stage.stageHeight);
			
		}
		

		
		private function createGuide():void
		{
			//Starling.multitouchEnabled = true;
			_starling = new Starling(StarlingGuide, this.stage, stage3DProxy.viewPort, stage3DProxy.stage3D);
			_starling.supportHighResolutions = true;
			_starling.shareContext = true;
			
			_starling.start();
		}
		
		private function onEnterFrame(e:Event):void
		{
			if(_pano)
				_pano.onEnterFrame(e);
			if(_starling)
				_starling.nextFrame();
		}
		
		private function onStartDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStartDrag()");
			//guide.mobileResize(appScale);
			
			Model.state.dispatchEvent(new Event(State.PANO_ENGAGED));
			Model.state.mode = State.MODE_DRAG;
			dragging = true;
			
			//hideUI();
			if(!Model.state.active)
				Model.state.active = true;
		}
		
		private function onStopDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStopDrag()");
			//guide.mobileResize(appScale);
			dragging = false;
			
			//setTimeout(function():void{if(!dragging)showUI();},1500);
			
		}
		
		private function onPanoDeepTilesLoaded(event:Event):void 
		{
			Log.record("swf_mobile.onPanoDeepTilesLoaded()");
			
			flash.system.System.gc();
			flash.system.System.gc();
			
			Model.state.mode = State.MODE_AUTO;
		}

		
		private var _width:Number;
		
		public function resize(width:Number, height:Number):void
		{
			_width = width;
			
			if(_pano)
				_pano.resize(width, height);
		
		}
		
		/**
		 * A pano scene has completed loading
		 */
		private function onPanoTilesLoaded(event:Event):void 
		{
			trace("Player.onPanoTilesLoaded()");
			if(Model.guideData != null)
			{
/*				if(Model.state.guideView == State.GUIDE_VIEW_LOADING)
					setTimeout(updateToCurrentScene, 100);
					
				else*/
					updateToCurrentScene();
			}
			
			dispatchEvent(new Event(PANO_TILES_LOADED));
		}
		
		private function updateToCurrentScene():void
		{
			trace("player.updateToCurrentScene()" + Model.state.guideView);
			//if(Model.state.guideView != State.GUIDE_VIEW_SECTION)
			//	_guide.updateToCurrentScene()
					
			
		}

		private function onPanoHotspotsReady(event:Event):void
		{
			if(Model.config.hotspotId && Model.history.scenes.length == 1)
				Model.hotspot = Model.getHotspot(Model.config.hotspotId);
			
			_pano._interrupted = false;

			setTimeout(_pano.play,3000);
		}
		
		
		private function onPanoMouseDown(event:Event):void
		{
			Model.state.dispatchEvent(new Event(State.PANO_ENGAGED));
		}

		private function onPanoStartDrag(event:Event):void
		{
			trace("Player.onPanoStartDrag()");
			Model.state.active = true;

			Model.state.dispatchEvent(new Event(State.PANO_ENGAGED));
			Model.state.mode = State.MODE_DRAG;
			
			if(!Model.loggedIn || Model.guideData)
				dragging = true;
		}
		
		private function onPanoStopDrag(e:Event):void
		{
			dragging = false;
			
		//	if(Model.state.guideId)
				//_guide.onPanoMouseUp();
		}
		
		
		private function onPanoStartAutorotate(event:Event):void
		{
		
			Log.record("Player.onPanoStartAutorotate()");
			Model.state.active = true;
		}
		
		private function onPanoStopAutorotate(event:Event):void
		{
			Log.record("Player.onPanoStopAutorotate()");
			dragging = false;
		}
		
		private function onPanoPauseAutorotate(event:Event):void
		{
			Log.record("Player.onPanoPauseAutorotate()");
			dragging = false;
		}
		
		
		private var muteBeforeOpen:Boolean;
		
		
		private function onHotspotShowWeb(event:HotspotEvent):void
		{
			trace("Player.onHotspotShowWeb()");
			var data:HotspotData = event.hotspot;
			data.inWorldHeight = event.hotspot.inWorldHeight;
			data.inWorldWidth = event.hotspot.inWorldWidth;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_SHOW_WEB, data));	
		}
		
		private function onHotspotCloseWeb(event:HotspotEvent):void
		{
			var data:HotspotData = event.hotspot;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_CLOSE_WEB, data));	
		}

		public function get pano():IPano
		{
			return _pano;
		}




	}
}