package co.beek.event
{
	import flash.events.Event;
	
	import co.beek.model.guide.GuideSectionData;
	
	public class GuideSectionEvent extends Event
	{
		private var _section:GuideSectionData;
		
		public function GuideSectionEvent(type:String, section:GuideSectionData)
		{
			trace("GuideSectionEvent()");
			super(type, true, true);
			this._section = section;
		}
		
		public function get guideSection():GuideSectionData
		{
			return _section;
		}
	}
}