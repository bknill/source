package co.beek.event
{
	import co.beek.model.data.PanoLook;
	
	import flash.events.Event;
	
	public class PanoLookEvent extends Event
	{
		private var _look:PanoLook;
		
		public function PanoLookEvent(type:String, panoLook:PanoLook)
		{
			super(type, bubbles, cancelable);
			_look = panoLook;
		}
		
		public function get panoLook():PanoLook
		{
			return _look;
		}
	}
}