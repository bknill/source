package co.beek.event
{
	import flash.events.Event;
	
	public class GameEvent extends Event
	{
		private var _code:String;
		
		public function GameEvent(type:String, code:String)
		{
			super(type, bubbles, cancelable);
			_code = code;
		}
		
		public function get panoLook():String
		{
			return _code;
		}
	}
}