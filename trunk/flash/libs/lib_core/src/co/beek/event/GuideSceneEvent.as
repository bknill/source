package co.beek.event
{
	import flash.events.Event;
	
	import co.beek.model.guide.GuideSceneData;
	
	public class GuideSceneEvent extends Event
	{
		private var _scene:GuideSceneData;
		
		public function GuideSceneEvent(type:String, scene:GuideSceneData)
		{
			trace("GuideSceneEvent()");
			super(type, true, true);
			this._scene = scene;
		}
		
		public function get guideScene():GuideSceneData
		{
			return _scene;
		}
	}
}