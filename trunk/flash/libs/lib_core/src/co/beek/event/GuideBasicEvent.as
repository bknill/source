package co.beek.event
{
	import co.beek.model.guide.GuideBasicData;
	
	import flash.events.Event;
	
	public class GuideBasicEvent extends Event
	{
		private var _guideBasic:GuideBasicData;
		
		public function GuideBasicEvent(type:String, guideBasic:GuideBasicData)
		{
			super(type, bubbles, cancelable);
			_guideBasic = guideBasic;
		}
		
		public function get guideBasic():GuideBasicData
		{
			return _guideBasic;
		}
	}
}