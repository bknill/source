package co.beek.admin
{
	import flash.system.Capabilities;

	public class ErrorStack extends Error
	{
		public static function createError(message:String):String
		{
			if(Capabilities.isDebugger)
				return new Error(message).getStackTrace();
			
			return message;
		}

		public function ErrorStack(message:String)
		{
			super(createError(message));
		}
	}
}