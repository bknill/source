package co.beek.admin
{
	import flash.events.IEventDispatcher;
	
	import co.beek.IPlayer;
	
	public interface IAdmin extends IEventDispatcher
	{
		function set player(value:IPlayer):void;
		
		function showError(message:String):void;
		
		function resize(width:Number, height:Number):void
	}
}