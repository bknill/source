package co.beek.pano.event
{
	import flash.events.Event;
	
	public class PanoEvent extends Event
	{
		public static const LOAD_COMPLETE:String = "LOAD_COMPLETE";
		
		public static const DRAG_START:String = "DRAG_START";
		
		public static const DRAG_STOP:String = "DRAG_STOP";
		
		public function PanoEvent(type:String)
		{
			super(type);
		}
	}
}