package co.beek.pano
{
	import flash.display.DisplayObject;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Bounds
	{
		public static function getGlobalBounds(component:DisplayObject):Bounds
		{
			//var matrix:Matrix3D = component.transform.getRelativeMatrix3D(component.stage);
			var rect:Rectangle = component.getRect(component);
			var stageMatrix:Matrix3D = component.transform.getRelativeMatrix3D(component.stage);
			var bounds:Bounds = new Bounds(rect);
			bounds.localToGlobal(component);
			return bounds;
		}
		
		public var topLeft:Point;
		public var topRight:Point;
		public var bottomLeft:Point;
		public var bottomRight:Point;
		
		function Bounds(rect:Rectangle)
		{
			topLeft = rect.topLeft;
			topRight = new Point(rect.bottomRight.x, rect.topLeft.y);
			bottomLeft = new Point(rect.topLeft.x, rect.bottomRight.y);
			bottomRight = rect.bottomRight;
		}
		
		public function localToGlobal(coordinateSpace:DisplayObject):void
		{
			topLeft = coordinateSpace.localToGlobal(topLeft);
			topRight = coordinateSpace.localToGlobal(topRight);
			bottomLeft = coordinateSpace.localToGlobal(bottomLeft);
			bottomRight = coordinateSpace.localToGlobal(bottomRight);
		}

		public function globalToLocal(coordinateSpace:DisplayObject):void
		{
			topLeft = coordinateSpace.globalToLocal(topLeft);
			topRight = coordinateSpace.globalToLocal(topRight);
			bottomLeft = coordinateSpace.globalToLocal(bottomLeft);
			bottomRight = coordinateSpace.globalToLocal(bottomRight);
		}
		
		public function get medianRight():Point
		{
			return Point.interpolate(topRight, bottomRight, 0.5);
		}

		public function get bottomCenter():Point
		{
			return Point.interpolate(bottomLeft, bottomRight, 0.5);
		}

		public function get center():Point
		{
			return Point.interpolate(topLeft, bottomRight, 0.5);
		}
		
		public function grow(pixels:int):void
		{
			topLeft.x -= pixels;
			topLeft.y -= pixels;
			topRight.x += pixels;
			topRight.y -= pixels;

			bottomLeft.x -= pixels;
			bottomLeft.y += pixels;
			bottomRight.x += pixels;
			bottomRight.y += pixels;
		}
		
		public function toString():String
		{
			return topLeft.toString()+", "+topRight.toString()+", "+bottomLeft.toString()+", "+bottomRight.toString();
		}
	}
}