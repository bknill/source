package co.beek.pano.event
{
	import co.beek.model.hotspots.HotspotData;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class HotspotDownEvent extends Event
	{
		public static const HOTSPOT_DOWN:String = "HOTSPOT_DOWN";
		
		private var _hotspotData:HotspotData;
		
		private var _hotspotPos:Point;
		
		public function HotspotDownEvent(hotspot:HotspotData, hotspotPos:Point)
		{
			super(HOTSPOT_DOWN);
			
			_hotspotData = hotspot;
			_hotspotPos = hotspotPos;
		}
		
		public function get hotspotData():HotspotData
		{	return _hotspotData }

		public function get hotspotPos():Point
		{	return _hotspotPos }
	}
}