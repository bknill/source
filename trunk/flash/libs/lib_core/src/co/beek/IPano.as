package co.beek
{
	import co.beek.model.data.PanoAngle;
	import co.beek.model.data.PanoLook;
	
	import flash.display.BitmapData;
	import flash.events.IEventDispatcher;
	
	public interface IPano extends IEventDispatcher
	{
		function get bitmapData():BitmapData
			
		function queueSnapshot():void	
			
		//function set model(value:Model):void
		
		//function resize(width:Number, height:Number):void;
		
		//function resize(width:Number, height:Number):void;
		
		/** these functions are used in the admin */
		function getAngle(x:Number, y:Number):PanoAngle;

		//function getTransformedPos(pos:Vector3D):Vector3D;

		//function getHotspotBounds(data:HotspotData):Bounds;
		//function getHotspotTransform(data:HotspotData):Matrix3D;
		//function get projectionCenter():PerspectiveProjection;
		
		//function getThumb():BitmapData;

		//function set selectedHotspot(value:HotspotData):void;
		//function get selectedHotspot():HotspotData;
		
		function get panoLook():PanoLook;
		
		//function get mouseX():Number;

		//function get mouseY():Number;
		
	}
}