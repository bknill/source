package co.beek.user
{
	import co.beek.model.data.AbstractData;
	
	public class TeamData
	{
		private var _data:Object;
		
		public function TeamData(data:Object)
		{
			super();
			_data = data;
		}
		
		public function get name():String
		{	return _data['team'] }
	}
}