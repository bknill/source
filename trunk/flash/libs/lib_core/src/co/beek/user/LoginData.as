package co.beek.user
{
	import co.beek.model.data.AbstractData;
	
	public class LoginData extends AbstractData
	{
		private var _user:UserData;
		public function LoginData(data:Object)
		{
			super(data);
			
			if(data["user"] != null)
				_user = new UserData(data["user"]);
		}
		
		public function get sessionId():String
		{	return _data['sessionId'] }
	}
}