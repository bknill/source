package co.beek.user
{
	import co.beek.model.data.AbstractData;
	
	public class UserData
	{
		private var _data:Object;
		
		private var _teams:Vector.<String> = new Vector.<String>;
		
		public function UserData(data:Object)
		{
			_data = data;
			parseTeams(data['teams']);
		}
		
		private function parseTeams(data:Array):void
		{
			for(var i:int = 0; i < data.length; i++)
				_teams.push(new TeamData(data[i]).name);
		}
		
		public function get email():String
		{	return _data['email'] }
		
		public function isOnTeam(team:String):Boolean
		{
			return _teams.indexOf(team) > -1;
		}
		
		public function get teams():Vector.<String>
		{
			return _teams;
		}
	}
}