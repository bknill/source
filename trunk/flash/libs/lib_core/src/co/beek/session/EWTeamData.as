package co.beek.session
{
	import co.beek.model.data.AbstractData;
	
	public class EWTeamData
	{
		private var _data:Object;
		
		public function EWTeamData(data:Object)
		{
			_data = data;
		}
		
		public function get id():String
		{	return _data['id'] }

		public function get teams():String
		{	return _data['teams'] }
	}
}