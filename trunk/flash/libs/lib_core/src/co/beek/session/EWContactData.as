package co.beek.session
{
	
	
	public class EWContactData 
	{
		private var _data:Object;

		private var _guideIds:Vector.<String> = new Vector.<String>;

		private var _locationIds:Vector.<String> = new Vector.<String>;

		private var _groups:Vector.<EWGroupData> = new Vector.<EWGroupData>;
		
		//private var _type:Vector.<String> = new Vector.<String>;
		
		//public static const EMPLOYEE:String = "employees";
		
		public function EWContactData(data:Object)
		{
			_data = data;
			parseGuideIds(data['guideids']);
			parseLocationIds(data['locationids']);
			parseGroups(data['DAO_dao3_link']);
		}
		
		public function parseGuideIds(data:Array):void
		{

			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
				_guideIds.push(new String(data[i]));
		}

		public function parseLocationIds(data:Array):void
		{

			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
				_locationIds.push(new String(data[i]));
		}

		public function parseGroups(data:Array):void
		{
			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
				_groups.push(new EWGroupData(data[i]));
		}
		
/*		public function parseType(data:Array):void
		{
			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
				_type.push(new String(data[i]));
		}
		*/
		public function get id():String
		{	return _data['id'] }
		
		public function get fistName():String
		{	return _data['first_name'] }

		public function get lastName():String
		{	return _data['last_name'] }

		public function get fullName():String
		{	return fistName+" "+lastName }

		public function get token():String
		{	return _data['client_token'] }

		public function get guideIds():Vector.<String>
		{	return _guideIds }

		public function get locationIds():Vector.<String>
		{	return _locationIds }

		public function get groups():Vector.<EWGroupData>
		{	return _groups }
		
/*		public function get type():Vector.<String>
		{	return _type }
		*/
		public function isOnTeamx(teamId:String):Boolean
		{
			/*for(var i:int = 0; i < _teams.length; i++)
				if(_teams[i].teams == teamId)
					return true;*/
			
			return false;
		}

		public function isInGroup(group:String):Boolean
		{
			for(var i:int = 0; i < _groups.length; i++)
				if(_groups[i].title == group)
					return true;
			
			return false;
		}
		
/*		public function isType(type:String):Boolean
		{
			if(_type == EMPLOYEE)
				return true;
			
			return false;			
		}*/
		
		public function isAdmin():Boolean
		{
			return isInGroup(EWGroupData.GROUP_ADMIN);
		}
		
/*		public function isEmployee():Boolean
		{
			return isType(EWGroupData.GROUP_ADMIN);
		}*/
		

		public function isCustomer():Boolean
		{
			return isInGroup(EWGroupData.GROUP_CUSTOMER);
		}

		public function isPhotographer():Boolean
		{
			return isInGroup(EWGroupData.GROUP_PHOTOGRAPHER);
		}
		
		public function canEditLocation(id:String):Boolean
		{

			for(var i:int = 0; i < _locationIds.length; i++)
				if(_locationIds[i] == id)
					return true;

			return false;
		}

		public function canEditGuide(id:String):Boolean
		{
			for(var i:int = 0; i < _guideIds.length; i++)
				if(_guideIds[i] == id)
					return true;
			
			return false;
		}
	}
}