package co.beek.session
{
	
	
	public class SessionData
	{
		private var _data:Object;
		
		private var _contact:EWContactData;
		
		public function SessionData(data:Object)
		{
			trace('SessionData()');
			_data = data;
			_contact = new EWContactData(data["contact"]);
		}
		
		public function get sessionId():String
		{	return _data['sessionId'] }

		public function get contact():EWContactData
		{	return _contact }

	}
}