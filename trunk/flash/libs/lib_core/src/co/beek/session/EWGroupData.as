package co.beek.session
{
	import co.beek.model.data.AbstractData;
	
	public class EWGroupData
	{
		public static const GROUP_ADMIN:String = "Client Admin";
		public static const GROUP_CUSTOMER:String = "Customer";
		public static const GROUP_PHOTOGRAPHER:String = "Photographer";
		
		
		private var _data:Object;
		
		public function EWGroupData(data:Object)
		{
			_data = data;
		}
		
		public function get type():String
		{	return _data['type'] }
		
		public function get id():String
		{	return _data['id'] }
		
		public function get title():String
		{	return _data['f_group'] }
	}
}