package co.beek
{
	import co.beek.model.guide.GuideBasicData;

	public interface IBookcase
	{
		function set visible(value:Boolean):void;
		
		function get open():Boolean;
			
		function tweenToOpen():void;
		
		function tweenToClosed():void;
		
		function get offlineGuides():Vector.<GuideBasicData>;
			
		function set offlineGuides(value:Vector.<GuideBasicData>):void;
		
		function get allGuides():Vector.<GuideBasicData>;
		
		function get guide():GuideBasicData;
	}
}