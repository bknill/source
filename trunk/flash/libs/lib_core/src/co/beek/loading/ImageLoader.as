package co.beek.loading
{
	import co.beek.model.Model;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	
	public class ImageLoader extends Loader
	{
		public static function load(assetUrl:String, successCallback:Function = null, errorCallback:Function = null,checkPolicyFile:Boolean = false):void
		{
			new ImageLoader(assetUrl, successCallback, errorCallback,checkPolicyFile)
		}
		

		
		private var assetUrl:String;
		
		private var successCallback:Function;

		private var errorCallback:Function;
		
		public function ImageLoader(assetUrl:String, successCallback:Function, errorCallback:Function, checkPolicyFile:Boolean)
		{
			super();
			this.assetUrl = assetUrl;
			this.successCallback = successCallback;
			this.errorCallback = errorCallback;
			
			if(errorCallback == null)
				throw new Error("must have error callback");
			
			var lc:LoaderContext = new LoaderContext(checkPolicyFile, null, SecurityDomain.currentDomain);
			
			if(assetUrl)
			{
				contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onImageLoadError, false, 0, true);
				contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadComplete, false, 0, true);
				contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError, false, 0, true);
				
				if(!Model.isMobile && !Model.isDesktop)
					load(new URLRequest(assetUrl), lc)
				else
					load(new URLRequest(assetUrl));
			}
			else
			{
				trace("Asset url was null: "+assetUrl);
				errorCallback();
				clear();
			}
		}
		
		private function onSecurityError(event:SecurityErrorEvent):void
		{
			trace("ImageLoader.onImageLoadError() - "+assetUrl);
			errorCallback();
			clear();
		}
		
		private function onImageLoadError(event:IOErrorEvent):void
		{
			trace("ImageLoader.onImageLoadError() - "+assetUrl);
			errorCallback();
			clear();
		}
		
		private function onImageLoadComplete(event:Event):void
		{
			try
			{
				var bitmap:BitmapData = Bitmap(content).bitmapData;
			}
			catch(e:SecurityError)
			{
				trace("sandbox error");
				clear();
				return;
			}
			successCallback(bitmap);
			clear();
		}
		
		private function clear():void
		{
			successCallback = null;
			errorCallback = null;
			
			contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onImageLoadError);
			contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadComplete);
		}
	}
}