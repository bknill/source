package co.beek.loading
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	
	import co.beek.map.Geo;
	import co.beek.model.IConfig;
	import co.beek.model.data.SceneBasicData;
	import co.beek.utils.JSONBeek;
	
	public class ScenesLoader extends EventDispatcher
	{
		public static const LOADED:String = "LOADED";
		
		private static const pastLoaders:Dictionary = new Dictionary;
		
		public static function locationScenesLoader(locationId:String):ScenesLoader
		{
			return getLoader("scenes/all/"+locationId);
		}
		
		public static function nearbyScenesLoader(geo:Geo):ScenesLoader
		{
			return getLoader("scenes/nearby/"+geo.lat+"/"+geo.lon);
		}
		
		private static function getLoader(path:String):ScenesLoader
		{
			if(pastLoaders[path] == null)
				pastLoaders[path] = new ScenesLoader(path);
			
			return ScenesLoader(pastLoaders[path]);
		}
		
		private var path:String;
		
		private var loader:URLLoader;
		
		private var _scenes:Vector.<SceneBasicData>;
		
		public function ScenesLoader(path:String)
		{
			super();
			this.path = path;
		}
		
		public function load(config:IConfig):void
		{
			var sceneDataUrl:String = config.serviceUrl+"/"+path;
			if(loader == null)
			{
				loader = new URLLoader();
				loader.addEventListener(Event.COMPLETE, onSceneDataLoaded); 
				loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				loader.load(new URLRequest(sceneDataUrl));
			}
			else if(_scenes != null)
			{
				dispatchEvent(new Event(LOADED));
			}
		}
		
		/**
		 * Returns a Vector that is a duplicate (shallow clone) 
		 * of the original Vector, so the original is preserved.
		 */
		public function get scenes():Vector.<SceneBasicData>
		{
			return _scenes.concat();
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace("ScenesLoader.onIOError()");
		}
		
		private function onSceneDataLoaded(event:Event):void
		{
			trace("onSceneDataLoaded():"+path);
			var json:String = URLLoader(event.target).data;
			try
			{
				var data:Array = JSONBeek.decode(json);
			}
			catch(e:Error)
			{
				throw new Error("ScenesLoader.onSceneDataLoaded() Error parsing scenes data from json.");
			}
			
			_scenes = new Vector.<SceneBasicData>
			for(var i:int = 0; i < data.length; i++)
				_scenes.push(new SceneBasicData(data[i]));
			
			dispatchEvent(new Event(LOADED));
		}
	}
}