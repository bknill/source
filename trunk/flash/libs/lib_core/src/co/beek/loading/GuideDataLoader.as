package co.beek.loading
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	import co.beek.model.guide.GuideData;
	import co.beek.utils.JSONBeek;
	
	public class GuideDataLoader extends Loader
	{
		public static function load(url:String, callback:Function):void
		{
			new GuideDataLoader(url, callback);
		}
		
		private var callback:Function;
		
		private var url:String;
		
		public function GuideDataLoader(url:String, callback:Function)
		{
			super();
			this.callback = callback;
			this.url = url;
			
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onGuideDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.load(new URLRequest(url));
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			throw new Error("IO Error when loading guide data: "+url)
		}
		
		private function onGuideDataLoaded(event:Event):void
		{
			trace("GuideDataLoader.onGuideDataLoaded()");
			var json:String = URLLoader(event.target).data;
			//json = json.substring(json.indexOf("{"));
			try
			{
				//json = json.substring(json.indexOf("{"));
				var data:Object = JSONBeek.decode(json);
			}
			catch(e:Error)
			{
				throw new Error("Model.onGuideDataLoaded() Error parsing guide data from json.");
			}
			
			var guide:GuideData = new GuideData(data.payload);
			
			callback(guide);
			
			callback = null;
		}
	}
}