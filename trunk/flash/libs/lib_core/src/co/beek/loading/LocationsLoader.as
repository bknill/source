package co.beek.loading
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	
	import co.beek.map.Geo;
	import co.beek.model.IConfig;
	import co.beek.model.data.LocationData;
	import co.beek.utils.JSONBeek;
	
	public class LocationsLoader extends EventDispatcher
	{
		public static const LOADED:String = "LOADED";
		
		private static const pastLoaders:Dictionary = new Dictionary;
		
		public static function loadForDestination(destinationId:String):LocationsLoader
		{
			return getLoader("locations/destination/"+destinationId);
		}
		
		/*public static function loadWithin(topLeft:Geo, bottomRight:Geo):LocationsLoader
		{
			var coordinates:String = JSONBeek.encode([
				topLeft.lat, 
				topLeft.lon, 
				bottomRight.lat, 
				bottomRight.lon
			]);
			return getLoader("locations/all/"+escape(coordinates));
		}
		*/
		public static function loadNearby(geo:Geo):LocationsLoader
		{
			return getLoader("locations/nearby/"+geo.lat+"/"+geo.lon+"/");
		}
		
		private static function getLoader(path:String):LocationsLoader
		{
			if(pastLoaders[path] == null)
				pastLoaders[path] = new LocationsLoader(path);
			
			return LocationsLoader(pastLoaders[path]);
		}
		
		private var loader:URLLoader;
		
		private var path:String;
		
		private var _locations:Vector.<LocationData>;
		
		public function LocationsLoader(path:String)
		{
			super();
			this.path = path;
		}
		
		public function load(config:IConfig):void
		{
			var sceneDataUrl:String = config.serviceUrl+"/"+path;
			if(loader == null)
			{
				loader = new URLLoader();
				loader.addEventListener(Event.COMPLETE, onLocationDataLoaded); 
				loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				loader.load(new URLRequest(sceneDataUrl));
				
				
			}
			else if(_locations != null)
			{
				dispatchEvent(new Event(LOADED));
			}
		}
		
		/*public function loadWithin(topLeft:Geo, bottomRight:Geo):void
		{
			var coordinates:String = JSONBeek.encode([
				topLeft.lat, 
				topLeft.lon, 
				bottomRight.lat, 
				bottomRight.lon
			]);
			
			
			var locationDataUrl:String = config.serviceUrl+"/locations/all/" + escape(coordinates);
			loader.load(new URLRequest(locationDataUrl));
			
			trace(locationDataUrl);
		}
		
		public function loadForDestination(destinationId:String):void
		{
			var locationDataUrl:String = config.serviceUrl+"/locations/destination/" + destinationId;
			loader.load(new URLRequest(locationDataUrl));
		}*/
		
		/*public function loadNearby(geo:Geo):void
		{
			var sceneDataUrl:String = config.serviceUrl+"/locations/nearby/"+geo.lat+"/"+geo.lon;
			loader.load(new URLRequest(sceneDataUrl));
		}*/
		
		/**
		 * Returns a Vector that is a duplicate (shallow clone) 
		 * of the original Vector, so the original is preserved.
		 */
		public function get locations():Vector.<LocationData>
		{
			return _locations.concat();
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace("LocationsLoader.onIOError()");
		}
		
		private function onLocationDataLoaded(event:Event):void
		{
			trace("onLocationDataLoaded()");
			var json:String = URLLoader(event.target).data;
			var data:Object;
			try
			{
				data = JSONBeek.decode(json);
			}
			catch(e:Error)
			{
				throw new Error("Error decoding Bulk Locations JSON"+e.getStackTrace());
			}
			
			var payload:Array = data.payload;
			_locations = new Vector.<LocationData>;
			for(var i:int = 0; i < payload.length; i++)
				if(payload[i]['defaultScene'] != null)
					_locations.push(new LocationData(payload[i]));
			
			dispatchEvent(new Event(LOADED));
		}
	}
}