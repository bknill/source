package co.beek.loading
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import co.beek.model.IConfig;
	import co.beek.model.data.LocationData;
	import co.beek.utils.JSONBeek;
	
	public class LocationLoader extends Loader
	{
		private var locationId:String;
		private var callbacks:Vector.<Function> = new Vector.<Function>;
		
		private var _location:LocationData;
		
		public function LocationLoader(config:IConfig, locationId:String, callback:Function)
		{
			super();
			this.locationId = locationId;
			
			callbacks.push(callback);
			
			var locationDataUrl:String = config.serviceUrl+"/locations/data/"+locationId;
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onLocationDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			loader.load(new URLRequest(locationDataUrl));
		}
		
		public function addCallback(callback:Function):void
		{
			if(_location)
				callback(_location);
			
			else
				callbacks.push(callback);
		}
		
		public function get location():LocationData
		{
			return _location;
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace("onIOError()");
		}
		
		private function onLocationDataLoaded(event:Event):void
		{
			trace("onLocationDataLoaded()");
			var json:String = URLLoader(event.target).data;
			var data:Object = JSONBeek.decode(json);
			
			_location = new LocationData(data);
			
			for(var i:int = 0; i<callbacks.length; i++)
				callbacks[i](_location);
			
			callbacks = null;
		}
	}
}