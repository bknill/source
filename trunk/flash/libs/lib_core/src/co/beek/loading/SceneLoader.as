package co.beek.loading
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import co.beek.admin.ErrorStack;
	import co.beek.model.data.SceneData;
	import co.beek.utils.JSONBeek;
	
	public class SceneLoader extends Loader
	{
		/*public static function load(config:Config, sceneId:String, callback:Function):void
		{
			var sceneDataUrl:String = config.getSceneJsonUrl(sceneId);
			new SceneLoader(sceneDataUrl, callback);
		}*/

		public static function loadx(url:String, callback:Function):void
		{
			new SceneLoader(url, callback);
		}

		public static function parse(json:String):SceneData
		{
			try
			{
				json = json.substring(json.indexOf("{"));
				var data:Object = JSONBeek.decode(json);
			}
			catch(e:Error)
			{
				throw new Error("Model.onSceneDataLoaded() Error parsing scene data from json.");
			}
			
			return new SceneData(data);
		}
		
		private var callback:Function;
		
		public function SceneLoader(url:String, callback:Function)
		{
			super();
			trace("SceneLoader(url:"+url+", callback:"+callback+")")
			this.callback = callback;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onSceneDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			loader.load(new URLRequest(url));
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			throw ErrorStack.createError("IO Error when loading scene data")
		}
		
		private function onSceneDataLoaded(event:Event):void
		{
			trace("SceneLoader.onSceneDataLoaded()");
			
			var scene:SceneData = SceneLoader.parse(URLLoader(event.target).data);
			
			callback(scene);
			callback = null;
		}
	}
}