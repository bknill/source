package co.beek
{
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import co.beek.model.Constants;
	import co.beek.model.Model;

	public class Fonts
	{	
		public static const SANS:String = "_sans";
		
		public static var titleFont:String = SANS;
		
		public static var bodyFont:String = SANS;
		
		public static var boldFont:String = SANS;
		
		public static function format(field:TextField, font:String, text:String = null, underline:Boolean = false):void
		{
			var text:String = text ? text : field.text;

			field.embedFonts = font != SANS;
			
			if(font == '_sans'){
				font = 'arial';
				var s:Number = field.defaultTextFormat.size.valueOf();
				var size:String = s.toString();
			}
			
			field.defaultTextFormat = new TextFormat(
				font, 
				size, 
				field.textColor, 
				null, null, underline, null, null, null, null, null, null, 
				field.defaultTextFormat.leading
			);
			field.text = text;
		}
		
		
		
		public static function cleanHTML(htmlText:String, embed:Boolean = true, font:String = null, colour = null, size:String = null):String
		{

			if(!htmlText)
				return null;
			
			//if(!font)
				//font = Fonts.bodyFont;
				
				
				if(htmlText.indexOf("TEXTFORMAT") < 0)
					return '<FONT FACE="'+font+'">'+htmlText+'</FONT>';
			

			if(font)htmlText = htmlText.replace(/FACE\=\"([^"]+)"/gi, font == null ? '' : 'FACE="'+font+'"');
			if(colour)htmlText = htmlText.replace(/COLOR\=\"([^"]+)"/gi, 'COLOR="'+colour+'"');
			if(size)htmlText = htmlText.replace(/SIZE\=\"([^"]+)"/gi, size == null ? '' : 'SIZE="'+size+'"');
			htmlText = htmlText.replace(/<\/?U>/gi,  '');
			if(embed)
			{
				htmlText = htmlText.replace(/<B>/gi, '<FONT FACE="'+font+'">');
				htmlText = htmlText.replace(/<\/B>/gi, '</FONT>');
			}

			return htmlText;
		}
		
		public static function formatHTML(field:TextField, font:String = null, htmlText:String = null,applyCSS:Boolean = true, colour = null, size:String = null):void
		{
			
			field.htmlText = cleanHTML(htmlText, font, font, colour, size);
			field.embedFonts = font != SANS;

			var css:String = "";
			css	+= "a:link { color: #ffffff; text-decoration:underline}\r\n";
			css	+= "a:hover {color: #ffffff;  text-decoration:underline}\r\n";
			css	+= ".selected {color:#ffffff;  text-decoration:underline}";
			
		
			var styleSheet:StyleSheet = new StyleSheet();
				styleSheet.parseCSS(css);
				
				
			if(applyCSS)
				field.styleSheet = styleSheet;
		}
	}
}