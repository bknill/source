package co.beek
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import co.beek.model.Model;
	import co.beek.utils.JSONBeek;

	public class Log
	{
		private static var _actions:Vector.<String> = new Vector.<String>;
		
		public static function record(string:String):void
		{
			_actions.unshift(string);
			
			if(_actions.length > 40)
				_actions.pop();
			
		//	if(Model.config && Model.config.domain && Model.config.domain == "beekdev.co")
				trace(string);
		}
		
		public static function get actions():Vector.<String>
		{
			return _actions;
		}
		
		public static function logError(message:String):void
		{
			try{
				logErrorToServer(message);
			}catch(e:Error){
				trace("!!!Error logging error!! " + message)
			}
		}
		
		private static var numLogged:int = 0;
		
		private static function logErrorToServer(message:String):void
		{
			if(numLogged > 5)
				return;
			
			numLogged ++;
			
			var info:Object = {
				"loggedIn" : Model.loggedIn,
				"guide" : Model.guideData ? Model.guideData.id : "",
				"currentScene" : Model.currentScene ? Model.currentScene.id : "",
				"currentHotspot" : Model.hotspot ? Model.hotspot.id : "",
				"memory" : System.totalMemory,
				"debugger" : Capabilities.isDebugger,
				"manufacturer" : Capabilities.manufacturer,
				"os" : Capabilities.os,
				"player" : Capabilities.playerType,
				"version" : Capabilities.version,
				"screenx" : Capabilities.screenResolutionX,
				"screeny" : Capabilities.screenResolutionY,
				"dpi" : Capabilities.screenDPI
			}
			
			var variables:URLVariables = new URLVariables();
			variables.message = message;
			variables.info = JSONBeek.encode(info);
			
			var url:String = Model.config.serviceUrl+"/clienterrors/logerror";
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onLogErrorIOError);
			loader.addEventListener(Event.COMPLETE, onLogErrorComplete);
			loader.load(request);
		}
		
		private static function onLogErrorIOError(event:IOErrorEvent):void
		{
			trace("onLogErrorIOError()");
		}
		
		private static function onLogErrorComplete(event:Event):void
		{
			trace("onLogErrorComplete()");
			trace(event.target.data)
			URLLoader(event.target).removeEventListener(Event.COMPLETE, onLogErrorComplete);
		}
	}
}