package co.beek.map
{
	import co.beek.utils.MathUtil;

	public class GeoZoom extends Geo 
	{
		public static function from(geo:Geo, zoom:int):GeoZoom
		{
			return new GeoZoom(geo.lat, geo.lon, zoom);
		}
		
		protected var _zoom:int;
		
		function GeoZoom(lat:Number, lon:Number, zoom:int)
		{
			super(lat, lon);
			_zoom = zoom;
		}
		
		public function get zoom():int
		{	return _zoom }

		public override function toString():String
		{
			return super.toString()+", "+_zoom
		}

		public override function equals(value:Geo):Boolean
		{
			return super.equals(value) 
				&& value is GeoZoom 
				&& _zoom == GeoZoom(value).zoom;
		}
	}
}