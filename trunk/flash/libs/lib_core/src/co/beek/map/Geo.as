package co.beek.map
{
	import co.beek.utils.MathUtil;

	public class Geo 
	{
		public static const EARTH_R:int = 6371;
		
		protected var _lat:Number;
		protected var _lon:Number;
		
		function Geo(lat:Number, lon:Number)
		{
			_lat = lat;
			_lon = lon;
		}
		
		public function get lat():Number
		{	return _lat }

		public function get lon():Number
		{	return _lon }
		
		public function toString():String
		{
			return _lat+", "+lon
		}

		public function equals(value:Geo):Boolean
		{
			return _lat == value.lat && _lon == value.lon;
		}
		
		
		public function distance(to:Geo):Number
		{
			if(lat == to.lat && lon == to.lon)
				return 0;
			
			var radlat1:Number = Math.PI * lat/180;
			var radlat2:Number = Math.PI * to.lat/180;
			var radlon1:Number = Math.PI * lon/180;
			var radlon2:Number = Math.PI * to.lon/180;
			var theta:Number = lon - to.lon;
			var radtheta:Number = Math.PI * theta/180;
			var dist:Number = Math.sin(radlat1) * Math.sin(radlat2) 
				+ Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI;
			dist = dist * 60 * 1.1515;
			return dist * 1.609344;
		}
		
		/**
		 * Returns the (initial) bearing from this point to the supplied point, in degrees
		 *   see http://williams.best.vwh.net/avform.htm#Crs
		 * 
		 * Found here: http://www.movable-type.co.uk/scripts/latlong.html
		 * 
		 * @param   {LatLon} point: Latitude/longitude of destination point
		 * @returns {Number} Initial bearing in degrees from North
		 */
		public function bearingTo(point:Geo):Number
		{
			var lat1:Number = MathUtil.toRad(_lat); 
			var lat2:Number = MathUtil.toRad(point._lat);
			var dLon:Number = MathUtil.toRad(point._lon-_lon);
			
			var y:Number = Math.sin(dLon) * Math.cos(lat2);
			var x:Number = Math.cos(lat1)*Math.sin(lat2) -
				Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
			var brng:Number = Math.atan2(y, x);
			
			return (MathUtil.toDeg(brng)+360) % 360;
		}
		
		public function minGeo(km:Number):Geo
		{
			var minLat:Number = _lat - MathUtil.toDeg(km/EARTH_R);
			var minLon:Number = _lon - MathUtil.toDeg(km/EARTH_R/Math.cos(MathUtil.toRad(lat)));
			return new Geo(minLat, minLon);
		}

		public function maxGeo(km:Number):Geo
		{
			var maxLat:Number = lat + MathUtil.toDeg(km/EARTH_R);
			var maxLon:Number = lon + MathUtil.toDeg(km/EARTH_R/Math.cos(MathUtil.toRad(lat)));
			return new Geo(maxLat, maxLon);
		}
	}
}