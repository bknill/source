package co.beek.map
{
	import flash.geom.Point;

	public class Functions
	{
		/**
		 * Returns the offset in pixels of the geo point from the tile origin
		 */
		public static function getOffsetPixels(tileSize:int, tile:Tile, geo:Geo, zoom:int):Point
		{
			var offsetGeo:Geo = getOffsetGeo(tile, geo, zoom);
			var pixelPerGeo:Geo = getPixelPerGeo(tileSize, tile, zoom);
			
			return new Point(
				offsetGeo.lon * pixelPerGeo.lon,
				offsetGeo.lat * pixelPerGeo.lat
			);
		}
		
		/**
		 * Returns the amount the geo changes each pixel of the passed in tile
		 * Divide the number of pixels by the change in geo accross the tile.
		 */
		public static function getGeoPerPixel(tileSize:int, tile:Tile, zoom:int):Geo
		{
			var tileDeltaGeo:Geo = getTileDeltaGeo(tile, zoom);
			return new Geo(
				tileDeltaGeo.lat/tileSize, 
				tileDeltaGeo.lon/tileSize
			);
		}

		public static function getPixelPerGeo(tileSize:int, tile:Tile, zoom:int):Geo
		{
			var tileDeltaGeo:Geo = getTileDeltaGeo(tile, zoom);
			return new Geo(
				tileSize/tileDeltaGeo.lat, 
				tileSize/tileDeltaGeo.lon
			);
		}
		
		/**
		 * Returns the amount the geo changes from top left to bottom right of a tile
		 */
		private static function getTileDeltaGeo(tile:Tile, zoom:int):Geo
		{
			var tileGeo:Geo = tileToGeo(tile, zoom);
			var tileOffsetGeo:Geo = tileToGeo(new Tile(tile.across+1, tile.down+1, zoom), zoom);
			return deltaGeo(tileGeo, tileOffsetGeo);
		}

		/**
		 * The amount the geo changes from the tile to the geo
		 */
		private static function getOffsetGeo(tile:Tile, geo:Geo, zoom:int):Geo
		{
			var tileGeo:Geo = tileToGeo(tile, zoom);
			return deltaGeo(tileGeo, geo);
		}
		
		/**
		 * Returns the delta in geo coordinates across the passed in geo
		 */
		private static function deltaGeo(geo1:Geo, geo2:Geo):Geo
		{
			return new Geo(geo1.lat - geo2.lat, geo1.lon - geo2.lon);
		}
		
		/**
		 * Returns the tile for the passed in geo
		 */
		public static function geoToTile(geo:Geo, zoom:int):Tile
		{
			return new Tile(geoToTileAcross(geo, zoom), geoToTileDown(geo, zoom), zoom);
		}
		
		/**
		 * Returns the tile num in x direction for the passed in geo
		 */
		private static function geoToTileAcross(geo:Geo, zoom:int):int
		{ 
			return (Math.floor((geo.lon+180)/360*Math.pow(2, zoom))); 
		}

		/**
		 * Returns the tile num in y direction for the passed in geo
		 */
		private static function geoToTileDown(geo:Geo, zoom:int):int
		{ 
			return Math.floor((1-Math.log(Math.tan(geo.lat*Math.PI/180) 
				+ 1/Math.cos(geo.lat*Math.PI/180))/Math.PI)/2 * Math.pow(2, zoom)); 
		}
		
		/**
		 *  Returns the geo coordinates of the passed in tile.
		**/
		public static function tileToGeo(tile:Tile, zoom:int):Geo
		{
			var n:Number = Math.pow(2, zoom);
			var lon:Number = tile.across / n * 360.0 - 180.0;
			var latRad:Number = Math.atan(sinh(Math.PI * (1 - 2 * tile.down / n)));
			var lat:Number = latRad * 180.0 / Math.PI;
			return new Geo(lat, lon);
		}
		
		/**
		 * AS3 implementation of the sinh function
		 */
		public static function sinh(a:Number):Number 
		{ 
			return (Math.exp(a) - Math.exp(-a)) / 2; 
		}
		
		public static function getDistance(geo1:Geo, geo2:Geo):Number
		{
			return (6371 * Math.acos(Math.sin(geo1.lat / 57.2958) * Math.sin(geo2.lat / 57.2958) + (Math.cos(geo1.lat / 57.2958) * Math.cos(geo2.lat / 57.2958) * Math.cos((geo2.lon / 57.2958) - (geo1.lon / 57.2958)))));
		}

		public static function getDistancePixels(geo1:Geo, geo2:Geo, zoom:Number, tileSize:int):Number
		{
			var tile:Tile = geoToTile(geo1, zoom);
			var pixelPerGeo:Geo = getPixelPerGeo(tileSize, tile, zoom);
			return getDistance(geo1, geo2) * pixelPerGeo.lat;
		}
		
		public static function getZoom(distance:Number, width:Number, height:Number):Number
		{
			return Math.floor(8 - Math.log(1.6446 * distance / Math.sqrt(2 * (width * height))) / Math.log(2));
		}

		
		public static function getRelativeTile(tile:Tile, tileSize:int, pos:Point):Tile
		{
			var t:Number = pos.x/tileSize;
			return new Tile(
				tile.across + Math.round(pos.x/tileSize), 
				tile.down + Math.round(pos.y/tileSize),
				tile.zoom
			);
		}
		
		public static function getTiles(tilesize:int, center:Geo, zoom:int, width:Number, height:Number):Vector.<Tile>
		{
			var centerTile:Tile = geoToTile(center, zoom);
			var centerTileOffset:Point = Functions.getOffsetPixels(tilesize, centerTile, center, zoom);
			
			var centerTileX:Number = (width/2 - centerTileOffset.x);
			var tilesBack:int = Math.ceil(centerTileX/tilesize);
			var originX:Number = centerTileX - (tilesBack * tilesize);
			var deltaX:Number = centerTileX - originX;
			var originTileAcross:int = centerTile.across - Math.ceil(centerTileX/tilesize);
			var tilesAcross:int =  Math.ceil((width-originX)/tilesize);
			
			var centerTiley:Number = (height/2 - centerTileOffset.y);
			var tilesUp:int = Math.ceil(centerTiley/tilesize);
			var originY:Number = centerTiley - (tilesUp * tilesize);
			var deltaY:Number = centerTiley - originY;
			var originTileDown:int = centerTile.down - tilesUp;
			var tilesDown:int = Math.ceil((height-originY)/tilesize);
			
			var tiles:Vector.<Tile> = new Vector.<Tile>;
			for(var a:int = originTileAcross; a<originTileAcross + tilesAcross; a++)
				for(var d:int = originTileDown; d<originTileDown + tilesDown; d++)
					tiles.push(new Tile(a, d, zoom));
			
			return tiles;
		}
	}
}