package co.beek.map
{
	/**
	 * With help from
	 * http://mapviewer.googlecode.com/hg/kcMapViewer.pas
	 */
	public class Tile 
	{
		public static const CLOUDMADE_TILE_SERVERS:Array = ["a", "b", "c"];
		
		public static const STYLE_GOOGLE_SATELLITE:String = "gsat";
		public static const STYLE_OVI_SATELLITE:String = "ovisat";
		
		public static const STYLE_DEFAULT_MAP:String =  "37964";
		
		protected var _across:int;
		protected var _down:int;
		protected var _zoom:int;
		
		function Tile(across:Number, down:Number, zoom:int)
		{
			_across = across;
			_down = down;
			_zoom = zoom;
		}
		
		public function get across():int
		{	return _across }

		public function get down():int
		{	return _down }

		public function get zoom():int
		{	return _zoom }
		
		public function getServer(style:String):String
		{
			return "http://api.tiles.mapbox.com/v4/bknill.hp29237j/";
		}
		
		public function getPath(key:String, style:String, size:int, zoom:int):String
		{
			return + zoom + "/" + _across + "/" + _down + ".png?access_token=pk.eyJ1IjoiYmtuaWxsIiwiYSI6Imh4dHNYbXcifQ.dSnZY6_OVlg3ryUCjPloiA"
		}
		
		public function toString(style:String):String
		{
			return style+", "+_across+", "+_down;
		}
	}
}