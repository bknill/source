package co.beek.map
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class MapData extends EventDispatcher
	{
		public static const CHANGE:String = "CHANGE";
		public static const PAN:String = "PAN";
		public static const ZOOM:String = "ZOOM";
		
		public static const API_KEY:String = "b3cd1de3306e477e9fc06e6161e787b9";
		//public static const MAP_STYLE:String =  "37964";
		public static const TILE_SIZE:int = 256;
		public static const MAX_ZOOM:int = 18;
		public static const DEFAULT_CENTER:Geo = new Geo(-41.283, 174.783);
		
		
		private var _style:String;
		private var _width:Number;
		private var _height:Number;
		
		private var _center:Geo;
		private var _centerTile:Tile;
		private var _centerTileOffset:Point;
		private var _zoom:int;
		
		function MapData(width:Number, height:Number, zoom:int, style:String)
		{
			_width = width;
			_height = height;
			_zoom = zoom;
			_style = style;
		}
		
		public function get style():String
		{
			return _style;
		}

		public function set style(value:String):void
		{
			_style = value;
		}
		
		public function get width():Number
		{
			return _width;
		}
		
		public function get height():Number
		{
			return _height;
		}
		
		public function set width(value:Number):void
		{
			_width = value;
			
			if(_center)
				dispatchEvent(new Event(CHANGE));
		}
		
		public function set height(value:Number):void
		{
			_height = value;
			if(_center)
				dispatchEvent(new Event(CHANGE));
		}
		
		public function set zoom(value:int):void
		{
			trace("MapData.set zoom(value:"+value+")")
			if(_zoom == value || value >= MAX_ZOOM)
				return;
			
			updateZoom(value);
			
			dispatchEvent(new Event(ZOOM));
		}
		
		private function updateZoom(zoom:int):void
		{
			_zoom = zoom;
			_centerTile = Functions.geoToTile(_center, _zoom);
			_centerTileOffset = Functions.getOffsetPixels(TILE_SIZE, centerTile, _center, _zoom);
		}
		
		public function get zoom():int
		{
			return _zoom;
		}
		
		public function set centerZoom(value:GeoZoom):void
		{
			if(_center != null && _center.equals(value))
				return;
			
			_zoom = value.zoom;
			updateCenter(value);
			
			dispatchEvent(new Event(ZOOM));
			dispatchEvent(new Event(PAN));
		}
		
		public function set center(value:Geo):void
		{
			if(_center != null && _center.equals(value))
				return;
			
			updateCenter(value);
			
			dispatchEvent(new Event(PAN));
		}
		
		public function updateCenter(value:Geo):void
		{
			_center = value;
			_centerTile = Functions.geoToTile(_center, _zoom);
			_centerTileOffset = Functions.getOffsetPixels(TILE_SIZE, centerTile, _center, _zoom);
			
		}
		
		public function get center():Geo
		{
			return _center;
		}
		
		public function get topLeft():Geo
		{
			return getGeoForPos(new Point(0, 0));
		}

		public function get bottomRight():Geo
		{
			return getGeoForPos(new Point(_height, _width));
		}
		
		/**
		 * The tile that is under the center point
		 */
		public function get centerTile():Tile
		{
			return _centerTile
		}
		
		/**
		 * The offset of the center on the center tile
		 */
		public function get centerTileOffset():Point
		{
			return _centerTileOffset;
		}
		
		private function getOffsetFromCenter(geo:Geo):Point
		{
			var offset:Point = centerTileOffset;
			var offsetFromCenter:Point = getOffsetFromCenterTile(geo);
			return new Point(
				offset.x - offsetFromCenter.x, 
				offset.y - offsetFromCenter.y
			);
		}

		public function offsetFromCenter(pos:Point):void
		{
			var geoPerPixel:Geo = Functions.getGeoPerPixel(TILE_SIZE, _centerTile, zoom);
			center = new Geo(
				_center.lat - geoPerPixel.lat * pos.y, 
				_center.lon - geoPerPixel.lon * pos.x
			);
		}

		public function centerAroundAll(group:Vector.<Geo>):void
		{
			
			
			if(group.length == 0)
				return;
			
			var topLeft:Geo = getTopLeft(group);
			var bottomRight:Geo = getBottomRight(group);
			
			updateCenter(new Geo(
				topLeft.lat + (bottomRight.lat - topLeft.lat)/2, 
				topLeft.lon + (bottomRight.lon - topLeft.lon)/2
			));
			
			var rect:Rectangle = new Rectangle(-width/2, -height/2, width, height);
			
			//if(rect.containsPoint(tl) && rect.containsPoint(br))
			//	return;
			
			// nasty solution to calculating zoom.
			// keep looping till the pos is within the rect.
			// zoom right in before looping
			updateZoom(20);
			var tl:Point = getOffsetFromCenter(topLeft);
			var br:Point = getOffsetFromCenter(bottomRight);
			while(!rect.containsPoint(tl) && !rect.containsPoint(br) && _zoom > 0)
			{
				updateZoom(zoom-1);
				tl = getOffsetFromCenter(topLeft);
				br = getOffsetFromCenter(bottomRight);
			}
			
			dispatchEvent(new Event(ZOOM));
			dispatchEvent(new Event(PAN));
		}
		
		
		private function getTopLeft(group:Vector.<Geo>):Geo
		{
			var br:Geo = group[0];
			for(var i:int =1; i < group.length; i++)
				br = new Geo(
					Math.min(br.lat, group[i].lat),
					Math.min(br.lon, group[i].lon)
				)
			return br
		}

		private function getBottomRight(group:Vector.<Geo>):Geo
		{
			var tl:Geo = group[0];
			for(var i:int =1; i < group.length; i++)
				tl = new Geo(
					Math.max(tl.lat, group[i].lat),
					Math.max(tl.lon, group[i].lon)
				)
			return tl
		}
		
		public function zoomToFit(geo:Geo):void
		{
			var pos:Point = getOffsetFromCenter(geo);
			var rect:Rectangle = new Rectangle(-_width/2, -_height/2, _width, _height);
			if(rect.containsPoint(pos))
				return;
			
			// nasty solution to calculating zoom.
			// keep looping till the pos is within the rect.
			while(!rect.containsPoint(pos) && _zoom > 0)
			{
				updateZoom(zoom-1);
				pos = getOffsetFromCenter(geo);
			}
			
			dispatchEvent(new Event(ZOOM));
		}
		
		private function getDistancePixels(geo:Geo):Number
		{
			var pixelsPerGeo:Geo = Functions.getPixelPerGeo(TILE_SIZE, _centerTile, zoom);
			var distance:Number = Functions.getDistance(_center, geo)
			return distance * pixelsPerGeo.lat;
		}
		
		private function getOffsetFromCenterTile(geo:Geo):Point
		{
			return Functions.getOffsetPixels(TILE_SIZE, centerTile, geo, _zoom);
		}

		private function getTileForPos(pos:Point):Tile
		{
			var relativeToCenter:Point = new Point(
				pos.x - centerTileOffset.x,
				pos.y - centerTileOffset.y
			);
			return Functions.getRelativeTile(centerTile, TILE_SIZE, relativeToCenter);
		}

		public function getGeoForPos(pos:Point):Geo
		{
			var geoPerPixel:Geo = Functions.getGeoPerPixel(TILE_SIZE, centerTile, _zoom);
			return new Geo(
				_center.lat + ((_height/2 - pos.y) * geoPerPixel.lat),
				_center.lon + ((_width/2 - pos.x) * geoPerPixel.lon)
			); 
		}
		
		public function posForGeo(geo:Geo):Point
		{
			var offset:Point = centerTileOffset;
			var offsetFromCenter:Point = getOffsetFromCenterTile(geo);
			return new Point(
				_width/2 - (offset.x - offsetFromCenter.x), 
				_height/2 - (offset.y - offsetFromCenter.y)
			);
		}
		
		private function get originTile():Tile
		{
			var centerTileX:Number = (width/2 - _centerTileOffset.x);
			var tilesBack:int = Math.ceil(centerTileX/TILE_SIZE);
			var originX:Number = centerTileX - (tilesBack * TILE_SIZE);
			var deltaX:Number = centerTileX - originX;
			var originTileAcross:int = centerTile.across - Math.ceil(centerTileX/TILE_SIZE);
			
			var centerTiley:Number = (height/2 - _centerTileOffset.y);
			var tilesUp:int = Math.ceil(centerTiley/TILE_SIZE);
			var originY:Number = centerTiley - (tilesUp * TILE_SIZE);
			var deltaY:Number = centerTiley - originY;
			var originTileDown:int = centerTile.down - tilesUp;
			
			return new Tile(originTileAcross, originTileDown, _centerTile.zoom)
		}

		private function getCenterTilePos():Point
		{
			return new Point(
				(_width/2 - _centerTileOffset.x),
				(_height/2 - _centerTileOffset.y)
			);
		}
		
		/*private function getOrigin():Point
		{
			var centerTile:Point = getCenterTilePos();
			return new Point(
				centerTile.x - (Math.ceil(centerTile.x/TILE_SIZE) * TILE_SIZE), 
				centerTile.y - (Math.ceil(centerTile.y/TILE_SIZE) * TILE_SIZE)
			)
		}*/
		
		private function get tilesAcross():int
		{
			var centerTileX:Number = (_width/2 - _centerTileOffset.x);
			var tilesBack:int = Math.ceil(centerTileX/TILE_SIZE);
			var originX:Number = centerTileX - (tilesBack * TILE_SIZE);
			var deltaX:Number = centerTileX - originX;
			var originTileAcross:int = centerTile.across - Math.ceil(centerTileX/TILE_SIZE);
			
			return Math.ceil((_width-originX)/TILE_SIZE);
		}
		
		private function get tilesDown():int
		{
			var centerTiley:Number = (height/2 - _centerTileOffset.y);
			var tilesUp:int = Math.ceil(centerTiley/TILE_SIZE);
			var originY:Number = centerTiley - (tilesUp * TILE_SIZE);
			var deltaY:Number = centerTiley - originY;
			var originTileDown:int = centerTile.down - tilesUp;
			return Math.ceil((height-originY)/TILE_SIZE);
		}
		
		public function get tiles():Vector.<Tile>
		{
			var origin:Tile = originTile;
			var across:int = tilesAcross;
			var down:int = tilesDown;
			
			var tiles:Vector.<Tile> = new Vector.<Tile>;
			for(var a:int = origin.across; a<origin.across + across; a++)
				for(var d:int = origin.down; d<origin.down + down; d++)
					tiles.push(new Tile(a, d, origin.zoom));
			
			return tiles;
		}
		
		public function getPos(tile:Tile):Point
		{
			return new Point(
				(tile.across - _centerTile.across) * TILE_SIZE - _centerTileOffset.x + _width/2,
				(tile.down - _centerTile.down) * TILE_SIZE - _centerTileOffset.y + _height/2
			);
		}
		
		public function get server():String
		{
					return "http://api.tiles.mapbox.com/v4/bknill.hp29237j";
			
		}
		
		public function getPath(tile:Tile):String
		{
			return tile.getPath(API_KEY, _style, TILE_SIZE, _zoom);
		}
		
		
		
	}
}