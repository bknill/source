package co.beek.game
{
	import co.beek.model.Model;
	
	public interface IGame
	{
		function set x(value:Number):void;
		function set visible(value:Boolean):void;
	}
}