package co.beek.header
{
	public interface IHeader
	{
		function set width(value:Number):void;
		
		function set visible(value:Boolean):void;
	}
}