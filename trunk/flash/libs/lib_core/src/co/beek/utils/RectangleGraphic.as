package co.beek.utils
{
	import flash.display.Sprite;
	
	public class RectangleGraphic extends Sprite
	{
		public function RectangleGraphic(width:Number, height:Number, color:uint = 0)
		{
			super();
			graphics.beginFill(color);
			graphics.drawRect(0, 0, width, height);
			graphics.endFill();
		}
	}
}