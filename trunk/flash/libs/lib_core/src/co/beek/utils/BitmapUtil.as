package co.beek.utils
{
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.engine.GraphicElement;

	public class BitmapUtil
	{
		/**
		 * We should be creating thumbs for the in scene view
		 */
		public static function createThumb(width:Number, height:Number, bitmap:BitmapData):BitmapData
		{
			var scale:Number = Math.min(1,
				width/bitmap.width, 
				height/bitmap.height
			);
			
			var scaleMatrix:Matrix = new Matrix();
			scaleMatrix.scale(scale, scale);
			
			var thumb:BitmapData = new BitmapData(
				scale * bitmap.width, 
				scale * bitmap.height, 
				true, 0x00000000
			)
			thumb.draw(bitmap, scaleMatrix, null, null, null, true);
			
			return thumb;
		}
		
		public static function drawAndScale9Grid(source:Sprite, target:Sprite) : void {
			var bitmapData:BitmapData = new BitmapData(source.width, source.height, true, 0x00000000);
			bitmapData.draw(source);
				
			var rect:Rectangle = source.scale9Grid;
			if(!rect)
			{
				target.graphics.beginBitmapFill(bitmapData);				
				target.graphics.drawRect(0, 0, bitmapData.width, bitmapData.height);
				target.graphics.endFill();
				return;
			}
			
			var gridX:Array = [rect.left, rect.right, bitmapData.width];
			var gridY:Array = [rect.top, rect.bottom, bitmapData.height];
			
			target.graphics.clear();
			
			var left:Number = 0;
			for (var i:int = 0; i < 3; i++) {
				var top:Number = 0;
				for (var j:int = 0; j < 3; j++) {
					target.graphics.beginBitmapFill(bitmapData);
					target.graphics.drawRect(left, top, gridX[i] - left, gridY[j] - top);
					target.graphics.endFill();
					top = gridY[j];
				}
				left = gridX[i];
			}
			target.scale9Grid = rect;
		}
	}
}