package co.beek.utils
{
	import com.adobe.serialization.json.JSONDecoder;
	import com.adobe.serialization.json.JSONEncoder;

	public class JSONBeek
	{
		public static function decode(json:String):*
		{
			return new JSONDecoder(json, true).getValue();	
		}

		public static function encode(value:*):String
		{
			return new JSONEncoder(value).getString();
		}
	}
}