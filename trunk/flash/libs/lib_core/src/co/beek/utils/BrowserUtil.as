package co.beek.utils
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	public class BrowserUtil
	{
		public static const TOP:String = "_top";
		public static const BLANK:String = "_blank";
		
		public static function navigate(url:String, target:String = BLANK):void
		{
			navigateToURL(new URLRequest(url), target);
		}
	}
}