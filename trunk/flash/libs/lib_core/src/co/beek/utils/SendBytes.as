package co.beek.utils
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	
	public class SendBytes extends ByteArray
	{
		public static const BOUNDARY:String = '---------------------------7d76d1b56035e';
		public static const CONTENT_TYPE:String = "multipart/form-data;  boundary="+BOUNDARY;
		
		public function SendBytes() 
		{
			writeMultiByte('--'+BOUNDARY+'\r\n', "ascii");
		}
		
		public function addFile(name:String, filename:String, data:ByteArray):void
		{
			var header:String = 'Content-Disposition: form-data; name="'+name
				+'"; filename="'+filename+'"\r\n'
				+'enctype="multipart/form-data" \r\n'
				+'Content-Type: application/octet-stream\r\n\r\n';
			
			writeMultiByte(header, "ascii");
			writeBytes(data);
			writeMultiByte('\r\n--'+BOUNDARY+'--\r\n', "ascii");
		}
		
		public function addVar(name:String, value:String):void
		{
			var header:String =	'Content-Disposition: form-data; name="'+name+'"\r\n\r\n'
				+value+'\r\n'
				+'--'+BOUNDARY+'\r\n';
			
			writeMultiByte(header, "ascii");
		}
	}
}