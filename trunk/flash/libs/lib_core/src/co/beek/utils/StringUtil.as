package co.beek.utils
{
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.xml.XMLDocument;

	public class StringUtil
	{
		private static var replaceChars:Dictionary = new Dictionary;
		{
			replaceChars['‘'] = "'";
			replaceChars['’'] = "'";
			
			replaceChars['amp;'] = "&";
			replaceChars['#038;'] = "&";
			replaceChars['#039;'] = "'";
			replaceChars['#39;'] = "'";
			replaceChars['lsquo;'] = "'";
			replaceChars['rsquo;'] = "'";
			replaceChars['sbquo;'] = "‚";
			replaceChars['ldquo;'] = "'";
			
			replaceChars['&amp;'] = "&";
			replaceChars['&#038;'] = "&";
			replaceChars['&#039;'] = "'";
			replaceChars['&#39;'] = "'";
			replaceChars['&lsquo;'] = "'";
			replaceChars['&rsquo;'] = "'";
			replaceChars['&sbquo;'] = ",";
			replaceChars['&ldquo;'] = "'";
			
			// Temp until we can sort out these chars in the json
			replaceChars['"'] = "'";
			
			replaceChars['['] = "(";
			replaceChars[']'] = ")";
			
			replaceChars['}'] = ")";
			replaceChars['{'] = "(";
			/*
			replaceChars["<br>\n"] = "\n";
			replaceChars["<br/>\n"] = "\n";
			replaceChars["<br>"] = "\n";
			replaceChars["<br/>"] = "\n";
			*/
			replaceChars["\\n"] = "\n";
			/*
			replaceChars["<p>"] = "\n";
			replaceChars["</p>"] = "\n";
			*/
			
			
			replaceChars["<b>"] = "";
			replaceChars["</b>"] = "";
			
			// fix double chars
			replaceChars['&&'] = "&";
			
		}

		private static var regex:Dictionary = new Dictionary;
		{
			regex[/<.*?>/g] = "";
		}
		
		public static function truncate(value:String, len:int):String
		{
			if(value.length < len)
				return value;
			
			return value.substring(0, value.lastIndexOf(" ", len-4))+ " ...";
		}

		public static function truncateTextField(field:TextField, chars:int = 0):void
		{
			var firstLine:String = field.getLineText(0);
			if(firstLine.length < field.text.length)
				field.text = firstLine + "...";
		}
		
		public static function truncateContent(field:TextField, chars:int = 0):void
		{
			var lastLine:String = field.getLineText(field.bottomScrollV - 1);
			
			// Correct for when last line is 
			if(lastLine == "\n")
				lastLine = ".";
			
			var endIndex:int = field.text.lastIndexOf(lastLine) + lastLine.length;
			
			if(endIndex < field.text.length)
				field.text = truncate(field.text, endIndex - chars);
		}
		
		public static function cleanChars(value:String):String
		{
			for(var match:String in replaceChars)
				value = value.split(match).join(replaceChars[match]);
			
			return value.replace(/<.*?>/g, "");
		}
		
		public static function toSlug(id:String, title:String):String
		{
			title = title.toLowerCase();
			title = title.split(" ").join("-");
			title = title.split("&").join("and");
			title = title.replace(/('|"|,)/g, "");
			
			return id+"-"+escape(title);
		}
		
		public static function bytesToMbString(bytes:int):String
		{
			return (Math.floor(((bytes/1024/ 1024)*100))/100)+"MB";
		}
		
		public static function removeHTML(htmlText:String):String
		{
			return htmlText.replace(/<[^>]*>/gi, '');
		}
		
		public static function trim( s:String ):String
		{
			return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
		}
	}
}