package co.beek.utils
{
	import flash.display.DisplayObject;
	import flash.geom.ColorTransform;

	public class ColorUtil
	{
		public function ColorUtil()
		{
		}
		
		public static function colorize(clip:DisplayObject, color:uint):void
		{
			var colorTransform:ColorTransform = clip.transform.colorTransform;
			colorTransform.color = color;
			clip.transform.colorTransform = colorTransform;
		}
	}
}