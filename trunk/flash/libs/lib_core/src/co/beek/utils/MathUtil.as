package co.beek.utils
{
	

	public class MathUtil
	{
		public static const TO_DEGREES:Number = 180 / Math.PI;
		public static const TO_RADIANS:Number = Math.PI / 180;
		
		public static const KM_PER_MINUTE:Number = 0.0833333;
		
		public static function delta(a:Number, b:Number):Number
		{
			if(a > b)
				return a - b;
			
			return b - a;
		}

		public static function toRad(a:Number):Number
		{
			return a * TO_RADIANS;
		}

		public static function toDeg(a:Number):Number
		{
			return a * TO_DEGREES;
		}
	}
}