package co.beek.model.hotspots {
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.utils.MathUtil;
	
	import flash.events.Event;

	public class BubbleData extends HotspotData {
		/**
		 * Utility function for creating a new bubble.
		 * The bubble can be specific to a guide if the guideId is not null
		 */
		
		public var target:String;
		
		public static function create(id:String, sceneId:String, target:SceneBasicData):BubbleData
		{
			return new BubbleData({
				"id":id,
				'sceneId':sceneId,
				'targetSceneId':target.id,
				'targetScene':target.data
			});
		}
		
		private var _targetScene:SceneBasicData;
		private var _targetLocation:LocationData;
		

		public function BubbleData(data:Object) {
			super(data);

			_targetScene = new SceneBasicData(data['targetScene']);
		}
		
		/**
		 * Override rotation y so bubble always faces camera
		 */
		public override function get rotationY():Number
		{
			return _data['pan'];
		}

		/**
		 * Override rotation x so bubble always faces camera
		 */
		public override function get rotationX():Number
		{
			return -_data['tilt'];
		}
		
		public function get icon():int
		{	return _data['icon']; }
		
		public function set icon(value:int):void
		{	setProperty("icon", value) }

		public override function get title():String {
			return _data['title'] != null || _targetScene == null 
				? super.title
				: _targetScene.title
		}
		
		public override function set title(value:String):void 
		{
			_data['title'] = value;
			_changed = true;
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get titleOverride():String {
			return super.title;
		}
		
		public function set titleOverride(value:String):void {
			setProperty('title', value);
		}
		
		public function get targetSceneId():String {
			return _data['targetSceneId']
		}

		public function set targetScene(value:SceneBasicData):void {
			_targetScene = value;

			_data["targetScene"] = value.data;
			_data["targetSceneId"] = value.id;

			_changed = true;

			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get targetScene():SceneBasicData {
			return _targetScene;
		}
		
		public function get targetLocation():LocationData {
			return _targetLocation;
		}

		public function set targetLocation(location:LocationData):void {
			_targetScene = location.defaultScene;
			_targetLocation = location;
			// Values pulled from location
			_data["targetSceneId"] = location.defaultSceneId;
			_data["title"] = location.title;
			_data["description"] = location.description;

			_changed = true;

			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}