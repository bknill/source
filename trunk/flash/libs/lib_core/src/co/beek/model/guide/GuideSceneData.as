package co.beek.model.guide
{
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.Model;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.hotspots.GuideSceneHotspot;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	public class GuideSceneData extends AbstractData
	{
		
		public static const GUIDE_SCENE_CHANGED:String = "GUIDE_SCENE_CHANGED";
		
		public static function from(id:String, section:GuideSectionData, scene:SceneBasicData, order:int):GuideSceneData
		{
			return new GuideSceneData({
				'id': id,
				'guideSectionId' : section.id,
				'sceneId' : scene.id,
				'scene' : scene.data,
				'order' : order
			})
		}

		public static function fromLocation(id:String, section:GuideSectionData, location:LocationData, order:int):GuideSceneData
		{
			return new GuideSceneData({
				'id': id,
				'guideSectionId' : section.id,
				'sceneId' : location.defaultScene.id,
				'scene' : location.defaultScene.data,
				'title' : location.title,
				'description' : location.description,
				'order' : order,
				'phone' : location.phone,
				'email' : location.email,
				'infoURL' : location.infoUrl
			})
		}

		public static function clone(id:String, section:GuideSectionData, guideScene:GuideSceneData):GuideSceneData
		{
			return new GuideSceneData({
				'id': id,
				'guideSectionId' : section.id,
				'sceneId' : guideScene.scene.id,
				'scene' : guideScene.scene.data,
				'title' : guideScene.title,
				'description' : guideScene.description,
				'hotspotsToHide' : guideScene.hotspotsToHide,
				
				'photos' : guideScene.getHotspotsData(PhotoData),
				'posters' : guideScene.getHotspotsData(PosterData),
				'videos' : guideScene.getHotspotsData(VideoData),
				'sounds' : guideScene.getHotspotsData(SoundData),
				'rssReaders' : guideScene.getHotspotsData(RssReaderData),
				
				'order' : int.MAX_VALUE
			})
		}
		
		public static function merge(v1:Vector.<GuideSceneData>, v2:Vector.<GuideSceneData>):Vector.<GuideSceneData>
		{
			for(var i:int = 0; i < v2.length; i++)
				v1.push(v2[i]);
			
			return v1;
		}
		
		public static const TITLE_MAX_LENGTH:int = 23;
		
		private var _scene:SceneBasicData;
		
		
		
		private var _hotspots:Vector.<HotspotData> = new Vector.<HotspotData>;
		
		public function GuideSceneData(data:Object)
		{
			super(data);
			
			_scene = new SceneBasicData(data['scene']);
			
			
			parse(data['sounds'], SoundData);
			parse(data['photos'], PhotoData);
			parse(data['posters'], PosterData);
			parse(data['videos'], VideoData);
			parse(data['rssReaders'], RssReaderData);
		}
		
		
		private function parse(data:Array, c:Class):void
		{
			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
			{
				var hotspot:GuideSceneHotspot = new c(data[i])
					
				// Ignore buggy media, it should get deleted next save.
				if(hotspot is MediaData && MediaData(hotspot).file == null)
					continue;
				
				hotspot.addEventListener(Event.CHANGE, onHotspotDataChange);
				_hotspots.push(hotspot); 
			}
		}
		
		public function get scene():SceneBasicData
		{	return _scene }

		public function get hotspots():Vector.<HotspotData>
		{	return _hotspots }
		
		public function hasHotspot(hotspotData:HotspotData):Boolean
		{
			return _hotspots.indexOf(hotspotData) >= 0;
		}
		
		public function get sectionId():String
		{	return _data['guideSectionId'] }

		public function set sectionId(value:String):void
		{	_data['guideSectionId'] = value; }
		
		public function get section():GuideSectionData
		{
			for each(var section:GuideSectionData in Model.guideData.guideSections)
				if(section.id == sectionId)
					return section
			
			return null
		}
		
		public function get pagePositionX():int
		{	return _data['pagePositionX'] }
		
		public function set pagePositionX(value:int):void
		{	_data['pagePositionX'] = value; }
		
		public function get pagePositionY():int
		{	return _data['pagePositionY'] }
		
		public function set pagePositionY(value:int):void
		{	_data['pagePositionY'] = value; }
		
		public function get order():int
		{	return _data['order'] }

		public function set order(value:int):void
		{	
			if(order != value)
				trace("changing order");
			setProperty('order', value) 
		}
		
		[Bindable (event="change")]
		public function get title():String
		{ 	
			return getProperty('title');
		}

		[Bindable (event="change")]
		public function get titleMerged():String
		{ 	
			if(getProperty('title') != null && getProperty('title') != "")
				return getProperty('title');
			
			return scene.title;
		}
		
		public function set title(value:String):void
		{	setProperty('title', value) }
		
		[Bindable (event="change")]
		public function get description():String
		{ 	
			return getProperty('description');
		}
		
		public function set description(value:String):void
		{	setProperty('description', value) }
		
		[Bindable (event="change")]
		public function get descriptionMerged():String
		{ 	
			if(getProperty('description') != null && getProperty('description') != "")
				return getProperty('description');
			
			return scene.description;
		}
		
		public function get links():Array
		{
			var links:Array = descriptionMerged.match(/<a([^>]+)>(.*?)<\/a>/ig);
			if(links.length > 0)
			for(var i:int = 0; links != null && i<links.length; i++){
				if(links[i].match(/event:([^"]+)\"/i))
					links[i] = String(links[i]).match(/event:([^"]+)\"/i)[1];
			}
			
			return links;
		}
		
		private function get hotspotsToHide():String {
			return getNotNullString('hotspotsToHide');
		}
		
		private function set hotspotsToHide(value:String):void {
			setProperty('hotspotsToHide', value);
		}
		
		public var pageNum:int;
		
/*		public function set pageNum(value:int):void {
			this.pageNum = value;
		}*/
		
		public function isHidden(hotspot:HotspotData):Boolean
		{
			return GameTaskHotspot.hasHotspot(hotspotsToHide, hotspot);
		}
		
		public function setHidden(hotspot:HotspotData, value:Boolean):void
		{
			if(value)
				hotspotsToHide = GameTaskHotspot.removeHotspot(hotspotsToHide, hotspot.id);
			else
				hotspotsToHide = GameTaskHotspot.addHotspot(hotspotsToHide, hotspot, SceneElementData.create(_scene));
		}
		
		[Bindable (event="transitionVideoChange")]
		public function get transitionVideo():String {
			return getProperty('transitionVideo');
		}
		
		public function set transitionVideo(value:String):void {
			setProperty('transitionVideo', value);
			dispatchEvent(new Event("transitionVideoChange"));
		}
		
		
		[Bindable (event="voiceoverChange")]
		public function get voiceover():String {
			return getProperty('voiceover');
		}
		
		public function set voiceover(value:String):void {
			setProperty('voiceover', value);
			dispatchEvent(new Event(GUIDE_SCENE_CHANGED));
			dispatchEvent(new Event("voiceoverChange"));
		}
		
		public function get voiceoverTrack():String {
			return getProperty('voiceoverTrack');
		}
		
		public function set voiceoverTrack(value:String):void {
			setProperty('voiceoverTrack', value);
		}
		
		
		public function addHotspot(hotspot:HotspotData):void
		{
			hotspot.addEventListener(Event.CHANGE, onHotspotDataChange);
			_hotspots.push(hotspot);
			_changed = true;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_ADDED, hotspot));
		}
		
		private function onHotspotDataChange(event:Event):void
		{
			dispatchEvent(event);
		}
		
		public function removeHotspot(hotspot:HotspotData):void
		{
			_hotspots.splice(_hotspots.indexOf(hotspot), 1);
			
			_changed = true;
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_REMOVED, hotspot));
		}
		
		override public function get data():Object
		{	
			var data:Object = super.data;
			
			data['photos'] = getHotspotsData(PhotoData);
			data['posters'] = getHotspotsData(PosterData);
			data['videos'] = getHotspotsData(VideoData);
			data['sounds'] = getHotspotsData(SoundData);
			data['rssReaders'] = getHotspotsData(RssReaderData);
			
			return data;
		}
		
		private function getHotspotsData(c:Class):Array
		{
			var data:Array = [];
			for(var i:int = 0; i < _hotspots.length; i++)
			{
				if(_hotspots[i] is GuideSceneHotspot && _hotspots[i] is c)
				{
					GuideSceneHotspot(_hotspots[i]).guideSceneId = id;
					data.push(_hotspots[i].data);
				}
			}
				
			
			return data;
		}
	}
}