package co.beek.model.data
{
	public class ScenePanoTask
	{
		private static const STATI:Array = [];
		{
			STATI[0] = "Pending";
			STATI[1] = "Processing";
			STATI[2] = "Uploading";
			STATI[3] = "Uploading";
		}
		private var _data:Object;
		
		private var _scene:SceneBasicData;
		
		public function ScenePanoTask(data:Object)
		{
			_data = data;
			_scene = new SceneBasicData(data['scene']);
		}
		
		public function get id():String
		{	return _data['id']  }
		
		public function get sceneId():String
		{	return _data['sceneId'] }
		
		private function get status():String
		{	return _data['status'] }
		
		public function get statusString():String
		{	return STATI[status] }
		
		public function get scene():SceneBasicData
		{	return _scene }
	}
}


