package co.beek.model.guide
{
	import co.beek.model.data.AbstractData;
	
	public class GuideBasicData extends AbstractData
	{

		public static function convert(guide:GuideData):GuideBasicData
		{
			return new GuideBasicData({
				"destinationId" : guide.destinationId,
				"teamId" : guide.teamId,
				"thumbIncrement" : guide.thumbIncrement,
				"thumbName" : guide.thumbName,
				"title" : guide.title,
				"description" : guide.description,
				"id" : guide.id,
				"parent" : guide.parent
			});
		}
		
		public function GuideBasicData(data:Object)
		{
			super(data);
		}
		
		
		
		public function get destinationId():String
		{	return _data['destinationId'] }
		
		public function get teamId():String
		{	return _data['teamId'] }
		
		public function get saveIncrement():int
		{	return _data['saveIncrement']; }
		
		public function get thumbName():String
		{	return _data['thumbName']; }
		
		/**
		 * The title of the guide as used on the front cover
		 */
		[Bindable (event="change")]
		public function get title():String
		{	return getNotNullString('title') }
		
		public function set title(value:String):void
		{	setProperty('title', value) }
		
		/**
		 * The description of the Location
		 */
		[Bindable (event="change")]
		public function get description():String
		{	return getNotNullString('description') }
		
		public function set description(value:String):void
		{	setProperty("description", value) }

	}
}