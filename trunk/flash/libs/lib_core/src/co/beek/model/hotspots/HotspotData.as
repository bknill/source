package co.beek.model.hotspots
{
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.PanoPos;
	import co.beek.utils.MathUtil;
	
	import flash.events.Event;

	[Bindable (event="change")]
	public class HotspotData extends AbstractData
	{
		public static const ORIENTATION_UPDATED:String = "ORIENTATION_UPDATED";
		
		
		
		public function HotspotData(data:Object = null)
		{
			super(data);
		}

		public function get sceneId():String
		{	return _data['sceneId'] }

		public function get title():String
		{	return getNotNullString('title'); }

		public function set title(value:String):void
		{	setProperty('title', value); }

		/*public function get hidden():Boolean
		{	return _data['sceneId'] }

		public function set hidden(value:Boolean):void
		{	setProperty('hidden', value) }*/
		
		public function get panoPos():PanoPos
		{	return new PanoPos(_data['pan'], _data['tilt'], distance); }

		public function set panoPos(value:PanoPos):void
		{
			_data['pan'] = value.pan;
			_data['tilt'] = value.tilt;
			_data['distance'] = value.distance;
			_changed = true;
			dispatchEvent(new Event(Event.CHANGE));
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		
		[Bindable (event="change")]
		public function set pan(value:Number):void
		{
			setProperty('pan', value);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		public function get pan():Number
		{
			return _data['pan'] ? _data['pan'] : 0;
		}

		[Bindable (event="change")]
		public function set tilt(value:Number):void
		{
			setProperty('tilt', value);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		public function get tilt():Number
		{
			return _data['tilt'] ? _data['tilt'] : 0;
		}

		[Bindable (event="change")]
		public function set rotationX(value:Number):void
		{
			setProperty('rotationX', value * MathUtil.TO_RADIANS);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}

		public function get rotationXEnabled():Boolean
		{
			return false;
		}
		
		/**
		 * Rotation of the object on x axis in Degrees
		 */
		public function get rotationX():Number
		{
			return _data['rotationX'] == null
				? 0 : _data['rotationX'] * MathUtil.TO_DEGREES;
		}

		/**
		 * The rotation
		 */
		[Bindable (event="change")]
		public function set rotationY(value:Number):void
		{
			setProperty('rotationY', value * MathUtil.TO_RADIANS);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		public function get rotationYEnabled():Boolean
		{
			return false;
		}

		/**
		 * Rotation of the object on y axis in Degrees
		 */
		public function get rotationY():Number
		{
			return _data['rotationY'] == null
				?  _data['pan']
				: _data['rotationY'] * MathUtil.TO_DEGREES;
		}

		[Bindable (event="change")]
		public function set rotationZ(value:Number):void
		{
			setProperty('rotationZ', value);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		/**
		 * Whether or not this hotspot can be selected
		 * in thepano and zoomed into. 
		 */
		public function get selectionEnabled():Boolean
		{
			return true;
		}

		public function get rotationZEnabled():Boolean
		{
			return false;
		}
		
		/**
		 * Rotation of the object on y axis in Degrees
		 */
		public function get rotationZ():Number
		{
			return _data['rotationZ'] == null
				? 0 : _data['rotationZ'] * MathUtil.TO_DEGREES;
		}

		/** value between 200 and 1000 */
		public function get distance():Number
		{
			return _data['distance']
				? _data['distance']
				: 100;
		}

		public function set distance(value:Number):void
		{
			setProperty('distance', value);
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}
		
		public var inWorldWidth:Number;
		public var inWorldHeight:Number;
		

	}
}