package co.beek.model.events
{
	import flash.events.Event;
	
	public class TrackingEvent extends Event
	{
		public static const TRACK_EVENT:String = "TRACK_EVENT";
		
		private var _category:String;
		private var _action:String;
		private var _label:String;
		private var _value:int;
		
		
		//category, action, opt_label, opt_value
		public function TrackingEvent(category:String, action:String, label:String , value:int)
		{
			super(TRACK_EVENT);
			_category = category;
			_action = action;
			_label = label;
			_value = value;
		}

		public function get category():String
		{
			return _category;
		}
		
		public function get action():String
		{
			return _action;
		}
		
		public function get label():String
		{
			return _label;
		}
		
		public function get value():int
		{
			return _value;
		}
		
		public function get code():String
		{
			return category+"~"+action+"~"+label;
		}
	}
}