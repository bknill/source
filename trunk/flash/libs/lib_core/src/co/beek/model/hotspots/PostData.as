package co.beek.model.hotspots
{
	public class PostData extends HotspotData
	{
		public static const SIGNS_CHANGED:String = "SIGNS_CHANGED";
		
		public static function create(id:String, sceneId:String):PostData
		{
			return new PostData({
				"id": id,
				"sceneId":sceneId
			});
		}
		
		public function PostData(data:Object)
		{
			super(data);
		}
		
		public override function get title():String
		{	return _data['title'] ? super.title : "SIGNPOST" }
	}
}