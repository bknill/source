package co.beek.model.data
{
	public class PanoLookTo extends PanoLook
	{
		public static function fromString(value:String):PanoLookTo
		{
			var params:Array = value.split(":");
			return new PanoLookTo(params[0], params[1], params[2], params[3])
		}
		private var _duration:int;
		
		public function PanoLookTo(pan:Number, tilt:Number, fov:Number, duration:int)
		{
			super(pan, tilt, fov);
			_duration = duration;
		}
		
		public function get duration():Number
		{
			return _duration;
		}
		
		public override function toString():String
		{
			return new Array(pan, tilt, fov, _duration).join(":");
		}
		
	}
}