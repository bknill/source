package co.beek.model.data
{
	public class FacebookUser
	{
		private var _data:Object;
		
		public function FacebookUser(data:Object)
		{
			_data = data;
		}
		
		public static function create(email:String):FacebookUser
		{
			return new FacebookUser({
				"email":email
			});
		}
		
		
		public function get email():String
		{	return _data['email']  }
		
		public function get data():Object
		{	return _data }
		
	}
}