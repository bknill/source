package co.beek.model.hotspots
{
	import co.beek.model.data.SceneBasicData;
	
	import flash.events.Event;

	public class PosterData extends GuideSceneHotspot
	{
		public static function createForScene(id:String, sceneId:String):PosterData
		{
			return new PosterData({
				"id":id,
				"sceneId":sceneId,
				"showIcon":false,
				"selectable":true,
				"frame":FRAME_BLUE_ROUNDED
			});
		}

		public static function createForGuide(id:String, guideSceneId:String):PosterData
		{
			return new PosterData({
				"id":id,
				"guideSceneId":guideSceneId,
				"showIcon":false,
				"selectable":true,
				"frame":FRAME_BLUE_ROUNDED
			});
		}
		
		public static const FRAME_NONE:int = 0;
		public static const FRAME_BLUE_BOX:int = 1;
		public static const FRAME_BLUE_ROUNDED:int = 2;
		public static const FRAME_BLACK_BOX:int = 3;
		public static const FRAME_BLACK_ROUNDED:int = 4;
		
		public function PosterData(data:Object)
		{
			super(data);
		}
		
		public override function get selectionEnabled():Boolean
		{
			return _data['selectable'];
		}

		public function set selectionEnabled(value:Boolean):void
		{
			setProperty('selectable', value);
		}
		
		public function get showIcon():Boolean
		{
			return _data['showIcon'];
		}
		
		public function set showIcon(value:Boolean):void
		{
			setProperty('showIcon', value);
		}
		
		/*public function get listenHotspotId():String
		{	
			return _data['listenHotspotId'];
		}
		
		public function set listenHotspotId(value:String):void
		{
			setProperty('listenHotspotId', value);
		}*/

		public override function get title():String
		{
			if(super.title)
				return super.title;
			
			return "Poster"; 
		}
		
		public override function set title(value:String):void
		{
			setProperty('title', value);
		}

		public function get titleOverride() : String
		{
			return getNotNullString('title');
		}
		public function set titleOverride(value:String): void
		{
			setProperty('title', value);
		}
		
		public function get frame():int
		{	return _data['frame'] }
		
		public function set frame(value:int):void
		{
			setProperty('frame', value);
		}

		public function get text():String
		{	return getNotNullString('text') }
		
		public function set text(value:String):void
		{
			setProperty('text', value);
		}
		
		public function get buttonUrl():String
		{	
			return _data['buttonUrl'];
		}
				
		public function set buttonUrl(value:String):void
		{
			setProperty('buttonUrl', value);
			setProperty('buttonSceneId', null);
		} 
		
		[Bindable (event="buttonSceneChange")]
		public function get buttonScene():SceneBasicData
		{
			if(_data['buttonScene'] != null)
				return new SceneBasicData(_data['buttonScene'])
			else
				return null
		}
		
		public function set buttonScene(value:SceneBasicData):void
		{
			setProperty(_data['buttonScene'],value);
		}
		
		public function get hasButton():Boolean
		{
			return buttonSceneId != null
				|| buttonUrl != null
				|| buttonFavourite
		}
		
		public function set hasButton(value:Boolean):void
		{
			if(!value && buttonSceneId)
				buttonSceneId = null;
			
			if(!value && buttonUrl)	
				buttonUrl = null;
		}
		
		public function get buttonLabel():String
		{	
			return _data['buttonLabel'];
		}
		
		public function set buttonLabel(value:String):void
		{
			setProperty('buttonLabel', value);
		}  
		
		public function get buttonFavourite():Boolean
		{	
			return _data['buttonFavourite'];
		}
		
		public function set buttonFavourite(value:Boolean):void
		{
			setProperty('buttonFavourite', value);
			setProperty('buttonSceneId', null);
			setProperty('buttonUrl', null);
		} 
		
		public function get buttonSceneId():String
		{	
			return _data['buttonSceneId'];
		}
		
		public function set buttonSceneId(value:String):void
		{
			setProperty('buttonSceneId', value);
			setProperty('buttonUrl', null);
			setProperty('buttonFavourite', false);
			
			dispatchEvent(new Event("buttonSceneChange"))
		}
		
		public override function get rotationXEnabled():Boolean
		{
			return true;
		}
		
		public override function get rotationYEnabled():Boolean
		{
			return true;
		}

		public override function get rotationZEnabled():Boolean
		{
			return true;
		}
		
	}
}