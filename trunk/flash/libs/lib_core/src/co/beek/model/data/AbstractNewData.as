package co.beek.model.data
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class AbstractNewData extends EventDispatcher
	{
		public static function id(data:Object):String
		{
			return new AbstractData(data).id;
		}
		
		protected var _data:Object;
		
		protected var _changed:Boolean;
		
		public function AbstractNewData()
		{
			super();
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		
		public function get data():Object
		{
			return _data;
		}

		public function get loaded():Boolean
		{
			return _data != null;
		}
		
		public function get changed():Boolean
		{
			return _changed;
		}
		
		public function get id():String
		{	return _data['id'] }
		
		protected function getNumber(property:String):Number
		{
			return _data[property] ? _data[property] : 0;
		}
		
		protected function getNotNullString(property:String):String
		{
			return _data[property] ? _data[property] : "";
		}
		
		protected function setProperty(property:String, value:Object):void
		{
			if(_data[property] == value)
				return;
			
			// This stops us saving empty values
			//if(value === null || value === "")
			//	delete _data[property];
			
			//else
				_data[property] = value;
			
			_changed = true;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}