package co.beek.model.hotspots
{
	public class SoundData extends MediaData
	{
		
		public var _preview:String;
		
		public static function createForScene(id:String, sceneId:String):SoundData
		{
			return new SoundData({
				"id":id,
				"sceneId":sceneId,
				"looping":"1"
			});
		}
		
		public static function createForGuide(id:String, guideSceneId:String):SoundData
		{
			return new SoundData({
				"id":id,
				"guideSceneId":guideSceneId,
				"looping":"1"
			});
		}
		
		public function SoundData(data:Object)
		{
			super(data);
		}
		
		/*public function get listenHotspotId():String
		{	
			return _data['listenHotspotId'];
		}
		
		public function set listenHotspotId(value:String):void
		{
			setProperty('listenHotspotId', value);
		}*/

		public override function get title():String
		{	return file != null ? file.fileName : "Blank Sound"; }
		
		public function get ambience():Number
		{	
			if(!_data['ambience'])
				return 0.5;
			
			return getNumber('ambience') 
		}
		
		public function set ambience(value:Number):void
		{	setProperty('ambience', Math.max(0.1, Math.min(value, 1))) }

		/**
		 * Volume ranges from [0.1 - 1]
		 */
		public function get volume():Number
		{	return (1100 - distance)/1000 }
		
		
		/**
		 * Volume ranges from [0.1 - 1]
		 * Distance will range from [1000 - 100]
		 */
		public function set volume(value:Number):void
		{	distance = 1100 - (value * 1000) }
		
		public function set preview(value:String):void
		{_preview = value}
		
		public function get preview():String
		{return _preview}
		
		/**
		 * Whether or not the sound will loop
		 */
		public function get looping():Boolean
		{	
			return _data['looping'] == "1";
		}
		
		public function set looping(value:Boolean):void
		{
			setProperty('looping', value?"1":"0");
		}
		
		public override function get selectionEnabled():Boolean
		{
			return false;
		}
		
		
	}
}