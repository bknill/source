package co.beek.model.data
{
	

	public class SceneElementData extends AbstractData
	{
		public static function create(scene:SceneBasicData):SceneElementData
		{
			return new SceneElementData({
				'id' : scene.id,
				'title' : scene.title
			});
		}
		
		public function SceneElementData(data:Object):void
		{
			super(data);
		}
		
		[Bindable (event="change")]
		public function get title():String
		{	return getNotNullString('title') }
		
		public function set title(value:String):void
		{	setProperty('title', value) }
	}
}