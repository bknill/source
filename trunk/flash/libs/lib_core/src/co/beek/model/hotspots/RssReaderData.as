package co.beek.model.hotspots
{
	import flash.events.Event;

	public class RssReaderData extends GuideSceneHotspot
	{
		public static function createForScene(id:String, sceneId:String):RssReaderData
		{
			return new RssReaderData({
				"id":id,
				"sceneId":sceneId,
				"showIcon":false,
				"selectable":true,
				"type":0
			});
		}
		
		public static function createForGuide(id:String, guideSceneId:String):RssReaderData
		{
			return new RssReaderData({
				"id":id,
				"guideSceneId":guideSceneId,
				"showIcon":false,
				"selectable":true,
				"type":0
			});
		}
		
		public static const URL_CHANGED:String = "URL_CHANGED";
		public static const TYPE_CHANGED:String = "TYPE_CHANGED";
		
		public function RssReaderData(data:Object=null)
		{
			super(data);
		}
		
		public override function get title():String
		{	return _data['title'] ? super.title : "Rss Reader" }
		
		
		public function set icon(value:int):void
		{
			setProperty('icon', value);
		}
		
		public function get type():int
		{	return _data['type'] }
		
		public function set type(value:int):void
		{
			setProperty('type', value);
			dispatchEvent(new Event(TYPE_CHANGED));
		}
		
		
		public function get url():String
		{ 	return _data['url'] }
		
		public function set url(value:String):void
		{	
			setProperty('url', value);
			dispatchEvent(new Event(URL_CHANGED));
		}
		
		/*public function get listenHotspotId():String
		{	
			return _data['listenHotspotId'];
		}
		
		public function set listenHotspotId(value:String):void
		{
			setProperty('listenHotspotId', value);
		}*/

	}
}