package co.beek.model.events
{
	import co.beek.model.hotspots.HotspotData;
	
	import flash.events.Event;
	
	public class HotspotEvent extends Event
	{
		public static const HOTSPOT_ADDED:String = "HOTSPOT_ADDED";

		public static const HOTSPOT_CLICKED:String = "HOTSPOT_CLICKED";

		public static const HOTSPOT_SELECTED:String = "HOTSPOT_SELECTED";
		
		public static const HOTSPOT_REMOVED:String = "HOTSPOT_REMOVED";
		
		public static const HOTSPOT_SHOW_WEB:String = "HOTSPOT_SHOW_WEB";
		
		public static const HOTSPOT_CLOSE_WEB:String = "HOTSPOT_CLOSE_WEB";
		
		private var _hotspot:HotspotData;
		
		public function HotspotEvent(type:String, hotspot:HotspotData)
		{
			super(type);
			_hotspot = hotspot;
		}

		public function get hotspot():HotspotData
		{
			return _hotspot;
		}
		
	}
}