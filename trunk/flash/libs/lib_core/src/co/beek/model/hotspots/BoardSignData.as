package co.beek.model.hotspots
{
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.PanoLook;
	import co.beek.model.data.PanoPos;
	import co.beek.model.data.SceneBasicData;
	
	public class BoardSignData extends AbstractData
	{
		public static const RIGHT:int = 0;
		public static const LEFT:int = 1;
		public static const UP:int = 2;
		public static const DOWN:int = 3;
		
		public static function create(id:String, boardId:String, scene:SceneBasicData):BoardSignData
		{
			if(boardId == null)
				throw new Error("BoardId must not be null");
			
			return new BoardSignData({
				'id' : id,
				'boardId' : boardId,
				'sceneId' : scene.id,
				'scene' : scene.data,
				'direction' : Math.random()*4
			});
		}
		
		private var _scene:SceneBasicData;
		
		public function BoardSignData(data:Object)
		{
			super(data);
			_scene = new SceneBasicData(_data['scene']);
		}
		
		public function get scene():SceneBasicData
		{	return _scene }

		public function get direction():int
		{	return _data['direction'] }
		
		public function set direction(value:int):void
		{	setProperty('direction', value) }
		
		public function get title():String
		{
			return _data['title']
			? _data['title']
				: _scene.title;
		}
		
		public function set title(value:String):void
		{	setProperty('title', value) }
		
		
		/*public function get lookTo():PanoLook
		{
			switch(direction)
			{
				case BoardSignData.UP:
					return new PanoLook(panoPos.pan, panoPos.tilt + 40, panoPos.distance - 20);
					
				case BoardSignData.DOWN:
					return new PanoLook(panoPos.pan, panoPos.tilt - 40, panoPos.distance - 20);
					
				case BoardSignData.LEFT:
					return new PanoLook(panoPos.pan - 90, panoPos.tilt, panoPos.distance - 20);
					
				case BoardSignData.RIGHT:
					return new PanoLook(panoPos.pan + 90, panoPos.tilt, panoPos.distance - 20);
			}
			
			return new PanoLook(panoPos.pan, panoPos.tilt, panoPos.distance);
		}*/
	}
}