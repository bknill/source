package co.beek.model.hotspots
{
	import co.beek.Fonts;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneData;
	import co.beek.utils.StringUtil;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Point;

	public class PhotoData extends MediaData
	{
		public static function createForScene(id:String, sceneId:String):PhotoData
		{
			return new PhotoData({
				"id":id,
				"sceneId":sceneId,
				"selectable":true,
				"frame":NO_FRAME
			});
		}

		public static function createForGuide(id:String, guideSceneId:String):PhotoData
		{
			return new PhotoData({
				"id":id,
				"guideSceneId":guideSceneId,
				"selectable":true,
				"frame":NO_FRAME
			});
		}
		
		
		public static function copyForGuide(id:String, guideSceneId:String, title:String, description:String ):PhotoData
		{
			return new PhotoData({
				"id":id,
				"guideSceneId":guideSceneId,
				"selectable":true,
				"frame":NO_FRAME,
				"title":title,
				"description":description
			});
		}
		
		public static const THUMB_SIZE:int = 200;
		
		public static const NO_FRAME:int = 0;
		public static const TYPE_FRAME_WOOD:int = 1;
		public static const TYPE_FRAME_PLASTIC:int = 2;
		public static const TYPE_FRAME_WHITE:int = 3;
		
		
		public static const MASK_CHANGED:String = "MASK_CHANGED";
		
		
		private var _thumb:BitmapData;
		
		public function PhotoData(data:Object)
		{
			super(data);
		}
		
		public override function get selectionEnabled():Boolean
		{
			return _data['selectable'];
		}

		public function set selectionEnabled(value:Boolean):void
		{
			setProperty('selectable', value);
		}
		
		public function get canFlip():Boolean
		{
			return StringUtil.trim(StringUtil.removeHTML(description)).length > 3 || webUrl;
		}
		
		public static const FLIPPED_CHANGE:String = "FLIPPED_CHANGE";
		
		private var _flipped:Boolean;
		public function set flipped(value:Boolean):void
		{
			if(_flipped == value)
				return;
			
			_flipped = value;
			dispatchEvent(new Event(FLIPPED_CHANGE));
		}
		
		public function get flipped():Boolean
		{
			return _flipped;
		}
		
		/*public function get listenHotspotId():String
		{	
			return _data['listenHotspotId'];
		}
		
		public function set listenHotspotId(value:String):void
		{
			setProperty('listenHotspotId', value);
		}*/
		
		public function get description() : String
		{
			return getNotNullString('description');
		}
		public function set description(value:String): void
		{
			setProperty('description', value);
		}

		public function get mask():Vector.<Point>
		{
			var points:Vector.<Point> = new Vector.<Point>;
			
			var serialized:String = getNotNullString('mask');
			var raw:Array = serialized.split(",");
			for(var i:int; i<raw.length; i++)
			{
				if(String(raw[i]).length < 3)
					continue;
				
				var parts:Array = raw[i].split(":");
				points.push(new Point(parts[0], parts[1]));
			}
			
			return points;
		}
		
		public function set mask(value:Vector.<Point>): void
		{
			var serialized:Array = [];
			for(var i:int; i<value.length; i++)
			{
				var point:Point = value[i];
				serialized.push(Math.round(point.x)+":"+Math.round(point.y));
			}
			
			setProperty('mask', serialized.join(","));
			dispatchEvent(new Event(MASK_CHANGED));
		}
		
		public function get outline():Boolean
		{
			return _data['outline'];
		}
		
		public function set outline(value:Boolean):void
		{
			setProperty('outline', value);
		}
		
		public function get fill():Boolean
		{
			return _data['fill'];
		}
		
		public function set fill(value:Boolean):void
		{
			setProperty('fill', value);
		}
		
		public function get frame():int
		{	return _data['frame'] }
		
		public function set frame(value:int):void
		{
			setProperty('frame', value);
		}
		
		public function get buttonFavourite():Boolean
		{	
			return _data['buttonFavourite'];
		}
				
		public function set buttonFavourite(value:Boolean):void
		{
			setProperty('buttonFavourite', value);
			setProperty('buttonSceneId', null);
			setProperty('buttonUrl', null);
		} 
		
		public function get buttonUrl():String
		{	
			return _data['buttonUrl'];
		}
				
		public function set buttonUrl(value:String):void
		{
			setProperty('buttonUrl', value);
			setProperty('buttonSceneId', null);
			setProperty('buttonFavourite', false);
		} 
		
		public function get webUrl():String
		{	
			return _data['webUrl'];
		}
		
		public function set webUrl(value:String):void
		{
			setProperty('webUrl', value);
		} 
		
		public function get hasButton():Boolean
		{
			return buttonSceneId != null
				|| buttonUrl != null
				|| buttonFavourite;
		}
		
		public function get buttonLabel():String
		{	
			return _data['buttonLabel'];
		}
		
		public function set buttonLabel(value:String):void
		{
			setProperty('buttonLabel', value);
		}  
		
		public function get buttonSceneId():String
		{	
			return _data['buttonSceneId'];
		}
		
		public function set buttonScene(value:SceneBasicData):void
		{
			setProperty('buttonScene', value);
			if(value != null)
				setProperty('buttonSceneId', value.id);
			else
			setProperty('buttonSceneId', null);
			setProperty('buttonUrl', null);
			setProperty('buttonFavourite', false);
		}
		
		public function get buttonScene():SceneBasicData
		{
			if(_data['buttonScene'])
				return new SceneBasicData(_data['buttonScene']);
			return null;
		}
		
		

		public function set thumb(value:BitmapData):void
		{
			_thumb = value;
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get thumb():BitmapData
		{
			return _thumb;
		}
		
		public override function get rotationXEnabled():Boolean
		{
			return true;
		}
		
		public override function get rotationYEnabled():Boolean
		{
			return true;
		}

		public override function get rotationZEnabled():Boolean
		{
			return true;
		}
		
	}
}