package co.beek.model.guide
{
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.hotspots.HotspotData;
	
	public class GameTaskData extends AbstractData
	{
		public static function create(guideid:String, code:String):GameTaskData
		{
			return new GameTaskData({
				'guideId' : guideid,
				'code' : code
			});
		}
		
		public static const GAME_PANO_DRAGGED:String = "panodrag";
		public static const GAME_FIND_HOTSPOTS:String = "findhotspots";
		
		public function GameTaskData(data:Object)
		{
			super(data);
		}
		
		public function get guideId():String
		{
			return _data['guideId'];
		}
		
		public function get order():int
		{	return _data['order'] }
		
		public function set order(value:int):void
		{	setProperty('order', value) }
		
		[Bindable (event="change")]
		public function get code():String
		{
			return _data['code'];
		}
		
		public function set code(value:String):void
		{
			setProperty('code', value);
		}

		[Bindable (event="change")]
		public function get title():String
		{
			return getNotNullString('title');
		}

		public function set title(value:String):void
		{
			setProperty('title', value);
		}

		[Bindable (event="change")]
		public function get input():String
		{
			return _data['input'];
		}

		public function set input(value:String):void
		{
			setProperty('input', value);
		}
		
		[Bindable (event="change")]
		public function get instructions():String
		{ 	
			return getNotNullString('instructions');
		}
		
		public function set instructions(value:String):void
		{
			setProperty('instructions', value);
		}

		[Bindable (event="change")]
		public function get feedback():String
		{ 	
			return getNotNullString('feedback');
		}
		
		public function set feedback(value:String):void
		{
			setProperty('feedback', value);
		}
		
		[Bindable (event="change")]
		public function get output():String
		{ 	
			return getNotNullString('output');
		}
		
		public function set output(value:String):void
		{
			setProperty('output', value);
		}
		
		public function get hotspotsToFind():Array
		{
			return GameTaskHotspot.deserializeHotspots(input);
		}
		
		public function get hotspotsToShow():Array
		{
			return GameTaskHotspot.deserializeHotspots(output);
		}
		
		public function addHotspotToFind(hotspot:HotspotData, scene:SceneElementData):void
		{
			input = GameTaskHotspot.addHotspot(input, hotspot, scene);
		}

		public function addHotspotToShow(hotspot:HotspotData, scene:SceneElementData):void
		{
			output = GameTaskHotspot.addHotspot(output, hotspot, scene);
		}
		
		public function hasHotspotToFind(hotspot:HotspotData):Boolean
		{
			return GameTaskHotspot.hasHotspot(input, hotspot);
		}
		
		public function hasHotspotToShow(hotspot:HotspotData):Boolean
		{
			return GameTaskHotspot.hasHotspot(output, hotspot);
		}
		
		public function removeHotspotToFind(hotspot:GameTaskHotspot):void
		{
			input = GameTaskHotspot.removeHotspot(input, hotspot.id);
		}

		public function removeHotspotToShow(hotspot:GameTaskHotspot):void
		{
			output = GameTaskHotspot.removeHotspot(output, hotspot.id);
		}
	}
}