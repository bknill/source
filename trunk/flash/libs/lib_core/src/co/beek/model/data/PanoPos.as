package co.beek.model.data
{
	import co.beek.utils.MathUtil;
	
	import flash.geom.Vector3D;

	public class PanoPos extends PanoAngle
	{
		private var _distance:Number;
		
		public function PanoPos(pan:Number, tilt:Number, distance:Number)
		{
			super(pan, tilt);
			_distance = distance;
		}
		
		public function get distance():Number
		{
			return _distance;
		}
		
		public function get vector():Vector3D
		{
			var pr:Number = -1 * (pan - 90) * MathUtil.TO_RADIANS;
			var tr:Number = -1 * tilt * MathUtil.TO_RADIANS;
			return new Vector3D(
				_distance * Math.cos(pr) * Math.cos(tr),
				_distance * Math.sin(tr),
				_distance * Math.sin(pr) * Math.cos(tr)
			);
		}
	}
}