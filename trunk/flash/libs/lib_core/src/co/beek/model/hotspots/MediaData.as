package co.beek.model.hotspots
{
	import flash.events.Event;
	
	import co.beek.model.data.FileData;

	public class MediaData extends GuideSceneHotspot
	{
		//public static const UPLOAD_CHANGED:String = "UPLOAD_CHANGED";
		//public static const FILE_CHANGED:String = "FILE_CHANGED";
		
		private var _file:FileData;
		
		//private var _upload:Object;
		
		public function MediaData(data:Object=null)
		{
			super(data);
			
			// There is no file when first dragged and dropped
			if(data['file'])
				_file = new FileData(data['file']);
		}
		
		public function get file():FileData
		{	return _file; }
		
		public function set file(value:FileData):void
		{	
			_file = value;
		}
		
		public override function get data():Object
		{
			var data:Object = super.data;
			if(_file)
			{
				data['file'] = _file.data;
				data['fileId'] = _file.id;
			}
			return data;
		}
	}
}