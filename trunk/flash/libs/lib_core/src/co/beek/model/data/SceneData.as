package co.beek.model.data
{
	import co.beek.map.MapData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GuideData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PostData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Rectangle;

	public class SceneData extends SceneBasicData
	{
		public static const ORIENTATION_UPDATED:String = "ORIENTATION_UPDATED";

		public static const PANO_UPDATED:String = "PANO_UPDATED";
		
		public static const NEW_THUMB:String = "NEW_THUMB";

		public static const THUMB_BUBBLE:Rectangle = new Rectangle(0,0,325,330);// Size of the guide cover.
		
		private var _location:LocationData;
		
		private var _hotspots:Vector.<HotspotData> = new Vector.<HotspotData>;
		
		public function SceneData(data:Object):void
		{
			super(data);
			
			_location = new LocationData(data['location']);

			// I think that was a failed experiment
			parse(data['bubbles'], BubbleData);
			parse(data['boards'], BoardData);
			//parse(data['posts'], PostData);
			parse(data['sounds'], SoundData);
			parse(data['photos'], PhotoData);
			parse(data['posters'], PosterData);
			parse(data['videos'], VideoData);
			parse(data['rssReaders'], RssReaderData);
		}
		
		private function parse(data:Array, c:Class):void
		{
			if(data == null)
				return;
			
			for(var i:int = 0; i < data.length; i++)
			{
				var hotspot:HotspotData = new c(data[i]);
				
				// Ignore buggy media, it should get deleted next save.
				if(hotspot is MediaData && MediaData(hotspot).file == null)
					continue;
				
				hotspot.addEventListener(Event.CHANGE, onHotspotDataChange);
				_hotspots.push(hotspot);
			}
		}
		
		private function onHotspotDataChange(event:Event):void
		{
			dispatchEvent(event);
		}
		
		public function get hotspots():Vector.<HotspotData>
		{	return _hotspots }
		
		public function hasHotspot(hotspotData:HotspotData):Boolean
		{
			return _hotspots.indexOf(hotspotData) >= 0;
		}
		
		public function get descriptorFile():String
		{	return panoPrefix+"_f.xml" }

		public function get panoPrefix():String
		{	return "scene_"+id+"_pano_"+panoIncrement }
		

		
		/**
		 * does not dispathc a change event as the scene 
		 * is already saved on the server
		 */
		public function set panoIncrement(value:int):void
		{
			if(_data['panoIncrement'] == value)
				return;
			
			_data['panoIncrement'] = value;
			dispatchEvent(new Event(PANO_UPDATED));
		}
		
		[Bindable (event="change")]
		public function get infoUrl():String
		{	
			if(_data['infoURL'] != null && _data['infoURL'] != "")
				return _data['infoURL'];
			
			return _location.infoUrl;
		}
		
		public function set infoUrl(value:String):void
		{	setProperty('infoURL', value) }

		[Bindable (event="change")]
		public function get bookingUrl():String
		{	
			if(_data['bookingURL'] != null && _data['bookingURL'] != "")
				return _data['bookingURL'];
			
			return _location.bookingUrl;
		}
		
		public function set bookingUrl(value:String):void
		{	setProperty('bookingURL', value) }
		
		public function get location():LocationData
		{
			return _location;
		}
		
		
		override public function get changed():Boolean
		{
			if(super.changed)
				return true;
			
			// Bug on mac using filter 
			var ret:Array = new Array(_hotspots.length);
			for (var i:int = 0; i < _hotspots.length; ++i)
			{
				ret[i] = _hotspots[i];
			}
			
			return ret.filter(testChanged).length > 0;
		}

		private function testChanged(element:HotspotData, index:int, array:Array):Boolean
		{
			return element.changed;
		}
		
		
		override public function set data(value:Object):void
		{
			super.data = value;
			
			_newThumb = null;
		}
		
		override public function get data():Object
		{	
			var data:Object = super.data;
			
			
			data['bubbles'] = getHotspotsData(BubbleData);
			data['boards'] = getHotspotsData(BoardData);
			data['posts'] = getHotspotsData(PostData);
			data['photos'] = getHotspotsData(PhotoData);
			data['posters'] = getHotspotsData(PosterData);
			data['videos'] = getHotspotsData(VideoData);
			data['sounds'] = getHotspotsData(SoundData);
			data['rssReaders'] = getHotspotsData(RssReaderData);
			
			return data;
		}
		
		private function getHotspotsData(c:Class):Array
		{
			var data:Array = [];
			for(var i:int = 0; i < _hotspots.length; i++)
			{
				if(_hotspots[i] is MediaData && !MediaData(_hotspots[i]).file)
					continue;
				
				if(_hotspots[i] is c)
					data.push(_hotspots[i].data);
			}
			
			return data;
		}
		
		public function get initialLook():PanoLook
		{
			return new PanoLook(
				_data['pan'], 
				_data['tilt'], 
				_data['fov'] > 0 ? _data['fov'] : 90
			) 
		}
		
		/**
		 * Due to difference between this pano and the last,
		 * Pan values need to have 180 added to them before storing.
		 * This value will be removed when loading
		 */
		public function set initialLook(value:PanoLook):void
		{
			_data['pan'] = value.pan;
			_data['tilt'] = value.tilt;
			_data['fov'] = value.fov;
			dispatchEvent(new Event(Event.CHANGE));
			dispatchEvent(new Event(ORIENTATION_UPDATED));
		}

		/**
		 * The degrees around that the photograpehr was facing when he took hte photo
		 */
		public function get north():int
		{	return _data['north'] }
		
		public function set north(value:int):void
		{	
			setProperty('north', value);
			dispatchEvent(new Event(PANOPOS_UPDATED));
		}
		
		public function addHotspot(hotspot:HotspotData):void
		{
			hotspot.addEventListener(Event.CHANGE, onHotspotDataChange);
			_hotspots.push(hotspot);
			
			_changed = true;
			dispatchEvent(new Event(Event.CHANGE));
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_ADDED, hotspot));
		}
		
		public function removeHotspot(hotspot:HotspotData):void
		{
			hotspot.removeEventListener(Event.CHANGE, onHotspotDataChange);
			_hotspots.splice(_hotspots.indexOf(hotspot), 1);
			
			_changed = true;
			dispatchEvent(new Event(Event.CHANGE));
			dispatchEvent(new HotspotEvent(HotspotEvent.HOTSPOT_REMOVED, hotspot));
		}
		
		private var _lastLook:PanoLook;
		public function get lastLook():PanoLook
		{	
			return _lastLook;
		}
		
		public function set lastLook(value:PanoLook):void
		{
			_lastLook = value;
		}
		
		public function getMapData(guide:GuideData):MapData
		{
			var data:MapData = new MapData(320, 320, 17, guide.mapStyle);
			data.center = geo;
			
			data.zoomToFit(guide.firstScene.scene.geo);
			
			/*if(guide)
				for(var i:int =0; i < guide.guideLocations.length; i++)
					data.zoomToFit(guide.guideLocations[i].location.geo);*/
			
			return data;
		}
		
		private var _newThumb:BitmapData;
		
		public function get newThumb():BitmapData
		{	return _newThumb }
		
		public function set newThumb(value:BitmapData):void
		{
			_newThumb = value;
			dispatchEvent(new Event(NEW_THUMB));
		}
	}
}