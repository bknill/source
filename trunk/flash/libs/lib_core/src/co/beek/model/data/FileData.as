package co.beek.model.data
{
	public class FileData 
	{
		private var _data:Object;
		
		public function FileData(data:Object)
		{
			_data = data;
		}
		
		public function get id():String
		{	return _data['id']  }

		public function get folderId():String
		{	return _data['folderId'] }
		
		/**
		 * the human readable name that is presented to the user
		 */
		public function get fileName():String
		{	return  _data ? _data['fileName'] : "" }

		/**
		 * Used in the tree control
		 */
		public function get label():String
		{	return  _data ? _data['fileName'] : "" }

		
		/**
		 * the renamed file that was uploaded to s3
		 */ 
		 public function get realName():String
		{	return _data ? _data['realName'] : null }
		 
		
		public function get extension():String
		{
			var f:String = realName;
			return f.substr(f.lastIndexOf('.') + 1, f.length).toLowerCase();
		}
		
		public function get data():Object
		{	return _data }
	}
}