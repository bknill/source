package co.beek.model.data
{
	public class PanoLook extends PanoAngle
	{
		public static function fromString(string:String):PanoLook
		{
			var parts:Array = string.split("|");
			return new PanoLook(parts[0], parts[1], parts[2])
		}
		private var _fov:Number;
		
		public function PanoLook(pan:Number, tilt:Number, fov:Number)
		{
			super(pan, tilt);
			_fov = fov;
		}
		
		public function get fov():Number
		{
			return _fov;
		}
		
		public function clone():PanoLook
		{
			return new PanoLook(pan, tilt, _fov);
		}
		
		public function equals(o:PanoLook):Boolean
		{
			if(!o)
				return false;
			
			return pan == o.pan
				&& tilt == o.tilt
				&& _fov == o.fov;
		}
		
		public function toString():String
		{
			return new Array(Math.round(pan), Math.round(tilt), Math.round(fov)).join("|");
		}
		
	}
}