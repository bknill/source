package co.beek.model.hotspots
{
	import co.beek.model.data.FileData;
	
	import flash.events.Event;

	public class ButtonData extends HotspotData
	{
		private var _file:FileData;

		public function ButtonData(data:Object)
		{
			super(data);
			
			if(data['file'])
				_file = new FileData(data['file']);
		}

		public function get url():String
		{ 	return getNotNullString('url');	}

		public function set url(value:String):void
		{	setProperty('url', value); }
		
		public override function get title():String
		{	return	getNotNullString('title');}
		
		public override function set title(value:String):void
		{	setProperty('title', value); }

		public function get file():FileData
		{	return _file; }
		
		public function set file(value:FileData):void
		{
			_file = value;
			_data['id'] = value.id;
			_data['file'] = value.data;
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}