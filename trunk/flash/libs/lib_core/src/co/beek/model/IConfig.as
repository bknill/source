package co.beek.model
{
	import co.beek.model.data.SceneBasicData;

	public interface IConfig
	{
		function get device():String;
		
		function get gaCode():String;
		
		function get version():String;
		
		function get assetCdn():String;
		
		function get adminSwf():String;
		
		/**
		 * Used to create the urls in the share panel
		 */
		function get domain():String;
		
		function get serviceUrl():String;
		
		function get sceneId():String;
		
		function get guideId():String;

		/**
		 * The hotspot that should be shown when the pano has finished loading tiles
		 */
		function get hotspotId():String;
			
		function get tourGuideId():String;
		
		function get tourRole():String;
		
		function get mapTileServer():String;
		
		
		function getGuideJsonUrl(guideId:String):String;
		
		function getSceneJsonUrl(sceneId:String):String;
		
		function getAssetUrl(path:String):String;
		
		function getMapTileUrl(path:String):String;
		
		function isAvailable(scene:SceneBasicData):Boolean
	}
}