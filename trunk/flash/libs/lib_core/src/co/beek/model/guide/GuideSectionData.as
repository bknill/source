package co.beek.model.guide
{
	import flash.display.BitmapData;
	import flash.events.Event;
	
	import co.beek.map.Geo;
	import co.beek.map.MapData;
	import co.beek.model.Model;
	import co.beek.model.data.AbstractData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.hotspots.HotspotData;
	
	
	public class GuideSectionData extends AbstractData
	{
		
		public static function from(id:String, guideId:String, title:String, parent_section_id:String):GuideSectionData
		{
			return new GuideSectionData({
				'id': id,
				'guideId': guideId,
				'title' : title,
				'parent_section_id' : parent_section_id
			})
		}
		
		public static const GUIDE_SCENES_CHANGED:String = "GUIDE_SCENES_CHANGED";
		
		public static const GUIDE_SECTIONS_CHANGED:String = "GUIDE_SECTIONS_CHANGED";
		
		public static const NEW_IMAGE:String = "NEW_IMAGE";
		
		public static const TITLE_CHANGED:String = "TITLE_CHANGED";
		
		public static const IMAGE_CHANGED:String = "IMAGE_CHANGED";
		
		public static const IMAGE_LOADED:String = "IMAGE_LOADED";
		
		public static const MARKER_POSITION:String = "MARKER_POSITION";
		
		private var _guideScenes:Vector.<GuideSceneData>;
		
		private var _guide:GuideData;
		
		private var _sectionChildren:Vector.<GuideSectionData> = new Vector.<GuideSectionData>;
		
		public var menuEdge:Number;
		
		private var _newPageImage:BitmapData;
		
		public var sectionContainerX:Number;
		
		private var _decendents:Vector.<GuideSectionData> = new Vector.<GuideSectionData>
			
		
		public function GuideSectionData(data:Object)
		{
			super(data);
			parseGuideScenes(_data['guideScenes']);

		}
		
		private function parseGuideScenes(scenes:Array):void
		{
			_guideScenes = new Vector.<GuideSceneData>;
			
			if(!scenes)
				return;
			
			for(var i:int = 0; i<scenes.length; i++)
			{
				var guideScene:GuideSceneData = new GuideSceneData(scenes[i]);
				guideScene.addEventListener(Event.CHANGE, onHotspotDataChange);
				_guideScenes.push(guideScene);
			}
			
			_guideScenes = _guideScenes.sort(orderScenes);
		}
		
		public function get sectionChildren():Vector.<GuideSectionData>
		{
			if(_sectionChildren.length == 0)
				parseSectionChildren();
			
			return _sectionChildren
		}
		
		public function parseSectionChildren():void
		{
			for(var i:int = 0; i<Model.guideData.guideSections.length; i++)
			{
				if(Model.guideData.guideSections[i].parent_section_id == this.id)
					_sectionChildren.push(Model.guideData.guideSections[i]);
			}
			
			_sectionChildren = _sectionChildren.sort(orderSections);
		}
		
		private function onHotspotDataChange(event:Event):void
		{
			dispatchEvent(event);
		}
		
		
		public function reorderGuideScenes():void
		{
			_guideScenes = _guideScenes.sort(orderScenes);
			dispatchEvent(new Event(GUIDE_SCENES_CHANGED));
		}
		
		public function reorderGuideSections():void
		{
			_sectionChildren = _sectionChildren.sort(orderSections);
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		
		private function orderScenes(s1:GuideSceneData, s2:GuideSceneData): int
		{
			return int(s1.order) - int(s2.order);
		}
		
		private function orderSections(s1:GuideSectionData, s2:GuideSectionData): int
		{
			return int(s1.order) - int(s2.order);
		}
		
		public function get order():int
		{	return _data['order'] }
		
		public function set order(value:int):void
		{	
			if(order != value)
			setProperty('order', value) 
		}
		
		[Bindable (event="change")]
		public function get title():String
		{ 	
			return getNotNullString('title');
		}
		
		public function set title(value:String):void
		{	setProperty('title', value) 
			dispatchEvent(new Event(TITLE_CHANGED));
		}
		
		
		[Bindable (event="change")]
		public function get parent_section_id():String
		{ 	
			return getNotNullString('parent_section_id');
		}
		
		public function set parent_section_id(value:String):void
		{	setProperty('parent_section_id', value) }
		
		[Bindable (event="change")]
		public function get page_image():String
		{ 	
			return getNotNullString('page_image');
		}
		
		public function set page_image(value:String):void
		{	setProperty('page_image', value);
			dispatchEvent(new Event(IMAGE_CHANGED));
		}
		
		public function get page_image_increment():int
		{	return _data['page_image_increment'] }
		
		public function set page_image_increment(value:int):void
		{	_data['page_image_increment'] = value; }
		
		public function get markers():int
		{	return _data['markers'] }
		
		public function set markers(value:int):void
		{	setProperty('markers', value);
			dispatchEvent(new Event(GUIDE_SCENES_CHANGED));
		}
		
		public function get pagePositionX():int
		{	return _data['pagePositionX'] }
		
		public function set pagePositionX(value:int):void
		{	_data['pagePositionX'] = value; }
		
		public function get pagePositionY():int
		{	return _data['pagePositionY'] }
		
		public function set pagePositionY(value:int):void
		{	_data['pagePositionY'] = value; }
		
		
		public function get firstScene():GuideSceneData
		{
			return  _guideScenes.length > 0 ? _guideScenes[0] : null;
		}
		
		public function get firstSceneWithGeo():GuideSceneData
		{
			for each(var gs:GuideSceneData in _guideScenes)
			if(gs.scene.geo.lat != 0){
			return gs;
			}
				
			
			return null;
		}
		
		public function get guideScenes():Vector.<GuideSceneData>
		{
			return _guideScenes;
		}
		
		public function get parentSection():GuideSectionData
		{
			if(parent_section_id)
				for each(var section:GuideSectionData in Model.guideData.guideSections)
				 if(section.id == parent_section_id)
					 return section

			return null
		}
		
		
		public function addGuideScene(scene:GuideSceneData):void
		{
			if(_guideScenes.indexOf(scene) > -1)
				return;
			
			_guideScenes.push(scene);
			_changed = true;
			
			dispatchEvent(new Event(GUIDE_SCENES_CHANGED));
		}
		
		public function addSectionChild(section:GuideSectionData):void
		{
			trace("GuideSectionData.addSectionChild()");
			
/*			if(sectionChildren.indexOf(section) > -1)
				return;*/
			
			_sectionChildren.push(section);
			_changed = true;
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		public function removeGuideScene(scene:GuideSceneData):void
		{
			trace("removeGuideScene()");
			if(_guideScenes.indexOf(scene) == -1)
				return;
			
			_guideScenes.splice(_guideScenes.indexOf(scene), 1);
			_changed = true;
			
			dispatchEvent(new Event(GUIDE_SCENES_CHANGED));
		}
		
		public function removeGuideSection(section:GuideSectionData):void
		{
			trace("GuideSectionData.removedGuideSection");
			if(_sectionChildren.indexOf(section) == -1)
				return;
			
			_sectionChildren.splice(_sectionChildren.indexOf(section), 1);
			_changed = true;

			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		public function getGuideSceneFromId(sceneId:String):GuideSceneData
		{
			for(var i:int = 0; i<_guideScenes.length; i++)
				if(_guideScenes[i].scene.id == sceneId)
					return _guideScenes[i];
			
			return null;
		}

		
		public function get sceneGeos():Vector.<Geo>
		{
			var group:Vector.<Geo> = new Vector.<Geo>;
			for(var j:int =0; j < guideScenes.length; j++)
				group.push(guideScenes[j].scene.geo);
			
			return group;
		}
		
		public function getMapData(style:String):MapData
		{
			var group:Vector.<Geo> = new Vector.<Geo>;
			for(var j:int =0; j < guideScenes.length; j++)
				group.push(guideScenes[j].scene.geo);
			
			var data:MapData = new MapData(320, 284, 20, style);
			if(group.length > 0)
				data.centerAroundAll(group);
			else
				data.center = MapData.DEFAULT_CENTER;
			
			return data;
		}
		
		public override function get data():Object
		{
			var data:Object = super.data;
			data['guideScenes'] = getGuideScenesData();
		//	data['sectionChildren'] = sectionChildren;
			return data;
		}
		
		private function getGuideScenesData():Array
		{
			var data:Array = [];
			for(var i:int = 0; i<_guideScenes.length; i++)
				data.push(_guideScenes[i].data);
			return data;
		}
		
		private function getSectionChildrenData():Array
		{
			var data:Array = [];
			
			for(var i:int = 0; i<_guide.guideSections.length; i++){
				if(_guide.guideSections[i].parent_section_id == this.id)
					data.push(_guide.guideSections[i].data);
			}
			
			//trace(data.length);
			return data;
		}
		
		
		public function get newPageImage():BitmapData
		{	return _newPageImage }
		
		public function set newPageImage(value:BitmapData):void
		{
			_newPageImage = value;
			dispatchEvent(new Event(NEW_IMAGE));
		}
		
		public function get pageImageName():String
		{	
			return "section_" + id + "_image_"+ page_image_increment +".png";
		}
		
		public function markerPositionChanged():void
		{
			dispatchEvent(new Event(MARKER_POSITION));
		}
		
		private function setDecendents():void
		{
			for each(var child:GuideSectionData in sectionChildren){
				_decendents.push(child);
				if(child.sectionChildren){
					for each(var child1:GuideSectionData in child.sectionChildren){
						_decendents.push(child1);
						if(child1.sectionChildren)
							for each(var child2:GuideSectionData in child1.sectionChildren){
							_decendents.push(child2);
							if(child2.sectionChildren)
								for each(var child3:GuideSectionData in child2.sectionChildren){
								_decendents.push(child3);
							}}}}};
		}
		
		public function isDecendant(section:GuideSectionData):Boolean
		{
			
			if(_decendents == null)
				setDecendents();
			
			if(_decendents.indexOf(section) > -1)
				return true
			else
				return false		
		}
		
		public function isAncestor(section:GuideSectionData):Boolean
		{
			var relatives:Vector.<GuideSectionData> = new Vector.<GuideSectionData>
			
			if(parentSection){
				relatives.push(parentSection);
				if(parentSection.parentSection){
					relatives.push(parentSection.parentSection);
					if(parentSection.parentSection.parentSection){
						relatives.push(parentSection.parentSection.parentSection);
						if(parentSection.parentSection.parentSection.parentSection){
							relatives.push(parentSection.parentSection.parentSection.parentSection);
							
						}}}};
			
			if(relatives.indexOf(section) > -1)
				return true
			else
				return false
		}
		
		
		public function isFamily(section:GuideSectionData):Boolean
		{
			return isAncestor(section) || isDecendant(section);
		}		
		
		public function get thumbName():String
		{
			if(firstScene)
				return firstScene.scene.thumbName;
			else if (!sectionChildren.length > 0)
				return null;
			else if(sectionChildren[0].firstScene != null)
					return sectionChildren[0].firstScene.scene.thumbName;
			else if (!sectionChildren[0].sectionChildren.length > 0)
				return null;
			else if(sectionChildren[0].sectionChildren[0].firstScene != null)
				return sectionChildren[0].sectionChildren[0].firstScene.scene.thumbName;
			else if (!sectionChildren[0].sectionChildren[0].sectionChildren.length > 0)
				return null;
			else if(sectionChildren[0].sectionChildren[0].sectionChildren[0].firstScene != null)
				return sectionChildren[0].sectionChildren[0].sectionChildren[0].firstScene.scene.thumbName;
			else return null;
		}
		
	}
}