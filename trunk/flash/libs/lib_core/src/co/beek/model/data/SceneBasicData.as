package co.beek.model.data
{
	import co.beek.map.Geo;
	
	import flash.display.BitmapData;
	import flash.events.Event;

	public class SceneBasicData extends SceneElementData
	{
		public static const PANOPOS_UPDATED:String = "PANOPOS_UPDATED";
		
		private var _geo:Geo;
		
		public function SceneBasicData(data:Object):void
		{
			super(data);
			_geo = new Geo(_data['latitude'], _data['longitude']);
		}

		override public function get data():Object
		{
			var data:Object = super.data;
			data['latitude'] = _geo.lat;
			data['longitude'] = _geo.lon;
			return data;
		}
		
		public function get geo():Geo
		{
			return _geo;
		}
		
		public function set geo(value:Geo):void
		{
			if(_geo.equals(value))
				return;
			
			_geo = value;
			_changed = true;
			dispatchEvent(new Event(Event.CHANGE));
			dispatchEvent(new Event(PANOPOS_UPDATED));
		}
		
		public function get locationId():String
		{	return _data['locationId'] }
		
		public function get locationTitle():String
		{	return _data['locationTitle'] }
		
		[Bindable (event="change")]
		public function get type():int
		{	return _data['type'] }

		public function get locationType():int
		{	return _data['locationType'] }
		
		public function set type(value:int):void
		{	setProperty('type', value) }
		
		public function get panoIncrement():int
		{	return _data['panoIncrement'] }
		
		public function get thumbIncrement():int
		{	return _data['thumbIncrement']; }
		
		public function set thumbIncrement(value:int):void
		{
			setProperty('thumbIncrement', value)
		}
		
		
		public function get thumbName():String
		{	
			if(thumbIncrement > 0)
				return "scene_" + id + "_thumb_" + thumbIncrement + ".jpg";
			
			return null;
		}
	
		[Bindable (event="change")]
		public function get description():String
		{	return getNotNullString('description')  }
		
		public function set description(value:String):void
		{	setProperty('description', value) }
		
		public function get phone():String
		{	return _data['phone']; }
		
		public function get email():String
		{	return _data['email']; }
		
		public function get url():String
		{	return _data['infoURL']; }
		
		public function get video():String
		{	return _data['video']; }
		
		public function set video(value:String):void
		{
			setProperty('video', value) 
		}
		
	}
}