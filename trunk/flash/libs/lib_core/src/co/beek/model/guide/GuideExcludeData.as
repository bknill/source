package co.beek.model.guide
{
	public class GuideExcludeData
	{
		public static function create(guideId:String, type:int):GuideExcludeData
		{
			return new GuideExcludeData({
				'guideId': guideId,
				'type': type
			});
		}
		
		private var _data:Object;
		
		public function GuideExcludeData(data:Object)
		{
			_data = data;
		}
		
		public function get id():String
		{return _data ? _data['id']: ""}
		
		public function get guideId():String
		{return _data ? _data['guideId']: ""}
		
		public function get type():int
		{return _data ? _data['type']:0 }
		
		public function get data():Object
		{	return _data }
	}
}