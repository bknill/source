package co.beek.model
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import co.beek.Log;
	import co.beek.event.NetworkDataEvent;
	import co.beek.event.PanoLookEvent;
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.PanoLook;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.events.StringEvent;
	import co.beek.model.events.TrackingEvent;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.session.SessionData;
	
	public class Model
	{
		public static const SESSION_CHANGE:String = "SESSION_CHANGE";
		
		public static const GUIDE_CHANGE:String = "GUIDE_CHANGE";
		
		public static const GUIDE_UPDATE:String = "GUIDE_UPDATE";
		
		public static const GUIDE_SAVED:String = "GUIDE_SAVED";
		
		public static const SCENE_CHANGE:String = "SCENE_CHANGE";
		
		public static const SCENE_LIKED:String = "SCENE_LIKED";
		
		public static const EMAIL_REQUIRED:String = "EMAIL_REQUIRED";
		
		public static const SECTION_CHANGE:String = "SECTION_CHANGE";

		public static const HOTSPOT_CHANGE:String = "HOTSPOT_CHANGE";
		
		public static const HOTSPOT_IN_VIEW:String = "HOTSPOT_IN_VIEW";

		public static const OFFLINE_ERROR:String = "OFFLINE_ERROR";

		public static const PANO_LOOK:String = "PANO_LOOK";
		
		public static const NETWORK_DATA:String = "NETWORK_DATA";
		
		public static const PLAY_PRESENTATION:String = "PLAY_PRESENTATION";
		
		public static const PAUSE_PRESENTATION:String = "PAUSE_PRESENTATION";
		
		public static const GUIDE_OPEN:String = "GUIDE_OPEN";
		
		public static const GAME_TASK_COMPLETED:String = "GAME_TASK_COMPLETED";
		
		public static const GAME_COMPLETE:String = "GAME_COMPLETE";
		
		public static const GAME_TASK_CHANGED:String = "GAME_TASK_CHANGED";
		
		//public static const GAME_TASK_CHANGED_INSTANT:String = "GAME_TASK_CHANGED_INSTANT";
		
		public static const GAME_HOTSPOT_FOUND:String = "GAME_HOTSPOT_FOUND";
		
		public static const RESET_GAME:String = "RESET_GAME";
		
		public static const HOTSPOT_CLICKED:String = "HOTSPOT_CLICKED";
		
		public static const BOOKMARK_SCENE:String = "BOOKMARK_SCENE";
		
		public static const PROGRESS_NEXT_MANUAL:String = "PROGRESS_NEXT_MANUAL";

		public static const FAVOURITES_PAGENUM_SET:String = "FAVOURITES_PAGENUM_SET";

		public static const INTRO_COMPLETE:String = "INTRO_COMPLETE";
		
		public static const OFFLINE_COMPLETE:String = "OFFLINE_COMPLETE";
		
		public static const GUIDE_INITIALISED:String = "GUIDE_INITIALISED";
		
		public static const PANO_VIDEO_WATCH_TASK_COMPLETE:String = "PANO_VIDEO_WATCH_TASK_COMPLETE";
		
		
		public static const VISITED_COLOR:uint = 0x666666;
		
		private static var dispatcher:EventDispatcher = new EventDispatcher;
		
		private static var _state:State = new State;
		
		private static var _history:History = new History;
		
		private static var _config:IConfig;
	
		
		private static var _sessionData:SessionData;
		
		private static var _currentScene:SceneData;
		
		private static var _currentSection:GuideSectionData;
		
		private static var _guideData:GuideData;
		
		private static var _hotspot:HotspotData;
		
		private static var _hotspotInView:HotspotData;
		
		private static var _appScale:Number;
		
		private static var _appName:String;
		
		private static var _appId:String;
		
		private static var _lockUI:Boolean;
		
		private static var _guideReady:Boolean;
		
		public static var guideSceneList:Vector.<GuideSceneData> = new Vector.<GuideSceneData>;
		public static var guideSectionList:Vector.<GuideSectionData> = new Vector.<GuideSectionData>;
		
		private static var lastFoundHotspot:Boolean = false;
		
		public static function set sessionData(value:SessionData):void
		{
			if(_sessionData == value)
				return;
			
			_sessionData = value;
			dispatcher.dispatchEvent(new Event(SESSION_CHANGE));
		}
		
		public static function get sessionData():SessionData
		{
			return _sessionData;
		}

		public static function get loggedIn():Boolean
		{
			return _sessionData != null;
		}
		
		public static function get state():State
		{
			return _state;
		}
		
		public static function get history():History
		{
			return _history;
		}
		
		public static function back():void
		{
			if(history.hasPrevious() && history.getPrevious() != guideSceneList[1].scene)
				state.loadingScene = history.getPrevious()
			else
				prevScene()
		}
		
		public static function addEventListener(type:String, listener:Function):void
		{
			//trace("addEventListener: "+type);
			dispatcher.addEventListener(type, listener);
		}

		public static function dispatchEvent(event:Event):void
		{
			//trace("dispatchEvent: "+event.type);
			dispatcher.dispatchEvent(event);
		}
		
		public static function removeEventListener(type:String, listener:Function):void
		{
			dispatcher.removeEventListener(type, listener);
		}

		public function addEventListener(type:String, listener:Function):void
		{
			Model.addEventListener(type, listener);
		}

		public function dispatchEvent(event:Event):void
		{
			Model.dispatchEvent(event);
		}
		
		public function removeEventListener(type:String, listener:Function):void
		{
			Model.removeEventListener(type, listener);
		}
		
		public static function get config():IConfig
		{
			return _config;
		}
		
		public static function set config(value:IConfig):void
		{
			_config = value;
		}
		
		public static function set appScale(value:Number):void
		{
			_appScale = value;
		}
		
		public static function get appScale():Number
		{
			if(!_appScale)
				return 1;
			else return _appScale;
		}
		
		
		public static function get guideData():GuideData
		{
			return _guideData;
		}
		
		
		public static function set guideData(value:GuideData):void
		{
			Log.record("Model.setGuideData()");
			
			if(!value)
				throw new Error("cannot set guideData null");
			
			if(value == _guideData && !state.htmlUpdate)
				return;

				
			_guideData = value;
			
			if(!state.htmlUpdate){
				_currentTaskIndex = 0;
				createGuideSceneList();
			}
			
			if(Model.state.htmlUpdate)
				dispatchEvent(new Event(GUIDE_UPDATE));
			
			dispatchEvent(new Event(GUIDE_CHANGE));
		}
		
		
		
		public static function isTypeExcluded(type:int):Boolean
		{	
			return (_guideData != null && _guideData.isExcluded(type));
		}
		
		public static function get currentScene():SceneData
		{
			return _currentScene;
		}
		
		public static function get currentSection():GuideSectionData
		{
			if(_currentSection)
				return _currentSection;
			else
				return _guideData.guideSections[0];
		}
		
		
		public static function get isEditableScene():Boolean
		{
			return _currentScene && _sessionData && (_sessionData.contact.isAdmin() || _sessionData.contact.canEditLocation(_currentScene.location.id));
		}
		

		public static function get isEditableGuide():Boolean
		{
			return _guideData && _sessionData && (_sessionData.contact.isAdmin() || _sessionData.contact.canEditGuide(_guideData.id));
		}
		
		public static function set currentSection(value:GuideSectionData):void
		{
			trace("Model.setCurrentSection()");
			
			if(!value)
				throw new Error("cannot set current section null");
			
			if(value == _currentSection)
				return;
			
			_currentSection = value;
		
			dispatchEvent(new Event(SECTION_CHANGE));
		
		}
		
		
		/**
		 * The currently scene being rendered in the pano.
		 * This is also used by the history back button.
		 */
		public static function set currentScene(value:SceneData):void
		{
			Log.record("Model.setCurrentScene("+value.title+")");
			
			if(!value)
				throw new Error("cannot set current scene null");
			
			if(value == _currentScene)
				return;
			
			// Nullify the hotspot in the prev scene
			_hotspot = null;
			
			// Set the new current scene
			_currentScene = value;
			
			_linkIndex = -1;
			
			// initialize the start look
			//_state.pan = value.initialLook.pan;
			
			if(lastFoundHotspot)
				dispatchGameTaskComplete();
			
			// add the scene to the history
			_history.currentScene = value;
			dispatchEvent(new Event(SCENE_CHANGE));
		}
		
		public static function get guideScene():GuideSceneData
		{
			if(_guideData && _currentScene)
				return _guideData.getGuideSceneFromId(_currentScene.id);
			else if(_guideData)
				return _guideData.firstScene;
			else
				return null;
		}
		
		public function Model()
		{
			super();
		}
		
		/**
		 * @deprecated All instance functions are deprecated in favour of static
		 */
		public function get config():IConfig
		{
			return Model.config;
		}
		
		/**
		 * @deprecated All instance functions are deprecated in favour of static
		 */
		public function get state():State
		{
			return Model.state;
		}
		
		public function set guideData(value:GuideData):void
		{
			Model.guideData = value;
		}
		
		public function get guideData():GuideData
		{
			return Model.guideData;
		}
		
		private static var _favouritesPageNum:int;
		
		public static function set favouritesPageNum(value:int):void
		{
			_favouritesPageNum = value;
			dispatchEvent(new Event(FAVOURITES_PAGENUM_SET));
		}
		
		public static function get favouritesPageNum():int
		{
			return _favouritesPageNum;
		}
		
		
		
		/**
		 * The currently active hotspot in the scene
		 */
		public static function set hotspot(value:HotspotData):void
		{
			trace("Model.setHotspot(" + value + ")");
			if(_hotspot == value)
				return;
			
			_hotspot = value;
			
			if(_hotspot && guideScene)
			{
				var links:Array = guideScene.links;
				for(var i:int; links != null && i<links.length; i++)
					if(links[i] == _hotspot.id)
						_linkIndex = i;
			}
			

			if(lastFoundHotspot)
				dispatchGameTaskComplete();

			
			dispatchEvent(new Event(HOTSPOT_CHANGE));
		}
		
		public static function set hotspotInView(value:HotspotData):void
		{
		 	_hotspotInView = value;
			dispatchEvent(new Event(HOTSPOT_IN_VIEW));
		}	
		
		public static function get hotspotInView():HotspotData
		{return _hotspotInView}	
		
		public static function locationInGuide(LocationId:String):Boolean
		{
			
			if(!Model.config.guideId)
				return true;
			
			var scenes:Vector.<GuideSceneData> = guideData.guideScenes;
			for(var i:int = 0; i<scenes.length; i++)
				if (scenes[i].scene.locationId == LocationId)
					return true;

				return false;	
		}
		
		public static function sceneInGuide(SceneId:String):Boolean
		{
			var scenes:Vector.<GuideSceneData> = guideData.guideScenes;
			for(var i:int = 0; i<scenes.length; i++)
				if (scenes[i].scene.id == SceneId)
					return true;
			
			return false;	
		}
		
		public static function get hotspotsMerged():Vector.<HotspotData>
		{
			var hotspots:Vector.<HotspotData> = new Vector.<HotspotData>;
			if(_currentScene)
				hotspots = _currentScene.hotspots.slice();
			
			if(Model.config.guideId){
			var guideScene:GuideSceneData = Model.guideScene;
			if(guideScene)
				for(var i:int; i<guideScene.hotspots.length; i++)
					hotspots.push(guideScene.hotspots[i]);
			}
			
			return hotspots;
		}

		public static function getHotspot(id:String):HotspotData
		{
			var hotspots:Vector.<HotspotData> = hotspotsMerged;
			for(var i:int; i<hotspots.length; i++)
				if(hotspots[i].id == id)
					return hotspots[i];
			
			return null;
		}
		
		public static function get hotspot():HotspotData
		{
			return _hotspot;
		}
		
		public function get history():History
		{	
			return Model.history;
		}
		
		public static function get scenePath():String
		{  
			return "/scene/"+Model.currentScene.id;
		}
		
		/*public static function get guidePath():String
		{  
			return "guide/"+Model.guideData.id+"-"+Model.guideData.titleCleaned;
		}*/
		/*
		public static function get guideUrl():String
		{  
			return "http://"+Model.config.domain+"/"+guidePath;
		}*/
		
		public static function get currentURL():String
		{
			return _guideData ? guideSceneUrl : sceneUrl
		}
		/**
		 * Returns the path to the current guide/scene. e.g g7/s12
		 */
		public static function get guideSceneUrl():String
		{  
			return "http://"+config.domain
				+"/guide/"+_guideData.id+"-"+_guideData.titleCleaned 
				+"/"+_currentScene.id+"-"+guideSceneTitleCleaned
				+ (_hotspot ? "/"+_hotspot.id : "");
		}
		
		public static function get sceneUrl():String
		{  
			return "http://"+config.domain
				+"/scene/"+_currentScene.id+"-"+cleanForUrl(currentScene.title)
				+ (_hotspot ? "/"+_hotspot.id : "");
		}
		
		private static function get guideSceneTitleCleaned():String
		{
			return guideScene ? cleanForUrl(guideScene.titleMerged) : cleanForUrl(currentScene.title);
		}
		
		private static function cleanForUrl(value:String):String
		{
			while(value.indexOf(" ") > -1)
				value = value.replace(" ", "-");
					
			while(value.indexOf(".") > -1)
				value = value.replace(".", "");
			return escape(value)
		}
		
		public function get currentHotspot():HotspotData
		{
			return Model.hotspot;
		}
		
		public function isTypeExcluded(type:int):Boolean
		{	
			return Model.isTypeExcluded(type);
		}
		
		//category, action
		public static function dispatchTrackingEvent(category:String, action:String, label:String, value:int = 0):void
		{
			dispatchEvent(
				new TrackingEvent(category, action, label, value)
			);
		}

		public static function dispatchOfflineError(name:String):void
		{
			dispatchEvent(
				new StringEvent(OFFLINE_ERROR, name)
			);
		}

		public static function dispatchPanoLookEvent(look:PanoLook):void
		{
			dispatchEvent(
				new PanoLookEvent(PANO_LOOK, look)
			);
			
		}
		
		public static function dispatchNetworkDataEvent(object:Object):void
		{
			dispatchEvent(
				new NetworkDataEvent(NETWORK_DATA, object)
			);
			
		}
		
		public static function get themeColor():uint
		{
			if(guideData)
				return guideData.guideColor;
			
			return Constants.BEEK_BLUE;
		}
		

		
		//private static var _currentTaskIndex:int = -1;
		
		// Hotspots found during this task
		private static var _hotspotsFound:Vector.<HotspotData> = new Vector.<HotspotData>;

		// All hotspots found during this game
		private static var _allHotspotsFound:Vector.<HotspotData> = new Vector.<HotspotData>;
		
		public static function get hasGame():Boolean
		{
			return _guideData && _guideData.gameTasks.length > 0;
		}
		
		public static function registerGameEvent(event:String):void
		{
			if(!hasGame || !currentTask || Model.state.uiState != State.UI_STATE_NORMAL)
				return;
			
			if(currentTask.code == event)
				dispatchGameTaskComplete();
		}
		
		public static function registerHotspotClicked(hotspot:HotspotData):void
		{
			trace("Model.registerHotspotClicked");
			if(!hasGame || !currentTask || currentTask.code != GameTaskData.GAME_FIND_HOTSPOTS)
				return;

			// If the current task code includes the hotspot id
			// and the hotspot has not been found yet.
			if(currentTask.hasHotspotToFind(hotspot) && _hotspotsFound.indexOf(hotspot) == -1)
			{
				_hotspotsFound.push(hotspot);
				_allHotspotsFound.push(hotspot);
				dispatchEvent(new Event(GAME_HOTSPOT_FOUND));
				
				var hotspotsFromInput:Array = currentTask.hotspotsToFind;
				if(hotspotsFromInput.length == _hotspotsFound.length){
					lastFoundHotspot = true;

				}
			}
		}
		
		public static function set currentTaskIndex(value:int):void
		{
			_currentTaskIndex = value;
			dispatchEvent(new Event(GAME_TASK_CHANGED));
		}
		
		public static function get currentTaskIndex():int
		{
			return _currentTaskIndex;
		}
			
		public static function nextGameTask():void
		{
			currentTaskIndex = currentTaskIndex +1;
		}
		
		public static function prevGameTask():void
		{
			currentTaskIndex = currentTaskIndex -1;
		}
		
		private static function dispatchGameTaskComplete():void
		{
			trace("dispatchGameTaskComplete");
			_hotspotsFound = new Vector.<HotspotData>;

			
			dispatchEvent(new Event(GAME_TASK_COMPLETED));
			_currentTaskIndex ++;
			lastFoundHotspot = false;
			
			// dispatch the current task index in the game
			dispatchTrackingEvent(Constants.GA_CATEGORY_GAME, String(_currentTaskIndex), _guideData.id+"|"+_currentScene.id);
		}
		
		public static function panoVideoFinished():void
		{
			trace('Model.panoVideoFinished()');
			dispatchGameTaskComplete();
		}
		
		public static function dispatchGameComplete():void
		{
			trace("dispatchGameComplete");
			dispatchEvent(new Event(GAME_COMPLETE));
		}
		
		public static function get prevTask():GameTaskData
		{
			if(!hasGame || _currentTaskIndex < 0)
				return null;
			
			return _guideData.gameTasks[_currentTaskIndex-1];
		}

		public static function get nextTask():GameTaskData
		{
			if(!hasGame || _currentTaskIndex > _guideData.gameTasks.length-2)
				return null;
			
			return _guideData.gameTasks[_currentTaskIndex+1];
		}
		
		public static function gameRevealsHotspot(hotspot:HotspotData):Boolean
		{
			if(!hasGame || !hotspot)
				return false;
			
			var hotspotsToShow:Array = futureHotspotsToShow;
			for(var i:int; i<hotspotsToShow.length; i++)
				if(hotspotsToShow[i].id == hotspot.id)
					return true;
			
			return false;	
		}

		
		public static function get currentTask():GameTaskData
		{
			if(!hasGame || _currentTaskIndex < 0 || _currentTaskIndex >= _guideData.gameTasks.length)
				return null;
			
			return _guideData.gameTasks[_currentTaskIndex];
		}
		
		public static function get pastHotspotsToShow():Array
		{
			return getHotspotsToShow(0, _currentTaskIndex);
		}
		
		public static function get futureHotspotsToShow():Array
		{
			return getHotspotsToShow(_currentTaskIndex, _guideData.gameTasks.length);
		}
		
		private static function getHotspotsToShow(start:int, end:int):Array
		{
			var arr:Array = [];
			for(var i:int = start; i<end; i++)
				for(var j:int = 0; _guideData != null && j<_guideData.gameTasks[i].hotspotsToShow.length; j++)
					arr.push(_guideData.gameTasks[i].hotspotsToShow[j]);
			return arr;
		}
		
		/*public static function get currentTaskIndex():int
		{
			return _currentTaskIndex;
		}*/
		
		private static var _currentTaskIndex:int;
		
		public static function get isPlayingGame():Boolean
		{
			return _guideData && _guideData.gameTasks.length > 0 
				&& _currentTaskIndex < _guideData.gameTasks.length;
		}
		
		public static function get remainingGameTasks():int
		{
			return  _guideData.gameTasks.length - _currentTaskIndex;
		}
		
		public static function loadGameTask(id:String):void
		{
			for each(var task:GameTaskData in _guideData.gameTasks)
				if(task.id == id)
					_currentTaskIndex = task.order
				
			dispatchEvent(new Event(GAME_TASK_CHANGED));
		}
		
		public static function resetGameTasks():void
		{
			_currentTaskIndex = 0;
			dispatchEvent(new Event(RESET_GAME));
		}
		
		public static function get totalGameTasks():int
		{
			return  _guideData.gameTasks.length;
		}
		
		public static function get isMobile():Boolean
		{
			return Constants.isIOS() || Constants.isAndroid()
		}

		public static function get isDesktop():Boolean
		{
			return Constants.isDesktop()
		}
		
		public static function get hotspotsFound():Vector.<HotspotData>
		{
			return _hotspotsFound;
		}

		public static function get allHotspotsFound():Vector.<HotspotData>
		{
			return _allHotspotsFound;
		}

		
        public static function resetLinkIndex():void
		{
			_linkIndex = -1;
		}
		
		private static var _linkIndex:int = -1;
		
		public static function progressNextManual():void
		{
			progressNext();
			Model.dispatchEvent(new Event(PROGRESS_NEXT_MANUAL))
		}
		
		public static function progressNext():void
		{
			Log.record("Model.progressNext()");
			
			if(guideScene.voiceoverTrack || _state.uiState != State.UI_STATE_NORMAL)
				return;
			
			// video must end
			if(hotspot is VideoData && (VideoData(hotspot).usesYoutube || VideoData(hotspot).preview))
			{
				return;
			}
			
/*			if(hotspot is PhotoData && PhotoData(hotspot).canFlip && !PhotoData(hotspot).flipped)
			{
				PhotoData(hotspot).flipped = true;
				return;
			}*/
			
			if(hotspot is PhotoData && PhotoData(hotspot).buttonScene)
			{
				state.loadingScene = PhotoData(hotspot).buttonScene;
				hotspot = null;
				return;
			}
			
			_linkIndex ++;
			
			var data:AbstractData = Model.guideScene;
			var links:Array = [];
			links = data == guideScene ? guideScene.links : [];
			if(_linkIndex < links.length)
			{
				processLink(links[_linkIndex]);
				return;
			}
			
			if(guideData.guideScenes.length > 1)
				nextScene()
			else
			{
				_linkIndex = -1;
				Model.progressNext()
			}
		}
		
		private static function createGuideSceneList():void
		{
			for each(var section:GuideSectionData in guideData.guideSections)
			for each(var guideScene:GuideSceneData in section.guideScenes)
				guideSceneList.push(guideScene);	
		}
		
		public static function nextScene():void
		{
			Model.state.loadingScene = 
				guideSceneList.indexOf(Model.guideScene) < guideSceneList.length - 1
					? guideSceneList[guideSceneList.indexOf(Model.guideScene) + 1].scene 
					: guideSceneList[0].scene;
		}
		
		public static function prevScene():void
		{
			Model.state.loadingScene = 
				guideSceneList.indexOf(Model.guideScene) > 0
				? guideSceneList[guideSceneList.indexOf(Model.guideScene) - 1].scene 
				: guideSceneList[guideSceneList.length - 1].scene;
		}
		
		private static function processLink(link:String):void 
		{
			Log.record("Model.processLink()");
			if(!link){
				Model.progressNext()
				return;
			}
			
			if(link.indexOf("|") > -1)
			{
				Model.dispatchPanoLookEvent(PanoLook.fromString(link));
			}
			else
			{
				var hotspot:HotspotData = getHotspot(link);
				if(hotspot != null)
					Model.hotspot = hotspot;
				else
					Model.progressNext()
			}
		}
		
		
		public static function videoComplete(data:VideoData):void
		{
			// if the currently active presentation item is the same video
			if(getHotspot(guideScene.links[_linkIndex]) == data)
			{
				hotspot = null;
				progressNext();
			}
		}
		
		public static function loadGuideScene(sceneId:String):void
		{
			if(guideData.getGuideSceneFromId(sceneId))
				state.loadingScene = guideData.getGuideSceneFromId(sceneId).scene;
		}
		
		public static function getGuide(guides:Vector.<GuideBasicData>, id:String):GuideBasicData
		{
			for(var i:int=0; i<guides.length; i++)
				if(guides[i].id == id)
					return guides[i];
			
			return null;
		}

		public static function get appName():String
		{
			return _appName;
		}

		public static function set appName(value:String):void
		{
			_appName = value;
		}

		public static function get appId():String
		{
			return _appId;
		}

		public static function set appId(value:String):void
		{
			_appId = value;
		}

		public static function get lockUI():Boolean
		{
			return _lockUI;
		}

		public static function set lockUI(value:Boolean):void
		{
			_lockUI = value;
		}

		public static function get guideReady():Boolean
		{
			return _guideReady;
		}

		public static function set guideReady(value:Boolean):void
		{
			_guideReady = value;
			
			dispatchEvent(new Event(GUIDE_INITIALISED));
		}

		
	}
}