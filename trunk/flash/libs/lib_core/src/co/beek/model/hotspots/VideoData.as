package co.beek.model.hotspots
{
	import flash.net.NetStream;
	
	public class VideoData extends MediaData
	{
		public static function createForScene(id:String, sceneId:String):VideoData
		{
			return new VideoData({
				"id":id,
				"sceneId":sceneId,
				"frame":FRAME_NONE,
				"volume":0.5,
				"ambience":0.5,
				"selectable":true
			});
		}
		
		public static function createForGuide(id:String, guideSceneId:String):VideoData
		{
			return new VideoData({
				"id":id,
				"guideSceneId":guideSceneId,
				"frame":FRAME_NONE,
				"volume":0.5,
				"ambience":0.5,
				"selectable":true
			});
		}
		
		public static const FRAME_NONE:int = 0;
		public static const FRAME_PROJECTOR:int = 1;
		public static const FRAME_WALL_TV:int = 2;
		public static const FRAME_TV:int = 3;
		
		private var _upload:NetStream;
		
		public function VideoData(data:Object)
		{
			super(data);
		}
		
		public override function get title():String
		{	return _data['title'] ? _data['title'] : "Video"; }
		
		/*public function get listenHotspotId():String
		{	
			return _data['listenHotspotId'];
		}
		
		public function set listenHotspotId(value:String):void
		{
			setProperty('listenHotspotId', value);
		}*/
		
		public override function get selectionEnabled():Boolean
		{
			return _data['selectable'];
		}
		
		public function set selectionEnabled(value:Boolean):void
		{
			setProperty('selectable', value);
		}
		
		public function get usesYoutube():Boolean
		{	
			return file && file.fileName.length == 11 && file.fileName.indexOf(".") == -1;
		}
		
		public function get videoId():String
		{
			return file.fileName;
		}
		
		public function get looping():Boolean
		{	return _data['looping'] }
		
		public function set looping(value:Boolean):void
		{	_data['looping'] = value; }
		
		public function get ambience():Number
		{	return getNumber('ambience') }
		
		public function set ambience(value:Number):void
		{	setProperty('ambience', Math.max(0.1, Math.min(value, 1))) }
		
		public function get frame():int
		{	return _data['frame'];	}
		
		public function set frame(frameId:int):void
		{ 	setProperty('frame', frameId);	}
		
		public function get volume():Number
		{	return getNumber('volume') }
		
		/**
		 * Need to be able to set volume to Zero on video.
		 */
		public function set volume(value:Number):void
		{	setProperty('volume', Math.max(0, Math.min(value, 1))) }
		
		public function get preview():String
		{	return _data['preview'];	}
		
		public function set preview(value:String):void
		{ 	setProperty('preview', value);	}
	}
}