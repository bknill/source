package co.beek.model.hotspots
{
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;
	
	import co.beek.model.data.SceneBasicData;

	public class BoardData extends HotspotData
	{
		public static const MAX_SIGNS:int = 4;
		
		public static const SCENES_CHANGED:String = "SCENES_CHANGED";
		
		public static function create(sceneId:String):BoardData
		{
			return new BoardData({
				"id": new Uuid().toString(),
				"sceneId":sceneId
			});
		}
		
		private var _signs:Vector.<BoardSignData> = new Vector.<BoardSignData>();

		public function BoardData(data:Object)
		{
			super(data);

			if(!data['boardSigns'])
				return;

			var raw:Array = data['boardSigns'];
			for(var i:int = 0; i < raw.length; i++)
				_signs.push(new BoardSignData(raw[i]));
			
			reorderSigns();
		}
		
		public function reorderSigns():void
		{
			_signs = _signs.sort(orderSigns);
		}
		
		private function orderSigns(s1:BoardSignData, s2:BoardSignData): int
		{
			return int(s1.scene.id) - int(s2.scene.id);
		}

		override public function get data():Object {
			var data:Object = super.data;
			data['boardSigns'] = [];
			for(var i:int = 0; i < _signs.length; i++)
				data['boardSigns'].push(_signs[i].data);

			return data;
		}

		public override function get title():String {
			return _data['title'] ? super.title:"SIGNBOARD";
		}

		[Bindable (event="change")]
		public function get boardSigns():Vector.<BoardSignData> 
		{
			return _signs;
		}

		public function addSign(sign:BoardSignData):void
		{
			trace("addSceneId()");
			if(_signs.indexOf(sign) > -1)
				return;
			
			_signs.push(sign);
			
			// remove the first sign
			if(_signs.length > MAX_SIGNS)
				_signs.splice(0, 1);

			reorderSigns();
			
			dispatchEvent(new Event(SCENES_CHANGED));
		}

		public function removeScene(scene:SceneBasicData):void
		{
			for(var i:int = 0; i < _signs.length; i++)
				if(_signs[i].scene.id == scene.id)
					removeSign(_signs[i]);
		}
		
		public function removeSign(sign:BoardSignData):void
		{
			if(_signs.indexOf(sign) == -1)
				return;

			_signs.splice(_signs.indexOf(sign), 1);
			reorderSigns();
			
			dispatchEvent(new Event(SCENES_CHANGED));
		}
		
		public override function get rotationXEnabled():Boolean
		{
			return true;
		}
		
		public override function get rotationYEnabled():Boolean
		{
			return true;
		}
		
		public override function get rotationZEnabled():Boolean
		{
			return true;
		}
	}
}