package co.beek.model
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.system.Capabilities;
	
	
	public class Constants
	{
		public static const BEEK_BLUE:uint = 0x00a8e1;
		
		public static const HEADER_HEIGHT:int = 30;
		
		public static const MIN_STAGE_WIDTH:int = 700;
		
		public static const CIRCLE_MENU_RADIUS:int = 800;
		
		public static const TYPE_UNDEFINED:int = 0;
		public static const TYPE_ACTIVITIES:int = 1;
		public static const TYPE_ACCOMMODATION:int = 2;
		public static const TYPE_ATTRACTION:int = 3;
		public static const TYPE_ENTERTAINMENT:int = 4;
		public static const TYPE_RESTAURANT:int = 5;
		public static const TYPE_TRANSPORTATION:int = 6;
		public static const TYPE_VENUE:int = 7;

		
		public static const TYPE_SHOPPING:int = 8;
		public static const TYPE_SWIMMING:int = 9;
		public static const TYPE_PARKING:int = 10;
		public static const TYPE_HOTEL:int = 11;
		public static const TYPE_BOAT:int = 12;
		public static const TYPE_COFFEE:int = 13;
		public static const TYPE_INFORMATION:int = 14;
		public static const TYPE_AIRPORT:int = 15;
		public static const TYPE_HOUSE:int = 16;
		public static const TYPE_WINE:int = 17;
		public static const TYPE_BEER:int = 18;
		public static const TYPE_THEATRE:int = 19;
		public static const TYPE_PLAY:int = 20;
		public static const TYPE_BEACH:int = 21;
		public static const TYPE_ART:int = 22;
		public static const TYPE_MAORI:int = 23;
		
		// Google analitics catagory CATEGORY
		public static const GA_CATEGORY_NAVIGATION:String = "navigation"
		public static const GA_CATEGORY_CONVERSION:String = "conversion"
		public static const GA_CATEGORY_FUNCTIONS:String = "functions"
		public static const GA_CATEGORY_SHARE:String = "share"
		public static const GA_CATEGORY_GUIDE:String = "guide"
		public static const GA_CATEGORY_GAME:String = "game"
		public static const GA_CATEGORY_FAVOURITES:String = "favourites"
		public static const GA_CATEGORY_BOOKCASE:String = "bookcase"
		public static const GA_CATEGORY_CONTENT_ELEMENT:String = "content element"; 
		
		public static var typeNames:Array = [];
		{
			typeNames[TYPE_UNDEFINED] = "Undefined";
			typeNames[TYPE_ACTIVITIES] = "Activities";
			typeNames[TYPE_ACCOMMODATION] = "Accommodation";
			typeNames[TYPE_ATTRACTION] = "Attraction";
			typeNames[TYPE_ENTERTAINMENT] = "Entertainment";
			typeNames[TYPE_RESTAURANT] = "Restaurant";
			typeNames[TYPE_TRANSPORTATION] = "Transportation";
			typeNames[TYPE_VENUE] = "Venue";
			
			typeNames[TYPE_SHOPPING] = "Shopping";
			typeNames[TYPE_SWIMMING] = "Swimming";
			typeNames[TYPE_PARKING] = "Parking";
			typeNames[TYPE_HOTEL] = "Hotel";
			typeNames[TYPE_BOAT] = "Boat";
			typeNames[TYPE_COFFEE] = "Coffee";
			typeNames[TYPE_INFORMATION] = "Information";
			typeNames[TYPE_AIRPORT] = "Airport";
			typeNames[TYPE_HOUSE] = "Home";
			typeNames[TYPE_WINE] = "Wine";
			typeNames[TYPE_BEER] = "Beer";
			typeNames[TYPE_THEATRE] = "Theatre";
			typeNames[TYPE_PLAY] = "Play";
			typeNames[TYPE_BEACH] = "Beach";
			typeNames[TYPE_ART] = "Art";
			typeNames[TYPE_MAORI] = "Maori";
		}
		
		public static function getIconsDictionary(ui:Sprite):Dictionary
		{
			var dict:Dictionary = new Dictionary;
			
			dict[TYPE_ACTIVITIES] = ui['activitiesIcon'];
			dict[TYPE_ACCOMMODATION] = ui['accommodionIcon'];
			dict[TYPE_ATTRACTION] = ui['attractionsIcon'];
			dict[TYPE_ENTERTAINMENT] = ui['entertainmentIcon'];
			dict[TYPE_RESTAURANT] = ui['restaurantIcon'];
			dict[TYPE_TRANSPORTATION] = ui['transportIcon'];
			dict[TYPE_VENUE] = ui['venueIcon'];

			dict[TYPE_SHOPPING] = ui['iconShopping'];
			dict[TYPE_SWIMMING] = ui['iconSwimming'];
			dict[TYPE_PARKING] = ui['iconParking'];
			dict[TYPE_HOTEL] = ui['iconHotel'];
			dict[TYPE_BOAT] = ui['iconBoat'];
			dict[TYPE_COFFEE] = ui['iconCoffee'];
			dict[TYPE_INFORMATION] = ui['iconInformation'];
			dict[TYPE_AIRPORT] = ui['iconAirport'];
			dict[TYPE_HOUSE] = ui['iconHome'];
			dict[TYPE_WINE] = ui['iconWine'];
			dict[TYPE_BEER] = ui['iconBeer'];
			dict[TYPE_THEATRE] = ui['iconTheatre'];
			dict[TYPE_PLAY] = ui['iconPlay'];
			dict[TYPE_BEACH] = ui['iconBeach'];
			dict[TYPE_ART] = ui['iconArt'];
			dict[TYPE_MAORI] = ui['iconMaori'];
			
			return dict;
		}
		
		
		public static const WEB:String = "web"; // first gen is 1,1
		public static const IPHONE_1G:String = "iPhone1,1"; // first gen is 1,1
		public static const IPHONE_3G:String = "iPhone1"; // second gen is 1,2
		public static const IPHONE_3GS:String = "iPhone2"; // third gen is 2,1
		public static const IPHONE_4:String = "iPhone3"; // normal:3,1 verizon:3,3
		public static const IPHONE_4S:String = "iPhone4"; // 4S is 4,1
		public static const IPHONE_5PLUS:String = "iPhone";
		public static const TOUCH_1G:String = "iPod1,1";
		public static const TOUCH_2G:String = "iPod2,1";
		public static const TOUCH_3G:String = "iPod3,1";
		public static const TOUCH_4G:String = "iPod4,1";
		public static const TOUCH_5PLUS:String = "iPod";
		public static const IPAD_1:String = "iPad1"; // iPad1 is 1,1
		public static const IPAD_2:String = "iPad2"; // wifi:2,1 gsm:2,2 cdma:2,3
		public static const IPAD_3:String = "iPad3"; // (guessing)
		public static const IPAD_4PLUS:String = "iPad";
		public static const UNKNOWN:String = "unknown";
		public static const ANDROID:String = "Android";
		
		public static const IOS_DEVICES:Array = [IPHONE_1G, IPHONE_3G, IPHONE_3GS,
			IPHONE_4, IPHONE_4S, IPHONE_5PLUS, IPAD_1, IPAD_2, IPAD_3, IPAD_4PLUS,
			TOUCH_1G, TOUCH_2G, TOUCH_3G, TOUCH_4G, TOUCH_5PLUS];
		
		public static function usesOnlyPreviewTiles(device:String):Boolean
		{
			return device == Constants.IPAD_1 || isOldPhone(device);
		}
		
		public static function isAndroid():Boolean
		{
			return (Capabilities.version.substr(0,3) == "AND");
		}
		public static function isIOS():Boolean
		{
			return (Capabilities.version.substr(0,3) == "IOS");
		}
		
		public static function isOldPhone(device:String):Boolean
		{
			return new Array(IPHONE_3G, IPHONE_3GS, IPHONE_4, IPHONE_4S).indexOf(device) > -1;
		}
		
		public static function isDesktop():Boolean
		{
			return !isIOS() && !isAndroid() 
		}
		
		public static  var isAir : Boolean = (Capabilities.playerType == "Desktop");
		public static  var isFlashPlayer : Boolean = (Capabilities.playerType == "StandAlone");
		public static  var isBrowser : Boolean = (Capabilities.playerType == "ActiveX" || Capabilities.playerType == "PlugIn");
		public static  var isOther : Boolean = (Capabilities.playerType == "External");
		
		
	}
}