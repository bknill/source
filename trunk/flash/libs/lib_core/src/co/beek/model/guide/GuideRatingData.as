package co.beek.model.guide
{
	public class GuideRatingData
	{
		private var _data:Object;
		
		public function GuideRatingData(data:Object)
		{
			_data = data;
		}
		
		public function get id():String
		{return _data ? _data['id']: ""}
		
		public function get guideId():String
		{return _data ? _data['guideId']: ""}
		
		public function get rating():String
		{return _data ? _data['rating']: ""}
		
		public function get data():Object
		{	return _data }
	}
}