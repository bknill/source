package co.beek.model.data
{
	import co.beek.map.Geo;
	
	import flash.events.Event;
	

	public class LocationData extends AbstractData
	{
		/**
		 * The medium logo is resized to fit within a square
		 * with dimensions not greater than 130
		 */
		//public static const LOGO_SQUARE:int = 130;
		
		private var _defaultScene:SceneBasicData;
		
		private var _geo:Geo;
		
		//private var _logoFile:FileData;
		
		public function LocationData(data:Object):void
		{
			super(data);
			_geo =  new Geo(_data['latitude'], _data['longitude']);
			
			// TODO remove this check
			if(data['defaultScene'])
				_defaultScene = new SceneBasicData(data['defaultScene']);
			
			//if(_data['logoFile'])
			//	_logoFile = new FileData(_data["logoFile"]);
		}
		
		
		override public function get data():Object
		{
			var data:Object = super.data;
			data['latitude'] = _geo.lat;
			data['longitude'] = _geo.lon;
			return data;
		}
		
		public function get teamId():String
		{	return _data['teamId'] }
		
		public function get destinationId():String
		{	return _data['destinationId'] }
		
		
		public function get type():int
		{	return _data['type']; }
		
		public function set type(value:int):void
		{	setProperty("type", value) }
		
		/**
		 * The title of the Location
		 */
		[Bindable (event="change")]
		public function get title():String
		{	return getNotNullString('title') }
		
		public function set title(value:String):void
		{	setProperty("title", value) }
		
		/**
		 * The description of the Location
		 */
		[Bindable (event="change")]
		public function get description():String
		{	return getNotNullString('description') }
		
		public function set description(value:String):void
		{	setProperty("description", value) }

		/**
		 * The description of the Location
		 */
		public function get defaultScene():SceneBasicData
		{	return _defaultScene }
		
		public function set defaultScene(value:SceneBasicData):void
		{	
			_defaultScene = value;
			setProperty("defaultSceneId", value.id);
		}
		
		/**
		 * The description of the Location
		 */
		public function get defaultSceneId():String
		{	
			return _data['defaultSceneId'] != "0"
				? _data['defaultSceneId']
				: null;
		}
		
		public function set defaultSceneId(value:String):void
		{	setProperty("defaultSceneId", value) }
		
		/**
		 * The url used in the Info button
		 */
		[Bindable (event="change")]
		public function get infoUrl():String
		{	
			if(_data['infoURL'] == "")
				return null;
			
			return _data['infoURL'];
		}
		
		public function set infoUrl(value:String):void
		{	setProperty("infoURL", value) }

		/**
		 * The url used when the user clicks the Book Button
		 */
		[Bindable (event="change")]
		public function get bookingUrl():String
		{	
			if(_data['bookingURL'] == "")
				return null;
			
			return _data['bookingURL'];
		}
		
		public function set bookingUrl(value:String):void
		{	setProperty("bookingURL", value) }
		
		/**
		 * The Address of the Location
		 */
		[Bindable (event="change")]
		public function get address():String
		{	return _data['address'] }
		
		public function set address(value:String):void
		{	setProperty("address", value) }
		
		/**
		 * The phone ofthe location
		 */
		[Bindable (event="change")]
		public function get phone():String
		{	return _data['phone']; }
		
		public function set phone(value:String):void
		{	setProperty("phone", value) }
		
		/**
		 * The email of the location
		 */
		[Bindable (event="change")]
		public function get email():String
		{	return _data['email']; }
		
		public function set email(value:String):void
		{	setProperty("email", value) }
		
		public function get geo():Geo
		{
			return _geo;
		}
		
		public function set geo(value:Geo):void
		{
			if(_geo.equals(value))
				return;
			
			_geo = value;
			_changed = true;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}