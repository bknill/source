package co.beek.model.data
{
	import co.beek.admin.ErrorStack;
	import co.beek.utils.StringUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	
	public class AbstractData extends EventDispatcher
	{
		/*public static function id(data:Object):String
		{
			return new AbstractData(data).id;
		}*/
		
		protected var _data:Object;
		
		protected var _changed:Boolean;
		
		public function AbstractData(data:Object)
		{
			super();
			this.data = data;
		}
		
		public function set data(value:Object):void
		{
			if(!value)
				throw new Error("Data is null in "+ getQualifiedClassName(this));
			
			_data = value;
		}
		
		
		public function get data():Object
		{
			return _data;
		}

		public function get changed():Boolean
		{
			return _changed;
		}
		
		public function get id():String
		{	return _data['id'] }
		
		protected function getProperty(property:String):String
		{
			return _data[property];
		}
		
		protected function getNumber(property:String):Number
		{
			return _data[property] ? _data[property] : 0;
		}
		
		
		protected function getNotNullString(property:String):String
		{
			return _data[property] ? _data[property] : "";
		}
		
		protected function setProperty(property:String, value:Object):void
		{
			if(_data[property] == value)
				return;
			
			if(value === "")
				value = null;
			
			_data[property] = value;
			
			_changed = true;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}