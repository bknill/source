package co.beek.model.hotspots
{
	/**
	 * Base class for all hotspots that can appear in both scenes and guidescenes.
	 * i.e Photos, Posters, Videos, Sounds
	 */
	public class GuideSceneHotspot extends HotspotData
	{
		public function GuideSceneHotspot(data:Object=null)
		{
			super(data);
		}
		
		/**
		 * Set the id when cloning guidescene
		 */
		public function set id(value:String):void
		{
			_data['id'] = value;
		}
		
		public function get guideSceneId():String
		{	return _data['guideSceneId'] }
		
		public function set guideSceneId(value:String):void
		{	_data['guideSceneId'] = value }
	}
}