package co.beek.model.data
{
	public class TwitterStatus
	{
		private var _data:Object;
		
		public function TwitterStatus(data:Object)
		{
			_data = data;
		}
		
		public static function create(status:String, user:String, date:String):TwitterStatus
		{
			return new TwitterStatus({
				"status":status,
				'user':user,
				'date':date
			});
		}
		
		
		public function get user():String
		{	return _data['user']  }
		
		public function get date():String
		{	return _data['date'] }
		
		public function get status():String
		{	return _data['status'] }
		
		public function get data():Object
		{	return _data }
		
	}
}