package co.beek.model.guide
{
	import co.beek.map.Geo;
	import co.beek.map.MapData;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GuideSceneData;
	
	import flash.events.Event;
	
	public class GuideMapData extends co.beek.map.MapData
	{
		public static const SCENES_CHANGE:String = "SCENES_CHANGE";
		
		private var _currentScene:SceneData;
		private var _guideScenes:Vector.<GuideSceneData>;
		
		public function GuideMapData(width:Number, height:Number, zoom:int, style:String)
		{
			super(width, height, zoom, style);
		}
		
		public function set currentScene(value:SceneData):void
		{
			_currentScene = value;
			center = value.geo;
			dispatchEvent(new Event(CHANGE));
		}
		
		public function set guideScenes(value:Vector.<GuideSceneData>):void
		{
			_guideScenes = value;
			dispatchEvent(new Event(SCENES_CHANGE));
		}
		
		public function zoomToShowAll():void
		{
			center = _currentScene.geo;
			zoom 14;
			for(var j:int =0; j < _guideScenes.length; j++)
				if(_guideScenes[j].scene.id != _currentScene.id)
					zoomToFit(_guideScenes[j].scene.geo);
		}
		
		public function get guideScenes():Vector.<GuideSceneData>
		{
			return _guideScenes;
		}
	}
}