package co.beek.model.guide
{
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.utils.JSONBeek;
	
	public class GameTaskHotspot
	{
		public static function create(hotspot:HotspotData, scene:SceneElementData):GameTaskHotspot
		{
			return new GameTaskHotspot({
				'id' : hotspot.id,
				'title' : hotspot.title//,
				//'sceneId' : scene.id,
				//'scene' : scene.data
			});
		}
		
		public static function serializeHotspots(hs:Array):String
		{
			return JSONBeek.encode(hs);
		}
		
		public static function deserializeHotspots(value:String):Array
		{
			var hotspots:Array = new Array;
			if(value == null || value == "")
				return hotspots;
			
			var raw:Array = JSONBeek.decode(value);
			for(var i:int; i<raw.length; i++)
				hotspots.push(new GameTaskHotspot(raw[i]))
			return hotspots;
		}
		
		public static function findHotspot(hotspots:Array, hotspotId:String):GameTaskHotspot
		{
			for(var i:int; i<hotspots.length; i++)
				if(hotspots[i].id == hotspotId)
					return hotspots[i];
			
			return null;
		}
		
		public static function hasHotspot(input:String, hotspot:HotspotData):Boolean
		{
			var hotspots:Array = GameTaskHotspot.deserializeHotspots(input);
			return findHotspot(hotspots, hotspot.id) != null;
		}
		
		public static function removeHotspot(value:String, hotspotId:String):String
		{
			var hotspots:Array = GameTaskHotspot.deserializeHotspots(value);
			var hotspot:GameTaskHotspot = findHotspot(hotspots, hotspotId)
			
			if(hotspot != null)
				hotspots.splice(hotspots.indexOf(hotspot), 1);
			
			return GameTaskHotspot.serializeHotspots(hotspots);
		}
		
		public static function addHotspot(value:String, hotspot:HotspotData, scene:SceneElementData):String
		{
			var hotspots:Array = GameTaskHotspot.deserializeHotspots(value);
			if(GameTaskHotspot.findHotspot(hotspots, hotspot.id) != null)
				return value;
			
			hotspots.push(GameTaskHotspot.create(hotspot, scene));
			return GameTaskHotspot.serializeHotspots(hotspots);
		}
		
		private var _data:Object;
		
		//private var _scene:SceneElementData;
		
		public function GameTaskHotspot(data:Object)
		{
			_data = data;
			//_scene = new SceneElementData(data['scene'])
		}
		
		public function get id():String
		{	return _data['id'] }

		public function get title():String
		{	return _data['title'] }
		
		/**
		 * I am not sure if this is used anywhere.
		public function get scene():SceneElementData
		{	return _scene }
		
		public override function get data():Object
		{
			var d:Object = _data;
			d['scene'] = _scene.data;
			return d;
		}
		 */
	}
}