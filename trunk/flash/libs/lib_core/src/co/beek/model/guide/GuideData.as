package co.beek.model.guide
{
	import co.beek.map.Tile;
	import co.beek.model.data.AbstractData;
	import co.beek.model.data.SceneData;
	import co.beek.model.Model;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	
	public class GuideData extends AbstractData
	{
		public static const COLOR_CHANGE:String = "COLOR_CHANGE";

		public static const NEW_THUMB:String = "NEW_THUMB";
		
		public static const GUIDE_SECTIONS_CHANGED:String = "GUIDE_SECTIONS_CHANGED";

		public static const EXCLUDES_CHANGED:String = "EXCLUDES_CHANGED";

		public static const FIRST_SCENE_UPDATED:String = "FIRST_SCENE_UPDATED";

		public static const GUIDE_LOCKED_CHANGED:String = "GUIDE_LOCKED_CHANGED";

		public static const GUIDE_FONT_CHANGED:String = "GUIDE_FONT_CHANGED";

		public static const GUIDE_SIZE_CHANGED:String = "GUIDE_SIZE_CHANGED";
		
		/**
		 * Defaults used when no data is found
		 */
		//public static const DEFAULT_MAP_STYLE:String =  "37964";

		public static const DEFAULT_SCENE_COLOR:String =  "1C82C0";

		public static const DEFAULT_LOCATION_COLOR:String =  "69A7CB";

		public static const DEFAULT_DESTINATION_COLOR:String =  "FEB300";
		
		private var _guideSections:Vector.<GuideSectionData>;
		
		private var _gameTasks:Vector.<GameTaskData>;
		
		private var _guideExcludes:Vector.<GuideExcludeData>;
		
		private var _newThumb:BitmapData;
		
		private var _firstScene:GuideSceneData;
		
		public function GuideData(data:Object)
		{
			super(data);
			parseGuideSections(_data["guideSections"]);
			parseGuideExcludes(_data['guideExcludes']);
			parseGuideTasks(_data['gameTasks']);
			updateFirstScene();
			
			reorderGameTasks();
			
		//	_firstScene = firstSection ? firstSection.firstScene : null;
		}
		
		
		[Bindable (event="GUIDE_SIZE_CHANGED")]
		public function get width():int
		{
			return _data['width'] ? _data['width'] : 330;
		}
		
		public function set width(value:int):void
		{	
			_data['width'] = value;
			dispatchEvent(new Event(GUIDE_SIZE_CHANGED));
		}

		[Bindable (event="GUIDE_SIZE_CHANGED")]
		public function get height():int
		{
			return _data['height'] ? _data['height'] : 330;
		}
		
		public function set height(value:int):void
		{	
			_data['height'] = value;
			dispatchEvent(new Event(GUIDE_SIZE_CHANGED));
		}
		
		[Bindable (event="GUIDE_FONT_CHANGED")]
		public function get font():int
		{
			return _data['font'];
		}
		
		public function set font(value:int):void
		{	
			_data['font'] = value;
			dispatchEvent(new Event(GUIDE_FONT_CHANGED));
		}
		
		private function parseGuideSections(raw:Array):void
		{
			trace('parseGuideSections');
			_guideSections = new Vector.<GuideSectionData>;
			
			if(!raw)
				return;
			
			for(var i:int = 0; i<raw.length; i++)
			{
				var section:GuideSectionData = new GuideSectionData(raw[i]);
				section.addEventListener(Event.CHANGE, onHotspotDataChange);
				section.addEventListener(GuideSectionData.GUIDE_SCENES_CHANGED, onGuideScenesChange);
				_guideSections.push(section);
			}
			
			_guideSections = _guideSections.sort(orderSections);
			
		}

		
		private function onHotspotDataChange(event:Event):void
		{
			dispatchEvent(event);
		}

		private function onGuideScenesChange(event:Event):void
		{
			if(event.target == firstSection 
				&& _firstScene != firstSection.firstScene)
				updateFirstScene();
				
		}
		
		private function parseGuideExcludes(raw:Array):void
		{
			_guideExcludes = new Vector.<GuideExcludeData>;
			
			if(!raw)
				return;
			
			for(var i:int = 0; i<raw.length; i++)
				_guideExcludes.push(new GuideExcludeData(raw[i]));
		}

		private function parseGuideTasks(raw:Array):void
		{
			_gameTasks = new Vector.<GameTaskData>;
			
			if(!raw)
				return;
			
			for(var i:int = 0; i<raw.length; i++)
				_gameTasks.push(new GameTaskData(raw[i]));
		}
		
		[Bindable (event="FIRST_SCENE_UPDATED")]
		public function get firstScene():GuideSceneData
		{
			return _firstScene;
		}
		
		private function updateFirstScene():void
		{
			trace('guideData.updateFirstScene()');
			var f:GuideSceneData = firstSection ? firstSection.firstScene : null;
			if(_firstScene == f)
				return;
			
			_firstScene = f;
			
			dispatchEvent(new Event(FIRST_SCENE_UPDATED));
		}
		
		public function get otherguides():int
		{	return _data['otherguides'] }
		
		public function set otherguides(value:int):void
		{	
			_data['otherguides'] = value;
		}
		
		public function get thumbIncrement():int
		{	return _data['thumbIncrement'] }
		
		public function set thumbIncrement(value:int):void
		{	setProperty('thumbIncrement', value) }
		
		public function get thumbName():String
		{	
			if (thumbIncrement == 0)
				return "guide_default_thumb_1.png";
			
			return "guide_" + id + "_thumb_" + thumbIncrement + ".png";
		}
		
		public function get newThumb():BitmapData
		{	return _newThumb }
		
		public function set newThumb(value:BitmapData):void
		{
			_newThumb = value;
			dispatchEvent(new Event(NEW_THUMB));
		}
		
		[Bindable (event="change")]
		public function get coverDesign():String
		{	return _data['coverDesign'] }
		
		public function set coverDesign(value:String):void
		{	setProperty('coverDesign', value) }
		
		public function addExclude(value:int):void
		{
			_guideExcludes.push(GuideExcludeData.create(id, value));
		}

		public function removeExclude(value:int):void
		{
			for(var i:int = 0; i<_guideExcludes.length; i++)
				if(_guideExcludes[i].type == value)
					_guideExcludes.splice(i, 1);
		}
		
		public function reorderGuideSections():void
		{
			_guideSections = _guideSections.sort(orderSections);
			updateFirstScene();
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}

		private function orderSections(l1:GuideSectionData, l2:GuideSectionData): int
		{
			return int(l1.order) - int(l2.order);
		}
		
		private function reorderGameTasks():void
		{
			_gameTasks = _gameTasks.sort(orderTasks);
		}
		
		private function orderTasks(l1:GameTaskData, l2:GameTaskData): int
		{
			return int(l1.order) - int(l2.order);
		}
		
		public function isExcluded(type:int):Boolean
		{
			for(var i:int = 0; i<_guideExcludes.length; i++)
				if(_guideExcludes[i].type == type)
					return true;
			
			return false;
		}
		
		public function get teamId():String
		{	return _data['teamId'] }
		
		public function get phone():String
		{	return _data['phone'] }
		
		public function get email():String
		{	return _data['email'] }
		
		/**
		 * The title of the guide as used on the front cover
		 */
		[Bindable (event="change")]
		public function get title():String
		{	return getNotNullString('title') }
		
		public function set title(value:String):void
		{	setProperty('title', value) }
		
		public function get titleCleaned():String
		{	return escape(title.replace(/ /g, "-")) }
		
		/**
		 * The title of the guide as used on the front cover
		 */
		[Bindable (event="change")]
		public function get destinationId():String
		{	
			if(_data['destinationId'] == null)
				return "1";
			
			return getNotNullString('destinationId');
		}
		
		/**
		 * The style used for hte maps in the guide
		 */
		[Bindable (event="COLOR_CHANGE")]
		public function get mapStyle():String
		{	
			if(_data['mapStyle'] == null || _data['mapStyle'] == "")
				return Tile.STYLE_DEFAULT_MAP;
			
			return _data['mapStyle'];
		}
		
		public function set mapStyle(value:String):void
		{	
			setProperty("mapStyle", value);
			dispatchEvent(new Event(COLOR_CHANGE));
		
		}
		
		/**
		 * The color used in scene hotspots, guide cover, etc.
		 */
		[Bindable (event="COLOR_CHANGE")]
		public function get guideColor():uint
		{	return uint("0x"+ (_data["colorScene"] ? _data["colorScene"] : DEFAULT_SCENE_COLOR)) }
		
		public function set guideColor(value:uint):void
		{	
			setProperty("colorScene", value.toString(16));
			dispatchEvent(new Event(COLOR_CHANGE))
		}
		
		/**
		 * The description of the Location
		 */
		[Bindable (event="change")]
		public function get description():String
		{	return getNotNullString('description') }
		
		public function set description(value:String):void
		{	setProperty("description", value) }
		
		public function get guideSections():Vector.<GuideSectionData>
		{
			return _guideSections;
		}
		
		/*public function get gameLocked():Boolean
		{
			return _data['gameLocked'];
		}*/
		
		public function set gameLocked(value:Boolean):void
		{	
			_data['gameLocked'] = value;
			dispatchEvent(new Event(GUIDE_LOCKED_CHANGED));
		}
		
		public function get gameCode():String
		{
			return _data['gameCode'];
		}
		
		public function set parent(value:String):void
		{	
			_data['parent'] = value;
		}
		
		public function get parent():String
		{
			return _data['parent'];
		}
		
		public function set gameCode(value:String):void
		{	
			_data['gameCode'] = value;
			dispatchEvent(new Event(GUIDE_LOCKED_CHANGED));
		}
			
		public function get gameTasks():Vector.<GameTaskData>
		{
			return _gameTasks;
		}
		
		public function addGameTask(task:GameTaskData, index:int):void
		{
			_gameTasks.splice(index, 0, task);
			// Update the order of the tasks for saving to the db
			for(var i:int = 0; i<_gameTasks.length; i++)
				_gameTasks[i].order = i;
			
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}

		public function removeGameTask(task:GameTaskData):void
		{
			_gameTasks.splice(_gameTasks.indexOf(task), 1);
			
			// Update the order of the tasks for saving to the db
			for(var i:int = 0; i<_gameTasks.length; i++)
				_gameTasks[i].order = i;
			
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		public function get firstSection():GuideSectionData
		{
			for each(var section:GuideSectionData in _guideSections)
				if(section.order == 0 && !section.parent_section_id)
					return section;
			
			return _guideSections[0];
		}
		
		public function addGuideSection(section:GuideSectionData):void
		{
			if(_guideSections.indexOf(section) > -1)
				return;
			
			_guideSections.push(section);
			_changed = true;
			
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		public function removeGuideSection(section:GuideSectionData):void
		{
			if(_guideSections.indexOf(section) == -1)
				return;
			
			_guideSections.splice(_guideSections.indexOf(section), 1);
			_changed = true;
			
			dispatchEvent(new Event(GUIDE_SECTIONS_CHANGED));
		}
		
		override public function get data():Object
		{	
			var data:Object = super.data;
			data["guideSections"] = getGuideSectionsData();
			data["guideExcludes"] = getGuideExcludesData();
			data["gameTasks"] = getGuideTasksData();
			return data;
		}
		
		private function getGuideSectionsData():Array
		{
			var data:Array = [];
			for(var i:int = 0; i < _guideSections.length; i++)
				data.push(_guideSections[i].data);
			return data;
		}

		private function getGuideTasksData():Array
		{
			var data:Array = [];
			for(var i:int = 0; i < _gameTasks.length; i++)
				data.push(_gameTasks[i].data);
			return data;
		}
		
		
		
		public function get guideScenes():Vector.<GuideSceneData>
		{
			var guideScenes:Vector.<GuideSceneData> = new Vector.<GuideSceneData>;
			for(var i:int = 0; i<_guideSections.length; i++)
				for(var j:int = 0; j<_guideSections[i].guideScenes.length; j++)
					guideScenes.push(_guideSections[i].guideScenes[j]);
			return guideScenes;	
		}
		
		public function getGuideSceneFromId(sceneId:String):GuideSceneData
		{
			var guideScene:GuideSceneData = null;
			for(var i:int = 0; i<_guideSections.length && guideScene == null; i++)
				if(_guideSections[i].getGuideSceneFromId(sceneId) != null)
					return _guideSections[i].getGuideSceneFromId(sceneId);
			
			return null;
			
		}
		
		
			

		public function getGuideSectionForScene(sceneId:String):GuideSectionData
		{
			for(var i:int = 0; i<_guideSections.length; i++)
				if(_guideSections[i].getGuideSceneFromId(sceneId) != null)
					return _guideSections[i];
			
			return null;
		}
		
		public function getDataForPage(page:int):AbstractData
		{
			var p:int = _gameTasks.length + 1;
			for(var i:int = 0; i<_guideSections.length; i++)
			{
				p ++;
				if(p == page)
					return _guideSections[i];
				
				var guideScenes:Vector.<GuideSceneData> = _guideSections[i].guideScenes;
				for(var j:int = 0; j< guideScenes.length; j++)
				{
					p ++;
					if(p == page)
						return guideScenes[j];
				}
				
			}
			return null;
		}
		
		
		
		
		public function getGuideSection(sectionId:String):GuideSectionData
		{
			for(var i:int = 0; i<_guideSections.length; i++)
				if(_guideSections[i].id == sectionId)
					return _guideSections[i];
			
			return null;
		}
		
		public function get topLevelGuideSections():Vector.<GuideSectionData>
		{
			var sections:Vector.<GuideSectionData> = new Vector.<GuideSectionData>;
			for(var i:int = 0; i<_guideSections.length; i++)
				if(!_guideSections[i].parent_section_id)
					sections.push(_guideSections[i])
			
			return sections
		}
		
		public function getGuideScene(scene:SceneData):GuideSceneData
		{
			return getGuideSceneFromId(scene.id);
		}

		private function getGuideExcludesData():Array
		{
			var data:Array = [];
			for(var i:int = 0; i < _guideExcludes.length; i++)
				data.push(_guideExcludes[i].data);
			return data;
		}
		
		public function getNextScene(guideScene:GuideSceneData):GuideSceneData
		{
			for(var i:int = 0; i<_guideSections.length; i++)
				for(var j:int = 0; j<_guideSections[i].guideScenes.length-1; j++)
					if(guideScene.id == _guideSections[i].guideScenes[j].id)
						return _guideSections[i].guideScenes[j+1];
			
			return null;
		}

		public function getNextSection(guideScene:GuideSceneData):GuideSectionData
		{
			for(var i:int = 0; i<_guideSections.length; i++)
				if(_guideSections[i].id == guideScene.sectionId)
					return _guideSections[i+1];
			
			return null;
		}
	}
}