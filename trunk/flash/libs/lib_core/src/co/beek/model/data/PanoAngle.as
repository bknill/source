package co.beek.model.data
{
	import co.beek.utils.MathUtil;
	
	import flash.geom.Point;
	import flash.geom.Vector3D;

	public class PanoAngle
	{
		public var _pan:Number;
		public var _tilt:Number;
		
		public function PanoAngle(pan:Number, tilt:Number)
		{
			_pan = pan;
			_tilt = tilt;
		}
		
		public function get pan():Number
		{
			return _pan;
		}
		
		public function get tilt():Number
		{
			return _tilt;
		}
		
		

		/*
		public function equals(o:Orientation):Boolean
		{
			if(!o)
				return false;
			
			return _distance == o.distance
				&& _tilt == o.tilt
				&& pan == o.pan;
		}
		
		public function get vector():Vector3D
		{
			var pr:Number = -1 * (_pan - 90) * MathUtil.TO_RADIANS;
			var tr:Number = -1 * _tilt * MathUtil.TO_RADIANS;
			return new Vector3D(
				distance * Math.cos(pr) * Math.cos(tr),
				distance * Math.sin(tr),
				distance * Math.sin(pr) * Math.cos(tr)
			);
		}
		
		*/
	}
}