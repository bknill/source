package co.beek.model
{
	import co.beek.Log;
	import co.beek.model.data.SceneData;
	
	import flash.events.EventDispatcher;

	public class History extends EventDispatcher
	{
		private var _scenes:Vector.<SceneData> = new Vector.<SceneData>;
		
		private var _currentScene:SceneData;
		
		private var i:int = 0;
		
		public function History()
		{
			super();
		}
		
		public function set currentScene(value:SceneData):void
		{
			Log.record("History.currentScene(" + value.id + ")");
			_currentScene = value;
			
			if(!contains(value.id)){
				_scenes.push(value);
			}
		
		}
		
		public function get historyIndex():int
		{
			return _scenes.indexOf(_currentScene)
		}
		

		public function get scenes():Vector.<SceneData>
		{
			return _scenes;
		}
		
		public function contains(sceneId:String):Boolean
		{
			return getFromHistory(sceneId) != null
		}
		
		public function getFromHistory(sceneId:String):SceneData
		{
			for(var i:int=0; i<_scenes.length; i++)
				if(_scenes[i].id == sceneId)
					return _scenes[i];
			
			return null;
		}
		
		
		public function isHome():Boolean
		{
			return _currentScene != null && _currentScene.id == _scenes[0].id;
		}
		
		public function getHome():SceneData
		{
			return _scenes[0];
		}
		
		public function loadHome():void
		{
			Model.state.loadingScene =  _scenes[0];
		}
		
		public function hasPrevious():Boolean
		{
			return historyIndex > 0;
		}
		
		public function getPrevious():SceneData
		{
			return _scenes.pop();
		}
		
		public function hasNext():Boolean
		{
			return historyIndex < _scenes.length - 1;
		}
		
		public function getNext():SceneData
		{
			
			return _scenes[historyIndex+1];
		}
		

		
	}
}