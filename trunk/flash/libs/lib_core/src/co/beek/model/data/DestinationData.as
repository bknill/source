package co.beek.model.data
{
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	
	import flash.events.Event;
	

	public class DestinationData extends AbstractData
	{
		private var _geoZoom:GeoZoom;
		
		public function DestinationData(data:Object):void
		{
			super(data);
			_geoZoom = new GeoZoom(_data['latitude'], _data['longitude'], _data['zoom']);
		}
		
		public function get geoZoom():GeoZoom
		{
			return _geoZoom;
		}
		
		public function set geoZoom(value:GeoZoom):void
		{
			if(_geoZoom.equals(value))
				return;
			
			_geoZoom = value;
			_changed = true;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		/**
		 * The title of the Location
		 */
		[Bindable (event="change")]
		public function get title():String
		{	return getNotNullString('title') }
		
		public function set title(value:String):void
		{	setProperty("title", value) }
		
		/**
		 * The description of the Location
		 */
		[Bindable (event="change")]
		public function get description():String
		{	return getNotNullString('description') }
		
		public function set description(value:String):void
		{	setProperty("description", value) }
		
		override public function get data():Object
		{
			var data:Object = super.data;
			data['latitude'] = _geoZoom.lat;
			data['longitude'] = _geoZoom.lon;
			data['zoom'] = _geoZoom.zoom;
			return data;
		}
	}
}