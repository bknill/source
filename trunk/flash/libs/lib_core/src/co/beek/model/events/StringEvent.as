package co.beek.model.events
{
	import flash.events.Event;
	
	public class StringEvent extends Event
	{
		private var _value:String;
		
		
		//category, action, opt_label, opt_value
		public function StringEvent(type:String, value:String)
		{
			super(type);
			_value = value;
		}
		
		public function get value():String
		{
			return _value;
		}
	}
}