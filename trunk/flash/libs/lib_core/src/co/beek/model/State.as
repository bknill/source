package co.beek.model
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	
	import co.beek.Log;
	import co.beek.model.data.SceneElementData;
	
	public class State extends EventDispatcher
	{
		public static const LOADING_SCENE_CHANGE:String = "SCENE_ID_CHANGE";

		public static const GUIDE_ID_CHANGE:String = "GUIDE_ID_CHANGE";
		
		public static const NETWORK_CHANGE:String = "NETWORK_CHANGE";
		
		public static const START_CHANGE:String = "START_CHANGE";
		
		public static const GAME_VIEW_CHANGE:String = "GAME_VIEW_CHANGE";

		public static const UI_STATE_CHANGE:String = "UI_STATE_CHANGE";
		
		public static const GUIDE_PAGE_CHANGE:String = "GUIDE_PAGE_CHANGE";

		public static const GAME_COMPLETE:String = "GAME_COMPLETE";
		
		public static const GUIDE_VIEW_CHANGE:String = "GUIDE_VIEW_CHANGE";
		
		public static const INTRO_COMPLETE:String = "INTRO_COMPLETE";
		
		public static const MAP_SELECTED:String = "MAP_SELECTED";
		
		public static const PANO_ENGAGED:String = "PANO_ENGAGED";
		
		public static const PANO_MOVE:String = "PANO_MOVE";
		
		public static const PANO_STOP:String = "PANO_STOP";	
		
		public static const AUTOPLAY_CHANGE:String = "AUTOPLAY_CHANGE";
		
		public static const PANO_TILES_LOADED:String = "PANO_TILES_LOADED";
		
		public static const PANO_SNAPSHOT_READY:String = "PANO_SNAPSHOT_READY";

		public static const PANO_DEEP_TILES_LOADED:String = "PANO_DEEP_TILES_LOADED";
		
		public static const SCENE_BUTTON_CLICKED:String = "SCENE_BUTTON_CLICKED";
		
		public static const MENU_BUTTON_CLICKED:String = "MENU_BUTTON_CLICKED";
		
		public static const VOLUME_CHANGE:String = "VOLUME_UPDATE";
		
		public static const ACTIVATED:String = "ACTIVATED";
		
		public static const MODE_CHANGE:String = "MODE_CHANGE";
		
		public static const VR_MODE_CHANGE:String = "VR_MODE_CHANGE";
		
		public static const CONNECT_MODE_CHANGE:String = "CONNECT_MODE_CHANGE";

		public static const CONNECT_NETWORK_CHANGE:String = "CONNECT_NETWORK_CHANGE";
		
		public static const CONNECT_STATE_CHANGE:String = "CONNECT_STATE_CHANGE";

		public static const NETWORK_CONNECT:String = "NETWORK_CONNECT";
		
		public static const NETWORK_DISCONNECT:String = "NETWORK_DISCONNECT";
		
		public static const GUIDE_VIEW_SCENE:int = 1; 
		public static const GUIDE_VIEW_SCENE_AND_SECTION:int = 2; 
		public static const GUIDE_VIEW_SECTION:int = 3; 
		public static const GUIDE_VIEW_SETTINGS:int = 4; 
		public static const GUIDE_VIEW_CLOSED:int = 5; 

		public static const UI_STATE_NORMAL:int = 0; // normal
		public static const UI_STATE_EDITING_SCENE:int = 1; // normal
		public static const UI_STATE_EDITING_GUIDE:int = 2; // normal
		public static const UI_STATE_EDITING_GAME:int = 3; // normal
		public static const UI_STATE_EDITING_VO:int = 4; // normal
		//public static const UI_STATE_PRESENTATION_MODE:int = 3; // normal
		
		public static const MODE_DRAG:int = 0;
		public static const MODE_AUTO:int = 1;
		public static const MODE_GYRO:int = 2;
		public static const MODE_REMOTE:int = 3;
		
		public static const CONNECT_MODE_BROADCAST:int = 0;
		public static const CONNECT_MODE_RECEIVE:int = 1;
		public static const CONNECT_MODE_CONTROL:int = 2;
		public static const CONNECT_NETWORK_WIFI:int = 0;
		public static const CONNECT_NETWORK_INTERNET:int = 1;
		public static const CONNECT_NETWORK_BLUETOOTH:int = 2;
		
		private var _networkAccess:Boolean;
		
		private var _hideGame:Boolean;
		
		private var _guideId:String;
		
		private var _loadingScene:SceneElementData;
		
		private var _guideView:int = GUIDE_VIEW_CLOSED;

		private var _uiState:int = UI_STATE_NORMAL;
		
		private var _guidePageNum:int = 0;
		
		private var _mode:int = MODE_DRAG;
		
		private var _connectMode:int;
		private var _connectNetwork:int;
		
		
		private var _volume:Number = 1;
		
		private var _mute:Boolean;
		
		private var _active:Boolean;
		
		
		private var _guideViewCurrentScene:Boolean;

		private var _pan:Number;

		private var _tilt:Number;

		private var _zoom:Number;
		
		private var _small:Boolean;
		
		private var _vr:Boolean = false;
		
		private var _connected:Boolean = false;
		
		private var _offline:Boolean = false;
		
		private var _startState:String;
		
		private var _connectState:String;
		
		private var _mobileAppId:String;
		
		private var _distriqKey:String;
		
		private var _guideZero:Point;
		
		private var _guideWidth:Number;
		
		private var _htmlUpdate:Boolean;
		
		
		
		public function State(target:IEventDispatcher=null)
		{
			super(target);
		}
		


		/**
		 * This is needed for guided tours
		 */
		public function set networkAccess(value:Boolean):void
		{
			if(_networkAccess == value)
				return;
			_networkAccess = value;
			dispatchEvent(new Event(NETWORK_CHANGE));
		}
		
		public function get networkAccess():Boolean
		{	return _networkAccess }
		
		public function set hideGame(value:Boolean):void
		{
			trace("State.hideGame("+value+")");
			_hideGame = value;
			dispatchEvent(new Event(GAME_VIEW_CHANGE));
		}
		
		public function get hideGame():Boolean
		{	return _hideGame }

		/**
		 * update hte state of hte ui
		 */
		public function set uiState(value:int):void
		{
			trace('State.set uiState(' +value+ ')');
			if(_uiState == value)
				return;
			
			_uiState = value;
			dispatchEvent(new Event(UI_STATE_CHANGE));
		}
		
		
		public function get uiState():int
		{	return _uiState }
		

		public function set guideId(value:String):void
		{
			if(_guideId == value || value == null)
				return;
			_guideId = value;
			dispatchEvent(new Event(GUIDE_ID_CHANGE));
		}
		
		public function get guideId():String
		{	return _guideId }
		
		/**
		 * This is needed for guided tours
		 */
		public function set loadingScene(value:SceneElementData):void
		{
			
			if(_loadingScene != null && _loadingScene.id == value.id)
				return;
			
			Log.record("Model.set loadingScene("+value.id+")");
			
			_loadingScene = value;
	
			dispatchEvent(new Event(LOADING_SCENE_CHANGE));
		}
		
		public function get loadingScene():SceneElementData
		{	return _loadingScene }
		
		/**
		 * The current page in the guide
		 */
		public function set guideView(value:int):void
		{
			trace("State.setGuideView("+ value +")");
			if(_guideView == value)
				return;
			
			_guideView = value;
			dispatchEvent(new Event(GUIDE_VIEW_CHANGE));
		}
		
		public function get guideView():int
		{	return _guideView }
		
		/**
		 * The control mode
		 */

		public function set mode(value:int):void
		{
			trace("State.setMode("+ value +")");
			if(_mode == value)
				return;
			
			_mode = value;
			dispatchEvent(new Event(MODE_CHANGE));
		}
		
		public function get mode():int
		{	return _mode }
		
		public function set connectMode(value:int):void
		{
			trace("State.set connectMode("+ value +")");
			if(_connectMode == value)
				return;

			if(value == CONNECT_MODE_RECEIVE)
				if(mode != MODE_REMOTE)
					mode = MODE_REMOTE;
			
			_connectMode = value;
			dispatchEvent(new Event(CONNECT_MODE_CHANGE));
		}
		
		public function get connectMode():int
		{	return _connectMode }
		
		public function set connectNetwork(value:int):void
		{
			trace("State.set connectMode("+ value +")");
			if(_connectNetwork == value)
				return;
			
			_connectNetwork = value;
			dispatchEvent(new Event(CONNECT_NETWORK_CHANGE));
		}
		
		public function get connectNetwork():int
		{	return _connectNetwork }
		
		
		/**
		 * Volume is a value between 0-1;
		 */
		public function get volume():Number
		{
			return _mute ? 0 : _volume;
		}
		
		public function set volume(value:Number):void
		{
			if(isNaN(value) || _volume == value)
				return;
			trace("State.setVolume( " +value+")");
			// max range 0 ~ 1	
			_volume = Math.max(0, Math.min(1, value));
			_mute = false;
			
			dispatchEvent(new Event(VOLUME_CHANGE));
		}

		/**
		 * Volume is a value between 0-1;
		 */
		public function get mute():Boolean
		{
			return _mute;
		}
		
		public function get active():Boolean
		{
			return _active;
		}
		
		
		public function set active(value:Boolean):void
		{
			if(_active == value)
				return;
			
			_active = value;
			dispatchEvent(new Event(ACTIVATED));
		}
		
		
		
		public function get vr():Boolean
		{
			return _vr;
		}
		
		
		public function set vr(value:Boolean):void
		{
			_vr = value;
			
			dispatchEvent(new Event(VR_MODE_CHANGE));
			
			if(value == true)
			  mode = MODE_GYRO;
		}	
		
		public function get connected():Boolean
		{
			return _connected;
		}
		
		
		public function set connected(value:Boolean):void
		{
			trace("set connected" + value);
/*			var location:String = new Error().getStackTrace().match( /(?<=\/|\\)\w+?.as:\d+?(?=])/g )[1].replace( ":" , ", line " );
			trace( location );*/
			if(_connected != value)
				_connected = value;
			
			//if(!_connected && mode == MODE_REMOTE)
			//	mode = MODE_DRAG;
			
			if(_connected)
				dispatchEvent(new Event(NETWORK_CONNECT));

		}	
		
		public function get offline():Boolean
		{
			return _offline;
		}
		
		
		public function set offline(value:Boolean):void
		{
			_offline = value;
		}
		
		public function get connectState():String
		{
			return _connectState;
		}
		
		
		public function set connectState(value:String):void
		{
			if(_connectState == value)
				return;
			
			_connectState = value;
			
			dispatchEvent(new Event(CONNECT_STATE_CHANGE));
			
		}	

		public function get startState():String
		{
			return _startState;
		}
		
		
		public function set startState(value:String):void
		{
			if(_startState == value)
				return;
			
			_startState = value;
			dispatchEvent(new Event(START_CHANGE));
			
		}	
		
		public function get mobileAppId():String
		{
			return _mobileAppId;
		}
		
		
		public function set mobileAppId(value:String):void
		{
	
			_mobileAppId = value;
			
		}	
		
		public function get distriqKey():String
		{
			return _distriqKey;
		}
		
		
		public function set distriqKey(value:String):void
		{
			_distriqKey = value;
		}	
		
		
		public function set mute(value:Boolean):void
		{
			if(_mute == value)
				return;
			
			_mute = value;
			dispatchEvent(new Event(VOLUME_CHANGE));
		}
		
		public function get small():Boolean
		{
			return _small;
		}
		
		
		public function set small(value:Boolean):void
		{
			_small = value;
		}
		
		public function get guideViewCurrentScene():Boolean
		{
			return _guideViewCurrentScene;
		}
		
		public function set guideViewCurrentScene(value:Boolean):void
		{
			_guideViewCurrentScene = value;
		}
		
		public function get guideZero():Point
		{
			return _guideZero
		}
		
		public function set guideZero(value:Point):void
		{
			_guideZero = value;
		}
		
		public function get guideWidth():Number
		{
			return _guideWidth
		}
		
		public function set guideWidth(value:Number):void
		{
			_guideWidth = value;
		}
		
		public function get htmlUpdate():Boolean
		{
			return _htmlUpdate;
		}
		
		public function set htmlUpdate(value:Boolean):void
		{
			_htmlUpdate = value;
		}
		
		public function dispatchMapEvent():void
		{
			dispatchEvent(new Event(MAP_SELECTED));
		}
		
	}
}