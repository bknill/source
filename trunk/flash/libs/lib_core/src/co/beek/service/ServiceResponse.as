package co.beek.service
{
	
	
	public class ServiceResponse
	{
		public static function create(p:Object):Object
		{
			return {
				'payload' : p,
				'error' : 0,
				'message' : null
			}
		}
		
		private var _data:Object;
		
		public function ServiceResponse(data:Object)
		{
			_data = data;
		}
		
		public function get payload():Object
		{	return _data['payload'] }
		
		public function get error():int
		{	return _data['error'] }
		
		public function get message():String
		{	return _data['message'] }
		
	}
}