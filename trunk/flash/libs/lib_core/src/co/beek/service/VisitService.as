package co.beek.service
{
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.events.StringEvent;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.service.ServiceLoader;
	import co.beek.service.ServiceResponse;
	import co.beek.Log;

	public class VisitService
	{
		public static const FAVOURITE_EMAIL_REQUIRED:String = "FAVOURITE_EMAIL_REQUIRED";
		
		public static const FAVOURITE_SCENE_EMAIL_REQUIRED:String = "FAVOURITE_SCENE_EMAIL_REQUIRED";

		public static const FAVOURITE_EMAIL_SUBMITTED:String = "FAVOURITE_EMAIL_SUBMITTED";
		
		public static const FAVOURITES_SENT:String = "FAVOURITES_SENT";
		
		
		private static var dispatcher:EventDispatcher = new EventDispatcher;
		
		private static var _key:String = new Uuid().toString();
		
		private static var favourited:Array = new Array();
		
		private static var _favouritedScenes:Vector.<GuideSceneData> = new Vector.<GuideSceneData>;
		
		private static var _email:String;
		
		private static var _user_id:String;
		
		private static var _first_name:String;
		
		private static var _last_name:String;
		
		public static function addEventListener(type:String, listener:Function):void
		{
			dispatcher.addEventListener(type, listener);
		}
		
		public static function removeEventListener(type:String, listener:Function):void
		{
			dispatcher.removeEventListener(type, listener);
		}
		
		public static function dispatchEvent(event:Event):void
		{
			dispatcher.dispatchEvent(event);
		}
		
		public static function get key():String
		{
			return _key;
		}
		
		public static function get user_id():String
		{
			return _user_id;
		}
		
		public static function get first_name():String
		{
			return _first_name;
		}
		
		public static function get last_name():String
		{
			return _last_name;
		}
		
		public static function set user_id(value:String):void
		{
			_user_id = value;
		}
		
		public static function set first_name(value:String):void
		{
			 _first_name = value;
		}
		
		public static function set last_name(value:String):void
		{
			 _last_name = value;
		}
		
		
		public static function get favouritedScenes():Vector.<GuideSceneData>
		{
			return _favouritedScenes;
		}
		
		public static function get email():String
		{
			return _email;
		}
		public static function set email(value:String):void
		{
			if(!value || value.indexOf("@") < 1 || _email == value)
				return;
			
			_email = value;
			
			var url:String = Model.config.serviceUrl+"/user/bind/{guideId}/{key}/{email}/{user_id}/{first_name}/{last_name}/";
			url = url.replace("{guideId}", Model.state.guideId);
			url = url.replace("{key}", _key);
			url = url.replace("{email}", _email);
			url = url.replace("{user_id}", _user_id);
			url = url.replace("{first_name}", _first_name);
			url = url.replace("{last_name}", _last_name);
			
			var loader:ServiceLoader = new ServiceLoader(ServiceResponse);
			loader.load(url, emailBoundComplete);
		}
		
		private static function emailBoundComplete(result:Object):void
		{
			dispatchEvent(new Event(FAVOURITE_EMAIL_SUBMITTED));
		}
		
		public static function favourite(text:String):void
		{
			if(_email)
			{

				var path:String = getFavouritePath(Model.hotspot);
				if(favourited.indexOf(path) > -1)
					return;
				
				favourited.push(path)
				ServiceLoader.load(path, onFavouriteServiceResponse, ServiceResponse);
				
				var hotspotId:String = Model.hotspot ? Model.hotspot.id : "";
				var action:String = getFavouriteAction(Model.hotspot);
				Model.dispatchTrackingEvent(Constants.GA_CATEGORY_FAVOURITES, action, hotspotId);
				
				dispatchEvent(new Event(Model.SCENE_LIKED));
			}
			else
			{
				dispatchEvent(new StringEvent(FAVOURITE_EMAIL_REQUIRED, text));
			}
		}
		
		public static function beenHere():void
		{
			if(_email)
			{
				
				var path:String = getBeenHerePath();
				if(favourited.indexOf(path) > -1)
					return;
				
				favourited.push(path)
				ServiceLoader.load(path, onFavouriteServiceResponse, ServiceResponse);
				
				var hotspotId:String =  "";
				var action:String = "beenHere";
				Model.dispatchTrackingEvent(Constants.GA_CATEGORY_FAVOURITES, action, hotspotId);
				
				dispatchEvent(new Event(Model.SCENE_LIKED));
			}
		}
		
		public static function favouriteScene(value:GuideSceneData):void
		{
			if(_email ){
				if(_favouritedScenes.indexOf(value) == -1){
					_favouritedScenes.push(value);
					//favourite(Model.guideSceneUrl);
				}
			}
			else
			{
				Model.dispatchEvent(new Event(Model.EMAIL_REQUIRED));
			}
		}
		
		public static function sendFavourites():void
		{
			var path:String = "user/{email}/send/{guideId}"
			path = path.replace("{email}", _email);
			path = path.replace("{guideId}", Model.guideData.id || Model.state.guideId);
			ServiceLoader.load(path, onFavouriteServiceResponse, ServiceResponse);
			_email = null;
			_favouritedScenes.length = 0;
			_key = new Uuid().toString();
		}
		
		private static function getFavouriteAction(hotspot:HotspotData):String
		{
			if(!hotspot)
				return "scene";
			
			if(hotspot is PhotoData)
				return "photo";
			
			if(hotspot is PosterData)
				return "poster";
			
			return "hotspot";
		}
		
		/**
		 * The path that is used to log this 'favourite'
		 */
		private static function getFavouritePath(hotspot:HotspotData):String
		{
			var path:String = "user/{email}/favourite/{guideId}/{sceneId}/{hotspotId}/{type}/1";
			path = path.replace("{email}", _email);
			path = path.replace("{guideId}", Model.guideData.id);
			path = path.replace("{sceneId}", Model.currentScene.id);
			path = path.replace("{hotspotId}", hotspot ? hotspot.id : "0");
			path = path.replace("{type}", getFavouriteType(hotspot));
			
			return path;
		}
		
		private static function getBeenHerePath():String
		{
			var path:String = "user/{email}/favourite/{guideId}/{sceneId}/{hotspotId}/{type}/0";
			path = path.replace("{email}", _user_id);
			path = path.replace("{guideId}", Model.guideData.id);
			path = path.replace("{sceneId}", Model.currentScene.id);
			path = path.replace("{hotspotId}",  "0");
			path = path.replace("{type}",  "0");
			
			return path;
		}
		
		private static function getFavouriteType(hotspot:HotspotData):int
		{		
			if(hotspot is PhotoData)
				return 1;
			
			if(hotspot is PosterData)
				return 2;
			
			return 0;
			
		}
		
		public static function hasBeenFavourited(hotspot:HotspotData):Boolean
		{
			return favourited.indexOf(getFavouritePath(hotspot)) > -1;
		}
		
		private static function onFavouriteServiceResponse(response:SecurityError):void
		{
			Log.record("VisitService.onFavouriteServiceResponse()");
			dispatchEvent(new Event(FAVOURITES_SENT));
		}
		
		public static function getNewKey():String
		{
			_key = new Uuid().toString();
			return _key;
		}
	}
}