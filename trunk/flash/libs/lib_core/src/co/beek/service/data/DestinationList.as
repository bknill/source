package co.beek.service.data
{
	import co.beek.model.data.DestinationData;
	import co.beek.service.ISerializable;

	public class DestinationList implements ISerializable
	{
		private var _destinations:Vector.<DestinationData>;
		
		public function DestinationList(data:Object)
		{
			_destinations = new Vector.<DestinationData>
			for(var i:int = 0; i<data.length; i++)
				_destinations.push(new DestinationData(data[i]));
		}
		
		public function get destinations():Vector.<DestinationData>
		{
			return _destinations;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_destinations.length; i++)
				data.push(_destinations[i].data);
			
			return data;
		}
	}
}