package co.beek.service
{
	import co.beek.model.guide.GuideBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.service.data.DestinationList;
	import co.beek.service.data.GuideBasicList;
	import co.beek.service.data.GuideSceneBasicList;
	import co.beek.Log;

	public class GuideService
	{	
		public static function getGuideJson(guideId:String, callback:Function):void
		{
			Log.record("GuideService.getGuideJson()");
			var path:String = "guide/{guideId}/json";
			path = path.replace("{guideId}", guideId);
			
			ServiceLoader.load(path, callback, GuideData);
		}
		
		public static function getGuideBasicJson(guideId:String, callback:Function):void
		{
			Log.record("GuideService.getGuideBasicJson()");
			var path:String = "guide/{guideId}/jsonbasic";
			path = path.replace("{guideId}", guideId);
			
			ServiceLoader.load(path, callback, GuideBasicData);
		}
		
		public static function getGuideScenes(guideId:String, callback:Function):void
		{
			Log.record("GuideService.getGuideScenes()");
			var path:String = "guide/{guideId}/scenes";
			path = path.replace("{guideId}", guideId);

			ServiceLoader.load(path, callback, GuideSceneBasicList);
		}
		
		public static function getLibraryGuides(callback:Function):void
		{
			ServiceLoader.load("guides/library/", callback, GuideBasicList);
		}

		public static function searchGuides(value:String, callback:Function):void
		{
			var path:String = "guides/search/{value}/";
			path = path.replace("{value}", escape(value.split(" ").join(",")));
			
			ServiceLoader.load(path, callback, GuideBasicList);
		}
		
		public static function getGuideChildren(value:String, callback:Function):void
		{
			var path:String = "guide/{value}/children";
			path = path.replace("{value}", escape(value.split(" ").join(",")));
			trace(path);
			ServiceLoader.load(path, callback, GuideBasicList);
		}

		public static function getDestinations(callback:Function):void
		{
			ServiceLoader.load("destinations/", callback, DestinationList);
		}
	}
}