package co.beek.service.data
{
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.service.ISerializable;

	public class GuideSceneBasicList implements ISerializable
	{
		private var _guideScenes:Vector.<SceneBasicData>;
		
		public function GuideSceneBasicList(data:Object)
		{
			_guideScenes = new Vector.<SceneBasicData>
			for(var i:int = 0; i<data.length; i++)
				_guideScenes.push(new SceneBasicData(data[i]));
		}
		
		public function get guideScenes():Vector.<SceneBasicData>
		{
			return _guideScenes;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_guideScenes.length; i++)
				data.push(_guideScenes[i].data);
			
			return data;
		}
	}
}