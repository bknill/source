package co.beek.service
{
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.utils.JSONBeek;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.UncaughtErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class ServiceLoader
	{
		public static function load(path:String, callback:Function, payloadClass:Class):ServiceLoader
		{
			Log.record("ServiceLoader.load(" + path +")");
			
			var loader:ServiceLoader = new ServiceLoader(payloadClass);
			var url:String = Model.config.serviceUrl+"/"+path;//+"?rand="+Math.random();
			loader.load(url, callback);
			return loader;
		}

		public static function post(path:String, variables:URLVariables, callback:Function):void
		{
			var loader:ServiceLoader = new ServiceLoader();
			var url:String = Model.config.serviceUrl+"/"+path;
			loader.post(url, variables, callback);
		}
		
		private var loader:URLLoader = new URLLoader;
		
		private var request:URLRequest = new URLRequest;
		
		private var callback:Function;
		
		private var payloadClass:Class;
			
		public function ServiceLoader(payloadClass:Class = null)
		{
			Log.record("ServiceLoader("+payloadClass+")");
			this.payloadClass = payloadClass;
			
			loader.addEventListener(Event.COMPLETE, onDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			loader.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler);
		}
		
		public function set postData(value:Object):void
		{
			request.method = URLRequestMethod.POST;
			request.data = value;
		}
		
		public function load(url:String, callback:Function):void
		{	
			Log.record("ServiceLoader.load("+url+")");
			this.callback = callback;
			request.url = url;
			loader.load(request);
	
		}
		
		private function uncaughtErrorHandler(event:UncaughtErrorEvent):void
		{
			Log.record("ServiceLoader.uncaughtErrorHandler()");
			if (event.error is Error)
			{
				var error:Error = event.error as Error;
				Log.record(error.message);
			}
			else if (event.error is ErrorEvent)
			{
				var errorEvent:ErrorEvent = event.error as ErrorEvent;
				Log.record(error.message);
			}
			else
			{
				Log.record("Non Error");
			}
		}
		

		public function post(url:String, variables:URLVariables, callback:Function):void
		{
			this.callback = callback;
			request.url = url;
			request.method = URLRequestMethod.POST;
			request.data = variables;  
			loader.load(request);
			
			Log.record("ServiceLoader.load(url:"+url+", callback:"+callback+")")
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			Log.record("IO Error when attempting to query url: "+request.url)
				trace(event.toString());
		}
		
		private function onDataLoaded(event:Event):void
		{
			Log.record("ServiceLoader.OnDataLoaded()");
			var json:String = URLLoader(event.target).data;
			var data:Object = JSONBeek.decode(json);
			var response:ServiceResponse = new ServiceResponse(data);
			
			if(response.error > 0)
			{
				// Error handling here
				//throw new Error(response.message);
			}
			else if(payloadClass != null && response.payload != null)
			{
				callback(new payloadClass(response.payload));
			}
			else
			{
				callback(null);
			}
		}
		
		public function dispose():void
		{
			loader.removeEventListener(Event.COMPLETE, onDataLoaded); 
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.close();
			
			loader = null;
			callback = null;
			request = null;
			payloadClass = null;
		}
	}
}