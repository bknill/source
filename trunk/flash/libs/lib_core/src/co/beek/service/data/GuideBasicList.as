package co.beek.service.data
{
	import co.beek.model.guide.GuideBasicData;
	import co.beek.service.ISerializable;

	public class GuideBasicList implements ISerializable
	{
		private var _guides:Vector.<GuideBasicData>;
		
		public function GuideBasicList(data:Object)
		{
			_guides = new Vector.<GuideBasicData>
			for(var i:int = 0; i<data.length; i++)
				_guides.push(new GuideBasicData(data[i]));
		}
		
		public function get guides():Vector.<GuideBasicData>
		{
			return _guides;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_guides.length; i++)
				data.push(_guides[i].data);
			
			return data;
		}
	}
}