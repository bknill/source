package co.beek.service
{
	import co.beek.map.Geo;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.ScenePanoTask;
	import co.beek.service.data.SceneBasicList;

	public class SceneService
	{
		public static function getSceneJson(sceneId:String, callback:Function):ServiceLoader
		{
			var path:String = "scene/{sceneId}/json";
			path = path.replace("{sceneId}", sceneId);
			
			return ServiceLoader.load(path, callback, SceneData);
		}
		
		public static function getScenesWithin(geo:Geo, meters:int, callback:Function):void
		{
			var path:String = "scenes/within/{lat}/{lon}/{meters}/";
			path = path.replace("{lat}", geo.lat);
			path = path.replace("{lon}", geo.lon);
			path = path.replace("{meters}", meters);
			
			ServiceLoader.load(path, callback, SceneBasicList);
		}
		
		private var locationId:String;
		
		public static function getLocationScenes(locationId:String, callback:Function):void
		{
			var path:String = "location/{locationId}/scenes";
			path = path.replace("{locationId}", locationId);
			
			ServiceLoader.load(path, callback, SceneBasicList);
		}

		public static function getScenesPanoTask(sceneId:String, callback:Function):void
		{
			var path:String = "scene/{sceneId}/panotask";
			path = path.replace("{sceneId}", sceneId);
			
			ServiceLoader.load(path, callback, ScenePanoTask);
		}
	}
}