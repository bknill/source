package co.beek.service
{
	public interface ISerializable
	{
		function get data():Object;
	}
}