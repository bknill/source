package co.beek.service.data
{
	import co.beek.model.data.SceneBasicData;
	import co.beek.service.ISerializable;

	public class SceneBasicList implements ISerializable
	{
		private var _scenes:Vector.<SceneBasicData>;
		
		public function SceneBasicList(data:Object)
		{
			_scenes = new Vector.<SceneBasicData>
			for(var i:int = 0; i<data.length; i++)
				_scenes.push(new SceneBasicData(data[i]));
		}
		
		public function get scenes():Vector.<SceneBasicData>
		{
			return _scenes;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_scenes.length; i++)
				data.push(_scenes[i].data);
			
			return data;
		}
	}
}