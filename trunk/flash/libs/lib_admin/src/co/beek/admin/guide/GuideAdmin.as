package co.beek.admin.guide
{
	import co.beek.admin.Admin;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.events.ResizeEvent;
	
	public class GuideAdmin extends Canvas
	{
		public static const ADMIN_NOTHING:int = 0;
		
		public static const ADMIN_SETTINGS:int = 1;

		public static const ADMIN_CONTENTS:int = 2;

		public static const ADMIN_GAME:int = 3;

		public static const ADMIN_SECTION:int = 4;

		public static const ADMIN_SCENE:int = 5;
		
		public static const ADMIN_VO:int = 6;
		
		[Bindable]
		protected var selectedAdminIndex:int;
		
		
		
		public function GuideAdmin()
		{
			super();
			
			
			Admin.instance.addEventListener(Admin.PLAYER_LOADED, onPlayerLoaded);
		}
		
		private function onPlayerLoaded(event:Event):void
		{
			Model.addEventListener(Model.SECTION_CHANGE, onSectionChange);
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditingChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE,onGuideViewChange);
			
			addEventListener(ResizeEvent.RESIZE, onResize);
			
			if(Model.guideData)
				updateAdminToGuideView();
			else
				Model.state.uiState = State.UI_STATE_EDITING_SCENE;
			
			dispatchEvent(new Event("pageChange"));
		}
		
		private function onResize(event:Event):void
		{
			if(Model.guideData)
				updateAdminToGuideView();
		}
		
		
		private function onEditingChange(event:Event):void
		{
			//updateToPage();
			dispatchEvent(new Event("uiUpdate"));
		}
		
		private function onGuideViewChange(event:Event):void
		{
			//updateToPage();
			dispatchEvent(new Event("uiUpdate"));
		}
		
		[Bindable (event="uiUpdate")]
		protected function get hidden():Boolean
		{
			return Model.state.guideView == State.GUIDE_VIEW_MENU && Model.state.uiState != State.UI_STATE_NORMAL;
		}
		
		private function onSectionChange(event:Event):void
		{
			dispatchEvent(new Event("pageChange"));
				updateAdminToGuideView()
		}
		
		protected function updateAdminToGuideView():void
		{
			//setTimeout(updateToPage, 100);
		}
		
		[Bindable (event="pageChange")]
		protected function get gameTask():GameTaskData
		{
			return Model.currentTask;
		}

		[Bindable (event="pageChange")]
		protected function get guideSection():GuideSectionData
		{
			trace('GuideAdmin.getGuideSection()' + Model.currentSection);
			return Model.currentSection;
		}


		[Bindable (event="pageChange")]
		protected function get guideScene():GuideSceneData
		{
			return Model.guideScene;
		}
		
		[Bindable (event="pageChange")]
		protected function get menuEdge():Number
		{
			return guideSection.menuEdge > 0 ? guideSection.menuEdge + 440 : 440;	
		}

		private function onGuidePageChange(event:Event):void
		{
			selectedAdminIndex = ADMIN_NOTHING;
			//setTimeout(updateToPage, 100);
			
			dispatchEvent(new Event("pageChange"));
		}
		
		private function updateToPage():void
		{
			trace("GuideAdmin.updateToPage()");
			if(Model.state.uiState == State.UI_STATE_NORMAL || Model.guideData == null || Model.state.guideView == State.GUIDE_VIEW_CLOSED)
				selectedAdminIndex = ADMIN_NOTHING;
			
			else if(Model.state.guideView == State.GUIDE_VIEW_SETTINGS)
				selectedAdminIndex = ADMIN_SETTINGS;

			else if(Model.state.uiState == State.UI_STATE_EDITING_VO)
				selectedAdminIndex = ADMIN_VO;
			
			else if(Model.state.guideView == State.GUIDE_VIEW_SCENE)
				selectedAdminIndex = ADMIN_SCENE;

			//else if(Model.state.guideView == State.GUIDE_VIEW_TASK)
			//	selectedAdminIndex = ADMIN_GAME;

			//else if(Model.state.guideView == State.GUIDE_VIEW_SECTION)
			//	selectedAdminIndex = ADMIN_SECTION;
			
			
			trace(selectedAdminIndex);
		}
	}
}