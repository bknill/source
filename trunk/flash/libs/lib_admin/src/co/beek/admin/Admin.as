package co.beek.admin
{
	import co.beek.IPlayer;
	import co.beek.admin.guide.GuideAdmin;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.GlobalErrorsPanel;
	import co.beek.admin.poup.GlobalErrorsPanelMX;
	import co.beek.admin.scene.SceneAdmin;
	import co.beek.model.Model;
	import co.beek.model.State;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class Admin extends Application implements IAdmin
	{
		public static var instance:Admin;
		
		public static const PLAYER_LOADED:String = "PLAYER_LOADED";
		
		private var _player:IPlayer;
		
		public var playerHolder:UIComponent;
		
		// The 3 admin sections
		public var sceneAdmin:SceneAdmin;
		public var guideAdmin:GuideAdmin;
		//public var pageAdmin:GuideSceneAdmin;
		
		public var savePanel:SavePanel;
		
		private var errorsPanel:GlobalErrorsPanel = new GlobalErrorsPanelMX;
		
		public function Admin()
		{
			super();
			instance = this;
			
			addEventListener(FlexEvent.APPLICATION_COMPLETE, onApplicationComplete);
			errorsPanel.addEventListener(CloseablePanel.CLOSE, onErrorPanelClose);
		}
		
		public function set player(value:IPlayer):void
		{
			_player = value;
			
			if(playerHolder)
			{
				playerHolder.addChild(_player as DisplayObject);
				//playerHolder.addChild(new Stats);
			}
			
			dispatchEvent(new Event(PLAYER_LOADED))
		}
		
		public function get player():IPlayer
		{
			return _player;
		}
		
		public function resize(width:Number, height:Number):void
		{
			this.width = width;
			this.height = height;
		}
		
		public function get guideAdminVisible():Boolean
		{
		 return Model.config.guideId != null
		}
		
		private function onApplicationComplete(event:FlexEvent):void
		{
			if(_player && DisplayObject(_player).parent != playerHolder)
				playerHolder.addChild(_player as DisplayObject);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE,onUIStateChange);
		}
		
		private function throwError():void
		{
			throw new Error("Testing");
		}
		
		public function showError(message:String):void
		{
			errorsPanel.message += message;
			
			if(!errorsPanel.parent)
			{
				PopUpManager.addPopUp(errorsPanel, this, true);
				PopUpManager.centerPopUp(errorsPanel);
			}
		}
		
		private function onErrorPanelClose(event:Event):void
		{
			PopUpManager.removePopUp(errorsPanel);
		}
		
		private function onUIStateChange(event:Event):void
		{
			
		}
		
		
		/*private function onGuideViewChange(event:Event):void
		{
			updateAdminToGuideView();
		}
		
		private function updateAdminToGuideView():void
		{
			switch(Model.state.guideView){
				case State.GUIDE_VIEW_PAGE:
					sceneAdmin.hide();
					guideAdmin.show(1000);
					break;

				case State.GUIDE_VIEW_COVER:
					guideAdmin.hide();
					sceneAdmin.show(500);
					break;
			}
		}*/
	}
}