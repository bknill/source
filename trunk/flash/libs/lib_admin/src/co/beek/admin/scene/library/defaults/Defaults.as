package co.beek.admin.scene.library.defaults
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.containers.VBox;
	import mx.controls.ProgressBar;
	import mx.graphics.codec.JPEGEncoder;
	import mx.managers.PopUpManager;
	
	import spark.core.SpriteVisualElement;
	
	import co.beek.admin.Admin;
	import co.beek.admin.guide.scene.TransitionMovie;
	import co.beek.admin.guide.scene.TransitionMovieMX;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.SceneTypeBrowser;
	import co.beek.admin.poup.SceneTypeBrowserMX;
	import co.beek.map.GeoZoom;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.PanoLook;
	import co.beek.model.data.SceneData;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.SendBytes;
	
	public class Defaults extends VBox
	{
		public var iconHolder:SpriteVisualElement;
		
		private var icons:AdminTypeIconsFl = new AdminTypeIconsFl;
		
		[Bindable]
		protected var noGuide:Boolean;
		
		[Bindable]
		protected var panoSelected:Boolean;
		
		public var panoProgressBar:ProgressBar;
		
		private var panoUploader:PanoUploader = new PanoUploader;
		
		private var timer:Timer = new Timer(100);
		
		public function Defaults()
		{
			super();
			
			panoUploader.addEventListener(PanoUploader.STAGE_CHANGE, onUploaderStageChange);
			panoUploader.addEventListener(PanoUploader.VIDEO_STAGE_CHANGE, onVideoUploaderStageChange);
			panoUploader.addEventListener(PanoUploader.POLLING_CHANGE, onPollCountChange);

	
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			
			icons.width = 30;
			icons.scaleY = icons.scaleX;
			
			
			iconHolder.addChild(icons);
			
			noGuide = Model.guideData == null;
			
			timer.addEventListener(TimerEvent.TIMER, onTimerEvent);
			timer.start();
			
			updateForScene();
		}
		
		
		[Bindable (event="scene_change")]
		protected function get title():String
		{
			return Model.currentScene.title;
		}
		
		protected function set title(value:String):void
		{
			Model.currentScene.title = value;
		}
		
		private function showIcon():void
		{
			for(var i:int; i<icons.numChildren; i++)
				icons.getChildAt(i).visible = false;
			
			var iconsReference:Dictionary = Constants.getIconsDictionary(icons);
			var icon:Sprite = iconsReference[Model.currentScene.type];
			if(icon)
			{
				ColorUtil.colorize(icon, Constants.BEEK_BLUE);
				icon.visible = true;
			}
		}
		
		protected function browseForType(event:MouseEvent):void
		{
			var b:SceneTypeBrowserMX = new SceneTypeBrowserMX;
			b.addEventListener(SceneTypeBrowser.TYPE_SELECTED, onTypeSelected);
			b.addEventListener(CloseablePanel.CLOSE, onPanelClose);
			b.type = Model.currentScene.type;
			PopUpManager.addPopUp(b, Admin.instance, true);
			PopUpManager.centerPopUp(b);
		} 
		
		private function onTypeSelected(event:Event):void
		{
			var b:SceneTypeBrowser = SceneTypeBrowser(event.target);
			Model.currentScene.type = b.type;
			
			removePanel(b);
			
			showIcon();
		}
		
		private function onPanelClose(event:Event):void
		{
			removePanel(SceneTypeBrowser(event.target));
		}
		
		private function removePanel(browser:SceneTypeBrowser):void
		{
			browser.removeEventListener(SceneTypeBrowser.TYPE_SELECTED, onTypeSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onPanelClose);
			PopUpManager.removePopUp(browser);
		}
		
		
		protected function browsePanoImage():void
		{
			panoSelected = false;
			panoUploader.browsePanoImage();
		}
		
		protected function browsePanoVideo():void
		{
			panoUploader.browsePanoVideo();
		}
		
		[Bindable (event="STAGE_CHANGE")]
		protected function get panoVideoUploadStage():int
		{
			return panoUploader.videoStage;
		}
		
		[Bindable (event="STAGE_CHANGE")]
		protected function get panoUploadStage():int
		{
			return panoUploader.stage;
		}
		

		
		[Bindable (event="STAGE_CHANGE")]
		protected function get processingCount():int
		{
			return panoUploader.stage;
		}
		
		protected function onUploaderStageChange(event:Event):void
		{
			dispatchEvent(event);
			panoUploader.addEventListener(ProgressEvent.PROGRESS, onPanoUploadProgress);
		} 
		
		protected function onVideoUploaderStageChange(event:Event):void
		{
			trace("onVideoUploaderStageChange" + event.toString());
			dispatchEvent(event);
			panoUploader.addEventListener(ProgressEvent.PROGRESS, onPanoVideoUploadProgress);
		} 
		
		[Bindable (event="POLLING_CHANGE")]
		protected function get pollingCount():int
		{
			return panoUploader.pollcount;
		}

		[Bindable (event="POLLING_CHANGE")]
		protected function get taskStatus():String
		{
			return panoUploader.panoTask ? panoUploader.panoTask.statusString : "Loading Data";
		}
		
		protected function onPollCountChange(event:Event):void
		{
			dispatchEvent(event);
		} 
		
		protected function onPanoUploadProgress(event:ProgressEvent):void
		{
			if(panoProgressBar)
				panoProgressBar.setProgress(event.bytesLoaded, event.bytesTotal);
		} 
		
		protected function onPanoVideoUploadProgress(event:ProgressEvent):void
		{
			if(panoProgressBar)
				videoProgressBar.setProgress(event.bytesLoaded, event.bytesTotal);
		} 
		
		protected function uploadPanoImage():void
		{
			panoUploader.uploadPanoImage();
		}
		
		protected function uploadPanoVideo():void
		{
			panoUploader.uploadPanoVideo();
		}
		
		[Bindable (event="thumb_change")]
		public function get sceneThumb():Object
		{
			if(Model.currentScene.newThumb)
				return new Bitmap(Model.currentScene.newThumb);

			if(Model.currentScene.thumbIncrement > 0)
				return Model.config.getAssetUrl(Model.currentScene.thumbName);
			
			return new BlankImageFl;
		}
		
		public function set panoZoom(value:int):void
		{
			//Model.state.zoom  = value;
		}

		public function get panoZoom():int
		{
			return 0;//Model.state.zoom;
		}

		protected function setInitialLook():void
		{
			// Due to difference between this pano and the last,
			// Pan values need to have 180 removed to them before storing.
			// tilt values need to be reversed.
			// This value will be removed when loading
			var look:PanoLook = Admin.instance.player.pano.panoLook;
			Model.currentScene.initialLook = new PanoLook((look.pan-180)%360, -look.tilt, look.fov/0.65);
			
			//Model.state.addEventListener(State.PANO_SNAPSHOT_READY,function(){});
			//Admin.instance.player.pano.queueSnapshot();
			var panoBitmap:BitmapData = Admin.instance.player.pano.bitmapData;


			var thumb:Rectangle = SceneData.THUMB_BUBBLE;

			
			var scale:Number = Math.max(thumb.width/panoBitmap.width, thumb.height/panoBitmap.height);
			var tx:Number = (thumb.width - (panoBitmap.width * scale)) / 2 ;
			var scaleMatrix:Matrix = new Matrix(scale, 0, 0, scale, tx);
			
			var thumbshot:BitmapData = new BitmapData(thumb.width, thumb.height, false, 0);
			thumbshot.draw(panoBitmap, scaleMatrix, null, null, null, true);
			
			Model.currentScene.newThumb = thumbshot;
			dispatchEvent(new Event("thumb_change"));
		}
		
		[Bindable (event="scene_change")]
		public function get mapCenter():GeoZoom
		{
			return new GeoZoom(
				Model.currentScene.geo.lat,
				Model.currentScene.geo.lon,
				17);
		}
		
		public function set mapCenter(value:GeoZoom):void
		{
			Model.currentScene.geo = value;
		}
		
		private function onSceneChange(event:Event):void
		{
			updateForScene();
		}
		
		private function updateForScene():void
		{
			if(!Model.currentScene)
				return;
			
			dispatchEvent(new Event("scene_change"));
			dispatchEvent(new Event("thumb_change"));
			dispatchEvent(new Event("look_change"));
			
			showIcon();
		}

		[Bindable (event="look_change")]
		public function get panoNorth():Number
		{
			return (Admin.instance.player.pano.panoLook.pan - Model.currentScene.north + 360)%360;
		}
		
		private function onTimerEvent(event:Event):void
		{
			dispatchEvent(new Event("look_change"));
		}
		
		protected function onNorthSliderChange(event:Event):void
		{
			Model.currentScene.north = Admin.instance.player.pano.panoLook.pan - event.currentTarget.value;
		}
		
		
	}
}