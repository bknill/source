package co.beek.admin.scene.library
{
	import caurina.transitions.Tweener;
	
	import co.beek.admin.BeekDragManager;
	import co.beek.model.hotspots.HotspotData;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.controls.Image;
	import mx.core.SpriteAsset;
	
	public class DraggableHotspot extends Canvas
	{
		protected var pos:Point;
		private var mouseDownPos:Point;
		
		private var shadow:DropShadowFilter = new DropShadowFilter(2, 135, 0, 0.5);
		
		//private var GraphicClass:Class;
		
		protected var _graphic:MovieClip;
		protected var _proxy:MovieClip;
		
		public function DraggableHotspot(GraphicClass:Class)
		{
			super();
			_graphic = new GraphicClass;
			_proxy = new GraphicClass;
			
			
			filters = [shadow];
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			rawChildren.addChild(_graphic);
		}
		
		public function get hotspotData():HotspotData
		{
			return null;
		}
		
		private function onMouseOver(event:MouseEvent):void 
		{
			if(!pos)
				pos = new Point(x, y);
			
			Tweener.addTween(shadow, {
				'time':0.5, 
				'distance' : 10, 
				'alpha' : 0.2, 
				'onUpdate':onShadowTweenUpdate 
			});
		}
		
		private function onShadowTweenUpdate():void
		{
			if(pos != null)
			{
				filters = [shadow];
				x = pos.x + shadow.distance/2;
				y = pos.y - shadow.distance/2;
			}
		}
		
		private function onMouseDown(event:MouseEvent):void 
		{
			mouseDownPos = new Point(event.localX, event.localY);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onMouseMove(event:MouseEvent):void 
		{
			/*var mousePos:Point = new Point(event.localX, event.localY);
			var distance:Number = Point.distance(mouseDownPos, mousePos);
			
			
			if(distance > 20)
			{*/
			var dragProxy:Image = new Image();
			dragProxy.source = _proxy;
			
				removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				BeekDragManager.startDrag(event, hotspotData, dragProxy);
				tweenToOriginal();
			//}
		}

		private function onMouseUp(event:MouseEvent):void 
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			//setTimeout(tweenToOriginal, 500);
		}
		
		private function tweenToOriginal():void
		{
			Tweener.removeTweens(shadow);
			
			Tweener.addTween(shadow, {
				'time':0.5, 
				'distance' : 2, 
				'alpha' : 0.5, 
				'onUpdate':onShadowTweenUpdate 
			});
		}

		private function onMouseOut(event:MouseEvent):void 
		{
			tweenToOriginal();
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			graphics.clear();
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			graphics.endFill();
			
		}
	}
}