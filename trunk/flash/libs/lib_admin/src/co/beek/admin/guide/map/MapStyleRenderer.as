package co.beek.admin.guide.map
{
	import co.beek.map.Map;
	import co.beek.map.MapData;
	import co.beek.model.Model;
	
	import mx.containers.Canvas;
	import mx.flash.UIMovieClip;
	
	public class MapStyleRenderer extends Canvas
	{
		private var map:Map = new Map;
		public function MapStyleRenderer()
		{
			super();
			map.x = map.y = 5;
			width = 100;
			height = 100;
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			rawChildren.addChild(map);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			map.x = (unscaledWidth - map.width)/2;
			map.y = (unscaledHeight - map.height)/2;
		}
		
		public override function set data(value:Object):void
		{
			super.data = value;
			map.data = new MapData(90, 90, 16, String(value));
			map.data.center = Model.currentScene
				? Model.currentScene.geo
				: MapData.DEFAULT_CENTER;
		}
	}
}