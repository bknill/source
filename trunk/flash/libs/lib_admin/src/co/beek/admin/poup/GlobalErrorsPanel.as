package co.beek.admin.poup
{
	public class GlobalErrorsPanel extends CloseablePanel
	{
		[Bindable]
		public var message:String = "";
		
		public function GlobalErrorsPanel()
		{
			super();
		}
	}
}