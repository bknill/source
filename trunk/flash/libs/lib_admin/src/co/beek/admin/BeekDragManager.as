package co.beek.admin
{
	import co.beek.Log;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import mx.core.DragSource;
	import mx.core.IFlexDisplayObject;
	import mx.core.IUIComponent;
	import mx.core.UIComponent;
	import mx.managers.DragManager;
	
	public class BeekDragManager extends EventDispatcher
	{
		public static const DRAG_STARTED:String = "DRAG_STARTED";
		public static const DRAG_DROPPED:String = "DRAG_DROPPED";
		public static const DRAG_CANCELLED:String = "DRAG_CANCELLED";
		
		
		public static var instance:BeekDragManager = new BeekDragManager;
		
		private static var dragSource:DragSource;
		
		public static function addEventListener(type:String, listener:Function):void
		{
			instance.addEventListener(type, listener);
		}
		
		public static function startDrag(event:MouseEvent, data:Object, proxy:IFlexDisplayObject):void
		{
			// Create a DragSource object.
			dragSource = new DragSource();
			
			// Add the data to the object.
			dragSource.addData(data, 'data');
			
			// Call the DragManager doDrag() method to start the drag. 
			var dragInitiator:IUIComponent = IUIComponent(event.currentTarget);
			mx.managers.DragManager.doDrag(dragInitiator, dragSource, event, proxy);
			
			instance.dispatchEvent(new Event(DRAG_STARTED));
		}
		
		public static function get dragData():Object 
		{
			return dragSource ? dragSource.dataForFormat('data') : null;
		}
		
		public static function cancel():void 
		{
			dragSource = null;
			instance.dispatchEvent(new Event(DRAG_CANCELLED));
		}
		
		public static function dropped():void 
		{
			instance.dispatchEvent(new Event(DRAG_DROPPED));
		}
	}
}