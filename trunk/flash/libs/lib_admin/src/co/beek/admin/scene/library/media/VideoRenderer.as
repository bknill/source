package co.beek.admin.scene.library.media
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.VideoData;
	
	import com.laiyonghao.Uuid;

	public class VideoRenderer extends DraggableHotspot 
	{
		public function VideoRenderer()
		{
			super(VideoRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return Model.guideScene != null 
				? VideoData.createForGuide(id, Model.guideScene.id)
				: VideoData.createForScene(id, Model.currentScene.id);
		}
	}
}