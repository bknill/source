package co.beek.admin.component
{
	import co.beek.admin.Admin;
	import co.beek.model.data.PanoLook;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PostData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.utils.JSONBeek;
	import co.beek.Fonts;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.RichTextEditor;
	import mx.core.UIComponent;
	
	[Event(name="focusOut", type="flash.events.FocusEvent")]
	[Event(name="checkbox_changed", type="flash.events.Event")]
	public class BeekRTE extends RTETest
	{
		[Bindable]
		protected var _hotspots:ArrayCollection = new ArrayCollection();
		
		/*private var hotspotsCombo:ComboBox = new ComboBox;*/

		//private var defaultCheckbox:CheckBox = new CheckBox;

		//private var panoLookButton:Button;
		
		override public function BeekRTE()
		{
			super();
			fontSize = 18;
			fontFamily = Fonts.bodyFont;
			
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			
			defaultCheckbox.addEventListener(Event.CHANGE, onCheckboxChange);
			defaultCheckbox.label = "Store default text";
			
			hotspotsCombo.dataProvider = _hotspots;
			hotspotsCombo.labelField="title"
			hotspotsCombo.width = 179;
			hotspotsCombo.addEventListener(Event.CHANGE, handleHotspotComboChange);
			
			panoLookButton.label = "Look Here";
			panoLookButton.addEventListener(MouseEvent.CLICK, onPanoLookButtonClick);
				
			//boldButton.parent.removeChild(boldButton);
			//underlineButton.parent.removeChild(underlineButton);
			//italicButton.parent.removeChild(italicButton);
			
			/*toolbar.removeChild(_RichTextEditor_VRule1);
			toolbar.removeChild(_RichTextEditor_VRule2);
			
			//toolbar.removeChild(bulletButton);
			toolBar2.removeChild(boldButton);
			toolBar2.removeChild(italicButton);
			toolBar2.removeChild(underlineButton);*/
			
			/*toolbar.removeChild(colorPicker);
			toolbar.removeChild(fontFamilyCombo);
			toolbar.removeChild(fontSizeCombo);
			toolbar.removeChild(alignButtons);*/
			
			/*toolbar.addChild(bulletButton);
			toolbar.addChild(defaultCheckbox);
			//toolbar.addChild(hotspotsCombo);
			toolbar.addChild(panoLookButton);*/
			
			// does nothing
			textArea.setStyle("fontFamily", _fontFamily);
			textArea.setStyle("fontSize", _fontSize);
			
			/*toolbar.setStyle("verticalGap", "1");*/
			
			/*UIComponent(toolbar.parent).setStyle("paddingTop", "0");
			UIComponent(toolbar.parent).setStyle("paddingRight", "0");
			UIComponent(toolbar.parent).setStyle("paddingBottom", "2");
			UIComponent(toolbar.parent).setStyle("paddingLeft", "0");*/
			
			textArea.addEventListener(TextEvent.LINK, onDescriptionFieldTextLink);
			textArea.addEventListener(FocusEvent.FOCUS_OUT, onFieldTextFocusOut);
			
			//populateHotspots();
		}
		
		private var _fontFamily:String;
		public function set fontFamily(value:String):void
		{
			_fontFamily = value;
			if(textArea)
				textArea.setStyle("fontFamily", value);
		}
		
		private var _fontSize:Number;
		public function set fontSize(value:Number):void
		{
			_fontSize = value;
			if(textArea)
				textArea.setStyle("fontSize", value);
		}
		
		private function onDescriptionFieldTextLink(linkEvent:TextEvent):void 
		{
			hotspotsCombo.selectedItem = getHotspot(linkEvent.text);
		}

		private function onFieldTextFocusOut(event:FocusEvent):void 
		{
			dispatchEvent(event);
		}
		
		public function get defaultSelected():Boolean
		{
			return defaultCheckbox.selected;
		}
		
		public function onCheckboxChange(event:Event):void
		{
			defaultCheckbox.enabled = !defaultCheckbox.selected;
			dispatchEvent(new Event("checkbox_changed"));
		}
		
		public function handleHotspotComboChange(event:Event):void
		{
			var hs:HotspotData = ComboBox(event.target).selectedItem as HotspotData;
			if(hs)
			{
				setTextStyles('url', "event:" + hs.id);
				setTextStyles('underline', true);
			}
				
				/*selection.htmlText = "<u><a href='event:" + hs.id + "'>" + selection.text + "</a></u>";*/
			
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function onPanoLookButtonClick(event:Event):void
		{
			var look:String = Admin.instance.player.pano.panoLook.toString();
			selection.htmlText = "<u><a href='event:"+look+"'>"+selection.text+"</a></u>";
			
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		/*private function addSceneListeners():void
		{
			if(Model.currentScene)
				Model.currentScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			
			if(Model.guideScene)
				Model.guideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
		}*/
		
		/*private function onSceneChange(event:Event):void
		{
			addSceneListeners();
			populateHotspots();
		}*/
		
		/*private function onHotspotAdded(event:Event):void
		{
			populateHotspots();
		}*/
		
		public function set hotspots(value:Vector.<HotspotData>):void
		{
			_hotspots.removeAll();
			_hotspots.addItem({"title" : "Select Hotspot"});
			for(var i:int; i<value.length; i++)
			{
				if(value[i] is SoundData 
					|| value[i] is PostData)
					continue;
				
				_hotspots.addItem(value[i]);
			}
		}
		
		private function getHotspot(id:String):HotspotData
		{
			for(var i:int; i<_hotspots.length; i++)
				if(_hotspots[i] is HotspotData 
					&& HotspotData(_hotspots[i]).id == id)
					return _hotspots[i] as HotspotData;
			
			return null;
		}
		
		/*private function populateHotspots():void
		{
			hotspots.removeAll();
			hotspots.addItem({'title':'no hotspot'});
			var hs:Vector.<HotspotData> = Model.hotspots;
			for(var i:int; i<hs.length; i++)
				hotspots.addItem(hs[i]);
		}*/
	}
}