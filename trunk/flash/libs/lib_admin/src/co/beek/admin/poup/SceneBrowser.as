package co.beek.admin.poup
{
	import co.beek.event.LocationDataEvent;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.markers.SceneMapMarker;
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneData;
	import co.beek.service.LocationService;
	import co.beek.service.SceneService;
	import co.beek.service.data.SceneBasicList;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	
	import mx.collections.ArrayCollection;
	
	import spark.components.List;
	import spark.events.IndexChangeEvent;

	public class SceneBrowser extends LocationBrowser
	{
		public static const SCENE_SELECTED:String = "SCENE_SELECTED";
		
		private var _selectedScene:SceneBasicData;
		
		public function SceneBrowser()
		{
			super();
			
			addEventListener("location_change", onLocationSelected);
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		private function onSceneChange(event:Event):void
		{
			dispatchEvent(new Event("scene_change"));
		}
		
		private function onLocationSelected(event:Event):void
		{
			if(selectedLocation)
				loadLocationScenes();
		}
		
		private function loadLocationScenes():void
		{
			clearTimeout(locationsLoadTimeout);
			removeMapMarkers();
			LocationService.getScenesForLocation(selectedLocation.id, onScenesList);
		}
		
		protected override function onMapChange(event:Event):void
		{
			if(!selectedLocation)
				super.onMapChange(event);
		}
		
		private function onScenesList(list:SceneBasicList):void
		{
			addSceneMarkers(list.scenes);
		}
		
		private function addSceneMarkers(scenes:Vector.<SceneBasicData>):void
		{
			for(var i:int=0; i<scenes.length; i++)
				addSceneMapMarker(scenes[i], String(i+1));
			
			//mapHolder.showAllMapMarkers();
		}
		
		private function addSceneMapMarker(scene:SceneBasicData, num:String):void
		{
			var marker:SceneMapMarker = new SceneMapMarker(scene, num);
			marker.addEventListener(MouseEvent.CLICK, onSceneMarkerClick);
			mapHolder.addMarker(marker);
			
			// add the 
			listCollection.addItem(scene);
			
			mapMarkers[scene] = marker;
		}
		
		private function onSceneMarkerClick(event:MouseEvent):void
		{
			selectedScene = SceneMapMarker(event.target).data;
		}
		
		protected override function onListChange(event:IndexChangeEvent):void
		{
			if(List(event.target).selectedItem is LocationData)
				selectedLocation = List(event.target).selectedItem as LocationData;
				
			else if(List(event.target).selectedItem is SceneBasicData)
				selectedScene = List(event.target).selectedItem as SceneBasicData;
		}
		
		[Bindable (event="scene_change")]
		public function get selectedScene():SceneBasicData
		{
			return _selectedScene;
		}
		
		public function set selectedScene(value:SceneBasicData):void
		{
			_selectedScene = value;
			
			if(_selectedScene)
				hilightSceneMapMarker();
			else
				hilightAllMarkers();
			
			dispatchEvent(new Event("scene_change"));
		}
		
		public function hilightSceneMapMarker():void
		{
			for each(var marker:Object in mapMarkers)
				DisplayObject(marker).alpha = 0.5;
			
			if(_selectedScene)
				SceneMapMarker(mapMarkers[_selectedScene]).alpha = 1;
		}
		
		protected function onBackClick(event:MouseEvent):void
		{
			selectedLocation = null;
			loadLocations();
		}
		
		protected function onSelectLocationClick():void
		{
			if(selectedLocation)
				dispatchEvent(new LocationDataEvent(LOCATION_SELECTED, selectedLocation));
		}
		
		protected function onSelectSceneClick():void
		{
			if(_selectedScene)
				dispatchEvent(new SceneBasicEvent(SCENE_SELECTED, _selectedScene));
		}
			
	}
}