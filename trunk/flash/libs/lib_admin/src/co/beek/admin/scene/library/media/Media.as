package co.beek.admin.scene.library.media
{
	import co.beek.admin.Admin;
	import co.beek.admin.guide.scene.TransitionMovie;
	import co.beek.admin.guide.scene.TransitionMovieMX;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.PhotoData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Tree;
	import mx.managers.PopUpManager;
	
	public class Media extends VBox
	{
		[Bindable]
		protected var guideScene:GuideSceneData;
		
		public function Media()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			guideScene = Model.guideScene;
			guide = Model.guideData;
			if(!Model.config.guideId)
				isGuideAdmin = false;
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		private function onSceneChange(event:Event):void
		{
			guideScene = Model.guideScene;
			//onListReady(Model.guideData);
		}
		
		[Bindable]
		protected var isGuideAdmin:Boolean = true;
		
		
		[Bindable]
		public var photoTreeData:ArrayCollection = new ArrayCollection([
			new MediaFolder(MediaFolder.FOLDER_ID_PHOTOS, "PHOTOS")
		]);
		
		[Bindable]
		protected var guideMedia:ArrayCollection = new ArrayCollection;
		
		public var tree:Tree;
		
		private var _guide:GuideData;
		
	
		
		private function set guide(value:GuideData):void
		{
			_guide = value;
			
		}
		
		[Bindable]
		protected var guideMediaIndex:int = -1;
		
		protected function get treeHeight():Number
		{
			var rows:int = photoTreeData.length;
			var open:Array = tree.openItems as Array;
			for(var i:int = 0; i<open.length; i++)
			{
				if(open[i].hasOwnProperty("children"))
					rows += (open[i].children as Array).length;
			}
			
			return (rows + 1) * tree.rowHeight;
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			onListReady(Model.guideData);
	         
		}
		
		public var photoTreeDataRaw:ArrayCollection = new ArrayCollection();
		
		private function onListReady(guide:GuideData):void
		{
			
			if(!Model.config.guideId)
				return;
			
			
			for(var i:int; i<guide.guideScenes.length; i++)
				for(var h:int = 0; h<guide.guideScenes[i].hotspots.length; h++)
					if(guide.guideScenes[i].hotspots[h] is PhotoData){
						var photo:PhotoData = new PhotoData(guide.guideScenes[i].hotspots[h]);
						photoTreeData.getItemAt(0).addFile(photo);
					}	
			
			photoTreeData.sort;	
			photoTreeData.refresh();
			//checkListDupicates()
		}
		
			
			private function checkListDupicates():void
			{
				
				for(var i:int=0; i<photoTreeData.length; i++)
				{
					for(var j:int = (i+1); j <photoTreeData.length; j++)
					{
						if(photoTreeData[i].title==photoTreeData[j].title)
						{
							photoTreeData.removeItemAt(j);
							j-=1;
						}
					}
				}
		
		}
	
}
	
}