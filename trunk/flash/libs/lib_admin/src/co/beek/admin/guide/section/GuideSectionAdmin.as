package co.beek.admin.guide.section
{
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.LocationBrowser;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.event.GuideSceneEvent;
	import co.beek.event.LocationDataEvent;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	
	public class GuideSectionAdmin extends Canvas
	{
		[Bindable]
		protected var titleFont:String;
		
		[Bindable]
		protected var scenes:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var section:GuideSectionData;
		
		[Bindable]
		protected var testdata:ArrayCollection = new ArrayCollection([
			"test1", "test2", "test3"
			
		]);
		
		public function GuideSectionAdmin()
		{
			super();
			
			titleFont = Fonts.titleFont;
		}
		
		public function set guideSection(value:GuideSectionData):void
		{
			showSectionScenes(value);
		}
		
		protected function showSectionScenes(section:GuideSectionData):void
		{
			this.section = section;
			scenes.removeAll();
			
			if(!section)
				return;
			
			for(var i:int=0; i<section.guideScenes.length; i++)
				scenes.addItem(section.guideScenes[i]);
		}

		protected function onSceneDelete(event:GuideSceneEvent):void
		{
			var guideSection:GuideSectionData = Model.guideData.getGuideSection(event.guideScene.sectionId);
			guideSection.removeGuideScene(event.guideScene);
			
			var index:int = scenes.getItemIndex(event.guideScene);
			if(index > -1)
				scenes.removeItemAt(index);
		}
		
		/*protected function onSceneDrop(event:SceneDropEvent):void
		{
			var sceneIndex:int = scenes.getItemIndex(event.guideScene);
			if(sceneIndex)
				scenes.removeItemAt(sceneIndex);
			
			var guideSection:GuideSectionData = Model.guideData.getGuideSection(event.guideScene.sectionId);
			guideSection.removeGuideScene(event.guideScene);
			
			var clone:GuideSceneData = GuideSceneData.clone(new Uuid().toString(), event.guideSection, event.guideScene);
			
			// Hack to fix hibernate error.
			for(var i:int; i<clone.hotspots.length; i++)
				GuideSceneHotspot(clone.hotspots[i]).id = new Uuid().toString();
			
			event.guideSection.addGuideScene(clone);
		}*/
		
		protected function onScenesListDragEnter(event:DragEvent):void
		{
			if (event.dragInitiator is ScenesList)
				DragManager.acceptDragDrop(this);
		}
		
		protected function onScenesListDragComplete(event:DragEvent):void
		{
			if (event.dragInitiator is ScenesList)
				for(var i:int; i<scenes.length; i++)
					GuideSceneData(scenes[i]).order = i;
			
			section.reorderGuideScenes();
		}
		
		protected function onAddSceneClick(event:MouseEvent):void
		{
			var center:Geo = Model.guideScene 
				? Model.guideScene.scene.geo 
				: new Geo(-41.2961, 174.7944);
			var b:SceneBrowser = new SceneBrowserMX;
			b.addEventListener(LocationBrowser.LOCATION_SELECTED, onLocationSelected);
			b.addEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			b.addEventListener(CloseablePanel.CLOSE, onPanelClose);
			b.mapCenter = new GeoZoom(center.lat, center.lon, 13);
			PopUpManager.addPopUp(b, Admin.instance, true);
			PopUpManager.centerPopUp(b);
			
			//b.x = stage.stageWidth/2 - 600;
			//b.y = 24;
		} 
		
		private function onLocationSelected(event:LocationDataEvent):void
		{
			var guideScene:GuideSceneData = GuideSceneData.fromLocation(
				new Uuid().toString(), section, event.location, scenes.length
			)
			section.addGuideScene(guideScene);
			scenes.addItem(guideScene);
			
			// No scene was loaded yet.
			if(Model.state.loadingScene == null)
				Model.state.loadingScene = guideScene.scene;
		}
		
		private function onSceneSelected(event:SceneBasicEvent):void
		{
			var guideScene:GuideSceneData = GuideSceneData.from(
				new Uuid().toString(), section, event.scene, 999
			)
			section.addGuideScene(guideScene);
			scenes.addItem(guideScene);
			
			// No scene was loaded yet.
			if(Model.state.loadingScene == null)
				Model.state.loadingScene = event.scene;
		}
		
		private function onPanelClose(event:Event):void
		{
			removePanel(SceneBrowser(event.target));
		}
		
		private function removePanel(browser:SceneBrowser):void
		{
			browser.removeEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onPanelClose);
			PopUpManager.removePopUp(browser);
		}
	}
}