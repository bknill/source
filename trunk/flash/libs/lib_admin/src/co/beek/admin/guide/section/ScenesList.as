package co.beek.admin.guide.section
{
	import spark.components.List;

	[Event(name="sceneDelete", type="co.beek.event.GuideSceneEvent")]
	[Event(name="sceneMove", type="co.beek.event.GuideSceneEvent")]
	public class ScenesList extends List
	{
		public function ScenesList()
		{
			super();
		}
	}
}