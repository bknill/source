package co.beek.admin.scene.library.media
{
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.PhotoData;

	public class MediaFolder
	{
		public static const FOLDER_ID_PHOTOS:String = "1";
		
		private var _id:String;
		private var _title:String;
		private var _children:Array = [];
		
		public function MediaFolder(id:String, title:String)
		{
			_id = id;
			_title = title;
		}
		
		public function get id():String
		{
			return _id;
		}

		public function get title():String
		{
			return _title;
		}
		
		public function get children():Array
		{
			return _children;
		}
		
		public function addFile(photo:PhotoData):void
		{
			_children.push(photo);
		}
	}
}