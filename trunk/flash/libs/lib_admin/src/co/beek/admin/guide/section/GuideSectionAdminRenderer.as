package co.beek.admin.guide.section
{
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GuideSceneEvent;
	import co.beek.model.guide.GuideSceneData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.DragSource;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	import spark.components.supportClasses.ItemRenderer;
	
	[Event(name="sceneDelete", type="co.beek.admin.guide.scenes.SceneEvent")]
	[Event(name="sectionDelete", type="co.beek.admin.guide.sections.SectionEvent")]
	public class GuideSectionAdminRenderer extends Canvas implements IListItemRenderer
	{
		/**
		 * For some reason, the native selected value is not bindable
		 */
		[Bindable]
		protected var isSelected:Boolean;
		
		
		public function GuideSectionAdminRenderer()
		{
			super();
		}
		
		
		/*public override function set selected(value:Boolean):void
		{
			super.selected = value;
			isSelected = value;
		}*/
		
		public override function set data(value:Object):void
		{
			super.data = value;
			
			if(guideSceneData)
				trace(guideSceneData.title)
		}
		
		[Bindable (event="dataChange")]
		public function get guideSceneData():GuideSceneData
		{
			return data as GuideSceneData;
		}
		
		protected function onDeleteButtonClick(event:MouseEvent):void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove the scene from the guide?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			dispatchEvent(new GuideSceneEvent("sceneDelete", guideSceneData));
		}

		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
	}
}