package co.beek.admin.component
{
	import mx.controls.Button;

	public class MoveButton extends Button
	{
		[Embed("/../images/button_move.png")] 
		private var skin: Class; 
		
		public function MoveButton()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 25;
		}	
	}
}