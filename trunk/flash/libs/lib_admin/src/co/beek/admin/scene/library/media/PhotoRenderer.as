package co.beek.admin.scene.library.media
{
	import co.beek.admin.BeekDragManager;
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PostData;
	
	import com.laiyonghao.Uuid;
	
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	
	public class PhotoRenderer extends DraggableHotspot 
	{
		public function PhotoRenderer()
		{
			super(PhotoRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return Model.guideScene != null 
				? PhotoData.createForGuide(id, Model.guideScene.id)
				: PhotoData.createForScene(id, Model.currentScene.id);
		}
	}
}