package co.beek.admin.component
{
	import mx.controls.Button;
	
	public class EditButton extends Button
	{
		[Embed("/../images/button_edit.png")] 
		private var skin: Class; 
		
		public function EditButton()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 25;
		}
	}
}