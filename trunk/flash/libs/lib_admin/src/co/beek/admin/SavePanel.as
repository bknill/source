package co.beek.admin
{
	import com.laiyonghao.Uuid;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.fscommand;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	
	import mx.containers.Panel;
	import mx.graphics.codec.JPEGEncoder;
	import mx.graphics.codec.PNGEncoder;
	
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSectionData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	import co.beek.utils.SendBytes;
	
	
	public class SavePanel extends Panel
	{
		private static const SHOW_BUTTON:int = 0;
		private static const SHOW_SAVING:int = 1;
		private static const SHOW_ERROR:int = 2;
		private static const SHOW_COMPLETE:int = 3;
		
		private static const SHOW_NEW_GAME_BUTTON:int = 0;
		private static const SHOW_HIDE_GAME:int = 1;
		private static const SHOW_EDIT_GAME:int = 2;
		
		public static var instance:SavePanel;
		
		private var pngEncoder:PNGEncoder = new PNGEncoder;
		
		private var jpgEncoder:JPEGEncoder = new JPEGEncoder(90);
		
		[Bindable] public var showIndex:int;
		[Bindable] public var showGameIndex:int;
		
		private var guideSaved:Boolean = false;
		private var sceneSaved:Boolean = false;
		
		public function SavePanel()
		{
			super();
			instance = this;
			
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);

			Model.state.addEventListener(State.LOADING_SCENE_CHANGE, onLoadingSceneChange);
			addGuideListeners();
			addSceneListeners();
			 
			//if(Model.hasGame)
				//showGameIndex = SHOW_HIDE_GAME;
		}
		
		public function get hasGuide():Boolean
		{
			return Model.config.guideId	!= null
		}
		
		public function get adminWidth():int
		{
			return Model.config.guideId	!= null ? 440 : 200
		}
		
		[Bindable (event="edit_mode_change")]
		protected function get editMode():Boolean
		{
			return Model.state.uiState == State.UI_STATE_EDITING_SCENE;
		}
		

		protected function set editMode(value:Boolean):void
		{
		Model.state.uiState = value ? State.UI_STATE_EDITING_SCENE : State.UI_STATE_NORMAL;
			
			dispatchEvent(new Event("edit_mode_change"))
		}
		
		private function onGuideChange(event:Event):void
		{
			addGuideListeners();
		}
		
		private function onSceneChange(event:Event):void
		{
			addSceneListeners();
		}
		
		/**
		 * If the user navigates scene while eidting the guide
		 */
		private function onLoadingSceneChange(event:Event):void
		{
			if(Model.currentScene && Model.currentScene.changed)
			{
				sceneSaved = false;
				guideSaved = true;
				showIndex = SHOW_SAVING;
				saveSceneData();
			}
		}
		
		private function addGuideListeners():void
		{
			if(!Model.guideData)
				return;
			
			Model.guideData.addEventListener(Event.CHANGE, onGuideDataChange);
		}
		
		private function onGuideDataChange(event:Event):void
		{
			showIndex = SHOW_BUTTON;
		}
		
		private function addSceneListeners():void
		{
			if(!Model.guideScene)
				return;
			
			Model.guideScene.addEventListener(Event.CHANGE, onGuideSceneDataChange);
		}
		
		private function onGuideSceneDataChange(event:Event):void
		{
			if(!Model.guideScene)
				return;
			
			
			showIndex = SHOW_BUTTON;
		}
		
		protected function save():void
		{
			if(Model.guideData)
				saveGuide();
			else
				guideSaved = true;
			
			if(Model.isEditableScene)
				saveScene();
			else
				sceneSaved = true;
			
			Model.dispatchEvent(new Event(Model.GUIDE_SAVED));
		}
		
		protected function cover():void
		{
		}
		
		protected function newSection():void
		{
			var newSection:GuideSectionData = GuideSectionData.from(
				new Uuid().toString(), Model.guideData.id, "NEW SECTION", null);
			
			Model.guideData.addGuideSection(newSection);
			
			Model.currentSection = newSection;
		}
		
		protected function newTask():void
		{
			var task1:GameTaskData = GameTaskData.create(
				Model.guideData.id, GameTaskData.GAME_PANO_DRAGGED
			);
			task1.instructions = "Welcome to the "+Model.guideData.title+" game. " +
				"Your first task is to drag the pano with your finger or mouse.";
			task1.feedback = "Congratulations, this is how you can move around the scene";
			Model.guideData.addGameTask(task1, 0);
			
			Model.state.dispatchEvent(new Event(State.GAME_VIEW_CHANGE));
			showGameIndex = SHOW_HIDE_GAME;
		}
		
		protected function hideGame():void
		{
            Model.state.hideGame = true;
			if(Model.hasGame)
				showGameIndex = SHOW_EDIT_GAME;
			else
				showGameIndex = SHOW_NEW_GAME_BUTTON;	
		}
		
		protected function showGame():void
		{
			if(Model.hasGame){
				Model.state.hideGame = false;
				showGameIndex = SHOW_HIDE_GAME;
			}
			else
				showGameIndex = SHOW_NEW_GAME_BUTTON;	
		}
		
		
		
		private function saveGuide():void
		{
			trace("saveGuide()");
			showIndex = SHOW_SAVING;
			guideSaved = false;
			
			var newPageImageToSave:Boolean = false;
			
			if(Model.guideData.newThumb)
				saveGuideThumb(Model.guideData.newThumb);
			else{
				//check for new page images to upload
				for(var i:int = 0; i<Model.guideData.guideSections.length; i++){
					if(Model.guideData.guideSections[i].newPageImage){
						if(i < Model.guideData.guideSections.length)
							savePageImage(Model.guideData.guideSections[i].newPageImage,Model.guideData.guideSections[i]);
							newPageImageToSave = true;
					}
				}
						
				if (!newPageImageToSave)
						saveGuideData();
			
				}
		}
		
		
		private function saveGuideThumb(thumbshot:BitmapData):void
		{
			trace("Defaults.saveSceneThumb()");
			
			var guide:GuideData = Model.guideData;
			guide.thumbIncrement ++;
			
			var data:SendBytes = new SendBytes();
			data.addVar("thumbName", guide.thumbName);
			data.addFile("thumb", guide.thumbName, pngEncoder.encode(thumbshot));
			
			var url:String = Model.config.serviceUrl+"/guide/"+guide.id+"/thumb";
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.contentType = SendBytes.CONTENT_TYPE;
			request.data = data;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onSaveGuideThumbIOError);
			loader.addEventListener(Event.COMPLETE, onSaveGuideThumbComplete);
			loader.load(request);
		}
		private var thisSection:GuideSectionData;
		
		private function savePageImage(image:BitmapData, section:GuideSectionData):void
		{
			trace("Defaults.savePageImage()");
			var data:SendBytes = new SendBytes();
			
			section.page_image_increment ++;

			data.addVar("pageImageName", section.pageImageName);
			data.addFile("pageImage", section.pageImageName, pngEncoder.encode(image));
			
			var url:String = Model.config.serviceUrl+"/guideSection/"+section.id+"/image";
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.contentType = SendBytes.CONTENT_TYPE;
			request.data = data;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onSaveGuideThumbIOError);
			loader.addEventListener(Event.COMPLETE, onSavePageImageComplete);
			loader.load(request);
			
			thisSection = section;
		}
		
		private function onSaveGuideThumbIOError(event:Event):void
		{
			trace("Defaults.onSaveGuideThumbIOError()");
			showIndex = SHOW_ERROR;
			setTimeout(revertToButton, 2000)
		}

		private function onSaveGuideThumbComplete(event:Event):void
		{
			trace("Defaults.onSaveGuideThumbComplete()");
			Model.guideData.newThumb = null;
			saveGuideData();
		}
		
		private function onSavePageImageComplete(event:Event):void
		{
			trace("Defaults.onSavePageImageComplete()");
			thisSection.page_image = thisSection.pageImageName;
			thisSection.newPageImage = null;
			saveGuideData();
		}
		
		private function saveGuideData():void
		{
			trace("saveGuideData()");
			var guide:GuideData = Model.guideData;
			
			var json:String = JSONBeek.encode(guide.data);
			var url:String = Model.config.serviceUrl+"/guide/"+guide.id+"/save";
			
			var variables:URLVariables = new URLVariables();  
			variables['json'] = JSONBeek.encode(guide.data);
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = variables;  
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onSaveGuideIOError);
			loader.addEventListener(Event.COMPLETE, onSaveGuideComplete);
			loader.load(request);		
		}
		
		private function onSaveGuideIOError(event:Event):void
		{
			trace("onSaveGuideIOError()");
			showIndex = SHOW_ERROR;
			setTimeout(revertToButton, 2000)
		}
		
		private function onSaveGuideComplete(event:Event):void
		{
			trace("onSaveGuideComplete()");
			var json:String = URLLoader(event.target).data;
			
			var data:Object = JSONBeek.decode(json);
			var response:ServiceResponse = new ServiceResponse(data);
			
			if(response.error > 0)
			{
				// Error handling here
				throw new Error(response.message);
			}
			guideSaved = true;
			
			if(sceneSaved)
			{
				showIndex = SHOW_COMPLETE;
				setTimeout(revertToButton, 2000)
				Model.dispatchEvent(new Event(Model.GUIDE_SAVED));
			}
		}
		
		private function revertToButton():void
		{
			showIndex = SHOW_BUTTON;
		}
		
		private function saveScene():void
		{
			trace("SavePanel.saveScene()");
			sceneSaved = false;
			showIndex = SHOW_SAVING;
			
			if(Model.currentScene.newThumb)
				saveSceneThumb(Model.currentScene.newThumb);
			
			else
				saveSceneData();
		}
		
		
		private function saveSceneThumb(thumbshot:BitmapData):void
		{
			trace("Defaults.saveSceneThumb()");
			
			var scene:SceneData = Model.currentScene;
			
			scene.thumbIncrement ++;
			
			var data:SendBytes = new SendBytes();
			data.addVar("thumbName", scene.thumbName);
			data.addFile("thumb", scene.thumbName, jpgEncoder.encode(thumbshot));
			
			var url:String = Model.config.serviceUrl+"/scene/"+scene.id+"/thumb";
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.contentType = SendBytes.CONTENT_TYPE;
			request.data = data;
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onSaveThumbIOError);
			loader.addEventListener(Event.COMPLETE, onSaveThumbComplete);
			loader.load(request);
		}
		
		private function onSaveThumbIOError(event:Event):void
		{
			trace("Defaults.onSaveThumbIOError()");
			showIndex = SHOW_ERROR;
			setTimeout(revertToButton, 2000)
		}
		
		private function onSaveThumbComplete(event:Event):void
		{
			trace("Defaults.onSaveThumbComplete()");
			Model.currentScene.newThumb = null;
			saveSceneData();
		}
		
		private function saveSceneData():void
		{
			var scene:SceneData = Model.currentScene;
			var url:String = Model.config.serviceUrl+"/scene/"+scene.id+"/save";
			
			var variables:URLVariables = new URLVariables();  
			variables['json'] = JSONBeek.encode(scene.data);
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = variables;  
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, onSaveSceneIOError);
			loader.addEventListener(Event.COMPLETE, onSaveSceneComplete);
			loader.load(request);
		}
		
		private function onSaveSceneIOError(event:Event):void
		{
			trace("SavePanel.onSaveSceneIOError()");
			showIndex = SHOW_ERROR;
			setTimeout(revertToButton, 2000)
		}
		
		private function onSaveSceneComplete(event:Event):void
		{
			trace("SavePanel.onSaveSceneComplete()");
			var json:String = URLLoader(event.target).data;
			
			var data:Object = JSONBeek.decode(json);
			var response:ServiceResponse = new ServiceResponse(data);
			
			if(response.error > 0)
			{
				// Error handling here
				throw new Error(response.message);
			}
			
			sceneSaved = true;
			
			if(guideSaved)
			{
				showIndex = SHOW_COMPLETE;
				setTimeout(revertToButton, 1000)
				Model.dispatchEvent(new Event(Model.GUIDE_SAVED));
			}
		}
	}
}

