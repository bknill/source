package co.beek.admin.guide.contents
{
	import com.laiyonghao.Uuid;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	
	import co.beek.event.GuideSectionEvent;
	import co.beek.model.Model;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GuideSectionData;
	
	public class ContentsAdmin extends Canvas
	{
		[Bindable]
		protected var sections:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var section:GuideSectionData;
		
		public function ContentsAdmin()
		{
			super();
			
			if(Model.guideData)
				for(var i:int=0; i<Model.guideData.guideSections.length; i++)
					if(!Model.guideData.guideSections[i].parent_section_id)
						sections.addItem(Model.guideData.guideSections[i]);
			
		}
		
		protected function onSectionDelete(event:GuideSectionEvent):void
		{
			Model.guideData.removeGuideSection(event.guideSection);
			
			var index:int = sections.getItemIndex(event.guideSection);
			if(index > -1)
				sections.removeItemAt(index);
		}
		
		protected function onDragEnter(event:DragEvent):void
		{
			DragManager.acceptDragDrop(SectionsList(event.target));
		}
		
		protected function onSectionDragComplete(event:DragEvent):void
		{
			if (event.dragInitiator is SectionsList)
				for(var i:int; i<sections.length; i++)
					GuideSectionData(sections[i]).order = i;
			
			Model.guideData.reorderGuideSections();
		}
		
		protected function onAddSectionClick(event:MouseEvent):void
		{
			var section:GuideSectionData = GuideSectionData.from(
				new Uuid().toString(), Model.guideData.id, "NEW SECTION", null
			)
			Model.guideData.addGuideSection(section);
			sections.addItem(section);
		}
		
		[Bindable (event="guide_change")]
		protected function get hasGame():Boolean
		{
			return Model.guideData.gameTasks.length > 0;
		}
		
		protected function addNewGame():void
		{
			var task1:GameTaskData = GameTaskData.create(
				Model.guideData.id, GameTaskData.GAME_PANO_DRAGGED
			);
			task1.instructions = "Welcome to the "+Model.guideData.title+" game. " +
				"Your first task is to drag the pano with your finger or mouse.";
			task1.feedback = "Congratulations, this is how you can move around the scene";
		
			Model.guideData.addGameTask(task1, 0);
			
			Model.currentTaskIndex = 0;
			
		}

	}
}