package co.beek.admin.poup
{
	import co.beek.model.Constants;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.events.IndexChangedEvent;
	
	import spark.events.IndexChangeEvent;

	public class SceneTypeBrowser extends CloseablePanel
	{
		public static const TYPE_SELECTED:String = "TYPE_SELECTED";
		
		[Bindable]
		protected var types:ArrayCollection = new ArrayCollection(Constants.typeNames);
		
		[Bindable]
		protected var selectedIndex:int;
		
		public function SceneTypeBrowser()
		{
			super();
		}
		
		protected function onListSelectionChange(event:IndexChangeEvent):void
		{
			selectedIndex = event.newIndex;
			dispatchEvent(new Event(TYPE_SELECTED));
		}
		
		public function set type(value:int):void
		{
			selectedIndex = value;
		}
		
		public function get type():int
		{
			return selectedIndex;
		}
	}
}