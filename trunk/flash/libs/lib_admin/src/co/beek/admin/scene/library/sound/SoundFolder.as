package co.beek.admin.scene.library.sound
{
	import co.beek.model.data.FileData;
	import co.beek.model.data.FreeSoundData;

	public class SoundFolder
	{
		public static const FOLDER_ID_PEOPLE:String = "1";
		public static const FOLDER_ID_INTERIOR:String = "2";
		public static const FOLDER_ID_EXTERIOR:String = "3";
		public static const FOLDER_ID_CLASSIC:String = "4";
		public static const FOLDER_ID_MODERN:String = "5";
		
		private var _id:String;
		private var _fileName:String;
		private var _children:Array = [];
		
		public function SoundFolder(id:String, fileName:String)
		{
			_id = id;
			_fileName = fileName;
		}
		
		public function get id():String
		{
			return _id;
		}

		public function get fileName():String
		{
			return _fileName;
		}
		
		public function get name():String
		{
			return _fileName;
		}
		
		public function get children():Array
		{
			return _children;
		}
		
		public function addFile(file:FileData):void
		{
			_children.push(file);
		}
		
		public function addFreeSound(file:FreeSoundData):void
		{
			_children.push(file);
		}
	}
}