package co.beek.admin.scene.library.media
{
	import caurina.transitions.Tweener;
	
	import co.beek.admin.BeekDragManager;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.PhotoData;
	
	import com.laiyonghao.Uuid;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.treeClasses.*;
	
	
	public class MediaFolderRenderer extends TreeItemRenderer
	{
		private var dragProxy:Image = new Image();
		
		private var originalPhotoData:PhotoData;
		
		private var fileData:FileData;
		
		private var photoIcon:Sprite = new PhotoRendererFl;
		
		public function MediaFolderRenderer()
		{
			super();
			
			dragProxy.source = PhotoRendererFl;
			dragProxy.width = 40;
			dragProxy.height = 40;
			photoIcon.width = 20;
			photoIcon.height = 20;
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		private function onMouseOver(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
		}
		
		private function onMouseDown(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onMouseMove(event:MouseEvent):void 
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			var id:String = new Uuid().toString();
			var photoData:PhotoData = PhotoData.createForGuide(id, Model.guideScene.id);
			photoData.title = originalPhotoData.title;
			photoData.description = originalPhotoData.description;
			photoData.file = originalPhotoData.file;
			photoData.buttonFavourite = originalPhotoData.buttonFavourite;
			photoData.buttonLabel = originalPhotoData.buttonLabel ;
			photoData.buttonUrl = originalPhotoData.buttonUrl;
			photoData.buttonScene = originalPhotoData.buttonScene;
			photoData.distance = originalPhotoData.distance ;
			photoData.mask = originalPhotoData.mask ;
			photoData.outline = originalPhotoData.outline ;
			photoData.fill = originalPhotoData.fill ;
			photoData.thumb = originalPhotoData.thumb ;
			photoData.webUrl = originalPhotoData.webUrl ;
			
			BeekDragManager.startDrag(event, photoData, dragProxy);
		}
		
		private function onMouseOut(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;

			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			
		}
		
		
		private function onMouseUp(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		
		override public function set listData(value:BaseListData):void
		{
			super.listData = value;
			photoIcon.visible = TreeListData(listData).hasChildren == false;
		}
		
		override public function set data(value:Object):void
		{
			super.data = value;
			originalPhotoData = value as PhotoData;
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			super.label.width = 100;
		}
		
		override protected function childrenCreated():void{
			super.childrenCreated();
			
			addChild(photoIcon);
			
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			if(super.data){
				var treeListData:TreeListData = TreeListData(super.listData);
				//previewButton.visible = treeListData.hasChildren == false;
				if(!treeListData.hasChildren)
				{
					super.label.x = 28;
					super.label.width = 150;
					
					photoIcon.x = 10;
					
				}
				
			}
			
			graphics.clear();
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			graphics.endFill();
		}
	}
}