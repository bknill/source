package co.beek.admin.scene.hotspot.photo.editor
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.hotspots.PhotoData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import spark.components.BorderContainer;

	public class PhotoMaskEditor extends CloseablePanel
	{
		public var maskLayer:BorderContainer;
		
		[Bindable]
		protected var imageUrl:Object;
		
		private var _photoData:PhotoData;
		
		private var points:Vector.<Point>;
		
		public function PhotoMaskEditor()
		{
			super();
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			drawPhotoMask();
		}
		
		public function set photoData(value:PhotoData):void
		{
			_photoData = value;
			
			// set the bindable properties
			imageUrl = Model.config.assetCdn+"/"+_photoData.file.realName;
			
			points = _photoData.mask;
		}
		
		protected function get minImageWidth():Number
		{
			if(!points || points.length == 0)
				return 100;
			
			var w:Number = points[0].x;
			for(var i:int=1; i<points.length; i++)
				w = Math.max(w, points[i].x);
			return w;
		}
		
		protected function get minImageHeight():Number
		{
			if(!points || points.length == 0)
				return 100;
			
			var h:Number = points[0].y;
			for(var i:int=1; i<points.length; i++)
				h = Math.max(h, points[i].y);
			return h;
		}
		
		protected function onMaskLayerClick(event:MouseEvent):void
		{
			points.push(new Point(event.localX, event.localY));
			
			drawPhotoMask();
		}
		
		private function drawPhotoMask():void
		{
			maskLayer.graphics.clear();
			maskLayer.graphics.lineStyle(2, 0xFFFFFF);
			maskLayer.graphics.beginFill(0xFFFFFF, 0.3);
			
			if(points.length == 0)
				return;
			
			maskLayer.graphics.moveTo(points[0].x, points[0].y);
			for(var i:int=1; i<points.length; i++)
				maskLayer.graphics.lineTo(points[i].x, points[i].y);
				
		}
		
		[Bindable (event="outlineChange")]
		public function get outline():Boolean
		{
			return _photoData.outline;
		}
		
		public function set outline(value:Boolean):void
		{
			if(_photoData.outline == value)
				return;
			
			_photoData.outline = value;
			dispatchEvent(new Event("outlineChange"));
		}

		[Bindable (event="fillChange")]
		public function get fill():Boolean
		{
			return _photoData.fill;
		}
		
		public function set fill(value:Boolean):void
		{
			if(_photoData.fill == value)
				return;
			
			_photoData.fill = value;
			dispatchEvent(new Event("fillChange"));
		}
		
		protected function onClearClick():void
		{
			points = new Vector.<Point>();
			drawPhotoMask();
		}
		
		protected function onDoneClick():void
		{
			_photoData.mask = points;
			dispatchCloseEvent();
		}
	}
}