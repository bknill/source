package co.beek.admin.scene.hotspot.board
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.service.LocationService;
	import co.beek.service.data.SceneBasicList;
	
	public class BoardControls extends AbstractControlPanel
	{
		[Bindable]
		public var locationScenes:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var viewStackIndex:int = 0;
		
		private var childrenCreateComplete:Boolean;
		
		public function BoardControls()
		{
			super();
			addEventListener("showConfig", onShowConfig);
		}
		
		[Bindable (event="hostspot_change")]
		public function get boardData():BoardData
		{
			return BoardData(_hotspotData);
		}
		
		protected override function activate():void
		{
			super.activate();
			
			//if(locationScenes.length ==0 )
				loadLocationScenes();
		}
		
		override protected function childrenCreated():void
		{
			super.childrenCreated();
			
			childrenCreateComplete = true;
		}
		
		private function onShowConfig(event:Event):void
		{
			viewStackIndex = viewStackIndex == 0 ? 1 : 0;
		}
		
		private function loadLocationScenes():void
		{
			locationScenes.removeAll();
			
			var locationId:String = Model.currentScene.location.id;
			LocationService.getScenesForLocation(locationId, onSceneBasicList);
		}
		
		private function onSceneBasicList(list:SceneBasicList):void
		{
			var scenes:Vector.<SceneBasicData> = list.scenes.sort(orderScenes);
			
			for(var i:int=0; i<scenes.length; i++)
				if(scenes[i].id != Model.currentScene.id)
					locationScenes.addItem(new SceneRendererData(scenes[i], boardData));
			
			dispatchEvent(new Event("SCENES_LOADED"));
		}
		
		/**
		 * Order the scenes so they are in the same order as on the signboard
		 */
		private function orderScenes(s1:SceneBasicData, s2:SceneBasicData): int
		{
			return int(s1.id) - int(s2.id);
		}
		
		private function removeCurrentScene(scenes:Vector.<SceneBasicData>):Vector.<SceneBasicData>
		{
			var currentSceneId:String = Model.currentScene.id;
			for(var i:int=0; i<scenes.length; i++)
				if(scenes[i].id == currentSceneId)
					scenes.splice(i, 1);
			
			return scenes;
		}
	}
}