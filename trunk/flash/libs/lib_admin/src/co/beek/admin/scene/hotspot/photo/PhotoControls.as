package co.beek.admin.scene.hotspot.photo
{
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.admin.scene.hotspot.photo.editor.PhotoMaskEditor;
	import co.beek.admin.scene.hotspot.photo.editor.PhotoMaskEditorMX;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.RichTextEditor;
	import mx.managers.PopUpManager;
	
	public class PhotoControls extends AbstractControlPanel
	{

		[Bindable]
		public var buttonScene:SceneBasicData;
		
		[Bindable]
		protected var photoHtmlText:String;
		
		public function PhotoControls()
		{
			super();
		}
		
		protected function compactifyToolbar(rte:RichTextEditor):void {
			rte.toolbar.removeChild(rte['fontFamilyCombo']);
			//rte.toolbar.removeChild(rte['colorPicker']);
			rte.toolBar2.removeChild(rte['italicButton']);
			rte.toolBar2.removeChild(rte['underlineButton']);
			rte.toolbar.removeChild(rte['_RichTextEditor_VRule2']);
			//rte.toolbar.removeChild(rte['linkTextInput']);
		}

		/*[Bindable (event="hostspot_change")]
		protected function get photoHtmlText():String {
			return 
		}*/
		
		

		protected function handleChange(rte:RichTextEditor):void {
			//rte.htmlText = Fonts.cleanHTML(rte.htmlText);
			photoData.description = rte.htmlText;
		}
		
		protected override function activate():void
		{
			super.activate();
			
			photoHtmlText = Fonts.cleanHTML(photoData.description);
			//buttonScene = photoData.buttonScene;			
		}
		
		protected override function deactivate():void
		{
			super.deactivate();
			//photoData.description = photoHtmlText;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get photoData():PhotoData
		{
			return _hotspotData as PhotoData;
		}
		[Bindable (event="hostspot_change")]
		public function get guideScenes():ArrayCollection
		{
			var array:ArrayCollection = new ArrayCollection();
			for each(var scene:GuideSceneData in Model.guideData.guideScenes)
				array.addItem(scene.scene);
					
			return  array;
		}
		
		[Bindable (event="hostspot_change")]
		public function get sceneButtonIndex():int
		{
			for(var i:int = 0;i<guideScenes.length; i++)
				if(guideScenes[i].id == photoData.buttonSceneId)
					return i
		
			return 0;
		
		}
		
		[Bindable (event="hostspot_change")]
		public function get noButton():Boolean
		{
			
			return !photoData.buttonFavourite && !photoData.buttonScene && !photoData.buttonUrl
		}
		
		public function clearButton():void
		{
			photoData.buttonScene = null;		
		}
		
		
		[Bindable (event="hostspot_change")]
		public function get hasBackground():Boolean
		{
			return photoData.frame != PosterData.FRAME_NONE;
		}
		
		public function set hasBackground(value:Boolean):void
		{
			photoData.frame = value ? PhotoData.TYPE_FRAME_PLASTIC : PhotoData.NO_FRAME;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get onGuide():Boolean
		{
			return Model.guideScene && Model.guideScene.hasHotspot(photoData);
		}
		
		
		protected function set onGuide(value:Boolean):void
		{
			if(onGuide == value)
				return
			
			var original:PhotoData = photoData;
			var data:Object = photoData.data;
			
			_hotspotData = null; // so admin doenst close
			
			if(value)
			{
				data["id"] = new Uuid().toString();
				data["sceneId"] = null;
				data["guideSceneId"] = Model.guideScene.id;
				var guidePhoto:PhotoData = new PhotoData(data);
				
				// Remove from scene, add to guideScene
				Model.currentScene.removeHotspot(original);
				Model.guideScene.addHotspot(guidePhoto);
				_hotspotData = guidePhoto;
				
			}
			else
			{
				data["id"] = new Uuid().toString();
				data["sceneId"] = Model.currentScene.id;
				data["guideSceneId"] = null;
				var scenePhoto:PhotoData = new PhotoData(data);
				
				// Remove from guidescene, add to scene
				Model.guideScene.removeHotspot(original);
				Model.currentScene.addHotspot(scenePhoto);
				_hotspotData = scenePhoto;
			}
		}
		
		protected function editMask():void
		{
			var b:PhotoMaskEditor = new PhotoMaskEditorMX;
			b.addEventListener(CloseablePanel.CLOSE, onPhotoMaskEditorClose);
			b.photoData = photoData;
			PopUpManager.addPopUp(b, this, true);
			PopUpManager.centerPopUp(b);
		}
		
		private function onPhotoMaskEditorClose(event:Event):void
		{
			removePopup(PhotoMaskEditor(event.target));
		}
		
		private function removePopup(panel:PhotoMaskEditor):void
		{
			panel.removeEventListener(CloseablePanel.CLOSE, onPhotoMaskEditorClose);
			PopUpManager.removePopUp(panel);
		}
		
	}
}