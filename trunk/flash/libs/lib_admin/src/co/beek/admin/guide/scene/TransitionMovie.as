package co.beek.admin.guide.scene
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.service.data.FileUploader;
	
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	
	public class TransitionMovie extends CloseablePanel
	{
		
		[Bindable]
		protected var progress:String;
		
		public function TransitionMovie()
		{
			super();
		}
		
		[Bindable]
		public var guideScene:GuideSceneData;
		
		
		protected function onVideoButtonClick(event:MouseEvent):void
		{
			browseForFlv();
		}
		
		
		private var fileRef:FileReference = new FileReference;
		
		[Bindable]
		protected var uploadProgress:String;
		
		protected function browseForFlv():void
		{
			fileRef.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			fileRef.addEventListener(Event.SELECT, onFileReferenceSelect);
			
			fileRef.addEventListener(Event.COMPLETE, onDataLoaded);
			fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileReferenceComplete);
			fileRef.browse([new FileFilter("Video files", "*.mp4")]);
		}
		
		protected function onFileReferenceSelect(event:Event):void 
		{
			var fileRef:FileReference = FileReference(event.target);
			fileRef.load();
		}
		
		
		
		protected function onDataLoaded(event:Event):void
		{
			fileRef.removeEventListener(Event.COMPLETE, onDataLoaded);
			
			var uploader:FileUploader = new FileUploader;
			uploader.addEventListener(ProgressEvent.PROGRESS, onVideoUploadProgress);
			uploader.addEventListener(Event.COMPLETE, onUploadComplete);
			uploader.upload( "txransitionvideo.mp4", fileRef.data, true);
			
			uploadProgress = "Uploading...";
		}
		
		protected function onVideoUploadProgress(event:ProgressEvent):void
		{
			trace("onVideoUploadProgress"+event.bytesTotal+" < "+event.bytesLoaded);
			if(event.bytesTotal > 100 && event.bytesTotal < event.bytesLoaded)
				uploadProgress = "uploading: "+ Math.round(event.bytesLoaded/event.bytesTotal * 100)+"%";
		}
		
		protected function onUploadComplete(event:Event):void 
		{
			trace('onUploadComplete()');
			var uploader:FileUploader = FileUploader(event.target);
			uploader.removeEventListener(ProgressEvent.PROGRESS, onVideoUploadProgress);
			uploader.removeEventListener(Event.COMPLETE, onUploadComplete);
			uploadProgress = null;
			
			// set the returned file on the photo
			guideScene.transitionVideo = uploader.fileData.realName;
			
			dispatchEvent(new Event("videoChange"));
		}
		
		protected function onIOError(event:IOErrorEvent):void
		{
			throw new Error("Error when uploading.");
		}
		
		
		protected function onFileReferenceComplete(event:DataEvent):void 
		{
			trace("onFileReferenceComplete");
		}
	}
}