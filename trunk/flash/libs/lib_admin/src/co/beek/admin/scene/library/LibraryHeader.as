package co.beek.admin.scene.library
{
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	
	public class LibraryHeader extends Canvas
	{
		private var _heading:String;
		private var _helpText:String;
		
		public var content:UIComponent;
		
		public function LibraryHeader()
		{
			super();
		}
		
		[Bindable (event="heading_changed")]
		public function get heading():String
		{
			return _heading;
		}
		
		public function set heading(value:String):void
		{
			_heading = value;
			dispatchEvent(new Event("heading_changed"));
		}
		
		[Bindable (event="help_changed")]
		public function get helpText():String
		{
			return _helpText;
		}
		
		public function set helpText(value:String):void
		{
			_helpText = value;
			dispatchEvent(new Event("help_changed"));
		}
	}
}