package co.beek.admin.component
{
	import mx.controls.Button;
	
	/**
	 * Wierd name cause it was causing conflicts.
	 */
	public class CloseButtonCl extends Button
	{
		[Embed("/../images/button_close.png")] 
		private var skin: Class; 
		
		public function CloseButtonCl()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 25;
		}
	}
}