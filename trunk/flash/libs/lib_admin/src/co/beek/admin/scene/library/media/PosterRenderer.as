package co.beek.admin.scene.library.media
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PosterData;
	
	import com.laiyonghao.Uuid;
	
	public class PosterRenderer extends DraggableHotspot 
	{
		public function PosterRenderer()
		{
			super(PosterRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return Model.guideScene != null 
				? PosterData.createForGuide(id, Model.guideScene.id)
				: PosterData.createForScene(id, Model.currentScene.id);
		}
	}
}