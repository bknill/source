package co.beek.admin.guide.task
{
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GameHotspotEvent;
	import co.beek.model.guide.GameTaskHotspot;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.managers.PopUpManager;
	
	import spark.components.TextInput;
	import spark.components.supportClasses.ItemRenderer;
	
	[Event(name="removeHotspot", type="co.beek.admin.guide.sections.SectionEvent")]
	public class GameHotspotRenderer extends ItemRenderer
	{
		[Bindable]
		public var titleTextInput:TextInput;

		/**
		 * For some reason, the native selected value is not bindable
		 */
		[Bindable]
		protected var isSelected:Boolean;
		
		
		[Bindable]
		protected var bodyFont:String = Fonts.bodyFont;
		
		public function GameHotspotRenderer()
		{
			super();
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			isSelected = value;
		}
		
		[Bindable (event="dataChange")]
		protected function get gameHotspotData():GameTaskHotspot
		{
			return data as GameTaskHotspot;
		}
		
		protected function onDeleteButtonClick(event:MouseEvent):void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove the hotspot from this task?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			dispatchEvent(new GameHotspotEvent("removeHotspot", gameHotspotData));
		}
		
		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
	}
}