package co.beek.admin.scene
{
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import co.beek.model.Constants;

	public class IconUtil
	{
		public static function getIcon(type:int):Sprite
		{
			var icons:Dictionary = Constants.getIconsDictionary(new AdminTypeIconsFl);
			return icons[type];
		}
	}
}