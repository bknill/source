package co.beek.admin.poup
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.utils.JSONBeek;
	
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.containers.Panel;
	import mx.controls.ProgressBar;
	
	public class FileUploader extends CloseablePanel
	{
		public static const UPLOAD_COMPLETE:String = "UPLOAD_COMPLETE";
		public var progressBar:ProgressBar;
		
		private var fileRef:FileReference = new FileReference;
		
		private var teamId:String;
		
		private var _file:FileData;
		
		[Bindable]
		protected var uploadReady:Boolean;
		
		public function FileUploader()
		{
			super();
			
			fileRef.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			fileRef.addEventListener(Event.COMPLETE, onDataLoaded);
			fileRef.addEventListener(Event.SELECT, onFileReferenceSelect);
			fileRef.addEventListener(Event.CANCEL, onFileReferenceCancel);
			fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileReferenceComplete);
			fileRef.addEventListener(ProgressEvent.PROGRESS, onUploadProgress);
		}
		
		public function browse(teamId:String, type:String, filter:String):void
		{
			this.teamId = teamId;
			fileRef.browse([new FileFilter(type, filter)]);
		}
		
		public function get file():FileData
		{
			return _file;
		}
		
		private function get uploadUrl():String
		{
			return Model.config.serviceUrl+"/files/uploadMultipart/;jsessionid="+Model.sessionData.sessionId;
		}
		
		protected function onIOError(event:IOErrorEvent):void
		{
			throw new Error("Error when uploading to:"+uploadUrl);
		}
		
		protected function onFileReferenceCancel(event:Event):void 
		{
			// do nothing
		}
		
		protected function onFileReferenceSelect(event:Event):void 
		{
			uploadReady = true;
		}

		protected function onUploadProgress(event:ProgressEvent):void 
		{
			trace("onUploadProgress("+event.bytesLoaded+", "+event.bytesTotal+")");
			progressBar.setProgress(event.bytesLoaded, event.bytesTotal);
		}
		
		protected function onDataLoaded(event:Event):void
		{
			//
		}
		
		protected function startUpload():void
		{
			var request:URLRequest = new URLRequest(uploadUrl)

			request.method = URLRequestMethod.POST;
			try 
			{
				fileRef.upload(request, "file");
			} 
			catch (error:Error) 
			{
				trace("Unable to upload file.");
			}
			uploadReady = false;
		}
		
		protected function onFileReferenceComplete(event:DataEvent):void 
		{
			trace("onFileReferenceComplete");
			var data:Object;
			try
			{
				data = JSONBeek.decode(event.data)
			}
			catch(e:Error)
			{
				throw new Error("Error decoding JSON for file:"+event.data)
			}
			
			if(data.error)
				throw new Error(data.message);
			
			// set the returned file on the photo
			_file = new FileData(data);
			
			dispatchEvent(new Event(UPLOAD_COMPLETE));
		}
		
		public function dispose():void
		{
			_file = null;
			
			fileRef.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			fileRef.removeEventListener(Event.COMPLETE, onDataLoaded);
			fileRef.removeEventListener(Event.SELECT, onFileReferenceSelect);
			fileRef.removeEventListener(Event.CANCEL, onFileReferenceCancel);
			fileRef.removeEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileReferenceComplete);
			fileRef.removeEventListener(ProgressEvent.PROGRESS, onUploadProgress);
			fileRef = null;
		}
	}
}