package co.beek.admin.poup
{
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.service.TeamService;
	import co.beek.service.data.LinkRequestOptions;
	import co.beek.session.EWTeamData;
	
	import mx.collections.ArrayCollection;
	
	import spark.components.Panel;
	
	public class GuideLinkRequest extends CloseablePanel
	{
		/**
		 * Select a guide in the destination users list of abailable guides
		 */
		[Bindable]
		protected var myLocations:ArrayCollection = new ArrayCollection();
		
		/**
		 * Select a guide in the destination users list of abailable guides
		 */
		[Bindable]
		protected var theirGuides:ArrayCollection = new ArrayCollection();

		[Bindable]
		protected var location:LocationData;
		
		[Bindable]
		protected var guide:GuideBasicData;
		
		[Bindable]
		protected var message:String;
		
		private var _linkRequestOptions:LinkRequestOptions;
		
		public function GuideLinkRequest()
		{
			super();
		}
		
		public function set linkRequestOptions(value:LinkRequestOptions):void
		{
			_linkRequestOptions = value;
			
			populateMyLocations(value.myLocations.locations);
			populateTheirGuides(value.theirGuides.guides);
		}
		
		private function populateMyLocations(locations:Vector.<LocationData>):void
		{
			for(var i:int = 0; i<locations.length; i++)
				myLocations.addItem(locations[i]);
			
			location = locations[0];
		}

		private function populateTheirGuides(guides:Vector.<GuideBasicData>):void
		{
			for(var i:int = 0; i<guides.length; i++)
				theirGuides.addItem(guides[i]);
			
			guide = guides[0];
		}
		
		protected function sendRequest():void
		{
			var myGuideId:String = Model.guideData.id;
			var theirLocationId:String = _linkRequestOptions.locationId;
			var theirGuideId:String = guide.id;
			var myLocationId:String = location.id;
			
			TeamService.sendLinkRequest(
				myGuideId, theirLocationId, 
				theirGuideId, myLocationId, 
				message, 
				onRequestCallback
			); 
		}
		
		private function onRequestCallback():void
		{
			dispatchCloseEvent();
		}
	}
}