package co.beek.admin.scene.hotspot.sound
{
	import mx.collections.ArrayCollection;
	
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.SoundData;
	
	public class SoundControls extends AbstractControlPanel
	{
		//[Bindable]
		//public var other:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		public var selectedHotspotIndex:int;
		
		public function SoundControls()
		{
			super();
		}
		
		protected override function activate():void
		{
			super.activate();
			
			
			
			/*other.removeAll();
			other.addItem({"title":"Nothing"});
			addOtherHotspots(Model.currentScene.hotspots);
			if(Model.guideScene)
				addOtherHotspots(Model.guideScene.hotspots);*/
		}
		
		/*private function addOtherHotspots(hotspots:Vector.<HotspotData>):void
		{
			selectedHotspotIndex = 0;
			for(var i:int;i<hotspots.length; i++)
			{
				var hotspot:HotspotData = hotspots[i];
				// exclude the current hotspot
				if((hotspot is PhotoData || hotspot is PosterData) 
					&& hotspot.id != soundData.id)
					other.addItem(hotspot);
				
				if(hotspot.id == soundData.listenHotspotId)
					selectedHotspotIndex = other.length - 1;
			}
		}*/
		
		[Bindable (event="hostspot_change")]
		public function get soundData():SoundData
		{
			return _hotspotData as SoundData;
		}
		
		public function set ambience(value:Number):void
		{
			if(soundData)
				soundData.ambience = value;
		}
		
		public function set volume(value:Number):void
		{
			if(soundData)
				soundData.volume = value;
		}
		
		/*public function listenTo(hotspotId:String):void
		{
			soundData.listenHotspotId = hotspotId;
		}*/
	}
}