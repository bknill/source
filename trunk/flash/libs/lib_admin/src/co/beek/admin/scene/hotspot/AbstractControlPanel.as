package co.beek.admin.scene.hotspot
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Panel;
	
	import co.beek.admin.component.CloseButtonCl;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	
	[Event(name="closePanel", type="flash.events.Event")]
	public class AbstractControlPanel extends Panel
	{
		private var closeButton:CloseButtonCl = new CloseButtonCl;
		
		protected var _hotspotData:HotspotData;
		
		private var hotspotControlBar:HotspotControlBar = new HotspotControlBarMX();
		
		public function AbstractControlPanel()
		{
			super();
			closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			
			visible = false;
			controlBar = hotspotControlBar;
		}
			
		override protected function createChildren():void
		{
			super.createChildren();
			titleBar.addChild(closeButton);
			addChild(hotspotControlBar);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			closeButton.width = 16;
			closeButton.height = 16;
			closeButton.x = titleBar.width - closeButton.width - 8;
			closeButton.y = 8;
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			dispatchEvent(new Event("closePanel"));
		}
		
		public function set hotspotData(value:HotspotData):void
		{
			if(_hotspotData == value)
				return;
			
			if(value == null)
				deactivate();
			
			_hotspotData = value;
			hotspotControlBar.hotspotData = value;
			dispatchEvent(new Event("hostspot_change"));
			
			if(_hotspotData != null)
				activate();
				
		}
		
		[Bindable (event="hostspot_change")]
		public function get hotspotData():HotspotData
		{
			return _hotspotData;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get isGuideScene():Boolean
		{
			return Model.guideScene != null;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get isHiddenInGuideScene():Boolean
		{
			return Model.guideScene && Model.guideScene.isHidden(_hotspotData);
		}

		protected function set isHiddenInGuideScene(value:Boolean):void
		{
			if(Model.guideScene)
				Model.guideScene.setHidden(_hotspotData, value);
		}
		
		protected function activate():void
		{
			visible = true;
		}
		
		protected function deactivate():void
		{
			visible = false;
			hotspotControlBar.hotspotData = null;
		}

		public override function set rotationX(value:Number):void
		{
			if(_hotspotData)
				_hotspotData.rotationX = value;
		}

		public override function set rotationY(value:Number):void
		{
			if(_hotspotData)
				_hotspotData.rotationY = value;
		}

		public function set distance(value:Number):void
		{
			if(_hotspotData)
				_hotspotData.distance = value;
		}
	}
}