package co.beek.admin.poup
{
	import co.beek.admin.map.MapHolder;
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.map.markers.IMapMarker;
	import co.beek.map.markers.LocationMapMarker;
	import co.beek.map.markers.LocationMapMarkerSelected;
	import co.beek.model.data.LocationData;
	import co.beek.service.LocationService;
	import co.beek.service.data.LocationList;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	
	[Event(name="locationChange", type="flash.events.Event")]
	[Event(name="locationGo", type="flash.events.Event")]
	public class LocationMap extends MapHolder
	{
		public var _mapCenter:GeoZoom;
		
		protected var mapMarkers:Dictionary = new Dictionary;
		
		private var _locationData:LocationData;
		
		protected var locationsLoadTimeout:uint;
		
		[Bindable]
		protected var _listCollection:ArrayCollection = new ArrayCollection();
		
		private var locationSelectedMarker:LocationMapMarkerSelected = new LocationMapMarkerSelected(false);
		
		public function LocationMap()
		{
			super();
			locationSelectedMarker.addEventListener(LocationMapMarkerSelected.CLOSE, onLocationMapMarkerSelectedClose);
			locationSelectedMarker.addEventListener(LocationMapMarkerSelected.GO, onLocationMapMarkerSelectedGo);
			markersHolder.addChild(locationSelectedMarker);
		}
		
		public function set mapCenter(value:GeoZoom):void
		{
			_mapCenter = value;
			dispatchEvent(new Event("map_center_change"));
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			
			// timeout to wait for map to resize, load tiles
			if(_mapCenter)
				locationsLoadTimeout = setTimeout(onLoadTimeout, 1000);
		}
		
		
		
		protected function onLocationMapMarkerSelectedClose(event:Event):void
		{
			selectedLocation = null;
		}

		protected function onLocationMapMarkerSelectedGo(event:Event):void
		{
			dispatchEvent(new Event("locationGo"));
		}
		
		protected override function resetMap():void
		{
			selectedLocation = null;
			
			clearTimeout(locationsLoadTimeout);
			
			locationsLoadTimeout = setTimeout(onLoadTimeout, 1000);
		}
		
		private function onLoadTimeout():void
		{
			loadLocations();
		}
		
		[Bindable (event="locationChange")]
		public function get selectedLocation():LocationData
		{
			return _locationData;
		}
		
		public function set selectedLocation(value:LocationData):void
		{
			_locationData = value;
			
			
			if(_locationData)
				hilightLocationMarker();
			else
				hilightAllMarkers();
			
			dispatchEvent(new Event("locationChange"));
		}
		
		private function hilightLocationMarker():void
		{
			for each(var marker:Object in mapMarkers)
				DisplayObject(marker).visible = true;
			
			var m:LocationMapMarker = LocationMapMarker(mapMarkers[_locationData]);
			locationSelectedMarker.location = _locationData;
			locationSelectedMarker.visible = true;
			markersHolder.setChildIndex(locationSelectedMarker, markersHolder.numChildren-1);
			DisplayObject(m).visible = false;
			
			positionSelectedMarker();
		}
		
		private function positionSelectedMarker():void
		{
			if(!_locationData)
				return;
			
			var locationMarker:LocationMapMarker = LocationMapMarker(mapMarkers[_locationData]);
			if(!locationMarker)
				return;
			
			locationSelectedMarker.x = locationMarker.x;
			locationSelectedMarker.y = locationMarker.y;
		}

		protected function hilightAllMarkers():void
		{
			for each(var marker:Object in mapMarkers)
				DisplayObject(marker).visible = true;
				
			locationSelectedMarker.visible = false;
		}
		
		[Bindable (event="map_center_change")]
		public function get mapCenter():GeoZoom
		{
			return _mapCenter;
		}
		
		[Bindable (event="collection_change")]
		public function get listCollection():ArrayCollection
		{
			return _listCollection;
		}
		
		protected function loadLocations():void
		{
			trace("loadLocations()");
			return;
			
			LocationService.getLocationsWithin(
				topLeft, 
				bottomRight, 
				onLocationsList
			);
		}
		
		protected function removeMapMarkers():void
		{
			for each(var marker:Object in mapMarkers)
				removeMarker(marker as IMapMarker);
			
			mapMarkers = new Dictionary;
			_listCollection.removeAll();
		}
		
		private function onLocationsList(list:LocationList):void
		{
			locations = list.locations;
		}
		
		private var _locations:Vector.<LocationData>;
		
		public function set locations(value:Vector.<LocationData>):void
		{
			_locations = value;
			removeMapMarkers();
			addLocationMarkers(value);
		}
		
		public function get locations():Vector.<LocationData>
		{
			return _locations;
		}
		
		private function addLocationMarkers(locations:Vector.<LocationData>):void
		{
			var g:Vector.<Geo> = new Vector.<Geo>;
			for(var i:int=0; i<locations.length; i++)
			{
				var location:LocationData = locations[i];
				addLocationMapMarker(location, String(i+1));
				g.push(location.geo);
			}
			mapData.centerAroundAll(g);
			positionMarkers();
		}
		
		private function addLocationMapMarker(location:LocationData, num:String):void
		{
			// Ignore locations that don't have a default scene
			// these locations are buggy or imcomplete
			if(!location.defaultScene)
				return;
			
			var marker:LocationMapMarker = new LocationMapMarker(location, num);
			marker.addEventListener(MouseEvent.CLICK, onLocationMarkerClick);
			addMarker(marker);
			
			// add the 
			_listCollection.addItem(location);
			
			mapMarkers[location] = marker;
		}
		
		override protected function positionMarkers():void
		{
			super.positionMarkers();
			positionSelectedMarker();
			resetMap();
		}
		
		private function onLocationMarkerClick(event:MouseEvent):void
		{
			selectedLocation = LocationMapMarker(event.target).data;
		}
	}
}