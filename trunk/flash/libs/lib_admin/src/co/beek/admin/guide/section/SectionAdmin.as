package co.beek.admin.guide.section
{
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.FileUploader;
	import co.beek.admin.poup.FileUploaderMX;
	import co.beek.admin.poup.LocationBrowser;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.event.GuideSceneEvent;
	import co.beek.event.GuideSectionEvent;
	import co.beek.event.LocationDataEvent;
	import co.beek.event.SceneBasicEvent;
	import co.beek.loading.ImageLoader;
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	
	import com.laiyonghao.Uuid;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import flashx.textLayout.formats.Float;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	public class SectionAdmin extends Canvas
	{
		[Bindable]
		protected var scenes:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var sections:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var section:GuideSectionData;
		
		
		[Bindable]
		protected var titleFont:String = Fonts.titleFont;
		
		[Bindable]
		protected var isScenesAdmin:Boolean = false;
		
		[Bindable]
		protected var hasImage:Boolean = false;
		
		[Bindable]
		protected var isMap:Boolean = false;
		
		
		[Bindable]
		protected var isSectionsAdmin:Boolean = false;
		
		private var pageImageBitmap:BitmapData;
		
		private var zoom:int = 13;
		private var lat:Number;
		private var lon:Number;
		
		public function SectionAdmin()
		{
			super();
			trace('SectionAdmin()');
			Model.addEventListener(Model.SECTION_CHANGE, onSectionChange);
		}
		
		private function onSectionChange(e:Event):void
		{
		 	trace('SectionAdmin.onSectionChange()');
			guideSection = Model.currentSection;
		}
		
		public function set guideSection(value:GuideSectionData):void
		{
			
			showSectionScenes(value);
			showSectionChildren(value);
			
			if(value.markers == 1)
				markers(1)
			else
				markers(0)
				
			hasImage = value.page_image ? true : false;	
		}
	

		public function markers(value:int):void
		{
			if(value == 1)
			{
				isScenesAdmin = false;
				isSectionsAdmin = false;
			}
			
			if(value == 0)
			{
				isScenesAdmin = true;
				isSectionsAdmin = false
			}
		}
		
		protected function showSectionScenes(section:GuideSectionData):void
		{
			this.section = section;
			scenes.removeAll();
			
			if(!section)
				return;
			
			for(var i:int=0; i<section.guideScenes.length; i++)
				scenes.addItem(section.guideScenes[i]);
		}
		
		protected function showSectionChildren(section:GuideSectionData):void
		{
			
			this.section = section;
			sections.removeAll();
			
			if(!section)
				return;
			
			for(var i:int=0; i<Model.guideData.guideSections.length; i++)
			{
				if(Model.guideData.guideSections[i].parent_section_id == section.id)
				{
					sections.addItem(Model.guideData.guideSections[i]);	
				}
				
			
			}
				
		}
		[Bindable (event="sceneMove")]
		protected function onSceneMove(event:GuideSceneEvent):void
		{
			var confirm:MoveScenePanel = new MoveScenePanelMX();
			confirm.guideScene = event.guideScene;
			confirm.addEventListener(CloseablePanel.CLOSE, onSectionSelectorClose);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		
		
		private function onSectionSelectorClose(event:Event):void
		{
			var panel:MoveScenePanel = MoveScenePanel(event.target);
			panel.removeEventListener(CloseablePanel.CLOSE, onSectionSelectorClose);

			if(panel.guideSection.id != section.id)
			{
				section.removeGuideScene(panel.guideScene);
				
				var index:int = scenes.getItemIndex(panel.guideScene);
				if(index > -1)
					scenes.removeItemAt(index);
				
				panel.guideSection.addGuideScene(
					GuideSceneData.clone(new Uuid().toString(), panel.guideSection, panel.guideScene)
				);
			}
			
			PopUpManager.removePopUp(panel);
		}
		
		[Bindable (event="sceneDelete")]
		protected function onSceneDelete(event:GuideSceneEvent):void
		{
			trace("onSceneDelete()");
			var guideSection:GuideSectionData = Model.guideData.getGuideSection(event.guideScene.sectionId);
			guideSection.removeGuideScene(event.guideScene);
			
			var index:int = scenes.getItemIndex(event.guideScene);
			if(index > -1)
				scenes.removeItemAt(index);
		}
		
		[Bindable (event="sectionDelete")]
		protected function onSectionChildDelete(event:GuideSectionEvent):void
		{
			trace("SectionChildDelete()");
			Model.guideData.removeGuideSection(event.guideSection);
			section.removeGuideSection(event.guideSection);
			var index:int = sections.getItemIndex(event.guideSection);
			if(index > -1)
				sections.removeItemAt(index);
		}
		
		/*protected function onSceneDrop(event:SceneDropEvent):void
		{
			var sceneIndex:int = scenes.getItemIndex(event.guideScene);
			if(sceneIndex)
				scenes.removeItemAt(sceneIndex);
			
			var guideSection:GuideSectionData = Model.guideData.getGuideSection(event.guideScene.sectionId);
			guideSection.removeGuideScene(event.guideScene);
			
			var clone:GuideSceneData = GuideSceneData.clone(new Uuid().toString(), event.guideSection, event.guideScene);
			
			// Hack to fix hibernate error.
			for(var i:int; i<clone.hotspots.length; i++)
				GuideSceneHotspot(clone.hotspots[i]).id = new Uuid().toString();
			
			event.guideSection.addGuideScene(clone);
		}*/
		
		protected function onScenesListDragEnter(event:DragEvent):void
		{
			if (event.dragInitiator is ScenesList)
				DragManager.acceptDragDrop(this);
		}
		
		protected function onScenesListDragComplete(event:DragEvent):void
		{
			if (event.dragInitiator is ScenesList)
				for(var i:int; i<scenes.length; i++)
					GuideSceneData(scenes[i]).order = i;
			
			section.reorderGuideScenes();
		}
		
		protected function onSectionsListDragComplete(event:DragEvent):void
		{
			
			if (event.dragInitiator is SectionsList)
				for(var i:int; i<sections.length; i++)
					GuideSectionData(sections[i]).order = i;
			
			section.reorderGuideSections();
			
			
		}
		
		
		protected function onSectionsListDragEnter(event:DragEvent):void
		{
			if (event.dragInitiator is SectionsList)
				DragManager.acceptDragDrop(this);
		}
		
		
		
		protected function onAddSceneClick(event:MouseEvent):void
		{
			var center:Geo = Model.guideScene 
				? Model.guideScene.scene.geo 
				: new Geo(-41.2961, 174.7944);
			var b:SceneBrowser = new SceneBrowserMX;
			b.addEventListener(LocationBrowser.LOCATION_SELECTED, onLocationSelected);
			b.addEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			b.addEventListener(CloseablePanel.CLOSE, onPanelClose);
			b.mapCenter = new GeoZoom(center.lat, center.lon, 13);
			PopUpManager.addPopUp(b, Admin.instance, true);
			PopUpManager.centerPopUp(b);
			
		} 
		
		protected function onAddSectionClick(event:MouseEvent):void
		{
			var sectionChild:GuideSectionData = GuideSectionData.from(
				new Uuid().toString(), Model.guideData.id, "NEW SECTION", section.id 
			)
				
			sectionChild.order = section.order;
			Model.guideData.addGuideSection(sectionChild);
			section.addSectionChild(sectionChild);
			sections.addItem(sectionChild);
			
		} 
		
		private function onLocationSelected(event:LocationDataEvent):void
		{
			var guideScene:GuideSceneData = GuideSceneData.fromLocation(
				new Uuid().toString(), section, event.location, scenes.length
			)
			section.addGuideScene(guideScene);
			scenes.addItem(guideScene);
			
			// No scene was loaded yet.
			if(Model.state.loadingScene == null)
				Model.state.loadingScene = guideScene.scene;
		}
		
		private function onSceneSelected(event:SceneBasicEvent):void
		{
			var guideScene:GuideSceneData = GuideSceneData.from(
				new Uuid().toString(), section, event.scene, 999
			)
			section.addGuideScene(guideScene);
			scenes.addItem(guideScene);
			
			// No scene was loaded yet.
			if(Model.state.loadingScene == null)
				Model.state.loadingScene = event.scene;
		}
		
		private function onPanelClose(event:Event):void
		{
			removePanel(SceneBrowser(event.target));
		}
		
		protected function toggle(value:String):void
		{
			if (value == "scenes" && section.markers == 0)
			{
				isScenesAdmin = true;
				isSectionsAdmin = false
			}
			
			else if (value == "sections" && section.markers == 0)
			{
				isScenesAdmin = false;
				isSectionsAdmin = true
			}
		}
		
		private function removePanel(browser:SceneBrowser):void
		{
			browser.removeEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onPanelClose);
			PopUpManager.removePopUp(browser);
		}
		
		private var cacheWhenLoadComplete:Boolean;
		
		protected function browseForImage():void
		{
			
			var uploader:FileUploader = new FileUploaderMX();
			uploader.addEventListener(FileUploader.UPLOAD_COMPLETE, onUploaderComplete);
			uploader.addEventListener(CloseablePanel.CLOSE, onVideoSelectorClose);
			uploader.browse("1", "image files", "*.jpg;*.jpeg;*.png;*.gif;*.JPG;*.JPEG;*.PNG;*.GIF");
			PopUpManager.addPopUp(uploader, Admin.instance, true);
			PopUpManager.centerPopUp(uploader);
		}
		
		protected function getMap(value:String = null):void
		{
		  trace("SectionAdmin.getMap("+ value +")");

		  if(!lat){
			  if(section.firstScene){
				  lat = section.firstScene.scene.geo.lat;
			   	  lon = section.firstScene.scene.geo.lon;
			  }
			  else 
			  {
				  lat = Model.currentScene.geo.lat;
				  lon = Model.currentScene.geo.lon;
			  } 
			  
			  if(lat == 0){
				  lat = Model.currentScene.geo.lat;
				  lon = Model.currentScene.geo.lon;
			  }
				  
		  }
  
		  if(value == "up") lat = lat + 0.01/zoom;
		  if(value == "down")lat = lat - 0.01/zoom;
		  if(value == "left")lon = lon - 0.01/zoom;
		  if(value == "right")lon = lon + 0.01/zoom;
		  if(value == "in")zoom = zoom  + 1;
		  if(value == "out")zoom  = zoom  - 1;
		  
		   var path:String = "http://api.tiles.mapbox.com/v3/bknill.hp29237j/"
				 + lon + "," + lat + ","+ zoom + "/400x400.png"

		   ImageLoader.load(path, MapLoadCallback, MapLoadCallbackError);	
		}
		
		private function MapLoadCallback(data:BitmapData):void
		{
			trace("SectionAdmin.MapLoadCallback()");
			section.newPageImage = data;
			section.page_image = null;
			isMap = true;
		}
		
		private function MapLoadCallbackError():void
		{
			trace("imagePageLoadCallbackError");
		}
		
		
		private function onUploaderComplete(event:Event):void
		{
			var uploader:FileUploader = FileUploaderMX(event.target);
			section.page_image = uploader.file.realName;
			hasImage = true;
			isMap = false;
			cacheWhenLoadComplete = true;
			//pageImageToLoad = uploader.file.realName;
			PopUpManager.removePopUp(uploader);
		}
		
		private function set pageImageToLoad(value:String):void
		{
			ImageLoader.load(
				Model.config.getAssetUrl(value), 
				onPageImageLoadCallback,
				onPageImageLoadCallbackError
			);
		}
		
		private function onPageImageLoadCallbackError():void
		{
			trace("onCoverImageLoadCallbackError");
		}
		
		private function onPageImageLoadCallback(data:BitmapData):void
		{
			pageImageBitmap = data;

		}
		protected function clearPageImage():void
		{
			section.page_image = null;
			hasImage = false;
		}
		
		protected function clearMap():void
		{
			section.page_image = null;
			section.newPageImage = null;
			isMap = false;
		}
		
/*		protected function setMapMarkers():void
		{
			for(var i:int; i<section.guideScenes.length; i++){
				var lat:Number = section.guideScenes[i].scene.geo.lat;
				var lon:Number = section.guideScenes[i].scene.geo.lon;
				var xy:Point = getXY(lat,lon);
				
				section.guideScenes[i].pagePositionX = xy.x;
				section.guideScenes[i].pagePositionY = xy.y;
			}
			
			section.markerPositionChanged();
		
		}*/
		
		public function getXY(lat:Number, lng:Number):Point
		{
			var mapHeight:Number = 400;
			var mapWidth:Number = 400;

			var screenX:Number = ((lng + 180) * (mapHeight  / 360));
			var screenY:Number = (((lat * -1) + 90) * (mapWidth/ 180));
			
			return new Point(screenX,screenY);
		}
		
		private function onVideoSelectorClose(event:Event):void
		{
			var uploader:FileUploader = FileUploaderMX(event.target);
			PopUpManager.removePopUp(uploader);
		}
		
		
		protected function popupDeleteSection():void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove this section?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			Model.guideData.removeGuideSection(section);
			Model.dispatchEvent(new Event(Model.SECTION_CHANGE));
			this.visible = false;
			//visible = false;
		}
		
		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
		
	}
}