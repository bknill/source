package co.beek.admin.poup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Panel;
	
	import co.beek.admin.component.CloseButtonCl;
	
	public class CloseablePanel extends Panel
	{
		public static const CLOSE:String = "CLOSE";
		
		private var closeButton:CloseButtonCl = new CloseButtonCl;
		
		public function CloseablePanel()
		{
			super();
			closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			//titleBar.addEventListener(MouseEvent.MOUSE_DOWN,handleDown);
			//titleBar.addEventListener(MouseEvent.MOUSE_UP,handleUp);
			titleBar.addChild(closeButton);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			closeButton.width = 16;
			closeButton.height = 16;
			closeButton.x = titleBar.width - closeButton.width - 8;
			closeButton.y = 8;
		}
		/*
		private var downPos:Point;
		private function handleDown(e:MouseEvent):void{
			//this.startDrag();
			downPos = new Point(mouseX, mouseY);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(event:Event):void
		{
			x = stage.mouseX - downPos.x;
			y = stage.mouseY - downPos.y;
			
			var appW:Number = FlexGlobals.topLevelApplication.width;
			var appH:Number = FlexGlobals.topLevelApplication.height;
			if(this.x+this.width>appW)
			{
				this.x=appW-this.width;
			}
			if(this.x<0)
			{
				this.x=0;
			}
			if(this.y+this.height>appH)
			{
				this.y=appH-this.height;
			}
			if(this.y<0)
			{
				this.y=0;
			}
		}
		
		private function handleUp(e:MouseEvent):void{
			//this.stopDrag();
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		*/
		protected function onCloseButtonClick(event:MouseEvent):void
		{
			dispatchCloseEvent();
		}
		
		protected function dispatchCloseEvent():void
		{
			dispatchEvent(new Event(CloseablePanel.CLOSE))
		}
	}
}