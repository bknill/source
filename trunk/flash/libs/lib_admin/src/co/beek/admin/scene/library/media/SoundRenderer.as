package co.beek.admin.scene.library.media
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.SoundData;
	
	import com.laiyonghao.Uuid;

	public class SoundRenderer extends DraggableHotspot
	{
		public function SoundRenderer()
		{
			super(SoundRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return Model.guideScene != null 
				? SoundData.createForGuide(id, Model.guideScene.id)
				: SoundData.createForScene(id, Model.currentScene.id);
		}
	}
}