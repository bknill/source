package co.beek.admin.scene.library.hotspots
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.map.Geo;
	import co.beek.Log;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	
	import com.laiyonghao.Uuid;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.core.IDataRenderer;
	
	public class BubbleRenderer extends DraggableHotspot implements IDataRenderer
	{
		[Bindable]
		protected var meters:Number;
		
		private var _bubbleData:BubbleData;
		
		public function BubbleRenderer()
		{ 	
			super(LibraryBubbleRendererFl);
		}
		
		public override function set data(value:Object):void
		{
			//Log.record("BubbleRenderer.setData("+value+")");
			if(!value)
				return;
			
			sceneData = SceneBasicData(value);
			pos = null;
		}
		
		public function set sceneData(value:SceneBasicData):void
		{
			//Log.record("BubbleRenderer.setsceneData("+value+")");
			//setBubbleIcon(graphic, value.type);
			//setBubbleIcon(proxy, value.type);
			
			var id:String = new Uuid().toString();
			var sceneId:String = Model.currentScene.id;
			_bubbleData = BubbleData.create(id, sceneId, value);
			
			var geo:Geo = Model.currentScene.geo;
			meters = Math.round(value.geo.distance(geo)*1000);
			
			dispatchEvent(new Event("change"));
		}
		
		private function get graphic():LibraryBubbleRendererFl
		{
			return LibraryBubbleRendererFl(_graphic);
		}
		
		private function get proxy():LibraryBubbleRendererFl
		{
			return LibraryBubbleRendererFl(_proxy);
		}
		
		public function setBubbleIcon(clip:LibraryBubbleRendererFl, value:int):void
		{
			var icons:Dictionary = Constants.getIconsDictionary(clip.icons);
			
			for(var i:String in icons)
				if(icons[i])
					MovieClip(icons[i]).visible = false;
			
			if(icons[value])
				MovieClip(icons[value]).visible = true;
		}
		
		[Bindable (event="change")]
		public function get bubbleData():BubbleData
		{
			return _bubbleData
		}
		
		public override function get hotspotData():HotspotData
		{
			return _bubbleData;
		}
	}
}