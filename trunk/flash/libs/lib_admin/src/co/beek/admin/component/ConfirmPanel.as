package co.beek.admin.component
{
	import flash.events.Event;
	
	import mx.containers.Panel;
	
	
	public class ConfirmPanel extends Panel
	{
		public static const CONFIRMED_NO:String = "CONFIRMED_NO";
		
		public static const CONFIRMED_YES:String = "CONFIRMED_YES";
		
		[Bindable]
		public var confirmText:String;
		
		public function ConfirmPanel()
		{
			super();
		}
		
		protected function yesClicked():void
		{
			dispatchEvent(new Event(CONFIRMED_YES));
		}
		
		protected function noClicked():void
		{
			dispatchEvent(new Event(CONFIRMED_NO));
		}
	}
}