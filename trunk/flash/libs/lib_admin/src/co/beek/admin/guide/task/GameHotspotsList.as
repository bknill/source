package co.beek.admin.guide.task
{
	import spark.components.List;
	
	[Event(name="removeHotspot", type="co.beek.event.GameHotspotEvent")]
	public class GameHotspotsList extends List
	{
		public function GameHotspotsList()
		{
			super();
		}
	}
}