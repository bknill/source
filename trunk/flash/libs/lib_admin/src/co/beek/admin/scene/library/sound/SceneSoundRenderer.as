package co.beek.admin.scene.library.sound
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.core.IDataRenderer;
	
	import co.beek.model.Model;
	import co.beek.model.data.PanoLook;
	import co.beek.model.hotspots.SoundData;
	
	
	public class SceneSoundRenderer extends Canvas implements IDataRenderer
	{
		[Bindable]
		protected var soundData:SoundData;
		
		[Bindable]
		protected var showSpinner:Boolean;
		
		private var sound:Sound = new Sound();
		
		private var soundChannel:SoundChannel;
		
		public function SceneSoundRenderer()
		{
			super();
			addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		override public function set data(value:Object):void
		{
			super.data = value;
			soundData = value as SoundData;
		}
		
		protected function onMouseClick(event:MouseEvent):void 
		{
			// show the sound in the scene
			//Model.state.pan = soundData.panoPos.pan;
			/*/Model.state.pan = soundData.panoPos.pan;
				
				
				= new PanoLook(
				soundData.panoPos.pan, 
				soundData.panoPos.tilt, 
				Model.state.panoLook.fov
			);*/
			Model.hotspot = soundData;
		}
		
		protected function onPlayButtonClick(event:MouseEvent):void
		{
			event.stopPropagation();
			
			var url:String = Model.config.assetCdn+"/"+soundData.file.realName;
			
			if(soundChannel)
				soundChannel.stop();
			
			var sound:Sound = new Sound();
			sound.addEventListener(ProgressEvent.PROGRESS, onProgressEvent);
			sound.load(new URLRequest(url));
			
			Model.state.mute = true;
			showSpinner = true;
		}
		
		private function onProgressEvent(event:ProgressEvent): void
		{
			var sound:Sound = Sound(event.target);
			trace("onProgressEvent:"+sound.isBuffering);
			if(!sound.isBuffering)
			{
				// bufferning is done, play for 3 seconds
				setTimeout(stopSound, 3000);
				sound.removeEventListener(ProgressEvent.PROGRESS, onProgressEvent);
				
				soundChannel = sound.play();
				soundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
		private function stopSound():void
		{
			soundChannel.stop();
			Model.state.mute = false;
			showSpinner = false;
		}
		
		private function onSoundComplete(event:Event):void
		{
			stopSound();
		}
	}
}