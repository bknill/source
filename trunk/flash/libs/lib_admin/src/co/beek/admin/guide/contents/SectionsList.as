package co.beek.admin.guide.contents
{
	import spark.components.List;
	
	[Event(name="sectionDelete", type="co.beek.event.GuideSectionEvent")]
	public class SectionsList extends List
	{
		public function SectionsList()
		{
			super();
		}
	}
}