package co.beek.admin.scene.hotspot.video
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.service.data.FileUploader;
	import co.beek.utils.JSONBeek;
	
	import flash.display.BitmapData;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.controls.VideoDisplay;
	import mx.graphics.codec.JPEGEncoder;
	
	public class PreviewGenerator extends CloseablePanel
	{
		public static const PREVIEW_UPLOADED:String = "PREVIEW_UPLOADED";
		
		private var encoder:JPEGEncoder = new JPEGEncoder(90);
		
		private var _videoData:VideoData;
		
		[Bindable]
		protected var stageHeight:Number;

		[Bindable]
		protected var stageWidth:Number;
		
		public function PreviewGenerator()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			stageHeight = stage.stageHeight-100;
			stageWidth = stage.stageWidth-200;
		}
		
		public function set videoData(value:VideoData):void
		{
			_videoData = value;
		}
		
		[Bindable (event="hostspot_change")]
		public function get videoData():VideoData
		{
			return _videoData;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get videoUrl():String
		{	
			return Model.config.getAssetUrl(_videoData.file.realName) 
		}
		
		protected function savePreview(display:VideoDisplay):void
		{
			var bmpData:BitmapData = new BitmapData(display.width, display.height);
			bmpData.draw(display);
			
			var teamId:String = Model.guideScene && Model.guideScene.hasHotspot(_videoData) 
				? Model.guideData.teamId
				: Model.currentScene.location.teamId;
			
			var uploader:FileUploader = new FileUploader();
			uploader.addEventListener(Event.COMPLETE, onUploaderComplete);
			uploader.upload( "preview.jpg", encoder.encode(bmpData));
		}
		
		protected function onUploaderComplete(event:Event):void 
		{
			trace("onUploaderComplete()");
			var uploader:FileUploader = FileUploader(event.target);
			uploader.removeEventListener(Event.COMPLETE, onUploaderComplete);
			
			// set the returned file on the photo
			if(!_videoData)
				return;
			
			_videoData.preview = uploader.fileData.realName;
			dispatchEvent(new Event(PREVIEW_UPLOADED))
		}
	}
}