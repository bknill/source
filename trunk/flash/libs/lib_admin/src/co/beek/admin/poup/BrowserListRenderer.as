package co.beek.admin.poup
{
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	
	import mx.containers.Canvas;
	
	import spark.components.supportClasses.ItemRenderer;
	
	public class BrowserListRenderer extends ItemRenderer
	{
		private var _location:LocationData;
		
		public function BrowserListRenderer()
		{
			super();
		}
		
		//public override function set data(value:Object)
		
		[Bindable (event="dataChange")]
		public function get title():String
		{
			if(data is LocationData)
				return LocationData(data).title;

			if(data is SceneBasicData)
				return SceneBasicData(data).title;
			
			return "";
		}
	}
}