package co.beek.admin.guide.task
{
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GameHotspotEvent;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GameTaskHotspot;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PostData;
	import co.beek.model.hotspots.SoundData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	
	import spark.core.SpriteVisualElement;
	
	public class GameTaskAdmin extends VBox
	{
		public var iconHolder:SpriteVisualElement;
		private var icons:AdminTypeIconsFl = new AdminTypeIconsFl;
		
		[Bindable]
		protected var _gameTask:GameTaskData;
		
		
		/**
		 * A collection of HotspotData representing the hotspots in the scene 
		 * to be found, that are not in the hotspotsToFind array from the task
		 */
		[Bindable]
		protected var sceneHotspotsToFind:ArrayCollection = new ArrayCollection();
		
		/**
		 * A collection of GameTaskHotspot representing the hotspots to find from this task
		 */
		[Bindable]
		protected var taskHotspotsToFind:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		protected var sceneHotspotsToShow:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		protected var taskHotspotsToShow:ArrayCollection = new ArrayCollection();
		
		public function GameTaskAdmin()
		{
			super();
			Model.addEventListener(Model.GAME_TASK_CHANGED,onGameTaskChanged);
			Model.state.addEventListener(State.GAME_VIEW_CHANGE,onGameViewChange);
			Model.state.addEventListener(State.UI_STATE_CHANGE,onUIStateChange);
			
			
			
			if(Model.currentTask)
				gameTask = Model.currentTask;
		}
		
		[Bindable (event="taskChanged")]
		public function get gameTask():GameTaskData
		{
			return _gameTask;
		}
		
		public function set gameTask(value:GameTaskData):void
		{
			_gameTask = value;
			
			trace("GameTaskAdmin.set gameTask()");
			updateHotspotsToFind();
			updateHotspotsToShow();
			
			//Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
			dispatchEvent(new Event("taskChanged"))
		}
		
		[Bindable (event="taskChanged")]
		protected function get count():int
		{
			return Model.guideData.gameTasks.indexOf(_gameTask);
		}
		
		[Bindable (event="taskChanged")]
		protected function get last():Boolean
		{
			return Model.currentTask == Model.guideData.gameTasks[0];
		}
		
		[Bindable (event="taskChanged")]
		protected function get first():Boolean
		{
			return Model.currentTask == Model.guideData.gameTasks[Model.guideData.gameTasks.length-1];
		}
		
		[Bindable (event="taskChanged")]
		protected function get instructions():String
		{
			return _gameTask.instructions;
		}
		
		[Bindable (event="taskChanged")]
		protected function update():void
		{
			gameTask = Model.currentTask;
		}
		

		protected function set instructions(value:String):void
		{
			_gameTask.instructions = value;
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
		}
		
		protected function nextTask():void
		{
			Model.nextGameTask();
			update();
		}
		
		protected function prevTask():void
		{
			Model.prevGameTask();
			update();
		}
		
		protected function onRemoveHotspotToFind(event:GameHotspotEvent):void
		{
			_gameTask.removeHotspotToFind(event.gameHotspot);
			
			updateHotspotsToFind();
			updateHotspotsToShow();
		}
		
		protected function onRemoveHotspotToShow(event:GameHotspotEvent):void
		{
			_gameTask.removeHotspotToShow(event.gameHotspot);
			
			updateHotspotsToFind();
			updateHotspotsToShow();
		}
		
		private function updateHotspotsToFind():void
		{
			sceneHotspotsToFind.removeAll();
			sceneHotspotsToFind.addItem({"title" : "Select a hotspot to find"});
			
			var allHotspots:Vector.<HotspotData> = Model.hotspotsMerged;
			for(var i:int; i<allHotspots.length; i++)
			{
				if(allHotspots[i] is PostData || allHotspots[i] is SoundData)
					continue;
				
				if(!_gameTask.hasHotspotToShow(allHotspots[i])
					&& !_gameTask.hasHotspotToFind(allHotspots[i]))
					sceneHotspotsToFind.addItem(allHotspots[i]);
			}
			
			taskHotspotsToFind = new ArrayCollection(_gameTask.hotspotsToFind);
		}
		
		private function updateHotspotsToShow():void
		{
			sceneHotspotsToShow.removeAll();
			sceneHotspotsToShow.addItem({"title" : "Select a hotspot to show"});
			
			var allHotspots:Vector.<HotspotData> = Model.hotspotsMerged;
			for(var i:int; i<allHotspots.length; i++)
			{
				if(!_gameTask.hasHotspotToShow(allHotspots[i])
					&& !_gameTask.hasHotspotToFind(allHotspots[i]))
					sceneHotspotsToShow.addItem(allHotspots[i]);
			}
			
			taskHotspotsToShow = new ArrayCollection(_gameTask.hotspotsToShow);
		}
		
		protected function addHotspotToFind(combo:ComboBox):void
		{
			var hotspot:HotspotData = combo.selectedItem as HotspotData; 
			
			if(hotspot && hotspot.id && _gameTask && !_gameTask.hasHotspotToFind(hotspot))
				_gameTask.addHotspotToFind(hotspot, SceneElementData.create(Model.currentScene));
			
			updateHotspotsToFind();
			updateHotspotsToShow();
			
			// revert combo back to instructions
			combo.selectedIndex = 0;
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
		}
		
		protected function addHotspotToShow(combo:ComboBox):void
		{
			var hotspot:HotspotData = combo.selectedItem as HotspotData; 
			
			if(hotspot && hotspot.id && _gameTask && !_gameTask.hasHotspotToShow(hotspot))
				_gameTask.addHotspotToShow(hotspot, SceneElementData.create(Model.currentScene));
			
			updateHotspotsToFind();
			updateHotspotsToShow();
			
			// revert combo back to instructions
			combo.selectedIndex = 0;
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
		}
		
		[Bindable (evnt="taskChanged")]
		protected function get feedback():String
		{
			return _gameTask.feedback;
		}
		
		
		protected function set feedback(value:String):void
		{
			_gameTask.feedback = value;
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
		}
		
		
		protected function popupDeleteTask():void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove the task from the game?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			Model.guideData.removeGameTask(_gameTask);
			Model.prevGameTask();
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
			//visible = false;
		}
		
		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
		
		protected function addGameTask():void
		{
			Model.guideData.addGameTask(GameTaskData.create(
				Model.guideData.id, GameTaskData.GAME_FIND_HOTSPOTS
			), Model.currentTaskIndex + 1);
			
			Model.nextGameTask();
			update();
			Model.dispatchEvent(new Event(Model.GAME_TASK_CHANGED));
			dispatchEvent(new Event("taskChanged"));
		}
		
		protected function hideGameAdmin():void
		{
			Model.state.hideGame = true;
		}
		
		
		protected function onGameTaskChanged(e:Event):void
		{
			trace("GameTaskAdmin.onGameTaskChanged()");
			gameTask = Model.currentTask;
			dispatchEvent(new Event("taskChanged"));
		}
		
		protected function onGameViewChange(e:Event):void
		{
			trace("GameTaskAdmin.onGameViewChange()");
				gameTask = Model.currentTask;
		}
		
		protected function onUIStateChange(e:Event):void
		{
			trace("GameTaskAdmin.onUIStateChange()");
			if(Model.state.hideGame && Model.state.uiState == State.UI_STATE_NORMAL)
				Model.state.hideGame = false;	
		}
		
	}
}