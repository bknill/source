package co.beek.admin.scene.library.sound
{
	import caurina.transitions.Tweener;
	
	import co.beek.admin.BeekDragManager;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.SoundData;
	
	import com.laiyonghao.Uuid;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.treeClasses.*;
	
	
	public class SoundRepoRenderer extends TreeItemRenderer
	{
		
		private var fileData:FileData;
		
		private var sound:Sound = new Sound();
		
		private var soundChannel:SoundChannel;
		
		private var dragProxy:Image = new Image();
		
		//protected var pos:Point;
		
		private var noteIcon:Sprite = new SoundIconFl;
		private var playSpinner:Sprite = new SoundPlaySpinnerFl;
		private var playButton:Sprite = new SoundPlayButtonFl;
		
		public function SoundRepoRenderer()
		{
			super();
			
			dragProxy.source = SoundRendererFl;
			dragProxy.width = 40;
			dragProxy.height = 40;
			
			playButton.addEventListener(MouseEvent.CLICK, onPlayButtonClick);
			playButton.addEventListener(MouseEvent.MOUSE_DOWN, onPlayButtonMouseDown);
			playButton.visible = false;
			playSpinner.visible = false;
			
			
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		private function onMouseOver(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			playButton.visible = true;
		}
		
		private function onMouseDown(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onMouseMove(event:MouseEvent):void 
		{
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			
			var id:String = new Uuid().toString();
			var soundData:SoundData = Model.guideScene != null 
				? SoundData.createForGuide(id, Model.guideScene.id)
				: SoundData.createForScene(id, Model.currentScene.id);
			soundData.file = fileData;
			
			BeekDragManager.startDrag(event, soundData, dragProxy);
		}
		
		private function onMouseOut(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			//tweenToOriginal();
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			
			playButton.visible = false;
		}
		
		
		private function onMouseUp(event:MouseEvent):void 
		{
			if(TreeListData(listData).hasChildren)
				return;
			
			removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onPlayButtonMouseDown(event:MouseEvent):void
		{
			event.stopPropagation();
		}
		
		private function onPlayButtonClick(event:MouseEvent):void
		{
			var url:String = Model.config.assetCdn+"/"+fileData.realName;
			
			if(soundChannel)
				soundChannel.stop();
			
			var sound:Sound = new Sound();
			sound.addEventListener(ProgressEvent.PROGRESS, onProgressEvent);
			sound.load(new URLRequest(url));
			
			Model.state.mute = true;
			playSpinner.visible = true;
		}
		
		private function onProgressEvent(event:ProgressEvent): void
		{
			var sound:Sound = Sound(event.target);
			trace("onProgressEvent:"+sound.isBuffering);
			if(!sound.isBuffering)
			{
				// bufferning is done, play for 3 seconds
				setTimeout(stopSound, 3000);
				sound.removeEventListener(ProgressEvent.PROGRESS, onProgressEvent);
				
				soundChannel = sound.play();
				soundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
		private function stopSound():void
		{
			soundChannel.stop();
			Model.state.mute = false;
			playSpinner.visible = false;
		}
		
		private function onSoundComplete(event:Event):void
		{
			stopSound();
		}
		
		override public function set listData(value:BaseListData):void
		{
			super.listData = value;
			noteIcon.visible = TreeListData(listData).hasChildren == false;
		}
		
		override public function set data(value:Object):void
		{
			super.data = value;
			
			fileData = value as FileData;
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			super.label.width = 100;
		}
		
		override protected function childrenCreated():void{
			super.childrenCreated();
			
			addChild(noteIcon);
			
			// add spinner second so it covers play button
			addChild(playButton);
			addChild(playSpinner);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			if(super.data){
				var treeListData:TreeListData = TreeListData(super.listData);
				//previewButton.visible = treeListData.hasChildren == false;
				if(!treeListData.hasChildren)
				{
					super.label.x = 28;
					super.label.width = 150;
					
					noteIcon.x = 10;
					
					playSpinner.x = playButton.x = unscaledWidth - playButton.width-2;
					playSpinner.y = playButton.y = 1;
				}
				
			}
			
			graphics.clear();
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
			graphics.endFill();
		}
	}
}