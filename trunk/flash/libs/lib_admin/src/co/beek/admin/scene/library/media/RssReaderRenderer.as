package co.beek.admin.scene.library.media
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	
	import com.laiyonghao.Uuid;
	
	public class RssReaderRenderer extends DraggableHotspot 
	{
		public function RssReaderRenderer()
		{
			super(RssReaderRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return Model.guideScene != null 
				? RssReaderData.createForGuide(id, Model.guideScene.id)
				: RssReaderData.createForScene(id, Model.currentScene.id);
		}
	}
}