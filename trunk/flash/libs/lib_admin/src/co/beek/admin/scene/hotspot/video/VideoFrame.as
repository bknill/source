package co.beek.admin.scene.hotspot.video
{
	public class VideoFrame
	{
		public var code:int;
		public var title:String;
		
		public function VideoFrame(code:int, title:String)
		{
			this.code = code;
			this.title = title;
		}
	}
}