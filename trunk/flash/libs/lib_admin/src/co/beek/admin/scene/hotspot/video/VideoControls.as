package co.beek.admin.scene.hotspot.video
{
	import co.beek.admin.Admin;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.utils.JSONBeek;
	
	import flash.display.BitmapData;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.controls.VideoDisplay;
	import mx.managers.PopUpManager;
	
	public class VideoControls extends AbstractControlPanel
	{
		[Bindable]
		protected var videoFrames:ArrayCollection = new ArrayCollection([
			new VideoFrame(VideoData.FRAME_NONE, "No Frame"),
			new VideoFrame(VideoData.FRAME_PROJECTOR, "Projector scren"),
			new VideoFrame(VideoData.FRAME_WALL_TV, "Wall mounted Television"),
			new VideoFrame(VideoData.FRAME_TV, "Television")
		]);
		
		[Bindable] protected var videoTitle:String;
		
		public function VideoControls()
		{
			super();
		}
		
		protected override function activate():void
		{
			super.activate();
			
			dispatchEvent(new Event("frameChange"))
		}
		
		[Bindable (event="frameChange")]
		protected function get selectedFrame():VideoFrame
		{
			var selectedFrame:int = videoData.frame;
			for (var i:int = 0; i < videoFrames.length; i++)
				if(VideoFrame(videoFrames.getItemAt(i)).code == selectedFrame)
					return VideoFrame(videoFrames.getItemAt(i));
			
			return null;
		}
		
		protected function set selectedFrame(value:VideoFrame):void
		{
			videoData.frame = value.code;
			dispatchEvent(new Event("frameChange"))
		}
		
		[Bindable (event="hostspot_change")]
		protected function get videoData():VideoData
		{
			return _hotspotData as VideoData;
		}
		
		
		
		protected function set ambience(value:Number):void
		{
			if(videoData != null)
				videoData.ambience = value;
		}
		
		protected function set volume(value:Number):void
		{
			if(videoData != null)
				videoData.volume = value;
		}
		
		[Bindable (event="hostspot_change")]
		protected function get previewUrl():String
		{	
			return Model.config.getAssetUrl(videoData.preview) 
		}
		
		/**
		 * Stupid hack to get height working on image
		 */
		[Bindable]
		protected var imageHeight:Number;
		protected function imageLoadComplete(image:Image):void
		{
			imageHeight = image.width/image.contentWidth * image.contentHeight;
		}
		
		protected function launchPreviewGenerator():void
		{
			var previewGenerator:PreviewGenerator = new PreviewGeneratorMX();
			previewGenerator.videoData = videoData;
			previewGenerator.addEventListener(PreviewGenerator.PREVIEW_UPLOADED, onPreviewUploaded);
			previewGenerator.addEventListener(CloseablePanel.CLOSE, onPreviewGeneratorClose);
			
			PopUpManager.addPopUp(previewGenerator, Admin.instance, true);
			PopUpManager.centerPopUp(previewGenerator);
		}
		
		private function onPreviewUploaded(event:Event):void
		{
			dispatchEvent(new Event("hostspot_change"))
		}
		
		private function onPreviewGeneratorClose(event:Event):void
		{
			var previewGenerator:PreviewGeneratorMX = PreviewGeneratorMX(event.target);
			previewGenerator.videoData = null;
			PopUpManager.removePopUp(previewGenerator);
		}
		
		protected function clearPreview():void
		{
			videoData.preview = null;
			dispatchEvent(new Event("hostspot_change"));
		}
	}
}