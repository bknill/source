package co.beek.admin.component
{
	import mx.controls.Button;
	
	public class DeleteButton extends Button
	{
		[Embed("/../images/button_delete.png")] 
		private var skin: Class; 
		
		public function DeleteButton()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 25;
		}
	}
}