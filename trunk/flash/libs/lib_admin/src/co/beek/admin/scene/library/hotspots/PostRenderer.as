package co.beek.admin.scene.library.hotspots
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PostData;
	
	import com.laiyonghao.Uuid;

	public class PostRenderer extends DraggableHotspot
	{
		public function PostRenderer()
		{
			super(PostRendererFl);
		}
		
		public override function get hotspotData():HotspotData
		{
			var id:String = new Uuid().toString();
			return PostData.create(id, Model.currentScene.id);
		}
	}
}