package co.beek.admin.scene.library.hotspots
{
	import co.beek.admin.scene.library.DraggableHotspot;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BoardSignData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.service.LocationService;
	import co.beek.service.SceneService;
	import co.beek.service.data.SceneBasicList;
	
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;

	public class BoardRenderer extends DraggableHotspot 
	{
		private var scenes:Vector.<SceneBasicData>;
			
		public function BoardRenderer()
		{
			super(BoardRendererFl);
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			
			loadLocationScenes();
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		private function onSceneChange(event:Event):void
		{
			loadLocationScenes();
		}
		
		private function loadLocationScenes():void
		{
			if(!Model.currentScene)
				return;
				
			var locationId:String = Model.currentScene.location.id;
			LocationService.getScenesForLocation(locationId, onSceneBasicList);
		}
		
		private function onSceneBasicList(list:SceneBasicList):void
		{
			scenes = removeCurrentScene(list.scenes);
		}
		
		private function removeCurrentScene(scenes:Vector.<SceneBasicData>):Vector.<SceneBasicData>
		{
			var sceneId:String = Model.currentScene.id;
			for(var i:int=0; i<scenes.length; i++)
			{
				var randomScene:SceneBasicData = scenes[i];
				if(scenes[i].id == sceneId)
					scenes.splice(i, 1);
			}
			
			return scenes;
		}
		
		public override function get hotspotData():HotspotData
		{
			//var sceneId:String = Model.state.loadingScene.id;
			var sceneId:String = Model.currentScene.id;
			var data:BoardData = BoardData.create(sceneId);
			
			for(var i:int=0; i<4 && scenes.length > 0; i++)
			{
				var randomIndex:int = Math.random() * scenes.length;
				var randomScene:SceneBasicData = scenes[randomIndex];
				scenes.splice(randomIndex, 1);
				
				var sign:BoardSignData = BoardSignData.create(
					new Uuid().toString(),
					data.id, 
					randomScene
				);
				data.addSign(sign);
			}
			
			return data;
		}
	}
}