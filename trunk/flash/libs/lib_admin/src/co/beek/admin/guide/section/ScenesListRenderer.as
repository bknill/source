package co.beek.admin.guide.section
{
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GuideSceneEvent;
	import co.beek.model.guide.GuideSceneData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.Font;
	
	import mx.managers.PopUpManager;
	
	import spark.components.supportClasses.ItemRenderer;
	
	[Event(name="sceneDelete", type="co.beek.event.GuideSceneEvent")]
	[Event(name="sceneMove", type="co.beek.event.GuideSceneEvent")]
	public class ScenesListRenderer extends ItemRenderer
	{
		/**
		 * For some reason, the native selected value is not bindable
		 */
		[Bindable]
		protected var isSelected:Boolean;
		
		
		[Bindable]
		protected var bodyFont:String = Fonts.bodyFont;
		
		public function ScenesListRenderer()
		{
			super();
		}
		
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			isSelected = value;
		}
		
		public override function set data(value:Object):void
		{
			super.data = value;
			
		}
		
		[Bindable (event="dataChange")]
		public function get guideSceneData():GuideSceneData
		{
			return data as GuideSceneData;
		}
		
		protected function onMoveButtonClick():void
		{
			dispatchEvent(new GuideSceneEvent("sceneMove", guideSceneData));
		}
		
		protected function onDeleteButtonClick(event:MouseEvent):void
		{
			trace("onDeleteButtonClick()");
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove the scene from the guide?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			trace("onConfirmYes()");
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			dispatchEvent(new GuideSceneEvent("sceneDelete", guideSceneData));
		}
		
		private function onConfirmNo(event:Event):void
		{
			trace("onConfirmNo()");
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
	}
}