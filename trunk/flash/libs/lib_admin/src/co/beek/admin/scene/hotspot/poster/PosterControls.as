package co.beek.admin.scene.hotspot.poster
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.RichTextEditor;
	import mx.managers.PopUpManager;
	
	import co.beek.Fonts;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.hotspots.PosterData;
	
	public class PosterControls extends AbstractControlPanel
	{
		[Bindable]
		public var frames:ArrayCollection = new ArrayCollection([
			{"id" : PosterData.FRAME_NONE, "title": "No Frame"},
			{"id" : PosterData.FRAME_BLACK_BOX, "title": "Frame"}
		]);
		
		[Bindable]
		public var selectedFrameIndex:int;
		
		[Bindable]
		public var buttonSceneName:String;
		
		[Bindable]
		protected var posterHtmlText:String;
		
		public function PosterControls()
		{
			super();
		}
		
		protected function compactifyToolbar(rte:RichTextEditor):void {
			rte.toolbar.removeChild(rte['fontFamilyCombo']);
			//rte.toolbar.removeChild(rte['colorPicker']);
			rte.toolBar2.removeChild(rte['italicButton']);
			rte.toolBar2.removeChild(rte['underlineButton']);
			rte.toolbar.removeChild(rte['_RichTextEditor_VRule2']);
			rte.textArea.setStyle("fontSize", 28);
			//rte.toolbar.removeChild(rte['linkTextInput']);
		}
		
		protected override function activate():void
		{
			super.activate();
			posterHtmlText = Fonts.cleanHTML(posterData.text);
			selectFrame();
		}
		
		/**
		 * Updates the posterData, but does not trigger a rebuild of the textarea;
		 */
		protected function setPosterHtmlText(value:String):void
		{
			//posterHtmlText = value;
			posterData.text = value;
		}
		
		private function selectFrame():void
		{
			for(var i:int; i<frames.length; i++)
			{
				if(frames[i].id == posterData.frame)
					selectedFrameIndex = frames.length;
			}
		}
		
		[Bindable (event="hostspot_change")]
		public function get noButton():Boolean
		{
			
			return !posterData.buttonFavourite && !posterData.buttonScene && !posterData.buttonUrl
		}
		
		public function clearButton():void
		{
			posterData.buttonScene = null;		
			posterData.buttonUrl = null;
			posterData.buttonFavourite = false;
		}
		
		
		protected override function deactivate():void
		{
			super.deactivate();
			//posterData.text = posterHtmlText;
		}
		
		private function onSceneDataLoaded(scene:SceneData):void
		{
			buttonSceneName = scene.title;
		}
		
		public function browseForScene(event:MouseEvent):void
		{
			var b:SceneBrowser = new SceneBrowserMX;
			b.addEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			b.addEventListener(CloseablePanel.CLOSE, onPanelClose);
			b.mapCenter = GeoZoom.from(Model.currentScene.geo, 15);
			PopUpManager.addPopUp(b, DisplayObject(event.target), true);
			PopUpManager.centerPopUp(b);
		} 
		
		private function onSceneSelected(event:SceneBasicEvent):void
		{
			posterData.buttonSceneId = event.scene.id;
			buttonSceneName = event.scene.title;
			removePanel(SceneBrowser(event.target));
		}
		
		private function onPanelClose(event:Event):void
		{
			removePanel(SceneBrowser(event.target));
		}
		
		private function removePanel(browser:SceneBrowser):void
		{
			browser.removeEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onPanelClose);
			PopUpManager.removePopUp(browser);
		}
		
		
		[Bindable (event="hostspot_change")]
		public function get posterData():PosterData
		{
			return _hotspotData as PosterData;
		}

		[Bindable (event="hostspot_change")]
		public function get hasBackground():Boolean
		{
			return posterData.frame != PosterData.FRAME_NONE;
		}

		public function set hasBackground(value:Boolean):void
		{
			posterData.frame = value ? PosterData.FRAME_BLUE_BOX : PosterData.FRAME_NONE;
		}
		
		public function set buttonUrl(value:String):void
		{
			posterData.buttonUrl = value;
			buttonSceneName = "Browse for scene";
		}
	}
}