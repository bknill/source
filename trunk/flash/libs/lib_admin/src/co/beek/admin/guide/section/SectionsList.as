package co.beek.admin.guide.section
{
	import spark.components.List;

	[Event(name="sectionChildDelete", type="co.beek.event.GuideSectionEvent")]
	[Event(name="sectionChildMove", type="co.beek.event.GuideSectionEvent")]
	public class SectionsList extends List
	{
		public function SectionsList()
		{
			super();
		}
	}
}