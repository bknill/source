package co.beek.admin.scene
{
	import co.beek.Log;
	import co.beek.admin.Admin;
	import co.beek.admin.BeekDragManager;
	import co.beek.admin.scene.hotspot.board.BoardControls;
	import co.beek.admin.scene.hotspot.bubble.BubbleControls;
	import co.beek.admin.scene.hotspot.photo.PhotoControls;
	import co.beek.admin.scene.hotspot.post.PostControls;
	import co.beek.admin.scene.hotspot.poster.PosterControls;
	import co.beek.admin.scene.hotspot.rssreader.RssReaderControls;
	import co.beek.admin.scene.hotspot.sound.SoundControls;
	import co.beek.admin.scene.hotspot.video.VideoControls;
	import co.beek.admin.scene.library.PanoDropTarget;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PostData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.containers.Panel;
	import mx.controls.Button;
	
	
	
	
	public class SceneAdmin extends Canvas
	{
		public var panoDropTarget:PanoDropTarget;
		public var libraryPanel:Panel;
		public var editScene:Button;
		
		public var photoControls:PhotoControls;
		public var postControls:PostControls;
		public var bubbleControls:BubbleControls;
		public var boardControls:BoardControls;
		public var soundControls:SoundControls;
		public var videoControls:VideoControls;
		public var posterControls:PosterControls;
		public var rssReaderControls:RssReaderControls;
		
		[Bindable]
		public var isEditMode:Boolean;
		
		public function SceneAdmin()
		{
			super();
			BeekDragManager.addEventListener(BeekDragManager.DRAG_STARTED, onDragStarted);
			BeekDragManager.addEventListener(BeekDragManager.DRAG_CANCELLED, onDragCanceled);
			BeekDragManager.addEventListener(BeekDragManager.DRAG_DROPPED, onDragDropped);
			
			Admin.instance.addEventListener(Admin.PLAYER_LOADED, onPlayerLoaded);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditSceneChange);
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			
			setTimeout(showAfterInPosition, 1000);
		}
		
		private function showAfterInPosition():void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = false;
		}
		
		protected function onEditSceneChange(event:Event):void
		{
			if(Model.state.uiState == State.UI_STATE_EDITING_SCENE)
			{
				if(Model.hotspot)
					configureHotspot = Model.hotspot;
				
				//else if (Model.state.guideView == State.GUIDE_VIEW_CLOSED)
					showLibraryPanel();
				
				isEditMode = true;
			}
			else
			{
				panoDropTarget.visible = panoDropTarget.includeInLayout = false;
				configureHotspot = null;
				Model.hotspot = null;
				if(Model.state.uiState != State.UI_STATE_EDITING_VO)
					hideLibraryPanel();
				
				isEditMode = false;
			}
		}
		
		protected function onPanelDragExit(event:Event):void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = true;
		}
		
		protected function onPanelDragEnter(event:Event):void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = false;
		}

		
		private function onDragStarted(event:Event):void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = true;
		}
		
		private function onDragCanceled(event:Event):void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = false;
		}
		
		private function onDragDropped(event:Event):void
		{
			Log.record("SceneAdmin.onDragDropped()");
			panoDropTarget.visible = panoDropTarget.includeInLayout = false;
		}
		
		private function onSceneChange(event:Event):void
		{
			addSceneListeners();
		}
		
		private function onPlayerLoaded(event:Event):void
		{
			panoDropTarget.visible = panoDropTarget.includeInLayout = false;
			
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE, onGuideViewChange);
			
			Admin.instance.player.pano.addEventListener(HotspotEvent.HOTSPOT_CLICKED, onHotspotClick);
			
			if(Model.currentScene)
				addSceneListeners();
		}
		
		private function addSceneListeners():void
		{
			Model.currentScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			Model.currentScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);
			if(Model.guideScene)
			{
				Model.guideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
				Model.guideScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onHotspotRemoved);			
			}
		}
		
		private function onHotspotAdded(event:HotspotEvent):void
		{
			configureHotspot = event.hotspot;
		}

		private function onHotspotRemoved(event:HotspotEvent):void
		{
			configureHotspot = null;
		}
		
		public function showLibraryPanel(milliseconds:int = 0):void
		{
			libraryPanel.visible = true;
			libraryPanel.includeInLayout = true;
			libraryPanel.right = 0;
		}
		
		private function hideLibraryPanel():void
		{
			slideAwayPanel();
			setTimeout(hideAfterTransition, 500);
		}
		
		private function closeLibrary(e:Event):void
		{hideLibraryPanel()}
		
		private function openLibrary(e:Event):void
		{showLibraryPanel()}
		
		private function slideAwayPanel():void
		{
			libraryPanel.right = -libraryPanel.width;
		}
		
		private function hideAfterTransition():void
		{
			libraryPanel.visible = false;
			libraryPanel.includeInLayout = false;
		}
		
		
		private function onHotspotClick(event:HotspotEvent):void
		{
			configureHotspot = event.hotspot;
		}

		
		protected function closeHotspotConfig():void
		{
			configureHotspot = null;
		}
		
		private var _configureHotspot:HotspotData;
		
		
		private function set configureHotspot(hotspot:HotspotData):void
		{
			if(_configureHotspot == hotspot)
				return;
			
			_configureHotspot = hotspot;
			
			photoControls.hotspotData = hotspot as PhotoData;
			postControls.hotspotData = hotspot as PostData;
			bubbleControls.hotspotData = hotspot as BubbleData;
			soundControls.hotspotData = hotspot as SoundData;
			boardControls.hotspotData = hotspot as BoardData;
			videoControls.hotspotData = hotspot as VideoData;
			posterControls.hotspotData = hotspot as PosterData;
			rssReaderControls.hotspotData = hotspot as RssReaderData;
			
			if(hotspot == null && Model.state.uiState == State.UI_STATE_EDITING_SCENE)
				showLibraryPanel();
				
			else
				hideLibraryPanel();
		}
		
		private function onGuideViewChange(event:Event):void
		{
			configureHotspot = null;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if(libraryPanel)
			{
				libraryPanel.height = unscaledHeight;
				libraryPanel.invalidateSize();
			}
		}
	}
}