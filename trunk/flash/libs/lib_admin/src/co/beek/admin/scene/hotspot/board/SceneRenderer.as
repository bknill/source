package co.beek.admin.scene.hotspot.board
{
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BoardSignData;
	
	import com.laiyonghao.Uuid;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.events.ListEvent;
	
	public class SceneRenderer extends Canvas
	{
		private var _sceneData:SceneBasicData;

		private var _boardData:BoardData;
		
		[Bindable]
		public var directions:ArrayCollection = new ArrayCollection([
			{"label": "right", "direction":BoardSignData.RIGHT},
			{"label": "left", "direction":BoardSignData.LEFT},
			{"label": "up", "direction":BoardSignData.UP},
			{"label": "down", "direction":BoardSignData.DOWN}
		]);
		
		public function SceneRenderer()
		{
			super();
		}
		
	
		public override function set data(value:Object):void
		{
			super.data = value;
			
			if(_sceneData == value)
				return;
			
			_sceneData = SceneRendererData(value).scene;
			_boardData = SceneRendererData(value).board;
			_boardData.addEventListener(BoardData.SCENES_CHANGED, onScenesChanged);
			
			dispatchEvent(new Event("change"));
		}
		
		[Bindable (event="change")]
		public function get sceneData():SceneBasicData
		{
			return _sceneData;
		}
		
		private function onScenesChanged(event:Event):void
		{
			// trigger the flex binding
			dispatchEvent(new Event("change"));
		}
		
		/**
		 * The data for this scene on the signboard.
		 */
		[Bindable (event="change")]
		public function get boardSignData():BoardSignData
		{
			for(var i:int=0; i<_boardData.boardSigns.length; i++)
				if(_boardData.boardSigns[i].scene.id == _sceneData.id)
					return _boardData.boardSigns[i];
			
			return null;
		}

		[Bindable (event="change")]
		protected function get directionIndex():int
		{
			var boardSignData:BoardSignData = boardSignData;
			for(var i:int=0; i<directions.length; i++)
				if(directions[i].direction == boardSignData.direction)
					return i;
			
			return 0;
		}
		
		/**
		 * Adds this scene to the board as a boardsign.
		 * Triggers the SCENES_CHANGED event in all the BoardData.
		 * Triggers the change event BoardSceneRenderer.
		 * Updates the checkbox in all the renderers.
		 **/
		protected function set selected(value:Boolean):void
		{
			if(value)
				_boardData.addSign(BoardSignData.create(
					new Uuid().toString(),
					_boardData.id, 
					sceneData
				));
			
			else
				_boardData.removeSign(boardSignData);
		}
		
		protected function onDirectionComboChange(event:ListEvent):void
		{
			boardSignData.direction = ComboBox(event.target).selectedItem.direction;
		}
	}
}