package co.beek.admin.guide.cover
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.managers.PopUpManager;
	
	import spark.components.BorderContainer;
	import spark.events.IndexChangeEvent;
	
	import co.beek.admin.Admin;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.FileUploader;
	import co.beek.admin.poup.FileUploaderMX;
	import co.beek.loading.ImageLoader;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	
	public class CoverAdmin extends Canvas
	{
		private var _guideData:GuideData;

		private var _currentScene:SceneData;
		
		private var coverImageBitmap:BitmapData;
		
		[Bindable]
		public var coverImagePath:String;
		
		private var cacheWhenLoadComplete:Boolean;
		
		public function CoverAdmin()
		{
			super();
			Admin.instance.addEventListener(Admin.PLAYER_LOADED, onPlayerLoaded);
		}
		
		private function onPlayerLoaded(event:Event):void
		{	
			currentScene = Model.currentScene;
			
			if(Model.guideData){
				coverImagePath = Model.config.getAssetUrl(Model.guideData.coverDesign);
				guideData = Model.guideData;
			}
			//Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			//Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		private function onGuideChange(event:Event):void
		{
			guideData = Model.guideData;
		}
		
		[Bindable (event="guide_change")]
		protected function get guideData():GuideData
		{
			return _guideData;
		}
		
		protected function set guideData(value:GuideData):void
		{
			_guideData = value;
			
			if(!value)
				return;
			
			_guideData.addEventListener(GuideData.FIRST_SCENE_UPDATED, onFirstSceneUpdated);
			cacheWhenLoadComplete = _guideData.thumbIncrement == 0;
			
			if(_guideData.coverDesign)
				coverImageToLoad = _guideData.coverDesign;
			
			else if(_guideData.firstScene && _guideData.firstScene.scene.thumbName)
				coverImageToLoad = _guideData.firstScene.scene.thumbName;
			
			else
				cacheWhenLoadComplete = true
			
			dispatchEvent(new Event("guide_change"));
		}
		
		private function set coverImageToLoad(value:String):void
		{
			trace('CoverAdmin.set coverImageToLoad()');
			ImageLoader.load(
				Model.config.getAssetUrl(value), 
				onCoverImageLoadCallback,
				onCoverImageLoadCallbackError
			);
		}
		
		private function onCoverImageLoadCallbackError():void
		{
			trace("onCoverImageLoadCallbackError");
		}
		
		private function onCoverImageLoadCallback(data:BitmapData):void
		{
			trace('onCoverImageLoadCallback');
			coverImageBitmap = data;
			if(cacheWhenLoadComplete)
				recaptureThumb();
		}
		
		protected function recaptureThumb():void
		{
			if(!coverImageBitmap)
			{
				cacheWhenLoadComplete = true;
				coverImageToLoad = _guideData.firstScene.scene.thumbName;
			}
			else
			{
				
				cacheWhenLoadComplete = false;
				dispatchEvent(new Event("thumb_change"));
			}
		}
		
		private function onFirstSceneUpdated(event:Event):void
		{
			cacheWhenLoadComplete = true;
			coverImageToLoad = guideData.firstScene.scene.thumbName;
		}
		
		private function onSceneChange(event:Event):void
		{
			currentScene = Model.currentScene;
		}
		
		private function set currentScene(value:SceneData):void
		{
			if(_guideData && _guideData.firstScene && value
				&& _guideData.firstScene.scene.id == value.id)
			{
				
				if(_currentScene)
					_currentScene.removeEventListener(SceneData.NEW_THUMB, onSceneNewThumb);
				
				//if(value.newThumb)
					//coverImageBitmap = value.newThumb;
				
				_currentScene = value;
				_currentScene.addEventListener(SceneData.NEW_THUMB, onSceneNewThumb);
			}
		}
		
		private function onSceneNewThumb(event:Event):void
		{
			coverImageBitmap = _currentScene.newThumb;
			recaptureThumb();
		}
		
		protected function set guideTitle(value:String):void
		{
			_guideData.title = value;
			recaptureThumb()
		}
		
		protected function set majorColor(value:int):void
		{
			_guideData.guideColor = value;
			recaptureThumb();
		}
		
		[Bindable (event="thumb_change")]
		public function get guideThumb():Object
		{
			if(Model.guideData.newThumb)
				return new Bitmap(Model.guideData.newThumb);
			
			return Model.config.getAssetUrl(Model.guideData.thumbName);
		}
		
		private var recaptureThumbTimeout:uint;
		
		private function recaptureThumbDelayed(millis:int = 2000):void
		{
			clearTimeout(recaptureThumbTimeout);
			recaptureThumbTimeout = setTimeout(recaptureThumb, millis);
		}
		
		
		//private var fileRef:FileReference = new FileReference;
		protected function browseForCover():void
		{
			var uploader:FileUploader = new FileUploaderMX();
			uploader.addEventListener(FileUploader.UPLOAD_COMPLETE, onUploaderComplete);
			uploader.addEventListener(CloseablePanel.CLOSE, onVideoSelectorClose);
			uploader.browse(guideData.teamId, "image files", "*.jpg;*.jpeg;*.png;*.gif;*.JPG;*.JPEG;*.PNG;*.GIF");
			PopUpManager.addPopUp(uploader, Admin.instance, true);
			PopUpManager.centerPopUp(uploader);
		}
		
		private function onUploaderComplete(event:Event):void
		{
			var uploader:FileUploader = FileUploaderMX(event.target);
			Model.guideData.coverDesign = uploader.file.realName;
			coverImagePath = Model.config.getAssetUrl(Model.guideData.coverDesign);
			cacheWhenLoadComplete = true;
			coverImageToLoad = uploader.file.realName;
			PopUpManager.removePopUp(uploader);
		}
		
		private function onVideoSelectorClose(event:Event):void
		{
			var uploader:FileUploader = FileUploaderMX(event.target);
			PopUpManager.removePopUp(uploader);
		}
		
		protected function clearCoverDesign():void
		{
			Model.guideData.coverDesign = null;
			cacheWhenLoadComplete = true;
			coverImageToLoad = _guideData.firstScene.scene.thumbName;
		}
	}
}