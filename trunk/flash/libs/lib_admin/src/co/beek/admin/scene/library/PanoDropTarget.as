package co.beek.admin.scene.library
{
	import co.beek.admin.Admin;
	import co.beek.admin.BeekDragManager;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.LocationBrowser;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.admin.scene.hotspot.video.VideoPreselector;
	import co.beek.admin.scene.hotspot.video.VideoPreselectorMX;
	import co.beek.event.LocationDataEvent;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.data.PanoAngle;
	import co.beek.model.data.PanoPos;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.GuideSceneHotspot;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	public class PanoDropTarget extends Canvas
	{
		public function PanoDropTarget()
		{
			super();
			
			addEventListener(DragEvent.DRAG_ENTER, dragEnterHandler);
			addEventListener(DragEvent.DRAG_DROP, dragDropHandler);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		// Called when the user moves the drag proxy onto the drop target.
		protected function dragEnterHandler(event:DragEvent):void 
		{
			// Get the drop target component from the event object.
			var dropTarget:Canvas = Canvas(event.currentTarget);
			
			// Accept the drop.
			DragManager.acceptDragDrop(dropTarget);
		}
		
		// Called if the target accepts the dragged object and the user 
		// releases the mouse button while over the Canvas container. 
		protected function dragDropHandler(event:DragEvent):void 
		{
			var d:Object = BeekDragManager.dragData;
			if(!(BeekDragManager.dragData is HotspotData))
			{
				BeekDragManager.dropped();
				return;
			}
				
			
			// Get the data from the drag source.
			var data:HotspotData = HotspotData(BeekDragManager.dragData);
			var angle:PanoAngle = Admin.instance.player.pano.getAngle(event.localX, event.localY);
			data.panoPos = new PanoPos(angle.pan, angle.tilt, 1000);
			
			if(data is VideoData)
				showVideoPreselector(data as VideoData);
			
			else if(data is SoundData && SoundData(data).preview != null)
				addHotspotToScene(data);
				
			else if(data is MediaData && MediaData(data).file == null)
				showMediaUploader(data as MediaData);
			
			else if(data is BubbleData && BubbleData(data).targetSceneId == null)
				showSceneBrowser(data as BubbleData);
			
			else
				addHotspotToScene(data);
		}
		
		private function showVideoPreselector(data:VideoData):void
		{
			var videoPreselector:VideoPreselector = new VideoPreselectorMX();
			videoPreselector.videoData = data;
			videoPreselector.addEventListener(VideoPreselector.BROWSE_FOR_FLV, onBrowseForFlv);
			videoPreselector.addEventListener(VideoPreselector.YOUTUBE_READY, onYoutubeReady);
			videoPreselector.addEventListener(CloseablePanel.CLOSE, onVideoSelectorClose);
			
			PopUpManager.addPopUp(videoPreselector, Admin.instance, true);
			PopUpManager.centerPopUp(videoPreselector);
		}
		
		private function onBrowseForFlv(event:Event):void
		{
			var videoPreselector:VideoPreselector = VideoPreselectorMX(event.target);
			showMediaUploader(videoPreselector.videoData);
			
			onVideoSelectorClose(event);
		}

		private function onYoutubeReady(event:Event):void
		{
			var videoPreselector:VideoPreselector = VideoPreselectorMX(event.target);
			addHotspotToScene(videoPreselector.videoData);
			
			onVideoSelectorClose(event);
		}
		
		private function onVideoSelectorClose(event:Event):void
		{
			var videoPreselector:VideoPreselector = VideoPreselectorMX(event.target);
			videoPreselector.videoData = null;
			PopUpManager.removePopUp(videoPreselector);
		}
		
		private function showMediaUploader(data:MediaData):void
		{
			var mediaUploader:MediaUploader = new MediaUploaderMX();
			mediaUploader.addEventListener(MediaUploader.UPLOAD_COMPLETE, onMediaUploadComplete);
			mediaUploader.addEventListener(CloseablePanel.CLOSE, onMediaUploaderClose);
			mediaUploader.hotspot = data;
			PopUpManager.addPopUp(mediaUploader, Admin.instance, true);
			PopUpManager.centerPopUp(mediaUploader);
		}
		
		private function onMediaUploaderClose(event:Event):void
		{
			var mediaUploader:MediaUploader = MediaUploaderMX(event.target);
			mediaUploader.hotspot = null;
			PopUpManager.removePopUp(mediaUploader);
		}
		
		private function onMediaUploadComplete(event:Event):void
		{
			var mediaUploader:MediaUploader = MediaUploaderMX(event.target);
			addHotspotToScene(mediaUploader.hotspot);
			
			mediaUploader.dispose();
			
			PopUpManager.removePopUp(mediaUploader);
		}
		
		private var pendingBubbleData:BubbleData;
		
		private function showSceneBrowser(bubble:BubbleData):void
		{
			pendingBubbleData = bubble;
			
			var center:Geo = Model.guideScene 
				? Model.guideScene.scene.geo 
				: new Geo(-41.2961, 174.7944);
			
			var browser:SceneBrowser = new SceneBrowserMX;
			browser.addEventListener(LocationBrowser.LOCATION_SELECTED, onLocationSelected);
			browser.addEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			browser.addEventListener(CloseablePanel.CLOSE, onSceneBrowserClose);
			browser.mapCenter = new GeoZoom(center.lat, center.lon, 13);
			PopUpManager.addPopUp(browser, Admin.instance, true);
			PopUpManager.centerPopUp(browser);
		}
		
		private function onSceneBrowserClose(event:Event):void
		{
			closeSceneBrowser(event.target as SceneBrowser);
		}
		
		private function closeSceneBrowser(browser:SceneBrowser):void
		{
			browser.removeEventListener(LocationBrowser.LOCATION_SELECTED, onLocationSelected);
			browser.removeEventListener(SceneBrowser.SCENE_SELECTED, onSceneSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onSceneBrowserClose);
			PopUpManager.removePopUp(browser);
			
			pendingBubbleData = null;
		}
		
		private function onLocationSelected(event:LocationDataEvent):void
		{
			pendingBubbleData.targetScene = event.location.defaultScene;
			pendingBubbleData.title = event.location.title;
			addHotspotToScene(pendingBubbleData);
			
			closeSceneBrowser(event.target as SceneBrowser);
		}
		
		private function onSceneSelected(event:SceneBasicEvent):void
		{
			pendingBubbleData.targetScene = event.scene;
			addHotspotToScene(pendingBubbleData);
			
			closeSceneBrowser(event.target as SceneBrowser);
		}
		
		private function addHotspotToScene(data:HotspotData):void
		{
			// add the hotspot to the guide scene or the scene
			if(Model.isEditableGuide && data is GuideSceneHotspot)
				Model.guideScene.addHotspot(data);
				
			else if(Model.isEditableScene)
				Model.currentScene.addHotspot(data);
			
			// select the newly added hotspot
			//Model.hotspot = data;
			
			// tell the world its dropped
			BeekDragManager.dropped();
		}
		
		private function onMouseMove(event:MouseEvent):void
		{
			if(!event.buttonDown)
				BeekDragManager.dropped();
		}
		
	}
}