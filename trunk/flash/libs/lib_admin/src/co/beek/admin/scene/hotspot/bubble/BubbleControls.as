package co.beek.admin.scene.hotspot.bubble
{
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.model.hotspots.BubbleData;
	
	public class BubbleControls extends AbstractControlPanel
	{
		public function BubbleControls()
		{
			super();
		}
		
		[Bindable (event="hostspot_change")]
		public function get bubbleData():BubbleData
		{
			return _hotspotData as BubbleData;
		}
	}
}