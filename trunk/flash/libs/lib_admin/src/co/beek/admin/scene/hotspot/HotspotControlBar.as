package co.beek.admin.scene.hotspot
{
	import co.beek.model.Model;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.ControlBar;
	import mx.containers.ViewStack;
	import mx.controls.Label;
	
	/**
	 * For some reason data binding is not working here.
	 * It might be the flex 4.5 sdk
	 */
	public class HotspotControlBar extends ControlBar
	{
		public var viewStack:ViewStack;
		
		public var removeTextField:Label;
		
		public var _hotspotData:HotspotData;
		
		public function HotspotControlBar()
		{
			super();
		}
		
		public function set hotspotData(value:HotspotData):void
		{
			if(_hotspotData == value)
				return;
			
			_hotspotData = value;
			viewStack.selectedIndex = 0;
			
			if(value)
				removeTextField.text = "Remove "+getHotspotName(value)+"?";
			
			dispatchEvent(new Event("hotspot_change"));
		}
		
		private function getHotspotName(data:HotspotData):String
		{
			if(data is SoundData)
				return "Sound";
			
			if(data is BubbleData)
				return "Bubble";
			
			if(data is PhotoData)
				return "Photo";
			
			if(data is BoardData)
				return "Board";
			
			if(data is VideoData)
				return "VideoPlayer";
			
			if(data is PosterData)
				return "Poster";

			if(data is RssReaderData)
				return "RssReader";
			
			return null;
		}
		
		[Bindable (event="hotspot_change")]
		public function get hotspotData():HotspotData
		{
			return _hotspotData;
		}
		
		protected function onOKRemoveButtonClick(event:MouseEvent):void
		{
			if(Model.guideScene && Model.guideScene.hasHotspot(_hotspotData))
				Model.guideScene.removeHotspot(_hotspotData);
			else
				Model.currentScene.removeHotspot(_hotspotData);
			
			Model.hotspot = null;
			//Model.state.adminView = State.ADMIN_VIEW_SCENE;
		}
	}
}