package co.beek.admin.scene.library
{
	import co.beek.admin.scene.library.defaults.DefaultsMX;
	import co.beek.admin.scene.library.guideScene.GuideSceneMX;
	import co.beek.admin.scene.library.hotspots.HotspotsMX;
	import co.beek.admin.scene.library.media.MediaMX;
	import co.beek.admin.scene.library.sound.SoundRepoMX;
	
	import co.beek.model.Model;
	
	import flash.events.Event;
	
	import mx.containers.Accordion;
	import mx.containers.Panel;
	
	[Event(name="dragBubbleStart", type="flash.events.Event")]
	public class LibraryPanel extends Panel
	{
		public var accordion:Accordion;
		
		private var defaultsPanel:DefaultsMX = new DefaultsMX;
		
		private var hotspotsPanel:HotspotsMX = new HotspotsMX;
		
		public function LibraryPanel()
		{
			super();
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		override protected function createChildren():void 
		{
			super.createChildren();
			
			if(Model.isEditableScene){
				accordion.addChild(defaultsPanel);
				accordion.addChild(hotspotsPanel);
			}
			
			if(Model.isEditableGuide)
				accordion.addChild(new GuideSceneMX);

			accordion.addChild(new MediaMX);
			accordion.addChild(new SoundRepoMX);
			
				
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if(accordion)
				accordion.invalidateSize();
		}
		
		private function onSceneChange(event:Event):void
		{
			if(Model.isEditableScene)
			{
				if(defaultsPanel.parent != accordion)
					accordion.addChildAt(defaultsPanel, 0);

				if(hotspotsPanel.parent != accordion)
					accordion.addChild(hotspotsPanel);
			}
			else
			{
				if(defaultsPanel.parent == accordion)
					accordion.removeChild(defaultsPanel);
				
				if(hotspotsPanel.parent == accordion)
					accordion.removeChild(hotspotsPanel);
			}
		}
	}
}