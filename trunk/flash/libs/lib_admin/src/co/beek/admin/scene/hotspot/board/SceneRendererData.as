package co.beek.admin.scene.hotspot.board
{
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneData;
	import co.beek.model.hotspots.BoardData;
	import co.beek.model.hotspots.BoardSignData;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class SceneRendererData extends EventDispatcher
	{
		private var _scene:SceneBasicData;
		private var _board:BoardData;
		
		public function SceneRendererData(scene:SceneBasicData, board:BoardData)
		{
			super();
			_scene = scene;
			_board = board;
		}
		
		public function get scene():SceneBasicData
		{
			return _scene;
		}

		public function get board():BoardData
		{
			return _board;
		}
	}
}