package co.beek.admin.guide.section
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	
	public class MoveScenePanel extends CloseablePanel
	{
		[Bindable]
		protected var sections:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var _selectedSection:GuideSectionData;
		
		private var _guideScene:GuideSceneData;
		
		public function MoveScenePanel()
		{
			super();
		}
		
		public function set guideScene(value:GuideSceneData):void
		{
			_guideScene = value;
			
			if(Model.guideData)
				for(var i:int=0; i<Model.guideData.guideSections.length; i++)
				{
					var section:GuideSectionData = Model.guideData.guideSections[i];
					if(section.id == guideScene.sectionId)
						_selectedSection = section;
					
					sections.addItem(section);
				}
			dispatchEvent(new Event("sectionChange"));
		}
		
		public function get guideScene():GuideSceneData
		{
			return _guideScene;
		}

		[Bindable (event="sectionChange")]
		public function get guideSection():GuideSectionData
		{
			return _selectedSection;
		}
		
		protected function onSelectedSectionChange(selected:Object):void
		{
			_selectedSection = GuideSectionData(selected);
			dispatchCloseEvent()
		}
	}
}