package co.beek.admin.map
{
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.map.Map;
	import co.beek.map.MapData;
	import co.beek.map.Tile;
	import co.beek.map.markers.IMapMarker;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideData;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.flash.UIMovieClip;
	
	[Event(name="centerChange", type="flash.events.Event")]
	[Event(name="zoomChange", type="flash.events.Event")]
	public class MapHolder extends Canvas
	{
		protected var mapData:MapData = new MapData(200, 200, 18, Tile.STYLE_DEFAULT_MAP);
		
		protected var map:Map = new Map;
		
		protected var markers:Vector.<IMapMarker> = new Vector.<IMapMarker>;
		protected var markersHolder:Sprite = new Sprite;
		protected var markersMask:Shape = new Shape();
		
		private var downPos:Point;
		
		public function MapHolder()
		{
			super();
			
			mapData.addEventListener(Event.CHANGE, onMapDataChange);
			
			map.data = mapData;
			markersHolder.mask = markersMask;
			
			rawChildren.addChild(map);
			rawChildren.addChild(markersHolder);
			rawChildren.addChild(markersMask);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			map.addEventListener(MouseEvent.MOUSE_DOWN, onMapMouseDown);
			map.addEventListener(MouseEvent.MOUSE_MOVE, onMapMouseMove);
			map.addEventListener(MouseEvent.MOUSE_UP, onMapMouseUp);
			map.addEventListener(MouseEvent.MOUSE_WHEEL, onMapMouseWheel);
		}
		
		public function set center(value:GeoZoom):void
		{
			if(value.lat == 0){
				var lat:Number = Model.currentScene.geo.lat != 0 ? Model.currentScene.geo.lat : Model.currentScene.location.geo.lat;
				var lon:Number = Model.currentScene.geo.lon != 0 ? Model.currentScene.geo.lon : Model.currentScene.location.geo.lon;
				value = new GeoZoom(lat,lon,13);
			}

			mapData.centerZoom = value;
			positionMarkers();
		}
		
		public function get center():GeoZoom
		{
			return new GeoZoom(mapData.center.lat, mapData.center.lon, mapData.zoom);
		}
		
		public function get topLeft():Geo
		{
			return mapData.topLeft;
		}

		public function get bottomRight():Geo
		{
			return mapData.bottomRight;
		}
		
		public function set zoom(value:Number):void
		{
			mapData.zoom = value;
			positionMarkers();
			dispatchEvent(new Event("zoomChange"));
		}
		
		public function get zoom():Number
		{
			return mapData.zoom;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number,unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			mapData.width = unscaledWidth;
			mapData.height = unscaledHeight;
			
			markersMask.graphics.clear();
			markersMask.graphics.beginFill(0);
			markersMask.graphics.drawRect(0,0,unscaledWidth, unscaledHeight);
		}
		
		public function addMarker(marker:IMapMarker):void
		{
			markersHolder.addChild(marker as DisplayObject);
			markers.push(marker);
			
			if(mapData.center)
			{
				var pos:Point = mapData.posForGeo(marker.geo);
				marker.x = pos.x;
				marker.y = pos.y;
			}
		}
		
		public function showAllMapMarkers():void
		{
			var group:Vector.<Geo> = new Vector.<Geo>;
			for(var i:int = 0; i<markers.length; i++)
				group.push(markers[i].geo);
			
			mapData.centerAroundAll(group);
			positionMarkers();
		}
		
		public function removeMarker(marker:IMapMarker):void
		{
			markersHolder.removeChild(marker as DisplayObject);
			markers.splice(markers.indexOf(marker), 1);
		}
		
		private function onMapDataChange(event:Event):void
		{
			positionMarkers();
		}
		
		protected function positionMarkers():void
		{
			for(var i:int = 0; i<markers.length; i++)
			{
				var marker:IMapMarker = markers[i];
				var pos:Point = mapData.posForGeo(marker.geo);
				marker.x = pos.x;
				marker.y = pos.y;
			}
		}
		
		private function onMapMouseDown(event:MouseEvent):void
		{
			downPos = new Point(event.stageX, event.stageY);
		}
		
		private function onMapMouseMove(event:MouseEvent):void
		{
			if(downPos == null || !event.buttonDown)
				return;
			
			var delta:Point = new Point(
				downPos.x - event.stageX, 
				downPos.y - event.stageY
			);
			mapData.offsetFromCenter(delta);
			
			downPos = new Point(event.stageX, event.stageY);
			positionMarkers();
		}
		
		private function onMapMouseUp(event:MouseEvent):void
		{
			downPos = null;
			resetMap();
			dispatchEvent(new Event("centerChange"));
		}
		
		private function onMapMouseWheel(event:MouseEvent):void
		{
			mapData.zoom += int(event.delta/3);
			positionMarkers();
		}
		
		protected function resetMap():void
		{
		}
	}
}