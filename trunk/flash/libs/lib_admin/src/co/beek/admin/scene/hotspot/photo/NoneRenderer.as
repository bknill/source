package co.beek.admin.scene.hotspot.photo
{
	import mx.flash.UIMovieClip;
	
	public class NoneRenderer extends UIMovieClip
	{
		public function NoneRenderer()
		{
			super();
			addChild(new AdminPhotoFrameNone);
		}
	}
}