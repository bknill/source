package co.beek.admin.component
{
	import mx.controls.Button;
	
	public class ConfigButton extends Button
	{
		[Embed("/../images/button_config.png")] 
		private var skin: Class; 
		
		public function ConfigButton()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 25;
		}
	}
}