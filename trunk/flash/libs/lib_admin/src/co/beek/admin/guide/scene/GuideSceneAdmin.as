package co.beek.admin.guide.scene
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	
	import mx.containers.Canvas;
	import mx.managers.PopUpManager;
	
	import co.beek.Fonts;
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.SceneTypeBrowser;
	import co.beek.admin.poup.SceneTypeBrowserMX;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.utils.ColorUtil;
	
	public class GuideSceneAdmin extends Canvas
	{
		
		[Bindable]
		protected var _scene:SceneData;

		[Bindable]
		protected var _guideScene:GuideSceneData;
		
		[Bindable]
		protected var title:String;

		[Bindable]
		protected var description:String;
		
		[Bindable]
		protected var hotspots:Vector.<HotspotData>;

		[Bindable]
		protected var titleFormatFamily:String = Fonts.titleFont;
		
		//[Bindable]
		//protected var titleFormatSize:Number = Fonts.titleFo
		
		[Bindable]
		protected var bodyFormatFamily:String = Fonts.bodyFont;
		
		//[Bindable]
		//protected var bodyFormatSize:Number = Fonts.bodyFormat.size as Number;
		
		
		public function GuideSceneAdmin()
		{
			super();
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.state.addEventListener(State.UI_STATE_CHANGE,onStateChange);
		}
		
		public function set guideScene(value:GuideSceneData):void
		{
			if(_guideScene)
			{
				_guideScene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_guideScene.removeEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			
			_guideScene = value;
			_guideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
			_guideScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			
			title = _guideScene.titleMerged;
			description = _guideScene.descriptionMerged;
			
			scene = Model.currentScene;
		}
		
		private function onSceneChange(event:Event):void
		{
			if(Model.config.guideId)
				guideScene = Model.guideScene;
		}
		
		private function onStateChange(event:Event):void
		{
			trace("GuideSceneAdmin.onStateChange()");
			if(Model.config.guideId)
				guideScene = Model.guideScene;
		}
		
		private function set scene(value:SceneData):void
		{
			if(_scene)
			{
				_scene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_scene.removeEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			
			if(!value || !_guideScene || value.id != _guideScene.scene.id)
				return;
				
			_scene = value;
			_scene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
			_scene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			
			this.hotspots =  Model.hotspotsMerged;
		}
		
		private function onSceneHotspotsChange(event:Event):void
		{
			this.hotspots = Model.hotspotsMerged;
		}
		
		protected function setTitle(value:String):void
		{
			if(_guideScene)
				_guideScene.title = value;
			else
				_scene.title = value;
		}
		
		protected function browseForType(event:MouseEvent):void
		{
			var b:SceneTypeBrowser = new SceneTypeBrowserMX;
			b.addEventListener(SceneTypeBrowser.TYPE_SELECTED, onTypeSelected);
			b.addEventListener(CloseablePanel.CLOSE, onPanelClose);
			b.type = Model.currentScene.type;
			PopUpManager.addPopUp(b, DisplayObject(event.target), true);
			PopUpManager.centerPopUp(b);
		} 
		
		private function onTypeSelected(event:Event):void
		{
			var b:SceneTypeBrowser = SceneTypeBrowser(event.target);
			
			// This updates the ui the client is seeing
			_guideScene.scene.type = b.type;
			
			// this is the only way to update the db
			if(Model.currentScene.id == _guideScene.scene.id)
				Model.currentScene.type = b.type;
			
			removePanel(b);
		}
		
		private function onPanelClose(event:Event):void
		{
			removePanel(SceneTypeBrowser(event.target));
		}
		
		private function removePanel(browser:SceneTypeBrowser):void
		{
			browser.removeEventListener(SceneTypeBrowser.TYPE_SELECTED, onTypeSelected);
			browser.removeEventListener(CloseablePanel.CLOSE, onPanelClose);
			PopUpManager.removePopUp(browser);
		}
		
		private var defaultSelected:Boolean;
		
		protected function defaultChecked(value:Boolean):void
		{
			defaultSelected = value;
			if(defaultSelected)
				Model.currentScene.description = _guideScene.description;
		}
		
		protected function setDescription(value:String):void
		{
			_guideScene.description = value;
			
			if(defaultSelected && Model.isEditableScene)
				Model.currentScene.description = value;
		}
	}
}