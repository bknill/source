package co.beek.admin.scene.library.sound
{
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.data.FreeSoundData;
	import co.beek.model.data.PanoLook;
	import co.beek.model.data.SceneData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.service.FileService;
	import co.beek.service.data.FileList;
	import co.beek.service.data.FileUploader;
	import co.beek.utils.JSONBeek;
	

	
	import flash.display.*;
	import flash.events.*;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.media.*;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.*;
	

	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.Tree;
	

	
	public class SoundRepo extends VBox
	{
		private static const EXTENSION_MP3:String = "mp3";
		
		public var tree:Tree;
		private var teamId:String;
		
		private var fileRef:FileReference = new FileReference;
		
		/**
		 * Ben: People
			Ambiance - Interiors
			Ambiance - Exteriors
			Music - Classic
			Music - Modern
		 **/
		[Bindable]
		public var treeData:ArrayCollection = new ArrayCollection([
			new SoundFolder(SoundFolder.FOLDER_ID_PEOPLE, "People"),
			new SoundFolder(SoundFolder.FOLDER_ID_INTERIOR, "Ambience - Interiors"),
			new SoundFolder(SoundFolder.FOLDER_ID_EXTERIOR, "Ambience - Exteriors"),
			new SoundFolder(SoundFolder.FOLDER_ID_CLASSIC, "Music - Classic"),
			new SoundFolder(SoundFolder.FOLDER_ID_MODERN, "Music - Modern")
		]);
		
		
		
		public var uploadButton:Button;
		public var browseButton:Button;
		
		
		[Bindable]
		protected var uploadFileName:String;
		
		
		
		[Bindable]
		protected var sceneSounds:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var guideSceneSounds:ArrayCollection = new ArrayCollection;
		
		[Bindable]
		protected var sceneSoundIndex:int = -1;
		
		[Bindable]
		protected var guideSceneSoundIndex:int = -1;
		
		
		[Bindable]
		protected var searchTerm:String;

		
		
		public var tags:Array = [];
		
		[Bindable]
		public var searchedSounds:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public var freeSoundsTreeData:ArrayCollection = new ArrayCollection();
		

		
		
		
		public function SoundRepo()
		{
			super();
			
			fileRef.addEventListener(Event.SELECT, onFileReferenceSelect);
			fileRef.addEventListener(ProgressEvent.PROGRESS, onFileRefProgress);
			fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileReferenceComplete);
			

				
	
			
			scene = Model.currentScene;
			guideScene = Model.guideScene;
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.addEventListener(Model.HOTSPOT_CHANGE, onHotspotChange);
		}
		
		override protected function childrenCreated():void
		{
			trace("SoundRepo.childrenCreated()");
			super.childrenCreated();
			FileService.getPublicFiles(onFileList);
			
			if(Model.currentScene)
				buildSceneSounds();
		}
		
		protected function get treeHeight():Number
		{
			
			var rows:int = treeData.length;
			var open:Array = tree.openItems as Array;
			for(var i:int = 0; i<open.length; i++)
			{
				if(open[i].hasOwnProperty("children"))
					rows += (open[i].children as Array).length;
			}
			
			return (rows + 1) * tree.rowHeight;
		}
		
		private function onFileList(list:FileList):void
		{
			for(var i:int; i<list.files.length; i++)
			{
				var file:FileData = list.files[i];
				
				// ignore other file types although 
				// none should come through if files table is good.
				if(file.extension != EXTENSION_MP3)
					continue;
				
				var category:SoundFolder = getCategory(file);
				if(category)
					category.addFile(file);
			}
		}
		
		private function getCategory(file:FileData):SoundFolder
		{
			for(var i:int; i<treeData.length; i++)
				if(SoundFolder(treeData.getItemAt(i)).id == file.folderId)
					return SoundFolder(treeData.getItemAt(i));
			
			return null;
		}
		
		protected function onBrowseButtonClick(event:Event):void 
		{
			fileRef.browse([new FileFilter("Sounds", "*.mp3")]);
		}
		
		private function onFileReferenceSelect(event:Event):void 
		{
			//browseButton.enabled = false;
			uploadButton.enabled = true;
			
			uploadFileName = fileRef.name;
		}
		
		
		
		protected function onUploadButtonClick(event:Event):void 
		{
			// pass in the jsessionid to restore the session which is lost in this kind or request.
			var uploadUrl:String = Model.config.serviceUrl+"/files/upload/" +
				teamId + ";jsessionid="+Model.sessionData.sessionId;
			var request:URLRequest = new URLRequest(uploadUrl)
			request.method = URLRequestMethod.POST;
			request.data = uploadFileName;
			try {
				fileRef.upload(request, "file");
			} catch (error:Error) {
				trace("Unable to upload file.");
			}
			browseButton.enabled = true;
			uploadButton.enabled = false;
		}
		
		private function onFileRefProgress(event:Event):void 
		{
			trace("onFileRefProgress");
		}
		
		private function onFileReferenceComplete(event:DataEvent):void 
		{
			trace("onFileReferenceComplete");
			browseButton.enabled = true;
			uploadButton.enabled = false;
			
			var data:Object;
			try
			{
				data = JSONBeek.decode(event.data)
			}
			catch(e:Error)
			{
				throw new Error("Error decoding JSON for file:"+event.data)
			}
			treeData.addItem(new FileData(data));
		}
		
		/***************************** Sounds in Scene **********************************/
		
		private var _scene:SceneData;
		private var _guideScene:GuideSceneData;
		
		private function onSceneChange(event:Event):void
		{
			scene = Model.currentScene;
			guideScene = Model.guideScene;
		}
		
		private function set scene(value:SceneData):void
		{
			if(_scene)
			{
				_scene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_scene.removeEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			_scene = value;
			
			if(_scene)
			{
				_scene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_scene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			buildSceneSounds();
		}
		
		private function set guideScene(value:GuideSceneData):void
		{
			if(_guideScene)
			{
				_guideScene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_guideScene.removeEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			_guideScene = value;
			
			if(_guideScene)
			{
				_guideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onSceneHotspotsChange);
				_guideScene.addEventListener(HotspotEvent.HOTSPOT_REMOVED, onSceneHotspotsChange);
			}
			
			buildGuideSceneSounds();
		}
		
		private function onSceneHotspotsChange(event:Event):void
		{
			buildSceneSounds();
		}
		
		private function buildSceneSounds():void
		{
			sceneSounds.removeAll();
			
			if(!_scene)
				return;
			
			for(var i:int; i<_scene.hotspots.length; i++){
				if(_scene.hotspots[i] is SoundData)
					sceneSounds.addItem(_scene.hotspots[i]);
			
			}

			sceneSoundIndex = -1;
		}
		
		private function buildGuideSceneSounds():void
		{
			guideSceneSounds.removeAll();
			
			if(!_guideScene)
				return;
			
			for(var g:int; g<_guideScene.hotspots.length; g++){
				if(_guideScene.hotspots[g] is SoundData){
				guideSceneSounds.addItem(_guideScene.hotspots[g]);
				}
			}
			
			guideSceneSoundIndex = -1;
		}
		
		private function onHotspotChange(event:Event):void
		{
			sceneSoundIndex = sceneSounds.getItemIndex(Model.hotspot);
		}
		
		protected function onSceneListChange(event:Event):void
		{
			var soundData:SoundData = event.currentTarget.selectedItem as SoundData;
			
			Model.hotspot = soundData;
		}
		
		protected function onGuideSceneListChange(event:Event):void
		{
			var soundData:SoundData = event.currentTarget.selectedItem as SoundData;
			
			Model.hotspot = soundData;
		}
		

		
	
			public function search():void
			{
				var loader:URLLoader = new URLLoader();
				loader.load(new URLRequest("http://www.freesound.org/apiv2/search/text/?query=" + searchTerm + "&type=mp3&format=json&page_size=150&token=7db60b6bf039a0a435d5aa36098fcb217908eb43"));
				loader.addEventListener(Event.COMPLETE,onFreeSoundDataLoaded)
				
				searchedSounds.removeAll();
				tags = [];
				freeSoundsTreeData.removeAll();
			}
			
				
			public function onFreeSoundDataLoaded(e:Event):void
			{
				var json:String = URLLoader(e.target).data;
				var data:Object = JSONBeek.decode(json);
				var results:Array = data.results;
				
				if(!data.results)
					return;
				
					for(var i:int = 0; i < results.length; i++){
						var fsFile:FreeSoundData = new FreeSoundData(results[i]);
						searchedSounds.addItem(fsFile);
						
						for(var t:int = 0; t < results[i].tags.length; t++)
								if(tags.indexOf(results[i].tags[t]) == -1)
									tags.push(results[i].tags[t]);	
					}
						
					for each(var tag:String in tags)
						freeSoundsTreeData.addItem(new SoundFolder(tag,tag))
				
					for each(var file:FreeSoundData in searchedSounds){
						for each(var folder:SoundFolder in freeSoundsTreeData)
						 if(file.tags.indexOf(folder.fileName) != -1)
							 folder.addFreeSound(file)
					}
			}

			
	}
}
