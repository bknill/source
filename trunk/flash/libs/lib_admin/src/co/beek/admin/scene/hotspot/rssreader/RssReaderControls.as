package co.beek.admin.scene.hotspot.rssreader
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.admin.poup.SceneBrowser;
	import co.beek.admin.poup.SceneBrowserMX;
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.event.SceneBasicEvent;
	import co.beek.map.GeoZoom;
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.service.SceneService;
	import co.beek.utils.JSONBeek;
	
	import com.adobe.serialization.json.JSON;
	import com.facebook.graph.Facebook;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	
	public class RssReaderControls extends AbstractControlPanel
	{

		protected static const APP_ID:String = "308635632515451"; 
		
		public function RssReaderControls()
		{
			super();
		}
		
		protected override function activate():void
		{
			super.activate();

		}
	
		
		[Bindable (event="hostspot_change")]
		public function get rssReaderData():RssReaderData
		{
			return _hotspotData as RssReaderData;
		}
		
		public function facebookInit():void   
		{
			trace("facebookInit");
			Facebook.init(APP_ID, facebookInitHandler,{
				appId: APP_ID,
				status: true,
				cookie: true,
				xfmbl: true,
				channelUrl: 'http://beekdev.co/resources/js/channel.html',
				oauth: true,
				scope: 'manage_pages'
			});
		}
		
	
		private var fbInit:Boolean = false;
		private var facebookLoggedInWithToken:Boolean;
		
		private function facebookInitHandler(response:Object, fail:Object):void
		{
			trace("facebookInitHandler" + response + fail);
				if (response)
				{
					facebookLoggedInWithToken = true;
					loadFeedData();
				} else {
					facebookLoggedInWithToken = false;
				    //Facebook.login(authApp);
				}
	
			
		}
		
		private function loadFeedData():void
		{
			Facebook.api("me/permissions",OnCompleteRequestPermissions);
		}
		
		private function OnCompleteRequestPermissions(response : Object, fail : Object):void
		{
			trace("OnCompleteRequestPermissions()");
			var perms:String = com.adobe.serialization.json.JSON.encode(response);
			
			if(perms.indexOf("manage_pages") == -1){
					Facebook.login(OnCompleteRequestPermissions, {scope: 'manage_pages'});
			}
			else
			{
				var request:String = "me/accounts";
				var requestType:String = "GET";
				Facebook.api(request, loadAccountsDataHandler, null, requestType);	
			}
		}
		
		[Bindable]
		public var pages:ArrayCollection =new ArrayCollection();
		
		[Bindable]
		public var pageIndex:int;
		
		private function loadAccountsDataHandler(response:Object, fail:Object):void
		{	
			trace("RSSReader.loadAccountsDataHandler()");
			var json:String = com.adobe.serialization.json.JSON.encode(response);
			var accounts:Object = JSONBeek.decode(json);

			for(var i:uint = 0; i < accounts.length; i++){
				pages.addItem(accounts[i]);
				pageIndex = i;
			}
		}
		
		public function set page(page:Object):void
		{
			//get long-lived access token
			var url:String = "https://graph.facebook.com/oauth/access_token?client_id=308635632515451&client_secret=830d446eb1526173c801f2d1221bfcec&grant_type=fb_exchange_token&fb_exchange_token=" + page.access_token;
			var loader:URLLoader = new URLLoader();
			
			loader.load(new URLRequest(url));
			loader.addEventListener(Event.COMPLETE,onLongAccessCodeLoaded);
			rssReaderData.url = page.id;
		}
		
		private function  onLongAccessCodeLoaded(event:Event):void
		{
			trace("onLongAccessCodeLoaded()");
			var loader:URLLoader = URLLoader(event.target); 
			var variables:URLVariables = new URLVariables(loader.data); 
			rssReaderData.url = rssReaderData.url + "|" + variables.access_token;
			
		}
		
		
	
	}
}