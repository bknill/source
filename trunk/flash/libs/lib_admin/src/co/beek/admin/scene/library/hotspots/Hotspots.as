package co.beek.admin.scene.library.hotspots
{
	import co.beek.admin.BeekDragManager;
	import co.beek.loading.LocationsLoader;
	import co.beek.loading.ScenesLoader;
	import co.beek.map.Geo;
	import co.beek.Log;
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.service.GuideService;
	import co.beek.service.SceneService;
	import co.beek.service.data.GuideSceneBasicList;
	import co.beek.service.data.SceneBasicList;
	
	import flash.events.Event;
	import flash.utils.setInterval;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	
	public class Hotspots extends VBox
	{
		[Bindable]
		public var nearbyScenes:ArrayCollection = new ArrayCollection;
		
		public function Hotspots()
		{
			super();
			
			// detect when a bubble has been dropped
			BeekDragManager.addEventListener(BeekDragManager.DRAG_DROPPED, onDragDropped);
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			loadNearbyScenes();
			//setInterval(loadNearbyScenes, 30000);
		}
		
		private function loadNearbyScenes():void
		{;
			Log.record("Admin.Hotspots.loadNearbyScenes()");
			
			if(Model.config.guideId)
				GuideService.getGuideScenes(Model.config.guideId,onGuideSceneList);
			else if(Model.currentScene)
				SceneService.getLocationScenes(Model.currentScene.locationId, onSceneBasicList);

			
		}
	
		
		private function onSceneBasicList(list:SceneBasicList):void
		{
			var scenes:Vector.<SceneBasicData> = list.scenes;
			nearbyScenes.removeAll();
			
			// Add the browse one
			nearbyScenes.addItem(new SceneBasicData({"title": "Browse for Scene"}));
			
			for(var i:int=0; i<scenes.length; i++)
				if(belongsInList(scenes[i]))
					nearbyScenes.addItem(scenes[i]);
		}
		
		private function onGuideSceneList(list:GuideSceneBasicList):void
		{
			var scenes:Vector.<SceneBasicData> = list.guideScenes;
					
				for(var i:int=0; i<scenes.length; i++){
					scenes[i].title =  scenes[i].locationTitle + " - " + scenes[i].title;
					if(belongsInList(scenes[i]))
						nearbyScenes.addItem(scenes[i]);
				}
		}
	
		
		/**
		 * Sort the scenes on distance from current scene.
		 * You have to multiply up to meters and then round to get ints.
		 */
		private function orderScenes(s1:SceneBasicData, s2:SceneBasicData):int
		{
			var geo:Geo = Model.currentScene.geo;
			return int(s1.geo.distance(geo)*1000) - int(s2.geo.distance(geo)*1000);
		}
		
		private function belongsInList(scene:SceneBasicData):Boolean
		{
			if(scene.id == Model.currentScene.id)
				return false;
			
			return isInScene(scene) == false;
		}
		
		/**
		 * Checks to see if this bubble already exists in the scene
		 */
		private function isInScene(scene:SceneBasicData):Boolean
		{
			var hotspots:Vector.<HotspotData> = Model.currentScene.hotspots;
			
			for(var i:int = 0; i < hotspots.length; i++)
				if(hotspots[i] is BubbleData
					&& BubbleData(hotspots[i]).targetSceneId == scene.id)
						return true;
			
			return false;
		}
		
		private function onSceneChange(event:Event):void
		{
			loadNearbyScenes();
		}
		
		private function onDragDropped(event:Event):void
		{
			Log.record("Hotspots.onDragDropped()");
			
			var bubbleDragged:BubbleData = BeekDragManager.dragData as BubbleData;
			
			// Remove the bubble, unless its the blank bubble for browsing
			if(bubbleDragged && bubbleDragged.targetScene.id != null)
				removeScene(bubbleDragged.targetScene.id);
		}
		
		private function removeScene(sceneId:String):void
		{
			for(var i:int = 0; i<nearbyScenes.length; i++)
			{
				var scene:SceneBasicData = SceneBasicData(nearbyScenes.getItemAt(i));
				if(scene.id == sceneId)
					nearbyScenes.removeItemAt(i);
			}
		}
	}
}