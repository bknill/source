package co.beek.admin.scene.library.defaults
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.clearInterval;
	import flash.utils.setTimeout;
	
	import co.beek.model.Model;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.ScenePanoTask;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.service.SceneService;
	import co.beek.service.data.FileUploader;
	
	public class PanoUploader extends EventDispatcher
	{
		public static const STAGE_CHANGE:String = "STAGE_CHANGE";
		public static const VIDEO_STAGE_CHANGE:String = "VIDEO_STAGE_CHANGE";
		public static const POLLING_CHANGE:String = "POLLING_CHANGE";
		
		public static const STAGE_INITIAL:int = 0;
		public static const STAGE_PENDING:int = 1;
		public static const STAGE_UPLOADING:int = 2;
		public static const STAGE_POLLING:int = 3;
		
		private var panoFileRef:FileReference = new FileReference;
		
		private var videoFileRef:FileReference = new FileReference;
		
		private var _stage:int = STAGE_INITIAL;
		
		private var _videoStage:int = STAGE_INITIAL;
		
		private var timeout:uint;

		private var _pollcount:int;

		private var _panoTask:ScenePanoTask;
		
		
		public function PanoUploader(target:IEventDispatcher=null)
		{
			super(target);
			
			panoFileRef.addEventListener(Event.SELECT, onPanoFileReferenceSelect);
			panoFileRef.addEventListener(ProgressEvent.PROGRESS, onPanoFileReferenceProgress);
			panoFileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onPanoFileReferenceComplete);
			
			videoFileRef.addEventListener(Event.SELECT, onVideoFileReferenceSelect);
			videoFileRef.addEventListener(ProgressEvent.PROGRESS, onVideoPanoFileReferenceProgress);
			videoFileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onVideoFileReferenceComplete);
		}
		
		public function get stage():int
		{
			return _stage;
		}
		
		public function get videoStage():int
		{
			return _videoStage;
		}
		
		private function updateStage(value:int):void
		{
			_stage = value;
			dispatchEvent(new Event(STAGE_CHANGE));
		}
		
		private function updateVideoStage(value:int):void
		{
			_videoStage = value;
			dispatchEvent(new Event(STAGE_CHANGE));
		}
		
		public function browsePanoImage():void
		{
			trace("PanoUploader.browsePanoImage()");
			var filter:FileFilter = new FileFilter("image files", "*.jpg;*.jpeg;*.JPG;*.JPEG;")
			panoFileRef.browse([filter]);
		}
		
		public function browsePanoVideo():void
		{
			trace("PanoUploader.browsePanoVideo()");
			var filter:FileFilter = new FileFilter("MP4 video files", "*.mp4")
			videoFileRef.browse([filter]);
		}
		
		private function onPanoFileReferenceSelect(event:Event):void 
		{
			trace("PanoUploader.onFileReferenceSelect()");
			updateStage(STAGE_PENDING);
		}
		
		private function onVideoFileReferenceSelect(event:Event):void 
		{
			trace("PanoUploader.onVideoFileReferenceSelect()");
			updateVideoStage(STAGE_PENDING);
		}
		
		public function uploadPanoImage():void
		{
			trace("PanoUploader.uploadPanoImage()");
			
			updateStage(STAGE_UPLOADING);
			_panoTask = null;
			
			var uploadUrl:String = Model.config.serviceUrl
				+"/scene/"+Model.currentScene.id+"/pano"
				+";jsessionid="+Model.sessionData.sessionId;
			
			var request:URLRequest = new URLRequest(uploadUrl)
			request.method = URLRequestMethod.POST;
			try 
			{
				panoFileRef.upload(request, "file");
			} 
			catch (error:Error) 
			{
				trace("Unable to upload file." + error.message);
			}
		}
		
		public function uploadPanoVideo():void
		{
			trace("PanoUploader.uploadPanoVideo()");
			
			updateVideoStage(STAGE_UPLOADING);
			
			var uploader:FileUploader = new FileUploader;
			uploader.addEventListener(ProgressEvent.PROGRESS, onVideoUploadProgress);
			uploader.addEventListener(Event.COMPLETE, onUploadComplete);
			uploader.upload( "panovideo.mp4", videoFileRef.data, true);
		}
		
		

		
		protected function onVideoUploadProgress(event:ProgressEvent):void
		{
			trace("onPanoVideoUploadProgress"+event.bytesTotal+" < "+event.bytesLoaded);
			if(event.bytesTotal > 100 && event.bytesTotal < event.bytesLoaded)
				uploadProgress = "uploading: "+ Math.round(event.bytesLoaded/event.bytesTotal * 100)+"%";
		}
		
		protected function onUploadComplete(event:Event):void 
		{
			trace('onUploadComplete()');
			var uploader:FileUploader = FileUploader(event.target);
			uploader.removeEventListener(ProgressEvent.PROGRESS, onVideoUploadProgress);
			uploader.removeEventListener(Event.COMPLETE, onUploadComplete);
			uploadProgress = null;
			
			// set the returned file on the photo
			Model.currentScene.video = uploader.fileData.realName;
			
			dispatchEvent(new Event("videoChange"));
		}
		
		
		private function onPanoFileReferenceProgress(event:ProgressEvent):void 
		{
			trace("PanoUploader.onFileReferenceProgress()");
			dispatchEvent(event);
		}
		
		private function onVideoPanoFileReferenceProgress(event:ProgressEvent):void 
		{
			trace("PanoUploader.onVideoFileReferenceProgress()");
			dispatchEvent(event);
		}
		
		private function onPanoFileReferenceComplete(event:DataEvent):void 
		{
			trace("PanoUploader.onFileReferenceComplete()");
			
			updateStage(STAGE_POLLING);
			
			//Model.currentScene.addEventListener(SceneData.PANO_UPDATED, onPanoUpdated);
			
			_pollcount = 0;
			loadPanoTask();
		}
		
		private function onVideoFileReferenceComplete(event:DataEvent):void 
		{
			trace("PanoUploader.onVideoFileReferenceComplete()");
			
			updateVideoStage(STAGE_POLLING);
			
			//Model.currentScene.addEventListener(SceneData.PANO_UPDATED, onPanoUpdated);
			
			_pollcount = 0;
		}
		
		private function loadPanoTask():void
		{
			//trace("PanoUploader.loadPanoTask()");
			SceneService.getScenesPanoTask(Model.currentScene.id, getScenesPanoTaskCallback);
		}
		
		public function get pollcount():int
		{
			return _pollcount;
		}

		public function get panoTask():ScenePanoTask
		{
			return _panoTask;
		}
		
		private function getScenesPanoTaskCallback(task:ScenePanoTask):void
		{
			var prev:ScenePanoTask = _panoTask;
			_panoTask = task;
			
			trace(prev+" != null && !"+_panoTask);
			
			// we have finished loading
			if(prev != null && !_panoTask)
				loadPanoIncrement();
			
			else if(_pollcount < 100)
				timeout = setTimeout(loadPanoTask, 3000);
			
			_pollcount++;
			dispatchEvent(new Event(POLLING_CHANGE));
		}
		
		private function loadPanoIncrement():void
		{
			var loader:URLLoader = new URLLoader;
			loader.addEventListener(Event.COMPLETE, onIncrementLoaderComplete);
			
			var uploadUrl:String = Model.config.serviceUrl
				+"/scene/"+Model.currentScene.id+"/increment";
			
			loader.load(new URLRequest(uploadUrl));
		}
		
		private function onIncrementLoaderComplete(event:Event):void
		{
			trace("PanoUploader.onLoaderComplete()"+URLLoader(event.target).data);
			Model.currentScene.panoIncrement = URLLoader(event.target).data;
			updateStage(STAGE_INITIAL);
		}
	}
}