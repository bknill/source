package co.beek.admin.guide.section
{
	import co.beek.Fonts;
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GuideSectionEvent;
	import co.beek.model.guide.GuideSectionData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.managers.PopUpManager;
	
	import spark.components.TextInput;
	import spark.components.supportClasses.ItemRenderer;
	
	[Event(name="sectionChildDelete", type="co.beek.admin.guide.sections.SectionEvent")]
	[Event(name="sectionChildMove", type="co.beek.admin.guide.sections.SectionEvent")]
	public class SectionsListRenderer extends ItemRenderer
	{
		[Bindable]
		protected var editmode:Boolean;
		
		[Bindable]
		public var titleTextInput:TextInput;

		/**
		 * For some reason, the native selected value is not bindable
		 */
		[Bindable]
		protected var isSelected:Boolean;
		

		
		
		[Bindable]
		protected var bodyFont:String = Fonts.bodyFont;
		
		public function SectionsListRenderer()
		{
			super();
		}
		
		public override function set selected(value:Boolean):void
		{
			super.selected = value;
			isSelected = value;
			editmode = false;
		}
		
		public override function set data(value:Object):void
		{
			super.data = value;
			
			if(guideSectionData)
				trace(guideSectionData.title)
		}
		
		[Bindable (event="dataChange")]
		protected function get guideSectionData():GuideSectionData
		{
			return data as GuideSectionData;
		}
		
		protected function set sectionTitle(value:String):void
		{
			if(guideSectionData)
				guideSectionData.title = value;
		}
		
		/*protected function dragEnterHandler(event:DragEvent):void
		{
			if (event.dragInitiator is ScenesList)
				DragManager.acceptDragDrop(this);
		}*/
		
		/*protected function onSceneDrop(event:DragEvent):void
		{
			var draggedItems:Vector.<Object> = Vector.<Object>(event.dragSource.dataForFormat("itemsByIndex"));
			var guideSceneData:GuideSceneData = GuideSceneData(draggedItems[0]);
			dispatchEvent(new SceneDropEvent("sceneDrop", guideSectionData, guideSceneData));
		}*/
		
		protected function toggleEdit():void
		{
			editmode = !editmode;
			if(editmode)
				titleTextInput.selectRange(0, titleTextInput.text.length-1);
		}
		
		protected function onSectionChildDeleteButtonClick(event:MouseEvent):void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to delete the section?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			dispatchEvent(new GuideSectionEvent("sectionChildDelete", guideSectionData));
		}
		
		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
	}
}