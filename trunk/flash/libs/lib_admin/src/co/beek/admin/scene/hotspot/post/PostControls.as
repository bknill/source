package co.beek.admin.scene.hotspot.post
{
	import co.beek.admin.scene.hotspot.AbstractControlPanel;
	import co.beek.model.hotspots.PostData;
	
	public class PostControls extends AbstractControlPanel
	{
		public function PostControls()
		{
			super();
		}
		
		[Bindable (event="hostspot_change")]
		public function get postData():PostData
		{
			return _hotspotData as PostData;
		}
	}
}