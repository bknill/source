package co.beek.admin.poup
{
	import co.beek.admin.map.MapHolder;
	import co.beek.event.LocationDataEvent;
	import co.beek.map.Geo;
	import co.beek.map.GeoZoom;
	import co.beek.map.markers.IMapMarker;
	import co.beek.map.markers.LocationMapMarker;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.service.LocationService;
	import co.beek.service.data.LocationList;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.ListEvent;
	
	import spark.components.List;
	import spark.events.IndexChangeEvent;
	
	public class LocationBrowser extends CloseablePanel
	{
		public static const LOCATION_SELECTED:String = "LOCATION_SELECTED";
		
		public static const SEARCH_DEFAULT_TEXT:String = "Type to search";
		
		public var _mapCenter:GeoZoom;
		
		public var mapHolder:MapHolder;
		
		protected var mapMarkers:Dictionary = new Dictionary;
		
		private var _locationData:LocationData;
		
		protected var locationsLoadTimeout:uint;
		
		[Bindable]
		protected var types:ArrayCollection = new ArrayCollection(Constants.typeNames);
		
		[Bindable]
		protected var listCollection:ArrayCollection = new ArrayCollection();
		
		private var loadedLocations:Array;
		
		[Bindable]
		protected var locationType:int;
		
		[Bindable]
		protected var searchText:String = SEARCH_DEFAULT_TEXT;
		
		public function LocationBrowser()
		{
			super();
			types[0] = "Show All";
		}
		
		public function set mapCenter(value:GeoZoom):void
		{
			_mapCenter = value;
			dispatchEvent(new Event("map_center_change"));
		}
		
		protected override function childrenCreated():void
		{
			super.childrenCreated();
			
			// timeout to wait for map to resize, load tiles
			locationsLoadTimeout = setTimeout(onLoadTimeout, 1000);
		}
		
		protected function onMapChange(event:Event):void
		{
			selectedLocation = null;
			
			clearTimeout(locationsLoadTimeout);
			
			locationsLoadTimeout = setTimeout(onLoadTimeout, 1000);
		}
		
		private function onLoadTimeout():void
		{
			loadLocations();
		}
		
		[Bindable (event="location_change")]
		public function get selectedLocation():LocationData
		{
			return _locationData;
		}
		
		public function set selectedLocation(value:LocationData):void
		{
			_locationData = value;
			
			if(_locationData)
				hilightLocationMarker();
			else
				hilightAllMarkers();
			
			dispatchEvent(new Event("location_change"));
		}
		
		private function hilightLocationMarker():void
		{
			for each(var marker:Object in mapMarkers)
				DisplayObject(marker).alpha = 0.5;
			
			if(_locationData)
			{
				var sel:LocationMapMarker = LocationMapMarker(mapMarkers[_locationData]);
				sel.alpha = 1;
				sel.parent.setChildIndex(sel, sel.parent.numChildren-1);
			}
		}

		protected function hilightAllMarkers():void
		{
			for each(var marker:Object in mapMarkers)
				DisplayObject(marker).alpha = 1;
		}
		
		[Bindable (event="map_center_change")]
		public function get mapCenter():GeoZoom
		{
			return _mapCenter;
		}
		
		protected function loadLocations():void
		{
			trace("loadLocations()");
			LocationService.getLocationsWithin(
				mapHolder.topLeft, 
				mapHolder.bottomRight, 
				onLocationsList
			);
		}
		
		protected function removeMapMarkers():void
		{
			for each(var marker:Object in mapMarkers)
				mapHolder.removeMarker(marker as IMapMarker);
			
			mapMarkers = new Dictionary;
			listCollection.removeAll();
		}
		
		private function onLocationsList(list:LocationList):void
		{
			
			
			// Ignore locations that don't have a default scene
			// these locations are buggy or imcomplete
			var array:Array = [];
			for(var i:int=0; i<list.locations.length; i++)
				if(list.locations[i].defaultScene)
					array.push(list.locations[i])
			
			array.sortOn("title");
			loadedLocations = array;
			
			addLocationMarkers(array);
		}
		
		protected function filterLocations(type:int):void
		{
			this.locationType = type;
			if(loadedLocations)
				addLocationMarkers(loadedLocations);
		}
		
		protected function searchLocations(search:String):void
		{
			this.searchText = search;
			if(loadedLocations)
				addLocationMarkers(loadedLocations);
		}
		
		
		private function addLocationMarkers(locations:Array):void
		{
			removeMapMarkers();
			
			var search:String = (searchText == null || searchText == "" || searchText == SEARCH_DEFAULT_TEXT)
				? null : searchText.toUpperCase();
			
			for(var i:int=0; i<locations.length; i++)
			{
				var location:LocationData = LocationData(locations[i]);
				if((locationType == 0 || location.type == locationType)
					&& (search == null || location.title.indexOf(search) > -1))
					addLocationMapMarker(location, String(i+1));
			}
		}
		
		private function vectorToArray(v:*):Array
		{
			var n:int = v.length; var a:Array = new Array();
			for(var i:int = 0; i < n; i++) 
				a[i] = v[i];
			return a;
		}
		
		private function addLocationMapMarker(location:LocationData, num:String):void
		{
			var marker:LocationMapMarker = new LocationMapMarker(location, num);
			marker.addEventListener(MouseEvent.CLICK, onLocationMarkerClick);
			mapHolder.addMarker(marker);
			
			// add the 
			listCollection.addItem(location);
			
			mapMarkers[location] = marker;
		}
		
		private function onLocationMarkerClick(event:MouseEvent):void
		{
			selectedLocation = LocationMapMarker(event.target).data;
		}
		
		protected function onListChange(event:IndexChangeEvent):void
		{
			selectedLocation = List(event.target).selectedItem as LocationData;
		}
		
		protected function onSelectClick():void
		{
			dispatchEvent(new LocationDataEvent(LOCATION_SELECTED, _locationData));
		}
	}
}