package co.beek.admin.scene.hotspot.video
{
	import co.beek.admin.poup.CloseablePanel;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.service.FileService;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class VideoPreselector extends CloseablePanel
	{
		public static const BROWSE_FOR_FLV:String = "BROWSE_FOR_FLV";
		public static const YOUTUBE_READY:String = "YOUTUBE_READY";
		
		private var _youtubeUrl:String;
		
		[Bindable]
		protected var processing:Boolean;
		
		public var videoData:VideoData;
		
		public function VideoPreselector()
		{
			super();
		}
		
		protected function onBrowseButtonClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(BROWSE_FOR_FLV));
		}
		
		[Bindable (event="urlChanged")]
		protected function get previewImageUrl():String
		{
			if(_youtubeUrl == null || videoId == null)
				return null;
			
			return "http://img.youtube.com/vi/"+videoId+"/default.jpg";
		}
		
		protected function set youtubeUrl(value:String):void
		{
			_youtubeUrl = value;
			dispatchEvent(new Event("urlChanged"))
		}
		
		[Bindable (event="urlChanged")]
		public function get videoId():String
		{
			if(!_youtubeUrl || _youtubeUrl == "")
				return null;
			
			var regExp:RegExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
			var match:Array = _youtubeUrl.match(regExp);
			if (match&&match[2].length==11)
				return match[2];
			
			return null;
		}
		
		
		protected function onDoneButtonClick(event:MouseEvent):void
		{
			var teamId:String = Model.guideScene 
				? Model.guideData.teamId
				: Model.currentScene.location.teamId;
			
			FileService.cacheYouTubeImage(teamId, videoId, onYouTubePreviewCached);
		}
		
		private function onYouTubePreviewCached(fileData:FileData):void
		{
			trace("onYouTubePreviewCached(fileData:FileData):void");
			videoData.file = fileData;
			dispatchEvent(new Event(YOUTUBE_READY));
		}
		
	}
}