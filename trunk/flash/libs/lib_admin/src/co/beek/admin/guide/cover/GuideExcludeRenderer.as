package co.beek.admin.guide.cover
{
	import co.beek.model.Model;
	
	import mx.controls.listClasses.IListItemRenderer;
	
	import spark.components.supportClasses.ItemRenderer;
	
	public class GuideExcludeRenderer extends ItemRenderer implements IListItemRenderer
	{
		public function GuideExcludeRenderer()
		{
			super();
		}
		
		public function get excluded():Boolean
		{
			return Model.isTypeExcluded(itemIndex);
		}
		
		public override function set data(value:Object):void
		{
			super.data = value;
		}
	}
}