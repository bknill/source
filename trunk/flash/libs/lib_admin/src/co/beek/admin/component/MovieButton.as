package co.beek.admin.component
{
	import mx.controls.Button;
	
	public class MovieButton extends Button
	{
		[Embed("/../images/movie.png")] 
		private var skin: Class; 
		
		public function MovieButton()
		{
			super();
			setStyle("upSkin", skin);
			setStyle("overSkin", skin);
			setStyle("downSkin", skin);
			
			width = height = 128;
		}
	}
}