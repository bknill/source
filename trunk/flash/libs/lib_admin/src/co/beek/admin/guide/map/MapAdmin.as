package co.beek.admin.guide.map
{
	import co.beek.model.Model;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	
	import spark.components.BorderContainer;
	
	public class MapAdmin extends VBox
	{
		[Bindable]
		protected var mapStyles:ArrayCollection = new ArrayCollection([
			47926, 47928, 37008, 54912, 25524, 25424
		]);
		
		public function MapAdmin()
		{
			super();
		}
		
		[Bindable (event="style_change")]
		public function get styleIndex():int
		{
			for(var i:int=0; i<mapStyles.length; i++)
				if(mapStyles[i] == Model.guideData.mapStyle)
					return i;
			return -1;
		}
		
		[Bindable (event="style_change")]
		public function get mapStyle():String
		{
			return Model.guideData.mapStyle;
		}
		
		public function set mapStyle(value:String):void
		{
			Model.guideData.mapStyle = value;
			dispatchEvent(new Event("style_change")); 
		}
	}
}