package co.beek.admin.guide.cover
{
	import co.beek.model.Constants;
	import co.beek.model.Model;
	
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	
	import spark.events.IndexChangeEvent;
	
	public class GuideExcludeAdmin extends Canvas
	{
		[Bindable]
		protected var typeNames:ArrayCollection = new ArrayCollection;
		
		public function GuideExcludeAdmin()
		{
			super();
			
			for(var i:int; i<Constants.typeNames.length; i++)
				typeNames.addItem(Constants.typeNames[i]);
		}
		
		[Bindable (event="excludes_change")]
		public function get selectedTypes():Vector.<int>
		{
			var types:Vector.<int> = new Vector.<int>;
			for(var i:int; i<Constants.typeNames.length; i++)
				if(Model.guideData.isExcluded(i))
					types.push(i);
			
			return types;
		}
		
		protected function onTileListChange(event:IndexChangeEvent):void
		{
			var exclude:int = event.newIndex;
			if(Model.guideData.isExcluded(exclude))
				Model.guideData.removeExclude(exclude);
				
			else
				Model.guideData.addExclude(exclude);
			
			setTimeout(dispatchChangeEvent, 100);
		}
		
		private function dispatchChangeEvent():void
		{
			dispatchEvent(new Event("excludes_change"));
		}
	}
}