package co.beek.admin.poup
{
	import co.beek.admin.scene.IconUtil;
	import co.beek.model.Constants;
	import co.beek.utils.ColorUtil;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import mx.core.MovieClipAsset;
	
	import spark.components.supportClasses.ItemRenderer;
	import spark.core.SpriteVisualElement;
	
	public class SceneTypeRenderer extends ItemRenderer
	{
		public var iconHolder:SpriteVisualElement;
		
		private var icons:AdminTypeIconsFl = new AdminTypeIconsFl;
		
		public function SceneTypeRenderer()
		{
			super();
		}
		
		public override function set data(value:Object):void
		{
			super.data = value;
			
		}
		
		public override function set itemIndex(value:int):void
		{
			super.itemIndex = value;
			
			var icons:Dictionary = Constants.getIconsDictionary(icons);
			if(icons[value])
			{
				ColorUtil.colorize(icons[value], 0x00a8e1);
				iconHolder.addChild(icons[value]);
			}
		}
	}
}