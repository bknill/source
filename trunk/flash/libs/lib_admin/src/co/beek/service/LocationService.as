package co.beek.service
{
	import co.beek.map.Geo;
	import co.beek.service.data.LocationList;
	import co.beek.service.data.SceneBasicList;

	public class LocationService
	{		
		
		public static function getScenesForLocation(locationId:String, callback:Function):void
		{
			var path:String = "location/{locationId}/scenes";
			path = path.replace("{locationId}", locationId);
			
			ServiceLoader.load(path, callback, SceneBasicList);
		}
		
		public static function getLocationsWithin(topLeft:Geo, bottomRight:Geo, callback:Function):void
		{
			var path:String = "locations/within/{minLat}/{minLon}/{maxLat}/{maxLon}/";
			path = path.replace("{minLat}", topLeft.lat);
			path = path.replace("{minLon}", topLeft.lon);
			path = path.replace("{maxLat}", bottomRight.lat);
			path = path.replace("{maxLon}", bottomRight.lon);
			
			ServiceLoader.load(path, callback, LocationList);
		}

		public static function getForDestination(destinationId:String, callback:Function):void
		{
			var path:String = "locations/destination/{destinationId}/";
			path = path.replace("{destinationId}", destinationId);

			ServiceLoader.load(path, callback, LocationList);
		}
	}
}