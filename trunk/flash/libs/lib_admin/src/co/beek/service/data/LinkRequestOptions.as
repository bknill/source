package co.beek.service.data
{
	import co.beek.service.ISerializable;

	public class LinkRequestOptions implements ISerializable
	{
		private var _locationId:String;
		
		private var _myLocations:LocationList;

		private var _theirGuides:GuideBasicList;
		
		public function LinkRequestOptions(data:Object)
		{
			_locationId = data['locationId']
			_myLocations = new LocationList(data['myLocations']);
			_theirGuides = new GuideBasicList(data['theirGuides']);
		}
		
		public function get locationId():String
		{
			return _locationId;
		}
		
		public function get myLocations():LocationList
		{
			return _myLocations;
		}
		
		public function get theirGuides():GuideBasicList
		{
			return _theirGuides;
		}
		
		public function get data():Object
		{
			var data:Object = {};
			data['locationId'] = _locationId;
			data['myLocations'] = _myLocations.data;
			data['theirGuides'] = _theirGuides.data;
			return data;
		}
	}
}