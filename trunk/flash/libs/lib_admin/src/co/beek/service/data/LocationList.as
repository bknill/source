package co.beek.service.data
{
	import co.beek.model.data.LocationData;
	import co.beek.service.ISerializable;

	public class LocationList implements ISerializable
	{
		private var _locations:Vector.<LocationData>;
		
		public function LocationList(data:Object)
		{
			_locations = new Vector.<LocationData>
			for(var i:int = 0; i<data.length; i++)
				_locations.push(new LocationData(data[i]));
		}
		
		public function get locations():Vector.<LocationData>
		{
			return _locations;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_locations.length; i++)
				data.push(_locations[i].data);
			
			return data;
		}
	}
}