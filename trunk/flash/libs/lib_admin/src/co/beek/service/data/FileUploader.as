package co.beek.service.data
{
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.utils.JSONBeek;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	flash.events.HTTPStatusEvent.HTTP_STATUS
	
	public class FileUploader extends EventDispatcher
	{
		private var _fileData:FileData;
		
		public function FileUploader()
		{
			super();
		}
		
		public function upload(filename:String, bytes:ByteArray, video:Boolean = false):void
		{
			//put :9081 in to run locally
			//if(!video)
			var url:String = Model.config.serviceUrl+"/files/uploadBytes/"+filename+"/;jsessionid="+Model.sessionData.sessionId;
			//else
			//var url:String = Model.config.serviceUrl+"/files/uploadVideo/"+filename+"/;jsessionid="+Model.sessionData.sessionId;

			var header:URLRequestHeader = new URLRequestHeader ("Content-type", "application/octet-stream");
			var request:URLRequest = new URLRequest(url);
			request.requestHeaders.push (header);
			request.method = URLRequestMethod.POST;
			request.data = bytes;
			
			var sendLoader:URLLoader = new URLLoader();
			sendLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			sendLoader.addEventListener(Event.COMPLETE, onUploadComplete);
			sendLoader.addEventListener(ProgressEvent.PROGRESS, onUploadProgress);
			sendLoader.load(request);
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace(event.text);
			throw new Error("Error uploading file. "+event.text);
		}

		private function onUploadProgress(event:ProgressEvent):void
		{
			dispatchEvent(event);
		}
		
		private function onUploadComplete(event:Event):void
		{
			var loader:URLLoader = URLLoader(event.target);
			var data:Object;
			try
			{
				data = JSONBeek.decode(loader.data)
			}
			catch(e:Error)
			{
				throw new Error("Error decoding JSON for file:"+loader.data)
			}
			
			if(data.error)
				throw new Error(data.message);
			
			// set the returned file on the photo
			_fileData = new FileData(data.payload);
			
			dispatchEvent(new Event(Event.COMPLETE))
		}
		
		public function get fileData():FileData
		{
			return _fileData;
		}
	}
}