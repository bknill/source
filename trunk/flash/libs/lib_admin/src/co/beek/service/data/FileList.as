package co.beek.service.data
{
	import co.beek.model.data.FileData;
	import co.beek.service.ISerializable;

	public class FileList implements ISerializable
	{
		private var _files:Vector.<FileData>;
		
		public function FileList(data:Object)
		{
			_files = new Vector.<FileData>
			for(var i:int = 0; i<data.length; i++)
				_files.push(new FileData(data[i]));
		}
		
		public function get files():Vector.<FileData>
		{
			return _files;
		}
		
		public function get data():Object
		{
			var data:Array = [];
			for(var i:int = 0; i <_files.length; i++)
				data.push(_files[i].data);
			
			return data;
		}
	}
}