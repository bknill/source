package co.beek.service
{
	import flash.net.URLVariables;
	
	import co.beek.service.data.LinkRequestOptions;

	public class TeamService
	{
		public static function getLinkRequestOptions(locationId:String, callback:Function):void
		{
			var path:String = "location/{locationId}/linkOptions";
			path = path.replace("{locationId}", locationId);
			
			ServiceLoader.load(path, callback, LinkRequestOptions);
		}

		public static function sendLinkRequest(myGuideId:String, yourLocationId:String,yourGuideId:String, myLocationId:String, message:String, callback:Function):void
		{
			var path:String = "guide/{myGuideId}/linked/{yourLocationId}"
				+"/guide/{yourGuideId}/linkrequest/{myLocationId}";
			path = path.replace("{myGuideId}", myGuideId);
			path = path.replace("{yourLocationId}", yourLocationId);
			path = path.replace("{yourGuideId}", yourGuideId);
			path = path.replace("{myLocationId}", myLocationId);
			
			var variables:URLVariables = new URLVariables();  
			variables['message'] = message;
			
			ServiceLoader.post(path, variables, callback); 
		}
	}
}