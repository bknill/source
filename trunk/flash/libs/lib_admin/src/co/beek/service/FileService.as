package co.beek.service
{
	import co.beek.model.data.FileData;
	import co.beek.service.data.FileList;
	
	import mx.graphics.codec.JPEGEncoder;

	public class FileService
	{
		private static var encoder:JPEGEncoder = new JPEGEncoder(90);
		
		public static function getPublicFiles(callback:Function):void
		{
			ServiceLoader.load("files/public/", callback, FileList);
		}

		public static function cacheYouTubeImage(teamId:String, videoId:String, callback:Function):void
		{
			var path:String = "cacheYouTubeImage/{teamId}/{videoId}";
			path = path.replace("{teamId}", teamId);
			path = path.replace("{videoId}", videoId);
			ServiceLoader.load(path, callback, FileData);
		}
	}
}