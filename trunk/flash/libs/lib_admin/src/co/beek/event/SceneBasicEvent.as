package co.beek.event
{
	import co.beek.model.data.SceneBasicData;
	
	import flash.events.Event;
	
	public class SceneBasicEvent extends Event
	{
		private var _scene:SceneBasicData;
		
		public function SceneBasicEvent(type:String, scene:SceneBasicData)
		{
			super(type);
			_scene = scene;
		}
		
		public function get scene():SceneBasicData
		{
			return _scene;
		}
	}
}