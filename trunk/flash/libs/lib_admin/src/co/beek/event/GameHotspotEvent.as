package co.beek.event
{
	import co.beek.model.guide.GameTaskHotspot;
	
	import flash.events.Event;
	
	public class GameHotspotEvent extends Event
	{
		private var _gameHotspot:GameTaskHotspot;
		
		public function GameHotspotEvent(type:String, gameHotspot:GameTaskHotspot)
		{
			super(type, true, true);
			_gameHotspot = gameHotspot;
		}
		
		public function get gameHotspot():GameTaskHotspot
		{
			return _gameHotspot;
		}
	}
}