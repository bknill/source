package co.beek.event
{
	import co.beek.model.data.LocationData;
	
	import flash.events.Event;
	
	public class LocationDataEvent extends Event
	{
		private var _location:LocationData;
		
		public function LocationDataEvent(type:String, location:LocationData)
		{
			super(type);
			_location = location;
		}
		
		public function get location():LocationData
		{
			return _location;
		}
	}
}