package com.jerry.components
{
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.core.IDataRenderer;
	
	import spark.components.supportClasses.SliderBase;
	
	/**
	 * 
	 * @author Jeremiah
	 */
	public class CircularSlider extends SliderBase
	{
		/**
		 * 
		 */
		public function CircularSlider()
		{
			super();
		}
		
		private var _thumbHeight:Number = 11;
		/**
		 * 
		 * @return 
		 */
		public function get thumbHeight():Number { return _thumbHeight; }
		/**
		 * 
		 * @param value
		 */
		public function set thumbHeight(value:Number):void { _thumbHeight = value; }
		
		private var _thumbWidth:Number = 11;
		/**
		 * 
		 * @return 
		 */
		public function get thumbWidth():Number { return _thumbWidth; }
		/**
		 * 
		 * @param value
		 */
		public function set thumbWidth(value:Number):void { _thumbWidth = value; }
		
		/**
		 *  @private
		 */
		override protected function pointToValue(x:Number, y:Number):Number
		{
			if (!thumb || !track)
				return 0;
			
			// find the center of the ellipse
			var center:Point = new Point((track.getLayoutBoundsWidth() - thumb.getLayoutBoundsWidth()) / 2, (track.getLayoutBoundsHeight() - thumb.getLayoutBoundsHeight()) / 2);
			
			// by default we want to begin at the top so we need to find the top of the ellipse
			var top:Point = new Point(((track.getLayoutBoundsWidth() - thumb.getLayoutBoundsWidth()) / 2) - center.x, -center.y);			
			
			// adjust the x,y coordinates
			var point:Point = new Point(x - center.x, y - center.y);	
			
			var m:Matrix = new Matrix();
			
			// check if the ellipse is an oval
			if (track.getLayoutBoundsHeight() < track.getLayoutBoundsWidth()) {
				m.scale(track.getLayoutBoundsHeight() / track.getLayoutBoundsWidth(), 1);
			} else if (track.getLayoutBoundsWidth() < track.getLayoutBoundsHeight()) {
				m.scale(1, track.getLayoutBoundsWidth() / track.getLayoutBoundsHeight());
			}
			
			// calculate the adjusted point if there is any scaling
			point = m.transformPoint(point);
			
			// calculate the angle between the top of the circle and where the mouse is
			var degrees:Number = (Math.atan2(top.x * point.y - top.y * point.x, top.x * point.x + top.y * point.y)) * (180 / Math.PI);
			
			// adjust the value to account for what is set as the maximum and minimum
			return (((degrees < 0) ? 360 + degrees : degrees) / (360 / (maximum - minimum))) + minimum;
		}
		
		/**
		 *  @private
		 */
		override protected function updateSkinDisplayList():void
		{
			if (!thumb || !track)
				return;
			
			// adjust the pending value to account for maximum and minimum
			var pend:Number = ((360 * (pendingValue - minimum)) / (maximum - minimum));
			
			// make sure the thumb is the correct size
			thumb.height = _thumbHeight;
			thumb.width = _thumbWidth;
			
			// find the center of the ellipse
			var center:Point = new Point(track.getLayoutBoundsWidth() / 2, track.getLayoutBoundsHeight() / 2);
			
			// set up a matrix to transform the top of the ellipse based on the angle of rotation
			var m:Matrix = new Matrix();
			m.translate(-center.x, -center.y);
			m.rotate(pend * (Math.PI / 180));
			m.translate(center.x, center.y);
			
			// find the top of the ellipse
			var top:Point = new Point((track.getLayoutBoundsWidth() / 2), 0);
			
			// now transform the point based on the rotation
			var p:Point = m.transformPoint(new Point(top.x, top.y));
			
			// if the ellipse is more of an oval then we need to further adjust the point
			if (track.getLayoutBoundsWidth() > track.getLayoutBoundsHeight() || track.getLayoutBoundsHeight() > track.getLayoutBoundsWidth()) {
				
				m = new Matrix();
				m.translate(-center.x, -center.y);
				m.scale(track.getLayoutBoundsWidth() / track.getLayoutBoundsHeight(), 1);
				m.translate(center.x, center.y);
				
				p = m.transformPoint(p);
			}
			
			// convert to parent's coordinates.
			var thumbPos:Point = track.localToGlobal(p);
			var thumbPosParentX:Number = thumb.parent.globalToLocal(thumbPos).x - (thumb.getLayoutBoundsWidth() / 2);
			var thumbPosParentY:Number = thumb.parent.globalToLocal(thumbPos).y - (thumb.getLayoutBoundsHeight() / 2);
			
			thumb.setLayoutBoundsPosition(Math.round(thumbPosParentX), Math.round(thumbPosParentY));
		}
		
		/**
		 *  @private
		 */
		override protected function updateDataTip(dataTipInstance:IDataRenderer, initialPosition:Point):void
		{
			var tipAsDisplayObject:DisplayObject = dataTipInstance as DisplayObject;
			
			if (tipAsDisplayObject && thumb)
			{
				const tipWidth:Number = tipAsDisplayObject.width;
				var relX:Number = thumb.getLayoutBoundsX() - (tipWidth - thumb.getLayoutBoundsWidth()) / 2;
				
				// If this component's coordinate system is RTL (x increases to the right), then
				// getLayoutBoundsX() returns the right edge, not the left.
				//if (layoutDirection == LayoutDirection.RTL)
					relX += tipAsDisplayObject.width;
				
				var o:Point = new Point(relX, initialPosition.y);
				var r:Point = thumb.parent.localToGlobal(o);  
				
				// Get the screen bounds
				var screenBounds:Rectangle = systemManager.getVisibleApplicationRect();
				// Get the tips bounds. We only care about the dimensions.
				var tipBounds:Rectangle = tipAsDisplayObject.getBounds(tipAsDisplayObject.parent);
				
				// Make sure the tip doesn't exceed the bounds of the screen
				r.x = Math.floor( Math.max(screenBounds.left, 
					Math.min(screenBounds.right - tipBounds.width, r.x)));
				r.y = Math.floor( Math.max(screenBounds.top, 
					Math.min(screenBounds.bottom - tipBounds.height, r.y)));
				
				r = tipAsDisplayObject.parent.globalToLocal(r);
				
				tipAsDisplayObject.x = r.x;
				tipAsDisplayObject.y = r.y;
			}
		}
	}
}