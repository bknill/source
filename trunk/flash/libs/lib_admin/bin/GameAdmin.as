package co.beek.admin.game
{
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	
	import co.beek.admin.Admin;
	import co.beek.admin.component.ConfirmPanel;
	import co.beek.admin.component.ConfirmPanelMX;
	import co.beek.event.GameHotspotEvent;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GameTaskData;
	import co.beek.model.guide.GameTaskHotspot;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.BubbleData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.model.hotspots.PostData;
	import co.beek.model.hotspots.PosterData;
	import co.beek.model.hotspots.RssReaderData;
	import co.beek.model.hotspots.SoundData;
	import co.beek.model.hotspots.VideoData;
	
	public class GameAdmin extends Canvas
	{
		private var currentScene:SceneData;
		private var currentGuideScene:GuideSceneData;

		[Bindable]
		protected var settingsVisible:Boolean;
		
		[Bindable]
		protected var adminVisible:Boolean;
		
		[Bindable]
		protected var codeNamesCollection:ArrayCollection = new ArrayCollection([
			{code:null, label:"Select Task Type"},
			//{code:Constants.GAME_GUIDE_NEXT, label:"Guide Next"},
			//{code:Constants.GAME_PANO_DRAGGED, label:"Drag Pano"},
			{code:GameTaskData.GAME_FIND_HOTSPOTS, label:"Find Hotspots"}
		]);
		
		private var codeNamesDictionary:Dictionary = new Dictionary;
		
		
		/**
		 * A collection of HotspotData representing the hotspots in the scene 
		 * to be found, that are not in the hotspotsToFind array from the task
		 */
		[Bindable]
		protected var sceneHotspotsToFind:ArrayCollection = new ArrayCollection();

		/**
		 * A collection of GameTaskHotspot representing the hotspots to find from this task
		 */
		[Bindable]
		protected var taskHotspotsToFind:ArrayCollection = new ArrayCollection();

		[Bindable]
		protected var sceneHotspotsToShow:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		protected var taskHotspotsToShow:ArrayCollection = new ArrayCollection();
		
		private var allHotspots:Vector.<HotspotData>;
		
		public function GameAdmin()
		{
			super();
			
			codeNamesDictionary[Constants.GAME_GUIDE_NEXT] = "Guide Next";
			codeNamesDictionary[Constants.GAME_PANO_DRAGGED] = "Drag Pano";
			codeNamesDictionary[Constants.GAME_FIND_HOTSPOTS] = "Find Hotspots";
			
			Admin.instance.addEventListener(Admin.PLAYER_LOADED, onPlayerLoaded);
		}
		
		private function onPlayerLoaded(event:Event):void
		{
			updateToScene();
			
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.addEventListener(Model.GAME_TASK_CHANGED, onGameTaskChanged);
			Model.addEventListener(Model.GAME_TASK_CHANGED_INSTANT, onGameTaskChanged);
			
			Model.state.addEventListener(State.UI_STATE_CHANGE, onEditingSceneChange);
			
			dispatchEvent(new Event("guide_changed"))
		}
		
		[Bindable (event="taskIndexChanged")]
		protected function get canAddTask():Boolean
		{
			return ((currentTask && currentTask.code != null) || !currentTask );
		}

		private function onEditingSceneChange(event:Event):void
		{
			visible = Model.state.uiState == State.UI_STATE_EDITING_GAME
				|| Model.state.uiState == State.UI_STATE_NORMAL;
		}
		
		protected function toggleEditingSettings():void
		{
			settingsVisible = !settingsVisible;
			adminVisible = false;
			//Model.state.editingGame = adminVisible;
		}
		
		protected function toggleEditingGame():void
		{
			adminVisible = !adminVisible;
			settingsVisible = false;
			Model.state.uiState = adminVisible ? State.UI_STATE_EDITING_GAME : State.UI_STATE_NORMAL;
		}
		
		[Bindable(event="guide_changed")]
		protected function get gameLocked():Boolean
		{
			return Model.guideData.gameLocked;
		}

		protected function set gameLocked(value:Boolean):void
		{
			Model.guideData.gameLocked = value;
		}

		[Bindable(event="guide_changed")]
		protected function get gameCode():String
		{
			return Model.guideData.gameCode;
		}

		protected function set gameCode(value:String):void
		{
			Model.guideData.gameCode = value;
		}
		
		private function onSceneChange(event:Event):void
		{
			updateToScene();
		}
		
		private function updateToScene():void
		{
			if(currentScene)
				currentScene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			
			if(currentGuideScene)
				currentGuideScene.removeEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			
			currentScene = Model.currentScene;
			currentGuideScene = Model.guideScene;
			
			allHotspots = Model.hotspotsMerged;
			if(currentTask)
			{
				updateHotspotsToFind();
				updateHotspotsToShow();
			}
			
			if(currentScene)
				currentScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
			
			if(currentGuideScene)
				currentGuideScene.addEventListener(HotspotEvent.HOTSPOT_ADDED, onHotspotAdded);
		}

		private function onHotspotAdded(event:Event):void
		{
			allHotspots = Model.hotspotsMerged;
			if(currentTask)
			{
				updateHotspotsToFind();
				updateHotspotsToShow();
			}
		}
		
		private function onGameTaskChanged(event:Event):void
		{
			if(currentTask)
			{
				updateHotspotsToFind();
				updateHotspotsToShow();
			}
			dispatchEvent(new Event("taskIndexChanged"))
		}
		
		private function updateHotspotsToFind():void
		{
			sceneHotspotsToFind.removeAll();
			sceneHotspotsToFind.addItem({"title" : "Select a hotspot to find"});
			for(var i:int; i<allHotspots.length; i++)
			{
				if(allHotspots[i] is PostData || allHotspots[i] is SoundData)
					continue;
				
				if(!currentTask.hasHotspotToShow(allHotspots[i])
					&& !currentTask.hasHotspotToFind(allHotspots[i]))
					sceneHotspotsToFind.addItem(allHotspots[i]);
			}
			
			taskHotspotsToFind = new ArrayCollection(currentTask.hotspotsToFind);
		}

		private function updateHotspotsToShow():void
		{
			sceneHotspotsToShow.removeAll();
			sceneHotspotsToShow.addItem({"title" : "Select a hotspot to show"});
			for(var i:int; i<allHotspots.length; i++)
			{
				if(!currentTask.hasHotspotToShow(allHotspots[i])
					&& !currentTask.hasHotspotToFind(allHotspots[i]))
					sceneHotspotsToShow.addItem(allHotspots[i]);
			}
			
			taskHotspotsToShow = new ArrayCollection(currentTask.hotspotsToShow);
		}
		
		protected function addHotspotToFind(combo:ComboBox):void
		{
			var hotspot:HotspotData = combo.selectedItem as HotspotData; 
				
			if(hotspot && hotspot.id && currentTask && !currentTask.hasHotspotToFind(hotspot))
				currentTask.addHotspotToFind(GameTaskHotspot.create(hotspot, Model.currentScene));
			
			updateHotspotsToFind();
			updateHotspotsToShow();
			
			// revert combo back to instructions
			combo.selectedIndex = 0;
		}
		
		[Bindable(event="taskIndexChanged")]
		public function get taskCount():int
		{
			return Model.guideData.gameTasks.length;
		}
		
		[Bindable(event="taskIndexChanged")]
		public function get currentTaskIndex():int
		{
			return Model.currentTaskIndex;
		}
		
		public function set currentTaskIndex(value:int):void
		{
			Model.currentTaskIndexInstant = value;
		}

		public function set currentTaskCode(value:String):void
		{
			Model.currentTask.code = value;
			dispatchEvent(new Event("taskIndexChanged"))
		}
		
		[Bindable(event="taskIndexChanged")]
		protected function get currentTask():GameTaskData
		{
			return Model.currentTask;
		}
		
		[Bindable(event="taskIndexChanged")]
		protected function get currentTaskType():String
		{
			return codeNamesDictionary[currentTask.code];
		}
		
		[Bindable(event="taskIndexChanged")]
		protected function get hasPrev():Boolean
		{
			if(adminVisible)
				return currentTaskIndex >= 0;
			
			return taskCount > 0 && currentTaskIndex >= taskCount;
		}

		protected function goPrev():void
		{
			currentTaskIndex --;
			settingsVisible = currentTaskIndex == -1;
		}
		
		[Bindable(event="taskIndexChanged")]
		protected function get hasNext():Boolean
		{
			return currentTaskIndex > -1 && currentTaskIndex <  Model.guideData.gameTasks.length - 1;
		}
		
		protected function goNext():void
		{
			if(Model.guideData.gameTasks.length == 0)
				newGame();
			settingsVisible = false;
			adminVisible = true;
			currentTaskIndex ++;
		}
		
		protected function onRemoveHotspotToFind(event:GameHotspotEvent):void
		{
			currentTask.removeHotspotToFind(event.gameHotspot);
			
			updateHotspotsToFind();
			updateHotspotsToShow();
		}

		protected function onRemoveHotspotToShow(event:GameHotspotEvent):void
		{
			currentTask.removeHotspotToShow(event.gameHotspot);
			
			updateHotspotsToFind();
			updateHotspotsToShow();
		}
		
		protected function addHotspotToShow(combo:ComboBox):void
		{
			var hotspot:HotspotData = combo.selectedItem as HotspotData; 
			
			if(hotspot && hotspot.id && currentTask && !currentTask.hasHotspotToShow(hotspot))
				currentTask.addHotspotToShow(GameTaskHotspot.create(hotspot, Model.currentScene));
			
			updateHotspotsToFind();
			updateHotspotsToShow();
			
			// revert combo back to instructions
			combo.selectedIndex = 0;
		}
		
		protected function newGame():void
		{
			var task1:GameTaskData = GameTaskData.create(
				Model.guideData.id, Constants.GAME_PANO_DRAGGED
			);
			task1.instructions = "Welcome to the "+Model.guideData.title+" game. " +
				"Your first task is to drag the pano with your finger or mouse.";
			task1.feedback = "Congratulations, this is how you can move around the scene";
			Model.guideData.gameTasks.push(task1);

			// only add this task if the guide is not locked
			if(!gameLocked)
			{
				var task2:GameTaskData = GameTaskData.create(
					Model.guideData.id, Constants.GAME_GUIDE_NEXT
				);
				task2.instructions = "Notice the book in the bottom left? " +
					"This is the "+Model.guideData.title+" guide, and " +
					"can help you explore more scenes. " +
					"Click the next button to see whats next";
				task2.feedback = "Good work, you have moved to a new " +
					"page in the guide, and are probably going to a new scene";
				Model.guideData.gameTasks.push(task2);
			}
		}
		
		protected function newTask():void
		{
			if(taskCount == 0)
			{
				if(settingsVisible)
				{
					newGame();
					currentTaskIndex = taskCount-1;
					adminVisible = true;
					settingsVisible = false;
				}
				else
				{
					adminVisible = false;
					settingsVisible = true;
				}
			}
			else
			{
				adminVisible = true;
				settingsVisible = false;
				Model.guideData.addGameTask(GameTaskData.create(
					Model.guideData.id, null
				), currentTaskIndex+1);
				currentTaskIndex ++;
			}
		}

		protected function removeTask():void
		{
			var confirm:ConfirmPanel = new ConfirmPanelMX();
			confirm.confirmText = "Are you sure you want to remove the task from the game?";
			confirm.addEventListener(ConfirmPanel.CONFIRMED_YES, onConfirmYes);
			confirm.addEventListener(ConfirmPanel.CONFIRMED_NO, onConfirmNo);
			PopUpManager.addPopUp(confirm, Admin.instance, true);
			PopUpManager.centerPopUp(confirm);
		}
		
		private function onConfirmYes(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
			Model.removeCurrentTask();
		}
		
		private function onConfirmNo(event:Event):void
		{
			PopUpManager.removePopUp(event.target as ConfirmPanel);
		}
	}
}