package co.beek.map.markers
{
	import co.beek.map.Geo;
	import co.beek.model.data.LocationData;

	public class LocationMapMarker extends LocationMapMarkerFl implements IMapMarker
	{
		private var _data:LocationData;
		
		public function LocationMapMarker(data:LocationData, num:String)
		{
			super();
			mouseChildren = false;
			
			_data = data;
			
			field.text = num;
		}
		
		public function get geo():Geo
		{
			return _data.geo;
		}
		
		public function get data():LocationData
		{
			return _data;
		}
	}
}