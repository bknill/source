package co.beek.map
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.utils.Dictionary;
	
	import caurina.transitions.Tweener;
	
	import co.beek.map.markers.IMapMarker;
	import co.beek.map.markers.LocationMapMarker;
	import co.beek.map.markers.SceneMapMarker;
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.model.data.SceneBasicData;
	
	public class Map extends Sprite
	{
		private var _model:Model;
		private var _data:MapData;
		
		private var loaders:Dictionary = new Dictionary;
		
		private var tiles:Sprite = new Sprite;
		private var prevTilesHolder:Sprite;
		//private var lines:Sprite = new Sprite;
		
		private var downPos:Point;
		
		public function Map()
		{
			super();
			mouseChildren = false;
			mask = new Sprite;
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMapMouseDown);
			addEventListener(MouseEvent.MOUSE_MOVE, onMapMouseMove);
			addEventListener(MouseEvent.MOUSE_UP, onMapMouseUp);
			addEventListener(MouseEvent.MOUSE_WHEEL, onMapMouseWheel);
			
			addChild(tiles);
			//addChild(lines);
			addChild(mask);
		}
		
		public override function get width():Number
		{
			return _data.width;
		}
		
		public override function get height():Number
		{
			return _data.height;
		}
		
		public function set model(value:Model):void
		{
			_model = value;
		}
		
		private var prevZoom:int;
		public function set data(value:MapData):void
		{
			if(_data)
			{
				_data.removeEventListener(MapData.ZOOM, onMapDataChange);
				_data.removeEventListener(MapData.PAN, onMapDataChange);
				_data.removeEventListener(MapData.CHANGE, onMapDataChange);
			}
			
			_data = value;
			_data.addEventListener(MapData.ZOOM, onMapDataChange);
			_data.addEventListener(MapData.PAN, onMapDataChange);
			_data.addEventListener(MapData.CHANGE, onMapDataChange);
			
			//prevZoom = _data.zoom;
			if(_data.center)
				buildMap();
		}
		
		public function get data():MapData
		{
			return _data;
		}
		
		private function onMapDataChange(event:Event):void
		{
			buildMap();
		}
		
		private var _prevScale:Number;
		
		public function set prevScale(value:Number):void
		{
			_prevScale = value;
			
			prevTilesHolder.scaleX = prevTilesHolder.scaleY = _prevScale;
			prevTilesHolder.x = (_data.width - _data.width*_prevScale)/2;
			prevTilesHolder.y = (_data.height - _data.height*_prevScale)/2;
		}

		public function get prevScale():Number
		{
			return _prevScale;
		}

		private function onTweenComplete():void
		{
			
			tiles.visible = true;
		}
		
		private function removeLoaders():void
		{
			for(var i:int=0; i<loaders.length; i++)
				removeChild(loaders[i]);
			//loaders = new Vector.<Loader>;
			
			//lines.graphics.clear();
		}
		
		private function buildMap():void
		{
			//removeLoaders();
			
			if(!_data)
				return;
			
			Sprite(mask).graphics.lineStyle(1, 0xFF00CC);
			Sprite(mask).graphics.beginFill(0xFF00CC);
			Sprite(mask).graphics.drawRect(0, 0, _data.width, _data.height);
			
			//lines.graphics.beginFill(0x666666);
			//lines.graphics.drawCircle(_data.width/2, _data.height/2, 2);
			//lines.graphics.endFill();
			
			graphics.clear();
			graphics.beginFill(0xa6c9e2);
			graphics.drawRect(0, 0, _data.width, _data.height);
			

			var tiles:Vector.<Tile> = _data.tiles;
			for(var tileString:String in loaders)
				if(!tileInList(tileString, tiles))
					removeLoader(loaders[tileString]);
			
			for(var i:int = 0; i<tiles.length; i++)
				loadTile(tiles[i]);
			
		}
		

		
		private function loadTile(tile:Tile):void
		{
			if(loaders[tile.toString(data.style)] == null)
				loaders[tile.toString(data.style)] = createLoader(tile);
			
			var loader:Loader = Loader(loaders[tile.toString(data.style)]);
			
			var pos:Point = _data.getPos(tile);
			loader.x = pos.x;
			loader.y = pos.y;
			
			tiles.addChild(loader);
			
			graphics.lineStyle(1, 0xCCCCCCC);
			graphics.drawRect(pos.x, pos.y, MapData.TILE_SIZE, MapData.TILE_SIZE);
		}
		
		private function createLoader(tile:Tile):Loader
		{
			var url:String = _data.server + "/" + _data.getPath(tile);
			//var url:String = Model.config.getMapTileUrl(path);
			//	: tile.getServer(_data.style)+"/"+path; 
			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onTileIOError);
			loader.load(new URLRequest(url), new LoaderContext(true));
			
			return loader;
		}
		
		private function onTileIOError(event:IOErrorEvent):void
		{
			trace("tile load failed: "+event.text);
		}
		
		private function tileInList(tileString:String, tiles:Vector.<Tile>):Boolean
		{
			for(var i:int = 0; i<tiles.length; i++)
				if(tiles[i].toString(data.style) == tileString)
					return true;
			
			return false;
		}
		
		private function removeLoader(loader:Loader):void
		{
			if(loader.parent == tiles)
				tiles.removeChild(loader);
		}
		
		private function onMapMouseDown(event:MouseEvent):void
		{
			downPos = new Point(event.stageX, event.stageY);
		}
		
		private function onMapMouseMove(event:MouseEvent):void
		{
			if(downPos == null || !event.buttonDown)
				return;
			
			var delta:Point = new Point(
				downPos.x - event.stageX, 
				downPos.y - event.stageY
			);
			data.offsetFromCenter(delta);
			
			downPos = new Point(event.stageX, event.stageY);
			positionMarkers();
		}
		
		private function onMapMouseUp(event:MouseEvent):void
		{
			downPos = null;
			//resetMap();
			dispatchEvent(new Event("centerChange"));
		}
		
		private function onMapMouseWheel(event:MouseEvent):void
		{
			data.zoom += int(event.delta/3);
			positionMarkers();
		}
		
		protected function positionMarkers():void
		{
			/*
			for(var i:int = 0; i<markers.length; i++)
			{
				var marker:IMapMarker = markers[i];
				var pos:Point = mapData.posForGeo(marker.geo);
				marker.x = pos.x;
				marker.y = pos.y;
			}
			*/
		}
	}
}