package co.beek.map.markers
{
	import co.beek.map.Geo;
	
	import flash.events.IEventDispatcher;

	public interface IMapMarker extends IEventDispatcher
	{
		function get geo():Geo;
		
		function set x(value:Number):void;
		function set y(value:Number):void;
	}
}