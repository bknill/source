package co.beek.map.markers
{
	import co.beek.Fonts;
	import co.beek.loading.ImageLoader;
	import co.beek.map.Geo;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.LocationData;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.StringUtil;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;

	/**
	 * 
	 * @author Daniel
	 * 
	 */
	public class LocationMapMarkerSelected extends LocationMapMarkerSelectedFl
	{
		public static const CLOSE:String = "CLOSE";
		public static const GO:String = "GO";
		private var logoHolder:Bitmap = new Bitmap();
		
		private var _location:LocationData;
		
		public function LocationMapMarkerSelected(showClose:Boolean)
		{
			super();
			//Fonts.setFormat(titleField, Fonts.titleFormatSmall);
			//Fonts.setFormat(goThereButton.field, Fonts.titleFormatSmall);
			//Fonts.setFormat(descriptionField, Fonts.bodyFormat);
			//Fonts.setFormat(distanceField, Fonts.bodyFormatSmall);
			//Fonts.setFormat(timeField, Fonts.bodyFormatSmall);
			
			goThereButton.field.text = "GO";
			
			logoHolder.x = logoMask.x;
			logoHolder.y = logoMask.y;
			logoHolder.mask = logoMask;
			addChild(logoHolder);
			
			closeButton.visible = showClose;
			goThereButton.visible = !showClose;
			
			closeButton.addEventListener(MouseEvent.CLICK, onCloseClick);
			goThereButton.addEventListener(MouseEvent.CLICK, onGoClick);
		}
		
		private function onCloseClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(CLOSE));
		}

		private function onGoClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(GO));
		}
		
		public function get location():LocationData
		{
			return _location;
		}
		
		public function set location(value:LocationData):void
		{
			_location = value;
			
			titleField.text = value.title;
			titleField.textColor = Model.themeColor;
			StringUtil.truncateTextField(titleField);
			
			descriptionField.text = value.description;
			StringUtil.truncateContent(descriptionField);
			
			showCorrectIcon(value.type);
			
			ColorUtil.colorize(goThereButton, Model.themeColor);
			ColorUtil.colorize(border, Model.themeColor);
			ColorUtil.colorize(icons, Model.themeColor);
			
			if(Model.guideData && Model.guideData.firstScene)
			{
				var home:Geo = Model.guideData.firstScene.scene.geo;
				var distance:Number = value.geo.distance(home);
				var time:int = distance/4 * 60;
				distanceField.text = "Distance: " + Math.round(distance*10)/10 + " k";
				timeField.text = "Time: " + time + " mins";
			}
			
			logoHolder.bitmapData = null;
			//if(value.defaultScene)
				ImageLoader.load(
					Model.config.getAssetUrl(value.defaultScene.thumbName), 
					logoLoadedCallback, 
					logoLoadedError
				);

		}
		
		
		
		private function showCorrectIcon(type:int):void
		{
			var icons:Dictionary = Constants.getIconsDictionary(icons);
			for(var i:String in icons)
				if(icons[i])
					MovieClip(icons[i]).visible = false;
			
			if(icons[type])
				MovieClip(icons[type]).visible = true;
		}
		
		private function logoLoadedCallback(data:BitmapData):void
		{
			logoHolder.bitmapData = data;
			logoHolder.smoothing = true;
			logoHolder.scaleX = logoHolder.scaleY = Math.min(
				logoMask.width/data.width, 
				logoMask.height/data.height
			);
			logoHolder.x = int(logoMask.x + 
				(logoMask.width - logoHolder.width)/2);
		}
		
		private function logoLoadedError():void
		{
			// TODO make this do something useful
			trace("logoLoadedError");
		}
	}
}