package co.beek.map
{
	import flash.display.Sprite;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	
	public class Marker extends Sprite
	{
		private var _pos:Vector3D;
		
		public function Marker()
		{
			super();
		}
		
		public override function set x(value:Number):void
		{
			super.x = value;
			
			_pos = new Vector3D(x,y,0);
		}
		
		public override function set y(value:Number):void
		{
			super.y = value;
			
			_pos = new Vector3D(x,y,0);
		}
		
		public function translateBy(matrix3D:Matrix3D):void
		{
			if(!transform.matrix3D)
				z = 0;
			
			if(matrix3D)
				transform.matrix3D.position = matrix3D.transformVector(_pos);
			
			else
				transform.matrix3D.position = _pos;
		}
		
		public function dispose():void
		{
			
		}
	}
}