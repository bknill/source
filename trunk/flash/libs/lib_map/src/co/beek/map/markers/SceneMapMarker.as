package co.beek.map.markers
{
	import co.beek.map.Geo;
	import co.beek.model.data.SceneBasicData;

	public class SceneMapMarker extends SceneMapMarkerFl implements IMapMarker
	{
		private var _data:SceneBasicData;
		
		public function SceneMapMarker(data:SceneBasicData, num:String)
		{
			super();
			_data = data;
			field.text = num;			
			mouseChildren = false;
		}
		
		public function get geo():Geo
		{
			return _data.geo;
		}
		
		public function get data():SceneBasicData
		{
			return _data;
		}
	}
}