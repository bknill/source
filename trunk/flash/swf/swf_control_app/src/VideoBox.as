package
{
	import flash.display.BitmapData;
	import flash.display3D.textures.VideoTexture;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.VideoTextureEvent;
	import flash.filesystem.File;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.ProgressBar;
	import feathers.controls.ScrollContainer;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.ConcreteTexture;
	import starling.textures.Texture;

	public class VideoBox extends ScrollContainer
	{
		public static const VIDEO_DOWNLOADED:String = "VideoDownloaded";
		
		private var progressInt:int;
		private var vidClient:Object;
		private var vTexture:VideoTexture;
		private var cTexture:ConcreteTexture;
		private var nc:NetConnection;
		private var ns:NetStream;
		private var vidImage:Image;
		private var texture:Texture;
		private var progress:ProgressBar ;
		private var progressLabel:Label ;
		public var videoDownloader:VideoDownloader;
		private var video:Video;
		private var bitmapHolder:BitmapData;
		
		public function VideoBox() 
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_MIDDLE;
			
			this.layout = layout;
			
			this.width = 200;
			this.height = 200;
	}
	

	
	public function downloadVideo(url:String,cameraId:int):void
	{

		trace("downloadVideo()");
		
		progress = new ProgressBar();
		progress.minimum = 0;
		progress.maximum = 1;
		progress.value = 0;
		var progressLabel:Label = new Label();
		progressLabel.styleNameList.add("controlAppLabels");
		
		this.addChild( progressLabel );
		this.addChild( progress );
		
		videoDownloader = new VideoDownloader(url,cameraId);
		progressLabel.text = "Camera " + cameraId + "file " + url.substr(url.length - 8,8);
		
		videoDownloader.addEventListener(VideoDownloader.VIDEO_STARTED,function(e):void{
			progressInt = setInterval(function(){
				progress.value = videoDownloader.progress;
				if(progress.value > 0.1 * cameraId && bitmapHolder == null)
					getImage();
			
			},100);
			
		});
		videoDownloader.addEventListener(VideoDownloader.VIDEO_DOWNLOADED,onVideoDownloaded);


		showVideo(url);
	}
	
	private function onVideoDownloaded(e:Event):void
	{
		clearInterval(progressInt);
		this.removeChild( progressLabel );
		this.removeChild( progress );
		
		var file:File = File.applicationStorageDirectory.resolvePath(videoDownloader.fileName);
		
		
		dispatchEvent(new Event(VIDEO_DOWNLOADED));
	}
	
	public function showVideo(url:String):void
	{
		//var context3D:Context3D = Starling.context;
		
		vidClient = new Object();
		vidClient.onMetaData = onMetaData;
		var _vidBitmapHolder:BitmapData;
		
		nc = new NetConnection();
		nc.connect(null);
		
		ns = new NetStream(nc);
		ns.client = vidClient;
		
		video = new Video();
		video.width = 160;
		video.height = 90;
		video.attachNetStream(ns);	
		
		ns.play(url);


	
		
		// Extra - Turn it into a BitmapMaterial for Papervision3D
		//_tempVideoScreen = new BitmapMaterial(_vidBitmapHolder, false); // Create the BitmapMaterial
		
		
		/*vTexture = context3D.createVideoTexture();
		vTexture.attachNetStream(ns);
		vTexture.addEventListener(VideoTextureEvent.RENDER_STATE, renderFrame);
		cTexture = new ConcreteTexture(vTexture, Context3DTextureFormat.BGRA, 160, 90, false, true, true);
		vidImage = new Image(cTexture);
		this.addChild(vidImage);
		this.validate();*/
		
/*		var player:VideoPlayer = new VideoPlayer();
		player.setSize( 160, 90 );
		player.videoSource = url;
		this.addChild( player );
		
		var loader:ImageLoader = new ImageLoader();
		player.addChild( loader );

		function videoPlayer_readyHandler( event:Event ):void
		{
			trace("videoReady");
			loader.source = player.texture;
			player.pause();
			
			var jpgSource:BitmapData = new BitmapData (player.width, player.height); 
			jpgSource.draw(player.texture.); 
			
		}
		player.addEventListener( Event.READY, videoPlayer_readyHandler );*/
	}
	
	private function getImage():void
	{
		bitmapHolder = new BitmapData(video.width, video.height, false, 0x000000);
		bitmapHolder.draw(video); 
		
		var image:Image = new Image(Texture.fromBitmapData(bitmapHolder));
		image.width = 160;
		image.height = 90;
		addChild( image );
		ns.pause();
		ns.dispose();
		nc.close();
		video = null;
	}
	
	
	
	private function onMetaData(metadata:Object):void
	{
		trace("metadata " + metadata);
	}
	
	
	}
}