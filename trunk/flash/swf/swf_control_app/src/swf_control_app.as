package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	public class swf_control_app extends Sprite
	{
		public function swf_control_app()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init); 
			

		}
		
		private function init(e:Object = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var _starling:Starling = new Starling(Main, this.stage);
			_starling.supportHighResolutions = true;
			_starling.start();
		}
	}
}