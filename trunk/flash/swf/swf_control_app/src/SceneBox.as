package
{
	import com.distriqt.extension.devicemotion.DeviceMotion;
	import com.distriqt.extension.devicemotion.events.DeviceMotionEvent;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display3D.Context3D;
	import flash.display3D.textures.VideoTexture;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.Sound;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	
	import away3d.core.math.Quaternion;
	
	import co.beek.connect.Telnet;
	import co.beek.event.VideoEvent;
	
	import feathers.controls.LayoutGroup;
	import feathers.controls.Panel;
	import feathers.controls.TextInput;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Image;
	import starling.events.Event;

	public class SceneBox extends Panel
	{
		
		public static const RECORD_START:String = "recordStart";
		public static const RECORD_STOP:String = "recordStop";
		
		private var _camera1:Telnet;
		private var _camera2:Telnet;
		private var _newTitle:TextInput;
		
		private var videoBox1:VideoBox = new VideoBox();
		private var videoBox2:VideoBox = new VideoBox();
		private var videoBoxes:LayoutGroup = new LayoutGroup();
		
		private var record_button:ToggleButton;
		
		private var vidClient:Object;
		private var vTexture:VideoTexture;
		private var nc:NetConnection;
		private var ns:NetStream;
		private var vidImage:Image;
		private var context3D:Context3D;
		private var voiceRecorder:VoiceRecorder;
		private var quats:Vector.<Quaternion>;
		private var quatsString:String;
		
		private var _db:SQLConnection = new SQLConnection;
		
		public function SceneBox(camera1:Telnet, camera2:Telnet,db:SQLConnection)
		{
			
			_camera1 = camera1;
			_camera2 = camera2;
			_db = db;
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_MIDDLE;
			
			this.layout = layout;
			this.title = "new Scene";
	
			
			_newTitle = new TextInput;
			_newTitle.prompt = "Scene Name";
			this.addChild(_newTitle);
			
			
			record_button = new ToggleButton;
			record_button.label = "Record";
			record_button.addEventListener(Event.CHANGE,record_button_handler);
			this.addChild(record_button);
			
			videoBoxes.layout = new HorizontalLayout;
			videoBoxes.addChild(videoBox1);
			videoBoxes.addChild(videoBox2);
			this.addChild(videoBoxes);
		}
		
		private function record_button_handler(e:Event):void
		{
			if(record_button.isSelected){
				record_button.label = "Stop recording";
				dispatchEvent(new Event(RECORD_START));
				
				sendMessage("513");
				setTimeout(startRecord,100);

			}
			else{
				record_button.label = "Record";
				sendMessage("514");
				dispatchEvent(new Event(RECORD_STOP));
				stopRecord();
			}
		}
		
		private function startRecord():void
		{
			
			_camera1.addEventListener(Telnet.VIDEO_READY,onVideoReady);
			_camera2.addEventListener(Telnet.VIDEO_READY,onVideoReady);
			
			voiceRecorder = new VoiceRecorder;
			voiceRecorder.addEventListener(VoiceRecorder.VOICEOVER_COMPLETE,onVoiceoverComplete);
			voiceRecorder.startRecording();
			quats = new Vector.<Quaternion>;
			DeviceMotion.service.addEventListener( DeviceMotionEvent.UPDATE_QUATERNION, storeDeviceMotionData );
		}
		
		private function stopRecord():void
		{
			DeviceMotion.service.removeEventListener( DeviceMotionEvent.UPDATE_QUATERNION, storeDeviceMotionData );
			voiceRecorder.stopRecording();
			
			var ba:ByteArray = new ByteArray;
			ba.writeUTF(quatsString);
			
			var fs : FileStream = new FileStream();
			var file:File = File.documentsDirectory.resolvePath("Beek/quats.txt");  
			fs.open(file, FileMode.WRITE);
			fs.writeBytes(ba,0,ba.length);
			fs.close();
			
		}
		
		private function onVoiceoverComplete(e:Object):void
		{
			var sound:Sound = new Sound;
			sound.load(new URLRequest(File.applicationStorageDirectory.resolvePath(voiceRecorder.fileName).url));
			sound.play();
		
			onVideoDownloaded();
		}

		
		private function sendMessage(id:String):void
		{
			if(_camera1)
				_camera1.sendMessage(id);
			if(_camera2)
				_camera2.sendMessage(id);
		}
		
		private function onVideoReady(e:co.beek.event.VideoEvent):void
		{
			trace("onVideoReady()");
			var telnet:Telnet = e.target as Telnet;
			var url:String = e.url.replace("/tmp/fuse_d",'http://'+telnet.serverURL);
			
			if(telnet.cameraId == 1){
				videoBox1.addEventListener(VideoBox.VIDEO_DOWNLOADED,onVideoDownloaded);
				videoBox1.downloadVideo(url,telnet.cameraId);
			}
			else if (telnet.cameraId == 2){
				videoBox2.addEventListener(VideoBox.VIDEO_DOWNLOADED,onVideoDownloaded);
				videoBox2.downloadVideo(url,telnet.cameraId);
			}
			
		}
		
		private function onVideoDownloaded(e:Object = null):void{
			trace("onVideoDownloaded()");

			if(videoBox1.videoDownloader.fileName && videoBox2.videoDownloader.fileName && voiceRecorder.fileName)
				insertSceneIntoDB();
		}
		
		private function insertSceneIntoDB():void
		{
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = _db;
			insertStatement.text = "INSERT INTO `scenes` (`title`, `video1`, `video2`, `audio`) values (?, ?, ?, ?)";
			insertStatement.parameters[0] = _newTitle.text; 
			insertStatement.parameters[1] =  videoBox1.videoDownloader.fileName;
			insertStatement.parameters[2] =  videoBox2.videoDownloader.fileName;
			insertStatement.parameters[3] = voiceRecorder.fileName;
			insertStatement.execute();
			var data:Array = insertStatement.getResult().data;
		}
		
		
		
		private function storeDeviceMotionData( event:DeviceMotionEvent):void
		{
			var quaternion:Quaternion = new Quaternion( event.values[1], event.values[2], event.values[3], event.values[0] );
			quats.push(quaternion);
			
			quatsString = quatsString + "\r\n" + event.values[1] +","+ event.values[2] +","+ event.values[3] +","+ event.values[0];
		}
	}
}