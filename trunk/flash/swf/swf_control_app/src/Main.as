package
{

	//import flash.events.Event;
	import com.distriqt.extension.devicemotion.DeviceMotion;
	import com.distriqt.extension.devicemotion.DeviceMotionOptions;
	import com.distriqt.extension.devicemotion.SensorRate;
	import com.distriqt.extension.devicemotion.events.DeviceMotionEvent;
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.display3D.Context3D;
	import flash.display3D.textures.VideoTexture;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.net.InterfaceAddress;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import away3d.core.math.Quaternion;
	
	import co.beek.Log;
	import co.beek.connect.Telnet;
	import co.beek.event.VideoEvent;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Panel;
	import feathers.controls.ProgressBar;
	import feathers.controls.ScrollContainer;
	import feathers.controls.TextInput;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.TiledRowsLayout;
	import feathers.layout.VerticalLayout;
	import feathers.themes.CustomMetalWorksTheme;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	
	public class Main extends Sprite
	{
		
		private var _loader:LayoutGroup;
		private var _scrollContainer:ScrollContainer = new ScrollContainer();
		private var _connectContainer:LayoutGroup = new LayoutGroup();
		private var _sceneContainer:LayoutGroup = new LayoutGroup();
		private var scan_button:Button;
		private var record_button:ToggleButton;
		

		
		private var telnet1:Telnet;
		private var telnet2:Telnet;
		
		//download videos
		private var camera1Progress:ProgressBar;
		private var camera2Progress:ProgressBar;
		
		//database
		private var db:SQLConnection = new SQLConnection;
		
		//camera IPs
		private var camera1Ip:TextInput;
		private var camera2Ip:TextInput;
		private var camera1Battery:ProgressBar;
		private var camera2Battery:ProgressBar;
		

		
		
		public function Main()
		{
			new CustomMetalWorksTheme(false);
			super();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init); 

		}
		
		private function init(e:Object = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_MIDDLE;
			layout.gap = 20;
			layout.padding = 10;
			
			_connectContainer.layout = _scrollContainer.layout = layout;
			_connectContainer.width = _scrollContainer.width = stage.stageWidth;
			
			var _cameraContainer:LayoutGroup = new LayoutGroup();
				_cameraContainer.layout = new HorizontalLayout;
				_connectContainer.addChild(_cameraContainer);

			var camera1:Panel = new Panel();
				camera1.title = "Camera 1";
			_cameraContainer.addChild( camera1 );
			
			camera1Ip = new TextInput();
			camera1Ip.prompt = "Camera 1 IP";
			camera1Ip.text = "192.168.43.36";
			camera1.addChild( camera1Ip );
			
			camera1Battery = new ProgressBar;
			camera1Battery.minimum = 0;
			camera1Battery.maximum = 100;
			camera1Battery.value = 100;
			camera1Battery.visible = false;
			camera1.addChild( camera1Battery );
			
			var camera2:Panel = new Panel();
			camera2.title = "Camera 2";
			_cameraContainer.addChild( camera2 );
			
			camera2Ip = new TextInput();
			camera2Ip.prompt = "Camera 2 IP";
			camera2Ip.text = "192.168.43.40";
			camera2.addChild( camera2Ip );
			
			camera2Battery = new ProgressBar;
			camera2Battery.minimum = 0;
			camera2Battery.maximum = 100;
			camera2Battery.value = 100;
			camera2Battery.visible = false;
			camera2.addChild( camera2Battery );
			
			scan_button = new Button;
			scan_button.label = "Connect to Cameras";
			scan_button.addEventListener(Event.TRIGGERED,button_scan_handler);
			_connectContainer.addChild(scan_button);
			
			_scrollContainer.addChild(_connectContainer);
			
			var sceneContainerLayout:TiledRowsLayout = new TiledRowsLayout();
			sceneContainerLayout.padding = 10;
			sceneContainerLayout.gap = 10;
			sceneContainerLayout.horizontalAlign = TiledRowsLayout.HORIZONTAL_ALIGN_LEFT;
			sceneContainerLayout.verticalAlign = TiledRowsLayout.VERTICAL_ALIGN_TOP;
			
			_sceneContainer.layout = sceneContainerLayout;
			_sceneContainer.width = stage.stageWidth;
			
			_scrollContainer.addChild( _sceneContainer );
			
			
			addChild(_scrollContainer);
			connectDb();
			deviceMotion();	
		}
		
		private function deviceMotion():void
		{
			
			try{
				
                var key:String ="8f5db1ab19fa3f027ad1ebb131ab997e3fd28b8dOIUUvkApjc6MLPXpx/N26TAbKJrX9NElTyMiMVZvXsf56rHfyeFYX/bg7bJJKuVssnd7l7C2joQNPe3qX3c0NkC1Tjk1irodMxglvIZfwJ2nI6yigkzJrzUW8rlqzVke9MoirnYOSSFMDWa/MVnsxFh9UfXdinyPryjeZl4+wA9wJfKht43x8ufo6ZrCRYYwRSpgoyMnYJ65fLjfGuf7hbVHSl+agxA33DXWd2XzbhWs9V0QgtNY/cxhV4lNsjEVTt1usD1v+Tfsa8i5j5bEQ8KA2OwC8BMPNAlOMbuZWBeXxlsDfQvsORqad5h8Z/sjaQ0BiN9qo6QgnrbuocPQog==";
			
				DeviceMotion.init(key);

				trace( "DeviceMotion Supported: " + DeviceMotion.isSupported );
				trace( "DeviceMotion Version:   " + DeviceMotion.service.version );

				
				var options:DeviceMotionOptions = new DeviceMotionOptions();
				options.rate        = SensorRate.SENSOR_DELAY_FASTEST;
				options.algorithm   = DeviceMotionOptions.ALGORITHM_NATIVE;
				options.format      = DeviceMotionOptions.FORMAT_QUATERNION;
				
				DeviceMotion.service.register( options );

				
			}
			catch (e:Error)
			{
				trace( "ERROR::" + e.message );
			}
		}
		


		
		private function button_scan_handler(e:Event):void
		{
			
			if(camera1Ip.text){	
				var sqlStatement1:SQLStatement = new SQLStatement();
					sqlStatement1.sqlConnection = db;
					sqlStatement1.text = "INSERT OR REPLACE INTO cameras (id, ip) VALUES ('1','"+ camera1Ip.text + "')";
					sqlStatement1.execute();
				
				 telnet1 = new Telnet(camera1Ip.text,7878,1);
				telnet1.addEventListener(Telnet.CONNECTION_SUCCESS,onConnectionSuccess);
				telnet1.addEventListener(Telnet.CONNECTION_FAILED,onConnectionFailed);
				telnet1.addEventListener(Telnet.CONNECTION_ERROR,onConnectionError);
				telnet1.addEventListener(Telnet.CONNECTION_CLOSED,onConnectionClosed);

				telnet1.addEventListener(Telnet.BATTERY_UPDATE,function(e):void{
					camera1Battery.value = telnet1.batteryLevel}
				);
			}
			
			if(camera2Ip.text){	
				
				var sqlStatement2:SQLStatement = new SQLStatement();
					sqlStatement2.sqlConnection = db;
					sqlStatement2.text = "INSERT OR REPLACE INTO cameras (id, ip) VALUES ('2','"+ camera2Ip.text + "')";
					sqlStatement2.execute();
				
				telnet2 = new Telnet(camera2Ip.text,7878,2);
				telnet2.addEventListener(Telnet.CONNECTION_SUCCESS,onConnectionSuccess);
				telnet2.addEventListener(Telnet.CONNECTION_FAILED,onConnectionFailed);
				telnet2.addEventListener(Telnet.CONNECTION_ERROR,onConnectionError);
				telnet2.addEventListener(Telnet.CONNECTION_CLOSED,onConnectionClosed);
				telnet2.addEventListener(Telnet.BATTERY_UPDATE,function(e):void{
					camera2Battery.value = telnet2.batteryLevel}
				);
			}
		}
		
		
		private function getIp():String{
			var netInterfaces:Vector.<NetworkInterface> = NetworkInfo.networkInfo.findInterfaces();
			var addresses:Vector.<InterfaceAddress> = netInterfaces[1].addresses;
			if(addresses.length > 0)
				return addresses[0].address;  
			else
				return null;
		}
		
		private function onConnectionSuccess(e:Object):void
		{	

			var telnet:Telnet = e.target as Telnet;
			var cameraId:int = telnet.cameraId;
			trace("onConnectionSuccess("+cameraId+")");
			
			if(cameraId == 1)
			{
				camera1Ip.visible = false;
				camera1Battery.visible = true;
				_connectContainer.validate();
			}
			
			if(cameraId == 2)
			{
				camera2Ip.visible = false;
				camera2Battery.visible = true;
				_connectContainer.validate();
			}
			
			if(telnet1.connected && telnet2.connected){
				_connectContainer.removeChild(scan_button);
				addScene();
				_connectContainer.validate();
				_scrollContainer.validate();
			}
		
		}

		
		private function onConnectionFailed(e:Object):void
		{
			trace('connectionFailed');
		}
		
		private function onConnectionError(e:Object):void
		{
			trace('connectionError');
		}
		
		private function onConnectionClosed(e:Object):void
		{
			trace('connectionClosed');
		}
		
		
		private function addScene():void
		{
		trace("addScene()");
		 var scene:SceneBox = new SceneBox( telnet1, telnet2, db );
		 _sceneContainer.addChild(scene);
		
		}
		

		
		private function connectDb():void
		{
			db.addEventListener(SQLEvent.OPEN, onDBConnected);
			db.addEventListener(SQLErrorEvent.ERROR, onDBError);
			db.open(File.applicationStorageDirectory.resolvePath("controlApp.db"));
		}
		
		private function onDBError(event:SQLErrorEvent):void
		{
			throw new Error("onDBError() " +event.errorID+" : "+event.text+" : "+event.error.details);
		}
		
		private function onDBConnected(event:SQLEvent):void
		{
			Log.record("swf_mobile.onDBConnected()");	
			
			var createTableStmt:SQLStatement = new SQLStatement();
			createTableStmt.sqlConnection = db;
			createTableStmt.text = "CREATE TABLE IF NOT EXISTS `cameras` (`id` INTEGER , `ip` TEXT )";
			createTableStmt.execute();	
			
			var createSceneTableStmt:SQLStatement = new SQLStatement();
			createSceneTableStmt.sqlConnection = db;
			createSceneTableStmt.text = "CREATE TABLE IF NOT EXISTS `scenes` (`id` INTEGER PRIMARY KEY AUTOINCREMENT , `title` TEXT, `video1` TEXT, `video2` TEXT, `audio` TEXT )";
			createSceneTableStmt.execute();	
			
			var getcamerasStatement:SQLStatement = new SQLStatement();
			getcamerasStatement.sqlConnection = db;
			getcamerasStatement.text = "SELECT * FROM `cameras`";
			getcamerasStatement.execute();
			
			var data:Array = getcamerasStatement.getResult().data;
			
			for each(var obj:Object in data){
				if(obj.id == 1)
					camera1Ip.text = obj.ip;
				
				if(obj.id == 2)
					camera2Ip.text = obj.ip;
			}

		}

		
		
	}
	

}