package
{
	import com.laiyonghao.Uuid;
	
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class VideoDownloader extends EventDispatcher
	{
		public static const VIDEO_STARTED:String = "VideoStarted";
		public static const VIDEO_DOWNLOADED:String = "VideoDownloaded";
		
		private var urlStream:URLStream;
		private var fileData:ByteArray;
		
		private var _progress:Number;
		private var _url:String;
		private var _cameraId:int;
		private var _fileName:String;
		
		
		public function VideoDownloader(url:String,cameraId:int) 
		{
			_url = url;
			_cameraId = cameraId;

			var urlReq:URLRequest = new URLRequest(url);

			urlStream = new URLStream();
			fileData = new ByteArray();
			urlStream.addEventListener(flash.events.Event.COMPLETE, videoDownloaded);
			urlStream.addEventListener(flash.events.Event.OPEN, function(e){dispatchEvent(new Event(VIDEO_STARTED))});
			urlStream.addEventListener(ProgressEvent.PROGRESS, function(e:ProgressEvent):void{_progress = e.bytesLoaded / e.bytesTotal});
			urlStream.load(urlReq);
		}
		
		
		
		private function videoDownloaded(e:Object):void
		{
			urlStream.readBytes(fileData, 0, urlStream.bytesAvailable);
			
			var uuid:Uuid = new Uuid();
			
			_fileName = uuid + ".mp4";
				
			var file:File = File.applicationStorageDirectory.resolvePath(_fileName); 
			
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(fileData, 0, fileData.length);
			fileStream.close();
			dispatchEvent(new Event(VIDEO_DOWNLOADED));
		}

		public function get progress():Number
		{
			if(_progress)
				return _progress
			else
				return 0;
		}

		public function get fileName():String
		{
			return _fileName;
		}


	}
}