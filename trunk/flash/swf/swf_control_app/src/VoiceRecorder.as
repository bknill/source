package
{
	import com.laiyonghao.Uuid;
	import com.noteflight.standingwave3.elements.AudioDescriptor;
	import com.noteflight.standingwave3.elements.IAudioSource;
	import com.noteflight.standingwave3.elements.Sample;
	import com.noteflight.standingwave3.filters.GainFilter;
	import com.noteflight.standingwave3.filters.StandardizeFilter;
	import com.noteflight.standingwave3.filters.ToneControlFilter;
	import com.noteflight.standingwave3.formats.WaveFile;
	import com.noteflight.standingwave3.output.AudioPlayer;
	import com.noteflight.standingwave3.performance.AudioPerformer;
	import com.noteflight.standingwave3.performance.ListPerformance;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import co.beek.model.Model;
	import co.beek.service.data.FileUploader;
	
	import fr.kikko.lab.ShineMP3Encoder;
	
	import org.bytearray.micrecorder.MicRecorder;
	import org.bytearray.micrecorder.encoder.WaveEncoder;
	import org.bytearray.micrecorder.events.RecordingEvent;
	

	public class VoiceRecorder extends EventDispatcher
	{
		public static const VOICEOVER_COMPLETE:String = "VoiceoverComplete";
		
		public var recorder:MicRecorder = new MicRecorder(new WaveEncoder());	
		private var recording:Boolean = false;		
		public var player:AudioPlayer = new AudioPlayer();
		private var mp3Encoder:ShineMP3Encoder;			
		private var myWavData:ByteArray = new ByteArray();
		private var myWavFile:ByteArray = new ByteArray();
		private var sequence = new ListPerformance();
		private var ap:AudioPerformer;
		private var _fileName;
		private var finalSource:IAudioSource;
		
		private var timer:Timer = new Timer(0);
		
		public var statusTxt:String;
		public var buttonTxt:String = "Record Voiceover";
		public var uploadButtonTxt:String = "Upload MP3";
		
		

		public var process:Boolean = false;
		private var startRecordTime:int;
		
		
		public function VoiceRecorder()
		{
			super();
		}
		
		public function startRecording():void
		{			
				recorder.record();
				myWavData = new ByteArray();
				myWavFile = new ByteArray();
				
				Model.state.mute = true;
				process = false;
				startRecordTime = getTimer();
			
		}
		
		public function stopRecording():void
		{
			recorder.stop();
			recordComplete();
			
		}
		
		
		public function onRecording(e:RecordingEvent):void
		{			
			var al:Number = recorder.microphone.activityLevel;
			if (!recording) recording = true;
		}		
		
		
		
		private function recordComplete():void
		{
			
			recording = false;
			statusTxt = "recording complete"	
			buttonTxt = "New Recording";
			process = true;
			var src:Sample = WaveFile.createSample(recorder.output);
			
			sequence.removeSource(0);
			sequence.addSourceAt(0, src);
			
			var source:IAudioSource = new AudioPerformer(sequence,new AudioDescriptor());
			var filteredSource1:IAudioSource = new ToneControlFilter(source);
			var filteredSource2:IAudioSource = new GainFilter(filteredSource1,5);
			var finalSource:IAudioSource = new StandardizeFilter(filteredSource2);
			
			var sequenceFiltered:ListPerformance = new ListPerformance()
			sequenceFiltered.addSourceAt(0, finalSource);
			ap = new AudioPerformer(sequenceFiltered, new AudioDescriptor());
			
			processVO();
		}
		

		
		
		protected function onIOError(event:IOErrorEvent):void
		{
			throw new Error("Error when uploading.");
		}
		
		
		
		public function processVO():void
		{
			renderWav(ap, true)	
		}
		
		private function renderWav(src, convertToMp3 = false):void
		{
			var innerTimer = new Timer(10,0)
			var framesPerChunk:uint = 8192;
			
			innerTimer.addEventListener(TimerEvent.TIMER, handleRenderTimer)
			innerTimer.start()
			
			function handleRenderTimer(e:TimerEvent)
			{
				src.getSample(framesPerChunk).writeWavBytes(myWavData)
				
				var m = Math.min(src.frameCount, src.position + framesPerChunk)
				var n = Math.max(0, m - src.position)
				
				if (n == 0)
				{
					if (src.position > 0) finishRender() else trace("cancel rendering")
					
				} else {
					statusTxt = "rendering audio: "+ Math.floor(src.position * 100 / src.frameCount) + "%";
				}
			}				
			function finishRender():void
			{
				innerTimer.stop()
				statusTxt = "finishing audio render"
				WaveFile.writeBytesToWavFile(myWavFile, myWavData, 44100, 2, 16)
				
				if (convertToMp3)
				{
					makeIntoMp3(myWavFile)
				}
			}				
		}
		
		
		
		private function makeIntoMp3(wav:ByteArray):void
		{
			wav.position = 0
			mp3Encoder = new ShineMP3Encoder(wav);
			mp3Encoder.addEventListener(Event.COMPLETE, mp3EncodeComplete);
			mp3Encoder.addEventListener(ProgressEvent.PROGRESS, mp3EncodeProgress);
			mp3Encoder.start();	
		}
		
		private function mp3EncodeProgress(e:ProgressEvent): void 
		{
			statusTxt = "encoding mp3: " + e.bytesLoaded + "%"
		}
		
		private function mp3EncodeComplete(e: Event) : void 
		{
			statusTxt = "mp3 encoding complete\n"
			saveVoiceover();
		}
		
		private function saveVoiceover() : void 
		{
			var uuid:Uuid = new Uuid();
			
			_fileName = uuid + ".mp3";
                                                                                       
			var sba:ByteArray = mp3Encoder.mp3Data;
			sba.position =  sba.length - 128;
			sba.writeByte(57);	
			
			var file:File = File.applicationStorageDirectory.resolvePath(_fileName); 
			
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeBytes(sba, 0, sba.length);
			fileStream.close();

			dispatchEvent(new Event(VOICEOVER_COMPLETE));

			
		}
		

		public function get fileName()
		{
			return _fileName;
		}

		
	}
}