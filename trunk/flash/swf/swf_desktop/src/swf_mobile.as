package
{
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.system.System;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.events.Stage3DEvent;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.connect.ConnectManager;
	import co.beek.connect.MotionDataManager;
	import co.beek.connect.WebSocketManager;
	import co.beek.connect.WifiManager;
	import co.beek.event.NetworkDataEvent;
	import co.beek.guide.StarlingGuide;
	import co.beek.mobile.MobileConfig;
	import co.beek.mobile.OtherGuideRenderer;
	import co.beek.mobile.ServiceChecker;
	import co.beek.mobile.SettingsMenu;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.events.StringEvent;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.offline.OfflineManager;
	import co.beek.pano.Black;
	import co.beek.pano.Pano;
	import co.beek.service.GuideService;
	import co.beek.service.ServiceLoader;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;

	//import starling.events.TouchEvent;
	
	

	public class swf_mobile extends Sprite
	{
		
		private var pano:Pano;
		//private var guide:Guide = new Guide;
		private var _starling:Starling;
		
	//	private var bookmarks:Bookmarks = new Bookmarks;
		private var progressLocator:Sprite = new Sprite;
		private var beekDB:SQLConnection = new SQLConnection;
		private var serviceChecker:ServiceChecker = new ServiceChecker;
		private var guidesHolder:Sprite = new Sprite();	
		private var useGyro:Boolean = true;
		private var stageWidth:Number = 500;
		private var stageHeight:Number = 375;
		private var appScale:Number = 1;
		private var initialized:Boolean;
		public var introComplete:Boolean = false;
		private var networkManager:ConnectManager = new ConnectManager;
		private var black:Black = new Black;
		private var closeVRButton:VRCloseButtonFl = new VRCloseButtonFl;
		private var controlInt:int;
		private var dragObject:Object = new Object();
		private var dragging:Boolean;
		
		private var stage3DManager:Stage3DManager;
		private var stage3DProxy:Stage3DProxy;
		private var antiAlias:Number = 2;
		
	
		public function swf_mobile()
		{
			super();
			

			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0].toString();
			var appId:String = appXml.ns::id[0].toString();
			appId = appId.substring(appId.indexOf("g") + 1,appId.length);
			Model.appName = appXml.ns::name[0].toString();
			
			
		Model.config = new MobileConfig({
				"domain":"beek.co",
				"assetCdn":"http://cdn.beek.co",
				"serviceUrl":"http://gms.beek.co",
				"guideId" : appId
			});
			
			Fonts.titleFont = new TitleFontFl().fontName;
			Fonts.bodyFont = new BodyFontFl().fontName;
			Fonts.boldFont = new BoldFontFl().fontName;
			
			
			//addEventListener(Event.ACTIVATE,onActivate);
			//addEventListener(Event.DEACTIVATE,onDeactivate);
			
			//Model.addEventListener(Model.GUIDE_INITIALISED,createPano);
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.OFFLINE_ERROR, onOfflineError);
			//Model.addEventListener(Model.OFFLINE_COMPLETE, onOfflineComplete);
			Model.addEventListener(Model.NETWORK_DATA,onNetworkData);
			Model.state.addEventListener(State.GUIDE_ID_CHANGE, onGuideIdChange);
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE, onLoadingSceneChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE, onGuideViewChange);
			Model.state.addEventListener(State.NETWORK_CONNECT, onNetworkConnect);
			Model.state.addEventListener(State.VR_MODE_CHANGE,onVRModeChange);
			
			//window.nativeWindow.addEventListener(air.Event.ACTIVATE, onClosingEvent);
			//window.nativeWindow.addEventListener(air.Event.DEACTIVATE, onClosingEvent); 	
	
		
			
			//pano.addEventListener(Pano.PAUSE_AUTOROTATE, onPanoPauseAutorotate);
			serviceChecker.addEventListener(ServiceChecker.CHECKED, onServiceChecked);
			//guide.addEventListener(Guide.PLAY_BUTTON_CLICKED, onGuideStartPresentation);
		
			if(!MobileConfig.localStorage.exists)
				MobileConfig.localStorage.createDirectory();
			
			
			//drawCircle();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		

		
		private function onAddedToStage(event:Event):void	
		{
			Log.record("swf_player.onAddedToStage()");
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			this.stage.align = StageAlign.TOP_LEFT;
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			
			//addChild(guide);
			//addChild(settingsMenu);
			//addChild(closeVRButton);

			

			if( stage.displayState == StageDisplayState.NORMAL ) 
				stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			
			this.stage.addEventListener(KeyboardEvent.KEY_DOWN, reportKeyDown);
			this.stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
			this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
		
			resize(stage.stageWidth,stage.stageHeight);
			
			initializeStage3d();
	
		}
		
		private function initializeStage3d(e:Event = null):void
		{
			Log.record("SWF_Mobile.initializeStage3d()");
			
			
			// Define a new Stage3DManager for the Stage3D objects
			stage3DManager = Stage3DManager.getInstance(stage);
			
			// Create a new Stage3D proxy to contain the separate views
			stage3DProxy = stage3DManager.getFreeStage3DProxy();
			
			stage3DProxy.antiAlias = antiAlias;	
			stage3DProxy.width = stage.stageWidth;
			stage3DProxy.height = stage.stageHeight;
			stage3DProxy.viewPort.width = stage.stageWidth;
			stage3DProxy.viewPort.height = stage.stageHeight;
			stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onContextCreated);
			
			

		}
		
		private function onContextCreated(e:Event):void
		{

			stage3DProxy.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN,hitTest);
			stage.addEventListener(MouseEvent.MOUSE_UP,hitTest);	

			createPano();
			createGuide();
		}
		
		private function onEnterFrame(e:Event):void
		{
			if(pano)
				pano.onEnterFrame(e);
			if(_starling)
				_starling.nextFrame();
		}

		
		private function createPano(e:Event = null):void
		{
			Log.record("swf_mobile.createPano()");
			
			if(pano)
				return;
			
			pano = new Pano;

			addChildAt(pano,0);

			pano.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb);
			pano.addEventListener(HotspotEvent.HOTSPOT_CLOSE_WEB, onHotspotCloseWeb);
			pano.addEventListener(Pano.PANO_TILES_LOADED, onPanoTilesLoaded);
			pano.addEventListener(Pano.PANO_DEEP_TILES_LOADED, onPanoDeepTilesLoaded);
			pano.addEventListener(Pano.START_DRAG, onStartDrag);
			pano.addEventListener(Pano.STOP_DRAG, onStopDrag);
			pano.addEventListener(Pano.START_AUTOROTATE, onPanoStartAutorotate);
			pano.addEventListener(Pano.STOP_AUTOROTATE, onPanoStopAutorotate);
			
			//pano.mouseEnabled = false;
			
			pano.initializePano(stage3DProxy, stage.stageWidth, stage.stageHeight);
		
		}
		
		private function createGuide():void
		{
			Starling.multitouchEnabled = true;
			_starling = new Starling(StarlingGuide, this.stage, stage3DProxy.viewPort, stage3DProxy.stage3D);
			_starling.supportHighResolutions = true;
			_starling.shareContext = true;

			_starling.start();
		}
		
		private function hitTest(e:MouseEvent):void
		{
			var test:Boolean = false;
			var xLeft:Number = 50;
			var xRight:Number = 50;
			var yBottom:Number = 50;
			
			switch(Model.state.guideView){
				case State.GUIDE_VIEW_SECTION:
					xLeft = 450;
					break;
				case State.GUIDE_VIEW_SCENE:
					xRight = 320;
					break;
				case State.GUIDE_VIEW_SCENE_AND_SECTION:
					xLeft = 450;
					xRight = 450;
					break;	
				case State.GUIDE_VIEW_SETTINGS:
					yBottom = 100;
					break;
				
			}
			
			if(!_starling)
				test = true;
			else if(Model.lockUI)
				test = false;
			else{
				
				var object:DisplayObject = _starling.root.hitTest(new Point(stage.mouseX, stage.mouseY));
			
				if(object == null) 
					test = true;
				else if(object.alpha == 0 && stage.mouseX > xLeft && stage.mouseX < stage.width -xRight && stage.mouseY < stage.height - yBottom) 
					test = true;
			}
			
				if(test)
				switch (e.type){
				case "mouseUp":
					if(pano.mouseDragPano)
						pano.onMouseUp(e);
					break;
				case "mouseDown":
					pano.onMouseDown(e);
					break;
				}

			
		}
		
		
		private function onActivate(e:Event):void {
			Model.state.mute = false;
		}
		private function onDeactivate(e:Event):void {
			Model.state.mute = true;
		}
		
		
		public function resize(w:Number,h:Number):void
		{
			Log.record("swf_mobile.onStageResize(width: "+stage.fullScreenWidth+", height: "+stage.fullScreenHeight+")")
		//	if(stageWidth == stage.fullScreenWidth && stageHeight == stage.fullScreenHeight)
			//	return;
			
			stageWidth = w;
			stageHeight = h;
			
			//guide.resize(stageWidth, stageHeight);
			//pano.resize(stageWidth, stageHeight);
			black.resize(stageWidth, stageHeight);
			
			progressLocator.x = 50;
			progressLocator.y = stageHeight - progressLocator.height - 50;
		
			closeVRButton.scaleX = closeVRButton.scaleY = 2;
			closeVRButton.x = stageWidth - closeVRButton.width - 20;
			closeVRButton.y = 20;
			
		//	if(!bookmarks.isDown())
				//bookmarks.toggle();
			
			// Cannot init ui before its been resized for the first time
			if(!checkServiceRun){
				checkService();
				
			}
			initialized = true;
			
		}
		

		
		
		//Checking for network connectivity
		private function onNetworkChange(e:Event):void
		{
			Log.record("swf_mobile.onNetworkChange()");
			checkService();
		}
		
		private var checkServiceRun:Boolean;
		
		private function checkService():void
		{
			Log.record("swf_mobile.checkService()");
			checkServiceRun = true;
			serviceChecker.start();
		}
		
		private function onServiceChecked(event:Event):void 
		{
			Log.record("swf_mobile.onServiceCheckerSuccess();");
			Model.state.networkAccess = serviceChecker.available;
			if(!beekDB.connected)
				connectDb();
			
			
		}
		
		private function connectDb():void
		{
			Log.record("swf_mobile.connectDb();");
			beekDB.addEventListener(SQLEvent.OPEN, onDBConnected);
			beekDB.addEventListener(SQLErrorEvent.ERROR, onBeekDBError);
			beekDB.open(MobileConfig.localStorage.resolvePath("beek.db"));
		}
		
		private function onBeekDBError(event:SQLErrorEvent):void
		{
			throw new Error("onBeekDBError() " +event.errorID+" : "+event.text+" : "+event.error.details);
		}
		
		private function onDBConnected(event:SQLEvent):void
		{
			Log.record("swf_mobile.onDBConnected()");
			var createTableStmt:SQLStatement = new SQLStatement();
			createTableStmt.sqlConnection = beekDB;
			createTableStmt.text = "CREATE TABLE IF NOT EXISTS `localguides` (" +
				"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`guide_id` INTEGER, " +
				"`json` TEXT" +
				")";
			
			
			createTableStmt.execute();
			
			OfflineManager.connect(beekDB);
			
			initComplete();

		}
		
		
		private function initComplete():void
		{
			Log.record("swf_mobile.initComplete()");
			
			//if main or other guide downloaded load this offline
			if(OfflineManager.checkForOfflineLoad(Model.config.guideId)){
				trace("Guide exists in local storage");
				loadGuideOffline(OfflineManager.checkForOfflineLoad(Model.config.guideId));
				Model.state.startState = "Loading from device (" +OfflineManager.offlineGuides.length+" downloaded)";
				return;
			} else if(serviceChecker.available){
				trace("loading guide from the Internet");
				loadGuide();
				Model.state.startState = "Connected - Loading from Internet";
				return;
			}else{
				
			Model.state.startState = "Waiting for network connection";
			var waitForConnection = setInterval(
				function(){
					serviceChecker.start();
					serviceChecker.addEventListener(ServiceChecker.CHECKED,function(){
						if(serviceChecker.available){
							loadGuide();
							Model.state.startState = "Connected - Loading from Internet";
							clearInterval(waitForConnection);
						}
						
					})},2000);
			}
			
		}
		
		
		private function loadGuide():void
		{
			Log.record('swf_mobile.loadGuide()');
			GuideService.getGuideBasicJson(Model.config.guideId,loadGuideOffline)		
		}
		
		private function loadGuideOffline(data:GuideBasicData):void
		{
			Log.record('swf_mobile.loadGuideOffline()');
			Model.state.guideId = data.id;
		}
	
		private function loadSceneData(sceneId:String):void
		{
			Log.record("swf_mobile.loadSceneData()")
			var loader:ServiceLoader = new ServiceLoader(SceneData);
			var url:String = Model.config.getSceneJsonUrl(sceneId);
			loader.load(url, onSceneDataLoaded);
		}
		
		private function onSceneDataLoaded(scene:SceneData):void
		{
			Log.record("swf_mobile.onSceneDataLoaded()")
			Model.currentScene = scene;
			
			var loadingScene:SceneElementData = Model.state.loadingScene;
			var guideScene:GuideSceneData = Model.guideData
				? Model.guideData.getGuideSceneFromId(loadingScene.id)
				: null;
		}
		
		/**
		 * A pano scene has completed loading
		 */
		private function onPanoTilesLoaded(event:Event):void 
		{
			Log.record("swf_mobile.onPanoTilesLoaded()");
			
			// perform garbage collection in AIR hopefully.
			flash.system.System.gc();
			flash.system.System.gc();
			
			if(Model.state.mode != State.MODE_REMOTE)
				Model.state.mode = State.MODE_AUTO;
			
			//black.tween(0);
			
			
		}
		
		private function onPanoDeepTilesLoaded(event:Event):void 
		{
			Log.record("swf_mobile.onPanoDeepTilesLoaded()");
				
				flash.system.System.gc();
				flash.system.System.gc();
				
				Model.state.mode = State.MODE_AUTO;
		}
			

		
		private function onStartDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStartDrag()");
			//guide.mobileResize(appScale);
			
			Model.state.dispatchEvent(new Event(State.PANO_ENGAGED));
			Model.state.mode = State.MODE_DRAG;
			dragging = true;
			
			//hideUI();
			if(!Model.state.active)
				Model.state.active = true;
		}
		
		private function onStopDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStopDrag()");
			//guide.mobileResize(appScale);
			dragging = false;
			
			//setTimeout(function():void{if(!dragging)showUI();},1500);
			
		}
		
		private function onBookcaseOpening(event:Event):void
		{
			Log.record("swf_mobile.onBookcaseOpening()");
			Model.state.mute = true;
			//pano.stopAutoRotate();
		}
		
		private function onBookcaseClosing(event:Event):void
		{
			Model.state.mute = false;
			//if(Model.currentScene)
			//pano.queueAutoRotate(5000);
		}
		
		private function onGlobalErrors(event:Event):void
		{
			return;
			
			var message:String = "";
			if (event['error'] is Error)
				message = Error(event['error']).message;
				
			else if (event['error'] is ErrorEvent)
				message = ErrorEvent(event['error']).text;
				
			else if(event['error'])
				message = event['error'].toString();
			
			message += "\n\n" + Log.actions.join("\n");
			
			var d:ErrorDialogFl = new ErrorDialogFl;
			d.textField.text = message;
			d.removeButton.scaleX = d.removeButton.scaleY = 10;
			d.removeButton.addEventListener(MouseEvent.CLICK,onCloseButtonClick);
			d.x = (stageWidth - d.width)/2;
			d.y = (stageHeight - d.height)/2;
			addChild(d);
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			removeChild(event.target.parent)
		}
		
		private function onGuideIdChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideIdChange")
			//if(browserInvokeGuideId != null) 
			loadGuideData(Model.state.guideId);
		}
		
		private function loadGuideData(guideId:String):void
		{
			Log.record("swf_mobile.loadGuideData(" + guideId + ")");
			var loader:ServiceLoader = new ServiceLoader(GuideData)
			var url:String = Model.config.getGuideJsonUrl(guideId);
			loader.load(url,onGuideDataLoaded);
		}
		
		private function onGuideDataLoaded(guideData:GuideData):void
		{
			Log.record("swf_mobile.onGuideDataLoaded");
			Model.guideData = guideData;
			
		
		}
		
		private function onGuideChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideChange()");
			
		}
		

		
		private function onLoadingSceneChange(event:Event):void
		{
			Log.record("swf_mobile.onLoadingSceneChange()");
			loadSceneData(Model.state.loadingScene.id);
			
			if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_RECEIVE){

				var obj:Object = new Object;
				obj.type = "scene";
				obj.id = Model.state.loadingScene.id;
				sendData(obj);
			}
		}
		
		private function onOfflineError(event:StringEvent):void
		{
			showNetworkError(
				event.value+" is not available",
				"You are not currently connected to the internet \n"
				+"To access '"+event.value+"' you need to connect to the internet"
			);
		}
		
		private function onGuideViewChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideIdChange")
			if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_RECEIVE)
				if(Model.state.guideView == State.GUIDE_VIEW_CLOSED)
					MotionDataManager.stop();
				else
					MotionDataManager.start();
			
			if(Model.state.guideView == State.GUIDE_VIEW_SECTION
				&& Model.state.connectMode == State.CONNECT_MODE_RECEIVE)
				Model.state.mode = State.MODE_DRAG;
				
		}

		
		private function showNetworkError(title:String, message:String):void
		{
			var warning:InfoPanelFl = new InfoPanelFl;
			
			warning.titleField.text = title;
			Fonts.format(warning.titleField, Fonts.titleFont);
			
			warning.bodyField.text = message;
			Fonts.format(warning.bodyField, Fonts.bodyFont);
			
			warning.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
			warning.x = (stageWidth - warning.width)/2;
			warning.y = (stageHeight - warning.height)/2;
			addChild(warning);
		}
		
		
		private function onGuideStartPresentation(event:Event):void
		{
			Log.record("swf_mobile.onGuideStartPresentation()");
			//gyro = false;
			//pano.play();
			
			if(!Model.state.active)
				Model.state.active = true;
			
		}
		

		
		public static function toDegrees(radians:Number):Number {
			return (radians * 180 / Math.PI);
		}
		
		private function onPanoStartAutorotate(event:Event):void
		{
			Log.record("SWF_mobile.onPanoStartAutorotate()");
			
			Model.state.active = true;
			
			//if(Model.state.connectMode != State.CONNECT_MODE_RECEIVE)
				//setTimeout(hideUI,1000);
			
		}
		
		private function onPanoStopAutorotate(event:Event):void
		{
			Log.record("SWF_mobile.onPanoStopAutorotate()");
			
	
		}
		
		
		private function onIntroComplete(event:Event):void
		{
			Log.record("SWF_mobile.onIntroComplete()");
			introComplete = true;
			//guide.visible = true;
			//Model.state.guideView = State.GUIDE_VIEW_CLOSED;
			
		}
		
		
		private var videoBackground:Sprite = new Sprite();
		
		
		private function onHotspotShowWeb(event:HotspotEvent):void
		{
			showWebView(event.hotspot);
		}
		
		private var webView:StageWebView = new StageWebView(); 
		
		private function showWebView(data:HotspotData):void
		{
			trace("swf_mobile.showWebView()");
			var photoData:PhotoData = data as PhotoData;
			
			webView.viewPort = new Rectangle( stageWidth/2 - data.inWorldWidth/2, stageHeight/2 - data.inWorldHeight/2, data.inWorldWidth, data.inWorldHeight); 
			webView.loadURL(photoData.webUrl);
			webView.addEventListener(Event.COMPLETE, addStageWebViewtoStage);
		}
		
		private function addStageWebViewtoStage(event:Event):void
		{
			webView.stage = this.stage; 
		}
		
		private function onHotspotCloseWeb(event:HotspotEvent):void
		{
			webView.stage = null;
		}
		
		private function reportKeyDown(event:KeyboardEvent):void 
		{ 
			//space bar - next
			if(event.charCode == 32){
				Model.progressNext();
			
					
			}
		} 
		

		
		private function onNetworkData(event:NetworkDataEvent):void
		{
			var obj = event.object;
			
			if(!obj)
				return;
			
			if(!obj.type)
				return;
			
			if(obj.guideId == Model.state.guideId
				&& obj.sceneId && Model.state.mode == State.MODE_REMOTE
				&& Model.state.guideView == State.GUIDE_VIEW_CLOSED)
				if(obj.sceneId != Model.currentScene.id || null){
					Model.loadGuideScene(obj.sceneId);
				}
			
		//	if(obj.type == "bookmark")
			//	bookmarks.bookmarkScene();
			else if(obj.type == "gyro" || obj.type == "drag" 
				&& Model.state.mode == State.MODE_REMOTE
				&& Model.state.guideView == State.GUIDE_VIEW_CLOSED)
				pano.remoteUpdate(obj);
		}
		
		private var networkManagerOpen:Boolean;
		
		private function onConnectButtonMouseUp(e:MouseEvent = null):void{
			Log.record('swf_mobile.onConnectButtonMouseUp()');
			

			closeButton();
			
			networkManagerOpen = true;
			
			
		}
		
		
		
		private function closeButton(value:Boolean = false):void
		{
			if(!value){
				closeVRButton.addEventListener(MouseEvent.MOUSE_UP, onCloseMouseUp);
				closeVRButton.x = stageWidth - closeVRButton.width;
				closeVRButton.visible = true;
				addChild(closeVRButton);
			}
			else{
				closeVRButton.removeEventListener(MouseEvent.MOUSE_UP, onCloseMouseUp);
				removeChild(closeVRButton);
			}
		}
		
		private function onCloseMouseUp(e:MouseEvent = null):void{
			Log.record('onCloseMouseUp()');
			

			 if(guidesHolder){
				black.tween(0);
				guidesHolder.removeChildren();
			}
				
		
		
			closeVRButton.removeEventListener(MouseEvent.MOUSE_UP, onCloseMouseUp);
			removeChild(closeVRButton);
		}
		
		private function sendDragData():void{
			
			if(!pano.currentPanAngle)
				return;
			
			dragObject.type = "drag";
			dragObject.pan = pano.currentPanAngle > 0
				? pano.currentPanAngle
				: 360 + pano.currentPanAngle;
			dragObject.tilt = pano.currentTiltAngle;
			dragObject.zoom = pano.zoom;
			dragObject.sceneId = Model.currentScene.id || Model.guideData.firstScene.id;
			sendData(dragObject);
		}
		
		private function sendData(obj){
			
			if(!Model.state.connected)
				return;
			
			trace("sendData()");
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_WIFI)
					WifiManager.sendData(obj);
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_INTERNET)
				WebSocketManager.sendData(obj);
		}
		
		private function onNetworkConnect(e:Event):void
		{
			Log.record("swf_mobile.onNetworkConnect()");
			onModeChange();
			
		}
		
		private function onVRModeChange(e:Event):void
		{
			Log.record("swf_mobile.onVRModeChange");
			//stage3DProxy.removeEventListener(Event.ENTER_FRAME, onEnterFrame);

		}
		
		private function onModeChange(e:Event = null):void
		{
			
			clearInterval(controlInt);	
			
			switch(Model.state.mode){

									
				case State.MODE_DRAG:{
					
					if(Model.state.connected)
						controlInt = setInterval(sendDragData, 10);
					break;
				}
					
			}
			
		}
		
		private function onBlackMouseUp(e:Event):void
		{
			black.removeEventListener(MouseEvent.MOUSE_UP, onBlackMouseUp);
			
		}
		

		
		private function stage_resizeHandler(event:Event):void
		{
			this._starling.stage.stageWidth = this.stage.stageWidth;
			this._starling.stage.stageHeight = this.stage.stageHeight;
			
			resize(stage.stageWidth,stage.stageHeight);
			
			var viewPort:Rectangle = this._starling.viewPort;
			viewPort.width = this.stage.stageWidth;
			viewPort.height = this.stage.stageHeight;
			try
			{
				this._starling.viewPort = viewPort;
			}
			catch(error:Error) {}
		}
		
		private function stage_deactivateHandler(event:Event):void
		{
			//this._starling.stop(true);
			this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:Event):void
		{
			this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
			//this._starling.start();
		}
		
		
		
		
		
	}
}