package co.beek
{
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import co.beek.mobile.MobileConfig;
	import co.beek.model.Model;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.StringUtil;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;

	public class GuideUnzipProgressPanel extends ProgressPanelFl
	{
		public static const COMPLETE:String = "COMPLETE";
		
		private var _guide:GuideBasicData;
		
		private var zip:FZip;
		private var _zipIndex:int;
		
		public function GuideUnzipProgressPanel()
		{
			super();
		}
		
		public function downloadUnzip(guide:GuideBasicData):void
		{
			_guide = guide;
			extractZipToCache(Model.config.assetCdn+"/"+guide.zipName);
		}
		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
		
		private function extractZipToCache(guideZipUrl:String):void
		{
			zip = new FZip();
			zip.addEventListener(Event.COMPLETE, onGuideLoadComplete);
			zip.addEventListener(ProgressEvent.PROGRESS, onGuideLoadProgress);
			zip.load(new URLRequest(guideZipUrl));
		}
		
		private function onGuideLoadProgress(event:ProgressEvent):void
		{
			var percent:Number = event.bytesLoaded/event.bytesTotal;
			progressField.text = StringUtil.bytesToMbString(event.bytesLoaded);
			progressBar.width = percent * (progressTrack.width - progressBar.x*2);
		}
		
		private function onGuideLoadComplete(event:Event):void
		{
			trace("onGuideLoadComplete");
			ColorUtil.colorize(progressBar, 0x0099FF);
			zipIndex = 0;
		}
		
		private function set zipIndex(value:int):void
		{
			_zipIndex = value;
			var total:int =zip.getFileCount();
			if(_zipIndex < total)
			{
				var limit:int = Math.min(zip.getFileCount(), _zipIndex+10);
				for(var i:uint = _zipIndex; i < limit; i++)
				{
					var fzipFile:FZipFile = zip.getFileAt(i);
					
					var file:File = MobileConfig.localStorage.resolvePath(fzipFile.filename);
					var fileStream:FileStream = new FileStream();
					fileStream.open(file, FileMode.WRITE);
					fileStream.writeBytes(fzipFile.content, 0, fzipFile.content.length);
					fileStream.close();
				}
				_zipIndex = i;
				
				var percent:Number = _zipIndex/total;
				progressField.text = "Unpacking "+_zipIndex+" of "+total;
				progressBar.width = percent * (progressTrack.width - progressBar.x*2);
				
				setTimeout(loadNextZipFile, 200);
			}
			else
			{
				zip = null;
				dispatchEvent(new Event(COMPLETE));
			}
		}
		
		private function loadNextZipFile():void
		{
			zipIndex = _zipIndex+1;
		}
	}
}