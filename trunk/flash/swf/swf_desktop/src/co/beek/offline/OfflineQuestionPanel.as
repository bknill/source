package co.beek.offline
{
	import co.beek.Fonts;
	import co.beek.model.guide.GuideBasicData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class OfflineQuestionPanel extends OfflineGuidePanelFl
	{
		public static const NO:String = "NO";
		public static const YES:String = "YES";
		
		private var _guide:GuideBasicData;
		
		public function OfflineQuestionPanel(guide:GuideBasicData, question:String)
		{
			super();
			_guide = guide;
			
			Fonts.format(textField, Fonts.bodyFont);
			textField.text = question;
			
			noButton.addEventListener(MouseEvent.CLICK, onNoButtonClick);
			yesButton.addEventListener(MouseEvent.CLICK, onYesButtonClick);
		}
		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
		
		private function onNoButtonClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(NO));
		}

		private function onYesButtonClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(YES));
		}
		
		public function dispose():void
		{
			_guide = null;
		}
	}
}