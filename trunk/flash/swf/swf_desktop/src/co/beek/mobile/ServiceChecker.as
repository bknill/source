package co.beek.mobile
{
	import co.beek.Log;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	public class ServiceChecker extends EventDispatcher
	{
		public static const CHECKED:String = "CHECKED";
		
		private var urlRequest:URLRequest = new URLRequest("http://gms.beek.co/user/session/");
		
		private	var loader:URLLoader = new URLLoader();

		private var checkedEvent:Event = new Event(CHECKED);
		
		private var _available:Boolean;
		
		public function ServiceChecker()
		{
			super();
			
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			
			loader.addEventListener(Event.COMPLETE, onDataLoaded);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
		}
		
		public function get available():Boolean
		{
			return _available;
		}
		
		public function start():void
		{
			_available = false;
			try
			{
				loader.load(urlRequest);
			}
			catch ( e:Error )
			{
				dispatchEvent(checkedEvent);
			}
		}
		
		private function onDataLoaded(event:Event):void
		{
			Log.record("ServiceChecker.onDataLoaded()")
			var json:String = URLLoader(event.target).data;
			try{
				if(json.charAt(0) == "{")
				{
					var data:Object = JSONBeek.decode(json);
					var response:ServiceResponse = new ServiceResponse(data);
					
					_available = true;
				}
			}
			catch ( e:Error )
			{
				// should rarely get here
				Log.record("ServiceChecker error:"+json)
			}
			
			dispatchEvent(checkedEvent);
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			dispatchEvent(checkedEvent);
		}
	}
}