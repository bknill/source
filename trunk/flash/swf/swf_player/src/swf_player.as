package
{
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.ContextMenuEvent;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Security;
	import flash.system.fscommand;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.ui.Keyboard;
	
	import co.beek.Config;
	import co.beek.FavouriteEmailPanel;
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.Player;
	import co.beek.admin.IAdmin;
	import co.beek.login.LoginForm;
	import co.beek.login.LoginService;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.events.StringEvent;
	import co.beek.model.events.TrackingEvent;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.pano.Pano;
	import co.beek.service.GuideService;
	import co.beek.service.ServiceLoader;
	import co.beek.service.VisitService;
	import co.beek.session.SessionData;
	
	[SWF(frameRate="60")]
	public class swf_player extends Sprite
	{
		private static const STAGE_SWF_LOADED:int = 1;
		private static const STAGE_GUIDE_LOADED:int = 2;
		private static const STAGE_SCENE_LOADED:int = 3;
		
		private var loginForm:LoginForm = new LoginForm;
		
		private var adminLoader:Loader;
		private var admin:IAdmin;
		private var htmlUpdate:Boolean;
		
		private var player:Player = new Player;
		
		public function swf_player()
		{
			super();
			Log.record("swf_player()");
			
			// Add error handling
			uncaughtErrorEvents.addEventListener("uncaughtError", onGlobalErrors);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onPlayerSwfIOError);
			
			// Set up the stage
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, onStageResize);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			// Data comes first
			Model.config = new Config(loaderInfo.parameters);
			Model.addEventListener(TrackingEvent.TRACK_EVENT, onTrackEvent);
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.GUIDE_SAVED, onGuideSaved);
			Model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			Model.addEventListener(Model.SESSION_CHANGE, onSessionDataChange);
			
			
			Model.state.networkAccess = true;
			Model.state.addEventListener(State.GUIDE_ID_CHANGE, onGuideIdChange);
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE, onSceneLoadingChange);
			
			var cm:ContextMenu = new ContextMenu();
			cm.hideBuiltInItems();
			var cmLink:ContextMenuItem = new ContextMenuItem("Beek virtual guide "+Model.config.version );
			cmLink.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onContextMenuClick );
			if( cm.customItems != null ) cm.customItems.push( cmLink );
			this.contextMenu = cm;
			
			Model.state.addEventListener(State.PANO_TILES_LOADED, onPanoTilesLoaded);
			player.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb);
			player.addEventListener(HotspotEvent.HOTSPOT_CLOSE_WEB, onHotspotCloseWeb);
			ExternalInterface.addCallback("update", update); 
			ExternalInterface.addCallback("sceneChange", sceneChange); 
			ExternalInterface.addCallback("taskChange", taskChange); 
			ExternalInterface.addCallback("loadAdmin", loadAdmin); 
			ExternalInterface.addCallback("unloadAdmin", unloadAdmin); 
			
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			addChild(player);
			
			
			if(Model.config.guideId)
				Model.state.guideId = Model.config.guideId;
				
			else
				loadSceneData(Model.config.sceneId);
				
			
			// remove the extras right away
			if(autoLogin)
			{
				LoginService.loadSession(onSessionDataLoaded);
			}
			resizeToStage();
			
			ExternalInterface.call("setSwfStage", STAGE_SWF_LOADED);
			
			VisitService.email = loaderInfo.parameters["email"];
			if(VisitService.email)
				ExternalInterface.call("trackVisitKey", VisitService.key);
			else
				VisitService.addEventListener(VisitService.FAVOURITE_EMAIL_SUBMITTED, onFavouriteEmailSubmitted);
			
			VisitService.addEventListener(VisitService.FAVOURITE_EMAIL_REQUIRED, onFavouriteEmailRequired);
		}
		
		private function onContextMenuClick( event:Event ):void
		{
			navigateToURL( new URLRequest( "http://www.beek.co" ), "_blank" );
		}
		
		private function get autoLogin():Boolean
		{
			return loaderInfo.parameters['autologin'] == "true";
		}
		
		private function onSessionDataChange(event:Event):void
		{
			if(stage.stageWidth < Constants.MIN_STAGE_WIDTH)
				return;
			trace('swf_player.onSessionDataChange()');
			trace(Model.isEditableScene);
			if(Model.isEditableGuide || Model.isEditableScene)
				loadAdmin();
			
			else if(adminLoaded)
				unloadAdmin();
		}
		
		private function get adminLoaded():Boolean
		{
			return adminLoader != null && adminLoader.parent == this;
		}
		
		private function loadGuideData(guideId:String):void
		{
			GuideService.getGuideJson(guideId, onGuideDataLoaded);
		}
		
		private function onGuideDataLoaded(guideData:GuideData):void
		{
			trace('swf_player.onGuideDataLoaded()');
			if(guideData.font == 0)
			{
				Fonts.titleFont = new TitleFontFl().fontName;
				Fonts.bodyFont = new BodyFontFl().fontName;
				Fonts.boldFont = new BoldFont2Fl().fontName;
			}
			else
			{
				Fonts.titleFont = "_sans";
				Fonts.bodyFont = "_sans";
				Fonts.boldFont = "_sans";
			}
			
			Model.guideData = guideData;
			//loadSceneData(guideData.firstScene.scene.id);
			
			//if(Model.isEditableGuide)
				//loadAdmin();

		}
		
		private var sceneDetailLoader:ServiceLoader;
		
		private function loadSceneData(sceneId:String):void
		{
			if(sceneDetailLoader)
				sceneDetailLoader.dispose();
			
			//var loading:SceneBasicData = new SceneBasicData({"id" : });
			
			var url:String = Model.config.getSceneJsonUrl(sceneId);
			sceneDetailLoader = new ServiceLoader(SceneData);
			sceneDetailLoader.load(url, onSceneDataLoaded);
		}
		
		private function onSceneDataLoaded(scene:SceneData):void
		{
			sceneDetailLoader.dispose();
			sceneDetailLoader = null;
			Model.currentScene = scene;
			//Model.history.currentScene = scene;
			if(Model.isEditableScene)
				loadAdmin();
		}
		
		private function onSessionDataLoaded(data:SessionData):void
		{
			Model.sessionData = data;
		}
		
		private function get uncaughtErrorEvents():Object
		{
			return loaderInfo.hasOwnProperty('uncaughtErrorEvents')
				? loaderInfo['uncaughtErrorEvents'] : this;
		}
		
		private function onStageResize(event:Event):void
		{
			resizeToStage();
		}
		
		private function resizeToStage():void
		{
			if(stage.stageWidth == 0 || stage.stageHeight == 0)
				return;
			
			player.resize(stage.stageWidth, stage.stageHeight);
			
			if(admin != null)
				admin.resize(stage.stageWidth, stage.stageHeight);
			
			if(stage.stageWidth < Constants.MIN_STAGE_WIDTH)
				showEmbedMode()
			else
				showNormalMode();
		}
		
		
		private function onKeyDown(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.F1)
				showLoginForm();

			if(event.keyCode == Keyboard.SPACE && !Model.loggedIn)
			{
		//		player.bookcase.visible = false;
				Model.progressNext();
			}
		}
		
		private function onPlayerSwfIOError(event:Event):void
		{
			Log.record("IO error loading player swf");
		}
		
		private function onLoadedSwfErrors(event:Event):void
		{
			onGlobalErrors(event);
		}
		
		private function onGlobalErrors(event:Event):void
		{
			var message:String
			
			// start with the url for this error
			if(Model.guideData && Model.currentScene)
				message = Model.guideSceneUrl + "\n\n";

			//else if(Model.guideData)
			//	message = Model.guideUrl+ "\n\n";
			
			else if(Model.currentScene)
				message = Model.guideSceneUrl+ "\n\n";
			
			// append the error message
			if (event['error'] is Error)
				message += Error(event['error']).message + "\n\n";
				
			else if (event['error'] is ErrorEvent)
				message += ErrorEvent(event['error']).text+ "\n\n";
				
			else if(event['error'])
				message += event['error'].toString()+ "\n\n";
			
			// append the last 40 actions
			message += Log.actions.join("\n");
			
			if(admin)
				admin.showError(message);
			
			Log.logError(message);
		}
		
		private function onSceneChange(event:Event):void
		{
			Log.record("swf_player.onSceneChange");
			
		}
		
		private function onGuideChange(event:Event):void
		{
			Log.record("swf_player.onGuideChange");
			ExternalInterface.call("setSwfStage", STAGE_GUIDE_LOADED);
			
			
			if(Model.state.htmlUpdate)
				return;
			
			var load:SceneBasicData;
			
			if(Model.guideData.id == Model.config.guideId && Model.config.sceneId != null)
				load = Model.guideData.getGuideSceneFromId(Model.config.sceneId).scene;
			
			if(load == null && Model.guideData.firstScene)
				load = Model.guideData.firstScene.scene;
			
			// We have just loaded the first guide, loading the scene specified by the config
			if(load != null)
				Model.state.loadingScene = load;
			// no scenes specified, we are done
			else
			{
				
				ExternalInterface.call("setSwfStage", STAGE_SCENE_LOADED);
			}
		}
		
		private function onGuideIdChange(event:Event):void
		{
			trace('swf_player.OnGuideChange()');
			loadGuideData(Model.state.guideId);
		}
		
		private function onSceneLoadingChange(event:Event):void
		{
			if(!sceneLoadedCalled)
			{
				var guideScene:GuideSceneData = Model.guideData 
					? Model.guideData.getGuideSceneFromId(Model.state.loadingScene.id) : null;
				
				if(guideScene && guideScene.transitionVideo)
					callJavascriptSceneLoaded();
			}
			
			loadSceneData(Model.state.loadingScene.id);
			
			
		}
		
		/**
		 * Tracks a page view as if it is within the guide.
		 * I.e, all the scene are child pages of guide
		 *  /g12/s654
		 */
		private function trackPageView():void
		{
			// Dont track page views of logged in users
			if(Model.sessionData || !Model.guideData || !Model.currentScene)
				return;
			
			// Track the url as '/g123/s345'
			ExternalInterface.call("trackSceneView", Model.currentScene.id);
		}
		
		private function loadAdmin(value:String = null):void
		{
			Model.state.uiState = State.UI_STATE_NORMAL;
			
			if(adminLoader != null)
				return;
			
			var adminUrl:String = Model.config.adminSwf;
			
			if(adminUrl.indexOf("beekdev") > -1)
				adminUrl += "?rand="+Math.random();
			

			adminLoader = new Loader();
			adminLoader.load(new URLRequest(adminUrl));
			adminLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			adminLoader.addEventListener("mx.managers.SystemManager.isBootstrapRoot", systemManagerHandler);
			adminLoader.addEventListener("mx.managers.SystemManager.isStageRoot", systemManagerHandler);
			
			addChild(adminLoader);
		}
		
		private function unloadAdmin(value:String = null):void
		{
			adminLoader.visible = false;
			removeChild(adminLoader);
			adminLoader = null;
			admin = null;
			
			//player.header.visible = true;
		//	player.bookcase.visible = true;
		}
		
		private function onLoadComplete(event:Event):void 
		{ 
			var loader:Loader = LoaderInfo(event.target).loader;
			loader.content.addEventListener("applicationComplete", onApplicationComplete);
		}
		
		private function onApplicationComplete(event:Event):void 
		{ 
			admin = IAdmin(event.target.application);
			admin.player = player;
			admin.resize(stage.stageWidth, stage.stageHeight);
		}
		
		private function systemManagerHandler(event:Event):void 
		{ 
			event.preventDefault(); 
		}
		
		/**
		 * Event handler for when the Model dispatches a 
		 * google analytics tracking event.
		 */
		private function onTrackEvent(event:TrackingEvent):void 
		{
			if(Model.loggedIn || Model.currentScene == null)
				return;
			
			ExternalInterface.call(
				"trackEvent", 
				event.category, 
				event.action, 
				event.label+"|"+Model.currentScene.id
			);
		}
		
		/**
		 * A pano scene has completed loading
		 */
		private function onPanoTilesLoaded(event:Event):void 
		{
			trace('swf_player.onPanoTilesLoaded()');
			if(!sceneLoadedCalled)
				callJavascriptSceneLoaded();
			
			// track the page view once hte pano is visible
			trackPageView();
			
			Model.currentScene.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb);
		}
		
		private var sceneLoadedCalled:Boolean;
		
		private function callJavascriptSceneLoaded():void
		{
			ExternalInterface.call("setSwfStage", STAGE_SCENE_LOADED);
			sceneLoadedCalled = true;
		}
		
		private function showNormalMode():void
		{
			Model.state.mute = false;
		}
		
		private function showEmbedMode():void
		{
			Model.state.mute = true;
		}
		
		private function showLoginForm():void
		{
			loginForm.x = Math.round((stage.stageWidth - loginForm.width)/2); 
			loginForm.y = Math.round((stage.stageHeight - loginForm.height)/2);
			loginForm.addEventListener(LoginForm.HIDE, onLoginFormClose);
			
			if(!Model.sessionData)
				loginForm.showLoginForm();
			else
				loginForm.showLogoutForm();
			
			addChild(loginForm);
			//Pano(player.pano).stopAutoRotate();
		}
		
		private function onLoginFormClose(event:Event):void
		{
			if(loginForm.parent == this)
				removeChild(loginForm);
			
			//if(!Model.hotspot)
				//Pano(player.pano).queueAutoRotate(5000);
		}

		
		private function onFavouriteEmailSubmitted(event:Event):void
		{
			ExternalInterface.call("trackVisitKey", VisitService.key);
		}
		
		private function onFavouriteEmailRequired(event:StringEvent):void
		{
			var emailForm:FavouriteEmailPanel = new FavouriteEmailPanel(event.value);
			emailForm.x = Math.round((stage.stageWidth - emailForm.width)/2); 
			emailForm.y = Math.round((stage.stageHeight - emailForm.height)/2);
			emailForm.addEventListener(FavouriteEmailPanel.CLOSE, onFavouriteEmailPanelClose);
			
			addChild(emailForm);
			
		//	Pano(player.pano).stopAutoRotate();
		}
		
		private function onFavouriteEmailPanelClose(event:Event):void
		{
			var emailForm:FavouriteEmailPanel = FavouriteEmailPanel(event.target);
			
			if(emailForm.emailAddress)
			{
				VisitService.email = emailForm.emailAddress;
				VisitService.favourite(emailForm.favouriteText);
			}
			
			removeChild(emailForm);
			
			//if(!Model.hotspot)
				//Pano(player.pano).queueAutoRotate(5000);
		}
		
		private function onHotspotShowWeb(event:HotspotEvent):void
		{
			showWebView(event.hotspot);
		}
		
		private function showWebView(data:HotspotData):void
		{
			var photoData:PhotoData = data as PhotoData;
			
			fscommand("openWindow",
				photoData.webUrl + "|" +
				data.inWorldWidth + "|" +
				data.inWorldHeight
			);
		}
		
		private function onHotspotCloseWeb(event:HotspotEvent):void
		{
			fscommand("closeWindow");
		}
		
		public function update(value:String = null):void
		{
			Model.state.htmlUpdate = true;

			loadGuideData(Model.config.guideId);

		}
		
		public function sceneChange(id:String = null):void
		{
			Model.loadGuideScene(id);
		}
		
		public function taskChange(id:String = null):void
		{
			Model.loadGameTask(id);
		}
		
		private function onGuideSaved(event:Event):void
		{
			ExternalInterface.call("guideSaved");
			//fscommand("guideSaved");
		}
		
		
	}
}