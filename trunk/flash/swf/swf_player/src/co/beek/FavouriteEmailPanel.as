package co.beek
{
	import co.beek.model.Model;
	
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class FavouriteEmailPanel extends FavouriteEmailPanelFl
	{
		public static const CLOSE:String = "CLOSE";
		
		private var _favouriteText:String;
			
		public function FavouriteEmailPanel(text:String)
		{
			super();
			_favouriteText = text;
			doneButton.addEventListener(MouseEvent.CLICK, onDoneButtonClick);
			closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
		}
		
		public function get favouriteText():String
		{
			return _favouriteText;
		}
		
		public function get emailAddress():String
		{
			return emailField.text != "" && emailField.text.indexOf("@") > 0 ? emailField.text : null;
		}
		
		private function onDoneButtonClick(event:MouseEvent):void
		{
			dispatchEvent(new Event(CLOSE));
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			emailField.text = "";
			dispatchEvent(new Event(CLOSE));
		}
	}
}