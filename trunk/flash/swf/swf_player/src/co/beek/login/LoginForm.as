package co.beek.login
{
	import co.beek.model.Model;
	import co.beek.session.SessionData;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFieldType;
	import flash.utils.setTimeout;

	public class LoginForm extends LoginPanelFl
	{
		public static const HIDE:String = "HIDE";
		
		public function LoginForm()
		{
			super();
			form.loginButton.addEventListener(MouseEvent.CLICK, onLoginButtonClick);
			details.logoutButton.addEventListener(MouseEvent.CLICK, onLogoutButtonClick);
			removeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClick);
		}
		
		public function showLoginForm():void
		{
			form.emailField.text = Model.config.domain.indexOf("beekdev") > -1 ? "admin@beek.co" : "";
			form.passwordField.text =  Model.config.domain.indexOf("beekdev") > -1 ? "KeyJn8mw" : "";
			form.passwordField.displayAsPassword = true;
			form.visible = true;
			pending.visible = false;
			details.visible = false;
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			hideLoginPanel();
		}
		
		private function onLoginButtonClick(event:MouseEvent):void
		{
			LoginService.login
			(
				form.emailField.text, 
				form.passwordField.text, 
				onSessionData
			);
			form.emailField.text = ""; 
			form.passwordField.text = ""; 
			form.visible = false;
			
			pending.visible = true;
			pending.field.text = "Logging in, please wait...";
		}
		
		private function onSessionData(data:SessionData):void
		{
			Model.sessionData = data;
			showLogoutForm();
			
			setTimeout(hideLoginPanel, 2000);
		}
		
		public function showLogoutForm():void
		{
			details.nameField.text = Model.sessionData.contact.fullName;
			
			form.visible = false;
			pending.visible = false;
			details.visible = true;
		}
		
		private function hideLoginPanel():void
		{
			dispatchEvent(new Event(HIDE));
		}
		
		private function onLogoutButtonClick(event:MouseEvent):void
		{
			LoginService.logout(onLogoutComplete);
			details.visible = false;
			pending.visible = true;
			pending.field.text = "Logging out, please wait...";
		}
		
		private function onLogoutComplete(data: Object):void
		{
			Model.sessionData = null;
			pending.field.text = "Log out complete.";
			setTimeout(hideLoginPanel, 2000);
		}
	}
}