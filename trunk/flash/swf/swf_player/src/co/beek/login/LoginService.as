package co.beek.login
{
	import co.beek.model.Model;
	import co.beek.service.ServiceLoader;
	import co.beek.session.SessionData;
	
	import flash.net.URLVariables;

	public class LoginService
	{
		public static function loadSession(callback:Function):void
		{
			var url:String = Model.config.serviceUrl+"/user/session/";
			var loader:ServiceLoader = new ServiceLoader(SessionData);
			loader.load(url, callback);
		}

		public static function login(emailaddress:String, password:String, callback:Function):void
		{
			var data:URLVariables = new URLVariables;
			data['email'] = emailaddress;
			data['pass'] = password;
			
			var url:String = Model.config.serviceUrl+"/user/login/";
			var loader:ServiceLoader = new ServiceLoader(SessionData);
			loader.postData = data;
			loader.load(url, callback);
		}

		public static function logout(callback:Function):void
		{
			var url:String = Model.config.serviceUrl+"/user/logout/";
			var loader:ServiceLoader = new ServiceLoader();
			loader.load(url, callback);
		}
	}
}