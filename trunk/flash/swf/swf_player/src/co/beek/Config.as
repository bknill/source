package co.beek
{
	import co.beek.model.Constants;
	import co.beek.model.IConfig;
	import co.beek.model.data.SceneBasicData;
	
	import flash.events.EventDispatcher;
	
	public class Config extends EventDispatcher implements IConfig
	{
		private static const TILE_SERVERS:Array = ["a", "b", "c"];
		
		protected var _data:Object;
		
		public function Config(data:Object)
		{
			_data = data;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		public function get device():String 
		{ 
			return Constants.WEB;
		}
		
		public function get gaCode():String
		{
			return _data['gaCode'] == null
				? "UA-271709-6"
				:_data['gaCode'];
		}
		
		public function get version():String
		{
			return _data['version'] == null
				? String(new Date().minutes)
				:_data['version'];
		}

		public function get assetCdn():String
		{	return _data['assetCdn'] }

		public function get adminSwf():String
		{	return _data['adminSwf'] }

		public function get domain():String
		{	return _data['domain'] }
		
		public function get serviceUrl():String
		{	return "http://gms."+domain }

		public function get sceneId():String
		{	return _data['sceneId'] }

		public function get guideId():String
		{	return _data['guideId'] }

		public function get hotspotId():String
		{	
			return _data['hotspotId'] != null 
				&& _data['hotspotId'] != "" 
				&& _data['hotspotId'] != "null" 
				? _data['hotspotId'] : null
		}
		
		public function get tourGuideId():String
		{	return _data['tourGuideId'] }

		public function get tourRole():String
		{	return _data['tourRole'] }
		
		public function get mapTileServer():String
		{
			var tileServer:String = TILE_SERVERS[int(Math.random()*(TILE_SERVERS.length-1))];
			return "http://"+tileServer+".tile.cloudmade.com";
		}
		
		public function getGuideJsonUrl(guideId:String):String
		{
			return serviceUrl+"/guides/json/"+guideId;
		}
		
		public function getSceneJsonUrl(sceneId:String):String
		{
			return serviceUrl+"/scene/{sceneId}/json".replace("{sceneId}", sceneId);
		}
		
		public function getAssetUrl(path:String):String
		{
			return path != null ? assetCdn+"/"+path : null;
		}
		
		public function getMapTileUrl(path:String):String
		{
			return mapTileServer + "/" +path;
		}
		
		public function isAvailable(scene:SceneBasicData):Boolean
		{
			return true;
		}
	}
}