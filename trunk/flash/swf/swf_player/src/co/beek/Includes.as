package co.beek
{
	import co.beek.loading.LocationsLoader;
	import co.beek.loading.ScenesLoader;
	import co.beek.model.Constants;
	import co.beek.model.data.PercentData;
	import co.beek.model.guide.GuideRatingData;
	import co.beek.pano.event.HotspotDownEvent;
	import co.beek.utils.BrowserUtil;
	import co.beek.utils.ColorUtil;
	import co.beek.utils.JSONBeek;
	import co.beek.utils.SendBytes;
	import co.beek.utils.StringUtil;

	public class Includes
	{
		private var constants:Constants;
		
		private var colorUtil:ColorUtil;
		private var hotspotDownEvent:HotspotDownEvent;
		private var stringUtil:StringUtil;
		private var sendBytes:SendBytes;
		private var percentData:PercentData;
		private var browserUtil:BrowserUtil;
		
		private var guideRating:GuideRatingData;
		private var jsonBeek:JSONBeek;
		private var scenesLoader:ScenesLoader;
		private var locationsLoader:LocationsLoader;
	}
}