package co.beek.tour
{
	import flash.accessibility.Accessibility;

	public class Actions
	{
		private var _data:Object;
		
		private var _actions:Vector.<Action> = new Vector.<Action>;
		
		public function Actions()
		{
			// do nothing
		}
		
		public function set data(value:Object):void
		{
			_data = value;
			
			// remove all previous actions
			_actions = _actions.splice(0, 0);
			
			var raw:Array = _data['actions'];
			for(var i:int = 0; i<raw.length; i++)
				_actions.push(new Action(raw[i]));
		}
		
		public function get time():int
		{
			return _data ? _data['time'] : 0;
		}

		public function get actions():Vector.<Action>
		{
			return _actions;
		}
	}
}