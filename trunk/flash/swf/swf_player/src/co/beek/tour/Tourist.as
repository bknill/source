package co.beek.tour
{
	import co.beek.model.Model;
	import co.beek.model.data.PanoLookTo;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.getTimer;
	
	public class Tourist extends EventDispatcher
	{
		private var loader:URLLoader = new URLLoader();
		
		private var model:Model;
		
		private var lasttime:int;
		
		private var start:int;
		
		public function Tourist(model:Model)
		{
			super();
			this.model = model;
			//this.model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			
			loader.addEventListener(Event.COMPLETE, onActionDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			loadNextAction();
		}
		
		private function joinGuidedTour():void
		{
			var url:String = model.config.serviceUrl+"/tours/join";
			loader.load(new URLRequest(url));
		}
		
		private function loadNextAction():void
		{
			start = getTimer();
			
			trace("Tourist.loadNextAction()");
			var url:String = model.config.serviceUrl+"/tours/readaction/"+model.config.tourGuideId+"/"+lasttime;
			loader.load(new URLRequest(url));
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			throw new Error("IO Error on tour");
		}
		
		private function onActionDataLoaded(event:Event):void
		{
			trace("onActionDataLoaded(): "+ (getTimer() - start));
			var result:String = unescape(URLLoader(event.target).data);
			
			try{
				if(int(result) > 0)
					lasttime;// = int(result);
				//else
			//		excuteAction(new Action(JSONBeek.decode(result)));
			}
			catch(e:Error)
			{
				trace("error decoding json");
				trace(e.message)
				trace(result);
				throw new Error("error decoding json:"+result);
			}
			
			if(pauseLoading)
				return;
			
			loadNextAction();
		}	
		
		private var pauseLoading:Boolean;
		
		private function excuteAction(action:Action):void
		{
			trace("excuteAction");
			// if the long poll timed out
			if(action.type == 0)
				return;
			
			switch(action.type)
			{
				case Action.TYPE_SCENE_LOAD:
					pauseLoading = true;
					//model.currentSceneId = action.payload;
					break;

				case Action.TYPE_PANO_LOOK:
					model.panoLookTo = PanoLookTo.fromString(action.payload);
					break;

				case Action.TYPE_VIEW_HOTSPOT:
					model.currentHotspot = model.currentScene.getHotspot(action.payload);
					break;
			}
			
			lasttime = action.time;
			
		}
		
		private function onSceneChange(event:Event):void
		{
			if(pauseLoading)
			{
				pauseLoading = false;
				loadNextAction();
			}
			
		}
	}
}