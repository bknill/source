package co.beek.tour
{
	import co.beek.model.Model;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.utils.JSONBeek;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	public class Tour extends EventDispatcher
	{
		private var loader:URLLoader = new URLLoader();
		private var timer:Timer = new Timer(1000);
		
		private var tourDataUrl:String;
		
		private var updated:int;
		
		private var model:Model;
		
		private var timeout:int = -1;
		
		private var _current:TourData;
		
		public function Tour(model:Model)
		{
			super();
			this.model = model;
			
			tourDataUrl = model.serviceUrl+"/tours/json/"+model.tourId;
			
			loader.addEventListener(Event.COMPLETE, onTourDataLoaded); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			
			//if(model.guideData)
				startTour();
			
			//else
			//	model.addEventListener(Model.GUIDE_DATA_LOADED, onGuideLoaded);
		}
		
		private function set current(value:TourData):void
		{
			if(_current)
				_current.removeEventListener(Event.CHANGE, onCurrentChange);
			
			_current = value;
			_current.addEventListener(Event.CHANGE, onCurrentChange);
		}
		
		private function onGuideLoaded(event:Event):void
		{
			startTour();
		}
		
		private function startTour():void
		{
			current = new TourData({
				'scene_id' : model.guideData.location.defaultSceneId
			});
			
			model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			model.addEventListener(Model.HOTSPOT_CHANGE, onHotspotChange);
			model.addEventListener(Model.PANO_LOOK_CHANGE, onOrientationChange);
			timer.start();
		}
		
		private function onTimer(event:TimerEvent):void
		{
			loader.load(new URLRequest(tourDataUrl+"?updated="+updated));
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			trace("onIOError()");
		}
		
		private function onSceneChange(event:Event):void
		{
			_current.sceneId = model.currentScene.id;
		}

		private function onHotspotChange(event:Event):void
		{
			var hs:HotspotData = model.currentHotspot;
			
			if(hs)
				_current.setHotspot(hs.type, hs.id);
			else
				_current.setHotspot(0, null);
		}

		private function onOrientationChange(event:Event):void
		{
			_current.panoLook = model.panoLook;
		}
		
		private function onTourDataLoaded(event:Event):void
		{
			//trace("Tour.onTourDataLoaded");
			if(timeout > -1)
				return;
			
			//trace("onTourDataLoaded()");
			var json:String = unescape(URLLoader(event.target).data);
			var loaded:TourData;
			try{
				loaded = new TourData(JSONBeek.decode(json));
			}
			catch(e:Error)
			{
				trace("error decoding json");
				trace(e.message)
				trace(json);
				throw e;
			}
			
			
			if(loaded.panoLook 
				&& (!_current.panoLook || !_current.panoLook.equals(loaded.panoLook)))
				model.panoLookTo = loaded.panoLook;

			if(loaded.hotspotId != _current.hotspotId)
				model.currentHotspot = model.currentScene.getHotspot(loaded.hotspotType, loaded.hotspotId);
			
			
			current = loaded;
		}
		
		private function onCurrentChange(event:Event):void
		{
			if(timeout > -1)
				return;
			
			timeout = setTimeout(update, 50);
		}
		
		private function update():void
		{
			trace("update()"+timeout);
			
			var url:String = model.serviceUrl+"/tours/update/"+model.tourId;
			var vars:URLVariables = new URLVariables();
			vars['data'] = escape(JSONBeek.encode(_current.data));
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = vars;
			
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onUpdateComplete);
			loader.load(request);
		}
		
		private function onUpdateComplete(event:Event):void 
		{
			timeout = -1;
			trace("onUpdateComplete()")
			trace(event.target.data);
		}
			
			
	}
}