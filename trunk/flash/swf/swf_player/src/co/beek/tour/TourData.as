package co.beek.tour
{
	import co.beek.model.data.PanoLook;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class TourData extends EventDispatcher
	{
		public var _data:Object;
		
		public function TourData(data:Object)
		{
			super();
			this._data = data;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		public function get updated():int
		{
			return _data['updated'];
		}

		public function get sceneId():String
		{
			return _data['scene_id'];
		}

		public function set sceneId(value:String):void
		{
			if(sceneId == value)
				return;
			
			_data['scene_id'] = value;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get hotspotType():int
		{
			return _data['hs_type'];
		}

		public function get hotspotId():String
		{
			return _data['hs_id'];
		}

		public function setHotspot(type:int, id:String):void
		{
			if(hotspotType == type && hotspotId == id)
				return;
			
			_data['hs_type'] = type;
			_data['hs_id'] = id;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function set panoLook(value:PanoLook):void
		{
			if(panoLook && panoLook.equals(value))
				return;
			
			_data['pan'] = value.pan;
			_data['tilt'] = value.tilt;
			_data['fov'] = value.fov;
			
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get panoLook():PanoLook
		{
			if(isNaN(_data['pan']) || isNaN(_data['tilt']) || isNaN(_data['fov']))
				return null;
			
			return new PanoLook(
				_data['pan'], 
				_data['tilt'], 
				_data['fov']
			) 
		}
		
			
			
	}
}