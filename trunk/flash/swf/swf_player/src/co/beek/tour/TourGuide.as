package co.beek.tour
{
	import co.beek.model.Model;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.getTimer;
	
	public class TourGuide extends EventDispatcher
	{
		private var loader:URLLoader;
		
		private var request:URLRequest;
		
		private var model:Model;
		
		private var prev:Action;
		
		public function TourGuide(model:Model)
		{
			super();
			this.model = model;
			
			//model.addEventListener(Model.SCENE_CHANGE, onSceneChange);
			model.addEventListener(Model.HOTSPOT_CHANGE, onHotspotChange);
			model.addEventListener(Model.PANO_LOOK_TO_CHANGE, onPanoLookToChange);
			
			request = new URLRequest(model.config.serviceUrl+"/tours/writeaction");
			request.method = URLRequestMethod.POST;
			request.data = new URLVariables();
			
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onSendActionComplete); 
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			startGuidedTour();
		}
		
		private function startGuidedTour():void
		{
			var url:String = model.config.serviceUrl+"/tours/start";
			loader.load(new URLRequest(url));
		}
		
		private function onIOError(event:IOErrorEvent):void
		{
			throw new Error("IO Error on tour");
		}
		
		private function onSceneChange(event:Event):void
		{
			sendAction(Action.create(Action.TYPE_SCENE_LOAD, model.state.loadingScene.id));
		}
		
		private function onHotspotChange(event:Event):void
		{
			var payload:String = model.currentHotspot ? model.currentHotspot.uid : null;
			sendAction(Action.create(Action.TYPE_VIEW_HOTSPOT, payload));
		}

		private function onPanoLookToChange(event:Event):void
		{
			sendAction(Action.create(Action.TYPE_PANO_LOOK, model.panoLook.toString()));
		}
		
		private var start:int;
		
		private function sendAction(action:Action):void
		{
			trace("sendAction(action:"+action.type+")");
			start = getTimer();
			
			request.data['type'] = action.type;
			request.data['time'] = getTimer();
			request.data['payload'] = action.payload;
			
			loader.load(request);
		}
		
		private function onSendActionComplete(event:Event):void 
		{
			trace("onSendActionComplete(): "+ (getTimer() - start));
			trace(loader.data);
		}
			
			
	}
}