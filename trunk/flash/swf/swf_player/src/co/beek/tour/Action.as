package co.beek.tour
{
	

	/**
	 * {"id":"1","tour_id":"1","time":"123456789","uid":"123","payload":"{\"mydata\":132}"}
	 */
	public class Action
	{
		public static function create(type:int, payload:String):Action
		{
			return new Action({
				"type" : type,
				"payload" : payload
			});
		}
		
		public static const TYPE_SCENE_LOAD:int = 1;

		public static const TYPE_VIEW_HOTSPOT:int = 2;

		public static const TYPE_PANO_LOOK:int = 3;
		
		private var _data:Object;
		
		private var _json:Object;
		
		public function Action(data:Object)
		{
			_data = data;
		}
		
		public function get time():int
		{
			return _data['time'];
		}

		public function get clientId():int
		{
			return _data['id'];
		}
		
		public function get type():int
		{
			return _data['type'];
		}
		
		public function get payload():String
		{
			return _data['payload'];
		}
	}
}