


This extension was built by distriqt // 

# Device Motion

Device motion is an extension that you can use for getting updates about the position or more precisely the orientation of the device in 3D space.

The extension calculates and reports angles by use of various sensor fusion algorithms which combine the available sensors into orientation and rotational information. The quality of this information depends mainly on the hardware contained in the current device. Generally we require 3 sensors for these calculations: an accelerometer, a magnetometer and a gyroscope.

The extension works by registering for notifications of device motion. When you register for updates you specify a series of options about the updates which the extension use to determine the algorithm that is used to calculate the device orientation, and also the information format that is returned.

Features

- Provides access to the orientation of the device in 3D space
- Uses sensor fusion algorithms to combine information from the accelerometer, magnetometer and gyroscope
- Works across iOS and Android with the same code
- Sample project code and ASDocs reference


## Documentation

Latest documentation:

http://airnativeextensions.com/extension/com.distriqt.DeviceMotion

```actionscript
DeviceMotion.service.addEventListener( DeviceMotionEvent.UPDATE_QUATERNION, deviceMotion_updateHandler );

var options:DeviceMotionOptions = new DeviceMotionOptions();
options.rate 		= SensorRate.SENSOR_DELAY_NORMAL;
options.algorithm 	= DeviceMotionOptions.ALGORITHM_NATIVE;
options.format 		= DeviceMotionOptions.FORMAT_QUATERNION;

DeviceMotion.service.register( options );

private function deviceMotion_updateHandler( event:DeviceMotionEvent ):void
{
	var q:Quaternion = new Quaternion( event.values[1], event.values[2], event.values[3], event.values[0] );
}
```


## License

You can purchase a license for using this extension:

http://airnativeextensions.com

distriqt retains all copyright.

