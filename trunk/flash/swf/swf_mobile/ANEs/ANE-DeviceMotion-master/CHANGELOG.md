

###### 2015.01.31

Updated Euler outputs so angles are continuous through the vertical position (resolves #8)
iOS: Added check for .debug suffix in application id
iOS: Corrected isAlgorithmSupported return value


###### 2014.12.19

iOS: Included arm64 support (resolves #1) 
Android: Corrected application id check when doesn't contain air prefix 


###### 2014.12.05

Corrected missing EventDispatcher functions from base class
iOS: Implemented autoreleasepools for all C function calls


###### 2014.12.01

New application based key check, removing server checks


###### 2014.10.24
iOS Update for iOS 8 + Android device default orientation fix
- iOS: Update for iOS 8
- Android: Corrected errors with devices with landscape default orientation (resolves #138)
- Android+iOS: Added autoOrient option to handle coordinate system with orientation changes
