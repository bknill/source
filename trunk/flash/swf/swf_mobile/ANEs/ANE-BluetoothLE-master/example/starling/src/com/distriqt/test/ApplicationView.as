/**
 *        __       __               __ 
 *   ____/ /_ ____/ /______ _ ___  / /_
 *  / __  / / ___/ __/ ___/ / __ `/ __/
 * / /_/ / (__  ) / / /  / / /_/ / / 
 * \__,_/_/____/_/ /_/  /_/\__, /_/ 
 *                           / / 
 *                           \/ 
 * http://distriqt.com
 *
 * @file   		Application.as
 * @brief  		
 * @author 		"Michael Archbold (ma&#64;distriqt.com)"
 * @created		17/03/2015
 * @updated		$Date:$
 * @copyright	http://distriqt.com/copyright/license.txt
 */
package com.distriqt.test
{
	
	import flash.geom.Rectangle;
	
	import feathers.controls.Button;
	import feathers.controls.ScrollContainer;
	import feathers.layout.VerticalLayout;
	import feathers.themes.MetalWorksMobileTheme;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	/**	
	 * @author	"Michael Archbold (ma&#64;distriqt.com)"
	 */
	public class ApplicationView extends Sprite implements ILogger
	{
		/**
		 *  Constructor
		 */
		public function ApplicationView()
		{
			_ble = new Controller(this);
			
			addEventListener( starling.events.Event.ADDED_TO_STAGE, addedToStageHandler );
		}
		
		
		////////////////////////////////////////////////////////
		//	VARIABLES
		//
		
		private var _buttons	: Vector.<Button>;
		private var _text		: TextField;
		private var _view		: Rectangle = new Rectangle( 0, 0, 640, 480 );
		
		private var _ble		: Controller;
		
		
		////////////////////////////////////////////////////////
		//	FUNCTIONALITY
		//
		
		private function setup( e:* ):void
		{
			_ble.setup();
		}
		
		private function dispose( e:* ):void
		{
			_ble.dispose();			
		}
		
		
		////////////////////////////////////////////////////////
		//	ILogger
		//
		
		public function log( tag:String, message:String ):void
		{
			trace( tag+"::"+message );
			if (_text)
				_text.text = tag+"::"+message + "\n" + _text.text ;
		}
		
		
		////////////////////////////////////////////////////////
		//	INTERNALS
		//
		
		private function createUI():void
		{
			_text = new TextField( stage.stageWidth, stage.stageHeight, "", "_typewriter", 18, Color.WHITE );
			_text.hAlign = HAlign.LEFT; 
			_text.vAlign = VAlign.TOP;
			_text.y = 40;
			_text.touchable = false;
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_RIGHT;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_BOTTOM;
			layout.gap = 5;
			var container:ScrollContainer = new ScrollContainer();
			container.layout = layout;
			container.width = stage.stageWidth;
			container.height = stage.stageHeight-50;
			
			var b:Button;
			_buttons = new Vector.<Button>();
			
			addAction( "Setup", 	_ble.setup );
			addAction( "Dispose", 	_ble.dispose );
			
			addAction( "Scan :Central",	_ble.scan );
			addAction( "Scan for Peripherals :Central",	_ble.scanForPeripherals );
			addAction( "Stop scan :Central",			_ble.stopScan );
			
//			addAction( "Discover services :Central",	_ble.discoverServices );
//			addAction( "Discover charac. :Central", 	_ble.discoverCharacteristics );
			addAction( "Read charac. :Central", 		_ble.readCharacteristic );
			addAction( "Write charac. :Central", 		_ble.writeCharacteristic );
			addAction( "Subscribe charac. :Central", 	_ble.subscribeCharacteristic );
			addAction( "Unsubscribe charac. :Central", 	_ble.unsubscribeCharacteristic );
			
			addAction( "Start Advertising :Peripheral",	_ble.startAdvertising );
			addAction( "Stop Advertising :Peripheral",	_ble.stopAdvertising );
			addAction( "Update value :Peripheral", 		_ble.updateValue );
			
			
			for each (var button:Button in _buttons)
			{
				container.addChild(button);
			}
			
			addChild( container );
			addChild( _text );

		}
		
		
		private function addAction( label:String, listener:Function ):void
		{
			var b:Button = new Button();
			b.label = label;
			b.addEventListener( starling.events.Event.TRIGGERED, listener );
			_buttons.push( b );
		}
		
		
		////////////////////////////////////////////////////////
		//	EVENT HANDLERS
		//
		
		protected function addedToStageHandler( event:starling.events.Event ):void
		{
			removeEventListener( starling.events.Event.ADDED_TO_STAGE, addedToStageHandler );
			new MetalWorksMobileTheme();
			createUI();
		}
		
		
		//
		//	EXTENSION HANDLERS
		//
		
		
		
		
		
	}
}