/**
 *        __       __               __ 
 *   ____/ /_ ____/ /______ _ ___  / /_
 *  / __  / / ___/ __/ ___/ / __ `/ __/
 * / /_/ / (__  ) / / /  / / /_/ / / 
 * \__,_/_/____/_/ /_/  /_/\__, /_/ 
 *                           / / 
 *                           \/ 
 * http://distriqt.com
 *
 * @file   		IABHelper.as
 * @brief  		
 * @author 		"Michael Archbold (ma&#64;distriqt.com)"
 * @created		26/03/2015
 * @updated		$Date:$
 * @copyright	http://distriqt.com/copyright/license.txt
 */
package com.distriqt.test
{
	import com.distriqt.extension.bluetoothle.BluetoothLE;
	import com.distriqt.extension.bluetoothle.events.BluetoothLEEvent;
	import com.distriqt.extension.bluetoothle.events.CentralEvent;
	import com.distriqt.extension.bluetoothle.events.CentralManagerEvent;
	import com.distriqt.extension.bluetoothle.events.CharacteristicEvent;
	import com.distriqt.extension.bluetoothle.events.PeripheralEvent;
	import com.distriqt.extension.bluetoothle.events.PeripheralManagerEvent;
	import com.distriqt.extension.bluetoothle.events.RequestEvent;
	import com.distriqt.extension.bluetoothle.objects.Characteristic;
	import com.distriqt.extension.bluetoothle.objects.Peripheral;
	import com.distriqt.extension.bluetoothle.objects.Request;
	import com.distriqt.extension.bluetoothle.objects.Service;
	
	import flash.events.EventDispatcher;
	import flash.text.ReturnKeyLabel;
	import flash.utils.ByteArray;
	
	import starling.events.Event;
	
	
	/**	
	 * @author	"Michael Archbold (ma&#64;distriqt.com)"
	 */
	public class Controller extends EventDispatcher
	{
		
		public static const TAG : String = "BLEHelper";
		
		// max length to limit characteristic values
		public static const MAX_VALUE_LENGTH	: int = 200;
		
		/**
		 *  Constructor
		 */
		public function Controller(log:ILogger)
		{
			super();
			_l = log;
		}
		
		
		////////////////////////////////////////////////////////
		//	VARIABLES
		//
		
		private var _l : ILogger;
		
		private var _isSetup	: Boolean = false;
		public function get isSetup():Boolean { return _isSetup; }
		public function set isSetup(value:Boolean):void { _isSetup = value; }

		private var _service		: Service;
		private var _characteristic	: Characteristic;
		private var _peripheral 	: Peripheral;
		
		
		
		////////////////////////////////////////////////////////
		//	FUNCTIONALITY
		//
		
		public function setup(e:Event=null):void
		{
			if (!isSetup)
			{
				try
				{
					BluetoothLE.init( Config.APP_KEY );
					
					_l.log( TAG, "BluetoothLE.isSupported = " + BluetoothLE.isSupported );
					
					if (BluetoothLE.isSupported)
					{
						BluetoothLE.service.addEventListener( BluetoothLEEvent.STATE_CHANGED, stateChangedHandler );
						
						_l.log( TAG, "BluetoothLE.service.version = " + BluetoothLE.service.version );
						_l.log( TAG, "BluetoothLE.service.state   = " + BluetoothLE.service.state );
						
					
						BluetoothLE.service.centralManager.addEventListener( CentralManagerEvent.STATE_CHANGED, central_stateChangedHandler );
						BluetoothLE.service.centralManager.addEventListener( PeripheralEvent.DISCOVERED, central_peripheralDiscoveredHandler );
						BluetoothLE.service.centralManager.addEventListener( PeripheralEvent.CONNECT, central_peripheralConnectHandler );
						BluetoothLE.service.centralManager.addEventListener( PeripheralEvent.CONNECT_FAIL, central_peripheralConnectFailHandler );
						BluetoothLE.service.centralManager.addEventListener( PeripheralEvent.DISCONNECT, central_peripheralDisconnectHandler );
						
						
						BluetoothLE.service.peripheralManager.addEventListener( PeripheralManagerEvent.STATE_CHANGED, peripheral_stateChangedHandler );
						BluetoothLE.service.peripheralManager.addEventListener( PeripheralManagerEvent.SERVICE_ADD, peripheral_serviceAddHandler );
						BluetoothLE.service.peripheralManager.addEventListener( PeripheralManagerEvent.SERVICE_ADD_ERROR, peripheral_serviceAddErrorHandler );
						BluetoothLE.service.peripheralManager.addEventListener( PeripheralManagerEvent.START_ADVERTISING, peripheral_startAdvertisingHandler );
						
						BluetoothLE.service.peripheralManager.addEventListener( CentralEvent.SUBSCRIBE, peripheral_central_subscribeHandler );
						BluetoothLE.service.peripheralManager.addEventListener( CentralEvent.UNSUBSCRIBE, peripheral_central_unsubscribeHandler );
						
						BluetoothLE.service.peripheralManager.addEventListener( RequestEvent.READ, peripheral_readRequestHandler );
						BluetoothLE.service.peripheralManager.addEventListener( RequestEvent.WRITE, peripheral_writeRequestHandler );
						
						_characteristic = new Characteristic( "2A37", [ Characteristic.PROPERTY_NOTIFY, Characteristic.PROPERTY_READ, Characteristic.PROPERTY_WRITE ], [ Characteristic.PERMISSION_READABLE, Characteristic.PERMISSION_WRITEABLE ] );
						
						_service = new Service();
						_service.uuid = "180D";
						_service.characteristics.push( _characteristic );
						
					}
					else
					{
						_l.log( TAG, "BluetoothLE not supported" );
					}
					
					isSetup = true;
				}
				catch (e:Error)
				{
					_l.log( TAG, e.message );
				}
			}
		}
		
		
		public function dispose(e:Event=null):void
		{
			if (isSetup)
			{
				_l.log( TAG, "dispose" );
				
				BluetoothLE.service.dispose();
				isSetup = false;
			}
		}
		
		
		//
		//	CENTRAL
		//
		
		public function scan( e:Event=null ):void
		{
			if (!isSetup) return;
			_l.log( TAG, "scan" );
			
			BluetoothLE.service.centralManager.scanForPeripherals();
		}
		
		public function scanForPeripherals(e:Event=null):void
		{
			if (!isSetup) return;
			
			_l.log( TAG, "scanForPeripherals" );
			
			BluetoothLE.service.centralManager.scanForPeripherals( new <String>[ _service.uuid ] );
		}
		
		
		public function stopScan(e:Event=null):void
		{
			if (!isSetup) return;
			
			_l.log( TAG, "stopScan" );
		
			BluetoothLE.service.centralManager.stopScan();
		}
		
		
		public function discoverServices(e:Event=null):void
		{
			if (!isSetup) return;
			if (_peripheral == null) return;
			_l.log( TAG, "discoverServices" );
			
			_peripheral.discoverServices();
		}
		
		
		public function discoverCharacteristics( e:Event=null ):void 
		{
			if (!isSetup) return;
			if (_peripheral == null) return;
			
			_l.log( TAG, "discoverCharacteristics" );
			
			var success:Boolean = _peripheral.discoverCharacteristics( _service );
			_l.log( TAG, "discoverCharacteristics: success="+success  );
		}
		
		
		public function readCharacteristic( e:Event=null ):void
		{
			if (!isSetup) return;
			if (_peripheral == null) return;
//			if (_peripheral.services.length == 0) return;
//			if (_peripheral.services[0].characteristics.length == 0) return;
			
			_l.log( TAG, "readCharacteristic" );
			
			var success:Boolean = _peripheral.readValueForCharacteristic( _characteristic );
			_l.log( TAG, "readCharacteristic: success="+success );
		}
		
		
		public function writeCharacteristic( e:Event=null ):void
		{
			if (!isSetup) ReturnKeyLabel;
			if (_peripheral == null) return;
			
			var content:String = "value_of_something_" + String(Math.floor(Math.random() * 10000));
			
			_l.log( TAG, "writeCharacteristic: "+content );

			var value:ByteArray = new ByteArray();
			value.writeUTFBytes( content );
			
			var success:Boolean = _peripheral.writeValueForCharacteristic( _characteristic, value );
			_l.log( TAG, "writeCharacteristic: success="+success );
		}
		
		
		public function subscribeCharacteristic( e:Event=null ):void
		{
			if (!isSetup) return;
			if (_peripheral == null) return;
			
			_l.log( TAG, "subscribeCharacteristic" );
			
			var success:Boolean = _peripheral.subscribeToCharacteristic( _characteristic );
			_l.log( TAG, "subscribeCharacteristic: success="+success );
			
		}
		
		public function unsubscribeCharacteristic( e:Event=null ):void
		{
			if (!isSetup) return;
			if (_peripheral == null) return;
			
			_l.log( TAG, "unsubscribeCharacteristic" );
			
			var success:Boolean = _peripheral.unsubscribeToCharacteristic( _characteristic );
			_l.log( TAG, "unsubscribeCharacteristic: success="+success );
		}
		
		//
		//	PERIPHERAL
		//

		public function startAdvertising(e:Event=null):void
		{
			if (!isSetup) return;
			
			_l.log( TAG, "startAdvertising" );
			
			// Remove all to prevent duplicate services
			BluetoothLE.service.peripheralManager.removeAllServices();
			BluetoothLE.service.peripheralManager.addService( _service );
			BluetoothLE.service.peripheralManager.startAdvertising( "distriqt", new <Service>[ _service ] );
		}
		
		
		public function stopAdvertising(e:Event=null):void
		{
			if (!isSetup) return;
			
			_l.log( TAG, "stopAdvertising" );
			
			BluetoothLE.service.peripheralManager.stopAdvertising();
			BluetoothLE.service.peripheralManager.removeAllServices();
		}
		
		
		public function updateValue( e:Event=null ):void
		{
			if (!isSetup) return;
			
			var content:String = "value_of_something_" + String(Math.floor(Math.random() * 10000));
			
			_l.log( TAG, "updateValue: "+content );
			
			var value:ByteArray = new ByteArray();
			value.writeUTFBytes( content );
			
			BluetoothLE.service.peripheralManager.updateValue( _characteristic, value );
		}
		
		
		
		
		////////////////////////////////////////////////////////
		//	INTERNALS
		//
		
		private function setActivePeripheral( peripheral:Peripheral ):void
		{
			if (_peripheral != null)
			{
				_peripheral.removeEventListener( PeripheralEvent.DISCOVER_SERVICES, peripheral_discoverServicesHandler );
				_peripheral.removeEventListener( PeripheralEvent.DISCOVER_CHARACTERISTICS, peripheral_discoverCharacteristicsHandler );
				
				_peripheral.removeEventListener( CharacteristicEvent.UPDATE, peripheral_characteristic_updatedHandler );
				_peripheral.removeEventListener( CharacteristicEvent.UPDATE_ERROR, peripheral_characteristic_errorHandler );
				_peripheral.removeEventListener( CharacteristicEvent.WRITE_SUCCESS, peripheral_characteristic_writeHandler );
				_peripheral.removeEventListener( CharacteristicEvent.WRITE_ERROR, peripheral_characteristic_writeErrorHandler );
				_peripheral.removeEventListener( CharacteristicEvent.SUBSCRIBE, peripheral_characteristic_subscribeHandler );
				_peripheral.removeEventListener( CharacteristicEvent.SUBSCRIBE_ERROR, peripheral_characteristic_subscribeErrorHandler );
				_peripheral.removeEventListener( CharacteristicEvent.UNSUBSCRIBE, peripheral_characteristic_unsubscribeHandler );
				
				_peripheral = null;
			}
			
			if (peripheral != null)
			{
				_peripheral = peripheral;
				
				_peripheral.addEventListener( PeripheralEvent.DISCOVER_SERVICES, peripheral_discoverServicesHandler );
				_peripheral.addEventListener( PeripheralEvent.DISCOVER_CHARACTERISTICS, peripheral_discoverCharacteristicsHandler );

				_peripheral.addEventListener( CharacteristicEvent.UPDATE, peripheral_characteristic_updatedHandler );
				_peripheral.addEventListener( CharacteristicEvent.UPDATE_ERROR, peripheral_characteristic_errorHandler );
				_peripheral.addEventListener( CharacteristicEvent.WRITE_SUCCESS, peripheral_characteristic_writeHandler );
				_peripheral.addEventListener( CharacteristicEvent.WRITE_ERROR, peripheral_characteristic_writeErrorHandler );
				_peripheral.addEventListener( CharacteristicEvent.SUBSCRIBE, peripheral_characteristic_subscribeHandler );
				_peripheral.addEventListener( CharacteristicEvent.SUBSCRIBE_ERROR, peripheral_characteristic_subscribeErrorHandler );
				_peripheral.addEventListener( CharacteristicEvent.UNSUBSCRIBE, peripheral_characteristic_unsubscribeHandler );
			}
		}
		
		
		////////////////////////////////////////////////////////
		//	EVENT HANDLERS
		//
		
		private function stateChangedHandler( event:BluetoothLEEvent ):void
		{
			_l.log( TAG, "state = " + BluetoothLE.service.state );
		}
		
		//
		//	CENTRAL EVENTS
		//
		
		private function central_stateChangedHandler( event:CentralManagerEvent ):void
		{
			_l.log( TAG, "central state = " + BluetoothLE.service.centralManager.state );	
		}
		
		
		private function central_peripheralDiscoveredHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral discovered: "+ event.peripheral.name );	

			BluetoothLE.service.centralManager.stopScan();
			
			_l.log( TAG, "connecting: " + event.peripheral.name );
			BluetoothLE.service.centralManager.connect( event.peripheral );
			
		}
		
		private function central_peripheralConnectHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral connected: "+ event.peripheral.toString() );	
		
			setActivePeripheral( event.peripheral );
		
			discoverServices();
		}
		
		private function central_peripheralConnectFailHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral connect fail: "+ event.peripheral.name );	
			setActivePeripheral( null );
		}
		
		private function central_peripheralDisconnectHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral disconnect: "+ event.peripheral.name );	
			setActivePeripheral( null );
		}
		
		
		//
		//	PERIPHERAL EVENTS
		//	
		
		private function peripheral_discoverServicesHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral discover services: " + event.peripheral.name );
			
			for each (var service:Service in event.peripheral.services)
			{
				_l.log( TAG, "service: "+ service.uuid );
			}
			
			discoverCharacteristics();
		}
		
		
		private function peripheral_discoverCharacteristicsHandler( event:PeripheralEvent ):void
		{
			_l.log( TAG, "peripheral discover characteristics: " + event.peripheral.name );
			
			for each (var service:Service in event.peripheral.services)
			{
				_l.log( TAG, "service: "+ service.uuid );
				for each (var ch:Characteristic in service.characteristics)
				{
					_l.log( TAG, "characteristic: "+ch.uuid );
				}
			}
			
		}
		
		
		private function peripheral_characteristic_updatedHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic updated: " + event.characteristic.uuid + " value="+ event.characteristic.value.readUTFBytes( event.characteristic.value.length )  );
		}
		
		private function peripheral_characteristic_errorHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic error: [" + event.errorCode +"] "+event.error );
		}
		
		private function peripheral_characteristic_writeHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic write: " + event.peripheral.name );
		}		

		private function peripheral_characteristic_writeErrorHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic error: [" + event.errorCode +"] "+event.error );
		}		

		private function peripheral_characteristic_subscribeHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic subscribe: " + event.peripheral.name );
		}		

		private function peripheral_characteristic_subscribeErrorHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic error: [" + event.errorCode +"] "+event.error );
		}		

		private function peripheral_characteristic_unsubscribeHandler( event:CharacteristicEvent ):void
		{
			_l.log( TAG, "peripheral characteristic unsubscribe: " + event.peripheral.name );
		}		

		
		
		//
		//	PERIPHERAL MANAGER EVENTS
		//
		
		private function peripheral_stateChangedHandler( event:PeripheralManagerEvent ):void
		{
			_l.log( TAG, "peripheral state = " + BluetoothLE.service.peripheralManager.state );
		}
		
		private function peripheral_serviceAddHandler( event:PeripheralManagerEvent ):void
		{
			_l.log( TAG, "peripheral manager service added" );
		}
		
		private function peripheral_serviceAddErrorHandler( event:PeripheralManagerEvent ):void
		{
			_l.log( TAG, "peripheral manager service add error" );
		}
		
		private function peripheral_startAdvertisingHandler( event:PeripheralManagerEvent ):void
		{
			_l.log( TAG, "peripheral manager start advertising" );
		}
		
		
		private function peripheral_central_subscribeHandler( event:CentralEvent ):void
		{
			_l.log( TAG, "peripheral manager: central subscribe: " + event.central.uuid  );
		}

		private function peripheral_central_unsubscribeHandler( event:CentralEvent ):void
		{
			_l.log( TAG, "peripheral manager: central unsubscribe: " + event.central.uuid  );
		}

		
		private function peripheral_readRequestHandler( event:RequestEvent ):void
		{
			//
			//	Read requests will only have one object in the requests event
			
			var request:Request = event.requests[0];
			
			_l.log( TAG, "peripheral manager: read request: " + request.characteristic.uuid );

			//
			//	Handle the read request by first checking the UUID and then responding with the requested data.
			// 	You need to make sure you correctly handle the offset variable as illustrated below
			
			if (request.characteristic.uuid == _characteristic.uuid)
			{
				if (request.offset > _characteristic.value.length)
				{
					BluetoothLE.service.peripheralManager.respondToRequest( request, Request.INVALID_OFFSET );
				}
				else
				{
					_characteristic.value.position = 0;
					
					var data:ByteArray = new ByteArray();
					data.writeBytes( _characteristic.value, request.offset, (_characteristic.value.length - request.offset) );
					
					BluetoothLE.service.peripheralManager.respondToRequest( request, Request.SUCCESS, data );
				}
			}
		}
		
		
		private function peripheral_writeRequestHandler( event:RequestEvent ):void
		{
			_l.log( TAG, "peripheral manager: write request: " + event.requests.length );

			//
			//	Write requests may have several requests and you should process each one
			//	before calling respondToRequest. You only need to call respondToRequest once
			//	with the first request in the array
			for each (var request:Request in event.requests)
			{
				_l.log( TAG, "peripheral manager: write request: " + request.characteristic.uuid +" :: [" + request.offset +"] "+ request.value.readUTFBytes( request.value.length ) );
				
				if (request.characteristic.uuid == _characteristic.uuid)
				{
					if (request.offset + request.value.length > MAX_VALUE_LENGTH)
					{
						BluetoothLE.service.peripheralManager.respondToRequest( request, Request.INVALID_OFFSET );
						return;
					}
					else
					{
						if (_characteristic.value == null) _characteristic.value = new ByteArray();
						_characteristic.value.position = 0;
						_characteristic.value.writeBytes( request.value, request.offset );
					}
				}
			}
			
			BluetoothLE.service.peripheralManager.respondToRequest( event.requests[0], Request.SUCCESS );
			
		}
	
	}
}