

###### 2015.05.13

Android: Added check to ensure bluetooth is on before attempting to connect or listen (resolves #5)


###### 2015.04.09

Moved to new structure to support FlashBuilder 4.6 (resolves #3)
Android: x86 Support


###### 2015.02.02

Added check for .debug suffix in application id


###### 2014.12.18

iOS: Included arm64 support (resolves #1) 
Android: Corrected application id check when doesn't contain air prefix 


###### 2014.12.05

Corrected missing EventDispatcher functions from base class
iOS: Implemented autoreleasepools for all c function calls


###### 2014.11.27

New application based key check, removing server checks


###### 2014.10.16
Fixed string formatting bug on close function (resolves #223)

