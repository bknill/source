package co.beek.connect
{
	import co.beek.event.NetworkDataEvent;
	import co.beek.model.Model;
	
	import flash.events.*;
	import flash.net.*;
	import flash.utils.*;
	import co.beek.utils.NetworkUtil;
	
	public class ReceiveData
	{
		public function ReceiveData()
		{
		}
		
		private static const subnetMask:Array = NetworkUtil.getSubnetMask(NetworkUtil.getPrefixLength());
		private static const ip:Array = NetworkUtil.getIpAddress().split(".");
		private static const ipBroadcastSocket:DatagramSocket = new DatagramSocket();
		private static const receiveDataSocket:DatagramSocket = new DatagramSocket();
		private static var _broadcastIp:Boolean = true;
		private static var intervalId:int;
		private static var broadcastPort:int = 7934;
		private static var receivePort:int = 7935;
		private static var _connected:Boolean;
		
		private var ipAddress:String;
		
		public static function createServer():void{
			trace('NetworkConnection.createServer()');
			
			//broadcast IP
			intervalId = setInterval(broadcastIP, 2000, NetworkUtil.getIpAddress());//broadcast the servers Ip-Address every 2 seconds
			
			//incoming Data
			if(!receiveDataSocket.bound){
				receiveDataSocket.bind(receivePort);
				receiveDataSocket.addEventListener(DatagramSocketDataEvent.DATA, dataReceived);
				receiveDataSocket.receive();
			}
		}
		
		private static function broadcastIP(message:String):void
		{
			trace("NetworkConnection.broadcastIP()");
				try
				{
					if(subnetMask[1] != 255 || subnetMask[2] != 255){
						trace("!!! WARNING: NOT A 'C' CLASS NETWORK !!! (Will not broadcast IP.)");
						clearInterval(intervalId);
					}
					else
					{
						var data:ByteArray = new ByteArray();
						data.writeUTFBytes(message);

						for(var i4:int = subnetMask[3]; i4 <= 255; i4++){
							var tempIp:String = ip[0] + "." + ip[1] + "." + ip[2] + "." + (subnetMask[3] == 255 ? ip[3] : i4);
							if(tempIp != NetworkUtil.getBroadcastIp() && tempIp != NetworkUtil.getSubnetIp(ip, subnetMask)){
								ipBroadcastSocket.send(data, 0, 0, tempIp, broadcastPort);
							}
						}
					}
				}
				catch (error:Error)
				{
					trace(error.message);
				}
		}
		
		private static function connected():void
		{
			_connected = true;
			trace('connected()');
			clearInterval(intervalId);
		}
		

		
		
		protected static function dataReceived(e:DatagramSocketDataEvent):void {
			
			if(!_connected)
				connected();
				
				var obj:Object = e.data.readObject();
				
				if(obj)
					Model.dispatchNetworkDataEvent(obj);

		}
		
		
		
	}
}