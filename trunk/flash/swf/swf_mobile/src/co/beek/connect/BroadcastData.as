package co.beek.connect
{
	import co.beek.event.NetworkDataEvent;
	import co.beek.model.*;
	
	import flash.errors.EOFError;
	import flash.events.*;
	import flash.net.*;
	import flash.utils.*;
	
	public class BroadcastData
	{
		public function BroadcastData()
		{
			super();
			
		}
		
		private static var  broadcastSocket:DatagramSocket = new DatagramSocket();
		private static var receiveIPSocket:DatagramSocket = new DatagramSocket();
		private static var ip:String;
		private static var broadcastPort:int = 7935;
		private static var receivePort:int = 7934;
		
		public static function findNetwork():void
		{
			trace('findNetwork()');
			if(!receiveIPSocket.bound){
				receiveIPSocket.bind(receivePort);
				receiveIPSocket.addEventListener(DatagramSocketDataEvent.DATA, firstDataReceived);
				receiveIPSocket.receive();
			}
		}
		
		private static function firstDataReceived(e:DatagramSocketDataEvent):void
		{
			trace('swf_mobile.firstDataReceived()');
			ip = e.data.readUTFBytes(e.data.bytesAvailable);
			
			if(ip){
				Model.dispatchEvent(new Event(Model.NETWORK_CONNECT));
				receiveIPSocket.removeEventListener(DatagramSocketDataEvent.DATA, firstDataReceived);
				receiveIPSocket.addEventListener(DatagramSocketDataEvent.DATA, onDataReceived);
			}
		}
		
		private static function onDataReceived(e:DatagramSocketDataEvent):void
		{
			trace('swf_mobile.dataReceived()');
			var object:Object = new Object;
			try{object = e.data.readObject();}
			catch(error:EOFError){trace("ERROR PROCESSING DATA: " + error.message);}
			Model.dispatchEvent(new NetworkDataEvent(Model.NETWORK_DATA, object));
	
		}

		public static function sendData(o:Object):void{
			var data:ByteArray = new ByteArray();
			data.writeObject(o);
			broadcastSocket.send(data,0,0,ip,broadcastPort);
		}
		
		public static function closeConnection():void
		{
			broadcastSocket.close();
			Model.state.connected = false;
		}
		
	}
}