package co.beek.connect
{
	import co.beek.Log;
	import co.beek.utils.*;
	
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class NetworkManager extends NetworkConnectFl
	{
		
		import co.beek.connect.BroadcastData;
		import co.beek.model.*;
		
		public const BROADCAST:int = 0;
		public const RECEIVE:int = 1;
		public const WIFI:int = 0;
		public const BLUETOOTH:int = 1;
		
		public var mode:int;
		public var network:int;
		
		public function NetworkManager()
		{
			super();
			
			broadcastButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			receiveButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			wifiButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			
			broadcastButton.addEventListener(MouseEvent.MOUSE_UP, onBroadcastButtonMouseUp);
			receiveButton.addEventListener(MouseEvent.MOUSE_UP, onReceiveButtonMouseUp);
			wifiButton.addEventListener(MouseEvent.MOUSE_UP, onWifiButtonMouseUp);
			bluetoothButton.addEventListener(MouseEvent.MOUSE_UP, onBluetoothButtonMouseUp);
			
	
			onBroadcastButtonMouseUp();
		}
		
		private function onButtonMouseDown(e:MouseEvent):void
		{
			e.target.alpha = 0.6;
		}
		
		private function onBroadcastButtonMouseUp(e:MouseEvent = null):void
		{
			Log.record("NetworkManager.onBroadcastButtonMouseUp()");
			ColorUtil.colorize(broadcastButton.button.fill, 0x111111);
			ColorUtil.colorize(receiveButton.button.fill, 0xffffff);
			mode = BROADCAST;
			
		}
		
		private function onReceiveButtonMouseUp(e:MouseEvent = null):void
		{
			Log.record("NetworkManager.onReceiveButtonMouseUp()");
			ColorUtil.colorize(receiveButton.button.fill, 0x111111);
			ColorUtil.colorize(broadcastButton.button.fill, 0xffffff);	
			mode = RECEIVE;
			
		}
		
		private function onWifiButtonMouseUp(e:MouseEvent  = null):void
		{
			Log.record("NetworkManager.onWifiButtonMouseUp()");
			network = WIFI;
			ColorUtil.colorize(wifiButton.icon, 0x666666);	
			
			Model.addEventListener(Model.NETWORK_CONNECT, onNetworkConnect);
			Model.addEventListener(Model.NETWORK_DATA, onNetworkConnect);
			
			if(mode == BROADCAST)
				BroadcastData.findNetwork();
			else 
				ReceiveData.createServer();
				
		}
		
		private function onNetworkConnect(e:Event = null):void
		{
			ColorUtil.colorize(wifiButton.icon, 0x006600);
			Model.state.connected = true;
			
			Model.removeEventListener(Model.NETWORK_CONNECT, onNetworkConnect);
			Model.removeEventListener(Model.NETWORK_DATA, onNetworkConnect);
			
			if(mode == RECEIVE)
				if(Model.state.mode != State.MODE_REMOTE)
					Model.state.mode = State.MODE_REMOTE;
		}
		
		private function onBluetoothButtonMouseUp(e:MouseEvent  = null):void
		{
			Log.record("NetworkManager.onBluetoothButtonMouseUp()");
			network = BLUETOOTH;
		}
		
		
	}
}