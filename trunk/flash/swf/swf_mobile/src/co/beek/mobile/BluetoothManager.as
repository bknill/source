package co.beek.mobile
{
	
	import com.distriqt.extension.bluetooth.Bluetooth;
	import com.distriqt.extension.bluetooth.BluetoothDevice;
	import com.distriqt.extension.bluetooth.events.BluetoothConnectionEvent;
	import com.distriqt.extension.bluetooth.events.BluetoothDeviceEvent;
	import com.distriqt.extension.bluetooth.events.BluetoothEvent;
	
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	
	import co.beek.Log;
	import co.beek.connect.MotionDataManager;
	import co.beek.model.Model;
	import co.beek.model.State;
	
	public class BluetoothManager
	{
		
		//All of the visible devices will be stored here
		private static var availableDevices:Vector.<BluetoothDevice> = new Vector.<BluetoothDevice>();
		public static var connectedDevices:Vector.<BluetoothDevice> = new Vector.<BluetoothDevice>();
		private static var key:String;
		private static var attemptingConnection:Boolean = false;
		private static var _connected:Boolean = false;
		private static var listening:Boolean = false;
		private static var initialised:Boolean = false;
		private static var _device: BluetoothDevice;
		private static var ba: ByteArray = new ByteArray;
		private static var obj:Object = new Object;
		private static var uuid: String = "fa87c0d0-afac-11de-8a39-0800200c9a66";
		private static var processing:Boolean;
		
		public function BluetoothManager()
		{
			super();
		}
		
		public static function init():void
		{
			
			try
			{
				Bluetooth.init( Model.state.distriqKey ); 
				
				Bluetooth.service.addEventListener( BluetoothEvent.SCAN_STARTED, bluetooth_scanStartedHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothEvent.SCAN_FINISHED, bluetooth_scanFinishedHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_DISCONNECTED, bluetooth_disconnectionHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_CONNECTED, bluetooth_connectionHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_CONNECTING, bluetooth_connectingHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_CONNECT_FAILED, bluetooth_connectFailedHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_CONNECT_ERROR, bluetooth_connectErrorHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_LISTEN_ERROR, bluetooth_connectListenErrorHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_LISTENING, bluetooth_connectListenHandler, false, 0, true );
				
				initialised = true;
				scan();
				
			}
			catch (e:Error)
			{
				Model.state.connectState = "Cannot connect with Bluetooth: "  + e.message;
			}
		}
		
		
		public static function scan():void
		{
			
			if(!initialised){
				init();
				return;
			}
			Log.record("BluetoothManager.scan()");

	
			if(!Bluetooth.service.isEnabled())
				Bluetooth.service.enable();
			
			if(Model.state.connectMode == State.CONNECT_MODE_BROADCAST){
				
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_REMOTE, bluetooth_remoteConnectionHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_RECEIVED_BYTES, bluetooth_receivedBytesHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothEvent.SCAN_MODE_CHANGED, bluetooth_scanModeChangedBroadcast, false, 0, true);
				
				if(!Bluetooth.service.isDiscoverable())
					Bluetooth.service.setDeviceDiscoverable(true,240);

				if(Bluetooth.service.getState() == "state:on"){
					listening = true;
					Bluetooth.service.listen(uuid);
				}
				else
					Model.state.connectState = "Please enable Bluetooth";
					
			}
			else
			{
				
				if(Bluetooth.service.getPairedDevices())
					Model.state.connectState = "Trying paired devices";
				else
					Model.state.connectState = "Scanning for devices";
				
				Bluetooth.service.addEventListener( BluetoothDeviceEvent.DEVICE_FOUND, bluetooth_deviceFoundHandler, false, 0, true );
				Bluetooth.service.addEventListener( BluetoothConnectionEvent.CONNECTION_RECEIVED_BYTES, bluetooth_receivedBytesHandler, false, 0, true );
			
				if(Bluetooth.service.getState() == "state:on")
					connectToAvailableDevices()
				else
					Bluetooth.service.addEventListener( BluetoothEvent.SCAN_MODE_CHANGED, bluetooth_scanModeChangedReceive, false, 0, true);
				

			}
			
		}
		
		private static function connectToAvailableDevices(e:Event = null):void
		{
			
			attemptingConnection = true;
			
			for each(var device:BluetoothDevice in Bluetooth.service.getPairedDevices()){
				var success:Boolean = Bluetooth.service.connect(device, uuid)
				if(success) 
				{
					Model.state.connectState = "Connecting to paired device " + device.deviceName;
					_device = device;
					break;
				}
				
				if(success)
					sendConnectionNotification(uuid)
				else
					Model.state.connectState = Bluetooth.service.startScan() ? "Scanning started " : "Can't scan - is Bluetooth enabled?"
						
			}
		}
		
		public static function set connected(value:Boolean):void
		{
			Log.record("BluetoothManager.connected - " + value);
			
			if(_connected == value)
				return;
			
			_connected = value;
			
			if(value)
				Model.state.connected = true;
			

		}
		
		public static function get connected():Boolean
		{return _connected}
		
		
		private static function bluetooth_deviceFoundHandler( event:BluetoothDeviceEvent ):void 
		{ 
			Log.record("BluetoothManager.bluetooth_deviceFoundHandler()");
			Model.state.connectState = "Found " + event.device.deviceName;
			Bluetooth.service.cancelScan();
			attemptingConnection = true;
			Bluetooth.service.connect( event.device, uuid ); 
		}
		
		private static function bluetooth_remoteConnectionHandler( event:BluetoothConnectionEvent ):void
		{			
			Log.record("BluetoothManager.bluetooth_remoteConnectionHandler()");
			Model.state.connectState = "Remot connection from to " + event.device.deviceName;
			_device = event.device;
			attemptingConnection = true;
			sendConnectionNotification( event.uuid );
			
		}

		private static function bluetooth_connectionHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_connectionHandler()");
			Model.state.connectState = "Connected to " + event.device.deviceName;
			_device = event.device;
			Bluetooth.service.listen(uuid);
			
			sendConnectionNotification(uuid);
		}
		
		
		private static function sendConnectionNotification(uuid:String):void
		{
			
			var data:ByteArray = new ByteArray();
			data.writeUTF( "connection" );
			var success:Boolean = Bluetooth.service.writeBytes(uuid , data );
			Model.state.connectState = success ? "Connected to " + _device.deviceName : "Trying to connect to " + _device.deviceName;
			
			if(success)
				connected = true;
			else if(attemptingConnection)
				setTimeout(sendConnectionNotification,100,uuid);
			
		}
		
		private static function bluetooth_connectingHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_connectingHandler()");
			Model.state.connectState = "Connecting to " + event.device.deviceName;
		}
	
		private static function bluetooth_connectFailedHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.luetooth_connectFailedHandler()");
			Model.state.connectState = "Please ensure device is discoverable";
			attemptingConnection = false;
			setTimeout(dispatchDisconnect,1000);
		}
		
		private static function bluetooth_connectErrorHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_connectErrorHandler()");
			Model.state.connectState = "Connecting to " + event.device.deviceName + " Error: " + event.message;
			attemptingConnection = false;
			setTimeout(dispatchDisconnect,1000);
		}
		
		private static function dispatchDisconnect():void
		{
			if(Model.state.connected)
				Model.state.dispatchEvent(new Event(State.NETWORK_DISCONNECT));
		}
		
		private static function reconnect(e:Event = null):void
		{
			Bluetooth.service.connect( _device, uuid ); 
		}
		
		private static function bluetooth_connectListenErrorHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_connectListenHandler()");
			Model.state.connectState = "Waiting for connections (Connection already exists)";
		}
		
		private static function bluetooth_connectListenHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_connectListenHandler()");
			Model.state.connectState = "Waiting for connections";
		}
		
		private static function bluetooth_disconnectionHandler( event:BluetoothConnectionEvent ):void
		{
			Log.record("BluetoothManager.bluetooth_disconnectionHandler()");
			Model.state.connectState = "Disconnected";
			setTimeout(dispatchDisconnect,1000);
		}
		
		private static function bluetooth_receivedBytesHandler( event:BluetoothConnectionEvent ):void 
		{ 
			
/*			if(processing)
				return;
			
			processing = true;*/
			
			ba = new ByteArray;
			obj = new Object;
			//ba.position = 0;
			ba = Bluetooth.service.readBytes( uuid );
			_device = event.device;

			if(!connected)
				connected = true;

			try{
				obj = ba.readObject();
				
			}
			catch(e:Error) {
				Model.state.connectState= "cannot process data " + e.message; 
				trace("Reading object Failed\n" + e.name 
					+ " - " + e.message + "\n")
				return;
			}
		
			if(obj){
				if(obj.hasOwnProperty("type"))	
					Model.state.connectState = "Receiving "+obj.type+" data";
				else
					return;
			if(connected && obj.type == "gyro" || "drag" || "scene" || "bookmark"){
				MotionDataManager.push(obj);
			}	
			}
			 
			//processing = false;
			
		}
		
		public static function sendData(obj:Object):Boolean
		{
			var ba:ByteArray = new ByteArray;
			ba.writeObject(obj);
			
			var success:Boolean = Bluetooth.service.writeBytes( uuid, ba );
			//Model.state.connectState= success ? "Sending "+ obj.type +" Data ": "Can't send data";
			
			return success;
		}
		
		public static function close(event:Event = null):void
		{
			trace("BluetoothManager.close()");
			
			if(!initialised)
				return;

			connected = false;
			attemptingConnection = false;
			listening = false;
			
			Model.state.connectState = "";
			
			if(_device)
				var disconnectSuccess:Boolean = Bluetooth.service.disconnect(_device, uuid);
			
			if(Model.state.connectMode == State.CONNECT_MODE_BROADCAST)
				var setDeviceDiscoverableFalseSuccess:Boolean = Bluetooth.service.setDeviceDiscoverable(false);
			else
				var cancelScanSuccess:Boolean = Bluetooth.service.cancelScan();
			
			var closeSuccess:Boolean = Bluetooth.service.closeAll();
			var disableSuccess:Boolean = Bluetooth.service.disable();
		}
		
		
		private static function bluetooth_scanStartedHandler( event:BluetoothEvent ):void
		{
			Model.state.connectState = "scan started";
		}
		
		
		private static function bluetooth_scanModeChangedBroadcast( event:BluetoothEvent ):void
		{
			Model.state.connectState = event.data = "bluetooth:scan:mode:changed::scan:mode:connectable:discoverable"
				? "Waiting for connection" : "There is a problem: " + event.data;
		
			
			if(!listening && Bluetooth.service.getState() == "state:on")
				Bluetooth.service.listen(uuid);

		}
		
		private static function bluetooth_scanModeChangedReceive( event:BluetoothEvent ):void
		{
			Model.state.connectState = event.data + "/" + Bluetooth.service.getState();

			if(!attemptingConnection && Bluetooth.service.getState() == "state:on")
					connectToAvailableDevices()
		}
		
		
		private static function bluetooth_scanFinishedHandler( event:BluetoothEvent ):void
		{
			Model.state.connectState = "scan finished";
			
		}
		


	}
}