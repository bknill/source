package co.beek.mobile
{
	import away3d.core.math.Quaternion;
	
	import co.beek.model.*;
	
	import com.distriqt.extension.devicemotion.DeviceMotion;
	import com.distriqt.extension.devicemotion.DeviceMotionOptions;
	import com.distriqt.extension.devicemotion.SensorRate;
	import com.distriqt.extension.devicemotion.events.DeviceMotionEvent;
	
	import flash.desktop.NativeApplication;
	import flash.external.ExtensionContext;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	
	public class MotionControls
	{
		
		public var supported:Boolean;
		public var quaternion:Quaternion;
		private var key:String;
		private var dispatcher:EventDispatcher = new EventDispatcher;
		public const NEW_QUATERNION:String = "NEW_QUATERNION";
		
		
		public  function DeviceMotionConnect():void
		{
			
			//this will read the descriptor data
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0].toString();
			var appName:String = appXml.ns::name;
			var filename:String = appXml.ns::filename;
			var id:String = appXml.ns::id;
			var gId = id.slice(id.indexOf("A"));
			
			if(id.indexOf('.debug') > -1)
				id = id.replace('.debug',"");
			
			Model.state.mobileAppId = id;
			

			switch(id){
				
				case'co.beek.218':
					key = "ee6ee6af44ed872ef50f52d9f5121a6f712da318XsHnUwDcG8pugseVZJysq06fIsRFBQXcQP6JEn6KanHAAnBU8SCPPeLEO3hmNWFiRtm+Va6wsmHpmk8t4nDpGZT0SPZh+1T9eJY5XOmCISXqZh17B36Jq1uKnCP0DyMSdihFS4sgMMnH2+Gua/VgUwJMN37IBxcDBofbuqE/KAUNnHBLzoTzqN72KbhFJdz0EFKJ6IrT/vP5u34G9Fw7zvgbt+Nw5IIZGRM3VUZPkEboSli5rOCfWL6+43XiEOqayBeTsFWsFApFmadlV9MLx54REtgX8SDeGrBviXuvbAjtbYlmU35uTglU+6YxnEKYxN4NUA/nPs+7Lb2bQlmfbA==";	
					break;
				
				case'co.beek.g218':
					key = "95c501864062cf4388dac4c114e6e2316c66a93cCs+neqpFGSgoZvi8MIP5J7RXte613kBiQbl/zaTUpxfRNWFmqB+/SIlJag2NpiWRiMec+CWEtLA4JlJRmGwxY2fKpCFCtGPPOIhYT5/ajiP46+gXISHxtet1jX09RH7dAQpTUmrgTJKjjacBnLAf7NJAb+B1fUAWkfYB1ZZSjzIznN7H9UQIgt9/vaf//cGP6w+UsQ2e9zW0tm+jUlzsvzIq1RL+q7v2unaflGYQeUGRZjqEDU+Sh49K6dfa0siOj5eYDQaiUN4BTNLhXaF8cyutUVAxEctEKxw0i1UbjBFzjxi/c7K9FttKteVeEXWYY7w3EHQqZjnfqMAuM3ikUA==";	
					break;
				
				case'co.beek.280':
					key = "a79634d1fe4cfc52f0731fad0d71b59cc52d711cHZgGnVXFwL4/eoHiTg5khgcjbB4UL49KTQERV77O/TciLWh5doz1qg+h/ssysGKykzV18vS3Aec5NuksI7Virdp3bOegbaG1GBsC5V8nAj5EQlBRU7aGAeyVnvkwhmCfy/DBbCt/CIthQcJtqDqnhe9x159kt2X4JfkhrgBYydE9JCKp4DJ2Iy0QoNMmlnMAqrkm5XMfbtr24VVEiwRPDDAVknk+NqOCq43r5eaSkVl1sdr/PliF5BHF0aeO5n7sq/6jeCaQHHhb4wM7HmLCgTbcUQNtAPot1hBSlHy+TtF9/sDnAeoszXG4CQ31HrowboQueo1N7K61EFM1aR9ekw==";	
					break;
				
				case'co.beek.A280':
					key = "63d9b0b127f7a74dd3a35d18b45c28ba43f5d781Q574CknoD7xN+Ui7ucnvaSHtUn6EPGdbYcZ2RdcF82DfCehkBcRNS17kI0hqXtpssgkf+K4I5OJefo5LTfn2cCW3plKlTk1iud0MEVBXtMv/wCAq0mbMH2wzDu6H39OVa3GPb9IqqRREgr3VDZnx93bvcFkOMsTDsJN4LgJcLAOwVL0oyQ4BTuUr0gb31y604Gp0e8bpnBfClOH4xd2zfB/nU51BDHQXqnEyGMlLBiWW8M/uKip2mo3w/ZNYLxr9PnHq40RXLve/qP6tYLsF2YIzYUx7XYHgXok42RSPEofLnVF0t13WCPjY15g7GvLXyjpXbJvRKnfgnQ/xcMzerg==";
					break;

				case'co.beek.A93':
					key = "5dd4ca6fd160404ca82d2aaff9c39d9fca50324eEo+8a99K3urKnpjGX2qfD01kEZJmof+SD0PhNNesZMjvuJhKb/E+ie3awKx1Mo0Oq2Zg2UrZAOv/TAat2qdWl5rnQ7lKPnCSE4l92wh9+XkpSemXP/WNZ+2pdU8Y/EfNTAUw62ZVpe0jhTuRb9/Q4aSd7DKeBLdu4hdUwzlxVXmP6Kz/miyBt/2DpnpsYzfX6uUOfbsuHNIZHxxxAXYTh6iKlTWAW64vBODXeeG9R9Ms9j0bPnirfRh6OZ9mzxQnIiYggKl4poHZfuhfZOjc3dt0Zz0PjOEC0zh385NoYTLa1eUcuBt85x38Wo7HlJfMo/3MF1NQw6aZUzQm+/QGfg==";
					break;
				
				case'co.beek.A218':
					key = "81566636e8ead65640ebdabd2cd97d3ca8140792Vnd4VLPnXGL7qYJuhJdYvIjMYTqEfTEI0an7qkuBCVZ5jKYhzjyeF8Jo/YsIKkjovoaO6aFzr34IEHVXx20+AVE0SC1WWPuCbZXx/6zL7eBItaTeOTZO2CmnkUpQmx2eeowlhgknx94pGiIsKKFO/f2eH9B0YctW+IdOUnx79OcK21i8yISMxfRDzVF/Q0wwYvuWPqB1rEuzRyDVvwILRL6CP3XxZvHsLOsJYasxApfXDcuOIcn4aMRbtkhTcGCBV6MncYDGe9kr6z0LjVTAM9bB9oONip814tQW28T1A3b1ES9dTl7+j479aWlPOVbjCBbYwH+jf29OWtKi6THeHQ==";
					break;
				
				case'co.beek.282':
					key = "9045037d6b872bf0083c2d02dade697ff797933eLgUxRWcdW/NRECgiX/hozn/YzhV0CaRMtvwbTc5CCINvxiBRZpx07NaWqZefMEdaazACJY6vWz7TSjLdkNMU5NXVl++msTRDaqB/2JsCwwXmyrjZ5MoI279G3gueq+q/TawhKWjOV+LawUpYb+EPCp8dABqmyNiRxpApbRD0nq6X6DyIBrfRJ0BnzCdzuk+QUhGNMYDQ4jLpc+Oi96qIbjJT5EnZFuI2Q2Hi8BXZQcHFfL9i4GMn7tR/rrsuTCQMMuGy88Ov1hhahPzeFOuSwOxE0FW9hf4oQ0DXJWpx8JygHHO6XGtg+7jicaljzqcqcb280BuqPzxg5i63m7fQbw==";
					break;
				
				case'co.beek.A282':
					key = "67e94862efe6f6f361561699602dd6dcc155813aWZkxOy2UyCZnGMjMK6LFEB+6Mp8ywSAj/sWKGAy6n7mU64J8b7nDuLAnbvZhDjIBD0qkuh7jkXIXNYn808y5NoePMBt0kbaUfndEMy7Xv00MtXGrtX80TatT6d4vDC/HKrCWEEueSfmHIUoCDRAJBZZJc2T19mCSLCwGBymrPFZ0r2h//CESkYDjcK1ffLElv4w8wdJI9wKedxmKfPvaaVPoJH/hm8ULUy6xZeEi1bjFgjP5IEP896BQ14u9uxWntm+Dq9kNbpsKj1oidDFHvotCdtlcmv1zscDz1i+dweMVRY3myf0AokbKmCH3qLJ2opVu2haOr3SwFYUlfyPY6g==";
					break;
				
				case'co.beek.g315':
					key = "e2005ed161a12ce8c667dfa475b423451ba6f895eATm3a7jwlL5VEW4EWtdrh4X95lFceWeKjt1RSzMp7+y20MBE6hX+OiLUWCeHrO34cNAPcD76GY1RKxmBzIYa0JbKzNZpKzI1T2wtJg47tsYF3CJsk4Leio3LYQPgI4CdQ9FqmBkId2YEhlfItYJPcoryktjxDS5R8ug6db1AKO8rRPDDWpxRVjrE/k6a4aKo5xOO1++nFHnF1fkqQyD++BXluPGWeOCAu3yYEY6BnK2eIhO4CcmVEJCjs0I/n/obUSb2I/ALWa5osamGItwtzQwAnZdE+KS+Z71N3LsaBx+KgrRPnKDzdKaXUKbFA6HPFJJmO/vszDqgZwS7y550Q==";
					break;
				
				case'co.beek.g321':
					key = "468decdff3f47de3a78347144600c65716c30b70ae3zMm5235N5h9af+ZUcr3MvBPjg0KIvOqa+lXfXkZp1WvIac+kE0zn+pfsEClW+rhmjzX62m239HITsjFjfPabd88WAbM2pZmxxB6bF+7QL8cuca2ZD4YQvoHmXm8aYUQtYUfMLage+vSmig3FBh3W7oxz2GqEpcyTKZS69+sqZYJSLtj4ra/QAti3tt0/g5msmkjpvXoDwjVVlxDKezmZvL/zTro1wteh0YPQ3qpZ5FDBVeRcFXA16RjJdaGf5v1UbwZtyfY2bNRvAVNLSsgFC73cIwqdhL24RklQxAn44m3r4tbLxTVjNTmtPwUKhG3a4nPRNX4p28yEgC8QS/A==";
					break;
				
				case'co.beek.g340':
					key = "dc510ac0ae0ce65d6b0c2dc3b577ac00d7532cb0Gjg7+6KKPvSPvI7TNiuI9DGPhto9Oa/GN0yMd2DIjKW8knZrkUAOj4GiNIiAIyAEVsddVhU/9GR33SMPUujqjXxX6oEeFmCdeU/Ffs/apDPKvbWDpicV7BTSr9ce8hA+eJ0gb/lwvl5IcqVsfYrEbwWyWtSHYREA9zjFvTVmi596Zx8XGcg7M+HAalte9+B48L1pcvX50NyC44G+Re8tnqYvQd7HlzHrUuwVQ2dhPLj8C4iEBCOhwZy1J11dA3QM+j8gqy3per0hgCsTih9UamJnl5KRupg6VcVQAdzwCZY3/WQ8WfZTod/qEy/iS4aAY/HR9gCY3EUI6jLI52THvg==";
					break;
				
				case'co.beek.g350':
					key = "3055aaee503c25301ff7cea0e18b87a52b8326eac23/0uhuroDpuBAUTLV+jwDKiOOimUUoWrpZXJUhELUVbyoMKvwxXnay/eMPGdaudmjQXZUmgSk5qklX68klq+Vi8M8t9ZInjdRpR2/fhh3SiM/sH7SO7o3vjYNPk3HcJy/8ZvtPWzQidL6bNCSphgz8EILDVrh44T+jfwwkKWIXvoij5EodXvlQCFBuB5RJq4PuAX9THk5oz2A37imTld9qPtDz6Jxn0pZwms2tdCo4jyHd18LMkTTxWfJdEGT7nmiDSx1wWVDRazwEZOXSeTCDUKzhYdQKFuCR2sOzoQ+7GOXkadkv3V+YJfv117Vl9bgiSHQVYxiY32KaYs1gtg==";
					break;
				
				case'co.beek.g303':
					key = "01cd182bf4d570e853bc348aecafb3164781c425SfAgdrvRviH3qu1oC3o/WMYcqD/m4t23czdmOLz80CdaGb488WLSCHhWbLgVOSK4SSjjotqG704PNUP++D9/TsULT0dudZPe31eMC6BMMzVuEaY7Faclqf5i1ppoE3LnbnDT/wwz50jqTnMqi+AKKfrMXbSJxi9XNoUpZTzf349JOQEI+yJaMFQvEl8DcGTXQ8hO2xMl6/4qoTxzNKjCNJh9oO6iJTaRRRumTx2g9nmu4H6EDEC3CJxkR91D3Noa/rTPKLoq2aGpdNtIc1Upy9COO0fvwcnAXa0PQAXicy658HH7i9AnO46JRQLJFP7yyMAkM0/63L2Fon/ffsvmQQ==";
					break;
				
				case'co.beek.g318':
					key = "cafa9e3fa87b8eb81e09afe27fd70d65fd31b827MVP40jvKJSEb42QO0+R3oUPIL81I3Hk+niJnkKbBggRPOP8aHXE6qOws2O98HQz4gxZ5O7SYqs9+zs7/xUSQ654cWqzj1EqYLpIEy3LxmLFQF7I/ISWaNTUp9PxcjXk0e8ajH+ofMuK6ez3XObJ42tGsyJmI7wF5p+BWuUyhZMUKqyd5tRQYeQaYDq+rR2GaJN+Fw9l0M299UJRZyjBFOVOFR7sQ5fUk3mu3Zx2e4hdA0HbaZoxfinseRokittejLIZIOtV0xYBcBbhKoszyhrjoCvRw/N4Z2WMggEHbH7tIghh5C0P5VxukgnFpIURBAWQ7AsHcy6nAkQ/basPIMA==";
					break;
				
			}
					

			try{
				
		

				Model.state.distriqKey = key;
				DeviceMotion.init(key);
				
				trace( "DeviceMotion Supported: " + DeviceMotion.isSupported );
				trace( "DeviceMotion Version:   " + DeviceMotion.service.version );
				
				DeviceMotion.service.addEventListener( DeviceMotionEvent.UPDATE_QUATERNION, deviceMotion_updateHandler );
				
				var options:DeviceMotionOptions = new DeviceMotionOptions();
				options.rate        = SensorRate.SENSOR_DELAY_FASTEST;
				options.algorithm   = DeviceMotionOptions.ALGORITHM_NATIVE;
				options.format      = DeviceMotionOptions.FORMAT_QUATERNION;
				
				DeviceMotion.service.register( options );
				
				supported = DeviceMotion.isSupported;

				
			}
			catch (e:Error)
			{
				trace( "ERROR::" + e.message );
			}
			
			
		}
		
		
		private  function deviceMotion_updateHandler( event:DeviceMotionEvent ):void
		{
		  quaternion = new Quaternion( event.values[1], event.values[2], event.values[3], event.values[0] );		
		  dispatchEvent(new Event(NEW_QUATERNION));
		}
		
		public  function addEventListener(type:String, listener:Function):void
		{
			dispatcher.addEventListener(type, listener);
		}
		
		public  function dispatchEvent(event:Event):void
		{
			dispatcher.dispatchEvent(event);
		}
	
	}
}