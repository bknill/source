package co.beek.mobile
{
	import com.sickle.flickbackFlash.FlickbackConnect;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.PixelSnapping;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.Security;
	import flash.utils.clearInterval;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.loading.ImageLoader;
	import co.beek.mobile.ServiceChecker;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.guide.GuideSectionData;
	import co.beek.pano.Pano;
	import flash.filters.DropShadowFilter;
	
	
	public class IntroScreen extends introScreenFl
	{
		public static const INTRO_COMPLETE:String ="INTRO_COMPLETE";
		
		
		private var appScale:Number = 1;
		private var guideData:GuideData;
		private var guideThumbHolder:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		private var nextCounter:int = 1;
		private var iconsToShow:Vector.<DisplayObject> = new Vector.<DisplayObject>;
		private var introInt:int;		
		private var _width:Number;
		private var _height:Number;
		private var buttonScale:Number = 3;
		private var serviceChecker:ServiceChecker = new ServiceChecker;
		
		[Embed(source="../../../../src/assets/icons321/icon1024.png")]
		private var cover:Class;
		
		private var coverBitmap:Bitmap;
		
		private var sceneReady:Boolean = false;
		
		public function IntroScreen()
		{
			
			super();
			
			Log.record('IntroScreen()');
			
			introStartButton.visible = false;
			introNextButton.visible = false;
			
			icons.visible = false;
			arrows.visible = false;
			

			text.autoSize = "center";
			text.wordWrap = true;
			
			
			Model.state.addEventListener(State.START_CHANGE,function():void{
				Log.record("IntroScreen.onStartChange();");
				Fonts.format(text,Fonts.bodyFont, Model.state.startState);
				if ( stage ) stage.quality = stage.quality;
			});
			

			introNextButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			introNextButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			introNextButton.addEventListener(MouseEvent.MOUSE_UP, onNextButton);
			
			introStartButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			introStartButton.addEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			introStartButton.addEventListener(MouseEvent.MOUSE_UP, startPlayer);
			
			//FlickbackConnect.init();
			serviceChecker.start();
			addEventListener(Event.ACTIVATE,onActivate);
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		
		private function onAddedToStage(e:Event):void
		{
			stage.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		    _width = stage.fullScreenWidth > stage.fullScreenHeight ? stage.fullScreenWidth : stage.fullScreenHeight;
			_height = stage.fullScreenWidth > stage.fullScreenHeight ? stage.fullScreenHeight : stage.fullScreenWidth;
			resize(_width, _height);
		
			
			Model.state.addEventListener(State.PANO_TILES_LOADED, onPanoTilesLoaded);
		
		
			introduction();
		
		}
		
		private function introduction():void
		{
			coverBitmap = new cover() as Bitmap;
			coverBitmap.scaleX = coverBitmap.scaleY = (_height * 0.6)/coverBitmap.height;
			coverBitmap.x = _width * 0.25 - coverBitmap.width/2;
			coverBitmap.y = _height * 0.5 - coverBitmap.height/2;
			addChild(coverBitmap);
			
			coverBitmap.filters = [new DropShadowFilter(20,45,0x000000,0.1,20,20,1.0,1,false,false,false)];
			
			
			//Fonts.format(text,Fonts.bodyFont, "      Loading Data - Please Wait");
	
		}
		
		private function onActivate(e:Event){
			Log.record("IntroScreen.onActivate()");
			if ( stage ) stage.quality = stage.quality;
			//resize(width, height);
		}

		
		public function onPanoTilesLoaded(e:Event = null):void
		{
			Log.record("IntroScreen.onPanoTilesLoaded()");
			bg.alpha = 0.85;
			sceneReady = true;
			introStartButton.visible = true;
			introNextButton.visible = true;
			
			Fonts.format(introNextButton.text,Fonts.titleFont, "Tips");
			//next();
			

			
			//start intro animation
			//introInt = setInterval(nextIntro,5000);
		}
	
		
		public function resize(width:Number, height:Number):void
		{
			bg.width = _width =  width > height ? width : height;
			bg.height = _height = height < width ? height : width;
	
			Log.record("resize" + width);
			
			text.scaleX = text.scaleY = icons.scaleX = icons.scaleY = arrows.scaleX = arrows.scaleY = Model.appScale;

			locator.x = _width * 0.25;
			locator.y = _height * 0.5;
			
			
			arrows.x = _width*0.25 - arrows.width/2;
			arrows.y = _height *0.5 - arrows.height/2;
			
			text.x = _width/2;
			text.y = _height/2;
			text.width = _width * 0.3;

			

			
			introStartButton.scaleX = introStartButton.scaleY = Model.appScale;
			introStartButton.x = width - introStartButton.width - 5;
			introStartButton.y = height - introStartButton.height - 5;
			
			introNextButton.scaleX = introNextButton.scaleY = Model.appScale;
			introNextButton.x = introStartButton.x - introNextButton.width - 5;
			introNextButton.y = introStartButton.y;
		}

		
		public function guideDataLoaded():void
		{
		 //button.text.text = "loaded";
		}
		
		private function onNextButton(e:Event):void
		{
			e.target.alpha = 1;
			
			nextIntro();
			//clearInterval(introInt);
			if(coverBitmap.parent == this)
				removeChild(coverBitmap);
			
			Fonts.format(introNextButton.text,Fonts.titleFont, "Next");
		}
		
		public function nextIntro():void
		{
			//put icons back in icon movie clip
			for( var h:int ; h < iconsToShow.length ; ++h ){
				if(iconsToShow[h].parent == this){
					removeChild(iconsToShow[h]);
					icons.addChild(iconsToShow[h]);
				}
			}
			iconsToShow.length = 0;
			next();
		}
		
		private function next():void
		{
			Log.record('IntroScreen.next()');
			
			switch(nextCounter)
			{
				case 1:
				{
					icons.visible = true;
					arrows.visible = true;
					iconsToShow.push(arrows.left, arrows.right, arrows.up, arrows.down, icons.tabletIcon);
					updateUI('This is a Virtual Reality app, hold your device in front of you and look around by moving your device around you');
					break;
				}
					
				case 2:
				{
					iconsToShow.push(arrows.left, arrows.right, icons.fingerIcon);
					updateUI('You can also drag the image with your finger, up and and around');
					break;
				}
					
				case 3:
				{
					iconsToShow.push(arrows.up, arrows.down, icons.tabletIcon);
					updateUI('Shake your device to return to motion controls');
					break;
				}
					
				case 4:
				{
					iconsToShow.push(icons.guideButton);
					icons.guideButton.scaleX = icons.guideButton.scaleY = buttonScale;
					updateUI('Use the menu to explore other scenes and find where you want to go next');
					break;
				}
					
				case 5:
				{
					iconsToShow.push(icons.nextButton);
					icons.nextButton.scaleX = icons.nextButton.scaleY = buttonScale;
					updateUI('Press Next to go to the next scene in the list');
					break;
				}
					
				case 6:
				{
					iconsToShow.push(icons.bookmarkButton);
					icons.bookmarkButton.scaleX = icons.bookmarkButton.scaleY = buttonScale;
					updateUI('Press bookmark to save scenes you like and want to access later');
					break;
				}
					
				case 7:
				{
					iconsToShow.push(icons.playButton);
					icons.playButton.scaleX = icons.playButton.scaleY = buttonScale;
					updateUI('Press play to watch in auto-presentation mode. Press the screen at any time to return to normal mode');
					break;
				}
					
				case 8:
				{
					iconsToShow.push(icons.vrButton);
					icons.vrButton.scaleX = icons.vrButton.scaleY = buttonScale;
					updateUI('If you have a Virtual Reality Headset,  press the VR Button to enter split screen view');
					nextCounter = 0;
					break;
				}
			
			}
			
			nextCounter++;


			
		}
		
		
		
		private function updateUI( string:String):void
		{
			
			//hide all icons
			for( var i:int = 0 ; i < icons.numChildren ; ++i )
						icons.getChildAt(i).visible = false;
			
			//hide all arrows
			for( var i:int = 0 ; i < arrows.numChildren ; ++i )
				arrows.getChildAt(i).visible = false;
			

			//show new icons
			for( var h:int ; h < iconsToShow.length ; ++h ){
				iconsToShow[h].visible = true;
				
				if(iconsToShow[h].parent != arrows){
					if(icons.contains(iconsToShow[h]))
						icons.removeChild(iconsToShow[h]);
					addChild(iconsToShow[h]);
					iconsToShow[h].x = locator.x - iconsToShow[h].width/2;
					iconsToShow[h].y = locator.y - iconsToShow[h].height/2;
				}
			}
			
			
			Fonts.format(text,Fonts.bodyFont, string);
			text.width = _width * 0.3 / Model.appScale;
			text.height = text.textHeight;
			text.y = locator.y - text.height/2;
			text.cacheAsBitmap = true;
			
			if ( stage ) stage.quality = stage.quality;
		}

		
		
		private function startPlayer(event:Event):void {
			
			
			Tweener.addTween(guideThumbHolder,{
				x: 0,
				time: 0.2
			});

			Tweener.addTween(bg,{
			x: width,
			time: 0.1,
			onComplete: cleanUp
			});
			
			Tweener.addTween(info,{
				alpha: 0,
				time: 0.2
			});	
			
			setTimeout(function():void{
				Model.dispatchEvent(new Event(Model.INTRO_COMPLETE));
			},1000);
			
			clearInterval(introInt);
			
		}
		
		private function cleanUp():void {
			
			parent.removeChild(this);
			
			introNextButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			introNextButton.removeEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			introNextButton.removeEventListener(MouseEvent.MOUSE_UP, onNextButton);
			
			introStartButton.removeEventListener(MouseEvent.MOUSE_DOWN, onButtonMouseDown);
			introStartButton.removeEventListener(MouseEvent.MOUSE_OUT, onButtonMouseOut);
			introStartButton.removeEventListener(MouseEvent.MOUSE_UP, startPlayer);
			
			
		}
		
		
		private function tween(element:DisplayObject, time:Number, x:Number, y:Number, delay:Number):void
		{
			var origX:Number = element.x;
			var origY:Number = element.y; 
			
			Tweener.addTween(element, {time:time, x: x, y: y, onComplete: tweenReset, onCompleteParams: [element, time, x, y, origX, origY, delay]});
		}
		
		private function tweenReset(element:DisplayObject, time:Number, x:Number, y:Number, origX:Number, origY:Number, delay:Number):void
		{
			setTimeout(function():void{Tweener.addTween(element, {time: time, x: origX, y: origY,onComplete: tween,onCompleteParams: [element, time, x, y, delay]})},delay);	
		}
		
		
		private function onButtonMouseDown(event:MouseEvent):void
		{
			event.target.alpha = 0.5;
			
		}
		
		
		private function onButtonMouseOut(event:MouseEvent):void
		{
			event.target.alpha = 1;
			
		}
		
		public function updateGuideContents():void
		{
			trace("updateGuideContents()");
			var string:String = " Loading ";
			
			for each(var section:GuideSectionData in Model.guideSectionList){
				if(section != Model.guideSectionList[Model.guideSectionList.length - 1])
					string = string + section.title + ", ";
				else
					string = string.substr(0,string.length - 2) + " and " + section.title;
			}
			
			updateUI(string);
		
		}
		

		
	}
}