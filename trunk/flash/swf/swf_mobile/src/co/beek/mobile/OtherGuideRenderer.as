package co.beek.mobile
{
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.Fonts;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	public class OtherGuideRenderer extends Sprite
	{
		
		public var _guide:GuideBasicData;
		private var guideImage:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
		private var label:TextField = new TextField();
		
		public function OtherGuideRenderer(data:GuideBasicData)
		{
			_guide = data;
			
			Fonts.format(label,Fonts.boldFont,guide.title);
			label.textColor = 0xffffff;
			this.addChild(label);
			this.addChild(guideImage);
			
			ImageLoader.load(
				Model.config.getAssetUrl(guide.thumbName), 
				imageLoadCallback,
				imageLoadCallbackError
			);	
		}
		
		private function imageLoadCallback(data:BitmapData):void
		{
			guideImage.bitmapData = data;
			guideImage.width = 150;
			guideImage.height = 160;	
			label.y = guideImage.height + 2;
		}
		
		private function imageLoadCallbackError():void
		{
			trace("OtherGuideRenderer.imagePageLoadCallbackError()");
		}
		
		public function set guide(data:GuideBasicData):void
		{
			_guide = data;
		}
		
		public function get guide():GuideBasicData
		{
			return _guide;
		}
	

	
	}
	
}