package co.beek.mobile
{
	import co.beek.model.*;
	
	import flash.data.*;
	import flash.display.*;
	import flash.events.*;

	public class Bookmarks extends Sprite
	{
		
		private var bmRenderers:MovieClip = new MovieClip();
		private var beekDB:SQLConnection;
		private var bmButton:BookmarkButtonFl = new BookmarkButtonFl;
		
		public function Bookmarks()
		{
			super();
			bmButton.addEventListener(MouseEvent.CLICK,onBookMarkMouseDown);
			bmButton.addEventListener(MouseEvent.CLICK,onBookMarkMouseUp);
			addChild(bmButton);
			addChild(bmRenderers);
			
			bmRenderers.x -= bmRenderers.width + 1;

		}
		
		public function connect(db:SQLConnection):void
		{
			beekDB = db;
			var createBookmarkTableStmt:SQLStatement = new SQLStatement();
			createBookmarkTableStmt.sqlConnection = beekDB;
			createBookmarkTableStmt.text = "CREATE TABLE IF NOT EXISTS `bookmarkScenes` (" +
				"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`scene_id` INTEGER, " +
				"`scene_title` TEXT," +
				"`scene_thumb` TEXT"  +
				")";
			createBookmarkTableStmt.execute();
			
			loadBookmarkScenes();
			
		}
		
		public function bookmarkScene():void{
			trace('bookmarkScene()');

			if(bmRenderers.getChildByName(Model.currentScene.id) || !Model.guideScene)
				return;
			
			var insertStatement:SQLStatement = new SQLStatement();
			insertStatement.sqlConnection = beekDB;
			insertStatement.text = "INSERT INTO `bookmarkScenes` (`scene_id`, `scene_title`, `scene_thumb`) values (?, ?, ?)";
			insertStatement.parameters[0] = Model.guideScene.scene.id;     
			insertStatement.parameters[1] = Model.guideScene.scene.title;   
			insertStatement.parameters[2] = Model.guideScene.scene.thumbName;  
			insertStatement.execute();
			
			Model.dispatchEvent(new Event(Model.BOOKMARK_SCENE));
			
			loadBookmarkScenes();
			
		}
		
		public function loadBookmarkScenes():void
		{
			trace('swf_mobile.loadBookmarkScenes()');
			
			var getAllStatement:SQLStatement = new SQLStatement();
			getAllStatement.sqlConnection = beekDB;
			getAllStatement.text = "SELECT * FROM `bookmarkScenes`";
			getAllStatement.execute();
			
			var data:Array = getAllStatement.getResult().data;
			var bmX:int = 0;
			
			while(bmRenderers.numChildren > 0)
				bmRenderers.removeChildAt(0);
			
			
			for each(var bookmark:Object in data)
			{
				
				var bm:BookmarkButton = new BookmarkButton(bookmark.scene_id, bookmark.scene_title, bookmark.scene_thumb);
				bm.x =  - (bmX + bm.width);
				bm.name = bookmark.scene_id;
				bmRenderers.addChild(bm);
				bmX += bm.width + 1;
				bm.addEventListener(BookmarkButton.BOOKMARK_REMOVED, onBookmarkRemoved);
			}
			
		}
		
		private function onBookmarkRemoved(e:Event):void{
			trace('onBookmarkRemoved()');
			
			var bm:BookmarkButton = e.target as BookmarkButton;
			bm.removeEventListener(BookmarkButton.BOOKMARK_REMOVED, onBookmarkRemoved);
			
			var removeStatement:SQLStatement = new SQLStatement();
			removeStatement.sqlConnection = beekDB;
			removeStatement.text = "DELETE FROM `bookmarkScenes` WHERE `scene_id` = :sceneId";
			removeStatement.parameters[":sceneId"] = bm.scene.id;
			removeStatement.execute();
			
			loadBookmarkScenes();
			
		}
		
		private function onBookMarkMouseDown(e:MouseEvent):void{
			bmButton.alpha = 0.6;
		}
		
		private function onBookMarkMouseUp(e:MouseEvent):void{
			bmButton.alpha = 1;
			bookmarkScene();
		}	
		
	}
}