package co.beek.mobile
{
	import caurina.transitions.Tweener;
	
	import co.beek.loading.ImageLoader;
	import co.beek.model.Model;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.guide.GuideSceneData;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.*;
	
	public class BookmarkButton extends BookMarkSceneFl
	{
		
		public static const BOOKMARK_REMOVED:String = "BOOKMARK_REMOVED";
		
		private var _scene:SceneBasicData;
		var dragging:Boolean = false;
		var bounds:Rectangle;
		var dragInt:int;
		var initalMouseY:Number;
			
		
		public function BookmarkButton(id:String, title:String, thumb:String)
		{
			super();
			label.text = title;
			ImageLoader.load(
				Model.config.getAssetUrl(thumb), 
				bookmarkImageLoadCallback,
				bookmarkImageLoadCallbackError
			);	
			
			
			for each(var guideScene:GuideSceneData in Model.guideSceneList)
				if(guideScene.scene.id == id)
					_scene = guideScene.scene;
			
			addEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP,onMouseUp);

		}
		
		
		private function bookmarkImageLoadCallback(data:BitmapData):void
		{
			trace("imagePageLoadCallback()");
			var buttonImage:Bitmap = new Bitmap(null, PixelSnapping.ALWAYS, true);
			buttonImage.bitmapData = data;
			addChild(buttonImage);
			buttonImage.width = 45;
			buttonImage.height = 42;	
		}
		
		private function bookmarkImageLoadCallbackError():void
		{
			trace("GuideSectionPage.imagePageLoadCallbackError");
		}
		
		public function get scene():SceneBasicData
		{return _scene}
		
		private function onMouseDown(e:MouseEvent):void
		{			
			trace('BookmarkButton.onMouseDown()');
			alpha = 0.6;
			addEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
		}
		
		
		private function onMouseUp(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseUp()');
			Model.state.loadingScene = scene;
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
			setTimeout(function(){alpha = 1},10);
			this.y = 0;
		}

		private function onMouseMove(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMove()');
			removeEventListener(MouseEvent.MOUSE_UP,onMouseUp);
			initalMouseY = stage.mouseY;
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMove);
			addEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
			addEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);

		}
		
		private function onMouseMoving(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMoveUp()');
			drag();
		}
		
		private function onMouseMoveUp(e:MouseEvent):void
		{
			trace('BookmarkButton.onMouseMoveUp()');
			//clearInterval(dragInt);
			this.y = 0;
			removeEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
		}
		
		private function fadeAway():void
		{
			trace('BookmarkButton.fadeAway()');
				Tweener.addTween(this, {
					time:0.2,
					alpha: 0,
					transition: "easeInCubic",
					onComplete: dispose});
		}	
		
		private function dispose():void
		{
			trace('BookmarkButton.dispose()');
			dispatchEvent(new Event(BOOKMARK_REMOVED));
			removeEventListener(MouseEvent.MOUSE_UP, onMouseMoveUp);
			removeEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
			removeEventListener(MouseEvent.MOUSE_MOVE,onMouseMoving);
			removeChildren();
		}
		
		private function drag():void
		{
			trace("drag");
			if(this.y < - this.height * 1.5)
			{
				clearInterval(dragInt);
				fadeAway();
			}
			else
				this.y = stage.mouseY - initalMouseY;
		}
	}
}