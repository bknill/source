package co.beek.offline
{
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.data.FileData;
	import co.beek.model.data.SceneBasicData;
	import co.beek.model.data.SceneData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.MediaData;
	import co.beek.model.hotspots.VideoData;
	import co.beek.service.ServiceLoader;
	import co.beek.service.ServiceResponse;
	import co.beek.utils.JSONBeek;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class OfflineGuideScene extends EventDispatcher
	{
		public static const READY:String = "READY";
		
		public static const PROGRESS:String = "PROGRESS";
		
		public static const COMPLETE:String = "COMPLETE";
		
		private var _loaders:Vector.<IOfflineFile>;
		
		private var _guideScene:GuideSceneData;
		
		public function OfflineGuideScene(guideScene:GuideSceneData, loaders:Vector.<IOfflineFile>)
		{
			super();
			this._guideScene = guideScene;
			this._loaders = loaders;
		}
		
		public function get guideScene():GuideSceneData
		{
			return _guideScene;
		}

		public function get sceneJsonName():String
		{
			return "scene_"+_guideScene.scene.id+".json";
		}
		
		public function start():void
		{
			addHotspotsToQueue(_guideScene.hotspots);
			cacheGuideSceneIntroVideo();
			
			//sometimes the scene is in the guide more than once.
			if(!isInLoaders(sceneJsonName))
				loadSceneDetail();
			else
				dispatchEvent(new Event(READY));
		}
		
		private function loadSceneDetail():void
		{
			var loader:ServiceLoader = new ServiceLoader(SceneData);
			var url:String = Model.config.serviceUrl+"/scene/"+_guideScene.scene.id+"/json";
			loader.load(url, onSceneDataLoaded);
		}
		
		private function onSceneDataLoaded(scene:SceneData):void
		{
			//trace("ZipScene.onSceneDataLoaded(scene:SceneData):void")
			// If we have been disposed already
			if(_loaders == null)
				return;
			
			_loaders.push(new JsonData(sceneJsonName, JSONBeek.encode(ServiceResponse.create(scene.data))));
			cacheAsset(scene.thumbName, "thumb");
			
			
				cachePreviewTile(scene, "f");
				cachePreviewTile(scene, "b");
				cachePreviewTile(scene, "l");
				cachePreviewTile(scene, "r");
				cachePreviewTile(scene, "u");
				cachePreviewTile(scene, "d");
				cache4DeepTiles(scene, "f");
				cache4DeepTiles(scene, "b");
				cache4DeepTiles(scene, "l");
				cache4DeepTiles(scene, "r");
				cache4DeepTiles(scene, "u");
				cache4DeepTiles(scene, "d");
			
			addHotspotsToQueue(scene.hotspots);
			
			// start the first cacher
			dispatchEvent(new Event(READY));
		}
		
		private function cachePreviewTile(scene:SceneData, face:String):void
		{
			cacheAsset(scene.panoPrefix+"_"+face+"/9/0_0.jpg", "preview tile");
		}
		
		private function cache4DeepTiles(scene:SceneData, face:String):void
		{
			//cacheAsset(scene.panoPrefix+"_"+face+"/9/0_0.jpg", "pano tile preview");
			
			cacheAsset(scene.panoPrefix+"_"+face+"/10/0_0.jpg", "pano tile front");
			cacheAsset(scene.panoPrefix+"_"+face+"/10/0_1.jpg", "pano tile left");
			cacheAsset(scene.panoPrefix+"_"+face+"/10/1_0.jpg", "pano tile right");
			cacheAsset(scene.panoPrefix+"_"+face+"/10/1_1.jpg", "pano tile back");

			
			/*
			cacheURL(scene.panoPrefix+"_"+face+"/11/0_0.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/0_1.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/0_2.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/0_3.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/1_0.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/1_1.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/1_2.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/1_3.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/2_0.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/2_1.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/2_2.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/2_3.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/3_0.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/3_1.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/3_2.jpg");
			cacheURL(scene.panoPrefix+"_"+face+"/11/3_3.jpg");
			*/
		}
		
		/*private function addMapTile(style:String, zoom:int, tile:Tile):void
		{
			var path:String = mapData.getPath(tile);
			
			createLoader(Model.config.mapTileServer, path, "Maptile "+tile.across+", "+tile.down);
		}*/

		
		private function addHotspotsToQueue(hotspots:Vector.<HotspotData>):void
		{
			for(var i:int = 0; i<hotspots.length; i++)
				if(hotspots[i] is MediaData && !(hotspots[i] is VideoData))
					cacheFile(MediaData(hotspots[i]).file, MediaData(hotspots[i]).title);
		}
		
		private function cacheFile(data:FileData, name:String):void
		{
			if(data != null)
				cacheAsset(data.realName, name);
		}
		
		private function cacheSceneThumb(scene:SceneBasicData):void
		{
			if(scene.thumbIncrement > 0)
				cacheAsset(scene.thumbName, scene.title+" thumb");
		}
		
		private function cacheGuideSceneIntroVideo():void
		{
			if(_guideScene.transitionVideo)
				cacheAsset(_guideScene.transitionVideo," intro video");
			
			if(_guideScene.voiceover)
				cacheAsset(_guideScene.voiceover," voice over");
		}
		
		
		private function cacheAsset(path:String, name:String):void
		{
			createLoader(Model.config.assetCdn, path, name);
		}
		
		private function createLoader(server:String, path:String, name:String):void
		{
			//trace("createLoader(server:"+server+", path:"+path+")");
			if(!isInLoaders(path))
				_loaders.push(new AssetLoader(server, path, _guideScene.titleMerged + " - "+name));
		}
		
		
		private function isInLoaders(path:String):Boolean
		{
			for(var i:int = 0; i<_loaders.length; i++)
				if(_loaders[i].path == path)
					return true;
			
			return false;
		}
		
		public function dispose():void
		{
			_loaders = null;
			//mapData = null;
			_guideScene = null;
		}
		
		/*public function get loaders():Vector.<IZipFile>
		{
			return _loaders;
		}
		*/
		/*
		public function set loadIndex(value:int):void
		{
			_loadIndex = value;
			if(value == 0)
				dispatchEvent(new Event(READY));
			
			if(_loadIndex >= _loaders.length)
			{
				dispatchEvent(new Event(COMPLETE));
			}
			else
			{
				dispatchEvent(new Event(PROGRESS));
				loadNext();
			}
		}
		
		
		public function get loadIndex():int
		{
			return _loadIndex;
		}

		public function get loaders():Vector.<AssetLoader>
		{
			return _loaders;
		}
		
		public function get totalLoaders():int
		{
			return _loaders.length;
		}
		
		private function loadNext():void
		{
			cacher = _loaders[_loadIndex];
			cacher.addEventListener(AssetLoader.ERROR, onLoadError);
			cacher.addEventListener(AssetLoader.COMPLETE, onLoadComplete);
			cacher.start();
		}
		
		private function onLoadError(event:Event):void 
		{
			var loader:AssetLoader = AssetLoader(event.target);
			//trace("onLoadError: "+loader.path);
			loader.removeEventListener(AssetLoader.ERROR, onLoadError);
			loader.removeEventListener(AssetLoader.COMPLETE, onLoadComplete);
			
			loadIndex ++;
		}
		
		private function onLoadComplete(event:Event):void 
		{
			var loader:AssetLoader = AssetLoader(event.target);
			//trace("onLoadComplete: "+loader.path);
			
			loader.removeEventListener(AssetLoader.ERROR, onLoadError);
			loader.removeEventListener(AssetLoader.COMPLETE, onLoadComplete);
			
			//zip.addFile(loader.path, loader.data);
			
			
			loadIndex ++;
		}
		*/
	}
}