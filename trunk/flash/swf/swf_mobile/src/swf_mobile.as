package
{
	
	import flash.data.SQLConnection;
	import flash.data.SQLStatement;
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.AccelerometerEvent;
	import flash.events.BrowserInvokeEvent;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.InvokeEvent;
	import flash.events.MouseEvent;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.StageOrientationEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Camera;
	import flash.media.StageWebView;
	import flash.media.Video;
	import flash.net.Socket;
	import flash.sensors.Accelerometer;
	import flash.system.System;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.clearInterval;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.core.math.Quaternion;
	import away3d.events.Stage3DEvent;
	
	import co.beek.Fonts;
	import co.beek.Log;
	import co.beek.connect.ConnectManager;
	import co.beek.connect.MotionDataManager;
	import co.beek.connect.WebSocketManager;
	import co.beek.connect.WifiManager;
	import co.beek.event.NetworkDataEvent;
	import co.beek.guide.StarlingGuide;
	import co.beek.mobile.BluetoothLEManager;
	import co.beek.mobile.BluetoothManager;
	import co.beek.mobile.MobileConfig;
	import co.beek.mobile.MotionControls;
	import co.beek.mobile.ServiceChecker;
	import co.beek.model.Constants;
	import co.beek.model.Model;
	import co.beek.model.State;
	import co.beek.model.data.SceneData;
	import co.beek.model.data.SceneElementData;
	import co.beek.model.events.HotspotEvent;
	import co.beek.model.guide.GuideBasicData;
	import co.beek.model.guide.GuideData;
	import co.beek.model.guide.GuideSceneData;
	import co.beek.model.hotspots.HotspotData;
	import co.beek.model.hotspots.PhotoData;
	import co.beek.offline.OfflineManager;
	import co.beek.pano.Pano;
	import co.beek.service.GuideService;
	import co.beek.service.ServiceLoader;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.TouchEvent;
	
	//import co.beek.utils.*;
	
	
	public class swf_mobile extends Sprite
	{
		private var pano:Pano;
		private var beekDB:SQLConnection = new SQLConnection;
		private var serviceChecker:ServiceChecker = new ServiceChecker;
		private var useGyro:Boolean = true;
		private var socket:Socket;
		private var stageWidth:Number = 500;
		private var stageHeight:Number = 375;
		private var appScale:Number = 1;
		private var deviceMotion:MotionControls = new MotionControls;
		//	private var introWorker:Worker;
		
		//ui
		private var vrTargetLeft:Shape = new Shape();
		private var vrTargetRight:Shape = new Shape();
		private var closeVRButton:VRCloseButtonFl = new VRCloseButtonFl;
		private var guidesHolder:Sprite = new Sprite();
		private var networkManager:ConnectManager = new ConnectManager;
		private var loadingText:LoadingTextFl = new LoadingTextFl;
		private var loadingTextRight:LoadingTextFl = new LoadingTextFl;
		
		
		//shake vars
		private var acc:Accelerometer = new Accelerometer();	
		private var lastShake:Number = 0;
		private var shakeWait:Number = 100;
		private var waitingForChange:Boolean = true;
		
		//home vars
		private var delay:Number = 0;
		private var wait:Number = 3000;
		private var homeLeft:HomeIconFl = new HomeIconFl;
		private var homeRight:HomeIconFl = new HomeIconFl;
		private var directionLeft:directionFl = new directionFl;
		private var directionRight:directionFl = new directionFl;
		private var checkForMotionInt:int;
		
		//flags
		private var initializingVR:Boolean = false;
		private var initialized:Boolean;
		public var introComplete:Boolean = false;
		private var wasRemote:Boolean = false;
		private var receiverIniatedSceneChange:Boolean = false;
		
		//scanner
		private const SRC_SIZE:int = 320;
		private const STAGE_SIZE:int = 350;
		private var startView:Sprite;
		private var cameraView:Sprite;    
		private var camera:Camera;
		private var video:Video = new Video(SRC_SIZE, SRC_SIZE);
		private var freezeImage:Bitmap;
		private var blue:Sprite = new Sprite();
		private var red:Sprite = new Sprite();
		//	private var s:Scanner;
		private var lastFocus:Number = 0;
		
		//broadcast
		private var dragging:Boolean = false;
		private var _moving:Boolean = false;
		private var dragObject:Object = new Object();
		private var sendQuat:Quaternion;
		private var sendQuatObj:Object = new Object;
		private var controlInt:int;
		
		//vr
		private var vrReset:Boolean = false;
		private var tolerance:Number = 0.25;
		
		//vr
		private var stage3DManager:Stage3DManager;
		private var stage3DProxy:Stage3DProxy;
		private var antiAlias:Number = 2;
		
		private var _starling:Starling;
		
		private var _panoReady:Boolean;
		
		private var _loading:Boolean;

		
		
		public function swf_mobile()
		{
			super();
			Log.record("swf_player");
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init); 
			
			
		}	
		
		private function init(e:Event = null):void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.autoOrients = false;
			stage.setAspectRatio(StageAspectRatio.LANDSCAPE);
			
			
			loaderInfo.uncaughtErrorEvents.addEventListener("uncaughtError", onGlobalErrors);
			
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			
			Fonts.titleFont = new TitleFontFl().fontName;
			Fonts.bodyFont = new BodyFontFl().fontName;
			Fonts.boldFont = new BoldFontFl().fontName;
			
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0].toString();
			var appId:String = appXml.ns::id[0].toString();
			appId = appId.substring(appId.indexOf("g") + 1,appId.length);
			Model.appName = appXml.ns::name[0].toString();
			
			Model.config = new MobileConfig({
				"domain":"beek.co",
				"assetCdn":"http://cdn.beek.co",
				"serviceUrl":"http://gms.beek.co",
				"guideId" : "280"//appId
			});
 	
			addEventListener(Event.ACTIVATE,onActivate);
			addEventListener(Event.DEACTIVATE,onDeactivate);
			stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
			stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
			
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.GUIDE_INITIALISED,onGuideInitialised);
			Model.addEventListener(Model.BOOKMARK_SCENE,onBookmarkScene);
			Model.addEventListener(Model.INTRO_COMPLETE,onIntroComplete);
			Model.addEventListener(Model.NETWORK_DATA,onNetworkData);
			Model.addEventListener(Model.GUIDE_CHANGE, onGuideChange);
			Model.addEventListener(Model.GAME_COMPLETE,onGameComplete);
			Model.state.addEventListener(State.GUIDE_ID_CHANGE, onGuideIdChange);
			Model.state.addEventListener(State.GUIDE_VIEW_CHANGE, onGuideViewChange);
			Model.state.addEventListener(State.LOADING_SCENE_CHANGE, onLoadingSceneChange);
			//Model.state.addEventListener(State.PANO_ENGAGED, onPanoTilesLoaded);
			Model.state.addEventListener(State.MODE_CHANGE, onModeChange);
			Model.state.addEventListener(State.CONNECT_NETWORK_CHANGE, onConnectNetworkChange);
			Model.state.addEventListener(State.NETWORK_CONNECT, onNetworkConnect);
			
			
			//pano.addEventListener(Pano.PAUSE_AUTOROTATE, onPanoPauseAutorotate);
			serviceChecker.addEventListener(ServiceChecker.CHECKED, onServiceChecked);


	
			if(!MobileConfig.localStorage.exists)
				MobileConfig.localStorage.createDirectory();
			
			Model.state.startState = "Loading, please wait";
			
			onAddedToStage();
			
		}
		
		
		private function onAddedToStage():void	
		{
			Log.record("swf_player.onAddedToStage()");
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			
			
			if (stage.autoOrients) {
				stage.removeEventListener(StageOrientationEvent.ORIENTATION_CHANGING, orientationChanging);
				stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, orientationChanging, false, 100, true);
			}
			
			if(Stage.supportsOrientationChange) {
				stage.setAspectRatio(StageAspectRatio.LANDSCAPE);
			}
			
			deviceMotion.DeviceMotionConnect();
			
			if(deviceMotion.supported){
				Model.state.mode = State.MODE_GYRO;
				deviceMotion.addEventListener(deviceMotion.NEW_QUATERNION,onQuaternionUpdate);
			}
			
			
			//	stage.addEventListener(Event.RESIZE, onStageResize);
			resize();
			
			
			initializeStage3d();
			
			Starling.multitouchEnabled = true;
		}
		
		
		
		//please stop auto orientating
		private function orientationChanging(event:StageOrientationEvent):void {
			event.stopImmediatePropagation();
			if (event.afterOrientation == StageOrientation.DEFAULT || event.afterOrientation == StageOrientation.UPSIDE_DOWN) {
				event.preventDefault(); 
			}   
		} 
		
		private function onGuideInitialised(e:Event):void
		{
			//if(this._starling)
			//this._starling.stop(true);
			createPano();
			
			//_loading = true;
			
		}
		
		private function createPano(e:Event = null):void
		{
			Log.record("swf_mobile.createPano()");
			
			if(pano)
				return;
			
			pano = new Pano;
			
			
			addChildAt(pano,0);
			
			pano.addEventListener(HotspotEvent.HOTSPOT_SHOW_WEB, onHotspotShowWeb);
			pano.addEventListener(HotspotEvent.HOTSPOT_CLOSE_WEB, onHotspotCloseWeb);
			pano.addEventListener(Pano.PANO_TILES_LOADED, onPanoTilesLoaded);
			pano.addEventListener(Pano.PANO_DEEP_TILES_LOADED, onPanoDeepTilesLoaded);
			pano.addEventListener(Pano.START_DRAG, onStartDrag);
			pano.addEventListener(Pano.STOP_DRAG, onStopDrag);
			pano.addEventListener(Pano.START_AUTOROTATE, onPanoStartAutorotate);
			pano.addEventListener(Pano.STOP_AUTOROTATE, onPanoStopAutorotate);
			
			//pano.mouseEnabled = false;
			
			pano.initializePano(stage3DProxy, stage.stageWidth, stage.stageHeight);
		}
		

		private function initializeStage3d():void
		{
			
			
			Log.record("Pano.initializeStage3d()");
			// Define a new Stage3DManager for the Stage3D objects
			stage3DManager = Stage3DManager.getInstance(stage);
			
			// Create a new Stage3D proxy to contain the separate views
			stage3DProxy = stage3DManager.getFreeStage3DProxy(false, "baselineExtended");
			
			stage3DProxy.antiAlias = antiAlias;	
			stage3DProxy.width = stage.stageWidth;
			stage3DProxy.height = stage.stageHeight;
			stage3DProxy.viewPort.width = stage.stageWidth;
			stage3DProxy.viewPort.height = stage.stageHeight;
			stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onContextCreated);
			
			initialized = true;
		}
		
		private function onContextCreated(e:Stage3DEvent):void
		{

			stage3DProxy.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(MouseEvent.MOUSE_DOWN,hitTest);
			stage.addEventListener(MouseEvent.MOUSE_UP,hitTest);

			createPano();
			createGuide();
			
		}
		
		private function hitTest(e:MouseEvent):void
		{
			var test:Boolean = false;
			var xLeft:Number = 50;
			var xRight:Number = 50;
			var yBottom:Number = 50;
			
			switch(Model.state.guideView){
				case State.GUIDE_VIEW_SECTION:
					xLeft = 450;
					break;
				case State.GUIDE_VIEW_SCENE:
					xRight = 320;
					break;
				case State.GUIDE_VIEW_SCENE_AND_SECTION:
					xLeft = 450;
					xRight = 450;
					break;	
				case State.GUIDE_VIEW_SETTINGS:
					yBottom = 100;
					break;
			}

			if(!_starling)
				test = true;
			else if(Model.lockUI)
				test = false;
			else{
				
				var object:DisplayObject = _starling.root.hitTest(new Point(stage.mouseX, stage.mouseY));
				
				if(object == null) 
					test = true;
				else if(object.alpha == 0 && stage.mouseX > xLeft && stage.mouseX < stage.width -xRight && stage.mouseY < stage.height - yBottom) 
					test = true;
			}
			
			if(test)
				switch (e.type){
					case "mouseUp":
						if(pano.mouseDragPano)
							pano.onMouseUp(e);
						break;
					case "mouseDown":
						pano.onMouseDown(e);
						break;
				}
		}
		
		private function createGuide():void
		{
			
			_starling= new Starling(StarlingGuide, this.stage, stage3DProxy.viewPort, stage3DProxy.stage3D,"auto","baselineExtended");
			_starling.supportHighResolutions = true;
			_starling.shareContext = true;
			_starling.start();
			
		}
		
		
		private function onEnterFrame(e:Event):void
		{
			if(pano)
				this.pano.onEnterFrame(e);
			if(_starling)
				this._starling.nextFrame();
		}
		
		
		private var browserInvokeGuideId:String;
		
		private function onInvoke(event:InvokeEvent):void
		{
			Log.record("swf_player.onInvoke()");
			if(event.arguments == null || event.arguments.length == 0)
				return;
			
			var path:String = String(event.arguments[0]);
			if(path == null || path.lastIndexOf("/") == -1)
				return;
			
			var guideId:String = path.substr(path.lastIndexOf("/")+1);
			if(guideId == null || guideId.length < 1)
				return;
			
			Log.record("browserInvokeGuideId = "+guideId);
			browserInvokeGuideId = guideId;
			
		}
		

		
		private function onActivate(e:Event):void {
			Log.record("swf_mobile.onActivate()");
			Model.state.mute = false;
			//resize();
			
			//if(Constants.isAndroid(Model.config.device))
			//	FlickbackConnect.showFlickScreen();
			
			
			
			if ( stage ) stage.quality = stage.quality;
		}
		
		private function onDeactivate(e:Event):void {
			Log.record("swf_mobile.onDeactivate()");
			Model.state.mute = true;
		}
		
		
		private function onOtherGuidesButtonMouseUp(e:MouseEvent):void{
			Log.record("swf_mobile.onOtherGuidesButtonMouseUp()");

			hideUI();
			closeButton();
			
			
		}
		
		
		
		private function closeButton(value:Boolean = false):void
		{
			if(!value){
				closeVRButton.visible = true;
				
			}
			else
				closeVRButton.visible = false;
		}
		
		
		/*		private function buildStartView():Sprite {
		var sprite:Sprite = new Sprite();
		sprite.graphics.lineStyle(2);
		sprite.graphics.drawRoundRect(0, 0, 200, 30, 5);
		
		var btnText:TextField = new TextField();
		btnText.autoSize = TextFieldAutoSize.LEFT;
		btnText.text = "SCAN";
		btnText.setTextFormat(new TextFormat(null, 16, null, true));
		btnText.selectable = false;
		
		btnText.x = 0.5 * (sprite.width - btnText.width);
		btnText.y = 0.5 * (sprite.height - btnText.height);
		
		sprite.addChild(btnText);      
		sprite.mouseChildren = false;
		sprite.buttonMode = true;
		
		sprite.x = 0;
		sprite.y = 0;
		
		return sprite;
		}
		
		private function start(e:Event):void {
		this.stage.removeEventListener(Event.ENTER_FRAME,start);
		startView = buildStartView();
		this.addChild( startView );
		startView.addEventListener(MouseEvent.CLICK, scan);
		
		
		}
		
		private function scan(e:Event):void
		{
		s = new Scanner();
		s.setTargetArea(100,"0xFF00FF00","0xFFFFFFFF");
		s.setConfig(Symbology.EAN13,Config.ENABLE,1);
		s.addEventListener(ScannerEvent.SCAN,onScan);
		s.launch(true,"rear");
		}
		
		
		
		private function onScan(e:ScannerEvent):void
		{
		for(var i:uint = 0; i < Model.guideData.guideScenes.length; i++)		
		if(Model.guideData.guideScenes[i].scene.id == e.data)
		Model.state.loadingScene = Model.guideData.guideScenes[i].scene
		vibrate(200);
		}*/
		
		private function onStageResize(event:Event):void
		{
			resize();
			
		}	
		
		
		private function resize():void
		{
			Log.record("swf_mobile.onStageResize(width: "+stage.fullScreenWidth+", height: "+stage.fullScreenHeight+")")
			//if(stageWidth == stage.fullScreenWidth && stageHeight == stage.fullScreenHeight)
			//	return;
			
			stageWidth = stage.fullScreenWidth;
			stageHeight = stage.fullScreenHeight;	
			
			if(stageWidth < stageHeight){
				stageHeight = stage.fullScreenWidth;
				stageWidth = stage.fullScreenHeight;	
			}
			
			if(pano)
				pano.resize(stageWidth, stageHeight);


			// Cannot init ui before its been resized for the first time
			if(!checkServiceRun){
				checkService();
				
			}
			initialized = true;
			
		}
		
		private function onBrowserInvoke(event:BrowserInvokeEvent):void
		{
			Log.record("swf_player.onBrowserInvoke()");
		}
		
		
		//Checking for network connectivity
		private function onNetworkChange(e:Event):void
		{
			Log.record("swf_mobile.onNetworkChange()");
			checkService();
			
		}
		
		
		private var checkServiceRun:Boolean;
		
		private function checkService():void
		{
			Log.record("swf_mobile.checkService()");
			checkServiceRun = true;
			serviceChecker.start();
		}
		
		private function onServiceChecked(event:Event):void 
		{
			Log.record("swf_mobile.onServiceCheckerSuccess();");
			Model.state.networkAccess = serviceChecker.available;
			
			if(!Model.state.networkAccess)
				Model.state.startState = "No Internet Connection";
			
			if(!beekDB.connected)
				connectDb();
		}
		
		private function connectDb():void
		{
			Log.record("swf_mobile.connectDb();");
			beekDB.addEventListener(SQLEvent.OPEN, onDBConnected);
			beekDB.addEventListener(SQLErrorEvent.ERROR, onBeekDBError);
			beekDB.open(MobileConfig.localStorage.resolvePath("beek.db"));
		}
		
		private function onBeekDBError(event:SQLErrorEvent):void
		{
			throw new Error("onBeekDBError() " +event.errorID+" : "+event.text+" : "+event.error.details);
		}
		
		private function onDBConnected(event:SQLEvent):void
		{
			Log.record("swf_mobile.onDBConnected()");
			var createTableStmt:SQLStatement = new SQLStatement();
			createTableStmt.sqlConnection = beekDB;
			createTableStmt.text = "CREATE TABLE IF NOT EXISTS `localguides` (" +
				"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`guide_id` INTEGER, " +
				"`json` TEXT" +
				")";
			createTableStmt.execute();			

			OfflineManager.connect(beekDB);
			
			initialisationComplete();
			
			
		}
		
		private function initialisationComplete():void
		{
			Log.record("swf_mobile.initComplete()");
			
			
			
			//if main or other guide downloaded load this offline
			if(OfflineManager.checkForOfflineLoad(Model.config.guideId)){
				loadGuideOffline(OfflineManager.checkForOfflineLoad(Model.config.guideId));
				Model.state.startState = "Loading from device (" +OfflineManager.offlineGuides.length+" downloaded)";
				return;
			}
			
			if(serviceChecker.available){
				loadGuide();
				Model.state.startState = "Connected - Loading from Internet";
				return;
			}
			
			Model.state.startState = "Waiting for network connection";
			var waitForConnection:int = setInterval(
				function():void{
					serviceChecker.start();
					serviceChecker.addEventListener(ServiceChecker.CHECKED,function():void{
						if(serviceChecker.available){
							loadGuide();
							Model.state.startState = "Connected - Loading from Internet";
							clearInterval(waitForConnection);
						}
						
					})},2000);
			
		}
		
		
		private function loadGuide():void
		{
			Log.record('swf_mobile.loadGuide()');
			GuideService.getGuideBasicJson(Model.config.guideId,loadGuideOffline)		
		}
		
		private function loadGuideOffline(data:GuideBasicData):void
		{
			Log.record('swf_mobile.loadGuideOffline()');
			Model.state.guideId = data.id;
			
			
		}
		
		
		
		private function loadSceneData(sceneId:String):void
		{
			Log.record("swf_mobile.loadSceneData()")
			var loader:ServiceLoader = new ServiceLoader(SceneData);
			var url:String = Model.config.getSceneJsonUrl(sceneId);
			loader.load(url, onSceneDataLoaded);
			
			//if(this._starling)
			//this._starling.stop(true);
		}
		
		private function onSceneDataLoaded(scene:SceneData):void
		{
			Log.record("swf_mobile.onSceneDataLoaded()")
			Model.currentScene = scene;
			
			var loadingScene:SceneElementData = Model.state.loadingScene;
			var guideScene:GuideSceneData = Model.guideData
				? Model.guideData.getGuideSceneFromId(loadingScene.id)
				: null;
			
			_loading = true;
			
			//if(_starling)
			//	_starling.stop(true);
			//black.tween(0.5);
			

			
/*			if(Model.state.connectMode != State.CONNECT_MODE_RECEIVE
				&& Model.state.guideView == State.GUIDE_VIEW_CLOSED)
			updateLoadingText();*/
			
		}
		
		
		private function updateLoadingText():void
		{
			Log.record("swf_mobile.updateLoadingText()")
			
			
			
			var height:Number = stage.stageHeight < stage.stageWidth ? stage.stageHeight :  stage.stageWidth;
			var desc:String = Model.guideScene.descriptionMerged || " ";
			desc = desc.replace(/COLOR\=\"([^"]+)"/gi, 'COLOR="0xffffff"');
			
			if(introComplete)
				loadingText.visible = true;
			loadingText.body.autoSize = loadingTextRight.body.autoSize = "center";
			loadingText.title.autoSize = loadingTextRight.title.autoSize = "left";
			loadingText.body.wordWrap = loadingTextRight.body.wordWrap = true;
			Fonts.format(loadingText.title, Fonts.titleFont, Model.guideScene.titleMerged || null);
			Fonts.formatHTML(loadingText.body,Fonts.bodyFont, desc, false);
			
			loadingText.bg.height = 0;
			
			loadingText.body.width = loadingText.bg.width = stage.stageWidth * 0.6;
			loadingText.title.width = loadingText.title.textWidth;
			loadingText.body.height = loadingText.body.textHeight;
			loadingText.bg.height = loadingText.body.x + loadingText.body.height + loadingText.title.height + 40;
			loadingText.bg.width = loadingText.body.width + 40;
			loadingText.bg.visible = false;
			
			if(!Model.state.vr){
				loadingText.x = stage.stageWidth/2 - loadingText.width/2;
				loadingText.y =  height/2 - loadingText.height/2;
			}
			else
			{
				loadingTextRight.visible = true;
				loadingTextRight.bg.visible = false;
				Fonts.format(loadingTextRight.title, Fonts.titleFont, Model.guideScene.titleMerged);
				Fonts.formatHTML(loadingTextRight.body,Fonts.bodyFont, desc, false );
				
				
				loadingText.body.width = loadingText.bg.width = loadingTextRight.bg.width = 
					loadingTextRight.body.width = stage.stageWidth * 0.4;
				
				
				loadingTextRight.title.width = loadingTextRight.title.textWidth;
				loadingTextRight.body.height = loadingText.body.textHeight;
				loadingText.bg.height = loadingTextRight.bg.height = loadingText.body.textHeight + loadingText.title.height + 40;
				loadingText.bg.width = loadingTextRight.bg.width = loadingText.body.width + 40;
				loadingText.x = stage.stageWidth * 0.25 - loadingText.width/2;
				loadingTextRight.x = stage.stageWidth * 0.75 - loadingText.width/2;
				
				loadingText.y = loadingTextRight.y = height/2 - loadingText.height/2;
			}
			
		}
		
		
		
		
		/**
		 * A pano scene has completed loading
		 */
		private function onPanoTilesLoaded(event:Event):void 
		{
			Log.record("swf_mobile.onPanoTilesLoaded()");
			_panoReady = true;
			_loading = false;
			
			// perform garbage collection in AIR hopefully.
			flash.system.System.gc();
			flash.system.System.gc();
			
			setTimeout(function():void{waitingForChange = true;}, 2000);
			
			if(Model.state.connectMode != State.CONNECT_MODE_RECEIVE)
				acc.addEventListener(AccelerometerEvent.UPDATE, onAccUpdate);
			
			//reset recever flag
			receiverIniatedSceneChange = false;
			
		//	this._starling.start();

		}
		
		private function onPanoDeepTilesLoaded(event:Event):void 
		{

			
			if(loadingText.visible){
				loadingText.body.backgroundColor = 0x88000000;
				loadingTextRight.body.backgroundColor = 0x88000000;
				loadingText.bg.visible = true;
				loadingTextRight.bg.visible = true;
			}
			
			if(initializingVR)
				initializingVR = false;

		}
		
		private function showLog():void
		{
			var d:ErrorDialogFl = new ErrorDialogFl;
			d.textField.text = Log.actions.join('\n');
			d.removeButton.scaleX = d.removeButton.scaleY = 10;
			d.removeButton.addEventListener(MouseEvent.CLICK,onCloseButtonClick);
			d.textField.height = stageHeight * 0.8;
			d.x = (stageWidth - d.width)/2;
			d.y = (stageHeight - d.height)/2;
			addChild(d);
		}
		
		private function onStartDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStartDrag()");

			
			Model.state.dispatchEvent(new Event(State.PANO_ENGAGED));
			Model.state.mode = State.MODE_DRAG;
			dragging = true;
			
			if(!Model.state.active)
				Model.state.active = true;
			
			//NEED TO THINK ABOUT THIS - for giving controller power
/*			if(Model.isMobile && Model.state.connectMode == State.CONNECT_MODE_RECEIVE && Model.state.connected
				&& controlInt == 0)
				controlInt = setInterval(sendDragData, 20);*/
		}
		
		private function onStopDrag(event:Event):void 
		{
			Log.record("swf_mobile.onStopDrag()");
			dragging = false;
			
		//	if(!Model.state.vr)
			//	setTimeout(function():void{if(!dragging)showUI();},1500);
			
		}
		
		private function onGlobalErrors(event:Event):void
		{
			
			var location:String = new Error().getStackTrace().match( /(?<=\/|\\)\w+?.as:\d+?(?=])/g )[1].replace( ":" , ", line " );
			trace( location );
			
			var message:String = "";
			if (event['error'] is Error){
				message = Error(event['error']).message;
				message +=  Error(event['error']).getStackTrace(); 
			}
				
			else if (event['error'] is ErrorEvent)
				message = ErrorEvent(event['error']).text;
				
				
			else if(event['error'])
				message = event['error'].toString();
			
			message += "\n\n" + Log.actions.join("\n");
			
			Log.record(message);
			
			var d:ErrorDialogFl = new ErrorDialogFl;
			d.textField.text = message;
			d.removeButton.scaleX = d.removeButton.scaleY = 10;
			d.removeButton.addEventListener(MouseEvent.CLICK,onCloseButtonClick);
			d.textField.height = stageHeight * 0.8;
			d.x = (stageWidth - d.width)/2;
			d.y = (stageHeight - d.height)/2;
			addChild(d);
		}
		
		private function onCloseButtonClick(event:MouseEvent):void
		{
			removeChild(event.target.parent)
		}
		
		private function onGuideIdChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideIdChange")
			loadGuideData(Model.state.guideId);
		}
		
		private function onGuideViewChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideIdChange")
			if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_RECEIVE)
				if(Model.state.guideView == State.GUIDE_VIEW_CLOSED)
					MotionDataManager.stop();
				else
					MotionDataManager.start();
			
			
	/*		if(Model.state.guideView == State.GUIDE_VIEW_CLOSED || Model.state.guideView == State.GUIDE_VIEW_TASK)
				black.tween(0)
			else{
				black.tween(0.8);
				setTimeout(function(){
					black.addEventListener(MouseEvent.MOUSE_UP, onBlackMouseUp);
				},1000);
			}*/
			
		}
		
		private function loadGuideData(guideId:String):void
		{
			Log.record("swf_mobile.loadGuideData(" + guideId + ")");
			var loader:ServiceLoader = new ServiceLoader(GuideData);
			var url:String = Model.config.getGuideJsonUrl(guideId);
			loader.load(url,onGuideDataLoaded);
		}
		
		private function onGuideDataLoaded(guideData:GuideData):void
		{
			Log.record("swf_mobile.onGuideDataLoaded");
			Model.guideData = guideData;
		//	intro.updateGuideContents();


		}
		
		private function onGuideChange(event:Event):void
		{
			Log.record("swf_mobile.onGuideChange(");
			
			
		}
		

		
		private function onLoadingSceneChange(event:Event):void
		{
			Log.record("swf_mobile.onLoadingSceneChange()");
			loadSceneData(Model.state.loadingScene.id);
			
			acc.addEventListener(AccelerometerEvent.UPDATE, onAccUpdate);
			
			
			if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_RECEIVE){
				var obj:Object = new Object;
				obj.type = "scene";
				obj.id = Model.state.loadingScene.id;
				sendData(obj);
				
				receiverIniatedSceneChange = true;
			}
			
			
			
		}
		
		private function onHotspotClicked(event:HotspotEvent):void
		{
			Log.record("swf_mobile.onHotspotClicked()");
			vibrate(100)
		}
		
		private function onGyroActive(event:Event):void
		{
			Log.record("swf_mobile.onGyroActive()");
			vibrate(100)
		}
		
		private function vibrate(seconds:int):void
		{
//			var vibe:Vibration;
//			if (Vibration.isSupported)
//			{
//				vibe = new Vibration();
//				vibe.vibrate(seconds);
//			}
			
		}
		
		public static function toDegrees(radians:Number):Number {
			return (radians * 180 / Math.PI);
		}
		
		private function onPanoStartAutorotate(event:Event):void
		{
			Log.record("SWF_mobile.onPanoStartAutorotate()");
			
			Model.state.active = true;
			
			//setTimeout(hideInterface,1000);
			
		}
		
		private function onPanoStopAutorotate(event:Event):void
		{
			Log.record("SWF_mobile.onPanoStopAutorotate()");
			
				if(!Model.isPlayingGame)
				setTimeout(showUI,500);
		}
		
		private function onIntroComplete(event:Event):void
		{
			Log.record("SWF_mobile.onIntroComplete()");
			
			resize();
			showUI();
			introComplete = true;
			Model.state.dispatchEvent(new Event(State.INTRO_COMPLETE));
			
		}
		
		private function showUI(e:Event = null):void
		{
			Log.record("SWF_mobile.showUI()");
			
			

			//loadingText.visible = true;
		}
		
		private function hideUI():void
		{
			Log.record("hideInterface()");
	

			loadingText.visible = false;
			
			
		}
		
		private var videoBackground:Sprite = new Sprite();
		
		protected function onCurrentVideoChanged(event:Event):void
		{	
			Log.record("onCurrentVideoChanged()");
			
			/*			if (AirVideo.getInstance().currentVideo == null || AirVideo.getInstance().queue == null)
			{
			showUI();
			AirVideo.getInstance().hidePlayer();
			removeChild(videoBackground);
			}
			else
			{
			hideUI();
			videoBackground.graphics.beginFill(0x000000, 1);
			videoBackground.graphics.drawRect(0,0,width,height);
			videoBackground.graphics.endFill();
			addChild(videoBackground);
			}*/
			
		}
		
		private function onHotspotShowWeb(event:HotspotEvent):void
		{
			showWebView(event.hotspot);
		}
		
		private var webView:StageWebView = new StageWebView(); 
		
		private function showWebView(data:HotspotData):void
		{
			Log.record("swf_mobile.showWebView()");
			var photoData:PhotoData = data as PhotoData;
			
			webView.viewPort = new Rectangle( stageWidth/2 - data.inWorldWidth/2, stageHeight/2 - data.inWorldHeight/2, data.inWorldWidth, data.inWorldHeight); 
			webView.loadURL(photoData.webUrl);
			webView.addEventListener(Event.COMPLETE, addStageWebViewtoStage);
		}
		
		private function addStageWebViewtoStage(event:Event):void
		{
			webView.stage = this.stage; 
		}
		
		private function onHotspotCloseWeb(event:HotspotEvent):void
		{
			webView.stage = null;
		}
		
		private function onGuideOpen(e:Event):void
		{
			hideUI();

		}
		
		private function onGuideClose(e:Event):void
		{
			showUI();
		}
		
		private function onQuaternionUpdate(e:Event):void
		{
			if(Model.state.mode == State.MODE_GYRO && pano 
			&& Model.state.connectMode != State.CONNECT_MODE_RECEIVE)
				pano.onGyroChangeQuaternion(deviceMotion.quaternion);
		}
		
		
		private function onAccUpdate(e:AccelerometerEvent):void
		{	

			
			if(getTimer() - lastShake > shakeWait &&  Model.state.mode != State.MODE_GYRO &&
				(e.accelerationX >= 1.2 || e.accelerationY >= 1.2 || e.accelerationZ >= 1.2))
			{
					shakeIt();
				
				lastShake = getTimer();
			}

			//	moving = e.accelerationY >= 0.9;
			
		}
/*		
		private function set moving(value:Boolean):void{
			
			if(_moving == value)
				return;
			else
				_moving = value;
			
			if(_moving)
				Model.state.dispatchEvent(new Event(State.PANO_MOVE));
			else
				Model.state.dispatchEvent(new Event(State.PANO_STOP));
			
		}*/
		
		
		private function checkForMotionControls():void
		{
			
			if(!waitingForChange)
				return;
			
			if(pano.camera.leftVector.y < -tolerance)
				vrAction('back');
			else if(pano.camera.leftVector.y > tolerance)
				vrAction('next');
			else if(Math.abs(pano.camera.forwardVector.x) <= tolerance) {
				if(Math.abs(pano.camera.forwardVector.z) <= tolerance)  
					if(Math.abs(-pano.camera.forwardVector.y-1) <= tolerance)
						vrAction('home');
				
			}
			else{
				hideVRActionUI();
				delay = 0;
				vrReset = true;
			}
			
		}
		
		
		private function vrAction(icon:String):void
		{
			trace("vrAction("+icon+")");
			vrTargetLeft.visible = false;
			vrTargetRight.visible = false;
			
			//switch icons on and off
			if(icon == 'home' && !Model.history.isHome()){
				homeLeft.visible = true;
				homeRight.visible = true;
			}
				
			else if(icon == 'next' || icon == 'back'){
				directionLeft.visible = true;
				directionRight.visible = true;
				
				if(icon == 'back')
					directionLeft.rotation = directionRight.rotation = 220;
				else
					directionLeft.rotation = directionRight.rotation = -40;
			}
			
			if(delay > 0){
				
				if(getTimer() - delay > wait && vrReset){
					
					waitingForChange = false;
					switch(icon)
					{
						case 'home':{
							if(!Model.history.isHome()) Model.history.loadHome();
							break;
						}
						case 'next':{
							Model.nextScene();
							break;
						}
						case 'back':{
							Model.back();
							break;
						}
					}
					vrReset = false;
					hideVRActionUI();
					vibrate(100);
				}
			}
			else
				delay = getTimer();
		}
		
		private function hideVRActionUI():void{
			Log.record('swf_mobile.hideVRActionUI()');
			homeLeft.visible = false;
			homeRight.visible = false;
			directionLeft.visible = false;
			directionRight.visible = false;
			
			vrTargetLeft.visible = true;
			vrTargetRight.visible = true;
		}
		
		private function shakeIt():void
		{
			trace("shakeIt()");
			//vibrate(100);
			
			Model.state.mode = State.MODE_GYRO;
			
			if(pano)
				pano.onShake();

			//in VR mode bookmark scene on shake
			//if(Model.state.vr)
			//	bookmarks.bookmarkScene();
			
		}
		
		private function sendDragData():void{
			
			dragObject.type = "drag";
			dragObject.x = pano.camera.rotationX;
			dragObject.y = pano.camera.rotationY;
			dragObject.z = pano.camera.rotationZ;
			dragObject.zoom = pano.zoom;
			dragObject.sceneId = Model.currentScene.id || Model.guideData.firstScene.id;
			sendData(dragObject);
		}
		
		
		private function sendGyroData():void
		{
			sendQuat = pano.currentQuaternion;
			sendQuatObj.type = "gyro";
			sendQuatObj.w = sendQuat.w;
			sendQuatObj.x = sendQuat.x;
			sendQuatObj.y = sendQuat.y;
			sendQuatObj.z = sendQuat.z;
			sendQuatObj.sceneId = Model.currentScene.id || Model.guideData.firstScene.id;
			
			sendData(sendQuatObj);
		}
		
		private function sendData(obj):void
		{
			
			if(!Model.state.connected)
				return;
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_WIFI)
				WifiManager.sendData(obj)
					
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_INTERNET)
				WebSocketManager.sendData(obj)
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_BLUETOOTH)
				if(Constants.isIOS())
					BluetoothLEManager.sendData(obj);
				else
					BluetoothManager.sendData(obj);
				
		}
		
		
		private function onNetworkConnect(e:Event):void
		{
			Log.record("swf_mobile.onNetworkConnect()");
			onModeChange();
			
		}
		
		private function onBookmarkScene(e:Event):void
		{
			Log.record("swf_mobile.onBookmarkScene()");
			
			if(Model.state.connected)
			{
				var obj:Object = new Object;
				obj.type = "bookmark";
				obj.sceneId = Model.currentScene.id || Model.guideData.firstScene.id;
				sendData(obj);
			}
		}
		
		private function onNetworkData(event:NetworkDataEvent):void
		{
			var obj:Object = event.object;
			
			if(!obj)
				return;
			
			if(!obj.hasOwnProperty("type"))
				return;
			
			if(obj.sceneId && Model.state.mode == State.MODE_REMOTE && !receiverIniatedSceneChange)
				if(obj.sceneId != Model.currentScene.id)
					Model.loadGuideScene(obj.sceneId);
			
		//	if(obj.type == "bookmark")
			//	bookmarks.bookmarkScene();
			if(obj.type == "gyro" || obj.type == "drag" )
				pano.remoteUpdate(obj);
		}
		
		private function onModeChange(e:Event = null):void
		{
			Log.record("swf_mobile.onModeChange("+ Model.state.mode+")" );
			clearInterval(controlInt);	
			
			switch(Model.state.mode){
				
				case State.MODE_AUTO:{
					
					hideUI();
					pano.addEventListener(Pano.START_DRAG,showUI);
					break;
				}
					
				case State.MODE_GYRO:{
					if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_BROADCAST){
						controlInt = setInterval(sendGyroData, 20);
					}
					break;
				}
					
				case State.MODE_DRAG:{
					
					if(Model.state.connected && Model.state.connectMode == State.CONNECT_MODE_BROADCAST)
						controlInt = setInterval(sendDragData, 20);
					break;
				}
					
					
			}
			
		}
		
		private function onConnectNetworkChange(e:Event):void
		{
			Log.record("swf_mobile.onConnectNetworkChange()");
			
			if(Model.state.connectNetwork == State.CONNECT_NETWORK_BLUETOOTH){
				if(Constants.isIOS())
					BluetoothLEManager.scan();
				else
					BluetoothManager.scan();
			}
			else
				if(Constants.isIOS())
					BluetoothLEManager.close();
				else
					BluetoothManager.close();
		}
		
		private function onVR(e:Event):void
		{
			Log.record("swf_mobile.onVR()");
			// try and get the black to appear above the 
			//black.resize(stage.width, stage.height);
			initializingVR = true;
			
			
		}
		
		private function onGameComplete(e:Event):void
		{
			showUI();
		}
		
		
		
		private function stage_resizeHandler(event:Event):void
		{
			

			stage3DProxy.width = stage.stageWidth;
			stage3DProxy.height = stage.stageHeight;
			stage3DProxy.viewPort.width = stage.stageWidth;
			stage3DProxy.viewPort.height = stage.stageHeight;
			
			if(this._starling){
				this._starling.stage.stageWidth = this.stage.stageWidth;
				this._starling.stage.stageHeight = this.stage.stageHeight;
			
			var viewPort:Rectangle = this._starling.viewPort;
				viewPort.width = this.stage.stageWidth;
				viewPort.height = this.stage.stageHeight;
			try
			{
			//	this._starling.viewPort = viewPort;
			}
			catch(error:Error) {}
			
			}
			
			resize();
			

		}
		
		private function stage_deactivateHandler(event:Event):void
		{
			if(this._starling)
				this._starling.stop(true);
			this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:Event):void
		{
			this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
			if(this._starling)
				this._starling.start();
		}
		
		
		
		
	}
}