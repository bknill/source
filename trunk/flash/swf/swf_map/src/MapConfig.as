package
{
	import co.beek.map.GeoZoom;

	public class MapConfig
	{
		private var parameters:Object;
		public function MapConfig(params:Object)
		{
			this.parameters = params;
		}
		
		public function get geoZoom():GeoZoom
		{
			var geoParam:String = parameters["geo"];
			if(geoParam == null)
				geoParam = "-41.3077,174.8808,14";
			
			var geo:Array = geoParam.split(",");
			return new GeoZoom(geo[0], geo[1], geo[2]);
		}
		
		public function get destinationId():String
		{
			return parameters["destinationId"];
		}
	}
}