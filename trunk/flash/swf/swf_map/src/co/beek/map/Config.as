package co.beek.map
{
	import co.beek.model.Constants;
	import co.beek.model.IConfig;
	import co.beek.model.data.SceneBasicData;
	
	public class Config implements IConfig
	{
		protected var _data:Object;
		
		public function Config(data:Object)
		{
			_data = data;
		}
		
		public function get device():String 
		{ 
			return Constants.WEB 
		}
		
		public function get gaCode():String
		{
			return null;
		}
		
		public function get version():String
		{
			return null;
		}
		
		public function get assetCdn():String
		{
			return null;
		}
		
		public function get adminSwf():String
		{
			return null;
		}
		
		public function get domain():String
		{	return _data['domain'] }
		
		public function get serviceUrl():String
		{	return "http://gms."+domain }
		
		
		public function get sceneId():String
		{
			return null;
		}
		
		public function get guideId():String
		{
			return null;
		}
		
		public function get tourGuideId():String
		{
			return null;
		}
		
		public function get tourRole():String
		{
			return null;
		}
		
		public function get mapTileServer():String
		{
			return null;
		}
		
		public function getGuideJsonUrl(guideId:String):String
		{
			return null;
		}
		
		public function getSceneJsonUrl(sceneId:String):String
		{
			return null;
		}
		
		public function getAssetUrl(path:String):String
		{
			return null;
		}
		
		public function getMapTileUrl(path:String):String
		{
			return null;
		}
		
		public function isAvailable(scene:SceneBasicData):Boolean
		{
			return true;
		}
	}
}