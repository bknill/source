#!/bin/bash

JAVA=`which java`

cd /srv/aditya

ulimit -v unlimited
ulimit -n 16384
ulimit -aH > /srv/aditya/logs/limits.log

# JAVA arguments
# JAVA_ARGS=-Xrs -Xms512M -Xmx768M -Xss128K -XX:PermSize=128M -XX:MaxPermSize=256M
$JAVA -jar UploadServer.jar

