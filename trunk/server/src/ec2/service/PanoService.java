package ec2.service;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.ScenePanoTask;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.sceceService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec2.model.Job;
import ec2.model.PanoJob;
import ec2.model.Ping;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
public class PanoService extends EC2Service {
    @Inject private SnsService snsService;
    @Inject private TaskService taskService;
    @Inject private SceneService sceneService;
    @Inject private MailService emailService;

    @Value("#{buildProperties.jobsQueueURL}")
    private String jobsQueueURL;

    @Value("#{buildProperties.tempAssetsBucket}")
    private String tempAssetsBucket;

    private Map<Instance, ScenePanoTask> jobsMap = new HashMap<Instance, ScenePanoTask>();

    @Async
    public void start() {
        if (!snsService.isSubscribe()){
            snsService.subscribe();
            return;
        }


        ScenePanoTask scenePanoTask = taskService.getTaskFromQueue();
        if (scenePanoTask == null) {
            System.out.println("no task returning");
            return;
        }

        if (scenePanoTask.template != null) {
            System.out.println("template task this is for Brett to process");
            return;
        }

        scenePanoTask.status = ScenePanoTask.PROCESSING;
        taskService.updateTask(scenePanoTask);

        Instance instance = createAndRunInstance();
        boolean isEc2Running = waitForInstanceToStart(instance);
        System.out.println("state = " + instance.getState().getName());
        if (isEc2Running) {
            jobsMap.put(instance, scenePanoTask);
        } else {
            scenePanoTask.status = ScenePanoTask.PENDING;
            taskService.updateTask(scenePanoTask);
        }
//        String instanceId = "i-2bd8eaab";
//        DescribeInstancesResult describeInstancesResult =
//                ec2Client.describeInstances(new DescribeInstancesRequest().withInstanceIds(instanceId));
//
//        List<Instance> instances = new ArrayList<Instance>();
//        for (Reservation reservation : describeInstancesResult.getReservations()) {
//            instances.addAll(reservation.getInstances());
//        }
//        if (instances.size() > 0) {
//            Instance instance = instances.get(0);
//            startInstance(instance);
//            boolean isEc2Running = waitForInstanceToStart(instance);
//            System.out.println("state = " + instance.getState().getName());
//            if (isEc2Running) {
//
//                scenePanoTask.status = ScenePanoTask.PROCESSING;
//                taskService.updateTask(scenePanoTask);
//
//                jobsMap.put(instance, scenePanoTask);
//            } else {
//                scenePanoTask = null;
//            }
//        }
        System.out.println("returning");
    }

    @Async
    public void handlePing(Ping ping) {
        System.out.println("got alive response");

        String instanceId = ping.getEC2InstanceId();
        Instance instance = null;
        ScenePanoTask scenePanoTask = null;

        for (Map.Entry<Instance, ScenePanoTask> entries : jobsMap.entrySet()) {
            if (entries.getKey().getInstanceId().equals(instanceId)) {
                instance = entries.getKey();
                scenePanoTask = entries.getValue();
                break;
            }
        }
        if (instance == null)
            return;

        if (scenePanoTask == null) {
            stop(instance);
            return;
        }

        try {
            System.out.println("uploading pano to s3 starts");

            File tempFile = new File(Constants.TEMP_DIR_PATH + scenePanoTask.getTempFileName());

            if (tempFile != null && !tempFile.exists()) {
                stop(instance);
                return;
            }

            pushProcessingStartsMsg(scenePanoTask);

            Scene scene = scenePanoTask.getScene();
            scene.incrementPano();

            s3Client.putObject(new PutObjectRequest(
                    tempAssetsBucket,
                    scene.getPanoName(),
                    tempFile));

            PanoJob panoJob = new PanoJob();
            panoJob.setJobStatus(Job.JobStatus.RAW);
            panoJob.setFileName(scene.getPanoName());
            panoJob.setJobType(Job.JobType.PANO_PROCESSING);

            panoJob.setId(scene.getId());
            panoJob.setIncrement(scene.getPanoIncrement());
            panoJob.setPrefix(scene.getPanoPrefix());
            panoJob.setLongitude(scene.getLongitude());
            panoJob.setLatitude(scene.getLatitude());

            ObjectMapper mapper = new ObjectMapper();
            String jobJSON = mapper.writeValueAsString(panoJob);

            System.out.println("sending job");
            sqsClient.sendMessage(jobsQueueURL, jobJSON);

            tempFile.delete();

        } catch (Exception e) {
            pushProcessingFailedMsg(scenePanoTask);
            taskService.deleteTask(scenePanoTask);
            stop(instance);
            e.printStackTrace();
        }
    }

    @Async
    public void handleJob(Job job) {
        String instanceId = job.getEC2InstanceId();
        System.out.println(job.getMessage());

        Instance instance = null;
        ScenePanoTask scenePanoTask = null;
        for (Map.Entry<Instance, ScenePanoTask> entries : jobsMap.entrySet()) {
            if (entries.getKey().getInstanceId().equals(instanceId)) {
                instance = entries.getKey();
                scenePanoTask = entries.getValue();
                break;
            }
        }
        if (instance == null)
            return;

        if (scenePanoTask == null) {
            stop(instance);
            return;
        }
        try{
            if (job.getJobStatus() == Job.JobStatus.DONE) {
                System.out.println("done");
                sceneService.saveScene(scenePanoTask.getScene());

                pushProcessingFinishedMsg(scenePanoTask);

            } else if (job.getJobStatus() == Job.JobStatus.ERROR) {
                System.out.println("error");
                System.out.println(job.getMessage());

                pushProcessingFailedMsg(scenePanoTask);

                emailService.emailError("Error Processing image", job.getMessage());
            }
        }catch (Exception e){
            pushProcessingFailedMsg(scenePanoTask);
            e.printStackTrace();
        }finally {
            stop(instance);
            taskService.deleteTask(scenePanoTask);
        }
    }

    public void stop(Instance instance) {
        terminateInstance(instance);
//        stopInstance(instance);
        jobsMap.remove(instance);
    }

    public void pushProcessingStartsMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Starts\"}";
        pushContext.publish("/", pushString);
    }
    public void pushProcessingFinishedMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"true\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Finished\"}";
        pushContext.publish("/", pushString);
    }
    public void pushProcessingFailedMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"false\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Failed\"}";
        pushContext.publish("/", pushString);
    }

    public static void main(String[] args) {
        PanoService panoService = new PanoService();
    }
}
