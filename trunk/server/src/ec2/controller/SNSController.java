package ec2.controller;

import com.amazonaws.services.sns.model.ConfirmSubscriptionRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec2.model.Job;
import ec2.model.PanoJob;
import ec2.model.Ping;
import ec2.service.PanoService;
import ec2.service.SnsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@Controller
public class SNSController {
    //private static int counter = 0;

    @Inject private SnsService snsService;
    @Inject private PanoService panoService;
    //@Inject private TranscodingService transcodingService;

    @PostConstruct
    public void init() {
        //snsService.subscribe();
    }

    @ResponseBody
    @RequestMapping("/response")
    public void index(HttpServletRequest request, @RequestBody String payload) {
        String messageType = request.getHeader("x-amz-sns-message-type");
        if (messageType == null)
            return;

        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> messageMap = mapper.readValue(payload, Map.class);

            String type = messageMap.get("Type");
            if (type != null && type.equals("SubscriptionConfirmation")) {
                System.out.println("got subscription msg");

                String token = messageMap.get("Token");
                if (token != null) {
                    ConfirmSubscriptionRequest confirmReq = new ConfirmSubscriptionRequest()
                            .withTopicArn(snsService.getTopicArn())
                            .withToken(token);
                    snsService.getSnsClient().confirmSubscription(confirmReq);
                    snsService.setSubscribe(true);
                }
            } else if (type != null && type.equals("Notification")) {
                System.out.println("got notification msg");

                String message = messageMap.get("Message");
                String subject = messageMap.get("Subject");
                if (subject.equals("ping")) {
                    Ping ping = mapper.readValue(message, Ping.class);
                    if (ping.getJobType() == Job.JobType.PANO_PROCESSING) {
                        panoService.handlePing(ping);
                    } else if (ping.getJobType() == Job.JobType.TRANSCODING) {
                        //transcodingService.handlePing(ping);
                    }
                } else if (subject.equals("job")) {
                    PanoJob job = mapper.readValue(message, PanoJob.class);
                    if (job.getJobType() == Job.JobType.PANO_PROCESSING) {
                        panoService.handleJob(job);
                    } else if (job.getJobType() == Job.JobType.TRANSCODING) {
                        //transcodingService.handleJobResponse(job);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Scheduled(fixedDelay = 10000)
//    private void timer() {
//        if(snsService.isSubscribe()){
//            if(counter == 0){
//                System.out.println("hello " + Thread.currentThread().getName());
//                transcodingService.start();
//            }
//            counter++;
//        }
//    }
}