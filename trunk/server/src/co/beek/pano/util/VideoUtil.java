package co.beek.pano.util;

import co.beek.Constants;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.ChunkOffsetBox;
import com.coremedia.iso.boxes.StaticChunkOffsetBox;
import com.googlecode.mp4parser.util.Path;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.bytedeco.javacv.Frame;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.*;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.bytedeco.javacpp.avutil.AV_LOG_QUIET;
import static org.bytedeco.javacpp.avutil.av_log_set_level;



public class VideoUtil {

    static {
        try {
            System.setProperty("java.library.path", Constants.NATIVE_DIR_PATH);
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File toWebm(File file)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

        //comment it if you want to see logs produced by ffmpeg
        av_log_set_level(AV_LOG_QUIET);

        String fileName = getFileNameWithoutExt(file);
        File webmFile = new File(Constants.TEMP_DIR_PATH + fileName + ".webm");

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getPath());
        grabber.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(webmFile.getPath(),
                grabber.getImageWidth(), grabber.getImageHeight());

        recorder.setFormat("webm");
        recorder.setVideoOption("preset", "ultrafast");

        recorder.setVideoQuality(5);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_VP8);
        recorder.setVideoBitrate(grabber.getVideoBitrate());
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);

        recorder.setAudioCodec(avcodec.AV_CODEC_ID_VORBIS);
        recorder.setAudioBitrate(grabber.getAudioBitrate());
        recorder.setAudioChannels(grabber.getAudioChannels());

        recorder.setSampleRate(grabber.getSampleRate());
        recorder.setFrameRate(grabber.getFrameRate());
        recorder.setSampleFormat(avutil.AV_SAMPLE_FMT_FLTP);

        encode(grabber, recorder);
        return webmFile;
    }

    public static File toOgv(File file)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

    	
    	System.out.println("toOgv " + file.toString());
        //comment it if you want to see logs produced by ffmpeg
        av_log_set_level(AV_LOG_QUIET);

        String fileName = getFileNameWithoutExt(file);
        File ogvFile = new File(Constants.TEMP_DIR_PATH + fileName + ".ogv");

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getPath());
        grabber.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(ogvFile.getPath(),
                grabber.getImageWidth(), grabber.getImageHeight());

        recorder.setFormat("ogv");
        recorder.setVideoOption("preset", "ultrafast");

        recorder.setVideoQuality(5);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_THEORA);
        recorder.setVideoBitrate(grabber.getVideoBitrate());
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);

        recorder.setAudioCodec(avcodec.AV_CODEC_ID_VORBIS);
        recorder.setAudioBitrate(grabber.getAudioBitrate());
        recorder.setAudioChannels(grabber.getAudioChannels());

        recorder.setSampleRate(grabber.getSampleRate());
        recorder.setFrameRate(grabber.getFrameRate());
        recorder.setSampleFormat(grabber.getSampleFormat());

        encode(grabber, recorder);
        return ogvFile;
    }

    public static File addMoovAtomAtBeginning(File file) throws IOException {
    	System.out.println("addMoovAtomAtBeginning " + file.toString());
        FileOutputStream fos = null;
        IsoFile isoFile = null;
        File tempFile = null;
        try {
            isoFile = new IsoFile(file.getPath());
            List<Box> boxes = new LinkedList<Box>(isoFile.getBoxes());

            if (boxes.size() >= 2) {
                if (boxes.get(1).getType().equals("moov")) {
                    System.out.println("File already in progressive download mode");
                    return file;
                }
            }

            String tempFileName = UUID.randomUUID().toString() + ".mp4";
            tempFile = new File(Constants.TEMP_DIR_PATH + tempFileName);

            for (int i = 0; i < boxes.size(); i++) {
                if (boxes.get(i).getType().equals("moov")) {
                    isoFile.getBoxes().remove(i);
                    isoFile.getBoxes().add(1, boxes.get(i));
                }
            }

            if (isoFile.getMovieBox() != null)
                correctChunkOffsets(isoFile, isoFile.getMovieBox().getSize());

            fos = new FileOutputStream(tempFile);
            isoFile.writeContainer(fos.getChannel());

            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (fos != null) fos.close();
                if (isoFile != null) {
                    isoFile.close();
                    isoFile = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.gc();
            }
        }
    }

    private static void correctChunkOffsets(IsoFile tempIsoFile, long correction) {
        List<Box> chunkOffsetBoxes = Path.getPaths(tempIsoFile, "/moov[0]/trak/mdia[0]/minf[0]/stbl[0]/stco[0]");
        for (Box chunkOffsetBox : chunkOffsetBoxes) {
            LinkedList<Box> stblChildren = new LinkedList<Box>(chunkOffsetBox.getParent().getBoxes());
            stblChildren.remove(chunkOffsetBox);
            long[] cOffsets = ((ChunkOffsetBox) chunkOffsetBox).getChunkOffsets();
            for (int i = 0; i < cOffsets.length; i++) {
                cOffsets[i] += correction;
            }
            StaticChunkOffsetBox cob = new StaticChunkOffsetBox();
            cob.setChunkOffsets(cOffsets);
            stblChildren.add(cob);
            chunkOffsetBox.getParent().setBoxes(stblChildren);
        }
    }

    private static void encode(FFmpegFrameGrabber grabber, FFmpegFrameRecorder recorder)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

    	System.out.println("encode()");
        recorder.start();
        Frame frame = null;
        while ((frame = grabber.grabFrame()) != null) {
            if (frame != null) {
                if (frame.image != null) {
                    recorder.record(frame.image);
                } else if (frame.samples != null) {
                    recorder.record(frame.samples);
                }
            } else {
                break;
            }
            Thread.sleep((long) grabber.getFrameRate());
        }
        if (grabber != null) {
            grabber.stop();
            grabber.release();
        }

        if (recorder != null) {
            recorder.stop();
            recorder.release();
        }
    }

    private static String getFileNameWithoutExt(File file) {
        String fileName = file.getName();
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

/*    public static File getFrame(File file) throws  JCodecException {

        try {
            BufferedImage frame = FrameGrab.getFrame(file, 10);


            File tempFile = new File(Constants.TEMP_DIR_PATH + file.getName() + ".jpg");
            ImageIO.write(frame, "jpg", tempFile);
            return tempFile;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }*/

    public static BufferedImage convert(BufferedImage src, int bufImgType) {
        BufferedImage img= new BufferedImage(src.getWidth(), src.getHeight(), bufImgType);
        Graphics2D g2d= img.createGraphics();
        g2d.drawImage(src, 0, 0, null);
        g2d.dispose();
        return img;
    }


}