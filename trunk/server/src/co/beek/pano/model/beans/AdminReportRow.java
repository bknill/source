package co.beek.pano.model.beans;

public class AdminReportRow {

	private String guide;
	private int visits;
	private float averageTime;
	private int conversions;
	private int fbShares;
	private int likes;
	private int webshares;
	private int linkshares;
	private int twittershares;
	
	private String id;
	
	public AdminReportRow(String guide, int visits, float averageTime,
			int conversions, String id) {
		this.guide = guide;
		this.visits = visits;
		this.averageTime = averageTime;
		this.conversions = conversions;
		this.id = id;
	}
	public AdminReportRow(String guide, int visits, float averageTime,
			int conversions, int fbShares, int likes, int webshares,
			int linkshares, int twittershares) {
		this.guide = guide;
		this.visits = visits;
		this.averageTime = averageTime;
		this.conversions = conversions;
		this.fbShares = fbShares;
		this.likes = likes;
		this.webshares = webshares;
		this.linkshares = linkshares;
		this.twittershares = twittershares;
	}

	public String getGuide() {
		return guide;
	}

	public void setGuide(String guide) {
		this.guide = guide;
	}

	public int getVisits() {
		return visits;
	}

	public void setVisits(int visits) {
		this.visits = visits;
	}

	public float getAverageTime() {
		return averageTime;
	}

	public void setAverageTime(float averageTime) {
		this.averageTime = averageTime;
	}

	public int getConversions() {
		return conversions;
	}

	public void setConversions(int conversions) {
		this.conversions = conversions;
	}

	public int getFbShares() {
		return fbShares;
	}

	public void setFbShares(int fbShares) {
		this.fbShares = fbShares;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getWebshares() {
		return webshares;
	}

	public void setWebshares(int webshares) {
		this.webshares = webshares;
	}

	public int getLinkshares() {
		return linkshares;
	}

	public void setLinkshares(int linkshares) {
		this.linkshares = linkshares;
	}

	public int getTwittershares() {
		return twittershares;
	}

	public void setTwittershares(int twittershares) {
		this.twittershares = twittershares;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
