package co.beek.pano.model.beans;




public class TableRowUtil{
    private String c1;
    private String c2;
    private String c3;
    private String c4;
    private String c5;
    private String c6;
    
    public TableRowUtil(String c1, String c2, String c3, String c4, String c5){
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.c5 = c5;
    }
    
    
    public TableRowUtil(String c1, String c2, String c3, String c4){
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
    }
    public TableRowUtil(String c1, String c2, String c3){
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        
    }
   
    
    public TableRowUtil(String c1, String c2){
        this.c1 = c1;
        this.c2 = c2;
        
    }
    
    public String getC1(){
        return c1;
    }
    
    public String getC2(){
        return c2;
    }
    
    public String getC3(){
        return c3;
    }
    
    
    
    public String getC4(){
    	return c4;
    }
    public String getC5(){
    	return c5;
    }
    
    public String getC6() {
		return c6;
	}

	public void setC1(String s)
    {
    	c1=s;
    }
    
    public void setC2(String s)
    {
    	c2=s;
    }
    
    public void setC3(String s)
    {
    	c3=s;
    }
    
    public void setC4(String s)
    {
    	c4=s;
    }
    
    public void setC5(String s)
    {
    	c5=s;
    }
	public void setC6(String c6) {
		this.c6 = c6;
	}
}