package co.beek.pano.model.beans;

import co.beek.pano.model.beans.HiddenTableRow; 
import java.util.ArrayList; 

public class SceneTableRow {

	private String scene;
	private int pageviews;
	private double timeSpent;
	private int bookNows;
	private int moreInfos;
	private int totalConversions;
	private double percentConversion;
	private double percentView;
	private ArrayList<HiddenTableRow> hiddenTable;
	private int timesChosen;
	
	
	
	public SceneTableRow(){
		
	}
	
	public SceneTableRow(String scene){
		this.setScene(scene);
		pageviews = 0;
		timeSpent = 0;
		bookNows = 0;
		moreInfos = 0;
		totalConversions = 0;
		percentConversion = 0;
		percentView = 0;
		
	}
	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}
	public int getPageviews() {
		return pageviews;
	}

	public void setPageviews(int pageviews) {
		this.pageviews = pageviews;
	}

	public double getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(double timeSpent) {
		this.timeSpent = timeSpent;
	}

	public int getBookNows() {
		return bookNows;
	}

	public void setBookNows(int bookNows) {
		this.bookNows = bookNows;
	}

	public int getMoreInfos() {
		return moreInfos;
	}

	public void setMoreInfos(int moreInfos) {
		this.moreInfos = moreInfos;
	}

	public int getTotalConversions() {
		return totalConversions;
	}

	public void setTotalConversions(int totalConversions) {
		this.totalConversions = totalConversions;
	}

	public double getPercentConversion() {
		return percentConversion;
	}

	public void setPercentConversion(double percentConversion) {
		this.percentConversion = percentConversion;
	}

	public double getPercentView() {
		return percentView;
	}

	public void setPercentView(double percentView) {
		this.percentView = percentView;
	}

	public ArrayList<HiddenTableRow> getHiddenTable() {
		return hiddenTable;
	}

	public void setHiddenTable(ArrayList<HiddenTableRow> hiddenTable) {
		this.hiddenTable = hiddenTable;
	}

	public int getTimesChosen() {
		return timesChosen;
	}

	public void setTimesChosen(int timesChosen) {
		this.timesChosen = timesChosen;
	}


}
