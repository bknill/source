package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Visit;

@Entity
@Table(name = "favourites")
public class Favourite implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final FavouriteComparator orderComparator = new FavouriteComparator();

	public static int TYPE_PHOTO = 1;
	public static int TYPE_POSTER = 2;
	
	

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@NotNull
	@Column(name = "email")
	public String email;
	

	@NotNull
	@Column(name = "guide_id")
	public String guideId;

	@NotNull
	@Column(name = "scene_id")
	public String sceneId;

	@Column(name = "hotspot_id")
	public String hotspotId;

	@Column(name = "type")
	public int type;

	@NotNull
	@Column(name = "favourite", columnDefinition = "TINYINT")
	public int favourite;
	

	@NotNull
	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	public Date timestamp;

	public Favourite() {
		timestamp = new Date();
	}

	public String url() {
		String u = "http://" + Constants.domain + "/guide/" + guideId + "/"
				+ sceneId;
		if (!hotspotId.equals("0"))
			u += "/" + hotspotId;
		return u;
	}


}

class FavouriteComparator implements Comparator<Favourite> {
	public int compare(Favourite object1, Favourite object2) {
		return object1.timestamp.after(object2.timestamp) ? 0 : 1;
	}
}
