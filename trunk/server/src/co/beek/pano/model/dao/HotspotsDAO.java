package co.beek.pano.model.dao;

import co.beek.pano.model.dao.hotspots.Photo;

public interface HotspotsDAO {
	Photo getPhoto(String photoId);
}
