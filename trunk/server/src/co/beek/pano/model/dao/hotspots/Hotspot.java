package co.beek.pano.model.dao.hotspots;

public interface Hotspot {
	public String getId();
	public String getTitle();
}
