package co.beek.pano.model.dao;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.hotspots.Photo;

@Repository
@Scope("prototype")
public class HotspotsDAOImpl implements HotspotsDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Photo getPhoto(String photoId) throws HibernateException {
		return (Photo) sessionFactory.getCurrentSession().get(Photo.class,
				photoId);
	}

}
