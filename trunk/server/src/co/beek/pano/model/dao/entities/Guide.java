package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWGroup;

class GuideComparator implements Comparator<Guide> {
	public int compare(Guide r1, Guide r2) {
		return (r2.getScore() - r1.getScore());
	}
}

class GuideIdComparator implements Comparator<Guide> {
	public int compare(Guide r1, Guide r2) {
		return (r2.getIdInt() - r1.getIdInt());
	}
}


@Entity
@Table(name = "guides")
public class Guide implements Serializable {

	public static final GuideComparator scoreComparator = new GuideComparator();
	public static final GuideIdComparator idComparator = new GuideIdComparator();

	private static final long serialVersionUID = 1L;

	public static int STATUS_FREE = 0;
	public static int STATUS_PUBLISHED = 1;
	public static int STATUS_PROMOTED = 2;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id = "";

	@NotNull
	@Column(name = "team_id")
	private String teamId;

	@NotNull
	@Column(name = "destination_id")
	private String destinationId;

	@Column(name = "location_id")
	public String locationId;

	@Column(name = "status")
	public int status;

	@Column(name = "otherguides", columnDefinition = "TINYINT")
	private boolean otherguides;

	@Column(name = "title")
	@Size(max = 100, message = "Invalid title(0-100 characters}")
	private String title;

	@Column(name = "description")
	private String description;


	@Column(name = "colormajor")
	@Size(max = 6, message = "Invalid major color(0-6 characters}")
	private String colorMajor;


	@Column(name = "logo_file_id")
	private Long logoFileId;
	
	@Column(name = "coverDesign")
	private String coverDesign;

	@Column(name = "saveincrement")
	public int saveIncrement;

	@Column(name = "thumbincrement")
	private int thumbIncrement;

	@Column(name = "zipincrement")
	public int zipIncrement;

	@Column(name = "pageviews")
	private int pageviews;

	@Column(name = "visits")
	private int visits;

	@Column(name = "time")
	private int time;

	@Column(name = "score")
	private int score;
	
	@Column(name = "subdomain")
	public String subdomain;
	
	@Column(name = "parent")
	public String parent;

	public String getId() {
		return id;
	}
	
	public Integer getIdInt() {
		return Integer.parseInt(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamId() {
		return teamId;
	}

	/*
	 * public String getDescription() { return description; }
	 * 
	 * public void setDescription(String description) { this.description =
	 * description; }
	 * 
	 * public String getInfoURL() { return infoURL; }
	 * 
	 * public void setInfoURL(String infoURL) { this.infoURL = infoURL; }
	 * 
	 * public String getBookingURL() { return bookingURL; }
	 * 
	 * public void setBookingURL(String bookingURL) { this.bookingURL =
	 * bookingURL; }
	 * 
	 * public String getPhone() { return phone; }
	 * 
	 * public void setPhone(String phone) { this.phone = phone; }
	 * 
	 * public String getEmail() { return email; }
	 * 
	 * public void setEmail(String email) { this.email = email; }
	 * 
	 * public String getAddress() { return address; }
	 * 
	 * public void setAddress(String address) { this.address = address; }
	 */
	
	public String getColorMajor() {
		return colorMajor;
	}

	public void setColorMajor(String colorMajor) {
		this.colorMajor = colorMajor;
	}



	public String getTitle() {
		return title;
	}
	
	public String getEditedTitle() {
		if (title == null || title.isEmpty())
			return "No Title!";
		else
			return title;
		
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * public void setDestinationDescription(String destinationDescription) {
	 * this.destinationDescription = destinationDescription; }
	 * 
	 * public String getDestinationDescription() { return
	 * destinationDescription; }
	 * 
	 * public void setDestinationTitle(String destinationTitle) {
	 * this.destinationTitle = destinationTitle; }
	 * 
	 * public String getDestinationTitle() { return destinationTitle; }
	 */

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Guide)) {
			return false;
		}
		Guide other = (Guide) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Guide[ id=" + id + " ]";
	}

	/**
	 * Only customers and sales staff have permission to edit guides.
	 * 
	 * @param contact
	 * @return
	 */
	public boolean hasWritePermission(EWContact contact) {
		if (contact.isInGroup(EWGroup.GROUP_ADMIN))
			return true;

		if (contact.isInTeam(teamId)) {
			if (contact.isInGroup(EWGroup.GROUP_CUSTOMER))
				return true;

			if (contact.isInGroup(EWGroup.GROUP_SALES))
				return true;
		}

		return false;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setLogoFileId(Long logoFileId) {
		this.logoFileId = logoFileId;
	}

	public Long getLogoFileId() {
		return logoFileId;
	}

	public void setThumbIncrement(int thumbIncrement) {
		this.thumbIncrement = thumbIncrement;
	}

	public int getThumbIncrement() {
		return thumbIncrement;
	}

	public void incrementThumb() {
		this.thumbIncrement++;
	}

	public String getThumbName() {
		
		if(coverDesign != null)
			return coverDesign;
		
		if (thumbIncrement == 0)
			return "guide_default_thumb_1.png";

		return "guide_" + id + "_thumb_" + thumbIncrement + ".png";
	}

	public void setVisits(int visits) {
		this.visits = visits;
	}

	public int getVisits() {
		return visits;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getZipName() {
		return "guide_" + id + "_" + zipIncrement + ".zip";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isOtherguides() {
		return otherguides;
	}

	public void setOtherguides(boolean otherguides) {
		this.otherguides = otherguides;
	}

	public int getPageviews() {
		return pageviews;
	}

	public void setPageviews(int pageviews) {
		this.pageviews = pageviews;
	}
	
	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String Subdomain) {
		this.subdomain = Subdomain;
	}
	
	public String getParent() {
		return parent;
	}

	public void setParent(String Parent) {
		this.parent = Parent;
	}
	
	
	public String getCoverDesign() {
		return coverDesign;
	}

	public void setCoverDesign(String CoverDesign) {
		this.parent = coverDesign;
	}

}
