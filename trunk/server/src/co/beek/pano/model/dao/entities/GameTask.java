package co.beek.pano.model.dao.entities;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
@Table(name = "gametasks")
public class GameTask implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id = 0L;


    @NotNull
    @Column(name = "guide_id")
    public String guideId;



    @Column(name = "[order]")
    public int order;
    
    @NotNull
    @Size(max = 40)
    @Column(name = "code")
    public String code;
    
    @Column(name = "title")
    public String title;

    @Column(name = "input")
    public String input;

    @Size(max = 1000)
    @Column(name = "instructions")
    public String instructions;

    @Size(max = 255)
    @Column(name = "feedback")
    public String feedback;
    
    @Column(name = "output")
    public String output;

    @Column(name = "scene")
    public String scene;

    @Column(name = "load_scene")
    public String load_scene;
    
    public GameTask cloneNow(String guideId) throws CloneNotSupportedException
	{
		GameTask clone = (GameTask) super.clone();
		clone.guideId = guideId;
		return clone;
	}

    public String getId(){
        return id.toString();
    }

    public String getInstructions(){
        return instructions;
    }

    public void setInstructions(String instructions){
        this.instructions = instructions;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String  title){
        this.title =  title;
    }

    public String getFeedback(){
        return feedback;
    }

    public void setFeedback(String  feedback){
        this.feedback = feedback;
    }

    public String getInput(){
        return input;
    }

    public void setInput(String input){
        this.input = input;
    }

    public String getOutput(){
        return output;
    }

    public void setOutput(String output){
        this.output = output;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getGuideId() {
        return guideId;
    }

    public void setGuideId(String guideId) {
        this.guideId = guideId;
    }

    public String getCode() {
        if(code == null)
        return "findhotspots";
            else
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getLoad_scene() {
        return load_scene;
    }

    public void setLoad_scene(String load_scene) {
        this.load_scene = load_scene;
    }

}
