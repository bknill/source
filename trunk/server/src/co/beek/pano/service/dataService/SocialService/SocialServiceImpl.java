package co.beek.pano.service.dataService.SocialService;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import co.beek.pano.model.dao.social.TwitterDAO;

@Service
@Scope("prototype")
@Transactional
public class SocialServiceImpl implements SocialService {


	@Override
	public TwitterDAO getTwitter(String guideId, String sceneId) {	

		System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
		
		ConfigurationBuilder cb;

		cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey("D7YuXcYDFdwZCQQP1eoag")
		  .setOAuthConsumerSecret("IqnNWJvP6IibxLyBB3ge0t7mllVp8rUZkGy0mVRf0M")
		  .setOAuthAccessToken("106841423-07T03nRDAa6PnBWgVZvJdr2c6g9fnuRfBncKvntB")
		  .setOAuthAccessTokenSecret("A89ULdQhN9ZHvXjvSkX2pOBDWec3XYwWpwfb9aYPw");
		
		TwitterFactory tf = new TwitterFactory(cb.build());

		
	    Twitter twitterSearch = tf.getInstance();
	    String queryText = "beek.co/g" + guideId + "/s" + sceneId;
	    Query query = new Query(queryText);
	    query.count(1);
	    QueryResult result = null;
		try {
			result = twitterSearch.search(query);
		    for (Status status : result.getTweets()) {
		      	TwitterDAO response = new TwitterDAO();
				response.setStatus(status.getText());
				response.setUser(status.getUser().getScreenName());
				response.setDate(status.getCreatedAt().toString());
				return response;
		    }
		
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}





}
