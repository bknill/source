package co.beek.pano.service.dataService.hotspotService;

import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;

public interface HotspotService {

	public Photo getPhoto(String photoId);

	public Poster getPoster(String posterId);

	public Bubble getBubble(String bubbleId);

}