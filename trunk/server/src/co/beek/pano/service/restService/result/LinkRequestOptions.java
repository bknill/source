package co.beek.pano.service.restService.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;

public class LinkRequestOptions implements Serializable {
	private static final long serialVersionUID = 1L;

	public String locationId;

	public List<Location> myLocations = new ArrayList<Location>();

	public List<Guide> theirGuides = new ArrayList<Guide>();

	public LinkRequestOptions(String locationId) {
		this.locationId = locationId;
	}
}
