package co.beek.pano.service.restService;


public class ServiceResponse {
	
	private Object payload;
	
	private int error;

	private String message;

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public Object getPayload() {
		return payload;
	}

	public void setError(int error) {
		this.error = error;
	}

	public int getError() {
		return error;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
