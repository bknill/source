package co.beek.pano.service.restService;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceceService.SceneService;

@Controller
public class LocationRestController extends BeekRestController {
	@Inject
	private LocationService locationService;
	
	@Inject
	private SceneService sceneService;

	@ResponseBody
	@RequestMapping(value = "/location/{locationId}/scenes", method = RequestMethod.GET)
	public ResponseEntity<String> getScenesForLocation(
			@PathVariable("locationId") String locationId) {

		try {
			List<Scene> scenes = sceneService.getAllScenesForLocation(locationId);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/location/create/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> createLocation(
			@PathVariable("name") String locationName) {
			Location location;
			location = new Location();
			location.setStatus(Location.STATE_NEW);
			location.setTitle(locationName);
			location.setTeamId("CUSTOMER_1_");
			location.setDestinationId("1");

		try {
			locationService.saveLocation(location);
			return getPayloadResponse(location);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/locations/within/{minLat}/{minLon}/{maxLat}/{maxLon}/", method = RequestMethod.GET)
	public ResponseEntity<String> getWith(
			@PathVariable("minLat") double minLat,
			@PathVariable("minLon") double minLon,
			@PathVariable("maxLat") double maxLat,
			@PathVariable("maxLon") double maxLon) {	
		try {
			List<Location> scenes = locationService.getLocationsWithin(minLat,
					minLon, maxLat, maxLon);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/locations/destination/{destinationId}", method = RequestMethod.GET)
	public ResponseEntity<String> getForDestination(
			@PathVariable("destinationId") String destinationId) {
		try {
			List<Location> locations = locationService.getLocationsForDestination(destinationId);
			return getPayloadResponse(locations);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
}
