package co.beek.web.guide;

import co.beek.Constants;
import co.beek.pano.model.beans.GameTaskHotspot;
import co.beek.pano.model.dao.GameTaskDAO;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.entities.*;
import co.beek.pano.model.dao.googleanalytics.Country;
import co.beek.pano.model.dao.googleanalytics.Referral;
import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;
import co.beek.pano.service.dataService.BeekService;
import co.beek.pano.service.dataService.GameTaskService.GameTaskService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import co.beek.pano.service.dataService.googleAnalytics.BasicData;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceceService.SceneService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import co.beek.util.BeekSession;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;
import co.beek.web.location.LocationDashboardController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import flexjson.JSONSerializer;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.primefaces.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.primefaces.event.FileUploadEvent;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import java.io.*;
import java.text.NumberFormat;
import java.util.*;

@Scope("session")
@Controller("GuideViewAdminController")
@ViewScoped
@ManagedBean
public class GuideViewAdminController extends BaseController implements Serializable {

    @Inject private GuideService guideService;
    @Inject private SceneService sceneService;
    @Inject private GameTaskService gameTaskService;
    @Inject private LocationDashboardController locationDashboardController;
    @Inject private S3UploadService s3UploadService;
    @Inject private BeekService beekService;
    private BeekSession session;


    @Value("#{buildProperties.assetsBucket}")
    private String assetsBucket;
    private Beek beek;
    private GuideDetail selectedGuide;
    public boolean guideFont;
    private TreeNode selectedNode;
    private GameTask selectedTask;
    private GuideScene selectedGuideScene;
    private GuideReport guideReport;
    private List<Referral> referrals;
    private String guideId;

    private EWContact user;
    private List<EWContact> users;
    public List<String> usersEmails;
    private EWService ewService = new EWServiceImpl();
    private String emailAddress;

    private List<GameTask> gameTasks;
    private DualListModel<GameTaskHotspot> gameTaskHotspots;
    private DualListModel<GameTaskHotspot> gameTaskHotspotsToShow;

    private ArrayList<GameTaskHotspot> selectedGuideSceneHotspotsToFind;
    private ArrayList<GameTaskHotspot> taskHotspotsToFind;
    private ArrayList<GameTaskHotspot> selectedGuideSceneHotspotsToShow;
    private ArrayList<GameTaskHotspot> taskHotspotsToShow;




    private ArrayList<GuideScene> selectedGuideScenes;

    private String multichoiceOption;
    private ArrayList<String> multichoice = new ArrayList<String>();



    private GameChoice gameChoiceOption;
    private ArrayList<GameChoice> gameChoices;


    private HashMap<String, String> flashVars;
    private Map<String, TreeNode> treeNodesMap;

    @Value("#{buildProperties.gaCode}")
    private String gaCode;

    @Value("#{buildProperties.cdnUrl}")
    private String cdnUrl;

    JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

    @RequestMapping(value = "/guide/{guideId}/admin", method = RequestMethod.GET)
    public String testView(@PathVariable("guideId") String guideId){
        return "/views/guide/admin.jsf?g=" + guideId;
    }


    private void updateSelectedGuide(){
        selectedGuide = guideService.getGuideDetail(guideId);
        gameTasks = selectedGuide.getGameTasks();
    }

    public void updateSelectedGuideScene(){
        updateSelectedGuide();

        selectedGuideScene = selectedGuide.getGuideScene(selectedGuideScene.getSceneId());

        selectedGuideSceneHotspotsToFind = loadSelectedGuideSceneHotspotsData(0);
        taskHotspotsToFind = loadTaskHotspots(0);
        selectedGuideSceneHotspotsToShow = loadSelectedGuideSceneHotspotsData(1);
        taskHotspotsToShow = loadTaskHotspots(1);
    }



    public void onMarkerSelect() {
        Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (requestParamMap.containsKey("locationId")) {
            String locationId = requestParamMap.get("locationId");
            String scenesJSON = updateGuideSceneTable(locationId);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("handleUpdateScenes("+scenesJSON+")");
        }
    }

    public void handleGuideCoverUpload(FileUploadEvent event) {

            selectedGuide.incrementThumb();
            File file = new File(Constants.TEMP_DIR_PATH + selectedGuide.getThumbName());

            UploadedFile uploadedFile = event.getFile();
            try {
                InputStream input = uploadedFile.getInputstream();
                OutputStream output = new FileOutputStream(file);

                 IOUtils.copy(input, output);
                // upload thumb to the scenes bucket
                 s3UploadService.uploadFile(assetsBucket, file);
                guideService.saveGuideDetail(selectedGuide);

             } catch (Exception e) {
                System.out.println(e.toString());
                sendErrorDan("Error Uploading Thumb", e);
             }

    }


    private String updateGuideSceneTable(String locationId){
        if (locationId == null || locationId.equals("")) {
            scenes = null;
            return "[]";
        }
        try {
            scenes = sceneService.getAllScenesForLocation(locationId);
            
            //clear out existing scenes in guide
            for(int i=scenes.size()-1; i> -1; i--)
            	if(selectedGuide.getGuideScene(scenes.get(i).getId()) != null )
            		scenes.remove(scenes.get(i));

            return serializer.serialize(scenes);
        } catch (Exception e) {
            scenes = null;
            return "[]";
        }
    }

    private void initGuideSectionTree(){
        treeNodesMap = new HashMap<String, TreeNode>();
        Map<String, List<TreeNode>> map2 = new HashMap<String, List<TreeNode>>();
        rootTreeNode = new DefaultTreeNode("root",new TreeNodeModel(selectedGuide.getId(), null ,selectedGuide.getTitle(),null, null, 0, 0, 0.0), null);

        rootTreeNode.setExpanded(true);

        for(GuideSection guideSection : selectedGuide.getGuideSections()){
            TreeNode sectionNode = new DefaultTreeNode("section", new TreeNodeModel(guideSection.id, guideSection.parent_section_id ,guideSection.getTitle(),null, null, guideSection.order, 0, 0.0), null);
           // sectionNode.setExpanded(true);
            for (int i = 0; i < guideSection.getGuideScenes().size(); i++) {
                GuideScene guideScene = guideSection.getGuideScenes().get(i);
                TreeNode sceneNode = new DefaultTreeNode("scene", new TreeNodeModel(guideScene.id, guideSection.id , guideScene.getTitleMerged(), guideScene.getScene().getId(), guideScene.getScene().getThumbName(), guideScene.getOrder(), guideScene.getVisits(), guideScene.getAvgTime()), sectionNode);
            }
            if(guideSection.childGuide != null && guideSection.childGuide.getGuideSections().size() == 1){
                for(GuideScene guideScene : guideSection.childGuide.getGuideSections().get(0).getGuideScenes()){
                    TreeNode sceneNode = new DefaultTreeNode("child-guide-scene", new TreeNodeModel(guideScene.id, guideSection.id , guideScene.getTitleMerged(), guideScene.getScene().getId(), guideScene.getScene().getThumbName(), guideScene.getOrder(), guideScene.getVisits(), guideScene.getAvgTime()), sectionNode);
                    sceneNode.setSelectable(false);
                }
            }

            if(guideSection.parent_section_id == null){
                ((TreeNodeModel)sectionNode.getData()).setParentId("root");
                rootTreeNode.getChildren().add(sectionNode);
            }else{
                if(treeNodesMap.containsKey(guideSection.parent_section_id)){
                    treeNodesMap.get(guideSection.parent_section_id).getChildren().add(sectionNode);
                }else{
                    if(guideSection.parent_section_id != null){
                        List<TreeNode> list;
                        if (map2.containsKey(guideSection.parent_section_id)) {
                            list = map2.get(guideSection.parent_section_id);
                        } else {
                            list = new ArrayList<TreeNode>();
                        }
                        list.add(sectionNode);
                        map2.put(guideSection.parent_section_id, list);
                    }
                }
            }
            if (map2.containsKey(guideSection.id)) {
                List<TreeNode> list = map2.get(guideSection.id);
                for (TreeNode node : list) {
                    sectionNode.getChildren().add(node);
                }
                map2.remove(guideSection.id);
            }
            treeNodesMap.put(guideSection.id, sectionNode);
            if(guideSection.childGuide != null && guideSection.childGuide.getGuideSections().size() > 1){
               initChildGuideSectionTree(guideSection.id, guideSection.childGuide);
            }
        }
        sortTree(treeNodesMap);
        addDropRegionsToTree(treeNodesMap);
        map2 = null;
    }

    public void updateGuideSection(){
        String childGuideId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("childGuideId");
        String sectionId =    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("sectionId");

        updateSelectedGuide();
        for(GuideSection guideSection : selectedGuide.getGuideSections()){
            if(guideSection.id.equals(sectionId)){
                guideSection.childGuideId = childGuideId;
                try{
                    guideService.saveGuideDetail(selectedGuide);
                    GuideDetail childGuideDetail = guideService.getGuideDetail(childGuideId);
                    initChildGuideSectionTree(sectionId, childGuideDetail);

                    TreeNode parentTreeNode = treeNodesMap.get(sectionId).getParent();
                    while(parentTreeNode != null){
                        parentTreeNode.setExpanded(true);
                        parentTreeNode = parentTreeNode.getParent();
                    }
                    treeNodesMap.get(sectionId).setExpanded(true);

                    RequestContext requestContext = RequestContext.getCurrentInstance();
                    requestContext.addCallbackParam("child_guide_id",childGuideId);
                    requestContext.addCallbackParam("section_id",sectionId);
                }catch(HibernateException e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void initChildGuideSectionTree(String sectionId, GuideDetail guideDetail){
        Map<String, TreeNode> map1 = new HashMap<String, TreeNode>();
        Map<String, List<TreeNode>> map2 = new HashMap<String, List<TreeNode>>();

        if(!treeNodesMap.containsKey(sectionId)) return;

        DefaultTreeNode rootSectionNode = (DefaultTreeNode) treeNodesMap.get(sectionId);

        if(guideDetail.getGuideSections().size() == 1){
            for(GuideScene guideScene : guideDetail.getGuideSections().get(0).getGuideScenes()) {
                addSceneTreeNode("child-guide-scene", sectionId, guideScene);
            }
        }else{
            for(GuideSection guideSection : guideDetail.getGuideSections()){
                TreeNode sectionNode = new DefaultTreeNode("child-guide-section", new TreeNodeModel(guideSection.id, guideSection.parent_section_id ,guideSection.getTitle(),null, null, guideSection.order, 0, 0.0), null);
                sectionNode.setSelectable(false);
                for (int i = 0; i < guideSection.getGuideScenes().size(); i++) {
                    GuideScene guideScene = guideSection.getGuideScenes().get(i);
                    TreeNode sceneNode = new DefaultTreeNode("child-guide-scene", new TreeNodeModel(guideScene.id, guideSection.id , guideScene.getTitleMerged(), guideScene.getScene().getId(), guideScene.getScene().getThumbName(), guideScene.getOrder(), guideScene.getVisits(), guideScene.getAvgTime()), sectionNode);
                    sceneNode.setSelectable(false);
                }
                if(guideSection.parent_section_id == null){
                    ((TreeNodeModel)sectionNode.getData()).setParentId(sectionId);
                    sectionNode.setType("child-guide-root-section");
                    rootSectionNode.getChildren().add(sectionNode);
                }else{
                    if(map1.containsKey(guideSection.parent_section_id)){
                        map1.get(guideSection.parent_section_id).getChildren().add(sectionNode);
                    }else{
                        if(guideSection.parent_section_id != null){
                            List<TreeNode> list;
                            if (map2.containsKey(guideSection.parent_section_id)) {
                                list = map2.get(guideSection.parent_section_id);
                            } else {
                                list = new ArrayList<TreeNode>();
                            }
                            list.add(sectionNode);
                            map2.put(guideSection.parent_section_id, list);
                        }
                    }
                }
                if (map2.containsKey(guideSection.id)) {
                    List<TreeNode> list = map2.get(guideSection.id);
                    for (TreeNode node : list) {
                        sectionNode.getChildren().add(node);
                    }
                    map2.remove(guideSection.id);
                }
                map1.put(guideSection.id, sectionNode);
            }
            //sortTree(map1);
            addDropRegionsToTree(map1);
        }
        map1 = null;
        map2 = null;
    }

    private void sortTree(Map<String, TreeNode> map){
        sort(rootTreeNode.getChildren());
        for(Map.Entry<String,TreeNode> entry : map.entrySet()){
            sort(entry.getValue().getChildren());
        }
    }

    private void sort(List<TreeNode> list){
        Comparator<TreeNode> comparator = new Comparator<TreeNode>() {
            @Override
            public int compare(TreeNode o1, TreeNode o2) {
                TreeNodeModel model1 = ((TreeNodeModel)o1.getData());
                TreeNodeModel model2 = ((TreeNodeModel)o2.getData());
                if (o1.getType().equals("scene") && o2.getType().equals("scene")) {
                    return model1.getOrder() - model2.getOrder();
                }else if (o1.getType().equals("section") && o2.getType().equals("section")) {
                    return model1.getOrder() - model2.getOrder();
                }
                return 0;
            }
        };
        Object[] array = list.toArray();
        Arrays.sort(array, (Comparator) comparator);
        int counter = 0;
        for (Object e : array) {
            list.add(counter++, (TreeNode)e);
        }
    }

    private void addDropRegionsToTree(Map<String, TreeNode> map){
        int index = 0;
        for (Map.Entry<String, TreeNode> entry : map.entrySet()) {
            TreeNode parentNode = entry.getValue();
            TreeNodeModel parentModel = ((TreeNodeModel) parentNode.getData());

            if (parentNode.getParent() == rootTreeNode) {
                String rootNodeId = ((TreeNodeModel)(parentNode.getParent()).getData()).getId();
                if (index == 0) {
                    DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-root"));
                    dropRegionNode.setType("dropRegion");
                    parentNode.getParent().getChildren().add(0, dropRegionNode);
                }
                DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("section-root"));
                dropRegionNode1.setType("dropRegion");
                index+=2;
                parentNode.getParent().getChildren().add(index , dropRegionNode1);

            }else if(parentNode.getType().equals("child-guide-root-section")){
                String rootNodeId = ((TreeNodeModel)(parentNode.getParent()).getData()).getId();

                if(parentNode.getParent().getChildCount() == 1){
                    DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-"+rootNodeId));
                    dropRegionNode.setType("dropRegion");
                    parentNode.getParent().getChildren().add(0, dropRegionNode);
                }
                DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("section-"+rootNodeId));
                dropRegionNode1.setType("dropRegion");
                int iindex = parentNode.getParent().getChildren().indexOf(parentNode);
                parentNode.getParent().getChildren().add(iindex+1 , dropRegionNode1);
            }

            boolean flag = true;
            for (int i = 0; i < parentNode.getChildren().size(); i++) {
                TreeNode node = parentNode.getChildren().get(i);
                if (node.getType().equals("scene") || node.getType().equals("child-guide-scene")) {
                    DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("scene-" + parentModel.getId()));
                    dropRegionNode.setType("dropRegion");
                    if (i == 0) {
                        parentNode.getChildren().add(0, dropRegionNode);
                    } else if (i == parentNode.getChildren().size() - 1) {
                        dropRegionNode = new DefaultTreeNode(new TreeNodeModel("scene-section-" + parentModel.getId()));
                        dropRegionNode.setType("dropRegion");
                        parentNode.getChildren().add(i + 1, dropRegionNode);

                    } else {
                        parentNode.getChildren().add(i + 1, dropRegionNode);
                    }

                } else if (node.getType().equals("section")
                        || ((node.getType().equals("child-guide-section")))
                        || (node.getType().equals("child-guide-root-section"))) {

                    DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-" + parentModel.getId()));
                    dropRegionNode.setType("dropRegion");

                    if(flag){
                        if(i != 0) {
                            DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("scene-section-" + parentModel.getId()));
                            dropRegionNode1.setType("dropRegion");
                            parentNode.getChildren().set(i - 1, dropRegionNode1);
                        }
                        flag = false;
                    }
                    if((node.getType().equals("child-guide-root-section")))continue;
                    if (i == 0) {
                        parentNode.getChildren().add(0, dropRegionNode);
                    }else {
                        parentNode.getChildren().add(i + 1, dropRegionNode);
                    }
                }
            }
        }
    }


    public void updateSceneOrder() throws JSONException {
        String json = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("updatedSceneOrder");

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(json);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        String parentSectionId = null;
        for(GuideSection guideSection : selectedGuide.getGuideSections())
            for(GuideScene guideScene : guideSection.getGuideScenes())
                if(map.containsKey(guideScene.id)) {
                    parentSectionId = guideSection.id;
                    guideScene.setOrder(Integer.parseInt(map.get(guideScene.id)));
                }

        saveGuide();
        updateTreeNodesSceneOrder(parentSectionId, map);
    }

    private void updateTreeNodesSceneOrder(String parentSectionId, HashMap<String, String> map){
        if(parentSectionId != null && treeNodesMap.containsKey(parentSectionId)) {
            TreeNode parentTreeNode = treeNodesMap.get(parentSectionId);
            List<TreeNode> childs = parentTreeNode.getChildren();
            Map<Integer, TreeNode>  tempMap  = new HashMap<Integer, TreeNode>();

            for(TreeNode treeNode: childs){
                TreeNodeModel treeNodeModel = (TreeNodeModel) treeNode.getData();
                if(treeNode.getType().equals("scene")){
                    if(map.containsKey(treeNodeModel.getId())){
                        int newOrder = Integer.parseInt(map.get(treeNodeModel.getId()));
                        int temp = newOrder + 1;
                        newOrder = newOrder + temp;
                        tempMap.put(newOrder, treeNode);
                    }
                }
            }
            for(Map.Entry<Integer, TreeNode> entries : tempMap.entrySet()){
                childs.set(entries.getKey(), entries.getValue());
            }
        }
    }

    public void updateSectionOrder() throws JSONException {
        String json = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("updatedSectionOrder");

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(json);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        for(GuideSection guideSection : selectedGuide.getGuideSections())
                if(map.containsKey(guideSection.id))
                    guideSection.order = Integer.parseInt(map.get(guideSection.id));

        saveGuide();
        updateTreeNodesSectionOrder(map);
    }

    private void updateTreeNodesSectionOrder(HashMap<String, String> map){
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();

            if(treeNodesMap.containsKey(key)){
                TreeNode treeNode = treeNodesMap.get(key);
                TreeNode parentTreeNode = treeNode.getParent();
                List<TreeNode> childs = treeNode.getParent().getChildren();
                int oldOrder  = childs.indexOf(treeNode);

                int childsCount = childs.size();
                int sectionsCount = map.size();
                int sectionsWithDropRegionsCount = 0;
                if(sectionsCount == 1){
                    sectionsWithDropRegionsCount = sectionsCount + (sectionsCount + 2);
                }else{
                    sectionsWithDropRegionsCount = sectionsCount + (sectionsCount + 1);
                }

                int newOrder = Integer.parseInt(entry.getValue());
                int temp = newOrder + 1;
                newOrder = newOrder + temp;
                newOrder = newOrder + (childsCount - sectionsWithDropRegionsCount);

                if(oldOrder != newOrder) {
                    Collections.swap(childs, oldOrder, newOrder);
                    childs.get(newOrder).setParent(parentTreeNode);
                }
            }
        }
    }

    public void preRenderView() throws Exception {
        if (redirectingToLogin())
            return;

        if (FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) return;

        scenes = null;
        rootTreeNode  = null;
        beek = beekService.getBeek();

        HttpServletRequest request = getRequest();

        if (Constants.domain == null)
            throw new Exception("Please visit the login page");

        guideId = request.getParameter("id");
        if (guideId == null || guideId.equals(""))
            guideId = request.getParameter("g");


        updateSelectedGuide();

        if (selectedGuide == null)
            throw new Exception("Guide not found");

        selectedGuideScene = selectedGuide.returnFirstScene();
        gameTasks = selectedGuide.getGameTasks();
        selectedGuideSceneHotspotsToFind = loadSelectedGuideSceneHotspotsData(0);
        taskHotspotsToFind = loadTaskHotspots(0);
        selectedGuideSceneHotspotsToShow = loadSelectedGuideSceneHotspotsData(1);
        taskHotspotsToShow = loadTaskHotspots(1);
        guideReport = guideService.getGuideReport(guideId);
        locationDashboardController.preRenderView();
        session = getSession();

        flashVars = new HashMap<String, String>();
        flashVars.put("domain", Constants.domain);
        flashVars.put("assetCdn", cdnUrl);
        flashVars.put("adminSwf", getAdminSwfUrl());
        flashVars.put("gaCode", gaCode);
        flashVars.put("guideId", selectedGuide.getId());
        if(selectedGuide.returnFirstScene() != null)
            flashVars.put("sceneId", selectedGuide.returnFirstScene().getSceneId());
        flashVars.put("autologin", "true");
    }

    private List<Scene> scenes;

    public List<Scene> getScenes(){
        return scenes;
    }

    private TreeNode rootTreeNode;

    public TreeNode getRootTreeNode() {
        if(rootTreeNode == null)
            initGuideSectionTree();
        return rootTreeNode;
    }

    public GuideDetail getSelectedGuide() {
        return selectedGuide;
    }

    public String getAdminSwfUrl() {
        if (Constants.domain.contains("beekdev.co"))
            return "http://gms.beekdev.co/resources/swf/swf_admin.swf";

        return cdnUrl + "/" + beek.getAdminFileName();
    }
    public String getPlayerSwfUrl() {
        if (Constants.domain.contains("beekdev.co"))
            return "http://gms.beekdev.co/resources/swf/swf_player.swf";

        return cdnUrl + "/" + beek.getPlayerFileName();
    }

    public void setSelectedGuide(GuideDetail selectedGuide) {
        this.selectedGuide = selectedGuide;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;



        if(selectedNode == null)
            return;

        if(selectedNode.isLeaf())
        {
            TreeNodeModel treeNodeModel = (TreeNodeModel) selectedNode.getData();

            updateSelectedGuide();
            for(GuideSection gse : selectedGuide.getGuideSections())
            for (GuideScene gsc : gse.getGuideScenes())
                if (gsc.getId().equals(treeNodeModel.getId()) )
                    selectedGuideScene = gsc;

        }


    }



    public GuideScene getSelectedGuideScene() {
        return selectedGuideScene;
    }

    public List<SelectItem> getSelectedGuideScenesSelectItemList() {
        List<SelectItem> list = new ArrayList<SelectItem>();

        for(GuideScene scene : selectedGuide.returnGuideScenes()){
            SelectItem item = new SelectItem(scene.getSceneId(),scene.getTitle());
            list.add(item);
        }

        return list;
    }



    public List<GameTask> getGameTasks() {
        return gameTasks;
    }

    public void setGameTasks(List<GameTask> gameTasks) {this.gameTasks = gameTasks;}


    public ArrayList<GameTaskHotspot> loadSelectedGuideSceneHotspotsData(int type) {
        ArrayList<GameTaskHotspot> tempArray = new ArrayList<GameTaskHotspot>();
        ArrayList<GameTaskHotspot> dupes = new ArrayList<GameTaskHotspot>();
        ArrayList<GameTaskHotspot> existing = loadTaskHotspots(type);

        SceneDetail scene = sceneService.getSceneDetail(selectedGuideScene.getSceneId());
        List<Bubble> bubbles = scene.getBubbles();

        List<Photo> photos = selectedGuideScene.getPhotos();
        photos.addAll(scene.getPhotos());

        List<Poster> posters = selectedGuideScene.getPosters();
        posters.addAll(scene.getPosters());

        for(Photo photo : photos){
            GameTaskHotspot photoData = new GameTaskHotspot();
            photoData.setId(photo.getId());
            photoData.setTitle(photo.getTitle());
            tempArray.add(photoData);
        }

        for(Poster poster :posters){
            GameTaskHotspot posterData = new GameTaskHotspot();
            posterData.setId(poster.getId());
            posterData.setTitle(poster.getTitle());
            tempArray.add(posterData);
        }

        for(Bubble bubble : bubbles){
            GameTaskHotspot bubbleData = new GameTaskHotspot();
            bubbleData.setId(bubble.getId());
            bubbleData.setTitle(bubble.getTitleMerged());
            tempArray.add(bubbleData);
        }

        if(tempArray.size() < 1)
            return tempArray;

        //clear out duplicates
        if(existing.size() > 0) {
            for (GameTaskHotspot g : tempArray)
                for (GameTaskHotspot e : existing)
                    if (g.getId().equals(e.getId()))
                        dupes.add(g);

            tempArray.removeAll(dupes);
        }

        return tempArray;

    }

    public ArrayList<GameTaskHotspot> loadTaskHotspots(int type){
        ArrayList<GameTaskHotspot> tempArray = new ArrayList<GameTaskHotspot>();

        if(selectedTask == null)
            return tempArray;

        if(type == 0 && selectedTask.input == null)
            return tempArray;

        if(type == 1 && selectedTask.output == null)
            return tempArray;


        Gson gson = new Gson();

        GameTaskHotspot[] existing = gson.fromJson(type == 0 ? selectedTask.input : selectedTask.output, GameTaskHotspot[].class);

        for(GameTaskHotspot gth : existing)
            tempArray.add(gth);

        return tempArray;
    }

    public  DualListModel<GameTaskHotspot> getGameTaskHotspots(){
        return new DualListModel<GameTaskHotspot>(selectedGuideSceneHotspotsToFind, taskHotspotsToFind);
    }

    public void setGameTaskHotspots(DualListModel<GameTaskHotspot> gameTaskHotspots) {
        this.gameTaskHotspots = gameTaskHotspots;
    }

    public  DualListModel<GameTaskHotspot> getGameTaskHotspotsToShow(){
        return new DualListModel<GameTaskHotspot>(selectedGuideSceneHotspotsToShow, taskHotspotsToShow);
    }

    public void setGameTaskHotspotsToShow(DualListModel<GameTaskHotspot> gameTaskHotspotsToShow) {
        this.gameTaskHotspotsToShow = gameTaskHotspotsToShow;
    }

    public void addGameTask(){

        GameTask task = new GameTask();
        task.order = gameTasks.size();
        task.code = "findHotspots";
        task.guideId = selectedGuide.getId();
       // gameTaskService.saveGameTask(task);
        gameTasks.add(task);
        //updateSelectedGuideScene();
        selectedTask = task;
    }



    public void addSection(){

        String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hidden:newSection");
        GuideSection section = new Gson().fromJson(value, GuideSection.class);
        List<GuideSection> sections = selectedGuide.getGuideSections();

        sections.add(section);
        updateSelectedGuide();
        selectedGuide.setGuideSections(sections);

        saveGuide();
        addSectionTreeNode(section);
    }

    private void addSectionTreeNode(GuideSection section){
        TreeNode sectionNode = null;
        if(section.parent_section_id  == null){
            sectionNode = new DefaultTreeNode("section", new TreeNodeModel(section.id, "root" ,section.getTitle(),null, null, section.order, 0, 0.0), null);
            if(rootTreeNode.getChildren().size() == 0){
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-root"));
                dropRegionNode.setType("dropRegion");
                rootTreeNode.getChildren().add(dropRegionNode);

                rootTreeNode.getChildren().add(sectionNode);

                DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("section-root"));
                dropRegionNode1.setType("dropRegion");
                rootTreeNode.getChildren().add(dropRegionNode1);
            }else{
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-root"));
                dropRegionNode.setType("dropRegion");
                rootTreeNode.getChildren().add(sectionNode);
                rootTreeNode.getChildren().add(dropRegionNode);
            }
        }else{
            sectionNode = new DefaultTreeNode("section", new TreeNodeModel(section.id, section.parent_section_id ,section.getTitle(),null, null, section.order, 0, 0.0), null);
            List<TreeNode> childs = treeNodesMap.get(section.parent_section_id).getChildren();

            if(childs.size() == 0){
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-" + section.parent_section_id));
                dropRegionNode.setType("dropRegion");
                childs.add(dropRegionNode);

                childs.add(sectionNode);

                DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("section-" + section.parent_section_id));
                dropRegionNode1.setType("dropRegion");
                childs.add(dropRegionNode1);

            }else{
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("section-" + section.parent_section_id));
                dropRegionNode.setType("dropRegion");
                childs.add(sectionNode);
                childs.add(dropRegionNode);
            }
        }
        treeNodesMap.put(section.id, sectionNode);
    }

    private GuideSection getSelectedSection(){
        String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hidden:newSection");
        return new Gson().fromJson(value, GuideSection.class);
    }

    public void deleteSection(){
        GuideSection section = getSelectedSection();
        updateSelectedGuide();
        List<GuideSection> sections = selectedGuide.getGuideSections();
        List<GuideScene> guideScenes = selectedGuide.returnGuideScenes();

//        for(Iterator<GuideSection> i = sections.listIterator(); i.hasNext(); ) {
//            GuideSection guideSection = i.next();
//            if(guideSection.id.equals(section.id))
//                i.remove();
//        }
//
//        for(Iterator<GuideSection> i = sections.listIterator(); i.hasNext(); ) {
//            GuideSection guideSection = i.next();
//            if(guideSection.parent_section_id != null)
//                if(guideSection.parent_section_id.equals(section.id))
//                    i.remove();
//        }
        for(Iterator<GuideSection> i = sections.listIterator(); i.hasNext(); ) {
            GuideSection guideSection = i.next();
            if(guideSection.id.equals(section.id) ||
                    ((guideSection.parent_section_id != null)&&(guideSection.parent_section_id.equals(section.id)))) {
                i.remove();
            }else {
                if(guideSection.parent_section_id != null && treeNodesMap.containsKey(guideSection.parent_section_id)){
                    DefaultTreeNode parentTreeNode =  (DefaultTreeNode) treeNodesMap.get(guideSection.parent_section_id);
                    if(parentTreeNode.getParent()!= null){
                        TreeNodeModel treeNodeModel = (TreeNodeModel) parentTreeNode.getParent().getData();
                        if(treeNodeModel.getId().equals(section.id)){
                            i.remove();
                        }
                    }
                }
            }
        }

        for(Iterator<GuideScene> i = guideScenes.listIterator(); i.hasNext(); ) {
            GuideScene guideScene = i.next();
            if(guideScene.getGuideSectionId().equals(section.id))
                selectedGuide.removeGuideScene(guideScene.getScene());
        }

            selectedGuide.setGuideSections(sections);
            saveGuide();

        deleteSectionTreeNode(section);

    }

    private void deleteSectionTreeNode(GuideSection section){
        if(treeNodesMap.containsKey(section.id)){
            DefaultTreeNode sectionTreeNode =  (DefaultTreeNode) treeNodesMap.get(section.id);

            Iterator<Map.Entry<String, TreeNode>> iterator = treeNodesMap.entrySet().iterator();
            while(iterator.hasNext()){
                Map.Entry<String, TreeNode> entry = iterator.next();
                TreeNode node = entry.getValue();
                TreeNodeModel nodeModel = ((TreeNodeModel) node.getData());

                if(node.getParent() == null){
                    iterator.remove();
                    continue;
                }

                if(nodeModel.getId().equals(section.id) || nodeModel.getParentId().equals(section.id)
                        || ((node.getParent().getParent() != null)&&(((TreeNodeModel)node.getParent().getData()).getParentId().equals(section.id)))){

                    iterator.remove();
                }
            }
            List<TreeNode> childs = sectionTreeNode.getParent().getChildren();
            if(childs.size() == 3){
                childs.clear();
            }else{
                int index  = childs.indexOf(sectionTreeNode);
                sectionTreeNode.getChildren().clear();
                childs.remove(sectionTreeNode);
                sectionTreeNode.setParent(null);
                sectionTreeNode = null;
                childs.remove(index);
            }
        }
    }

    public void editSection(){
        GuideSection section = getSelectedSection();

        updateSelectedGuide();
        for(GuideSection guideSection : selectedGuide.getGuideSections())
            if(guideSection.id.equals(section.id)) {
                guideSection.setTitle(section.getTitle());
                break;
            }

        saveGuide();
        editSectionTreeNode(section);
    }

    private void editSectionTreeNode(GuideSection section){
        if(treeNodesMap.containsKey(section.id)){
            TreeNode treeNode = treeNodesMap.get(section.id);
            ((TreeNodeModel)treeNode.getData()).setName(section.getTitle());
        }
    }

    private GuideScene getSelectedScene(){
        String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hidden:newScene");
        return new Gson().fromJson(value, GuideScene.class);
    }

    public void addScene(){

        GuideScene guideScene = getSelectedScene();
        updateSelectedGuide();
        selectedGuide.addGuideSceneToSection(guideScene);

        saveGuide();
        addSceneTreeNode("scene", guideScene.getGuideSectionId(), guideScene);
    }

    private void addSceneTreeNode(String type, String guideSectionId,GuideScene guideScene){
        if(treeNodesMap.containsKey(guideSectionId)){
            TreeNode sceneNode = new DefaultTreeNode(type, new TreeNodeModel(guideScene.id, guideSectionId , guideScene.getTitleMerged(), guideScene.getScene().getId(), guideScene.getScene().getThumbName(), guideScene.getOrder(), guideScene.getVisits(), guideScene.getAvgTime()), null);
            if(type.equals("child-guide-scene"))
                sceneNode.setSelectable(false);

            TreeNode parentTreeNode = treeNodesMap.get(guideSectionId);
            List<TreeNode> childs = parentTreeNode.getChildren();
            if(childs.size() == 0){
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("scene-" + guideSectionId));
                dropRegionNode.setType("dropRegion");
                childs.add(dropRegionNode);

                childs.add(sceneNode);

                DefaultTreeNode dropRegionNode1 = new DefaultTreeNode(new TreeNodeModel("scene-" + guideSectionId));
                dropRegionNode1.setType("dropRegion");
                childs.add(dropRegionNode1);
            }else{
                DefaultTreeNode dropRegionNode = new DefaultTreeNode(new TreeNodeModel("scene-section-" + guideSectionId));
                dropRegionNode.setType("dropRegion");
                for(TreeNode node:childs){
                    if(node.getType().equals("section") || node.getType().equals("child-guide-root-section")){
                        int index = childs.indexOf(node);
                        DefaultTreeNode dr = new DefaultTreeNode(new TreeNodeModel("scene-" + guideSectionId));
                        dr.setType("dropRegion");
                        childs.set(index-1, dr);
                        childs.add(index, sceneNode);
                        childs.add(index + 1, dropRegionNode);
                        return;
                    }
                }
                childs.add(sceneNode);
                childs.add(dropRegionNode);
            }
        }
    }

    public void deleteScene(){

        GuideScene gs = getSelectedScene();
        updateSelectedGuide();
        selectedGuide.removeGuideScene(gs.getScene());

        saveGuide();
        deleteSceneTreeNode(gs,gs.getGuideSectionId());
    }

    private void deleteSceneTreeNode(GuideScene guideScene, String guideSectionId){
        if(treeNodesMap.containsKey(guideSectionId)){
            TreeNode parentTreeNode = treeNodesMap.get(guideSectionId);
            for(TreeNode node : parentTreeNode.getChildren()){
                TreeNodeModel nodeModel = (TreeNodeModel)node.getData();
                if(node.getType().equals("scene") && nodeModel.getId().equals(guideScene.getId())){
                    List<TreeNode> childs = parentTreeNode.getChildren();
                    if(childs.size() == 3){
                        childs.clear();
                    }else{
                        int index  = childs.indexOf(node);
                        childs.remove(index-1);
                        childs.remove(node);
                        node.setParent(null);
                        node = null;
                    }
                    break;
                }
            }
        }
    }

    public void updateSceneMove(){
        Map<String, String> paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String oldParentSectionId = paramMap.get("oldParentSectionId");
        String scene = paramMap.get("scene");

        GuideScene guideScene = new Gson().fromJson(scene, GuideScene.class);
        updateSelectedGuide();
        selectedGuide.addGuideSceneToSection(guideScene);

        saveGuide();
        updateSceneMoveTreeNode(guideScene, oldParentSectionId);
    }

    private void updateSceneMoveTreeNode(GuideScene guideScene, String oldParentSectionId){
        deleteSceneTreeNode(guideScene, oldParentSectionId);
        addSceneTreeNode("scene", guideScene.getGuideSectionId(), guideScene);
    }

    public void storeInputOutputHotspots(){
        List inputs = gameTaskHotspots.getTarget();
        String input = new Gson().toJson(inputs);
        selectedTask.input = input;

        List outputs = gameTaskHotspotsToShow.getTarget();
        String output = new Gson().toJson(outputs);
        selectedTask.output = output;

    }

    public void storeMultiChoices(){
        List inputs = gameChoices;
        String input = new Gson().toJson(inputs);
        selectedTask.input = input;


    }



    public void saveSelectedTask(){
        if(selectedTask.code.equals("findhotspots"))
            storeInputOutputHotspots();

        if(selectedTask.code.equals("multichoice"))
            storeMultiChoices();

        gameTaskService.saveGameTask(selectedTask);
       updateSelectedGuideScene();
    }

    public void saveGuide(){
        guideService.saveGuideDetail(selectedGuide);

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("refreshData()");
    }

    public void onGameTaskReOrder() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("reorderGameTasks()");
    }

    public void reorderGameTasks()throws JSONException {
        String json = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hidden:updatedTaskOrder");

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(json);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }

        for(GameTask task : selectedGuide.getGameTasks())
            task.order = Integer.parseInt(map.get(task.id.toString()));


        guideService.saveGuideDetail(selectedGuide);
    }


    public void setSelectedGuideScene(GuideScene selectedGuideScene) {
        this.selectedGuideScene = selectedGuideScene;
    }

    public GameTask getSelectedTask() {

        if(selectedTask == null)
            return null;

        if(selectedTask.code.equals("findhotspots")) {
            selectedGuideSceneHotspotsToFind = loadSelectedGuideSceneHotspotsData(0);
            taskHotspotsToFind = loadTaskHotspots(0);
            selectedGuideSceneHotspotsToShow = loadSelectedGuideSceneHotspotsData(1);
            taskHotspotsToShow = loadTaskHotspots(1);
        }
        else if (selectedTask.code.equals("multichoice")) {

            gameChoices = new Gson().fromJson(selectedTask.input, new TypeToken<ArrayList<GameChoice>>() {}.getType());
        }

        return selectedTask;
    }

    public void setSelectedTask(GameTask selectedTask) {
        this.selectedTask = selectedTask;
    }

   public  void deleteSelectedTask(){
       gameTasks.remove(selectedTask);
       selectedGuide.removeGameTask(selectedTask);
       gameTaskService.deleteGameTask(selectedTask);
       updateSelectedGuideScene();
   }


    public boolean getGuideFont() {
        return selectedGuide.font != 0;
    }

    public void setGuideFont(boolean checked) {
        selectedGuide.font = checked ? 1 : 0;
    }


    public co.beek.pano.model.beans.DataTable getPerformance() throws Exception {

        BasicData data = guideReport.getBasicData();

        co.beek.pano.model.beans.DataTable table = new co.beek.pano.model.beans.DataTable();

        if(data == null)
            return null;

        table.addRow("Total Visitors", NumberFormat.getIntegerInstance().format(data.getVisits()));
        table.addRow("Total Scenes",
                NumberFormat.getIntegerInstance().format(data.getPageViews()));
        table.addRow( "Avg Time",
                DateUtil.timeConversion((int) data.getAvgTime()));
        table.addRow(
                "Avg Scenes",
                String.format("%.2f",data.getAvgScenes()));


        return table;
    }


    public String getCitiesData(){

        List<List<Object>> list = new ArrayList<List<Object>>();
        int counter = 0;

        for(Country visit : guideReport.getCountriesList()){

            if(visit.getSessions() < 2 || visit.getCity().endsWith("(not set)"))
                continue;

            ArrayList<Object> city = new ArrayList<Object>();
            city.add(visit.getCity() + ", " + visit.getCountry());
            city.add(visit.getSessions());
            city.add("Total Visits: " + visit.getSessions() + "<br/>Avg Scenes: " + visit.getAvgSessionDurationString() + "<br/>Avg Time: " + visit.getPageViewsPerSessionString());
            list.add(city);

            counter++;
        }

        JSONArray JSONArray = new JSONArray(list);
        return JSONArray.toString();
    }

    public String getSourceData() {

        if(getReferals() == null)
            return null;

        List<Referral> referralSources = new ArrayList<Referral>();
        if(referrals.size() > 0 )
            referralSources.add(referrals.get(0));

        for(Referral referal : referrals)
            for(int i=0;i<referralSources.size();i++) {

                if(referal.getSource().equals(referralSources.get(i).getSource())){
                    referralSources.get(i).setSessions(referal.getSessions() + referralSources.get(i).getSessions());
                    break;
                }
                else if(i == referralSources.size() - 1)
                    referralSources.add(referal);

            }

        List<List<Object>> list = new ArrayList<List<Object>>();

        for(Referral referal : referralSources){

            if(referal.getSessions() < 2 )
                continue;

            ArrayList<Object> referals = new ArrayList<Object>();
            referals.add(referal.getSource());
            referals.add(referal.getSessions());

            list.add(referals);
        }

        JSONArray JSONArray = new JSONArray(list);
        return JSONArray.toString();
    }

    public List<Referral> getReferals() {
        if(guideReport.getReferralsList() == null)
            return null;

        referrals = new ArrayList<Referral>();

        for (Referral referal : guideReport.getReferralsList()) {

            if (referal.wasFrom("localhost") || referal.wasFrom("beektest")
                    || referal.wasFrom("beekdev")  || referal.wasFrom("beekdev"))
                continue;

            referrals.add(referal);
        }

        return referrals;
    }

    public String getFlashVarsJSON() {
        System.out.println(flashVars);
        return serializer.serialize(flashVars);
    }

    public List<String> getUsersEmails() {
        return usersEmails;
    }

    public void setUsersEmails(List<String> usersEmails) {
        this.usersEmails = usersEmails;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void addUser() {
        EWContactsResponse ewContactsResponse = null;

        try {
            ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), emailAddress);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //use this for if it comes back null
        }
        if (ewContactsResponse.getContacts().size() == 0)
        {
            try {
                ewContactsResponse = ewService.addGuideUserByEmail(session.getUsername(), session.getPassword(), emailAddress, selectedGuide.getId());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //use this for if it comes back null
            }
        }

        else
        {
            try {
                ewContactsResponse = ewService.updateGuideID(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type,  selectedGuide.getId());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //use this for if it comes back null
            }
        }

        getGuideUsers(selectedGuide.getId());
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("tabView:users");
    }



    public void removeUser(String email) {
        EWContactsResponse ewContactsResponse = null;
        EWContactsResponse ewContactsResponse1 = null;
        try {
            ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), email);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //use this for if it comes back null
        }
        if (ewContactsResponse.getContacts().size() != 0)
            try {
                ewContactsResponse1 = ewService.removeGuideUserByEmail(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type, selectedGuide.getId());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //use this for if it comes back null
            }

        getGuideUsers(selectedGuide.getId());
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("tabView:users");
        //reloadToUserTab();
    }

    public void getGuideUsers(String guideid) {
        EWContactsResponse ewContactsResponse = null;
        try {
            ewContactsResponse = ewService.getGuideUsers(session.getUsername(), session.getPassword(), guideid);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        users = ewContactsResponse.getContacts();
        usersEmails = new ArrayList<String>();
        for (int i = 0; i < ewContactsResponse.getContacts().size(); i++)
        {
            String temp =ewContactsResponse.getContacts().get(i).email;
            usersEmails.add(temp);
        }

    }

    public ArrayList<GameTaskHotspot> getTaskHotspotsToShow() {
        return taskHotspotsToShow;
    }

    public void setTaskHotspotsToShow(ArrayList<GameTaskHotspot> taskHotspotsToShow) {
        this.taskHotspotsToShow = taskHotspotsToShow;
    }

    public ArrayList<GameTaskHotspot> getSelectedGuideSceneHotspotsToFind() {
        return selectedGuideSceneHotspotsToFind;
    }

    public void setSelectedGuideSceneHotspotsToFind(ArrayList<GameTaskHotspot> selectedGuideSceneHotspotsToFind) {
        this.selectedGuideSceneHotspotsToFind = selectedGuideSceneHotspotsToFind;
    }

    public ArrayList<GameTaskHotspot> getTaskHotspotsToFind() {
        return taskHotspotsToFind;
    }

    public void setTaskHotspotsToFind(ArrayList<GameTaskHotspot> taskHotspotsToFind) {
        this.taskHotspotsToFind = taskHotspotsToFind;
    }

    public ArrayList<GameTaskHotspot> getSelectedGuideSceneHotspotsToShow() {
        return selectedGuideSceneHotspotsToShow;
    }

    public void setSelectedGuideSceneHotspotsToShow(ArrayList<GameTaskHotspot> selectedGuideSceneHotspotsToShow) {

        this.selectedGuideSceneHotspotsToShow = selectedGuideSceneHotspotsToShow;
    }

    public ArrayList<String> getMultichoice() {
        return multichoice;
    }

    public void setMultichoice(ArrayList<String> multichoice) {
        this.multichoice = multichoice;
    }

    public String getMultichoiceOption() {
        return multichoiceOption;
    }

    public void setMultichoiceOption(String multichoiceOption) {
        this.multichoiceOption = multichoiceOption;
    }

    public void addMultiChoiceOption(){
        multichoice.add(multichoiceOption);


    }
    public void clearSelectedTaskInputAndOutput(){
        selectedTask.input = null;
        selectedTask.output = null;
    }


    public GameChoice getGameChoiceOption() {
        return gameChoiceOption;
    }

    public void setGameChoiceOption(GameChoice gameChoiceOption) {
        this.gameChoiceOption = gameChoiceOption;
    }

    public ArrayList<GameChoice> getGameChoices() {
        return gameChoices;
    }

    public void setGameChoices(ArrayList<GameChoice> gameChoices) {
        this.gameChoices = gameChoices;
    }

    public void addGameChoice(){
        GameChoice choice = new GameChoice();
        choice.setTitle(multichoiceOption);
        choice.setCorrect(false);
        choice.setOrder(gameChoices != null ? gameChoices.size() : 0);

        if(gameChoices == null)
            gameChoices = new ArrayList<GameChoice>();

        gameChoices.add(choice);
        storeMultiChoices();
    }

    public void removeGameChoice(GameChoice choice){

        gameChoices.remove(choice);
        storeMultiChoices();
    }

    public Boolean showFindHotspotDialogue(){
        return selectedTask.code.equals("findhotspots");
    }

    public class TreeNodeModel{
        private String id;
        private String parentId;
        private String name;
        private String thumb;
        private String sceneId;
        private int visits;
        private double avgTime;
        private int order = -1;

        public TreeNodeModel(String parentId) {
            this.parentId = parentId;
        }

        public TreeNodeModel(String id, String parentId, String name, String sceneId, String thumb, int order, int visits, Double avgTime) {
            this.id = id;
            this.parentId = parentId;
            this.name = name;
            this.sceneId = sceneId;
            this.thumb = thumb;
            this.order = order;
            this.visits = visits;
            this.avgTime = avgTime;
        }



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        public String getSceneId() {
            return sceneId;
        }

        public void setSceneId(String sceneId) {
            this.sceneId = sceneId;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getAvgTime() {
            return DateUtil.timeConversion(Integer.valueOf((int)Math.round(avgTime)));
        }

        public void setAvgTime(Double avgTime) {
            this.avgTime = avgTime;
        }

        public String getVisits() {
            return NumberFormat.getIntegerInstance().format(visits);
        }

        public void setVisits(int visits) {
            this.visits = visits;
        }


    }
}


