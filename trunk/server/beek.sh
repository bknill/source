#!/bin/bash

JAVA=`which java`

cd /srv/beek/dist

ulimit -v unlimited
ulimit -n 16384
ulimit -aH > /srv/beek/dist/logs/limits.log

VERSION_PROPERTIES=/etc/beek/version.properties
BUILD_PROPERTIES=/srv/beek/dist/WEB-INF/properties/build.properties


function update_build_properties
{
  if [ -f "${VERSION_PROPERTIES}" ]; then

    if [ -f "${BUILD_PROPERTIES}" ]; then

      # Update the buildVersion property with the property value in /etc/beek/version.properties
      SWFS_VERSION=`cat ${VERSION_PROPERTIES}|cut -f2 -d'='`
      sed -i -e "s/^buildVersion.*=.*/buildVersion = $SWFS_VERSION/" ${BUILD_PROPERTIES}

    fi

  fi
}


# Update any build properties values prior to running the beek service
update_build_properties


# JAVA arguments
# JAVA_ARGS=-Xrs -Xms512M -Xmx768M -Xss128K -XX:PermSize=128M -XX:MaxPermSize=256M
$JAVA -jar UploadServer.jar

