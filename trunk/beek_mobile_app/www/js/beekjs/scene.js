	var sceneReady = false,
		firstRun = true,
		sceneLoaded = false,
		loadingSceneId,
		currentScene,
		boards,
		bubbles,
		historyScenes = [],
		hemiLight,
		hsLight,
		lightValue,
		lightsOn = 0.75,
		deepTileCounter,
		queuedTiles,
		sceneId;
		//sceneDefaultQuaternion = new THREE.Quaternion();
	
	function getScene(id) {
		console.log('scene.getScene('+id+')' );

		console.log(id, loadingSceneId);

		if(id == loadingSceneId)
			return;

		if(isOffline()){
			var result = $.grep(storedSceneData, function(e){ return e.name == "s"+id+".json"; });
			if (result.length == 0) {
				console.log('no scene data');
			} else if (result.length == 1) {
				readFile(result[0],function(data){
					var json = JSON.parse(data);
					startScene(json.payload)
				});
			} else {
				console.log('too much scene data');
			}
		}
		else{
			var jsonpURL = jsonpPrefix + '/scene/' + id + '/jsonp?callback=?';
			$.getJSON(jsonpURL, $.proxy(startScene, this));
		}
	    
	    sceneId = loadingSceneId = id;

		if(!firstRun)
			showLoader();
	};
	
	
	function startScene(sceneObject) {
		console.log('scene.startScene()' );

		currentScene = sceneObject;

		sceneLoaded = false;

		historyScenes.push( sceneObject.id );

		
		if(sceneObject);
			$.address.value('g' + guideId + '/s' + sceneObject.id);

		if(currentScene.video != null)
			panovideo(currentScene);
		//if offline any photo scenes should load as complete jpgs
		else if(isOffline()) {

			var file = sceneObject.panoPrefix + "_EX.jpg";

			if(!localStoragePath)
				createStorageDirectory(function(){
					loadSphereTexture(localStoragePath + file);
				});
			else
				loadSphereTexture(localStoragePath + file);

		}
		// if online load tiles
		else{
			//get tiles
			tiles = [];
			materials = [];
			previewTileCounter = 0;
			for (var face in FACES) {
				materials.push(loadTexture(cdnPrefix + '/scene_' + sceneObject.id + "_pano_" +
				sceneObject.panoIncrement + "_" + FACES[face] + "/9/0_0.jpg" ));
				for (var tile in TILES) tiles.push(
					cdnPrefix + '/scene_' + sceneObject.id + "_pano_" + sceneObject.panoIncrement + "_" + FACES[face] + "/10/" + TILES[tile] + ".jpg"
				);
			}
		}

		processHotspots(sceneObject);
		updateGuideScene(currentScene.id);


	} 
	
	function processHotspots(sceneObject){

		console.log("hotspots");
		//get bubbles
        bubbles = [];
        for (ix in sceneObject.bubbles) 
        	bubbles.push(sceneObject.bubbles[ix]);

        //get boards
        boards = [];
        for (ix in sceneObject.boards) 
            boards.push(sceneObject.boards[ix]);
        
        //get photos
        photos = [];
        for (i in sceneObject.photos) 
            photos.push(sceneObject.photos[i]);
        
        //get posters
        posters = [];
        for (i in sceneObject.posters) 
            posters.push(sceneObject.posters[i]);
        
        //get sounds
        sounds = [];
        for (i in sceneObject.sounds) 
            sounds.push(sceneObject.sounds[i]);

	}

	
	function clearScene() {
		console.log('scene.clearScene()');

		//remove everything from the scene and reset variables
		//setLights(0);

		if(deviceControls)
			deviceControls.disconnect();
		
		selectedHotspot = null;
		viewingHotspot = null;
		targetList = [];
		
		for(i in sceneSounds)
			sceneSounds[i].source.stop();
		
		sceneSounds = [];


		if(sphere)
			sphere = null;

		if(box)
			box = null;

		var obj, i,o;
		for ( i = scene.children.length - 1; i >= 0 ; i -- ) {
		    obj = scene.children[ i ];
		    if ( obj !== projector && obj !== camera) {
		    	if(obj.hotspotData)
				{
		    		   for ( o = obj.children.length - 1; o >= 0 ; o -- ) 
		    			   obj.remove(obj.children[o]);
   				}

		         scene.remove(obj);
		    }
		}

	   
		if(closeButton)
			removeCloseButton();
	};
	
	function createBox(){
		console.log("scene.createBox()");
		//the inital pano cube
	    box = new THREE.Mesh(new THREE.BoxGeometry(1024,1024,1024, 20, 20, 10,10), new THREE.MeshFaceMaterial(materials));
	    box.scale.x = -1;
	    scene.add(box);
	}

	function createSphere(material){
		console.log("scene.createSphere()");
        console.log(material);
		var geometry = new THREE.SphereGeometry( 500, 60, 40 );
		sphere = new THREE.Mesh( geometry, material );
		sphere.scale.x = -1;
		sphere.rotation.y = Math.PI / 2;

		scene.add(sphere);

	}


	function loadTexture(path) {
	
	    var texture = new THREE.Texture(texture_placeholder);
	    var material = new THREE.MeshBasicMaterial({
	        map: texture,
	        overdraw: 0.5
	    });
	
	    var image = new Image();
	    image.crossOrigin = '';
	    
	    image.onload = function() {

	        texture.image = this;
	        texture.needsUpdate = true;
	        previewTileCounter ++;

	        if(previewTileCounter == 6){
	        	sceneReady = true;
	        	loadScene();
				createBox();
				loadTiles(tiles);
	        }
	    };
	    image.src = path;
	
	    return material;
	
	}

	function loadSphereTexture(path) {

		console.log(path);

		var texture = new THREE.Texture(texture_placeholder);

		texture.generateMipmaps = false;
		texture.magFilter = THREE.LinearFilter;
		texture.minFilter = THREE.LinearFilter;

		texture = THREE.ImageUtils.loadTexture(path, {} ,function(){
			sceneReady = true;
			loadScene();
			createSphere(material);

		});


		var material = new THREE.MeshBasicMaterial({
			map: texture,
			overdraw: 0.5
		});



	}
	
	function setDefaults(){
		console.log('scene.setDefaults()');
		data.longitude = currentScene.pan+90;
		data.latitude = currentScene.tilt;
		camera.fov = 80;//currentScene.fov > 0 ? currentScene.fov * 0.6 : 100;
		
		camera.updateProjectionMatrix();


		if(deviceControls){
			deviceControls.pan = data.longitude;
			deviceControls.connect();
			data.manualControl = false;
		}


		sceneReady = false;
    	isUserInteracting = false;
	}
	
	function loadScene(){
		
		console.log('scene.loadScene()');
		selectedHotspot = null;

    	TWEEN.removeAll();

		$( document ).trigger( "scene_loaded" );

		if(!firstRun)
			clearScene();
		else
			setUp();
    	
		if(currentScene.id != sceneId && !$('#prevButton').length && !gameInProgress)
			prevButton();
		else if (currentScene.id == sceneId)
			removePrevButton();

		setDefaults();
		hideLoader();
		addLights();
		hotspots();

		sceneLoaded = true;

	}
	

	
   function loadTiles(tiles){
	   console.log("scene.loadTiles");
	   deepTileCounter = 0;
	   
	   var face_b = new THREE.Object3D();
	   	   face_b.position.z = -511;
	   	   face_b.receiveShadow = true;
	       face_b.frustumCulled = true;
	   	   scene.add(face_b);
	   var face_f = new THREE.Object3D();
   	   	   face_f.position.z = 511;
   	   	   face_f.rotation.y = 180;
	   	   face_f.receiveShadow = true;
	       face_f.frustumCulled = true;
	   	   face_f.lookAt( camera.position );
   	   	   scene.add(face_f);
	   var face_l = new THREE.Object3D();
	   	   face_l.position.x = 511;
	  	   face_l.rotation.y = -90;
	   	   face_l.receiveShadow = true;
	       face_l.frustumCulled = true;
	   	   face_l.lookAt( camera.position );
	   	   scene.add(face_l);
	   var face_r = new THREE.Object3D();
  	   	   face_r.position.x = -511;
	  	   face_r.rotation.y = 90;
	   	   face_r.receiveShadow = true;
	       face_r.frustumCulled = true;
	   	   face_r.lookAt( camera.position );
	  	   scene.add(face_r);
		var face_u = new THREE.Object3D();
			face_u.position.y = 511;
	   	   	face_u.lookAt( camera.position );
	   	   face_u.receiveShadow = true;
	       face_u.frustumCulled = true;
	   	   face_u.rotation.z = THREE.Math.degToRad(180);
			scene.add(face_u);
		var face_d = new THREE.Object3D();
			face_d.position.y = -511;
	   	   	face_d.lookAt( camera.position );
			face_d.rotation.z = THREE.Math.degToRad(180);
	   	    face_d.receiveShadow = true;
		    face_d.frustumCulled = true;
			scene.add(face_d);
	   
	   for(var tile in tiles){
		   
		   var image = new Image();
		   image.crossOrigin = '';
	       image.src = tiles[tile];
	       image.group = image.src.charAt(image.src.length - 12);
	       image.vertical = image.src.charAt(image.src.length - 7);
	       image.horizontal = image.src.charAt(image.src.length - 5);
	       
		   image.onload = function() {
			   
			   	var texture = new THREE.Texture(texture_placeholder);
	            texture.image = this;
	            texture.needsUpdate = true;
		        var material = new THREE.MeshLambertMaterial({
		            map: texture
		        });
			   
		        material.side = THREE.DoubleSide;
		        material.receiveShadow = true;
		        
	            var geometry = new THREE.PlaneBufferGeometry(512,512);
	                geometry.receiveShadow = true;
			    var planeMesh = new THREE.Mesh( geometry, material );
			    	planeMesh.receiveShadow = true;
			    	
			    if(this.horizontal == 0)
			    	planeMesh.position.y += 256;
			     if(this.horizontal == 1)
			    	planeMesh.position.y -= 256;
			     if(this.vertical == 0)
			    	planeMesh.position.x -= 256;
			     if(this.vertical == 1)
			    	planeMesh.position.x += 256;
			    
			    
			    if(this.group == 'b'){				    
				    face_b.add(planeMesh);
			    }else if(this.group == 'f'){
			    	face_f.add(planeMesh);	
			    }else if(this.group == 'l'){
			    	face_l.add(planeMesh);	
			    }else if(this.group == 'r'){
			    	face_r.add(planeMesh);	
			    }else if(this.group == 'u'){
			    	face_u.add(planeMesh);	
			    }else if(this.group == 'd'){
			    	face_d.add(planeMesh);	
			    }
			    
			    
			    deepTileCounter ++;
			    if(deepTileCounter == 16 && !videoPlaying && !connected && !deviceControls)
			    	setTimeout(autoplay,1000, getLinks() );
		   };
	   };
	   

   }
   
   function delayTileLoad(){
	   loadTiles(queuedTiles);
	   queuedTiles = null;
	   
   }
   
   
   function setUp(){
	   console.log('scene.setUp()');
       $("#preloader").remove();
       //give it a moment to organise tiles before guide load
		if(gameInProgress)
			updateGameTask(gameTasks[0]);
		else{

			nextButton();			
		}
	   openGuideSceneButtons();

		firstRun = false;
   }
 
   
   function addLights(){
   
	   console.log('scene.addLights()');
	   
	   hsLight = new THREE.SpotLight( 0xffffff,0,500 );
	   hsLight.position.set(0, 0, 300);
	   camera.add( hsLight );
	   scene.add( camera );
	
		hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.8 );
		scene.add( hemiLight);
		
		var ambiLight = new THREE.AmbientLight( 0x404040 ); // soft white light
		scene.add( ambiLight );
	   
   }
   
   function setLights(value){
	   console.log("setLights(" + value + ")" );

	    lightValue = value;
	   
	   if( hemiLight )
		   var tween = new TWEEN.Tween( hemiLight ).to( {intensity: value}, 1000 ).start();

   }
   