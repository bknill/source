var hs_bubble, 
	refractSphereCamera,
	selectedHotspot,
	viewingHotspot,
	hotspotLat,
	hotspotLon,
	closeButton = false,
	group,
	photos,
	posters,
	sounds,
	hiddenHotspots = [];



	function hotspots(){

        for(var b in bubbles)
    	  if(!hotspotHidden(bubbles[b], 'bubble' ))
        	bubble(bubbles[b]);
        
        for(var b in boards)
      	  if(!hotspotHidden(boards[b], 'board' ))
        	board(boards[b]);
        
        for(var p in photos)
    	  if(!hotspotHidden(photos[p], 'photo' ))
		    createPhoto(photos[p]);
        
        for(var p in posters)
    	  if(!hotspotHidden(posters[p], 'poster' ))
        	createPoster(posters[p]);
        
        for(var s in sounds)
    	  if(!hotspotHidden(sounds[s], 'sound' ))
        	createSound(sounds[s]);

		console.log("HotspotsComplete");
	}
	
	function hotspotHidden( hotspot, type ){
		
		  if(hotspotsToHide.indexOf( hotspot.id ) < 0 )
			  return false;
		  else{
			  hotspot.type = type;
			  hiddenHotspots.push( hotspot );
		  }
		  
		  return true;
	}
	
	function showHiddenHotspot( hotspot ){

    	  if( hotspot.type == 'bubble')
        	bubble(hotspot);
        
    	  if( hotspot.type == 'board')
        	board(hotspot);
        
    	  if( hotspot.type == 'photo')
		    createPhoto(hotspot);
        
    	  if( hotspot.type == 'poster')
        	createPoster(hotspot);
        
    	  if( hotspot.type == 'sound')
        	createSound(hotspot);
	}
	
	function textSprite(text,size,color,italics) {
	    
		var font = "Lato";
	    padding = 30;

	    font = size + "px " + font;

	    var canvas = document.createElement('canvas');
	    var context = canvas.getContext('2d');
	    context.font = font;

	    var metrics = context.measureText(text),
	        textWidth = metrics.width;

	    canvas.width = textWidth + 3;
	    canvas.height = size + 20;

	    context.font = font;
	    context.fillStyle = color;
	    context.fillText(text, 0, size + 3);
//	    context.shadowColor = '#999';
//	    context.shadowBlur = 20;
//	    context.shadowOffsetX = 15;
//	    context.shadowOffsetY = 15;

	    var texture = new THREE.Texture(canvas);
	    texture.needsUpdate = true;
	    
	    var mesh = new THREE.Mesh(
	    new THREE.PlaneBufferGeometry(canvas.width, canvas.height),
		    new THREE.MeshBasicMaterial({
		    	map: texture,
		    	transparent: true
		    })
	    );	
		
	    return mesh;
	}
	
	
	function hotspotClick(object){
		console.log('hotspots.hotspotClick()');
		
		
		//if iFrame go fullscreen
		//if(inIframe() && !fullscreen)
		//	launchFullscreen(document.documentElement);
		
	
		if(object.parent.hotspotData)
			selectedHotspot = object.parent;
		else
			selectedHotspot = object;
		
		selectedHotspot.castShadow = true;
		selectedHotspot.receiveShadow = true;
		isUserInteracting = false;
		

		//sort bubbles
		if(selectedHotspot.hotspotData)
		if(selectedHotspot.hotspotData.targetSceneId){
			object.material.opacity = 0.5;
			tweenToSelectedHotspot(selectedHotspot, function(){checkForSceneToLoad()}, true );
			getScene(selectedHotspot.hotspotData.targetSceneId);
			trackEvent( 'hotspot', 'bubble', guideId+'|'+ selectedHotspot.hotspotData.targetSceneId +'|' +currentScene.id );
			
		}
		//should be signboards after the board has been selected
		else if(object.hotspotData && viewingHotspot){
			object.material.opacity = 0.75;
			getScene(object.hotspotData.scene.id);
			trackEvent( 'hotspot', 'board', guideId+'|'+ object.hotspotData.scene.id +'|' +currentScene.id );
			selectedHotspot = null;
		}
		//everything else tweens
		else if(!viewingHotspot){
			//if unselectable tween camera towards hotspot then zoom
			if(selectedHotspot.hotspotData.selectable == false){
				$( document ).trigger( "hotspot" );
				//tweenToSelectedHotspot(selectedHotspot, function(){tweenHotpotToCamera( selectedHotspot ); if(selectedHotspot.hotspotData.buttonSceneId) checkForSceneToLoad(); }, true );
				if(selectedHotspot.hotspotData.buttonSceneId != null)
					getScene(selectedHotspot.hotspotData.buttonSceneId);
				else
					selectedHotspot = null;
			}
			else
				tweenHotpotToCamera( selectedHotspot );

			
			trackEvent( 'hotspot', 'media', guideId+'|'+ selectedHotspot.hotspotData.id +'|' +currentScene.id );
			
		}
		

	}
	
	function tweenHotpotToCamera(object){
		console.log('hotspots.tweenHotpotToCamera('+ object.hotspotData.title +')');
		
		selectedHotspot = object;
		
		//bubbles or unselectable content just tween towards
		if(selectedHotspot.hotspotData.targetSceneId || selectedHotspot.hotspotData.selectable == false){
			new TWEEN.Tween(  camera ).to({fov : object.hotspotData.distance*0.05}, 1000).easing(TWEEN.Easing.Sinusoidal.InOut)
				.onUpdate(function(){
					camera.updateProjectionMatrix();
				}).start();
			return;
		}

       
		//let's copy original position and rotation
		object.originalPosition = object.position.clone();
		object.originalQuaternion = object.quaternion.clone();

		//magic maths to get objects around the same size of the screen
		var vFOV = camera.fov * Math.PI / 180; 
		var pLocal,cPos;
		var bbox=getCompoundBoundingBox( object );
		var sizeY = bbox.max.y-bbox.min.y;
		var sizeX= bbox.max.x-bbox.min.x;
		sizeX*=object.scale.x;
		sizeY*=object.scale.y;

		// ALEX
		// assuming FOV is vertical
		var ratio=window.innerWidth/window.innerHeight;
		var uSpace=1/0.8;//used space
		vFOV/=2;
		sizeY/=2;sizeX/=2;// half size 
		sizeY*=uSpace;sizeX*=uSpace;
		var mode=0;
		if(object.hotspotData.description)
		{
			if(ratio>=1)//landscape
			{
				sizeX*=1/0.6;
				mode=1;
			}else
			{
				sizeY*=1/0.6;
				mode=2;
			}
		}
		var tanFov=Math.tan( vFOV );
		var distY=sizeY/tanFov;
		var distX=sizeX/(ratio*tanFov);
		if(distX<distY)distX=distY;
		pLocal= new THREE.Vector3( 0, 0, -distX );
		
		cPos = camera.position.clone();
		// offset to the 
		switch(mode)
		{
		case 1:{
				var W2=distX*(ratio*tanFov);
				W2*=0.4;
				pLocal.x-=W2;
				cPos.x-=W2;
			   }break;
		case 2:{
				var H2=distX*tanFov;
				H2*=0.4;
				pLocal.y=H2;
				cPos.y+=H2;
			   }break;
		}

		//apply the direction the camera is facing
		var target = pLocal.applyMatrix4( camera.matrixWorld );
		var targetLook = cPos.applyMatrix4( camera.matrixWorld );
		
		object.position.copy(target);
		object.lookAt(targetLook);
		var targetRotation=object.quaternion.clone();
		object.position.copy(object.originalPosition);
		object.quaternion.copy(object.originalQuaternion);

		var tweenMove = new TWEEN.Tween(object.position).to(target, 1500).easing(TWEEN.Easing.Cubic.InOut);
        setLights(0.5);
        lightsOn=0;// disable restoring in onmouse down handler

        hsLight.target = object;
		//var newInt = distX/12;
        var newInt=0.8;// play with this value
        hsLight.intensity=0.0;// interpolate from 0 to newInt
		tween = new TWEEN.Tween( hsLight ).to( {intensity: newInt }, 1000 ).start();
        

		tweenMove.onUpdate(function(){
			var dstF=object.originalPosition.distanceTo(target);
			var dstC=object.originalPosition.distanceTo(object.position);
			var k=dstC/dstF;
			THREE.Quaternion.slerp(object.originalQuaternion,targetRotation,object.quaternion,k);//
		});
		tweenMove.onComplete(function(){
			  
              lightsOn = 0.75,
			  
			 addCloseButton();
			
			if(object.hotspotData.description){
				//setLights(0);
				createHotspotText( object, sizeX );
			}
				
		});
		tweenMove.start();
		object.rotationAmount=0;


	//	if(gameInProgress)
	//		$( "#gameContainer" ).fadeTo(200,0);
		
		
		//posters with icons
		if(object.hotspotData.showIcon){
			object.children[0].visible = false;
			object.children[1].visible = true;
		}

		viewingHotspot = object;
	}
	

	
	function tweenToSelectedHotspot(object , callback, zoom, auto){
		console.log('tweenToSelectedHotspot('+ object.hotspotData.title +')');

		
		var newLatLon = {lat: lat, lon : lon};
		var targetLatLon = hotspotPanTiltToLatLon(object.hotspotData.pan,object.hotspotData.tilt );
		var newFov = {nFov : camera.fov};
		var targetFov = {nFov : object.hotspotData.distance*0.03};


		//if its been spun around a lot this number gets big
		if(Math.abs(newLatLon.lon) > 360){
			var f = newLatLon.lon/360;
			f = f - Math.floor(f);
			newLatLon.lon = f * 360;
		}
		

		
		if(newLatLon.lon < 0)
			newLatLon.lon += 360;
		
		if(Math.abs(newLatLon.lon - targetLatLon.lon) > 180)
			targetLatLon.lon += targetLatLon.lon > 360 ? -360 : 360;

		var diff = newLatLon.lon - targetLatLon.lon;	
		

        var time = auto == true ? Math.abs(diff) *  150 : Math.abs(diff) * 100;

		var loadsScene = selectedHotspot.hotspotData.targetSceneId != null;

		//set up zoom tween
		var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, time ).easing(TWEEN.Easing.Cubic.Out);
			tweenZoom.onUpdate(function(){
				camera.fov = newFov.nFov;
				camera.updateProjectionMatrix();
			});
		
		//tween lon/lat to tween camera
		var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, time).easing(TWEEN.Easing.Cubic.Out);;
			tweenLon.onUpdate(function(){
				lon = newLatLon.lon;
				lat = newLatLon.lat;

				if(loadsScene && sceneReady)
					loadScene();
				else if( !loadsScene && lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5 && zoom)
					tweenZoom.start();
				});
			tweenLon.onComplete(function(){if(callback)callback.call(); $( document ).trigger( "hotspot" )});
			tweenLon.start();

	}
	
	function checkForSceneToLoad(){
		console.log('checkForSceneToLoad()');
		if(sceneReady)
			loadScene();
		else if(selectedHotspot)
			setTimeout(checkForSceneToLoad,100);
	}
	
	function deselectHotspot()
	{
		console.log(' deselectHotspot() ' );
		if(!selectedHotspot)
			return;

		if(selectedHotspot.hotspotData.selectable)
			$( document ).trigger( "hotspot" );
		
		var shs = selectedHotspot;
		selectedHotspot = null;
		viewingHotspot = null;
		

		var srcRotation=shs.quaternion.clone();
		//var targetRotation=shs.originalQuaternion.clone();
		var srcPos=shs.position.clone();

		var tweenMove=new TWEEN.Tween(shs.position).to(shs.originalPosition, 1000).easing(TWEEN.Easing.Cubic.InOut);
		tweenMove.onComplete(function(){
			//turn posters back into icons
			if(shs){
				if(shs.hotspotData.showIcon){
					shs.children[0].visible = true;
					shs.children[1].visible = false;
				};
					// just in case - avoid error
					shs.quaternion.copy(shs.originalQuaternion);
					shs.position.copy(shs.originalPosition);
				}
			});
		
		tweenMove.onUpdate(function(){
			var dstF=srcPos.distanceTo(shs.originalPosition);
			var dstC=srcPos.distanceTo(shs.position);
			var k=dstC/dstF;
			THREE.Quaternion.slerp(srcRotation,shs.originalQuaternion,shs.quaternion,k);//
		});
		tweenMove.start();
	
/*		var zoomFov = currentScene.fov * 0.8 > 0 ? currentScene.fov * 0.8 : 80;
		
		new TWEEN.Tween(  camera ).to({fov : zoomFov }, 1000).easing(TWEEN.Easing.Sinusoidal.InOut)
				.onUpdate(function(){
					camera.updateProjectionMatrix();
				}).start();*/

		
    	setLights(0.8);
    	 tween = new TWEEN.Tween( hsLight ).to( {intensity: 0}, 500 ).start();
    	 
		if(shs.hotspotData.description)
			removeHotspotText();
    	
		//shs = null;

	}
	
	
	function powerOf2Down(value)
	{
		if(value < 80) 
			return 64;
		else if(value < 150) 
			return 128;
		else if(value < 400) 
			return 256;
		else if(value < 800)
			return 512;
		
		return 1024;
	}

	 function powerOf2Up(value)
	{
		if(value <= 64) 
			return 64;
		else if(value <= 128) 
			return 128;
		else if(value <= 256) 
			return 256;
		else if(value <= 512)
			return 512;
		
		return 1024;
	}
	 function getCompoundBoundingBox(object3D) {
		    var box = null;
		    object3D.traverse(function (obj3D) {

		        var geometry = obj3D.geometry;
		        if (geometry === undefined) return;
		        geometry.computeBoundingBox();
		        if (box === null) {
		            box = geometry.boundingBox;
		        } else {
		            box.union(geometry.boundingBox);
				}
		    });
		    return box;
		}
	
	
	