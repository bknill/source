/**
 * Created by Ben on 10/06/2016.
 */
var _fs,
    _dirEntry,
    _dirStorageEntry,
    localPath,
    localStoragePath,
    assetURLs = [],
    totalToLoad = 0,
    totalSize = 0,
    storedGuideData = [],
    storedSceneData = [],
    storedMediaFiles = [],
    states = {};;

function download(){
console.log("Download()");
    getGuideSize();
   //createDatabase();

}

function  checkOfflineGuides(callback){
    console.log('checkOfflineGuides()');

    listDir(cordova.file.dataDirectory,function(fileEntries){

        for(var i in fileEntries){

            //fileEntries[i].id = fileEntries[i].name.replace(/\D/g,'');

            if(fileEntries[i].name.charAt(0) == 'g')
                storedGuideData.push(fileEntries[i]);
            else if(fileEntries[i].name.charAt(0) == 's')
                storedSceneData.push(fileEntries[i]);

        }

        if(typeof callback === 'function' && callback)
            callback();

    });

    listDir(cordova.file.applicationStorageDirectory, function(fileEntries){

        for(var i in fileEntries)
            if(fileEntries[i].isFile)
                storedMediaFiles.push(fileEntries[i]);
    })


}

function ifOffline(){
     console.log('ifOffline' + isOffline());
    if(!isOffline())
        loadGuide(guideId);
    else
        checkOfflineGuides(loadOffline);

}

function loadOffline(){
        console.log("loadOffline()");

    for(var i in storedGuideData)
        if(storedGuideData[i].name.indexOf(guideId) > -1){
            loadGuideOffline(storedGuideData[i]);
            return;
        }

    loadGuideOffline(storedGuideData[0]);

}

function loadGuideOffline(fileEntry){

    readFile(fileEntry,function(data){
        var json = JSON.parse(data);

        onGuideDataLoaded(json.payload);
    });

}

function isOffline(){
    return navigator.connection.type == Connection.NONE;
}

function createDatabase() {
    console.log("createDatabase()");
    var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});


    db.transaction(function(tx) {
        tx.executeSql('DROP TABLE IF EXISTS test_table');
        tx.executeSql('CREATE TABLE IF NOT EXISTS test_table (id integer primary key, data text, data_num integer)');

        tx.executeSql("INSERT INTO test_table (data, data_num) VALUES (?,?)", ["test", 100], function(tx, res) {
            console.log("insertId: " + res.insertId + " -- probably 1");
            console.log("rowsAffected: " + res.rowsAffected + " -- should be 1");

            tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
                console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
            });

        }, function(e) {
            console.log("ERROR: " + e.message);
        });
    });
}



function downloadFile(name, url, callback, counter){

    var fileTransfer = new FileTransfer();
    var uri = encodeURI(url);

    if(typeof counter === 'function' && counter)
        fileTransfer.onprogress = function(progressEvent) {
            if (progressEvent.lengthComputable)
                counter(progressEvent.loaded/progressEvent.total);

        };


    fileTransfer.download(
        uri, name, function(entry) {
            if(typeof callback === 'function' && callback())
            callback(entry);
        },

        function(error) {
            console.log("download error source " + error.source);
            console.log("download error target " + error.target);
            console.log("download error code" + error.code);
        },

        false, {
            headers: {
                "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
            }
        }
    );

}

function connectFileSystem(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onLoadFsSuccess, onErrorLoadFs);
}

function onLoadFsSuccess(fs){
    _fs = fs;
}

function createDataDirectory(callback){

    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {
        _dirEntry = dirEntry;
        localPath = dirEntry.nativeURL;
        callback();
    }, onErrorLoadFs);
}

function createStorageDirectory(callback){

    window.resolveLocalFileSystemURL(cordova.file.applicationStorageDirectory, function (dirEntry) {
        _dirStorageEntry = dirEntry;
        localStoragePath = dirEntry.nativeURL;
        callback();
    }, onErrorLoadFs);
}

function getGuideSize(){

    var c = 0;

    for(var i in guideScenes){

        var assetURL;

        if(guideScenes[i].scene.video)
            assetURL = guideScenes[i].scene.video;
        else
            assetURL = guideScenes[i].scene.panoName.slice(0, -4) + "_EX.jpg";

        get_filesize(cdnPrefix +"/"+ assetURL, function(size) {
            totalSize += size;
            c++;

            if(c == guideScenes.length)
                downloadConfirmDialogue(formatBytes(totalSize,2));
        });
    }
}

function downloadDataAndAssets(){

    createDataDirectory(function(){

        downloadFile(localPath +"/g"+ guideId +".json", jsonpPrefix +'/guide/' + guideId + '/json', function(){
            console.log('guide data downloaded');
        },null);

        for(var i in guideScenes)
            downloadFile(localPath + "/s"+ guideScenes[i].scene.id +".json", jsonpPrefix +'/scene/' + guideScenes[i].scene.id + '/json', function(){
                console.log(guideScenes[i].title + ' scene data downloaded');
            },null);

    });

    createStorageDirectory(function(){

        //var totalPerc = 0;
        for(var i in guideScenes) {

            var assetURL;

            if (guideScenes[i].scene.video)
                assetURL = guideScenes[i].scene.video;
            else
                assetURL = guideScenes[i].scene.panoName.slice(0, -4) + "_EX.jpg";

            assetURLs.push(assetURL);

            for(var p in guideScenes[i].photos)
                assetURLs.push(guideScenes[i].photos[p].file.realName);

            //for(var p in guideScenes[i].sounds)
            //    assetURLs.push(guideScenes[i].photos[p].file.realName);
            //sounds

            //videos

        }


        downloadAssets(assetURLs.length);

    });


}

function search(source) {
    var results;
    var types = [ ".jpg" , ".png" , ".mp3 ", ".mp4"];

    var matches = [];
    var regexp = new RegExp(subject, 'g');

    for (var i = 0; i < objects.length; i++) {
        for (key in objects[i]) {
            if (objects[i][key].match(regexp)) matches.push(objects[i][key]);
        }
    }
    return matches;
}

function downloadAssets(total){

    var assetURL =  assetURLs.pop();

    downloadFile(localStoragePath + assetURL, cdnPrefix + '/'+ assetURL, function(){

        if(assetURLs.length > 0)
            downloadAssets(total);
        else
            $("#download")[0].innerHTML = '<i class="fa fa-check"></i>';

    },function(loaded){
        var perc = (((total - (assetURLs.length + 1)) + loaded) /total) * 100;
        $("#download")[0].innerHTML = "<span>" + Math.floor(perc) + "%</span>";
    });
}

function createFile(name, data){

    if(!_dirEntry)
        createDirectory();

    _fs.root.getFile(dir +"/"+name, { create: true, exclusive: false }, function (fileEntry) {
        writeFile(fileEntry, data);
    }, onErrorCreateFile);

}

function writeFile(fileEntry, dataObj) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function() {
            console.log("Successful file read...");
            readFile(fileEntry);
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file read: " + e.toString());
        };

        // If we are appending data to file, go to the end of the file.
        if (isAppend) {
            try {
                fileWriter.seek(fileWriter.length);
            }
            catch (e) {
                console.log("file doesn't exist!");
            }
        }
        fileWriter.write(dataObj);
    });
}

function readFile(fileEntry,callback) {

    fileEntry.file(function (file) {
        var reader = new FileReader();

        reader.onloadend = function() {
            callback(this.result)};

        reader.readAsText(file);

    }, onErrorReadFile);
}

function get_filesize(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("HEAD", url, true);
    xhr.onreadystatechange = function() {
        if (this.readyState == this.DONE) {
            callback(parseInt(xhr.getResponseHeader("Content-Length")));
        }
    };
    xhr.send();
}

function downloadConfirmDialogue(size){

    var message = "Download " + guideData.title + " to this device? (" +size+ ")";
    var title = "CONFIRM";
    var buttonLabels = "YES,NO";

    navigator.notification.confirm(message, confirmCallback, title, buttonLabels);

    function confirmCallback(buttonIndex) {
        if(buttonIndex == 1)
            downloadDataAndAssets();
    }
}

function formatBytes(bytes,decimals) {
    if(bytes == 0) return '0 Byte';
    var k = 1000; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function listDir(path, callback){

    window.resolveLocalFileSystemURL(path,
        function (fileSystem) {
            var reader = fileSystem.createReader();
            reader.readEntries(
                function (entries) {
                    callback(entries);
                },
                function (err) {
                    console.log(err);
                }
            );
        }, function (err) {
            console.log(err);
        }
    );
}



function onErrorCreateFile(){console.log("onErrorCreateFile()")}

function onErrorReadFile(){console.log("onErrorReadFile()")}

function onErrorLoadFs(){console.log("onErrorLoadFs()")}



function checkConnection() {
    var networkState = navigator.connection.type;

    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    console.log('Connection type: ' + states[networkState]);
    return states[networkState];
}
