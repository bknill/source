#!/usr/bin/env python

import argparse
import moviepanorama as mp
import s3file as s3



videoProcess = mp.VideoProcess("/home/ubuntu/test/templates/pto_template1.pto", 
        "/home/ubuntu/test/1.mp4", "/home/ubuntu/test/2.mp4", "/home/ubuntu/test/images/", 
        "/home/ubuntu/test/audio/", "/home/ubuntu/test/work/", "/home/ubuntu/test/output/",
        "/home/ubuntu/test/testing-out.mp4")
videoProcess.seperateVideo()
videoProcess.stitchImage()
videoProcess.mergeVideo()
