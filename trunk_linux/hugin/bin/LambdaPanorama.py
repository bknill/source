from __future__ import print_function
import json
import boto3
import time
import urllib
import s3file
import tempfile
import os



def lambda_handler(event, context):
    
    bucket = event['Records'][0]['s3']['bucket']['name']
    video1 = urllib.unquote_plus(event['Records'][0]['s3']['object']['video1'])
    video2 = urllib.unquote_plus(event['Records'][0]['s3']['object']['video2'])
    pto = urllib.unquote_plus(event['Records'][0]['s3']['object']['ptoFile'])
    ouput = urllib.unquote_plus(event['Records'][0]['s3']['object']['output'])
    
    s3file.S3Config.getInstance().setBucket(bucket)
    
    # setup the working environment
    tempBase = tempfile.mkdtemp()
    imagesDir = "%s%s%s" %(tempBase,os.sep,"images")
    os.makedirs(imagesDir)
    audioDir = "%s%s%s" %(tempBase,os.sep,"audio")
    os.makedirs(audioDir)
    workDir = "%s%s%s" %(tempBase,os.sep,"work")
    os.makedirs(workDir)
    outputDir = "%s%s%s" %(tempBase,os.sep,"output")
    os.makedirs(outputDir)
    
    # download s3 file 1
    videoFile1 = "%s%s%s" %(tempBase,os.sep,video1)
    s3download1 = s3file.S3DownloadFile(video1,videoFile1)
    s3download1.wait()
    s3download1.download()
    
    # download s3 file 2
    videoFile2 = "%s%s%s" %(tempBase,os.sep,video2)
    s3download2 = s3file.S3DownloadFile(video2,videoFile2)
    s3download1.wait()
    s3download1.download()
    
    # download the pto file
    ptoFile = "%s%s%s" %(tempBase,os.sep,pto)
    s3downloadPto = s3file.S3DownloadFile(video2,ptoFile)
    s3downloadPto.wait()
    s3downloadPto.download()
    
    # process out the video
    outputFile = "%s%s%s" %(tempBase,os.sep,output)
    videoProcess = mp.VideoProcess(ptoFile, 
            videoFile1, videoFile2, imagesDir, 
            audioDir, workDir, outputDir,outputFile)
    videoProcess.seperateVideo()
    videoProcess.stitchImage()
    videoProcess.mergeVideo()
    
    # upload the s3
    s3uploadVideo = s3file.S3UploadFile(outputFile,output)
    s3uploadVideo.upload()
    
    