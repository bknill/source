
import subprocess
import glob
import tempfile
import os
import tempfile
import av
import moviepy.editor as mp
import numpy as np

#
# The config data
class ConfigData:
    def __init__(self):
        self.fps = 25
        self.codec = "libx264"
    
    @staticmethod
    def getInstance():
        config = ConfigData()
        return config
    
    def getFps(self):
        return self.fps
    
    def setFps(self,fps):
        self.fps = fps
        
    def getCodec(self):
        return self.codec
    
    def setCodec(self,codec):
        self.codec = codec

#
# Frame stitcher
class FrameStitcher:
    
    def __init__(self, ptoTemplateFile, workDirectory, outputDirectory, image1, image2, frameName):
        self.ptoTemplateFile = ptoTemplateFile
        self.workDirectory = workDirectory
        self.outputDirectory = outputDirectory
        self.image1 = image1
        self.image2 = image2
        self.frameName = frameName
        
        
    def createPTOFile(self):
        ptoFile = open(self.ptoTemplateFile,"r+")
        fileTemplate = ptoFile.read()
        ptoFile.close()
        dict = {"image_path_1":self.image1,"image_path_2":self.image2}
        ptoOutputFile = tempfile.NamedTemporaryFile(dir=self.workDirectory,delete=False)
        self.ptoWorkingFile = os.path.abspath(ptoOutputFile.name)
        print ("Create the temporary file %s" %(self.ptoWorkingFile))
        ptoOutputFile.write(fileTemplate.format(**dict))
        ptoOutputFile.close()
        
        
    def undistortImages(self):
        arguments = ['nona', '-o', self.workDirectory + os.sep + 'remapped', self.ptoWorkingFile]
        print arguments
        subprocess.call(arguments)
    
    def stitchImages(self):    
        self.stitchedImage = self.outputDirectory + os.sep + self.frameName + '.tif'
        subprocess.call(['enblend', '--primary-seam-generator=graph-cut', '-o',
                     self.stitchedImage] + glob.glob(self.workDirectory + os.sep + 'remapped*'))
    
    def stitchImage(self):
        self.createPTOFile()
        self.undistortImages()
        self.stitchImages()
        return self.stitchedImage

#
# This object is responsible for seperting the video
class SeperateVideo:
    
    def __init__(self, videoPath, imageDirectory, prefix, audioDirectory):
        self.videoPath = videoPath
        self.imageDirectory = imageDirectory
        self.prefix = prefix
        self.audioDirectory = audioDirectory
        self.files = []
        
    def seperateImages(self):
        clip = mp.VideoFileClip(self.videoPath)
        ConfigData.getInstance().setFps(clip.fps)
        print "Fps %s" %(ConfigData.getInstance().getFps())
        #ConfigData.getInstance().setCodec(clip.codec)
        print "Fps %s" %(ConfigData.getInstance().getCodec())

        self.files = clip.write_images_sequence(self.imageDirectory+os.sep+self.prefix + "-%04d.jpeg")
        return self.files
        
    def seperateAudio(self):
        clip = mp.VideoFileClip(self.videoPath)
        self.audioClip = self.audioDirectory + os.sep + self.prefix + ".mp3"
        clip.audio.write_audiofile(self.audioClip,codec="mp3")
        return self.audioClip

class VideoProcess:

    
    def __init__(self, ptoTemplate, video1, video2, imageDirectory, audioDirectory, workDirectory, outputDirectory, videoOutput):
        self.ptoTemplate = ptoTemplate
        self.video1 = video1
        self.video2 = video2
        self.head, self.videoName1 = os.path.split(video1)
        self.head, self.videoName2 = os.path.split(video2)
        self.imageDirectory = imageDirectory
        self.videoImageDirectory1 = self.imageDirectory + os.sep + self.videoName1
        self.videoImageDirectory2 = self.imageDirectory + os.sep + self.videoName2
        self.audioDirectory = audioDirectory
        self.workDirectory = workDirectory
        self.ouputDirectory = outputDirectory
        self.videoOutput = videoOutput
        self.stitchedFrames = []
        
    def seperateVideo(self):
        os.mkdir(self.videoImageDirectory1)
        self.seperateVideo1 = SeperateVideo(self.video1,self.videoImageDirectory1,self.videoName1,self.audioDirectory)
        self.video1Images = self.seperateVideo1.seperateImages()
        self.audio1 = self.seperateVideo1.seperateAudio()
        os.mkdir(self.videoImageDirectory2)
        self.seperateVideo2 = SeperateVideo(self.video2,self.videoImageDirectory2,self.videoName2,self.audioDirectory)
        self.video2Images = self.seperateVideo2.seperateImages()
        self.audio2 = self.seperateVideo2.seperateAudio()
        
    def stitchImage(self):
        for index in range(0, len(self.video1Images)):
            head, frame1 = os.path.split(self.video1Images[index])
            head, frame2 = os.path.split(self.video2Images[index])
            stitcher = FrameStitcher(self.ptoTemplate, self.workDirectory, self.ouputDirectory, self.video1Images[index], self.video2Images[index], frame1+ "-" + frame2)
            self.stitchedFrames.append(stitcher.stitchImage())
            
    def mergeVideo(self):
        audio = mp.CompositeAudioClip([mp.AudioFileClip(self.audio1),mp.AudioFileClip(self.audio2)])
        print "Process directory %s" %(self.ouputDirectory)
        images = mp.ImageSequenceClip(self.ouputDirectory,fps=ConfigData.getInstance().getFps()).set_audio(audio)
        images.write_videofile(self.videoOutput,codec=ConfigData.getInstance().getCodec())
        return None