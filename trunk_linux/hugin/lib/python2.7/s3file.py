

import boto
import sys, os
from boto.s3.key import Key

class S3Config:
    def __init__(self):
        self.awsAccessKeyId = "" 
        self.awsSecretAccessKey = ""
        self.bucketName = "mybucket"
    
    @staticmethod
    def getInstance():
        config = S3Config()
        return config
    
    def getKey():
        return self.awsAccessKeyId
    
    def setKey(awsAccessKeyId):
        self.awsAccessKeyId = awsAccessKeyId
    
    def getSecret():
        return self.awsSecretAccessKey
    
    def setSecret(awsSecretAccessKey):
        self.awsSecretAccessKey = awsSecretAccessKey
        
    def getBucket():
        return self.bucketName
    
    def setBucket(bucketName):
        self.bucketName = bucket

class S3DownloadFile:
    def __init__(self,key,target):
        self.key = key
        self.target = target
        self.conn = boto.connect_s3(S3Config.getInstance().getKey(),S3Config.getInstance().getSecret())
        
        
    def download(self):
        bucket = conn.get_bucket(S3Config.getInstance().getBucket())
        bucket_list = bucket.list()
        for l in bucket_list:
            keyString = str(l.key)
            if keyString == self.key and not os.path.exists(self.target):
                l.get_contents_to_filename(self.target)
                break
    
    def wait(self):
        waiter.wait(Bucket=S3Config.getInstance().getBucket(), Key=self.key)
        
    
class S3UploadFile:
    def __init__ (self,sourceFile,key):
        self.sourceFile = sourceFile
        self.key = key
    
    def upload(self):
        conn = boto.connect_s3(S3Config.getInstance().getKey(),S3Config.getInstance().getSecret())
        bucket = conn.get_bucket(S3Config.getInstance().getBucket())
        k = Key(bucket)
        k.key = source.key
        k.set_contents_from_filename(self.sourceFile)
        k.make_public()

